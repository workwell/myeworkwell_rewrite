﻿<%@ Page Language="C#"  MasterPageFile="~/myeworkwell_webapp_dashboard.master" AutoEventWireup="true" CodeFile="ViewPanels.aspx.cs" Inherits="ViewPanels" %>

<asp:Content ID="ei_ViewPP" ContentPlaceHolderID="contentHldr" Runat="Server">

    <asp:Panel runat="server" ID="AuthenticatedMessagePanel">
        
        <h1>View Physician Panels</h1>
        <div class="divider"></div>
        <div class="appFormLeft"></div> 

            <asp:Panel ID="nwPanelSearchPanel" runat="server" DefaultButton="ipSearchBtn">
                    
                <asp:Table ID="nwPanelSearchTbl" runat="server" Width="860" >                 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Panel Name:&nbsp;</asp:TableCell>
                        <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="nwCompanyNameTxt" runat="server" Width="575"  class="txtbox"></asp:TextBox></asp:TableCell>
                        <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="2"><asp:ImageButton ID="srSearchClearBtn" runat="server" ImageUrl="images/appBtnClear.gif" OnClick="ClearForm" CssClass="smallappBtn"/><asp:ImageButton ID="ipSearchBtn" runat="server" ImageUrl="images/appBtnSearch.gif" OnClick="GetPanels" CssClass="smallappBtn"/></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow HorizontalAlign="Right">
                        <asp:TableCell Wrap="false">&nbsp;</asp:TableCell>                    
                    </asp:TableRow>                                                                                                                                                        
                </asp:Table>
        
            </asp:Panel>

            <asp:Panel ID="searchResultsPanel" runat="server" Visible="false">

                <asp:GridView ID="panelSearchRsltsGrd" Runat="server" 
                    AutoGenerateColumns="False" 
                    AllowPaging="True" 
                    PageSize="35" 
                    AlternatingRowStyle-Wrap="False" 
                    RowStyle-Wrap="False" 
                    GridLines="None" 
                    Width="860" 
                    Font-Name="Calibri" 
                    color="#ffffff"
                    CssClass="mGrid"
                    PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt"
                    RowStyle-HorizontalAlign="Left" 
                    DataKeyNames="FileName"       
                     OnRowCommand ="panelSearchRsltsGrd_RowCommand"    
                     OnPageIndexChanging = "panelSearchRsltsGrd_PageIndexChanging"        
                    >
                    <columns>        
                        <asp:TemplateField HeaderText="Panel Name">
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("FileName") %>' 
                                NavigateUrl =  '<%# Eval("FileDir") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>                              
                        <asp:boundfield datafield="UploadDate" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="center" ItemStyle-Wrap="false" HeaderText="Uploaded" ItemStyle-Width="100"/>
                        <asp:TemplateField HeaderStyle-Width="50" ControlStyle-Width="80" ItemStyle-Width="80"  ItemStyle-BackColor="White" ControlStyle-BackColor="White">
                            <ItemTemplate>
                                <asp:LinkButton ID="acctEditBtn" CommandArgument='<%# Eval("FileName") %>' CausesValidation="false" CommandName="View" runat="server" CssClass="smallappBtnGrd">
                                    <asp:Image ID="imgEditImage" ImageUrl="~/Images/appBtnView.gif" ToolTip="" runat="server" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>                                         
                    </columns>
                </asp:GridView>
                
                <asp:Table runat="server" ID="nwNoResultsFoundTbl" Width="860" Font-Name="Calibri" Visible="false" CssClass="mGrid">
                    <asp:TableFooterRow>
                        <asp:TableHeaderCell Width="400" HorizontalAlign="Center">Panel Name</asp:TableHeaderCell>
                        <asp:TableHeaderCell Width="200" HorizontalAlign="Center">Date Uploaded</asp:TableHeaderCell>
                    </asp:TableFooterRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="6" HorizontalAlign="Left" BackColor="LightYellow"><asp:Label ID="Label1" runat="server" Font-Bold="true" BackColor="LightYellow" ForeColor="Black" Text="No Physcian Panels found."></asp:Label></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>     
                   
            </asp:Panel>

    </asp:Panel>
    
    <asp:Panel runat="Server" ID="AnonymousMessagePanel">
        <asp:HyperLink runat="server" ID="lnkLogin" Text="Log In**" NavigateUrl="~/Login.aspx"></asp:HyperLink>        
    </asp:Panel>

</asp:Content>


