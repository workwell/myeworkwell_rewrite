﻿<%@ Page Title="" Language="VB" MasterPageFile="~/myeworkwell_webapp_dashboard.master" AutoEventWireup="false" CodeFile="ei_SearchSR.aspx.vb" Inherits="ei_SearchSR" Debug="true"%>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="ei_SearchSR" ContentPlaceHolderID="contentHldr" Runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" AsyncPostBackTimeout="5000"  runat="server">
        <Scripts>			          
	        <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
			<asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
			<asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
		</Scripts>
	</telerik:RadScriptManager>
    <telerik:RadNotification ID="error_notification" runat="server"  Height="150px" Width="350px" 
             ShowCloseButton="true" Font-Bold="true" Font-Size="XX-Large" Position="Center" Animation="Fade" AutoCloseDelay="1000">
    </telerik:RadNotification>

    <asp:HiddenField id="reportType" runat="server" Value="0"/>
    <asp:HiddenField id="EditVal" runat="server" Value="0"/>
    <asp:HiddenField id="ipCustAccountID" runat="server" Value="0"/>
    <asp:HiddenField id="empPartyID" runat="server" Value="0"/>
    <asp:HiddenField id="statusReportID" runat="server" Value="0"/>
    <asp:HiddenField id="writeItVal" runat="server" Value="0"/>
    <asp:HiddenField id="PrvDOECons" runat="server" Value=""/>
    <asp:HiddenField id="IPCOns" runat="server" Value=""/>
    <asp:HiddenField id="MLTDOECons" runat="server" Value=""/>
  
    <asp:Panel runat="server" ID="AuthenticatedMessagePanel">
         
        <h1>Patient Status Report Search</h1>
        <div class="divider"></div>
        <div class="appFormLeft"></div> 

         <asp:Panel ID="eiSearchStatusReportsPanel" runat="server" Visible="True">  
                         
            <asp:Table ID="eiSearchStatusReportsFieldsTbl" runat="server" Width="860">  
                
                <asp:TableHeaderRow>
                    <asp:TableCell HorizontalAlign="Left">First Name</asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">Last Name</asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="5">Social Security #</asp:TableCell>
                    <asp:TableCell ColumnSpan="4" HorizontalAlign="Left">Date of Injury Range</asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">&nbsp;</asp:TableCell>
                </asp:TableHeaderRow>                                                                  
                    
                <asp:TableRow>
                    <asp:TableCell ><asp:TextBox runat="Server" ID="ipViewFirstNameTxt"  Width="150px" class="txtbox"></asp:TextBox><asp:Label  runat="Server" ID="ipViewFirstNameErrorPointer"  Font-Bold="True" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                    <asp:TableCell ><asp:TextBox  runat="Server" ID="ipViewLastNameTxt"  Width="150px" class="txtbox"></asp:TextBox><asp:Label  runat="Server" ID="ipViewLastNameErrorPointer"  Font-Bold="True" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                    <asp:TableCell ><asp:TextBox  runat="Server" ID="ipViewSS1Txt"  Width="30px" class="txtbox"></asp:TextBox></asp:TableCell>
                    <asp:TableCell ><label style="width:10px; text-align:right;">-</label></asp:TableCell>
                    <asp:TableCell ><asp:TextBox  runat="Server" ID="ipViewSS2Txt"  Width="30px" class="txtbox"></asp:TextBox></asp:TableCell>
                    <asp:TableCell ><label style="width:10px; text-align:right;">-</label></asp:TableCell>
                    <asp:TableCell ><asp:TextBox  runat="Server" ID="ipViewSS3Txt"  Width="40px" class="txtbox"></asp:TextBox></asp:TableCell>
                    <asp:TableCell Wrap="False"  Width="30">Start:</asp:TableCell>
                    <asp:TableCell Wrap="False"  Width="66"><asp:TextBox  runat="Server" ID="ipViewDOIStartRangeTxt"  Width="60px" class="txtbox"></asp:TextBox>&nbsp;<asp:Label  runat="Server" ID="ipViewstartDOIRangeErrorPointer"  Font-Bold="True" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                    <asp:TableCell Wrap="False"  Width="30">End:</asp:TableCell>
                    <asp:TableCell Wrap="False"  Width="66"><asp:TextBox  runat="Server" ID="ipViewDOIEndRangeTxt"   Width="60px" class="txtbox"></asp:TextBox>&nbsp;<asp:Label  runat="Server" ID="ipViewendDOIRangeErrorPointer"  Font-Bold="True" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                    <asp:TableCell Wrap="false" HorizontalAlign="Right" ><asp:ImageButton ID="eiClearSearch" runat="server" ImageUrl="images/appBtnClear.gif" CssClass="smallappBtn" OnClick="NewViewSearch"/><asp:ImageButton ID="ipViewSearchBtn" runat="server" ImageUrl="images/appBtnSearch.gif" OnClick="SearchViewStsRpt" CssClass="smallappBtn"/></asp:TableCell>
                    <asp:TableCell Wrap="false" HorizontalAlign="Right" >
                        <asp:Button ID="btnAnalytics" runat="server" Visible="true" ForeColor="White" BackColor="#77a4c6" Text="Analytics" PostBackUrl="~/analytics.aspx" OnClick="NewViewSearch" />

     <%--                   <asp:ImageButton ID="ImgAnalyitics" runat="server" Visible="true" PostBackUrl="~/analytics.aspx" ImageUrl="~/images/btnanalytics.png" CssClass="smallappBtn" OnClick="NewViewSearch"/>--%>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow HorizontalAlign="Right">
                    <asp:TableCell Wrap="false" ColumnSpan="3" >&nbsp;</asp:TableCell>                    
                </asp:TableRow>                                   
            </asp:Table> 
            <asp:Label ID="startViewSRDOIRangeError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: Invalid Starting Date Of Injury date!<BR>" Visible="False"></asp:Label>
            <asp:Label ID="endViewSRDOIRangeError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: Invalid Ending Date Of Injury date!<BR>" Visible="False"></asp:Label>
            <asp:Label ID="startDateAfterEndDateError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: The Starting Date Must Be BEFORE The Ending date!<BR>" Visible="False"></asp:Label>  
        </asp:Panel>    


         <asp:Panel runat="server" ID="ciStatusRptsViewSrchResultsPanel" Visible="false">            
                <asp:Panel ID="ipViewSearchResultsPanel" runat="server" Visible="false">
                       
                        
                    <asp:GridView ID="ciStatusRptViewGrd" Runat="server" 
                        AutoGenerateColumns="False" 
                        AllowPaging="True" 
                        AlternatingRowStyle-BackColor="#C3E4ED" 
                        PageSize="15" 
                        AlternatingRowStyle-Wrap="True" 
                        RowStyle-Wrap="False" 
                        GridLines="None" 
                        Width="860" 
                        DataKeyNames="STATUSREPORTID, IPCUSTACCTID"
                        Font-Name="Calibri" 
                        color="#ffffff"
                        CssClass="mGrid"
                        PagerStyle-CssClass="pgr"
                        AlternatingRowStyle-CssClass="alt"
                        RowStyle-HorizontalAlign="Left">
                        <columns>                                 
                            <asp:boundfield HeaderText="Name" datafield="IPFIRSTNAME" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Left"/>
                            <asp:boundfield HeaderText="Employer Name" datafield="IPEMPLOYERNAME" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Left"/>
                            <asp:boundfield Headertext="Date of Injury" datafield="IPDOI" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" DataFormatString="{0:M/d/yyyy}" />
                            <asp:boundfield Headertext="Date of Exam" datafield="IPDOE" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" DataFormatString="{0:M/d/yyyy}" />
                            <asp:boundfield Headertext="Social Security #" datafield="SOCIALSECURITYNUMBER" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center"/>
                            <asp:boundfield Headertext="Type" datafield="REPORT_TYPE" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center"/>
                            
                            <asp:TemplateField HeaderStyle-Width="50" ControlStyle-Width="50" ItemStyle-Width="50" Headertext="Triage Rpt" Visible="false">
                                <ItemTemplate>
                                    <asp:ImageButton ID="triage_btn" ImageUrl="~/Images/appBtnView.gif" CommandArgument='<%# Eval("IPCUSTACCTID")%>' CommandName="triage"  runat="server" CssClass="smallappBtnGrd" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderStyle-Width="80" ControlStyle-Width="80" ItemStyle-Width="80"  ItemStyle-BackColor="White" ControlStyle-BackColor="White" ItemStyle-HorizontalAlign="Center" Headertext="Status Rpt">
                                <ItemTemplate>
                                    <asp:LinkButton ID="ibtnDelete" runat="server" CommandName="showRelAppt" CommandArgument='<%# Eval("STATUSREPORTID") %>'><asp:Image ID="imgEditImage" ImageUrl="../Media/layout/btn/appBtnView.gif" ToolTip="" runat="server" BorderStyle="None" CssClass="smallappBtnGrd"/></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                    
                        </columns>
                    </asp:GridView>  
                            
                    <asp:Table runat="server" ID="srNoViewSearchResultsTbl" Width="860" Font-Name="Calibri" Visible="false" CssClass="mGrid" Font-Size="Small">
                        <asp:TableFooterRow  ForeColor="White" Font-Bold="True" BackColor="#000000" Font-Size = "12px">
                            <asp:TableHeaderCell HorizontalAlign="Center">Name</asp:TableHeaderCell>
                            <asp:TableHeaderCell HorizontalAlign="Center">Employer Name</asp:TableHeaderCell>
                            <asp:TableHeaderCell HorizontalAlign="Center">Date of Injury</asp:TableHeaderCell>
                            <asp:TableHeaderCell HorizontalAlign="Center">Social Security #</asp:TableHeaderCell> 
                            <asp:TableHeaderCell HorizontalAlign="Center">Type</asp:TableHeaderCell>                                   
                            <asp:TableHeaderCell HorizontalAlign="Center">&nbsp;</asp:TableHeaderCell>
                        </asp:TableFooterRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="6" HorizontalAlign="Left" BackColor="LightYellow"><asp:Label ID="Label2" runat="server" Font-Bold="true" BackColor="LightYellow" ForeColor="Black" Text="No Employees found."></asp:Label></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>   
                    
                    <asp:Table ID="ipViewSearchNewSearchTbl" runat="server" width="860">
                        <asp:TableRow>
                            <asp:TableCell>&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" >
                                <asp:ImageButton ID="ciNewSearchViewStsRptBtn" runat="server" ImageUrl="images/appBtnNewSrch.png" OnClick="NewViewSearch" CssClass="smallappBtn"/>
                            </asp:TableCell>
                        </asp:TableRow>		                
	                </asp:Table> 
                                           
                         
                </asp:Panel>  
            </asp:Panel>

            

            <asp:Panel ID="StatusReprtFormPanel" runat="server" Visible="false" Width="860">
                
                <asp:Panel ID="ciCreateSRHdr" Visible="false" runat="server">
                    <h1>Create New Status Report</h1>
                    <div class="divider"></div>
                    <div class="appFormLeft"></div>
                </asp:Panel>
                <asp:Panel ID="ciReCheckSRHdr" Visible="false" runat="server">
                    <h1>ReCheck Status Report</h1>
                    <div class="divider"></div>
                    <div class="appFormLeft"></div>
                </asp:Panel>                 
                <asp:Panel ID="ciEditSRHdr" Visible="false" runat="server">
                    <h1>Edit Existing Status Report</h1>
                    <div class="divider"></div>
                    <div class="appFormLeft"></div>
                </asp:Panel> 
                <asp:Panel ID="ciRTWSRHdr" Visible="false" runat="server">
                    <h1>Return To Work Status Report</h1>
                    <div class="divider"></div>
                    <div class="appFormLeft"></div>
                </asp:Panel> 
                         
                <br />                
                <div class="shortAppForm">
                                         
                    <asp:Table ID="EmployeeInfoTbl" runat="server" Width="500">                                               
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100">First Name:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="srIPFNameTxt" runat="server" Width="150" class="txtbox"></asp:TextBox>&nbsp;<asp:Label ID="srNoIPFNameErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100">Middle:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="srIPMNameTxt" runat="server" Width="60" class="txtbox"></asp:TextBox>&nbsp;<asp:Label ID="Label10" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label>&nbsp;</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" Width="100">Last Name:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="srIPLNameTxt" runat="server" Width="150" class="txtbox"></asp:TextBox>&nbsp;<asp:Label ID="srNoIPLNameErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>                           
                        </asp:TableRow>                        
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Social Security #:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="left"><asp:TextBox ID="srSocialSecurityNumTxt" runat="server" Width="100" class="txtbox"></asp:TextBox></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right">Date Of Birth:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="left"><asp:TextBox ID="srIPDOBTxt" runat="server" Width="60" class="txtbox"></asp:TextBox>&nbsp;<asp:Label ID="srNotValidDOBErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right">Claim #:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="left"><asp:TextBox ID="srIPSSNumClaimNumTxt" runat="server" Width="150" class="txtbox"></asp:TextBox>&nbsp;<asp:Label ID="srNoIPSSNClaimErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right">Employer Name:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="5"><asp:TextBox ID="srIPEmployerNameTxt" runat="server" Width="420" class="txtbox"></asp:TextBox>&nbsp;<asp:Label ID="srNoIPEmployerErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label>&nbsp;</asp:TableCell>                                         
                        </asp:TableRow>   
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right">Date Of Exam:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="left" ><asp:TextBox ID="srIPDOETxt" runat="server" Width="60" class="txtbox"></asp:TextBox>&nbsp;<asp:Label ID="srNoDOEErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" >Date Of Injury:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="left" ><asp:TextBox ID="srIPDOITxt" runat="server" Width="60" class="txtbox"></asp:TextBox>&nbsp;<asp:Label ID="srNoDOIErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                        </asp:TableRow>                                      
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100">Clinic:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:DropDownList id="srClincTypeDrp" 
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">--</asp:ListItem>                                                    
                                    <asp:ListItem Value="1">CONSULTING CLINIC</asp:ListItem> 
                                    <asp:ListItem Value="2">WORKWELL CLINIC</asp:ListItem>                                                    
                                </asp:DropDownList>&nbsp;<asp:Label ID="srNoClinicChosenErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100">Practice Name:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="5"><asp:TextBox ID="srClinicNameTxt" runat="server" Width="420" class="txtbox"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>                                        
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100px">Status:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ><asp:DropDownList id="srActiveStatusDrp" 
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="1" selected="true">ACTIVE</asp:ListItem>                                                    
                                    <asp:ListItem Value="0">INACTIVE</asp:ListItem>                                                     
                                </asp:DropDownList></asp:TableCell>
                        </asp:TableRow>    
                        <asp:TableRow ID="reporTypeRow" runat="server" Visible="false">
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100px">Visit Type:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ><asp:DropDownList id="srVisitTypeDrp"  AutoPostBack="true" 
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">IP</asp:ListItem>                                                    
                                    <asp:ListItem Value="1">ReCheck</asp:ListItem>   
                                    <asp:ListItem Value="2">RTW</asp:ListItem>                            
                                </asp:DropDownList></asp:TableCell>
                        </asp:TableRow>           
                    </asp:Table>
                    <asp:Table ID="aghEmpTimeInOutTbl" runat="server" Visible="False">
                        <asp:TableRow>
                            <asp:TableCell>&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100">Time In:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="srTimeInTxt" runat="server" Width="60" class="txtbox"></asp:TextBox></asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                           <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100">Time Out:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="srTimeOutTxt" runat="server" Width="60" class="txtbox"></asp:TextBox></asp:TableCell>                            
                        </asp:TableRow>                        
                    </asp:Table>
                </div>

                <div class="appHeader"> 
                    <!--creates the top edge-->
                    <div class="appHeaderTop"> <img src="images/sectionTopLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionTopRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                    <!--the mid section-->
                    <div class="sectionTitle"> <img src="images/sectionMidLeft.jpg" width="8" height="15" alt="" class="sectionLeftCorner" />
                                <h3>Diagnosis</h3>
                            </div>
                    <!--the bottom-->
        
                    <div class="appHeaderBottom"> <img src="images/sectionBottomLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionBottomRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                </div>
                
                <div class="shortAppForm">
                    <asp:Table ID="DiagnosisInfoTbl" runat="server" Width="650">
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="right" Width="100">Body Part:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="left" ColumnSpan="5"><asp:TextBox ID="srBodyPartTxt" runat="server" Width="350" class="txtbox"></asp:TextBox></asp:TableCell>                                   
                        </asp:TableRow>     
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100"><span style="vertical-align:top;">Diagnosis:</span></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ><asp:TextBox ID="srDiagonsisTxt" runat="server" class="txtbox" Width="540" Height="50" TextMode="MultiLine"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>  
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100"><span style="vertical-align:top;">Objective Findings:</span></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ><asp:TextBox ID="srObjectiveFindingsTxt" runat="server" class="txtbox" Width="540" Height="50" TextMode="MultiLine"></asp:TextBox></asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100"><span style="vertical-align:top;">Mechanism of Injury:</span></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ><asp:TextBox ID="srMechanismOfInjuryTxt" runat="server" class="txtbox" Width="540" Height="50" TextMode="MultiLine"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>                                        
                    </asp:Table>       
                </div>

                <div class="appHeader"> 
                    <!--creates the top edge-->
                    <div class="appHeaderTop"> <img src="images/sectionTopLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionTopRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                    <!--the mid section-->
                    <div class="sectionTitle"> <img src="images/sectionMidLeft.jpg" width="8" height="15" alt="" class="sectionLeftCorner" />
                                <h3>Diagnostics Needed</h3>
                            </div>
                    <!--the bottom-->
        
                    <div class="appHeaderBottom"> <img src="images/sectionBottomLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionBottomRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                </div>

               
                    <asp:Table ID="DiagnosticsInfoTbl" runat="server" Width="600" CssClass="clearBoth">
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="right">Type:</asp:TableCell>
                            <asp:TableCell Wrap="false"><asp:CheckBox ID="srDiagXRayChk" Text="&nbsp;&nbsp;X-ray" runat="server"/></asp:TableCell>
                            <asp:TableCell Wrap="false"><asp:CheckBox ID="srDiagMRIChk" Text="&nbsp;&nbsp;MRI" runat="server"/></asp:TableCell>
                            <asp:TableCell Wrap="false"><asp:CheckBox ID="srDiagEMGNCVChk" Text="&nbsp;&nbsp;EMG/NCV"  runat="server"/></asp:TableCell>
                            <asp:TableCell Wrap="false"><asp:CheckBox ID="srDiagOtherChk" Text="&nbsp;&nbsp;Other" runat="server"/><asp:Label ID="srOtherNotChkedErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                            <asp:TableCell Wrap="false"><asp:TextBox ID="srDiagOtherTxt" runat="server" Width="175" class="txtbox"></asp:TextBox><asp:Label ID="srOtherNotEnteredErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>                            
                        </asp:TableRow>      
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="right" Width="100">Body Part:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="left" ColumnSpan="5"><asp:TextBox ID="srDiagBodyPartTxt" runat="server" Width="350" class="txtbox"></asp:TextBox></asp:TableCell>                                   
                        </asp:TableRow>  
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="right" Width="100">Reason:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="left"  ColumnSpan="5"><asp:TextBox ID="srReasonTxt" runat="server" Width="350" class="txtbox"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>                                                                          
                    </asp:Table>
               

                <div class="appHeader"> 
                    <!--creates the top edge-->
                    <div class="appHeaderTop"> <img src="images/sectionTopLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionTopRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                    <!--the mid section-->
                    <div class="sectionTitle"> <img src="images/sectionMidLeft.jpg" width="8" height="15" alt="" class="sectionLeftCorner" />
                                <h3>Therapy Needed</h3>
                            </div>
                    <!--the bottom-->
        
                    <div class="appHeaderBottom"> <img src="images/sectionBottomLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionBottomRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                </div>

                <div class="shortAppForm">
                    <asp:Table ID="TherapyInfoTbl" runat="server" Width="750">
                        <asp:TableRow VerticalAlign="middle">
                            <asp:TableCell  Wrap="false" Width="100" HorizontalAlign="right" >Type:</asp:TableCell>
                            <asp:TableCell Wrap="false"><asp:CheckBox ID="srPTChk" Text="&nbsp;&nbsp;Physical Therapy" runat="server"/>&nbsp;&nbsp;</asp:TableCell>
                            <asp:TableCell Wrap="false" ColumnSpan="2" Width="200"><asp:CheckBox ID="srOTChk" Text="&nbsp;&nbsp;Occupational Therapy" runat="server" />&nbsp;&nbsp;</asp:TableCell>
                            <asp:TableCell Wrap="false" ColumnSpan="2"><asp:CheckBox ID="srWHChk" Text="&nbsp;&nbsp;Work Hardening"  runat="server" />&nbsp;<asp:Label ID="srNoTherapyCheckedErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>                                                 
                           
                                
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="right" >Visit Type:</asp:TableCell>
                            <asp:TableCell Wrap="false" Width="100"><asp:DropDownList id="srVistTypeDrp" 
                                        runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                        <asp:ListItem value="0" selected="true">--</asp:ListItem>
                                        <asp:ListItem value="1">INITIAL EVAL</asp:ListItem>
                                        <asp:ListItem value="2">CONTINUE</asp:ListItem>
                                        <asp:ListItem value="4">CONTINUE AS PREVIOUSLY SCRIPTED</asp:ListItem>   
                                        <asp:ListItem value="3">DISCONTINUE</asp:ListItem>                                                    
                                    </asp:DropDownList><asp:Label ID="srVisitTypeErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="right" Width="100">&nbsp;&nbsp;Times per Week:</asp:TableCell>
                            
                            <asp:TableCell Wrap="false" Width="100"><asp:DropDownList id="srTimesPerWeekDrp" 
                                        runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                        <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem> 
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                        <asp:ListItem Value="6">2-3</asp:ListItem>                                                    
                                        <asp:ListItem Value="7">3-4</asp:ListItem>
                                        <asp:ListItem Value="8">3-5</asp:ListItem>
                                         
                                    </asp:DropDownList><asp:Label ID="srTherapyNoPerWeekErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                            <asp:TableCell Wrap="false" Width="20" HorizontalAlign="left">Weeks:</asp:TableCell>
                            <asp:TableCell Wrap="false" Width="100" HorizontalAlign="left"><asp:TextBox ID="srTherapyWeeksTxt" runat="server" Width="20" class="txtbox"></asp:TextBox><asp:Label ID="srTherapyNoWeeksErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                        </asp:TableRow>                                                                                                                
                    </asp:Table> 
                </div>

                <div class="appHeader"> 
                    <!--creates the top edge-->
                    <div class="appHeaderTop"> <img src="images/sectionTopLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionTopRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                    <!--the mid section-->
                    <div class="sectionTitle"> <img src="images/sectionMidLeft.jpg" width="8" height="15" alt="" class="sectionLeftCorner" />
                                <h3>Treatment</h3>
                            </div>
                    <!--the bottom-->
        
                    <div class="appHeaderBottom"> <img src="images/sectionBottomLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionBottomRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                </div>

                <div class="shortAppForm">                    
                    <asp:Table ID="srTreatmentTbl" runat="server" Width="650">
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100"><span style="vertical-align:top;">Medication:</span></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ><asp:TextBox ID="srMedicationTxt" runat="server" class="txtbox" Width="540" Height="50" TextMode="MultiLine"></asp:TextBox></asp:TableCell>
                            </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100"><span style="vertical-align:top;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Instructions:</span></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ><asp:TextBox ID="srInstructionsTxt" runat="server" class="txtbox" Width="540" Height="50" TextMode="MultiLine"></asp:TextBox></asp:TableCell>
                            </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100"><span style="vertical-align:top;">Other:</span></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ><asp:TextBox ID="srOtherTxt" runat="server" class="txtbox" Width="540" Height="50" TextMode="MultiLine"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>                                        
                    </asp:Table>   
                </div>

                <div class="appHeader"> 
                    <!--creates the top edge-->
                    <div class="appHeaderTop"> <img src="images/sectionTopLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionTopRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                    <!--the mid section-->
                    <div class="sectionTitle"> <img src="images/sectionMidLeft.jpg" width="8" height="15" alt="" class="sectionLeftCorner" />
                                <h3>Work Status</h3>
                            </div>
                    <!--the bottom-->
        
                    <div class="appHeaderBottom"> <img src="images/sectionBottomLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionBottomRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                </div>

                <div class="shortAppForm">
                    <center>
                        <asp:Table ID="aghEmpDisclaimerTbl" runat="server" Visible="False">
                            <asp:TableRow>
                                <asp:TableCell>Please contact Barbara Shealey at 412-359-4522 regarding work restrictions.</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>&nbsp;</asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </center>
                    <asp:Table ID="srWorkStatusTbl" runat="server" Width="400">                        
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="right" Width="100">Status:</asp:TableCell>
                            <asp:TableCell Wrap="false"><asp:DropDownList id="srWorkStatusDrp" AutoPostBack="true" 
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">RETURN TO FULL DUTY</asp:ListItem>
                                    <asp:ListItem Value="2">UNABLE TO RETURN TO WORK</asp:ListItem>
                                    <asp:ListItem Value="3">RETURN TO MODIFIED DUTY</asp:ListItem>                                                     
                                </asp:DropDownList><asp:Label ID="srNoWorkStatusErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>                                                                                       
                        </asp:TableRow>                            
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="right" Width="100">Effective:</asp:TableCell>
                            <asp:TableCell Wrap="false"><asp:TextBox ID="srEffectiveDateTxt" runat="server" Width="60" class="txtbox"></asp:TextBox>&nbsp;<asp:Label ID="srEffectiveDateErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="right" runat="server" ID="ciUntilDateLblCell" Visible="false">Until:</asp:TableCell>
                            <asp:TableCell Wrap="false" runat="server" ID="ciUntilDateTxtCell" Visible="false"><asp:TextBox ID="srUntilDateTxt" runat="server" Width="60" class="txtbox"></asp:TextBox>&nbsp;<asp:Label ID="srUntilDateErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                        </asp:TableRow>                                                                  
                    </asp:Table>
                    <asp:Table ID="srOtherCommentsTbl" runat="server">
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right">Other Restrictions/Comments:</asp:TableCell> 
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="3"><asp:TextBox ID="srOtherRestrictionsTxt" runat="server" class="txtbox" Width="540" Height="50" TextMode="MultiLine"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>                                                                                                                  
                    </asp:Table>
                </div>
                <asp:Panel ID="ciModifiedRestrictionsPanel" runat="server" Visible="false">
                    <div class="appHeader"> 
                        <!--creates the top edge-->
                        <div class="appHeaderTop"> <img src="images/sectionTopLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionTopRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                        <!--the mid section-->
                        <div class="sectionTitle"> <img src="images/sectionMidLeft.jpg" width="8" height="15" alt="" class="sectionLeftCorner" />
                                    <h3>Modified Duty Restrictions</h3>
                                </div>
                        <!--the bottom-->
        
                        <div class="appHeaderBottom"> <img src="images/sectionBottomLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionBottomRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                    </div>

                    <asp:Table ID="srModifiedDutyRestrictionsTbl" runat="server" Width="400">
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="right" Width="100" style="text-decoration:underline;">Patient can lift & carry</asp:TableCell>
                            <asp:TableCell ColumnSpan="1">&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="2">&nbsp;</asp:TableCell>                                                                                       
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell  Wrap="false" HorizontalAlign="Right">Lift:</asp:TableCell>
                            <asp:TableCell Wrap="false"><asp:DropDownList id="srCanLiftDrp" AutoPostBack="true"  
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">&nbsp;0 - 10LBS</asp:ListItem>
                                    <asp:ListItem Value="2">11 - 20LBS</asp:ListItem>
                                    <asp:ListItem Value="3">21 - 50LBS</asp:ListItem>
                                    <asp:ListItem Value="4">51 - 100LBS</asp:ListItem>
                                    <asp:ListItem Value="5">AS TOLERATED</asp:ListItem>
                                    <asp:ListItem Value="6">NOT AT ALL</asp:ListItem>
                                    <asp:ListItem Value="7">OTHER</asp:ListItem>                                                       
                                </asp:DropDownList></asp:TableCell>   
                           <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanLiftLblCell" Visible="false">Other:</asp:TableCell>
                           <asp:TableCell Wrap="false" HorizontalAlign="Left" runat="server" ID="srCanLiftTxtCell" Visible="false"><asp:TextBox ID="srCanLiftOtherTxt" runat="server" Width="200" class="txtbox"></asp:TextBox><asp:Label ID="srCanLiftOtherErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>                                                                                                                    
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell  Wrap="false" HorizontalAlign="Right">Carry:</asp:TableCell>
                            <asp:TableCell Wrap="false"><asp:DropDownList id="srCanCarryDrp" AutoPostBack="true" 
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">&nbsp;0 - 10LBS</asp:ListItem>
                                    <asp:ListItem Value="2">11 - 20LBS</asp:ListItem>
                                    <asp:ListItem Value="3">21 - 50LBS</asp:ListItem>
                                    <asp:ListItem Value="4">51 - 100LBS</asp:ListItem>
                                    <asp:ListItem Value="5">AS TOLERATED</asp:ListItem>
                                    <asp:ListItem Value="6">NOT AT ALL</asp:ListItem>
                                    <asp:ListItem Value="7">OTHER</asp:ListItem>                                                       
                                </asp:DropDownList></asp:TableCell>   
                           <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanCarryLblCell" Visible="false">Other:</asp:TableCell>
                           <asp:TableCell Wrap="false" HorizontalAlign="Left" runat="server" ID="srCanCarryTxtCell" Visible="false"><asp:TextBox ID="srCanCarryOtherTxt" runat="server" Width="200" class="txtbox"></asp:TextBox><asp:Label ID="srCanCarryOtherErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>                                                                                                                    
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left">&nbsp;</asp:TableCell>                                                                                       
                        </asp:TableRow>  
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" style="text-decoration:underline;">Patient Can Use Hands for Repetitive</asp:TableCell> 
                            <asp:TableCell ColumnSpan="1">&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="2">&nbsp;</asp:TableCell>                                                                                       
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell  Wrap="false" HorizontalAlign="Right">Simple Grasping:</asp:TableCell>
                            <asp:TableCell  Wrap="false" HorizontalAlign="left"><asp:DropDownList id="srSimpleGraspingDrp"   AutoPostBack="true"
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">RIGHT HAND ONLY</asp:ListItem>
                                    <asp:ListItem Value="2">LEFT HAND ONLY</asp:ListItem>
                                    <asp:ListItem Value="3">BOTH HANDS</asp:ListItem> 
                                    <asp:ListItem Value="4">NEITHER HAND</asp:ListItem> 
                                    <asp:ListItem Value="5">OTHER</asp:ListItem>                                                       
                                </asp:DropDownList></asp:TableCell>   
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srSimpleGraspingLblCell" Visible="false">Other:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" runat="server" ID="srSimpleGraspingTxtCell" Visible="false"><asp:TextBox ID="srSimpleGraspingOtherTxt" runat="server" Width="200" class="txtbox"></asp:TextBox><asp:Label ID="srSimpleGraspingOtherErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>              
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell  Wrap="false" HorizontalAlign="Right">Pushing/Pulling:</asp:TableCell> 
                            <asp:TableCell  Wrap="false" HorizontalAlign="Left"><asp:DropDownList id="srPushingPullingDrp"   AutoPostBack="true"
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                     <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">RIGHT HAND ONLY</asp:ListItem>
                                    <asp:ListItem Value="2">LEFT HAND ONLY</asp:ListItem>
                                    <asp:ListItem Value="3">BOTH HANDS</asp:ListItem> 
                                    <asp:ListItem Value="4">NEITHER HAND</asp:ListItem> 
                                    <asp:ListItem Value="5">OTHER</asp:ListItem>                                                                
                                </asp:DropDownList></asp:TableCell>              
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srPushingPullingLblCell" Visible="false">Other:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" runat="server" ID="srPushingPullingTxtCell" Visible="false"><asp:TextBox ID="srPushingPullingOtherTxt" runat="server" Width="200" class="txtbox"></asp:TextBox><asp:Label ID="srPushingPullingOtherErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>                              
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell  Wrap="false" HorizontalAlign="Right">Fine Manipulation:</asp:TableCell>
                            <asp:TableCell  Wrap="false" HorizontalAlign="Left"><asp:DropDownList id="srFineManipulationDrp"   AutoPostBack="true"
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                     <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">RIGHT HAND ONLY</asp:ListItem>
                                    <asp:ListItem Value="2">LEFT HAND ONLY</asp:ListItem>
                                    <asp:ListItem Value="3">BOTH HANDS</asp:ListItem> 
                                    <asp:ListItem Value="4">NEITHER HAND</asp:ListItem> 
                                    <asp:ListItem Value="5">OTHER</asp:ListItem>                                                        
                            </asp:DropDownList></asp:TableCell>         
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srFineManipulationLblCell" Visible="false">Other:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" runat="server" ID="srFineManipulationTxtCell" Visible="false"><asp:TextBox ID="srFineManipulationOtherTxt" runat="server" Width="200" class="txtbox"></asp:TextBox><asp:Label ID="srFineManipulationOtherErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>                              
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left">&nbsp;</asp:TableCell>                                                                                       
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="right" style="text-decoration:underline;">Patient Can Use Feet For Repetitive Motion</asp:TableCell> 
                            <asp:TableCell ColumnSpan="1">&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow> 
                            <asp:TableCell Wrap="false" HorizontalAlign="Left">&nbsp;</asp:TableCell>                                                                                       
                        </asp:TableRow>
                        <asp:TableRow>                                                                                     
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" >Right Foot:</asp:TableCell> 
                            <asp:TableCell  Wrap="false" HorizontalAlign="Left"><asp:DropDownList id="srRepeatRFootWorkDrp"  AutoPostBack="true"
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">YES</asp:ListItem>
                                    <asp:ListItem Value="2">NO</asp:ListItem>  
                                    <asp:ListItem Value="3">OTHER</asp:ListItem>                                                                                                     
                                </asp:DropDownList>
                            </asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srRepeatRFootWorkLblCell" Visible="false">Other:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" runat="server" ID="srRepeatRFootWorkTxtCell" Visible="false"><asp:TextBox ID="srRepeatRFootWorkOtherTxt" runat="server" Width="200" class="txtbox"></asp:TextBox><asp:Label ID="srRepeatRFootWorkOtherErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow> 
                                <asp:TableCell Wrap="false" HorizontalAlign="Right" >Left Foot:</asp:TableCell> 
                                <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:DropDownList id="srRepeatLFootWorkDrp"   AutoPostBack="true"
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">YES</asp:ListItem>
                                    <asp:ListItem Value="2">NO</asp:ListItem>
                                    <asp:ListItem Value="3">OTHER</asp:ListItem>                                                                                                       
                                </asp:DropDownList>
                            </asp:TableCell> 
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srRepeatLFootWorkLblCell" Visible="false">Other:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" runat="server" ID="srRepeatLFootWorkTxtCell" Visible="false"><asp:TextBox ID="srRepeatLFootWorkOtherTxt" runat="server" Width="200" class="txtbox"></asp:TextBox><asp:Label ID="srRepeatLFootWorkOtherErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>                                            
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="2">&nbsp;</asp:TableCell>                                                                                       
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="right" style="text-decoration:underline;">In a Work Day Patient is Able To</asp:TableCell> 
                            <asp:TableCell ColumnSpan="1">&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow> 
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="2">&nbsp;</asp:TableCell>                                                                                       
                        </asp:TableRow>
                       
                         <asp:TableRow>
                                <asp:TableCell Wrap="false" HorizontalAlign="right">Sit:</asp:TableCell>
                                <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:DropDownList id="srCanSitPerDayDrp"  AutoPostBack="true"
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="-1" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="0">0</asp:ListItem>
                                    <asp:ListItem Value="1">1</asp:ListItem>
                                    <asp:ListItem Value="2">2</asp:ListItem>
                                    <asp:ListItem Value="3">3</asp:ListItem>
                                    <asp:ListItem Value="4">4</asp:ListItem>
                                    <asp:ListItem Value="5">5</asp:ListItem>
                                    <asp:ListItem Value="6">6</asp:ListItem>
                                    <asp:ListItem Value="7">7</asp:ListItem>
                                    <asp:ListItem Value="8">8</asp:ListItem>
                                    <asp:ListItem Value="9">9</asp:ListItem>
                                    <asp:ListItem Value="10">10</asp:ListItem>
                                    <asp:ListItem Value="11">11</asp:ListItem>
                                    <asp:ListItem Value="12">12</asp:ListItem>
                                    <asp:ListItem Value="13">OTHER</asp:ListItem>
                                </asp:DropDownList>&nbsp;hrs/day&nbsp;<asp:Label ID="srCanSitPerDayErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label>                                                
                            </asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanSitPerDayLblCell" Visible="false">Other:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" runat="server" ID="srCanSitPerDayTxtCell" Visible="false"><asp:TextBox ID="srCanSitPerDayOtherTxt" runat="server" Width="200" class="txtbox"></asp:TextBox><asp:Label ID="srCanSitPerDayOtherErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="right">Stand:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:DropDownList id="srCanStandPerDayDrp"  AutoPostBack="true"
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="-1" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="0">0</asp:ListItem>
                                    <asp:ListItem Value="1">1</asp:ListItem>
                                    <asp:ListItem Value="2">2</asp:ListItem>
                                    <asp:ListItem Value="3">3</asp:ListItem>
                                    <asp:ListItem Value="4">4</asp:ListItem>
                                    <asp:ListItem Value="5">5</asp:ListItem>
                                    <asp:ListItem Value="6">6</asp:ListItem>
                                    <asp:ListItem Value="7">7</asp:ListItem>
                                    <asp:ListItem Value="8">8</asp:ListItem>
                                    <asp:ListItem Value="9">9</asp:ListItem>
                                    <asp:ListItem Value="10">10</asp:ListItem>
                                    <asp:ListItem Value="11">11</asp:ListItem>
                                    <asp:ListItem Value="12">12</asp:ListItem>
                                    <asp:ListItem Value="13">OTHER</asp:ListItem>
                                </asp:DropDownList>&nbsp;hrs/day&nbsp;<asp:Label ID="srCanStandPerDayErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label>                                                
                            </asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanStandPerDayLblCell" Visible="false">Other:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" runat="server" ID="srCanStandPerDayTxtCell" Visible="false"><asp:TextBox ID="srCanStandPerDayOtherTxt" runat="server" Width="200" class="txtbox"></asp:TextBox><asp:Label ID="srCanStandPerDayOtherErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="right">Walk:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:DropDownList id="srCanWalkPerDayDrp"  AutoPostBack="true"
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="-1" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="0">0</asp:ListItem>
                                    <asp:ListItem Value="1">1</asp:ListItem>
                                    <asp:ListItem Value="2">2</asp:ListItem>
                                    <asp:ListItem Value="3">3</asp:ListItem>
                                    <asp:ListItem Value="4">4</asp:ListItem>
                                    <asp:ListItem Value="5">5</asp:ListItem>
                                    <asp:ListItem Value="6">6</asp:ListItem>
                                    <asp:ListItem Value="7">7</asp:ListItem>
                                    <asp:ListItem Value="8">8</asp:ListItem>
                                    <asp:ListItem Value="9">9</asp:ListItem>
                                    <asp:ListItem Value="10">10</asp:ListItem>
                                    <asp:ListItem Value="11">11</asp:ListItem>
                                    <asp:ListItem Value="12">12</asp:ListItem>
                                    <asp:ListItem Value="13">OTHER</asp:ListItem>
                                </asp:DropDownList>&nbsp;hrs/day&nbsp;<asp:Label ID="srCanWalkPerDayErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label>                                                
                            </asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanWalkPerDayLblCell" Visible="false">Other:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" runat="server" ID="srCanWalkPerDayTxtCell" Visible="false"><asp:TextBox ID="srCanWalkPerDayOtherTxt" runat="server" Width="200" class="txtbox"></asp:TextBox><asp:Label ID="srCanWalkPerDayOtherErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="right">Drive:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:DropDownList id="srCanDrivePerDayDrp"  AutoPostBack="true"
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="-1" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="0">0</asp:ListItem>
                                    <asp:ListItem Value="1">1</asp:ListItem>
                                    <asp:ListItem Value="2">2</asp:ListItem>
                                    <asp:ListItem Value="3">3</asp:ListItem>
                                    <asp:ListItem Value="4">4</asp:ListItem>
                                    <asp:ListItem Value="5">5</asp:ListItem>
                                    <asp:ListItem Value="6">6</asp:ListItem>
                                    <asp:ListItem Value="7">7</asp:ListItem>
                                    <asp:ListItem Value="8">8</asp:ListItem>
                                    <asp:ListItem Value="9">9</asp:ListItem>
                                    <asp:ListItem Value="10">10</asp:ListItem>
                                    <asp:ListItem Value="11">11</asp:ListItem>
                                    <asp:ListItem Value="12">12</asp:ListItem>
                                    <asp:ListItem Value="13">OTHER</asp:ListItem>
                                </asp:DropDownList>&nbsp;hrs/day&nbsp;<asp:Label ID="srCanDrivePerDayErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label>                                                
                            </asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanDrivePerDayLblCell" Visible="false">Other:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" runat="server" ID="srCanDrivePerDayTxtCell" Visible="false"><asp:TextBox ID="srCanDrivePerDayOtherTxt" runat="server" Width="200" class="txtbox"></asp:TextBox><asp:Label ID="srCanDrivePerDayOtherErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                        </asp:TableRow> 
                    </asp:Table>
                        
                    <asp:Table runat="server" ID="srRestrictionsTypesTbl">
                        
                    </asp:Table>
                        
                    <asp:Table ID="ciPatientIsAbleToTbl" runat="server" Width="860">            
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left">&nbsp;</asp:TableCell>                                                                                       
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" style="text-decoration:underline;">Patient is Able To</asp:TableCell> 
                            <asp:TableCell ColumnSpan="3">&nbsp;</asp:TableCell>                                                                                      
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left">&nbsp;</asp:TableCell>                                                                                       
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100">Bend:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:DropDownList id="srCanBendDrp"   AutoPostBack="true"
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">FREQUENTLY</asp:ListItem>
                                    <asp:ListItem Value="2">OCCASIONALLY</asp:ListItem>
                                    <asp:ListItem Value="3">NOT AT ALL</asp:ListItem>  
                                    <asp:ListItem Value="4">OTHER</asp:ListItem>                                                                                                     
                                </asp:DropDownList>
                            </asp:TableCell> 
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanBendBlankCell" Visible="true">&nbsp;</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanBendBlankCell2" Visible="true">&nbsp;</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanBendLblCell" Visible="false">Other:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" runat="server" ID="srCanBendTxtCell" Visible="false"><asp:TextBox ID="srCanBendOtherTxt" runat="server" Width="200" class="txtbox"></asp:TextBox><asp:Label ID="srCanBendOtherErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100">Squat:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:DropDownList id="srCanSquatDrp"  AutoPostBack="true"  
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">FREQUENTLY</asp:ListItem>
                                    <asp:ListItem Value="2">OCCASIONALLY</asp:ListItem>
                                    <asp:ListItem Value="3">NOT AT ALL</asp:ListItem>  
                                    <asp:ListItem Value="4">OTHER</asp:ListItem>                                                                                                           
                                </asp:DropDownList>
                            </asp:TableCell>                             
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanSquatLblCell" Visible="false">Other:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" runat="server" ID="srCanSquatTxtCell" Visible="false"><asp:TextBox ID="srCanSquatOtherTxt" runat="server" Width="200" class="txtbox"></asp:TextBox><asp:Label ID="srCanSquatOtherErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100">Climb Stairs:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:DropDownList id="srCanClimbStairsDrp"   AutoPostBack="true"
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">FREQUENTLY</asp:ListItem>
                                    <asp:ListItem Value="2">OCCASIONALLY</asp:ListItem>
                                    <asp:ListItem Value="3">NOT AT ALL</asp:ListItem>  
                                    <asp:ListItem Value="4">OTHER</asp:ListItem>                                                                                                        
                                </asp:DropDownList>
                            </asp:TableCell> 
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanClimbStairBlankCell" Visible="true">&nbsp;</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanClimbStairBlankCell2" Visible="true">&nbsp;</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanClimbStairsLblCell" Visible="false">Other:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" runat="server" ID="srCanClimbStairsTxtCell" Visible="false"><asp:TextBox ID="srCanClimbStairsOtherTxt" runat="server" Width="200" class="txtbox"></asp:TextBox><asp:Label ID="srCanClimbStairsOtherErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100">Climb a Ladder:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:DropDownList id="srCanClimbLadderDrp"   AutoPostBack="true"
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">FREQUENTLY</asp:ListItem>
                                    <asp:ListItem Value="2">OCCASIONALLY</asp:ListItem>
                                    <asp:ListItem Value="3">NOT AT ALL</asp:ListItem>  
                                    <asp:ListItem Value="4">OTHER</asp:ListItem>                                                                                                    
                                </asp:DropDownList>
                            </asp:TableCell>   
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanClimbLadderLblCell" Visible="false">Other:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" runat="server" ID="srCanClimbLadderTxtCell" Visible="false"><asp:TextBox ID="srCanClimbLadderOtherTxt" runat="server" Width="200" class="txtbox"></asp:TextBox><asp:Label ID="srCanClimbLadderOtherErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>                                
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100">Crawl:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:DropDownList id="srCanCrawlDrp"   AutoPostBack="true"
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">FREQUENTLY</asp:ListItem>
                                    <asp:ListItem Value="2">OCCASIONALLY</asp:ListItem>
                                    <asp:ListItem Value="3">NOT AT ALL</asp:ListItem>  
                                    <asp:ListItem Value="4">OTHER</asp:ListItem>                                                                                                          
                                </asp:DropDownList>
                            </asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanCrawlBlankCell" Visible="true">&nbsp;</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanCrawlBlankCell2" Visible="true">&nbsp;</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanCrawlLblCell" Visible="false">Other:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" runat="server" ID="srCanCrawlTxtCell" Visible="false"><asp:TextBox ID="srCanCrawlOtherTxt" runat="server" Width="200" class="txtbox"></asp:TextBox><asp:Label ID="srCanCrawlOtherErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100">Reach:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:DropDownList id="srCanReachDrp"   AutoPostBack="true"
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">FREQUENTLY</asp:ListItem>
                                    <asp:ListItem Value="2">OCCASIONALLY</asp:ListItem>
                                    <asp:ListItem Value="3">NOT AT ALL</asp:ListItem>  
                                    <asp:ListItem Value="4">OTHER</asp:ListItem>                                                                                                     
                                </asp:DropDownList>
                            </asp:TableCell> 
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" runat="server" ID="srCanReachlLblCell" Visible="false">Other:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" runat="server" ID="srCanReachCell" Visible="false"><asp:TextBox ID="srCanReachOtherTxt" runat="server" Width="200" class="txtbox"></asp:TextBox><asp:Label ID="srCanReachOtherErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>                     
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left">&nbsp;</asp:TableCell>                                                                                       
                        </asp:TableRow>
                    </asp:Table> 
                    
            </asp:Panel>

                <div class="appHeader"> 
                    <!--creates the top edge-->
                    <div class="appHeaderTop"> <img src="images/sectionTopLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionTopRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                    <!--the mid section-->
                    <div class="sectionTitle"> <img src="images/sectionMidLeft.jpg" width="8" height="15" alt="" class="sectionLeftCorner" />
                                <h3>Specialist Referral</h3>
                            </div>
                    <!--the bottom-->
        
                    <div class="appHeaderBottom"> <img src="images/sectionBottomLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionBottomRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                </div>

                <div class="shortAppForm">
                    <asp:Table ID="srSpecialistReferralTbl" runat="server" Width="400">
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right">Referred to Specialist:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:DropDownList id="srReferredToSpecialistDrp"  
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem  Selected="True" Value="0">No</asp:ListItem>
                                    <asp:ListItem Value="1">Yes</asp:ListItem>                                                                                                     
                                </asp:DropDownList>&nbsp;<asp:Label ID="srReferredToSpecialistErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label>                                                
                            </asp:TableCell>
                        </asp:TableRow>                        
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" >Reason for Referral:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="srReasonForReferraltTxt" runat="server" Width="570" class="txtbox"></asp:TextBox>&nbsp;<asp:Label ID="srReasonForSpecialistErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" >Type:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="srTypeOfSpecialistTxt" runat="server" Width="250" class="txtbox"></asp:TextBox>&nbsp;<asp:Label ID="srSpecialistTypeErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label> </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right">Physician/Practice Name:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ><asp:TextBox ID="srReferredDrNameTxt" runat="server" Width="250" class="txtbox"></asp:TextBox>&nbsp;<asp:Label ID="srNameOfSpecialistErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <asp:Table ID="srSpecialistAppoinmentTbl" runat="server" Width="200">
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" >Date:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="left" ><asp:TextBox ID="srSpecialistApptDateTxt" runat="server" Width="60" class="txtbox"></asp:TextBox>&nbsp;<asp:Label ID="srSpecialistApptDateErroPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                            <asp:TableCell Wrap="false" width="50">&nbsp;</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right">Time:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="left"><asp:DropDownList id="srSpecialistApptHourDrp" 
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">1</asp:ListItem>
                                    <asp:ListItem Value="2">2</asp:ListItem>
                                    <asp:ListItem Value="3">3</asp:ListItem>
                                    <asp:ListItem Value="4">4</asp:ListItem>
                                    <asp:ListItem Value="5">5</asp:ListItem>
                                    <asp:ListItem Value="6">6</asp:ListItem>
                                    <asp:ListItem Value="7">7</asp:ListItem>
                                    <asp:ListItem Value="8">8</asp:ListItem>
                                    <asp:ListItem Value="9">9</asp:ListItem>
                                    <asp:ListItem Value="10">10</asp:ListItem>
                                    <asp:ListItem Value="11">11</asp:ListItem>
                                    <asp:ListItem Value="12">12</asp:ListItem>
                                </asp:DropDownList></asp:TableCell>
                            <asp:TableCell Wrap="false" width="10">:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="left"><asp:DropDownList id="srSpecialistApptMinuteDrp" 
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem  Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">00</asp:ListItem>
                                    <asp:ListItem Value="2">15</asp:ListItem>
                                    <asp:ListItem Value="3">30</asp:ListItem>
                                    <asp:ListItem Value="4">45</asp:ListItem>                                                    
                                </asp:DropDownList></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="left"><asp:DropDownList id="srSpecialistApptAmPmDrp" 
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">AM</asp:ListItem>
                                    <asp:ListItem Value="2">PM</asp:ListItem>                                                    
                                </asp:DropDownList>&nbsp;<asp:Label ID="srSpecialistApptTimeErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label>                                               
                            </asp:TableCell>
                        </asp:TableRow>                                            
                    </asp:Table>
                </div>

                <div class="appHeader"> 
                    <!--creates the top edge-->
                    <div class="appHeaderTop"> <img src="images/sectionTopLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionTopRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                    <!--the mid section-->
                    <div class="sectionTitle"> <img src="images/sectionMidLeft.jpg" width="8" height="15" alt="" class="sectionLeftCorner" />
                                <h3>Follow Up Appointment</h3>
                            </div>
                    <!--the bottom-->
        
                    <div class="appHeaderBottom"> <img src="images/sectionBottomLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionBottomRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                </div>

                <div class="shortAppForm">
                    <asp:Table ID="srFollowUpAppoinmentTbl" runat="server" Width="200">
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" >Date:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="left" ><asp:TextBox ID="srNextAptDateTxt" runat="server" Width="60" class="txtbox"></asp:TextBox>&nbsp;<asp:Label ID="srNextApptDateErroPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label></asp:TableCell>
                            <asp:TableCell Wrap="false" width="50">&nbsp;</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right">Time:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="left"><asp:DropDownList id="srNextAptHourDrp" 
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">1</asp:ListItem>
                                    <asp:ListItem Value="2">2</asp:ListItem>
                                    <asp:ListItem Value="3">3</asp:ListItem>
                                    <asp:ListItem Value="4">4</asp:ListItem>
                                    <asp:ListItem Value="5">5</asp:ListItem>
                                    <asp:ListItem Value="6">6</asp:ListItem>
                                    <asp:ListItem Value="7">7</asp:ListItem>
                                    <asp:ListItem Value="8">8</asp:ListItem>
                                    <asp:ListItem Value="9">9</asp:ListItem>
                                    <asp:ListItem Value="10">10</asp:ListItem>
                                    <asp:ListItem Value="11">11</asp:ListItem>
                                    <asp:ListItem Value="12">12</asp:ListItem>
                                </asp:DropDownList></asp:TableCell>
                            <asp:TableCell Wrap="false" width="10">:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="left"><asp:DropDownList id="srNextAptMinuteDrp" 
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem  Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">00</asp:ListItem>
                                    <asp:ListItem Value="2">15</asp:ListItem>
                                    <asp:ListItem Value="3">30</asp:ListItem>
                                    <asp:ListItem Value="4">45</asp:ListItem>                                                    
                                </asp:DropDownList></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="left"><asp:DropDownList id="srNextAptAmPmDrp" 
                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">
                                    <asp:ListItem Value="0" selected="true">--</asp:ListItem>
                                    <asp:ListItem Value="1">AM</asp:ListItem>
                                    <asp:ListItem Value="2">PM</asp:ListItem>                                                    
                                </asp:DropDownList>&nbsp;<asp:Label ID="srNextApptTimeErrorPointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label>                                               
                            </asp:TableCell>
                        </asp:TableRow>                                            
                    </asp:Table>
                </div>

                <div class="appHeader"> 
                    <!--creates the top edge-->
                    <div class="appHeaderTop"> <img src="images/sectionTopLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionTopRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                    <!--the mid section-->
                    <div class="sectionTitle"> <img src="images/sectionMidLeft.jpg" width="8" height="15" alt="" class="sectionLeftCorner" />
                                <h3>Treating Physician / Case Manager</h3>
                            </div>
                    <!--the bottom-->
        
                    <div class="appHeaderBottom"> <img src="images/sectionBottomLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionBottomRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                </div>

                <div class="shortAppForm">
                    <asp:Table ID="srTreatingPhysicianCMTbl" runat="server">
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right">Treating Physician:</asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="srTreatingPhysicianTxt" runat="server" Width="200" class="txtbox"></asp:TextBox></asp:TableCell>                            
                        </asp:TableRow> 
                        <asp:TableRow>
                                <asp:TableCell Wrap="false" HorizontalAlign="Right">Case Manager:</asp:TableCell>
                                <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="srCMTxt" runat="server" Width="200" class="txtbox"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>                                       
                    </asp:Table>
                 </div>
                    <div class="appHeader"> 
                    <!--creates the top edge-->
                    <div class="appHeaderTop"> <img src="images/sectionTopLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionTopRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                    <!--the mid section-->
                    <div class="sectionTitle"> <img src="images/sectionMidLeft.jpg" width="8" height="15" alt="" class="sectionLeftCorner" />
                                <h3>Additional Email Recipients</h3>
                            </div>
                    <!--the bottom-->
        
                    <div class="appHeaderBottom"> <img src="images/sectionBottomLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionBottomRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                </div>
                
                <div class="shortAppForm">
                    <asp:Table ID="srsrRecipientTbl" runat="server" Width="500">
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100"><span style="vertical-align:top;">Recipient 1:</span></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ><asp:TextBox ID="srRecipient1" runat="server" class="txtbox" Width="150"></asp:TextBox></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100"><span style="vertical-align:top;">Recipient 2:</span></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ><asp:TextBox ID="srRecipient2" runat="server" class="txtbox" Width="150"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100"><span style="vertical-align:top;">Recipient 3:</span></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ><asp:TextBox ID="srRecipient3" runat="server" class="txtbox" Width="150"></asp:TextBox></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100"><span style="vertical-align:top;">Recipient 4:</span></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ><asp:TextBox ID="srRecipient4" runat="server" class="txtbox" Width="150"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100"><span style="vertical-align:top;">Recipient 5:</span></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ><asp:TextBox ID="srRecipient5" runat="server" class="txtbox" Width="150"></asp:TextBox></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="100"><span style="vertical-align:top;">Recipient 6:</span></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ><asp:TextBox ID="srRecipient6" runat="server" class="txtbox" Width="150"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>                                       
                    </asp:Table>       
                </div>

                    <asp:Table ID="StsRptBtnsTbl" runat="server" Width="860">
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right"><asp:ImageButton ID="CancelStsRptBtn" runat="server" ImageUrl="images/appBtnCancel.gif"  CssClass="smallappBtn"/>
                            <asp:ImageButton ID="ciSaveBtn" runat="server" ImageUrl="images/appBtnSave.png"  CssClass="smallappBtn"/><asp:ImageButton ID="ipStsRptBtn" runat="server" ImageUrl="images/appBtnSubmit.gif"  CssClass="smallappBtn"/></asp:TableCell>                                      
                        </asp:TableRow>
                    </asp:Table>

                                        
                    <asp:Label ID="srMissingInfoSpecialistError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST fill in all fields that refer to the Specialist. *<BR>" Visible="False"></asp:Label> 
                    <asp:Label ID="srNoWorkStatusError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST choose the patient's work status. *<BR>" Visible="False"></asp:Label> 
                    <asp:Label ID="srEffectiveDateNotDateError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter a valid effective date. *<BR>" Visible="False"></asp:Label> 
                    <asp:Label ID="srNotVaildDatesError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter valid dates. *<BR>" Visible="False"></asp:Label> 
                    <asp:Label ID="srEffectiveDateBeforeUntilDateError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: The UNTIL date can not come before the EFFECTIVE date. *<BR>" Visible="False"></asp:Label> 
                    <asp:Label ID="srVisitTypeError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST choose a visit type. *<BR>" Visible="False"></asp:Label>
                    <asp:Label ID="srNoModifiedChoicesError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter at LEAST ONE modified duty restriction. *<BR>" Visible="False"></asp:Label>
                    <asp:Label ID="srRestrictionsTypeError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST choos eat LEAST ONE work restriction. *<BR>" Visible="False"></asp:Label> 
                    <asp:Label ID="srNoSpecialitstTypeError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter the type of specialist. *<BR>" Visible="False"></asp:Label> 
                    <asp:Label ID="srNoRefferedToSpecialistError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter YES to referred to specialist or remove the specialist type. *<BR>" Visible="False"></asp:Label>                     
                    <asp:Label ID="srNoValidNextApptDateError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter a valid appointment date. *<BR>" Visible="False"></asp:Label> 
                    <asp:Label ID="srNoValidNextApptDateTimeError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter a valid appointment date and time or none at all. *<BR>" Visible="False"></asp:Label> 
                    <asp:Label ID="srNoValidNextApptTimeError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter a valid appointment time or none at all. *<BR>" Visible="False"></asp:Label>                        
                    <asp:Label ID="srNoClinicChosenError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST choose a clinic type. *<BR>" Visible="False"></asp:Label>  
                    <asp:Label ID="srNothingEnteredForUntilDateError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter a date or 'NEXT APPT' for the until date. *<BR>" Visible="False"></asp:Label> 
                    <asp:Label ID="srTherapyNoPerWeekError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter the number of sessions per week. *<BR>" Visible="False"></asp:Label> 
                    <asp:Label ID="srTherapyNoWeeksError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter the number of weeks for therapy. *<BR>" Visible="False"></asp:Label> 
                    <asp:Label ID="srOtherNotEnteredError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter the 'Other' type of diagnostics needed. *<BR>" Visible="False"></asp:Label>
                    <asp:Label ID="srOtherNotChkedError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST check 'Other' diagnostics. *<BR>" Visible="False"></asp:Label>
                    <asp:Label ID="srNoIPNameError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST the patient's name. *<BR>" Visible="False"></asp:Label>  
                    <asp:Label ID="srNoDOEError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter a valid exam date. *<BR>" Visible="False"></asp:Label> 
                    <asp:Label ID="srNoIPEmployerError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter the name of the employer. *<BR>" Visible="False"></asp:Label> 
                    <asp:Label ID="srNoDOIError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter valid date of injury. *<BR>" Visible="False"></asp:Label> 
                    <asp:Label ID="srNoIPSSNClaimError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter the patient's social security number or claim number. *<BR>" Visible="False"></asp:Label>
                    <asp:Label ID="srNotValidDOBError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter a valid date of birth. *<BR>" Visible="False"></asp:Label>
                    <asp:Label ID="srNoTherapyCheckedError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST choose at LEAST on therapy type. *<BR>" Visible="False"></asp:Label>
                    <asp:Label ID="srExamDateError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You CAN NOT enter a date that is in the future. *<BR>" Visible="False"></asp:Label>
                    
                    <asp:Label ID="myerror" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter the name of the facility/physician that treated the injured employee. *<BR>" Visible="False"></asp:Label> 
               
            </asp:Panel>

             


              

          
                      

                
            </asp:Panel>

            
    </asp:Panel>
    
    <asp:Panel runat="Server" ID="AnonymousMessagePanel">
        <asp:HyperLink runat="server" ID="lnkLogin" Text="Log In**" NavigateUrl="~/Login.aspx"></asp:HyperLink>        
    </asp:Panel>

</asp:Content>


