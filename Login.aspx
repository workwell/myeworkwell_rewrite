﻿<%@ Page Title="" Language="C#" MasterPageFile="~/myeworkwell_webapp.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="loginContent" ContentPlaceHolderID="contentHldr" runat="server">
        <asp:Panel runat="server" ID="loginForm">
            <div id="login">
                <h1>Login to <span class="lowercase">mye</span>WorkWell</h1>
                <div id="eWorkWellLogin"><img src="images/loginTopBkgd.jpg"  alt="" />
                    <asp:Table ID="loginTbl" runat="server" HorizontalAlign="Center" Width="300">      
                        <asp:TableRow >
                            <asp:TableCell Wrap="false" Font-Names="Calbri" Font-size="12px" CssClass="eWorkWellLoginLbl">User Name:</asp:TableCell>
                            <asp:TableCell Wrap="false"><asp:TextBox ID="UserName" runat="server" Width="285" CssClass="eWorkWellLoginTxtbx"></asp:TextBox></asp:TableCell>                     
                        </asp:TableRow> 
                        <asp:TableRow >
                            <asp:TableCell Wrap="false" Font-Names="Calbri" Font-size="12px" CssClass="eWorkWellLoginLbl">Password:</asp:TableCell>
                            <asp:TableCell Wrap="false"><asp:TextBox ID="Password" runat="server" Width="285"  CssClass="eWorkWellLoginTxtbx" TextMode="Password"></asp:TextBox></asp:TableCell>                     
                        </asp:TableRow> 
                        <asp:TableRow HorizontalAlign="Right">
                            <asp:TableCell Wrap="false" ColumnSpan="2" ><asp:ImageButton id="LoginButton" OnClick="LoginButton_Click"  runat="server" class="appBtn"  src="images/appBtnLoginSubmit.gif" /></asp:TableCell>                    
                        </asp:TableRow>
                    </asp:Table>  
                    <div id="reseteWorkWellLoginBtm"></div>                                  
                </div>  
                <center>
                    <asp:Label ID="InvalidCredentialsMessage" runat="server" ForeColor="Red"  Font-Names="Calbri" Text="Your user name or password is invalid. Please try again." Visible="False"></asp:Label>
                </center> 
            </div>
           
        </asp:Panel>

        <asp:Panel runat="server" ID="resetPassword" visible="false">
            <div id="resetlogin">
                <h1>Reset Password</h1>
                <div id="resetWorkWellLogin"><img src="images/loginTopBkgd.jpg" width="437" height="32" alt="" />
                    <asp:Table ID="resetPasswordTbl" runat="server" HorizontalAlign="Center" width="320">      
						<asp:TableRow >
							<asp:TableCell Wrap="false" Font-Names="Calbri" ForeColor="Black" Font-size="12px" CssClass="eWorkWellLoginLbl">New Password:&nbsp;</asp:TableCell>
							<asp:TableCell Wrap="false"><asp:TextBox ID="newPassword" runat="server" Width="155"  CssClass="eWorkWellLoginTxtbx" TextMode="Password"></asp:TextBox></asp:TableCell>                     
						</asp:TableRow> 
						<asp:TableRow >
							<asp:TableCell Wrap="false" Font-Names="Calbri" ForeColor="Black" Font-size="12px" CssClass="eWorkWellLoginLbl">Confirm Password:&nbsp;</asp:TableCell>
							<asp:TableCell Wrap="false"><asp:TextBox ID="confirmPassword" runat="server" Width="155"  CssClass="eWorkWellLoginTxtbx" TextMode="Password"></asp:TextBox></asp:TableCell>                     
						</asp:TableRow> 
						<asp:TableRow HorizontalAlign="Right">
							<asp:TableCell Wrap="false" ColumnSpan="2" ><asp:ImageButton id="resetPasswordBtn" OnClick="ResetButton_Click" Text="Submit" runat="server" class="appBtn"  src="images/appBtnLoginSubmit.gif" /></asp:TableCell>                    
						</asp:TableRow>
					</asp:Table> 
                    <div id="eWorkWellLoginBtm"></div>       
                </div>
                 <center>
                    <asp:Label ID="userNameLabel" runat="server" ForeColor="Green" Font-Bold="true" Font-Names="Calbri" Font-Size="Smaller" Text="Your user name has been updated. <br> You have been logged out.  You must log in for changes to take affect." Visible="false"></asp:Label>      
                    <asp:Label ID="noMatchMessage" runat="server" ForeColor="Red" Font-Names="Calbri" Font-Size="Smaller" Text="Your passwords didn't match. Please enter it again." Visible="False"></asp:Label>
                </center>
            </div>
        </asp:Panel>    
            
        <asp:Table ID="footerSpacer" runat="server">      
            <asp:TableRow >
                <asp:TableCell>&nbsp;</asp:TableCell>                                  
            </asp:TableRow> 
            <asp:TableRow >
                <asp:TableCell>&nbsp;</asp:TableCell>                                  
            </asp:TableRow> 
            <asp:TableRow >
                <asp:TableCell>&nbsp;</asp:TableCell>                                  
            </asp:TableRow>             
        </asp:Table>  
       
</asp:Content>




