﻿<%@ Page Language="C#" MasterPageFile="~/myeworkwell_webapp_dashboard.master" AutoEventWireup="true" CodeFile="RequestNewPanel.aspx.cs" Inherits="RequestNewPanel" %>

<asp:Content ID="ei_RequestNP" ContentPlaceHolderID="contentHldr" Runat="Server">

    <asp:Panel runat="server" ID="AuthenticatedMessagePanel">

        <asp:Panel ID="eiRqstPnlHdrPanel" runat="server">
            <h1>Request Physician Panels for Locations</h1>
            <div class="divider"></div>
            <div class="appFormLeft"></div> 
        </asp:Panel>

         

             <asp:Table ID="searchLocFieldsTbl" runat="server" Width="860">  
                
                <asp:TableHeaderRow><asp:TableCell HorizontalAlign="Left">Location Name</asp:TableCell><asp:TableCell HorizontalAlign="Left">Address</asp:TableCell><asp:TableCell HorizontalAlign="Left">City</asp:TableCell><asp:TableCell HorizontalAlign="Left">State</asp:TableCell><asp:TableCell HorizontalAlign="Left">Zip</asp:TableCell><asp:TableCell HorizontalAlign="Center">&nbsp;</asp:TableCell></asp:TableHeaderRow>                                              
                <asp:TableRow HorizontalAlign="Left">
                    <asp:TableCell Wrap="false" HorizontalAlign="Center"><asp:TextBox ID="searchLocationNameTxt" runat="server" Width="200"  class="txtbox" ></asp:TextBox></asp:TableCell>
                    <asp:TableCell Wrap="false" HorizontalAlign="Center"><asp:TextBox ID="searchLocationAddressTxt"  runat="server" Width="200"  class="txtbox" ></asp:TextBox></asp:TableCell>
                    <asp:TableCell Wrap="false" HorizontalAlign="Center"><asp:TextBox ID="searchLocationCityTxt" runat="server" class="txtbox" Width="100"></asp:TextBox></asp:TableCell> 
                    <asp:TableCell Wrap="false" HorizontalAlign="Center"><asp:TextBox ID="searchLocationStateTxt" runat="server" MaxLength="2" Width="20" class="txtbox" ></asp:TextBox></asp:TableCell>
                    <asp:TableCell Wrap="false" HorizontalAlign="Center"><asp:TextBox ID="searchLocationZipTxt" runat="server" MaxLength="10"  Width="60" class="txtbox" ></asp:TextBox></asp:TableCell>
                    <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="2"><asp:ImageButton ID="clearSearchFieldsBtn" runat="server" ImageUrl="images/appBtnClear.gif" OnClick="ClearSearchFields" CssClass="smallappBtn"/><asp:ImageButton ID="searchLocationsBtn" runat="server" ImageUrl="images/appBtnSearch.gif" OnClick="SearchLocations" CssClass="smallappBtn"/></asp:TableCell>
                 </asp:TableRow>
                <asp:TableRow HorizontalAlign="Right">
                    <asp:TableCell Wrap="false">&nbsp;</asp:TableCell>                    
                </asp:TableRow>                                   
            </asp:Table>

            <asp:Panel ID="accountLocationsListingPanel" runat="server" Visible="true">               


                <asp:GridView ID="viewActiveLocationsGrd" Runat="server" 
                    AutoGenerateColumns="False" 
                    AllowPaging="True" 
                    PageSize="25" 
                    AlternatingRowStyle-Wrap="True" 
                    RowStyle-Wrap="False" 
                    GridLines="None" 
                    Width="860" 
                    Font-Name="Calibri" 
                    color="#ffffff"
                    CssClass="mGrid"
                    PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt"
                    RowStyle-HorizontalAlign="Left"
                    DataKeyNames="CUST_ACCOUNT_ID"
                     OnPageIndexChanging="viewActiveLocationsGrd_PageIndexChanging">  
                    <columns>    
                        <asp:TemplateField>                                                                   
                            <ItemTemplate>
                                <asp:CheckBox ID="selectionBox" runat="server"/>
                            </ItemTemplate>
                        </asp:TemplateField>                             
                        <asp:boundfield datafield="ACCOUNT_NAME" HeaderText="Location Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="300"/>
                        <asp:boundfield datafield="ADDRESS" HeaderStyle-HorizontalAlign="Left" HeaderText="Address" ItemStyle-Width="300"/>
                        <asp:boundfield datafield="CITY" ItemStyle-HorizontalAlign="Left" HeaderText="City" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="120"/>
                        <asp:boundfield datafield="STATE" ItemStyle-HorizontalAlign="Left" HeaderText="State" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="40"/> 
                        <asp:boundfield datafield="POSTAL_CODE" ItemStyle-HorizontalAlign="Left" HeaderText="Zip" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="80"/>                            
                    </columns>
                </asp:GridView>
                
                <asp:Table runat="server" ID="noLocationsFoundTbl" Width="860" Visible="false" CssClass="mGrid">
                    <asp:TableFooterRow  ForeColor="White" Font-Bold="True" BackColor="#000000">
                        <asp:TableHeaderCell Width="260" HorizontalAlign="Left">Location Name</asp:TableHeaderCell>
                        <asp:TableHeaderCell Width="260" HorizontalAlign="Left">Address</asp:TableHeaderCell>
                        <asp:TableHeaderCell Width="110" HorizontalAlign="Left">City</asp:TableHeaderCell>
                        <asp:TableHeaderCell Width="30" HorizontalAlign="Left">State</asp:TableHeaderCell>
                        <asp:TableHeaderCell Width="70" HorizontalAlign="Left">Zip</asp:TableHeaderCell>
                        <asp:TableHeaderCell ></asp:TableHeaderCell>
                    </asp:TableFooterRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="6" HorizontalAlign="Left"><asp:Label ID="adNoLocationsLbl" runat="server" Font-Bold="true" BackColor="LightYellow" ForeColor="Black" Text="No locations found."></asp:Label></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>                                
                 
                <asp:Table ID="selectionSubmitBtnsTbl" runat="server" Width="860">
                    <asp:TableRow>
                        <asp:TableCell>&nbsp;</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="2"><asp:ImageButton ID="cancelSelectionsBtn" runat="server" ImageUrl="images/appBtnCancel.gif" OnClick="CancelSelection" CssClass="smallappBtn"/><asp:ImageButton ID="showSelectedLocsBtn" runat="server" ImageUrl="images/appBtnSubmit.gif" OnClick="ShowSelected" CssClass="smallappBtn"/></asp:TableCell>
                    </asp:TableRow>
                     
                </asp:Table>          
            </asp:Panel>

            <asp:Panel ID="confirmListingPanel" runat="server" Visible="false">

                <asp:Table ID="selectionConfirmHdrTbl" runat="server" Width="860">
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2" style="font:12px Calibri, Arial, Helvetica, sans-serif;font-weight:bold;color:#467B99;border-bottom:1px solid #8FABD6;">Selection Confirmation</asp:TableCell>
                    </asp:TableRow>                    
                </asp:Table>     

                <asp:GridView ID="requestCnfrmGrd" Runat="server" 
                    AutoGenerateColumns="False" 
                    AllowPaging="True" 
                    PageSize="25" 
                    AlternatingRowStyle-Wrap="True" 
                    RowStyle-Wrap="False" 
                    GridLines="None" 
                    Width="860" 
                     Font-Name="Calibri" 
                    color="#ffffff"
                    CssClass="mGrid"
                    PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt"
                    RowStyle-HorizontalAlign="Left"
                    DataKeyNames="CUST_ACCOUNT_ID">    
                    <columns>    
                        <asp:TemplateField>                                                                   
                            <ItemTemplate>
                                <asp:CheckBox ID="selectionBox" runat="server" Checked="true"/>
                            </ItemTemplate>
                        </asp:TemplateField>                             
                        <asp:boundfield datafield="ACCOUNT_NAME" HeaderText="Location Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="300"/>
                        <asp:boundfield datafield="ADDRESS" HeaderStyle-HorizontalAlign="Left" HeaderText="Address" ItemStyle-Width="300"/>
                        <asp:boundfield datafield="CITY" ItemStyle-HorizontalAlign="Left" HeaderText="City" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="120"/>
                        <asp:boundfield datafield="STATE" ItemStyle-HorizontalAlign="Left" HeaderText="State" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="40"/> 
                        <asp:boundfield datafield="POSTAL_CODE" ItemStyle-HorizontalAlign="Left" HeaderText="Zip" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="80"/>                            
                    </columns>
                </asp:GridView>

                <asp:Table ID="confirmationSubmitBtnsTbl" runat="server" Width="860">
                    <asp:TableRow>
                        <asp:TableCell>&nbsp;</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Wrap="false" HorizontalAlign="Right"><asp:Button id="submitRequestBtn" Text="Submit" runat="server" Font-Names="Calibri" Font-Size="10px" OnClick="SubmitRequest" /><asp:Button id="cancelRequestBtn" Text="Cancel" runat="server" Font-Names="Calibri" Font-Size="10px" OnClick="CancelRequest"/></asp:TableCell>                    
                    </asp:TableRow>
                </asp:Table>
               

            </asp:Panel>

            <asp:Panel runat="Server" ID="noneSelectedErrorPanel" Visible="false">
                    <asp:Table ID="noneSelectedErrorTbl" runat="server">                
                    <asp:TableRow>
                        <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:Label ID="noneSelectedError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST select at least ONE location above. *<BR>"></asp:Label></asp:TableCell>                    
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
    
            <asp:Panel runat="Server" ID="requestSumbitPanel" Visible="false" Width="860">
                <div id="halfPageRight1">
                    <h1>Your Requst has been Sent!</h1>
					<div class="divider"></div>
			    </div>
			    <div class="clearBoth"></div>
                <label>Click here to go to the </label><asp:HyperLink ID="savedReturnToMainPageLnk" runat="server" Font-Underline="true" ForeColor="LightSkyBlue" NavigateUrl="LandingPage.aspx">main page.</asp:HyperLink>             
            </asp:Panel>

    </asp:Panel>
    
    <asp:Panel runat="Server" ID="AnonymousMessagePanel">
        <asp:HyperLink runat="server" ID="lnkLogin" Text="Log In**" NavigateUrl="~/Login.aspx"></asp:HyperLink>        
    </asp:Panel>

</asp:Content>