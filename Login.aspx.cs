﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data.Odbc;
using System.Configuration;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Url.AbsoluteUri.Contains("iS=0"))
        {
            userNameLabel.Visible = true;
        }
    }

    protected void LoginButton_Click(object sender, System.EventArgs e)
    {
        insertDatabaseTimeStamp(UserName.Text.ToUpper());

        bool bValidLogin = false;
        int bResetPassword = -1;
        Int32 user_id = -1;

        OdbcCommand objSqlCmd = new OdbcCommand();
        OdbcDataReader objSqlDataRdr = null;
        string strConnection = ConnectionStrings.MySqlConnections();

        OdbcConnection objConnection = new OdbcConnection(strConnection);
        Response.Write("After New ODBC Connection");

        try
        {
            objConnection.Open();
            Response.Write("After Open ODBC Connection");

            string strSQL = "SELECT u.IUSERID, "
                + " u.BRESETPASSWORD, " //Flag for resetting the password
                + " a.USER_TYPE, " //Not for sure what this is for just yet
                + " u.INS_CARRIER, " //Check if an insurance carrier account tbh no idea why this isn't with the user type
                + " u.BISEMPLOYER, " //Multiple location flag
                + " a.CUST_ACCOUNT_ID " //User's cust account ID that they are responsible for
                + " FROM USER_INFO u, "
                + " ACCOUNT_PRIVS a "
                + " where u.SUSERNAME = '" + UserName.Text.ToUpper() 
                + "' and u.SPASSWORD = '" + Password.Text 
                + "' and  u.BACTIVE = 1 and u.IUSERID = a.IUSERID";
            objSqlCmd = new OdbcCommand(strSQL, objConnection);
            objSqlDataRdr = objSqlCmd.ExecuteReader();

            if (objSqlDataRdr.Read())
            {
                bValidLogin = true;
                user_id = Convert.ToInt32(objSqlDataRdr.GetValue(0));
                bResetPassword = Convert.ToInt32(objSqlDataRdr.GetValue(1));
                Session["UserType"] = objSqlDataRdr.GetValue(2);
                Session["UserName"] = UserName.Text.ToUpper();
                Session["UserID"] = objSqlDataRdr.GetValue(0);
                Session["InsCarrier"] = objSqlDataRdr.GetValue(3);
                Session["MultipleLocs"] = objSqlDataRdr.GetValue(4);
                Session["MotoristsSub"] = objSqlDataRdr.GetValue(5);
                Session["CustAccount_IDs"] = null;
            }

            objSqlDataRdr.Close();

            if (bValidLogin == true)
            {
                //This procedure searches thru account_privs table to see if the account has multiple party ids attached.
                objSqlCmd = new OdbcCommand("{call sp_getUsersAcctCount(?)}", objConnection);
                objSqlCmd.CommandType = CommandType.StoredProcedure;
                objSqlCmd.Parameters.Add("vUSERID", OdbcType.Int, 11).Value = user_id;
                objSqlDataRdr = objSqlCmd.ExecuteReader();
                if (objSqlDataRdr.Read())
                {
                    Session["mult_accts"] = objSqlDataRdr.GetValue(0); //This is the number of party ids attached to the account.
                }
                objSqlDataRdr.Close();

                //This is if the user isn't an insurance carrier
                if ((Convert.ToString(Session["InsCarrier"]) == "N") || (Convert.ToString(Session["InsCarrier"]) == "E"))
                {
                    Session["InsPartyID"] = 0;
                    strSQL = "SELECT PARTY_ID FROM ACCOUNT_PRIVS WHERE IUSERID = " + user_id;
                    objSqlCmd = new OdbcCommand(strSQL, objConnection);
                    objSqlDataRdr = objSqlCmd.ExecuteReader();

                    //Preseting these to null just for safety
                    Session["PartyID"] = null;
                    Session["Party_IDs"] = null; 
                    if (Convert.ToInt32(Session["mult_accts"]) == 1)
                    {
                        if (objSqlDataRdr.Read())
                        {
                            Session["PartyID"] = objSqlDataRdr.GetValue(0);
                        }
                        //Saving an if check
                        strSQL = "SELECT COUNT(DISTINCT(CUST_ACCOUNT_ID)) FROM ACCOUNT_PRIVS WHERE IUSERID = " + user_id + " AND CUST_ACCOUNT_ID <> 0 AND PARTY_ID = " + Session["PartyID"];
                    }
                    else if ((Convert.ToInt32(Session["mult_accts"]) > 1))
                    {
                        string tmp_party_ids = "";
                        int i = 0;

                        while (objSqlDataRdr.Read())
                        {
                            if (i == 0)
                            {
                                Session["PartyID"] = objSqlDataRdr.GetValue(0);
                            }

                            tmp_party_ids += Convert.ToString(objSqlDataRdr.GetValue(0)) + ",";
                            i++;
                        }

                        Session["Party_IDs"] = tmp_party_ids.Substring(0, tmp_party_ids.Length - 1);
                        //Saving an if check
                        strSQL = "SELECT COUNT(DISTINCT(CUST_ACCOUNT_ID)) FROM ACCOUNT_PRIVS WHERE IUSERID = " + user_id + " AND CUST_ACCOUNT_ID <> 0";
                    }

                    objSqlDataRdr.Close();
                    objSqlCmd = new OdbcCommand(strSQL, objConnection);
                    objSqlDataRdr = objSqlCmd.ExecuteReader();
                    //Setting multiple locations.
                    if (objSqlDataRdr.Read())
                    {
                        Session["mult_locs"] = objSqlDataRdr.GetValue(0);
                    }
                    objSqlDataRdr.Close();

                    //This most likly needs rewritten on the landing pag when i get to it thier is no reason to be doing half of this.
                    strSQL = "SELECT CUST_ACCOUNT_ID FROM ACCOUNT_PRIVS WHERE IUSERID = " + user_id + " AND CUST_ACCOUNT_ID <> 0 AND PARTY_ID = " + Session["PartyID"];
                    objSqlCmd = new OdbcCommand(strSQL, objConnection);
                    objSqlDataRdr = objSqlCmd.ExecuteReader();

                    if (Convert.ToInt32(Session["mult_locs"]) == 1)
                    {
                        if (objSqlDataRdr.Read())
                        {
                            Session["CustAccountID"] = objSqlDataRdr.GetValue(0);
                        }
                    }
                    else if ((Convert.ToInt32(Session["mult_locs"]) > 1))
                    {
                        string tmp_cust_acct_ids = "";

                        while (objSqlDataRdr.Read())
                        {
                            tmp_cust_acct_ids += Convert.ToString(objSqlDataRdr.GetValue(0)) + ",";
                        }

                        Session["CustAccount_IDs"] = tmp_cust_acct_ids.Substring(0, tmp_cust_acct_ids.Length - 1);

                        //Response.Write("cust_acct_ids = " + tmp_cust_acct_ids.Substring(0, tmp_cust_acct_ids.Length - 1) + "<BR>");

                    }
                    else
                    {
                        Session["CustAccountID"] = null;  // single cust_acct_id attached to user
                        Session["CustAccount_IDs"] = null; // multiple cust_acct_ids attached to user
                    }

                    objSqlDataRdr.Close();
                    objSqlDataRdr.Dispose();

                }
                else
                {
                    // insurance carrier logged in

                    strSQL = "SELECT PARTY_ID FROM ACCOUNT_PRIVS WHERE IUSERID = " + user_id;

                    objSqlCmd = new OdbcCommand(strSQL, objConnection);
                    objSqlDataRdr = objSqlCmd.ExecuteReader();

                    if (objSqlDataRdr.Read())
                    {
                        Session["InsPartyID"] = objSqlDataRdr.GetValue(0);
                    }

                    objSqlDataRdr.Close();

                    Session["PartyID"] = 0;
                    Session["Party_IDs"] = null;
                    Session["CustAccountID"] = null;
                    Session["CustAccount_IDs"] = null;
                }

                if (bResetPassword == 0)
                {
                    strSQL = "UPDATE USER_INFO SET DLASTLOGINDATE = CURDATE() WHERE IUSERID = " + user_id + " and  BACTIVE = 1";
                    objSqlCmd = new OdbcCommand(strSQL, objConnection);
                    objSqlCmd.ExecuteNonQuery();

                    objSqlCmd = null;
                    objSqlDataRdr = null;
                    objConnection.Close();
                    Response.Write("Session['PartyID'] = " + Session["PartyID"] + "<BR>");
                    FormsAuthentication.SetAuthCookie(UserName.Text, false);
                    FormsAuthentication.RedirectFromLoginPage(UserName.Text, false);
                }
                else
                {
                    // USER IS FORCED TO CHANGE THEIR PASSWORD AFTER LOGGING IN
                    loginForm.Visible = false;
                    resetPassword.Visible = true;
                    InvalidCredentialsMessage.Visible = false;

                }

            }
            else
            {
                // If we reach here, the user's credentials were invalid
                InvalidCredentialsMessage.Visible = true;
            }

        }
        catch (Exception ex)
        {
            Response.Write("Can't load Web page:" + ex.Message);
        }
        finally
        {
            objConnection.Close();
            objConnection.Dispose();
        }
    }

    protected void ResetButton_Click(object sender, System.EventArgs e)
    {

        OdbcCommand objSqlCmd = new OdbcCommand();

        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);


        try
        {
            objConnection.Open();

            if (newPassword.Text.Trim() == confirmPassword.Text.Trim())
            {
                string strSQL = "UPDATE USER_INFO SET SPASSWORD = '" + newPassword.Text + "', BRESETPASSWORD = 0 WHERE SUSERNAME = '" + UserName.Text.ToUpper() + "'";
                objSqlCmd = new OdbcCommand(strSQL, objConnection);
                objSqlCmd.ExecuteNonQuery();

                objSqlCmd = null;
                objConnection.Close();

                FormsAuthentication.SetAuthCookie(UserName.Text, false);
                FormsAuthentication.RedirectFromLoginPage(UserName.Text, false);
            }
            else
            {
                noMatchMessage.Visible = true;
            }

        }
        catch (Exception ex)
        {
            Response.Write("Can't load Web page:" + ex.Message);
        }
        finally
        {
            objConnection.Close();
            objConnection.Dispose();
        }

    }

    //This is for getting a users ip address to log on imput attempts.
    protected string GetIPAddress()
    {
        return Server.HtmlEncode(Request.UserHostAddress);
    }
    protected string getCurrentTime()
    {
        return DateTime.Now.ToString("h:mm:ss tt");
    }
    protected string getCurrentDate()
    {
        DateTime thisDay = DateTime.Today;
        return thisDay.ToString("d");
    }
    protected void insertDatabaseTimeStamp(string username)
    {
        OdbcCommand objSqlCmd = new OdbcCommand();
        OdbcDataReader objSqlDataRdr = null;
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        objConnection.Open();
        string strSQL = "INSERT INTO LOGINATTEMPTS VALUES (DEFAULT,'" + username + "','" + getCurrentTime() + "',' " + GetIPAddress() + "','" + getCurrentDate() + "')";
        objSqlCmd = new OdbcCommand(strSQL, objConnection);
        objSqlDataRdr = objSqlCmd.ExecuteReader();
        objSqlDataRdr.Close();
        objSqlDataRdr.Dispose();
    }
}