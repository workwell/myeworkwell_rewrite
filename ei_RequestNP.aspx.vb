﻿Imports System.Data
Imports System.Data.Odbc
Imports System.IO
Imports System.Net
Imports System.Web.Mail
Imports System.Net.Mail

Partial Class ei_RequestNP
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserType") = 2 Then
            Response.Redirect("iiRoadMap.aspx")
        ElseIf Session("UserType") = 3 Then
            Response.Redirect("ciStatusReport.aspx")
        ElseIf Session("UserType") = 4 Then
            Response.Redirect("MACInsurance.aspx")
        ElseIf Session("UserType") = 5 Then
            Response.Redirect("AholdIT.aspx")
        ElseIf Session("UserType") = 6 Then
            Response.Redirect("Nationwide.aspx")
        ElseIf Session("UserType") = 7 Then
            Response.Redirect("SynergyComp.aspx")
        End If

        noLocationsFoundTbl.Style("border-collapse") = "collapse"

        If Request.IsAuthenticated Then
            If Not IsPostBack Then
                SearchLocations()
            End If
            AuthenticatedMessagePanel.Visible = True
            AnonymousMessagePanel.Visible = False
        Else
            AuthenticatedMessagePanel.Visible = False
            AnonymousMessagePanel.Visible = True
        End If

    End Sub

    Sub SearchLocations()

        Dim iPartyID As Integer = Session("PartyID")
        Dim bAllNulls As Boolean = True
        Dim bLocationName As Boolean = False
        Dim bLocationAddress As Boolean = False
        Dim bLocationAddress2 As Boolean = False
        Dim bLocationCity As Boolean = False
        Dim bLocationState As Boolean = False
        Dim bLocationZip As Boolean = False
        Dim dt As New DataTable()
        Dim sSqlQuery As String = "SELECT hp.PARTY_ID, hp.ATTRIBUTE_CATEGORY, hp.PARTY_NUMBER, hp.PARTY_NAME, hca.ACCOUNT_NUMBER, " & _
                                    "       hca.ACCOUNT_NAME, hca.STATUS, CONCAT(IFNULL(hl.ADDRESS1,' '),' ',IFNULL(hl.ADDRESS2,' ')) ADDRESS, hl.CITY, hl.STATE, hl.POSTAL_CODE, hca.CUST_ACCOUNT_ID " & _
                                    "FROM  HZ_PARTIES hp, " & _
                                    "      HZ_CUST_ACCOUNTS hca, " & _
                                    "      HZ_CUST_ACCT_SITES_ALL hcasa, " & _
                                    "      HZ_PARTY_SITES hps, " & _
                                    "      HZ_LOCATIONS hl " & _
                                    "WHERE hp.PARTY_ID = hca.PARTY_ID AND hca.CUST_ACCOUNT_ID = hcasa.CUST_ACCOUNT_ID AND " & _
                                    "      hcasa.PARTY_SITE_ID = hps.PARTY_SITE_ID AND hps.LOCATION_ID = hl.LOCATION_ID AND " & _
                                    "      hp.PARTY_ID = " & iPartyID & "  AND hca.STATUS = 'A' AND " & _
                                    "      hca.ATTRIBUTE_CATEGORY = 'EMPLOYER' AND " & _
                                    "      hca.ACCOUNT_NAME LIKE '%" & searchLocationNameTxt.Text.Replace("'", "''") & "%' AND " & _
                                    "      hl.ADDRESS1 LIKE '%" & searchLocationAddressTxt.Text.Replace("'", "''") & "%' AND " & _
                                    "      hl.CITY LIKE '%" & searchLocationCityTxt.Text.Replace("'", "''") & "%' AND " & _
                                    "      hl.STATE LIKE '%" & searchLocationStateTxt.Text.Replace("'", "''") & "%' AND " & _
                                    "      hl.POSTAL_CODE LIKE '%" & searchLocationZipTxt.Text.Replace("'", "''") & "%' " & _
                                    "ORDER BY hl.CITY, hl.POSTAL_CODE, hca.ACCOUNT_NAME "


        Dim strConnection As String = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;" 'ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()
        Dim objConnection As New OdbcConnection(strConnection)
        Dim objDataAdapter As OdbcDataAdapter = New OdbcDataAdapter(sSqlQuery, objConnection)

        Try
            objConnection.Open()
            objDataAdapter.Fill(dt)

            viewActiveLocationsGrd.DataSource = dt
            viewActiveLocationsGrd.PageIndex = 0
            viewActiveLocationsGrd.DataBind()
            If dt.Rows.Count > 0 Then
                viewActiveLocationsGrd.Visible = True
                noLocationsFoundTbl.Visible = False
            Else
                viewActiveLocationsGrd.Visible = False
                noLocationsFoundTbl.Visible = True
            End If

        Catch ex As Exception
            Response.Write("Can't load Web page: " & ex.Message)
        Finally
            objConnection.Close()
            objDataAdapter.Dispose()
            objConnection.Dispose()
        End Try


    End Sub

    Sub ClearSearchFields()

        searchLocationNameTxt.Text = ""
        searchLocationAddressTxt.Text = ""
        searchLocationCityTxt.Text = ""
        searchLocationStateTxt.Text = ""
        searchLocationZipTxt.Text = ""

        SearchLocations()

    End Sub



    Protected Sub viewActiveLocationsGrd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles viewActiveLocationsGrd.PageIndexChanging


        ''*******************
        '* in this section, I need to test if the session var already exists.  If it does, I need to clear the contents
        '*******************

        noneSelectedErrorTbl.Visible = False

        Dim d As Integer = viewActiveLocationsGrd.PageCount
        Dim values As Boolean() = New Boolean(viewActiveLocationsGrd.PageSize - 1) {}
        Dim chb As CheckBox
        Dim isChecked As Integer = 0

        Dim count As Integer = 0
        For i As Integer = 0 To viewActiveLocationsGrd.Rows.Count - 1
            chb = DirectCast(viewActiveLocationsGrd.Rows(i).FindControl("selectionBox"), CheckBox)
            If chb IsNot Nothing Then
                values(i) = chb.Checked
                isChecked = 1
            End If
        Next

        If isChecked = 1 Then
            Session("page" & viewActiveLocationsGrd.PageIndex) = values
        End If

        SearchLocations()

        viewActiveLocationsGrd.PageIndex = e.NewPageIndex
        viewActiveLocationsGrd.DataBind()

        If Session("page" & e.NewPageIndex) IsNot Nothing Then
            Dim prevValues As Boolean() = DirectCast(Session("page" & e.NewPageIndex), Boolean())
            'Response.Write("page" & viewActiveLocationsGrd.PageIndex & "<BR>")
            For x = 0 To viewActiveLocationsGrd.Rows.Count - 1
                'Response.Write("values(" & x & ") = " & values(x))
                If prevValues(x) Then
                    DirectCast(viewActiveLocationsGrd.Rows(x).FindControl("selectionBox"), CheckBox).Checked = True
                End If
            Next
        End If


    End Sub

    Sub ShowSelected()

        Dim strConnection As String = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;" 'ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()
        Dim objConnection As New OdbcConnection(strConnection)
        Dim objSqlCmd As New OdbcCommand
        Dim objSqlDataRdr As OdbcDataReader = Nothing
        Dim currentGrdPage As Integer = viewActiveLocationsGrd.PageIndex
        Dim selectedLocs As New DataTable()
        Dim values As Boolean() = New Boolean(viewActiveLocationsGrd.PageSize - 1) {}
        Dim checkBx As CheckBox
        Dim isChecked As Integer = 0
        Dim curPageOneChecked As Integer = 0
        Dim bOneChecked As Integer = 0

        selectedLocs.Columns.Add("ACCOUNT_NAME")
        selectedLocs.Columns.Add("ADDRESS")
        selectedLocs.Columns.Add("CITY")
        selectedLocs.Columns.Add("STATE")
        selectedLocs.Columns.Add("POSTAL_CODE")
        selectedLocs.Columns.Add("CUST_ACCOUNT_ID")

        noneSelectedErrorTbl.Visible = False

        '--------------------------------------------------------

        'get the selections current page that user is on
        For i As Integer = 0 To viewActiveLocationsGrd.Rows.Count - 1
            checkBx = DirectCast(viewActiveLocationsGrd.Rows(i).FindControl("selectionBox"), CheckBox)
            If checkBx IsNot Nothing Then
                values(i) = checkBx.Checked
                If values(i) Then
                    curPageOneChecked = 1
                End If
            End If
        Next

        If curPageOneChecked = 1 Then
            Session("page" & viewActiveLocationsGrd.PageIndex) = values
        End If

        For i = 0 To viewActiveLocationsGrd.PageCount - 1

            If Session("page" & i) IsNot Nothing Then

                'set panelSearchRsltsGrd to page that the checked locations were

                SearchLocations()
                viewActiveLocationsGrd.PageIndex = i
                viewActiveLocationsGrd.DataBind()

                Dim prevValues As Boolean() = DirectCast(Session("page" & i), Boolean())

                For x = 0 To viewActiveLocationsGrd.Rows.Count - 1
                    If prevValues(x) Then
                        bOneChecked = 1
                    End If
                Next
            End If
        Next

        If bOneChecked = 1 Then

            'Try

            'store the selected locations on current page

            objConnection.Open()
            objSqlCmd.Connection = objConnection

            'now get all the other pages that user looked and selected

            For i = 0 To viewActiveLocationsGrd.PageCount - 1

                If Session("page" & i) IsNot Nothing Then

                    'set viewActiveLocationsGrd to page that the checked locations were

                    SearchLocations()
                    viewActiveLocationsGrd.PageIndex = i
                    viewActiveLocationsGrd.DataBind()


                    Dim prevValues As Boolean() = DirectCast(Session("page" & i), Boolean())

                    For x = 0 To viewActiveLocationsGrd.Rows.Count - 1

                        If prevValues(x) Then
                            Dim iCustAcctID As String = viewActiveLocationsGrd.DataKeys(x).Value

                            Try
                                objSqlCmd.CommandText = "SELECT hca.ACCOUNT_NAME, CONCAT(IFNULL(hl.ADDRESS1,' '),' ',IFNULL(hl.ADDRESS2,' ')) ADDRESS, hl.CITY, hl.STATE, hl.POSTAL_CODE, hca.CUST_ACCOUNT_ID " & _
                                                       "FROM  HZ_PARTIES hp, " & _
                                                       "      HZ_CUST_ACCOUNTS hca, " & _
                                                       "      HZ_CUST_ACCT_SITES_ALL hcasa, " & _
                                                       "      HZ_PARTY_SITES hps, " & _
                                                       "      HZ_LOCATIONS hl " & _
                                                       "WHERE hp.PARTY_ID = hca.PARTY_ID AND " & _
                                                       "      hca.CUST_ACCOUNT_ID = hcasa.CUST_ACCOUNT_ID AND " & _
                                                       "      hcasa.PARTY_SITE_ID = hps.PARTY_SITE_ID AND " & _
                                                       "      hps.LOCATION_ID = hl.LOCATION_ID AND " & _
                                                       "      hca.CUST_ACCOUNT_ID = " & iCustAcctID & "  AND " & _
                                                       "      hca.STATUS = 'A' AND " & _
                                                       "      hca.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                                objSqlCmd.CommandType = Data.CommandType.Text
                                objSqlDataRdr = objSqlCmd.ExecuteReader()

                                If objSqlDataRdr.Read() Then

                                    Dim dr As DataRow = selectedLocs.NewRow()
                                    dr("ACCOUNT_NAME") = myCStr(objSqlDataRdr.Item(0))
                                    dr("ADDRESS") = myCStr(objSqlDataRdr.Item(1))
                                    dr("CITY") = myCStr(objSqlDataRdr.Item(2))
                                    dr("STATE") = myCStr(objSqlDataRdr.Item(3))
                                    dr("POSTAL_CODE") = myCStr(objSqlDataRdr.Item(4))
                                    dr("CUST_ACCOUNT_ID") = myCStr(objSqlDataRdr.Item(5))
                                    selectedLocs.Rows.Add(dr)

                                End If
                                objSqlDataRdr.Close()
                                objSqlDataRdr.Dispose()
                            Catch ex As Exception
                                Response.Write("Can't load Web page " & ex.Message)
                            End Try

                        End If

                    Next
                End If
            Next

            requestCnfrmGrd.DataSource = selectedLocs
            requestCnfrmGrd.DataBind()

            'Catch ex As Exception
            ' Response.Write("Can't load Web page " & ex.Message)
            ' Finally
            objConnection.Close()
            objConnection.Dispose()
            ' End Try

            'now get back to the current page that they were on before submiting the selections
            SearchLocations()
            viewActiveLocationsGrd.PageIndex = currentGrdPage
            viewActiveLocationsGrd.DataBind()

            'keep to current page's selections checked if they click back

            Dim existingValues As Boolean() = DirectCast(Session("page" & viewActiveLocationsGrd.PageIndex), Boolean())
            'Response.Write("page" & viewActiveLocationsGrd.PageIndex & "<BR>")
            For x = 0 To viewActiveLocationsGrd.Rows.Count - 1
                'Response.Write("values(" & x & ") = " & values(x))
                If existingValues(x) Then
                    DirectCast(viewActiveLocationsGrd.Rows(x).FindControl("selectionBox"), CheckBox).Checked = True
                End If
            Next

            searchLocFieldsTbl.Visible = False
            accountLocationsListingPanel.Visible = False
            confirmListingPanel.Visible = True

        Else
            'show error
            noneSelectedErrorTbl.Visible = True
            SearchLocations()

            viewActiveLocationsGrd.PageIndex = currentGrdPage
            viewActiveLocationsGrd.DataBind()

        End If

    End Sub

    Function myCStr(ByVal test As Object) As String

        Dim sReturnStr As String = ""

        If IsDBNull(test) Then
            sReturnStr = ""
        Else
            sReturnStr = CStr(test)
        End If

        If StrComp(sReturnStr, "#") = 0 Then
            sReturnStr = ""
        End If

        myCStr = sReturnStr

    End Function


    Sub SubmitRequest()

        Dim body As New StringBuilder
        Dim message As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage
        Dim sPanelRequests As String = ""
        Dim bOneSelected As Boolean = False
        Dim values As Boolean() = New Boolean(viewActiveLocationsGrd.PageSize - 1) {}


        message.IsBodyHtml = True
        message.Subject = "Panel Request for Locations - myeWorkWell.com"
        message.From = New MailAddress("info@myeworkwell.com", "myeWorkWell Info Request")
        message.To.Add(New MailAddress("customerservice@eworkwell.com", "Customer Service"))
        message.To.Add(New MailAddress("notifications@eworkwell.com", "IT"))


        For i As Integer = 0 To requestCnfrmGrd.Rows.Count - 1

            Dim row As GridViewRow = requestCnfrmGrd.Rows(i)
            Dim isChecked As Boolean = DirectCast(row.FindControl("selectionBox"), CheckBox).Checked
            Dim sLocationName As String = ""
            Dim sAddress As String = ""

            If isChecked Then
                'e.Row.Cells[columnIndex].Attributes
                sLocationName = myCStr(row.Cells(1).Text)
                sAddress = myCStr(row.Cells(2).Text) ' address
                sAddress = sAddress & " " & myCStr(row.Cells(3).Text) ' city
                sAddress = sAddress & ", " & myCStr(row.Cells(4).Text) 'state
                sAddress = sAddress & " " & myCStr(row.Cells(5).Text) 'zip
                bOneSelected = True

                sPanelRequests = sPanelRequests & "LOCATION NAME: " & sLocationName & "<BR>" & "ADDRESS: " & sAddress & " <BR><BR>"
            End If
        Next

        With body
            .Append("The " & Session("UserName") & " has requested panels for the following location(s)." & "<br/><br/>")
            .Append(sPanelRequests)
            .Append("<hr />")
            .Append("<font size='2' color='#cccccc'>")
            .Append("<hr />")
            .Append("www.myeworkwell.com")
            .Append("<br>")
            .Append(Date.Now.AddHours(3))
            .Append("</font>")
        End With

        message.Body = body.ToString

        'relay-hosting.secureserver.net is the valid SMTP server of godaddy.com
        Dim mailClient As SmtpClient = New SmtpClient("dedrelay.secureserver.net")
        'Dim mailClient As SmtpClient = New SmtpClient("65.242.19.203")

        If bOneSelected = True Then
            mailClient.Send(message)

            'delete the session vars holding the checked locations
            For i = 0 To viewActiveLocationsGrd.PageCount - 1

                '********
                ' this is to clear all selected locations session vars, i can't remove the session vars wirh remove (above), so I'm just seting everything to false for now.
                For x As Integer = 0 To viewActiveLocationsGrd.Rows.Count - 1
                    values(x) = False
                Next
                Session("page" & i) = values
            Next

            confirmListingPanel.Visible = False
            eiRqstPnlHdrPanel.Visible = False
            requestSumbitPanel.Visible = True
        Else
            noneSelectedErrorTbl.Visible = True
        End If

        message.Dispose()

    End Sub

    Sub CancelSelection()

        Dim values As Boolean() = New Boolean(viewActiveLocationsGrd.PageSize - 1) {}

        For i = 0 To viewActiveLocationsGrd.PageCount - 1
            'Session.Remove(Convert.ToString(Session("page" & i)))
            'Session.Contents.Remove(Convert.ToString(Session("page" & i)))

            '********
            ' this is to clear all selected locations session vars, i can't remove the session vars wirh remove (above), so I'm just seting everything to false for now.
            For x As Integer = 0 To viewActiveLocationsGrd.Rows.Count - 1
                values(x) = False
            Next
            Session("page" & i) = values
        Next

        SearchLocations()

    End Sub

    Sub CancelRequest()

        confirmListingPanel.Visible = False
        accountLocationsListingPanel.Visible = True
        searchLocFieldsTbl.Visible = True

    End Sub

End Class
