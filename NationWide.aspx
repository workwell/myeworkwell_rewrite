﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NationWide.aspx.cs" Inherits="NationWideLanding" MasterPageFile="~/myeworkwell_webapp.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentHldr" Runat="Server">
    
    <asp:HiddenField ID="which_drp" Value="0" runat="server"/>

    <asp:Panel runat="server" ID="AuthenticatedMessagePanel" Visible="false">

    <asp:Panel ID="state_selection_pnl" runat="server">
    
        <asp:Table ID="state_selection_tbl" runat="server" HorizontalAlign="Left" Width="750">
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3" style="font:12px Calibri, Arial, Helvetica, sans-serif;font-weight:bold;color:#000000;border-bottom:1px solid #8FABD6;">Nationwide Physician Panel Search</asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3">&nbsp;</asp:TableCell>
                </asp:TableRow>    
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right" Wrap="false" Width="150">Please choose the state the policy holder is in:&nbsp;</asp:TableCell>
                    <asp:TableCell><asp:DropDownList ID="states_drp" 
                                                     runat="server"
                                                     AutoPostBack="true"
                                                     OnSelectedIndexChanged="select_state"
                                                     Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" 
                                                     Font-Size="X-Small">
                                                        <asp:ListItem Value="0" Text="--"></asp:ListItem>
                                                        <asp:ListItem Value="PA" Text="Pennsylvania"></asp:ListItem>
                                                        <asp:ListItem Value="DC" Text="District of Columbia"></asp:ListItem>
                                                        <asp:ListItem Value="DE" Text="Delaware"></asp:ListItem>
                                                        <asp:ListItem Value="MD" Text="Maryland"></asp:ListItem> 
                                                        <asp:ListItem Value="GA" Text="Georgia"></asp:ListItem>     
														<asp:ListItem Value="NC" Text="North Carolina"></asp:ListItem>
                                                        </asp:DropDownList></asp:TableCell>
                </asp:TableRow>                
            </asp:Table>
            <br />
            <br />
    
    </asp:Panel>
    <asp:Panel runat="server" ID="view_panels_pnl" Visible="false">
        <div id="content"> 
            <asp:Panel ID="nwPanelSearchPanel" runat="server" DefaultButton="ipSearchBtn">
                    
                <asp:Table ID="nwPanelSearchTbl" runat="server" Width="750"> 
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="3" style="font:12px Calibri, Arial, Helvetica, sans-serif;font-weight:bold;color:#000000;border-bottom:1px solid #8FABD6;">Nationwide Physician Panel Search</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="3">&nbsp;</asp:TableCell>
                    </asp:TableRow>              
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Company Name:&nbsp;</asp:TableCell>
                        <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="nwCompanyNameTxt" runat="server" Width="350"  class="txtbox"></asp:TextBox></asp:TableCell>
                        <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="2"><asp:ImageButton id="srSearchClearBtn" 
                            ImageUrl="images/appBtnClear.jpg" runat="server" Font-Names="Calibri" Font-Size="10px" OnClick="ClearForm" 
                             CssClass="smallappBtnGrd"/>&nbsp;<asp:ImageButton id="ipSearchBtn" ImageUrl="images/appBtnSearch.gif" 
                                 runat="server" Font-Names="Calibri" OnClick="GetPanels" Font-Size="10px" CssClass="smallappBtnGrd"/></asp:TableCell>
                    </asp:TableRow> 
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Right" Wrap="false">State the policy holder is in:&nbsp;</asp:TableCell>
                        <asp:TableCell><asp:DropDownList ID="states_drp2" 
                                                     runat="server"
                                                     AutoPostBack="true"
                                                     OnSelectedIndexChanged="select_state"
                                                     Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" 
                                                     Font-Size="X-Small">
                                                        <asp:ListItem Value="0" Text="--"></asp:ListItem>
                                                        <asp:ListItem Value="PA" Text="Pennsylvania"></asp:ListItem>
                                                        <asp:ListItem Value="DC" Text="District of Columbia"></asp:ListItem>
                                                        <asp:ListItem Value="DE" Text="Delaware"></asp:ListItem>
                                                        <asp:ListItem Value="MD" Text="Maryland"></asp:ListItem>
                                                        <asp:ListItem Value="GA" Text="Georgia"></asp:ListItem>         
														<asp:ListItem Value="NC" Text="North Carolina"></asp:ListItem>														
                                                        </asp:DropDownList></asp:TableCell>
                        </asp:TableRow> 
                    <asp:TableRow></asp:TableRow>                                                                                                              
                </asp:Table>
        
            </asp:Panel>
 
    
            <asp:Panel ID="nwSearchResultsPanel" runat="server" Visible="False">
                <asp:Table ID="nwSearchResultsTblHdr" runat="server" Width="750">
                    <asp:TableRow>
                        <asp:TableCell>&nbsp;</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>&nbsp;</asp:TableCell>
                    </asp:TableRow>                    
                    <asp:TableRow>
                        <asp:TableCell style="font:12px Calibri, Arial, Helvetica, sans-serif;font-weight:bold;color:#000000;border-bottom:1px solid #8FABD6;">Physician Panel Search Results</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>&nbsp;</asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:GridView ID="nwPanelSearchRsltsGrd" Runat="server" 
                    AutoGenerateColumns="False" 
                    AlternatingRowStyle-BackColor="#C3E4ED" 
                    AlternatingRowStyle-Wrap="True" 
                    RowStyle-Wrap="True" 
                    GridLines="None" 
                    Width="750" 
                    Font-Name="Calibri" 
                    Font-Size="12px"
                    color="#ffffff"
                    CssClass="mGrid"
                    AllowPaging="false"        
                    DataKeyNames="FileDir"
                     OnRowCommand="nwPanelSearchRsltsGrd_RowCommand">
                    <HeaderStyle ForeColor="White" Font-Bold="True" BackColor="#000000" HorizontalAlign="Center"></HeaderStyle>
                    <columns>
                        <asp:boundfield HeaderText="Panel Name" datafield="FileName" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Left"/>
                        <asp:boundfield HeaderText="Date Uploaded" datafield="UploadDate" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="150"/>
                        <asp:TemplateField ShowHeader="False" HeaderStyle-Width="50" ControlStyle-Width="50" ItemStyle-Width="50"> 
                            <ItemTemplate>
                                <asp:ImageButton ID="btnUpdate" ImageUrl="~/Images/appBtnView.gif" runat="server" CausesValidation="false" CommandName="view" CommandArgument='<%# Eval("FileDir") %>' CssClass="smallappBtnGrd"/> 
                            </ItemTemplate> 
                        </asp:TemplateField>
                   
                    </columns>
                </asp:GridView>
                <asp:Table ID="nwNoResultsFoundTbl" runat="server" Visible="false" Width="600">
                    <asp:TableRow>       
                            <asp:TableCell Wrap="false" HorizontalAlign="Left" BackColor="LightYellow" ForeColor="Black">No Panels Found</asp:TableCell>                    
                    </asp:TableRow>                                                             
                </asp:Table>           
            </asp:Panel>
        </div>
      </asp:Panel>

    </asp:Panel>
  
  <asp:Panel runat="Server" ID="AnonymousMessagePanel" Visible="false">
        <asp:HyperLink runat="server" ID="lnkLogin" Text="Log In" NavigateUrl="~/Login.aspx"></asp:HyperLink>        
    </asp:Panel>
              
</asp:Content>