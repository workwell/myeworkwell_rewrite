﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Odbc;
using System.Web.Security;
public partial class UserAccountEditor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        eiUserNameInfoTP.HeaderText = "User Name";
        eiPasswordInfoTP.HeaderText = "Password";
        eiPersonalInfoTP.HeaderText = "Personal Information";
        eiContactInfoTP.HeaderText = "Contact Information";

        //this will determine they type of the user and send them where they need to go.
            if (Convert.ToInt32(Session["UserType"]) == 2)
                Response.Redirect("iiRoadMap.aspx");
            else if (Convert.ToInt32(Session["UserType"]) == 3 )
                Response.Redirect("ciStatusReport.aspx");
            else if (Convert.ToInt32(Session["UserType"]) == 4 )
                Response.Redirect("MACInsurance.aspx");
            else if (Convert.ToInt32(Session["UserType"]) == 5 )
                Response.Redirect("AholdIT.aspx");
            else if (Convert.ToInt32(Session["UserType"]) == 6 )
                Response.Redirect("Nationwide.aspx");
            else if (Convert.ToInt32(Session["UserType"]) == 7 )
                Response.Redirect("SynergyComp.aspx");

        if (Page.Request.IsAuthenticated)
        {
            //not written by eb -  only want to get data when the page is visited for the 1st time so postbacks aren't over written by GetData()
            if (!IsPostBack)
            {
                GetPageData(sender, e);
            }
            AuthenticatedMessagePanel.Visible = true;
            AnonymousMessagePanel.Visible = false;
        }
        else
        {
            AuthenticatedMessagePanel.Visible = false;
            AnonymousMessagePanel.Visible = true;
        }
    }
    public void GetPageData(object sender, EventArgs e)
    {
        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        objConnection.Open();

        string strSQL  = "SELECT SUSERNAME, SFIRSTNAME, SLASTNAME, SEMAILADDRESS, SADDRESS, SADDRESS2, SCITY, " 
            + "SSTATE, SZIP, SPHONEAREACODE, SPHONENUMBER, SEXTENSION, SFAXAREACODE, SFAXNUMBER " 
            + "FROM USER_INFO " 
            + "WHERE SUSERNAME = '" + User.Identity.Name.ToString().ToUpper() + "' AND BACTIVE = 1 "; //Note to change this trash when I get to it

        objSqlCmd = new OdbcCommand(strSQL, objConnection);
        OdbcDataReader objSqlDataRdr = objSqlCmd.ExecuteReader();

        if (objSqlDataRdr.Read())
        {
            userNameTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(0));
            firstNameTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(1));
            lastNameTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(2));
            emailTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(3));
            addressTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(4));
            address2Txt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(5));
            cityText.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(6));
            stateTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(7));
            zipTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(8));
            phoneAreaCodeTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(9));
            phoneNumberTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(10));
            extensionTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(11));
            faxAreaCodeTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(12));
            faxNumberTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(13));
        }

        ClearUpdateMessages();

        objSqlCmd = null;
        objSqlDataRdr = null;
        objConnection.Close();
    }
    public void ClearUpdateMessages()
    {
        userNameError.Visible = false;
        userNameErrorAlreadyExists.Visible = false;
        userNameErrorEmptyName.Visible = false;
        passwordError.Visible = false;
        passwordErrorCurrentPswd.Visible = false;
        passwordErrorNotMatching.Visible = false;
        passwordSuccess.Visible = false;
        passwordErrorMustEnterPswd.Visible = false;
        contactInfoError.Visible = false;
        contactInfoSuccess.Visible = false;
        personalInfoError.Visible = false;
        peronsalInfoSuccess.Visible = false;
    }
    public void UpdateTableData(object sender, EventArgs e)
    {
        string strSQL = "";
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        OdbcCommand objSqlCmd = new OdbcCommand();
        objSqlCmd.Connection = objConnection;
        OdbcDataReader objSqlDataRdr = null;
        Page.Validate();

        if (Page.IsValid)
        {

            ImageButton btn = (ImageButton)sender;
            string argument = btn.CommandName;
            switch(argument)
            {
                case "userNameSubmit":
                    ClearUpdateMessages();
                    // see if the user name isn't null; if not null change continue; else throw error that name can't be null
                    if (userNameTxt.Text != null)
                    {
                        // see if the new name is still the same; if not the same update; else do nothing
                        if (userNameTxt.Text != User.Identity.Name.ToString().ToUpper())
                        {
                            //see if name is already taken; if not update; else spit back error

                            objConnection.Open();
                            objSqlCmd = new OdbcCommand(strSQL, objConnection);
                            objSqlCmd.CommandText = "{call sp_updateUserName(?,?)}";
                            objSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
                            objSqlCmd.Parameters.Add(new OdbcParameter("@userName", OdbcType.VarChar, 255)).Value = User.Identity.Name.ToString().ToUpper();
                            objSqlCmd.Parameters.Add(new OdbcParameter("@newUserName", OdbcType.VarChar, 255)).Value = userNameTxt.Text.ToString().ToUpper();
                            objSqlDataRdr = objSqlCmd.ExecuteReader();
                            if (objSqlDataRdr.Read())
                            {
                                if (Convert.ToInt32(objSqlDataRdr.GetValue(0)) == 1) 
                                {
                                    AuthenticatedMessagePanel.Visible = false;
                                    AnonymousMessagePanel.Visible = false;
                                    FormsAuthentication.SignOut();
                                    Session.Abandon();

                                    // clear authentication cookie; won't allow user to navigate site after logout 
                                    HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                                    authCookie.Expires = DateTime.Now.AddYears(-1);
                                    Response.Cookies.Add(authCookie);

                                    // clear session cookie; reccomended for further security; won't allow user to nagivate site after log out
                                    HttpCookie sessionCookie = new HttpCookie("ASP.NET_SessionId", "");
                                    sessionCookie.Expires = DateTime.Now.AddYears(-1);
                                    Response.Cookies.Add(sessionCookie);

                                    objConnection.Close();
                                    
                                    Response.Redirect("~/login.aspx?iS=0");
                                }
                                else
                                {
                                    ClearUpdateMessages();
                                    userNameErrorAlreadyExists.Visible = true;
                                    objConnection.Close();
                                }
                            }
                            else
                            {
                                userNameError.Visible = false;
                                userNameErrorAlreadyExists.Visible = false;
                                objConnection.Close();
                            }
                        }
                    else
                        userNameErrorEmptyName.Visible = true;
                    }
                    break;
                case "passwordSubmit":
                    ClearUpdateMessages();
                    objConnection.Open();

                    if (oldPasswordTxt.Text != null && newPasswordTxt.Text != null && confirmPasswordTxt.Text != null)
                    {
                        if (newPasswordTxt.Text != confirmPasswordTxt.Text)
                        {
                            objSqlCmd = new OdbcCommand(strSQL, objConnection);
                            objSqlCmd.CommandText = "{call sp_updatePassword(?,?,?)}";
                            objSqlCmd.Parameters.Add(new OdbcParameter("@userName", OdbcType.VarChar, 255)).Value = User.Identity.Name.ToString().ToUpper();
                            objSqlCmd.Parameters.Add(new OdbcParameter("@oldPassword", OdbcType.VarChar, 255)).Value = oldPasswordTxt.Text;
                            objSqlCmd.Parameters.Add(new OdbcParameter("@newPassword", OdbcType.VarChar, 255)).Value = newPasswordTxt.Text;
                            objSqlDataRdr = objSqlCmd.ExecuteReader();
                            if (objSqlDataRdr.Read())
                            {
                                if (Convert.ToInt32(objSqlDataRdr.GetValue(0)) != 0)
                                {
                                    passwordSuccess.Visible = true;
                                    /*1/31/17 eb
                                     * adding in force log out for changing password
                                     * */
                                    AuthenticatedMessagePanel.Visible = false;
                                    AnonymousMessagePanel.Visible = false;
                                    FormsAuthentication.SignOut();
                                    Session.Abandon();

                                    // clear authentication cookie; won't allow user to navigate site after logout 
                                    HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                                    authCookie.Expires = DateTime.Now.AddYears(-1);
                                    Response.Cookies.Add(authCookie);

                                    // clear session cookie; reccomended for further security; won't allow user to nagivate site after log out
                                    HttpCookie sessionCookie = new HttpCookie("ASP.NET_SessionId", "");
                                    sessionCookie.Expires = DateTime.Now.AddYears(-1);
                                    Response.Cookies.Add(sessionCookie);

                                    objConnection.Close();

                                    Response.Redirect("~/login.aspx?iS=0");
                                }
                                else
                                    passwordErrorCurrentPswd.Visible = true;
                            }
                        else
                            passwordErrorNotMatching.Visible = true;
                        }
                    }
                    else
                    {
                        if (oldPasswordTxt.Text != null && newPasswordTxt.Text != null && confirmPasswordTxt.Text != null)
                            ClearUpdateMessages();
                        else
                            passwordErrorMustEnterPswd.Visible = true;
                    }
                    

                    objConnection.Close();
                    
                    break;
                case "personalInfoSubmit":

                    objConnection.Open();

                    objSqlCmd = new OdbcCommand(strSQL, objConnection);
                    objSqlCmd.CommandText = "{call sp_checkPersonalInfo(?,?,?,?,?,?,?,?)}";
                    objSqlCmd.Parameters.Add(new OdbcParameter("@firstName", OdbcType.VarChar, 255)).Value = UtilityFunctions.myCStr(firstNameTxt.Text.ToUpper());
                    objSqlCmd.Parameters.Add(new OdbcParameter("@lastName", OdbcType.VarChar, 255)).Value = UtilityFunctions.myCStr(lastNameTxt.Text.ToUpper());
                    objSqlCmd.Parameters.Add(new OdbcParameter("@address", OdbcType.VarChar, 255)).Value = UtilityFunctions.myCStr(addressTxt.Text.ToUpper());
                    objSqlCmd.Parameters.Add(new OdbcParameter("@address2", OdbcType.VarChar, 255)).Value = UtilityFunctions.myCStr(address2Txt.Text.ToUpper());
                    objSqlCmd.Parameters.Add(new OdbcParameter("@city", OdbcType.VarChar, 255)).Value = UtilityFunctions.myCStr(cityText.Text.ToUpper());
                    objSqlCmd.Parameters.Add(new OdbcParameter("@state", OdbcType.Char, 2)).Value = UtilityFunctions.myCStr(stateTxt.Text.ToUpper());
                    objSqlCmd.Parameters.Add(new OdbcParameter("@zip", OdbcType.VarChar, 10)).Value = UtilityFunctions.myCStr(zipTxt.Text);
                    objSqlCmd.Parameters.Add(new OdbcParameter("@userName", OdbcType.VarChar, 255)).Value = User.Identity.Name.ToString().ToUpper();
                    objSqlDataRdr = objSqlCmd.ExecuteReader();

                    if (objSqlDataRdr.Read())
                    {
                        if (Convert.ToInt32(objSqlDataRdr.GetValue(0)) == 1)
                        {
                            objSqlCmd = new OdbcCommand(strSQL, objConnection);
                            objSqlCmd.CommandText = "{call sp_updatePersonalInfo(?,?,?,?,?,?,?,?)}";
                            objSqlCmd.Parameters.Add(new OdbcParameter("@firstName", OdbcType.VarChar, 255)).Value = UtilityFunctions.myCStr(firstNameTxt.Text.ToUpper());
                            objSqlCmd.Parameters.Add(new OdbcParameter("@lastName", OdbcType.VarChar, 255)).Value = UtilityFunctions.myCStr(lastNameTxt.Text.ToUpper());
                            objSqlCmd.Parameters.Add(new OdbcParameter("@address", OdbcType.VarChar, 255)).Value = UtilityFunctions.myCStr(addressTxt.Text.ToUpper());
                            objSqlCmd.Parameters.Add(new OdbcParameter("@address2", OdbcType.VarChar, 255)).Value = UtilityFunctions.myCStr(address2Txt.Text.ToUpper());
                            objSqlCmd.Parameters.Add(new OdbcParameter("@city", OdbcType.VarChar, 255)).Value = UtilityFunctions.myCStr(cityText.Text.ToUpper());
                            objSqlCmd.Parameters.Add(new OdbcParameter("@state", OdbcType.Char, 2)).Value = UtilityFunctions.myCStr(stateTxt.Text.ToUpper());
                            objSqlCmd.Parameters.Add(new OdbcParameter("@zip", OdbcType.VarChar, 10)).Value = UtilityFunctions.myCStr(zipTxt.Text);
                            objSqlCmd.Parameters.Add(new OdbcParameter("@userName", OdbcType.VarChar, 255)).Value = User.Identity.Name.ToString().ToUpper();
                            objSqlCmd.ExecuteNonQuery();

                            ClearUpdateMessages();
                            peronsalInfoSuccess.Visible = true;
                        }
                        else
                            ClearUpdateMessages();
                    }
                    objConnection.Close();
                    break;
                case "contactInfoSubmit":

                    objConnection.Open();

                    objSqlCmd = new OdbcCommand(strSQL, objConnection);
                    objSqlCmd.CommandText = "{call sp_checkContactInfo(?,?,?,?,?,?,?)}";
                    objSqlCmd.Parameters.Add(new OdbcParameter("@emailAddress", OdbcType.VarChar, 255)).Value = UtilityFunctions.myCStr(emailTxt.Text.ToUpper());
                    objSqlCmd.Parameters.Add(new OdbcParameter("@phoneAreaCode", OdbcType.VarChar, 3)).Value = UtilityFunctions.myCStr(phoneAreaCodeTxt.Text);
                    objSqlCmd.Parameters.Add(new OdbcParameter("@phoneNumber", OdbcType.VarChar, 8)).Value = UtilityFunctions.myCStr(phoneNumberTxt.Text);
                    objSqlCmd.Parameters.Add(new OdbcParameter("@extension", OdbcType.VarChar, 10)).Value = UtilityFunctions.myCStr(extensionTxt.Text);
                    objSqlCmd.Parameters.Add(new OdbcParameter("@faxAreaCode", OdbcType.VarChar, 3)).Value = UtilityFunctions.myCStr(faxAreaCodeTxt.Text);
                    objSqlCmd.Parameters.Add(new OdbcParameter("@faxNumber", OdbcType.VarChar, 8)).Value = UtilityFunctions.myCStr(faxNumberTxt.Text);
                    objSqlCmd.Parameters.Add(new OdbcParameter("@userName", OdbcType.VarChar, 255)).Value = User.Identity.Name.ToString().ToUpper();
                    objSqlDataRdr = objSqlCmd.ExecuteReader();

                    if (objSqlDataRdr.Read())
                    {  
                        if (Convert.ToInt32(objSqlDataRdr.GetValue(0)) == 1)
                        {
                            objSqlCmd = new OdbcCommand(strSQL, objConnection);
                            objSqlCmd.CommandText = "{call sp_updateContactInfo(?,?,?,?,?,?,?)}";
                            objSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
                            objSqlCmd.Parameters.Add(new OdbcParameter("@emailAddress", OdbcType.VarChar, 255)).Value = UtilityFunctions.myCStr(emailTxt.Text.ToString().ToUpper());
                            objSqlCmd.Parameters.Add(new OdbcParameter("@phoneAreaCode", OdbcType.VarChar, 3)).Value = UtilityFunctions.myCStr(phoneAreaCodeTxt.Text);
                            objSqlCmd.Parameters.Add(new OdbcParameter("@phoneNumber", OdbcType.VarChar, 8)).Value = UtilityFunctions.myCStr(phoneNumberTxt.Text);
                            objSqlCmd.Parameters.Add(new OdbcParameter("@extension", OdbcType.VarChar, 10)).Value = UtilityFunctions.myCStr(extensionTxt.Text);
                            objSqlCmd.Parameters.Add(new OdbcParameter("@faxAreaCode", OdbcType.VarChar, 3)).Value = UtilityFunctions.myCStr(faxAreaCodeTxt.Text);
                            objSqlCmd.Parameters.Add(new OdbcParameter("@faxNumber", OdbcType.VarChar, 8)).Value = UtilityFunctions.myCStr(faxNumberTxt.Text);
                            objSqlCmd.Parameters.Add(new OdbcParameter("@userName", OdbcType.VarChar, 255)).Value = User.Identity.Name.ToString().ToUpper();

                            objSqlCmd.ExecuteNonQuery();
                            ClearUpdateMessages();
                            contactInfoSuccess.Visible = true;
                        }
                        else
                            ClearUpdateMessages();
                    }

                    objConnection.Close();

                    break;
            }
        }

    }
    //12-1-16 NO idea wtf these was for
    public void UpdateLocationInfo()
    {

    }
    public void CancelUpdateLocationInfo()
    {

    }
}