﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class meww_no_float : System.Web.UI.MasterPage
{


    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                GetUrl(myCStr(Request.UrlReferrer.AbsoluteUri.ToString().Trim()));
            }
            catch
            {
                dashBoardLnk.NavigateUrl = "LandingPage.aspx";
                dashBoardLnk.ImageUrl = "images/tabOnDashboard.jpg";
            }
        }

    }


    public void GetUrl(string returnUrl)
    {

        dashBoardLnk.NavigateUrl = "LandingPage.aspx";
        dashBoardLnk.ImageUrl = "images/tabOnDashboard.jpg";

    }

    public string myCStr(object test)
    {

        string sReturnStr = "";

        if (test == DBNull.Value)
        {
            sReturnStr = "";
        }
        else
        {
            sReturnStr = Convert.ToString(test);
        }

        if (sReturnStr == "#")
        {
            sReturnStr = "";
        }

        return sReturnStr;

    }

}