﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
public partial class NationWideLanding : System.Web.UI.Page
{
    DataTable nwPanelURLs = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.Request.IsAuthenticated)
        {
            // this will determine if the user is an employer or insurance carrier/broker

            if (Session["UserType"] == "1")
                Response.Redirect("LandingPage.aspx");
            else if (Session["UserType"] == "2" )
                Response.Redirect("iiRoadMap.aspx");
            else if (Session["UserType"] == "3")
                Response.Redirect("ciStatusReport.aspx");
            else if (Session["UserType"] == "4")
                Response.Redirect("MACInsurance.aspx");
            else if (Session["UserType"] == "5")
                Response.Redirect("AholdIT.aspx");
            else if (Session["UserType"] == "7")
                Response.Redirect("SynergyComp.aspx");

            AuthenticatedMessagePanel.Visible = true;
            AnonymousMessagePanel.Visible = false;
        }
        else
        {
            AuthenticatedMessagePanel.Visible = false;
            AnonymousMessagePanel.Visible = true;

        }
    }
    public void GetPanels(object sender, System.EventArgs e)
    {

        nwPanelURLs.Columns.Add("FileName");
        nwPanelURLs.Columns.Add("FileDir");
        nwPanelURLs.Columns.Add("UploadDate");

        
        nwSearchResultsPanel.Visible = true;
        nwNoResultsFoundTbl.Visible = false;

        if (which_drp.Value == "0")
            GetFiles("~\\nationwide\\nationwidefiles\\" + states_drp.SelectedValue);
        else
            GetFiles("~\\nationwide\\nationwidefiles\\" + states_drp2.SelectedValue);

        nwPanelSearchRsltsGrd.DataSource = nwPanelURLs;
        nwPanelSearchRsltsGrd.DataBind();

        if (nwPanelURLs.Rows.Count == 0) 
            nwNoResultsFoundTbl.Visible = true;
    }
    public void GetDocuments(string relativePath)
    {
        string physicalPath = Server.MapPath(relativePath);
        DirectoryInfo directory = new DirectoryInfo(physicalPath);

        if (directory.Exists)
        {
            string searchStr = "";

            if (String.IsNullOrEmpty(nwCompanyNameTxt.Text) == false)
                searchStr = "*" + nwCompanyNameTxt.Text + "*.pdf";
            else
                searchStr = searchStr + "*.pdf";

            //Return directory.GetFiles("*- " & nwDistrictsDrp.SelectedItem.Text & "*.pdf") 'searchStr
            foreach (FileInfo myFile in directory.GetFiles(searchStr))
            {
                DataRow dr = nwPanelURLs.NewRow();
                dr["FileName"] = myFile.Name;
                dr["FileDir"] = myFile.FullName.Replace("D:\\Hosting\\6546477\\html", "");
                dr["UploadDate"] = myFile.LastWriteTime.ToShortDateString();
                nwPanelURLs.Rows.Add(dr);
            }
        }
        else
            throw new DirectoryNotFoundException();
    }
    public void FindFiles(string Path)
    {
        string physicalPath= Path;
        DirectoryInfo directories = new DirectoryInfo(physicalPath);

       // Dim myDir As IO.DirectoryInfo;

        //Dim myFile As IO.FileInfo
        foreach( DirectoryInfo myDir in directories.GetDirectories())
        {
            foreach( FileInfo myFile in myDir.GetFiles("*.pdf"))
            {
            }

            if (myDir.GetDirectories().Length > 0)
                FindFiles(myDir.FullName + "\\");
        }
    }
    public void GetFiles(string path)
    {
        string myPath = Server.MapPath(path);
        if (File.Exists(myPath)) 
        {
            //' This path is a file 
            ProcessFile(myPath);
        }
        else if (Directory.Exists(myPath))
        {
            // This path is a directory 
            ProcessDirectory(myPath);
        }
    }
    // Process all files in the directory passed in, recurse on any directories 
    // that are found, and process the files they contain. 
    public void ProcessDirectory(string targetDirectory)
    {
        // Process the list of files found in the directory. 
        String[] fileEntries = Directory.GetFiles(targetDirectory);
        foreach (String fileName in fileEntries)
        {
            ProcessFile(fileName);
        }


        // Recurse into subdirectories of this directory. 
        String[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
        foreach (String subdirectory in subdirectoryEntries)
        {
            ProcessDirectory(subdirectory);
        }
    }
    // Insert logic for processing found files here. 
    public void ProcessFile(string path)
    {
        FileInfo fi = new FileInfo(path);
        string companyNameStr = nwCompanyNameTxt.Text.Trim();
        if (String.IsNullOrEmpty(nwCompanyNameTxt.Text) == false)
        {
            if (fi.Name.ToLower().Contains(companyNameStr.ToLower()) && fi.FullName.ToLower().Contains(".pdf"))
            {
                if (!fi.Name.ToLower().Contains("%27"))
                {
                    DataRow dr = nwPanelURLs.NewRow();
                    dr["FileName"] = fi.Name;
                    dr["FileDir"] = fi.FullName.Replace("D:\\Hosting\\6546477\\html", "").Trim();
                    dr["UploadDate"] = fi.LastWriteTime.ToShortDateString();
                    nwPanelURLs.Rows.Add(dr);
                }
            }
        }
        else
        {
            if (!fi.Name.ToLower().Contains("%27"))
            {
                if (fi.FullName.ToLower().Contains(".pdf"))
                {
                    DataRow dr = nwPanelURLs.NewRow();
                    dr["FileName"] = fi.Name;
                    dr["FileDir"] = fi.FullName.Replace("D:\\Hosting\\6546477\\html", "").Trim();
                    dr["UploadDate"] = fi.LastWriteTime.ToShortDateString();
                    nwPanelURLs.Rows.Add(dr);
                }
            }
        }
    }
    public void NewSearch()
    {
        nwSearchResultsPanel.Visible = false;
        nwPanelSearchPanel.Visible = true;
    }
    public void ClearForm(object sender, System.EventArgs e)
    {
        nwCompanyNameTxt.Text = "";
        GetPanels(sender, e);
    }
    protected void nwPanelSearchRsltsGrd_RowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "view")
        {
            string file_  = e.CommandArgument.ToString();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + file_ + "");
            Response.TransmitFile(file_);
            Response.End();
        }
    }
    public void nwPanelSearchRsltsGrd_RowDataBound(Object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
    }
    public void select_state(Object sender, System.EventArgs e)
    {
        switch(Convert.ToString(states_drp.SelectedValue))
        {
            case "PA":
                //Session("state") = "PA"
                GetPanels(sender, e);
                state_selection_pnl.Visible = false;
                view_panels_pnl.Visible = true;
                break;
            case "DC":
                //Session("state") = "DC"
                GetPanels(sender, e);
                state_selection_pnl.Visible = false;
                view_panels_pnl.Visible = true;
                break;
            case "MD":
                //Session("state") = "MD"
                GetPanels(sender, e);
                state_selection_pnl.Visible = false;
                view_panels_pnl.Visible = true;
                break;
            case "DE":
                //Session("state") = "DE"
                GetPanels(sender, e);
                state_selection_pnl.Visible = false;
                view_panels_pnl.Visible = true;
                break;
            case "VA":
                //Session("state") = "VA"
                GetPanels(sender, e);
                state_selection_pnl.Visible = false;
                view_panels_pnl.Visible = true;
                break;
            case "GA":
                //Session("state") = "VA"
                GetPanels(sender, e);
                state_selection_pnl.Visible = false;
                view_panels_pnl.Visible = true;
                break;
			case "NC":
                GetPanels(sender, e);
				state_selection_pnl.Visible = false;
				view_panels_pnl.Visible = true;
				break;
            case "0":
                //Session("state") = "0";
                state_selection_pnl.Visible = true;
                view_panels_pnl.Visible = false;
                break;
        }

        if (which_drp.Value == "0")
        {
            which_drp.Value = "1";
            states_drp2.SelectedValue = states_drp.SelectedValue;
        }

    }
}