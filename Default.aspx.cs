﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.Request.IsAuthenticated)
        {
            //this will determine they type of the user and send them where they need to go.
            if (Convert.ToInt32(Session["UserType"]) == 1 || Convert.ToInt32(Session["UserType"]) == 10)
                Response.Redirect("LandingPage.aspx");
            else if (Convert.ToInt32(Session["UserType"]) == 2)
                Response.Redirect("iiRoadMap.aspx");
            else if (Convert.ToInt32(Session["UserType"]) == 3 )
                Response.Redirect("ciStatusReport.aspx");
            else if (Convert.ToInt32(Session["UserType"]) == 4 )
                Response.Redirect("MACInsurance.aspx");
            else if (Convert.ToInt32(Session["UserType"]) == 5 )
                Response.Redirect("AholdIT.aspx");
            else if (Convert.ToInt32(Session["UserType"]) == 6 )
                Response.Redirect("Nationwide.aspx");
            else if (Convert.ToInt32(Session["UserType"]) == 8)
                Response.Redirect("NationWideAggri.aspx");
            else if (Convert.ToInt32(Session["UserType"]) == -96 )
                Response.Redirect("dv_ciStatusReport.aspx");
            else if (Convert.ToInt32(Session["UserType"]) == -97 )
                Response.Redirect("LandingPage.aspx");
            else if (Convert.ToInt32(Session["UserType"]) == -99)
                Response.Redirect("meww_createClient.aspx");
        }
        else
            AnonymousMessagePanel.Visible = true;

    }
}