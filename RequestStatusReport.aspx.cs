﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.Odbc;
using System.Text;
using System.Net.Mail;
public partial class RequestStatusReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.Request.IsAuthenticated)
        {
            AuthenticatedMessagePanel.Visible = true;
            AnonymousMessagePanel.Visible = false;

            if (!IsPostBack)
            {
                GetData();

                if (Session["eiRequestSR0"] != null)
                {
                    bool[] prevValues = (bool[])Session["eiRequestSR0"];

                    for (int x = 0; x < employeeSearchResultsGrd.Rows.Count - 1; x++)
                    {
                        if (prevValues[x])
                        {
                            ((CheckBox)employeeSearchResultsGrd.Rows[x].FindControl("selectionBox")).Checked = true;
                        }
                    }
                }
            }
        }
        else
        {
            AuthenticatedMessagePanel.Visible = false;
            AnonymousMessagePanel.Visible = true;
        }
    }
    public void GetData()
    {
        int iPartyID = Convert.ToInt32(Session["PartyID"]);
        string dDOIEndRangeDate = UtilityFunctions.ReverseDate(DateTime.Now.AddYears(1).ToShortDateString());
        string dDOEEndRangeDate = UtilityFunctions.ReverseDate(DateTime.Now.AddYears(1).ToShortDateString());
        string sBuiltQuery = "";
        string sSocialSecNum = "";
        bool bSSEntered = true;

        srNoEditSearchResultsTbl.Visible = false;

        if (searchFirstNameTxt.Text.Trim() != null)
            sBuiltQuery = " AND HP_IP.PERSON_FIRST_NAME LIKE '%" + searchFirstNameTxt.Text.ToUpper().Trim() + "%' ";
        if (searchLastNameTxt.Text.Trim() != null)
            sBuiltQuery = sBuiltQuery + " AND HP_IP.PERSON_LAST_NAME LIKE '%" + searchLastNameTxt.Text.ToUpper().Trim() + "%' ";
        //For the date verification
        DateTime temp;
        if (searchDOITxt.Text.Trim() != null && DateTime.TryParse(searchDOITxt.Text, out temp))
            sBuiltQuery = sBuiltQuery + " AND HCA_IP.ATTRIBUTE8 = '" + UtilityFunctions.ReverseDate(searchDOITxt.Text) + " 00:00:00' ";

        if (string.IsNullOrEmpty(searchSS1Txt.Text) != true || string.IsNullOrEmpty(searchSS2Txt.Text) != true || string.IsNullOrEmpty(searchSS3Txt.Text) != true)
        {
            if (string.IsNullOrEmpty(searchSS1Txt.Text) != true)
                sSocialSecNum = searchSS1Txt.Text.Trim() + "-";
            else
            {
                sSocialSecNum = "%-";
                bSSEntered = false;
            }

            if (string.IsNullOrEmpty(searchSS2Txt.Text) != true)
                sSocialSecNum = sSocialSecNum + searchSS2Txt.Text.Trim() + "-";
            else
            {
                sSocialSecNum = sSocialSecNum + "%-";
                bSSEntered = false;
            }

            if (string.IsNullOrEmpty(searchSS3Txt.Text) != true)
                sSocialSecNum = sSocialSecNum + searchSS3Txt.Text.Trim();

            else
            {
                sSocialSecNum = sSocialSecNum + "%";
                bSSEntered = false;
            }

            if (bSSEntered)
                sBuiltQuery = sBuiltQuery + " AND HP_IP.JGZZ_FISCAL_CODE = '" + sSocialSecNum + "' ";
            else
                sBuiltQuery = sBuiltQuery + " AND HP_IP.JGZZ_FISCAL_CODE LIKE '" + sSocialSecNum + "' ";
        }

        DataTable dt = new DataTable();
        string sSqlQuery  = "";

        if ((Session["InsCarrier"].ToString() == "N" || Session["InsCarrier"].ToString() == "Y") && Convert.ToInt32(Session["PartyID"]) != Convert.ToInt32(Session["InsPartyID"]))
        {

            sSqlQuery = "SELECT HP_IP.PARTY_NAME, REPLACE(HCA_IP.ATTRIBUTE8,' 00:00:00','') ATTRIBUTE8, REPLACE(HCA_IP.ATTRIBUTE3,' 00:00:00','') ATTRIBUTE3, HCA_IP.ATTRIBUTE2, "
                + " HCA_IP.ATTRIBUTE5, HCA_IP.CUST_ACCOUNT_ID, HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME, HP_IP.PERSON_LAST_NAME "
                + "FROM mewwoh.HZ_CUST_ACCOUNTS HCA_IP "
                        + "INNER JOIN mewwoh.HZ_CUST_ACCT_RELATE_ALL HCARA_INS ON HCARA_INS.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID "
                        + "INNER JOIN mewwoh.HZ_CUST_ACCOUNTS HCA_INS ON HCARA_INS.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID "
                        + "INNER JOIN mewwoh.HZ_CUST_ACCT_RELATE_ALL HCARA_EMP ON HCARA_EMP.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID "
                        + "INNER JOIN mewwoh.HZ_CUST_ACCOUNTS HCA_EMP ON HCA_EMP.CUST_ACCOUNT_ID = HCARA_EMP.RELATED_CUST_ACCOUNT_ID "
                        + "INNER JOIN mewwoh.HZ_PARTIES HP_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID "
                + "WHERE HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT' AND " 
                        + "HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' AND "
                        + "HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' AND " 
                        + "HCA_EMP.PARTY_ID = " + iPartyID + " AND " 
                        + "HCARA_INS.STATUS = 'A' AND " 
                        + "HCARA_EMP.STATUS = 'A'  AND " 
                        + "HCA_IP.CUST_ACCOUNT_ID > 0  AND " 
                        + "HCA_IP.ATTRIBUTE9 IS NOT NULL " + sBuiltQuery 
                + "ORDER BY 2 DESC";
        }
        else
        {
            sSqlQuery = "SELECT HP_IP.PARTY_NAME, REPLACE(HCA_IP.ATTRIBUTE8,' 00:00:00','') ATTRIBUTE8, REPLACE(HCA_IP.ATTRIBUTE3,' 00:00:00','') ATTRIBUTE3, HCA_IP.ATTRIBUTE2, "
                + " HCA_IP.ATTRIBUTE5, HCA_IP.CUST_ACCOUNT_ID, HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME, HP_IP.PERSON_LAST_NAME " 
                + "FROM HZ_CUST_ACCOUNTS HCA_IP  " 
                + "        INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_INS ON HCARA_INS.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID  "
                + "        INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA_INS.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID  "
                + "        INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_EMP ON HCARA_EMP.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID  "
                + "        INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCA_EMP.CUST_ACCOUNT_ID = HCARA_EMP.RELATED_CUST_ACCOUNT_ID  "
                + "        INNER JOIN HZ_PARTIES HP_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID  "
                + "        INNER JOIN (SELECT HP.PARTY_ID "
                + "			FROM HZ_CUST_ACCOUNTS HCA_INS INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_INS.CUST_ACCOUNT_ID = HCARA.RELATED_CUST_ACCOUNT_ID "
                + "				 INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID "
                + "				 INNER JOIN HZ_PARTIES HP ON HCA_EMP.PARTY_ID = HP.PARTY_ID "
                + "			WHERE HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' "
                + "				  AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                + "				  AND HCARA.STATUS = 'A' "
                + "				  AND HCA_INS.PARTY_ID = " + Convert.ToString(Session["InsPartyID"]) + " "
                + "				  AND HCA_EMP.STATUS = 'A') REL_ACCTS ON HCA_EMP.PARTY_ID = REL_ACCTS.PARTY_ID "
                + "WHERE HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT' AND  "
                + "        HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' AND  "
                + "        HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' AND " 
                + "	HCARA_INS.STATUS = 'A' AND  "
                + "        HCARA_EMP.STATUS = 'A' AND "
                + "         HCA_IP.CUST_ACCOUNT_ID > 0  AND" 
                + "        HCA_IP.ATTRIBUTE9 IS NOT NULL  " + sBuiltQuery 
                + "GROUP BY HP_IP.PARTY_NAME, HCA_IP.ATTRIBUTE8, HCA_IP.ATTRIBUTE3, HCA_IP.ATTRIBUTE2, HCA_IP.ATTRIBUTE5, HCA_IP.CUST_ACCOUNT_ID, HP_IP.PERSON_FIRST_NAME, "
                + " HP_IP.PERSON_MIDDLE_NAME, HP_IP.PERSON_LAST_NAME  "
                + "ORDER BY 2 DESC ";
        }

        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        OdbcDataAdapter ObjDataAdapter = new OdbcDataAdapter(sSqlQuery, objConnection);

        try
        {
            objConnection.Open();
            ObjDataAdapter.Fill(dt);

            employeeSearchResultsGrd.DataSource = dt;
            employeeSearchResultsGrd.DataBind();

            if (dt.Rows.Count > 0)
            {
                employeeSearchResultsGrd.Visible = true;
                submitSelectionsButtonsTbl.Visible = true;
            }
            else
            {
                employeeSearchResultsGrd.Visible = false;
                submitSelectionsButtonsTbl.Visible = false;
                srNoEditSearchResultsTbl.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            objConnection.Close();
            ObjDataAdapter.Dispose();
            objConnection.Dispose();
        }
    }
    public void CancelRequest(object sender, EventArgs e)
    { 
        bool[] values = new bool[requestCnfrmGrd.PageSize - 1];

        for (int i = 0;  i <= employeeSearchResultsGrd.PageCount - 1; i++)
        {

            //********
            // this is to clear all selected locations session vars, i can't remove the session vars wirh remove (above), so I'm just seting everything to false for now.
            for (int x = 0; x <= employeeSearchResultsGrd.Rows.Count - 1; i++)
            {
                values[x] = false;
            }
            Session["eiRequestSR" + i] = values;
        }
        ClearSearchFields();

        employeeSearchResultsGrd.PageIndex = 0;
        employeeSearchResultsGrd.DataBind();

        employeeSearchPanel.Visible = true;
        employeeSearchResultsPanel.Visible = true;
        eiConfirmPanel.Visible = false;
        eiRqstSRHdrPanel.Visible = true;
        confirmListingPanel.Visible = false;
    }
    public void SearchEmployees(object sender, EventArgs e)
    {
        GetData();
    }
    
    public void ClearSearchFields()
    {
        searchFirstNameTxt.Text = "";
        searchLastNameTxt.Text = "";
        searchDOITxt.Text = "";
        searchSS1Txt.Text = "";
        searchSS2Txt.Text = "";
        searchSS3Txt.Text = "";
        GetData();
    }
    public void SubmitRequest(object sender, EventArgs e)
    {
        string sPatientsInfo = "";
        bool bOneSelected = false;
        bool[] values = new bool[requestCnfrmGrd.PageSize - 1];

        noneSelectedErrorTbl.Visible = false;
        OdbcCommand objSqlCmd = new OdbcCommand();
        OdbcDataReader objSqlDataRdr = null;
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);

        for ( int i=0; i <= requestCnfrmGrd.Rows.Count - 1; i++)
        {
            
            GridViewRow row = requestCnfrmGrd.Rows[i];
            int iCustAcctID = Convert.ToInt32(requestCnfrmGrd.DataKeys[i].Value);
            bool isChecked = ((CheckBox)row.FindControl("selectionBox")).Checked;

            if (isChecked)
            {
                bOneSelected = true;

                string sSqlQuery = "SELECT HP_IP.PARTY_NAME, REPLACE(HCA_IP.ATTRIBUTE8,' 00:00:00','') ATTRIBUTE8, REPLACE(HCA_IP.ATTRIBUTE3,' 00:00:00','') ATTRIBUTE3, HCA_IP.ATTRIBUTE2, "
                    + " HCA_IP.ATTRIBUTE5, HCA_IP.CUST_ACCOUNT_ID, HCA_EMP.ACCOUNT_NAME " 
                    + "FROM HZ_CUST_ACCOUNTS HCA_IP " 
                        + "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_INS ON HCARA_INS.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID " 
                        + "INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA_INS.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID " 
                        + "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_EMP ON HCARA_EMP.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID " 
                        + "INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCA_EMP.CUST_ACCOUNT_ID = HCARA_EMP.RELATED_CUST_ACCOUNT_ID " 
                        + "INNER JOIN HZ_PARTIES HP_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID "
                    + "WHERE HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT' AND " 
                            + "HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' AND " 
                            + "HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' AND "
                            + "HCA_IP.CUST_ACCOUNT_ID = " + iCustAcctID + " AND " 
                            + "HCARA_INS.STATUS = 'A' AND " 
                            + "HCARA_EMP.STATUS = 'A' AND "
                            + "HCA_IP.ATTRIBUTE9 IS NOT NULL "
                    + "ORDER BY 2 ";

                

                try
                {
                    objConnection.Open();

                    objSqlCmd = new OdbcCommand("", objConnection);
                    objSqlCmd.CommandText = sSqlQuery;
                    objSqlCmd.CommandType = System.Data.CommandType.Text;
                    objSqlDataRdr = objSqlCmd.ExecuteReader();

                    if (objSqlDataRdr.Read())
                    {
                        sPatientsInfo = sPatientsInfo + "Name: " + objSqlDataRdr.GetValue(0) + "<BR> Employer: " + objSqlDataRdr.GetValue(6) + "<BR> Date of Injury: " + Convert.ToDateTime(objSqlDataRdr.GetValue(1)) 
                            + "<BR> Date Reprted: " + Convert.ToDateTime(objSqlDataRdr.GetValue(2)) + "<BR><BR>";
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    objConnection.Close();
                    objSqlCmd.Dispose();
                    objConnection.Dispose();
                }
            }
        }
        string req_email = "";
        string req_person = "";
        StringBuilder body = new StringBuilder();
        System.Net.Mail.MailMessage message  = new System.Net.Mail.MailMessage();
        message.IsBodyHtml = true;
        message.Subject = "Status Report Request - myeWorkWell.com";
        message.From = new MailAddress("info@myeworkwell.com", "myeWorkwell Info");
        message.To.Add(new MailAddress("consulting@eworkwell.com", "Consulting Case Managers"));
        message.To.Add(new MailAddress("notifications@eworkwell.com", "WorkWell Notifications"));


        string sql = "SELECT SEMAILADDRESS, SFIRSTNAME, SLASTNAME FROM USER_INFO WHERE SUSERNAME = '" + Convert.ToString(Session["UserName"]) + "'";
        OdbcCommand cmd = null;

        //try
        //{'\
        strConnection = ConnectionStrings.MySqlConnections();
        objConnection = new OdbcConnection(strConnection);

            objConnection.Open();
            cmd = new OdbcCommand("", objConnection);
            cmd.CommandText = sql;
            cmd.CommandType = System.Data.CommandType.Text;
            OdbcDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read())
            {
                req_email = UtilityFunctions.myCStr(rdr.GetValue(0));
                req_person = UtilityFunctions.myCStr(rdr.GetValue(1)) + " " + UtilityFunctions.myCStr(rdr.GetValue(2));
            }
            rdr.Close();
            cmd.Dispose();
            objConnection.Dispose();

        body.Append("Requested by: " + req_person + "<br/>");
        body.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + req_email + "<br/><br/>");
        body.Append("Please enter the patient's status report into the myeWorkwell.com database." + "<br/><br/>");
        body.Append(sPatientsInfo);
        body.Append("<hr />");
        body.Append("<font size='2' color='#cccccc'>");
        body.Append("<hr />");
        body.Append("www.eworkwell.com");
        body.Append("<br>");
        body.Append(DateTime.Now.AddHours(3));
        body.Append("</font>");

        if (bOneSelected)
        {
            message.Body = body.ToString();
            //relay-hosting.secureserver.net is the valid SMTP server of godaddy.com
            SmtpClient mailClient = new SmtpClient("dedrelay.secureserver.net");

            mailClient.Send(message);
            message.Dispose();

            for (int i = 0; i < requestCnfrmGrd.PageCount - 1; i++)
            {
                //********
                // this is to clear all selected locations session vars, i can't remove the session vars wirh remove (above), so I'm just seting everything to false for now.
                for (int x = 0; x <= requestCnfrmGrd.Rows.Count - 1; x++)
                {
                    values[x] = false;
                }
                //First Proitity remove for the second rewrite phase 12/05/16 eb
                Session["eiRequestSR" + i] = values;
            } 

            eiConfirmPanel.Visible = false;
            eiRqstSRHdrPanel.Visible = false;
            requestSumbitPanel.Visible = true;
            confirmListingPanel.Visible = false;
        }
        else
            noneSelectedErrorTbl.Visible = true;
    }
    protected void employeeSearchResultsGrd_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        int d = employeeSearchResultsGrd.PageCount;
        bool[] values = new bool[employeeSearchResultsGrd.PageSize - 1];
        CheckBox chb;
        int isChecked  = 0;

        for (int i = 0; i < employeeSearchResultsGrd.Rows.Count - 1; i++)
        {
            chb = (CheckBox)employeeSearchResultsGrd.Rows[i].FindControl("selectionBox");
            if (chb != null)
            {
                values[i] = chb.Checked;
                isChecked = 1;
            }
        }

        if (isChecked == 1)
            Session["eiRequestSR" + employeeSearchResultsGrd.PageIndex] = values;
        
        GetData();

        employeeSearchResultsGrd.PageIndex = e.NewPageIndex;
        employeeSearchResultsGrd.DataBind();
        
        if (Session["eiRequestSR" + e.NewPageIndex] != null)
        {
            //Get this converted by turning it into viewstates ...........
            bool[] prevValues = (bool[])Session["eiRequestSR" + e.NewPageIndex];

            for (int x = 0; x <= employeeSearchResultsGrd.Rows.Count - 1; x++)
            {
                if (prevValues[x])
                    ((CheckBox)employeeSearchResultsGrd.Rows[x].FindControl("selectionBox")).Checked = true;
            }
        }
    }
    public void ShowSelected(object sender, EventArgs e)
    {
        OdbcCommand objSqlCmd = new OdbcCommand();
        OdbcDataReader objSqlDataRdr = null;
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        int currentGrdPage = employeeSearchResultsGrd.PageIndex;
        DataTable selectedLocs = new DataTable();
        bool[] values = new bool[requestCnfrmGrd.PageSize - 1];
        CheckBox checkBx;
        int curPageOneChecked = 0;
        int bOneChecked = 0;

        selectedLocs.Columns.Add("CUST_ACCOUNT_ID");
        selectedLocs.Columns.Add("PARTY_NAME");
        selectedLocs.Columns.Add("ATTRIBUTE8");
        selectedLocs.Columns.Add("ATTRIBUTE3");
        selectedLocs.Columns.Add("ATTRIBUTE2");

        noneSelectedErrorPanel.Visible = false;

        //get the selections current page that user is on
        for ( int i = 0; i < employeeSearchResultsGrd.Rows.Count - 1; i++)
        {
            bool checker = ((CheckBox)(employeeSearchResultsGrd.Rows[i].FindControl("selectionBox"))).Checked;
            checkBx = (CheckBox)(employeeSearchResultsGrd.Rows[i].FindControl("selectionBox"));
            if (checkBx != null)
            {
                values[i] = checker;
                if (values[i])
                    curPageOneChecked = 1;
            }
        }

        if (curPageOneChecked == 1)
            Session["eiRequestSR" + employeeSearchResultsGrd.PageIndex] = values;

        for (int i = 0; i <= employeeSearchResultsGrd.PageCount - 1; i++)
        {
            if (Session["eiRequestSR" + i] != null)
            {
                //set panelSearchRsltsGrd to page that the checked locations were

                GetData();
                employeeSearchResultsGrd.PageIndex = i;
                employeeSearchResultsGrd.DataBind();

                bool[] prevValues = (bool[])Session["eiRequestSR" + i];

                for (int x = 0; x < employeeSearchResultsGrd.Rows.Count - 1; x++)
                {
                    bool why = prevValues[x];  
                    if (why == true)
                        bOneChecked = 1;
                }
            }
        }

        if (bOneChecked == 1)
        {
            try
            {
                //store the selected locations on current page
                //Session("page" & viewActiveLocationsGrd.PageIndex) = values

                objConnection.Open();
                objSqlCmd.Connection = objConnection;

                //now get all the other pages that user looked and selected

                for (int i = 0; i <= employeeSearchResultsGrd.PageCount - 1; i++)
                {
                    if (Session["eiRequestSR" + i] != null)
                    {
                        //set viewActiveLocationsGrd to page that the checked locations were
                        GetData();
                        employeeSearchResultsGrd.PageIndex = i;
                        employeeSearchResultsGrd.DataBind();
                        bool[] prevValues = (bool[])(Session["eiRequestSR" + i]);
                        //Response.Write("page" & viewActiveLocationsGrd.PageIndex & "<BR>")
                        for (int x = 0; x < employeeSearchResultsGrd.Rows.Count - 1; x++)
                        {
                            //Response.Write("values(" & x & ") = " & values(x))
                            if (prevValues[x])
                            {
                                string iCustAcctID = employeeSearchResultsGrd.DataKeys[x].Value.ToString();
                                //Response.Write("Page: " & i & " Row: " & x & " is checked.  The CUST_ACCOUNT_ID = " & iCustAcctID & "<BR>")

                                try
                                {
                                    objSqlCmd.CommandText = "SELECT HP_IP.PARTY_NAME, REPLACE(HCA_IP.ATTRIBUTE8,' 00:00:00','') ATTRIBUTE8, REPLACE(HCA_IP.ATTRIBUTE3,' 00:00:00','') ATTRIBUTE3, "
                                        + "HCA_IP.ATTRIBUTE2, HCA_IP.ATTRIBUTE5, HCA_IP.CUST_ACCOUNT_ID "
                                        + "FROM HZ_CUST_ACCOUNTS HCA_IP " 
                                                + "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_INS ON HCARA_INS.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID " 
                                                + "INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA_INS.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID " 
                                                + "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_EMP ON HCARA_EMP.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID " 
                                                + "INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCA_EMP.CUST_ACCOUNT_ID = HCARA_EMP.RELATED_CUST_ACCOUNT_ID " 
                                                + "INNER JOIN HZ_PARTIES HP_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID " 
                                        + "WHERE HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT' AND " 
                                                + "HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' AND " 
                                                + "HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' AND " 
                                                + "HCA_IP.CUST_ACCOUNT_ID = " + iCustAcctID + " AND " 
                                                + "HCARA_INS.STATUS = 'A' AND " 
                                                + "HCARA_EMP.STATUS = 'A' AND " 
                                                + "HCA_IP.ATTRIBUTE9 IS NOT NULL " 
                                        + "ORDER BY 2 ";
                                    objSqlCmd.CommandType = System.Data.CommandType.Text;
                                    objSqlDataRdr = objSqlCmd.ExecuteReader();

                                    if (objSqlDataRdr.Read())
                                    {
                                        DataRow dr = selectedLocs.NewRow();
                                        dr["PARTY_NAME"] = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(0));
                                        dr["ATTRIBUTE8"] = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(1));
                                        dr["ATTRIBUTE3"] = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(2));
                                        dr["ATTRIBUTE2"] = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(3));
                                        dr["CUST_ACCOUNT_ID"] = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(5));

                                        selectedLocs.Rows.Add(dr);
                                    }
                                    objSqlDataRdr.Close();
                                    objSqlDataRdr.Dispose();
                                }
                                catch (Exception ex)
                                {
                                    Response.Write("Can't load Web page " + ex.Message);
                                }


                            }

                        }
                    }
                }

                requestCnfrmGrd.DataSource = selectedLocs;
                requestCnfrmGrd.DataBind();
            }
            catch (Exception ex)
            {
                Response.Write("Can't load Web page " + ex.Message);
            }
            finally
            {
                objConnection.Close();
                objConnection.Dispose();
            }

            //now get back to the current page that they were on before submiting the selections
            GetData();
            employeeSearchResultsGrd.PageIndex = currentGrdPage;
            employeeSearchResultsGrd.DataBind();

            //keep to current page's selections checked if they click back

            bool[] existingValues  = (bool[])(Session["eiRequestSR" + employeeSearchResultsGrd.PageIndex]);
            for ( int x = 0; x < employeeSearchResultsGrd.Rows.Count - 1; x++)
            {
                if (existingValues[x])
                ((CheckBox)(employeeSearchResultsGrd.Rows[x].FindControl("selectionBox"))).Checked = true;
            }

            employeeSearchPanel.Visible = false;
            employeeSearchResultsPanel.Visible = false;
            eiConfirmPanel.Visible = true;
            eiRqstSRHdrPanel.Visible = false;
            confirmListingPanel.Visible = true;
        }
        else
        {
            //show error
            noneSelectedErrorPanel.Visible = true;
            GetData();

            employeeSearchResultsGrd.PageIndex = currentGrdPage;
            employeeSearchResultsGrd.DataBind();
        }
    }
    protected void employeeSearchResultsGrd_RowDataBound(Object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string dDateOfInjury  = DataBinder.Eval(e.Row.DataItem, "ATTRIBUTE8").ToString();
            string dDateReported = UtilityFunctions.myCStr(DataBinder.Eval(e.Row.DataItem, "ATTRIBUTE3"));
            string sFName = UtilityFunctions.myCStr(DataBinder.Eval(e.Row.DataItem, "PERSON_FIRST_NAME")).Trim();
            string sLName = UtilityFunctions.myCStr(DataBinder.Eval(e.Row.DataItem, "PERSON_LAST_NAME")).Trim();
            string sMName = UtilityFunctions.myCStr(DataBinder.Eval(e.Row.DataItem, "PERSON_MIDDLE_NAME")).Trim();

            DataRow dr = (DataRow)(e.Row.DataItem);

            if (sMName != null)
                if (sMName.Length == 1)
                    sMName = sMName + ".";


            e.Row.Cells[1].Text = sLName + ", " + sFName + " " + sMName;
            e.Row.Cells[2].Text = UtilityFunctions.ConvertDate(Convert.ToDateTime(dDateOfInjury));
            e.Row.Cells[3].Text = UtilityFunctions.ConvertDate(Convert.ToDateTime(dDateReported));

        }
    }
}