﻿
Partial Class Logout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.QueryString("iS") = 1 Then
            Response.Redirect("~/default.aspx")
        Else
            FormsAuthentication.SignOut()
            Session.Abandon()

            ' clear authentication cookie; won't allow user to navigate site after logout 
            Dim authCookie As New HttpCookie(FormsAuthentication.FormsCookieName, "")
            authCookie.Expires = DateTime.Now.AddYears(-1)
            Response.Cookies.Add(authCookie)

            ' clear session cookie; reccomended for further security; won't allow user to nagivate site after log out
            Dim sessionCookie As New HttpCookie("ASP.NET_SessionId", "")
            sessionCookie.Expires = DateTime.Now.AddYears(-1)
            Response.Cookies.Add(sessionCookie)

            Session.Abandon()
            Session.RemoveAll()

            FormsAuthentication.RedirectToLoginPage()
        End If

    End Sub

End Class
