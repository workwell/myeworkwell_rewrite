﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Odbc;
using System.IO;
using System.Net;
using System.Net.Mail;

public partial class RequestStatusReportChange : System.Web.UI.Page
{
    DataTable panelURLs = new DataTable();
    DataTable selectedPanels = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Page.Request.IsAuthenticated)
        {
            if (!Page.IsPostBack)
            {
                GetPanels(sender, e);
            }
            AuthenticatedMessagePanel.Visible = true;
            AnonymousMessagePanel.Visible = false;
            
        }
        else
        {
            AuthenticatedMessagePanel.Visible = false;
            AnonymousMessagePanel.Visible = true;
        }
    }
    public void GetPanels(object sender, EventArgs e)
    {
        panelURLs = new DataTable();
        panelURLs.Columns.Add("FileName");
        panelURLs.Columns.Add("FileDir");
        panelURLs.Columns.Add("UploadDate");

        int iPartyID = Convert.ToInt32(Session["PartyID"]);


        searchResultsPanel.Visible = true;
        noResultsFoundTbl.Visible = false;

        string DocumentVirtualPath = "/PANELS/" + iPartyID.ToString() + "/";

        GetFiles("/PANELS/" + iPartyID.ToString() + "/");

        panelSearchRsltsGrd.DataSource = panelURLs;
        panelSearchRsltsGrd.DataBind();

        if (panelURLs.Rows.Count == 0)
            noResultsFoundTbl.Visible = true;
    }
    public void GetFiles(string path)
    {
        string myPath = Server.MapPath("~/" + path);
        if (File.Exists(myPath))
            ProcessFile(myPath);
        else if (Directory.Exists(myPath))
            ProcessDirectory(myPath);
    }
    public void ProcessFile(string path)
    {
        FileInfo fi = new FileInfo(path);
        string companyNameStr = companyNameTxt.Text.Trim();

        if (string.IsNullOrEmpty(companyNameTxt.Text) == false)
        {
            if (fi.Name.ToLower().Contains(companyNameStr) && fi.FullName.ToLower().Contains(".pdf"))
            {
                DataRow dr = panelURLs.NewRow();

                dr["FileName"] = fi.Name;
                dr["FileDir"] = fi.FullName.Replace("D:/Hosting/6546477/html", "");
                dr["UploadDate"] = fi.LastWriteTime.ToShortDateString();
                panelURLs.Rows.Add(dr);
            }
        }
        else
        {
            if (fi.FullName.ToLower().Contains(".pdf"))
            {
                DataRow dr = panelURLs.NewRow();
                dr["FileName"] = fi.Name;
                dr["FileDir"] = fi.FullName.Replace("D:/Hosting/6546477/html", "");
                dr["UploadDate"] = fi.LastWriteTime.ToShortDateString();
                panelURLs.Rows.Add(dr);
            }
        }
    }
    public void ProcessDirectory(string targetDirectory)
    {
        string[] fileEntries = Directory.GetFiles(targetDirectory);
        string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);

        foreach (string fileName in fileEntries)
            ProcessFile(fileName);
        foreach (string subdirectory in subdirectoryEntries)
            ProcessDirectory(subdirectory);
    }
    public void SubmitRequest(object sender, EventArgs e)
    {
        String body = string.Empty;
        System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
        string sPanelChanges = "";
        int iPartyID = Convert.ToInt32(Session["PartyID"]);
        Boolean bChangeDescError = false;
        Boolean[] values = new Boolean[panelSearchRsltsGrd.PageSize - 1];
        string from_email_addy = "";


        OdbcCommand objSqlCmd = new OdbcCommand();
        OdbcDataReader objSqlDataRdr = null;
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);

        try
        {
            objConnection.Open();
            objSqlCmd.Connection = objConnection;

            objSqlCmd.CommandText = "SELECT SEMAILADDRESS FROM USER_INFO WHERE SUSERNAME = '" + Session["UserName"] + "' ";

            objSqlCmd.CommandType = System.Data.CommandType.Text;
            objSqlDataRdr = objSqlCmd.ExecuteReader();

            if (objSqlDataRdr.Read())
                from_email_addy = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(0));

            objSqlDataRdr.Close();
        }
        catch (Exception ex )
        {
            Response.Write("Can't load Web page " + ex.Message);
        }
        finally
        {
            objConnection.Close();
            objConnection.Dispose();
        }

        message.IsBodyHtml = true;
        message.Subject = "Change to Panel Request - myeWorkWell.com;";
        message.From = new System.Net.Mail.MailAddress("info@myeworkwell.com", "myeWorkWell Info Request");
        message.To.Add(new System.Net.Mail.MailAddress("customerservice@eworkwell.com", "Customer Service"));
        message.To.Add(new System.Net.Mail.MailAddress("notifications@eworkwell.com", "IT"));

        for ( int i = 0; i <= selectedPanelsGrd.Rows.Count - 1; i++)
        {
            GridViewRow row = selectedPanelsGrd.Rows[i];
            string sFileName = ((Label)row.FindControl("panelNameLbl")).ToString();
                
            string sChangesDesc = ((TextBox)row.FindControl("changesDesc")).Text;

            if (string.IsNullOrEmpty(sChangesDesc.Trim()) == true)
            {
                bChangeDescError = true;
                i = selectedPanelsGrd.Rows.Count + 1;
            }
            else
                sPanelChanges = sPanelChanges + "PANEL FILE NAME: " + sFileName + "<BR>" + "REQUESTED CHANGES TO PANEL: " + sChangesDesc + " <BR><BR><BR> PLEASE SEND A COPY OF THE UPDATED PANEL TO: " + from_email_addy + "<BR><BR>";

        }

        body += "The following panels(s) have been requested to be changed by the employer." + "<br/><br/>";
        body += sPanelChanges;
        body += "<hr />";
        body += "<font size='2' color='#cccccc'>";
        body += "<hr />";
        body += "www.myeworkwell.com";
        body += "<br>";
        body += System.DateTime.Now.AddHours(3);
        body += "</font>";
        
        message.Body = body;

        //relay-hosting.secureserver.net is the valid SMTP server of godaddy.com Only works on godaddy
        System.Net.Mail.SmtpClient mailClient = new System.Net.Mail.SmtpClient("dedrelay.secureserver.net");

        if (bChangeDescError == false)
        {
            try
            {
                mailClient.Send(message);
            }
            catch(Exception ex)
            {
                string error = ex.ToString();
            }
            message.Dispose();
            //eb 01/23/17 thier is no reason for the below will be removed with the session variable removal.
            for (int i = 0; i < panelSearchRsltsGrd.PageCount - 1; i++)
            {
                //********
                //this is to clear all selected locations session vars, i can't remove the session vars wirh remove (above), so I'm just seting everything to false for now.
                for (int x = 0; x < panelSearchRsltsGrd.Rows.Count - 1; x++)
                    values[x] = false;
                Session["eiRequestCP" + i] = values;
            }
            noChangeDescErrorTbl.Visible = false;
            selectedPanelsPanel.Visible = false;
            eiRqstOnlChngHdrPanel.Visible = false;
            requestSumbitPanel.Visible = true;
        }
        else
            noChangeDescErrorTbl.Visible = true;
    }
    public void CancelRequest(object sender, EventArgs e)
    {
        panelSearchPanel.Visible = true;
        searchResultsPanel.Visible = true;
        selectedPanelsPanel.Visible = false;
        noChangeDescErrorTbl.Visible = false;
    }
    public void CancelSelection(object sender, EventArgs e)
    {
        //Junk that i'm not for sure is needed or even wise to have
        Boolean[] values = new Boolean[panelSearchRsltsGrd.PageSize - 1];

        for (int i = 0; i <= panelSearchRsltsGrd.PageCount - 1; i++)
        {
            // this is to clear all selected locations session vars, i can't remove the session vars wirh remove (above), so I'm just seting everything to false for now.
            for (int x = 0; x <= panelSearchRsltsGrd.Rows.Count - 1; x++ )
                values[x] = false;
            Session["eiRequestCP" + i] = values;
        }
        
        GetPanels(sender, e);

        panelSearchRsltsGrd.PageIndex = 0;
        panelSearchRsltsGrd.DataBind();
    }
    public void ClearForm(object sender, EventArgs e)
    {
        companyNameTxt.Text = "";
        Boolean[] values = new Boolean[panelSearchRsltsGrd.PageSize - 1];
        for (int i = 0; i <= panelSearchRsltsGrd.PageCount - 1; i++)
        {
            // this is to clear all selected locations session vars, i can't remove the session vars wirh remove (above), so I'm just seting everything to false for now.
            for (int x = 0; x <= panelSearchRsltsGrd.Rows.Count - 1; x++)
                values[x] = false;
            Session["eiRequestCP" + i] = values;

        }
        
        GetPanels(sender, e);

        panelSearchRsltsGrd.PageIndex = 0;
        panelSearchRsltsGrd.DataBind();
    }
    public void GetSelectedPanels(object sender, EventArgs e)
    {
        DataTable selectedLocs = new DataTable();
        Boolean[] values = new Boolean[panelSearchRsltsGrd.PageSize - 1];
        CheckBox checkBx;
        int curPageOneChecked = 0;
        int bOneChecked = 0;
        int currentGrdPage = panelSearchRsltsGrd.PageIndex;

        noneSelectedErrorPanel.Visible = false;

        //get the selections current page that user is on
        for (int i = 0; i <= panelSearchRsltsGrd.Rows.Count - 1; i++)
        {
            checkBx = (CheckBox)(panelSearchRsltsGrd.Rows[i].FindControl("selectionBox"));
            if (checkBx != null)
            {
                values[i] = checkBx.Checked;
                if (values[i])
                    curPageOneChecked = 1;
            }
        }

        if (curPageOneChecked == 1)
            Session["eiRequestCP" + panelSearchRsltsGrd.PageIndex] = values;

        // loop thru the existing Session("eiRequestCP") and see if any are checked

        for (int i = 0; i <= panelSearchRsltsGrd.PageCount - 1; i++)
        {
            if (Session["eiRequestCP" + i] != null)
            {
                //set panelSearchRsltsGrd to page that the checked locations were

                GetPanels(sender, e);
                panelSearchRsltsGrd.PageIndex = i;
                panelSearchRsltsGrd.DataBind();


                Boolean[] prevValues = (Boolean[])Session["eiRequestCP" + i];

                for (int x = 0; x <= panelSearchRsltsGrd.Rows.Count - 1; x++)
                {
                    if (prevValues[x])
                        bOneChecked = 1;
                }
            }
        }

        if (bOneChecked == 1)
        {
            DataTable selectedPanels = new DataTable();
            selectedPanels.Columns.Add("FileName");

            for (int i = 0; i <= panelSearchRsltsGrd.PageCount - 1; i++)
            {
                if (Session["eiRequestCP" + i] != null)
                {

                    //set viewActiveLocationsGrd to page that the checked locations were

                    GetPanels(sender, e);
                    panelSearchRsltsGrd.PageIndex = i;
                    panelSearchRsltsGrd.DataBind();

                    Boolean[] prevValues = (Boolean[])Session["eiRequestCP" + i];

                    for (int x = 0; x <= panelSearchRsltsGrd.Rows.Count - 1; x++)
                    {
                        if (prevValues[x])
                        {
                            string sFileName = ((HyperLink)(panelSearchRsltsGrd.Rows[x].FindControl("panelLink"))).Text;
                            DataRow dr = selectedPanels.NewRow();
                            dr["FileName"] = sFileName;
                            selectedPanels.Rows.Add(dr);
                        }
                    }
                }

                //now get back to the current page that they were on before submiting the selections
                GetPanels(sender, e);
                panelSearchRsltsGrd.PageIndex = currentGrdPage;
                panelSearchRsltsGrd.DataBind();

                //keep to current page's selections checked if they click back

                Boolean[] existingValues  = (Boolean[])Session["eiRequestCP" + panelSearchRsltsGrd.PageIndex];

                for (int x = 0; x <= panelSearchRsltsGrd.Rows.Count - 1; x++)
                {
                    if (existingValues[x])
                        ((CheckBox)(panelSearchRsltsGrd.Rows[x].FindControl("selectionBox"))).Checked = true;
                }
            }

            selectedPanelsGrd.DataSource = selectedPanels;
            selectedPanelsGrd.DataBind();

            selectedPanelsPanel.Visible = true;
            searchResultsPanel.Visible = false;
            panelSearchPanel.Visible = false;
        }
        else
        {
            //show error
            noneSelectedErrorPanel.Visible = true;
            GetPanels(sender, e);

            panelSearchRsltsGrd.PageIndex = currentGrdPage;
            panelSearchRsltsGrd.DataBind();
        }
    }

    protected void panelSearchRsltsGrd_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        noneSelectedErrorPanel.Visible = false;

        int d = panelSearchRsltsGrd.PageCount;
        Boolean[] values = new Boolean[panelSearchRsltsGrd.PageSize - 1];
        CheckBox chb;
        int isChecked = 0;
        for (int i = 0; i <=  panelSearchRsltsGrd.Rows.Count - 1; i++)
        {
            chb = (CheckBox)(panelSearchRsltsGrd.Rows[i].FindControl("selectionBox"));
            if (chb != null)
            {
                values[i] = chb.Checked;
                isChecked = 1;
            }
        }
        if (isChecked == 1)
            Session["eiRequestCP" + panelSearchRsltsGrd.PageIndex] = values;
        GetPanels(sender, e);
        panelSearchRsltsGrd.PageIndex = e.NewPageIndex;
        panelSearchRsltsGrd.DataBind();
        if (Session["eiRequestCP" + e.NewPageIndex] != null)
        {
            Boolean[] prevValues = new Boolean[Convert.ToInt32(Session["eiRequestCP" + e.NewPageIndex])];

            for (int x = 0; x <= panelSearchRsltsGrd.Rows.Count - 1; x++)
            {
                if (prevValues[x])
                    ((CheckBox)(panelSearchRsltsGrd.Rows[x].FindControl("selectionBox"))).Checked = true;
            }
        }
    }
}