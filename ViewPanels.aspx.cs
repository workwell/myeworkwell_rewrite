﻿using System.IO;
using System.Data;
using System;
using System.Web.UI.WebControls;
public partial class ViewPanels : System.Web.UI.Page
{
    DataTable nwPanelURLs = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.Request.IsAuthenticated)
        {
            AuthenticatedMessagePanel.Visible = true;
            AnonymousMessagePanel.Visible = false;
            GetPanels(sender, e);
        }
        else
        {
            AuthenticatedMessagePanel.Visible = false;
            AnonymousMessagePanel.Visible = true;
        }
    }
    public void GetPanels(object sender, EventArgs e)
    {
        nwPanelURLs = new DataTable();
        nwPanelURLs.Columns.Add("FileName");
        nwPanelURLs.Columns.Add("FileDir");
        nwPanelURLs.Columns.Add("UploadDate");

        string iPartyID  = Convert.ToString(Session["PartyID"]);
        string iInsPartyID = Convert.ToString(Session["InsPartyID"]);
        string motorists = Convert.ToString(Session["MotoristsSub"]);
        searchResultsPanel.Visible = true;
        nwNoResultsFoundTbl.Visible = false;
        if (Convert.ToString(Session["InsCarrier"]) == "1")
        {
            if (iInsPartyID == "5674")
                GetFiles("/PANELS/" + iInsPartyID + "/");
            
            else
                GetFiles("/PANELS/" + iInsPartyID + "/" + Session["MotoristsSub"]);
            //10/31/16 eb fixed an issue with strcomp where it wasn't working properly and i'm just going to 
            // use the simple version instead.
        }
        else if (iInsPartyID == "4107")
            Response.Redirect("NationWideDemo.aspx");
        else if (iInsPartyID == "45516")
            GetFiles("/PANELS/DemoPanels/");
        else if (iPartyID == "388046") //11/7/16 update for catholic fixing thier workaround they seperated both when it wasn't required
            GetFiles("/Panels/388046/");
        else if (iPartyID == "15900") //11/7/16 update for catholic fixing thier workaround they seperated both when it wasn't required
            GetFiles("/Panels/388046/");
        else if (Convert.ToString(Session["InsCarrier"]) == "N")
            GetFiles("/PANELS/" + iPartyID + "/");
        else if (Convert.ToString(Session["InsCarrier"]) == "I")
            GetFiles("/PANELS/" + iPartyID + "/" + Session["CarrierID"]);
        else if (Convert.ToString(Session["InsCarrier"]) == "E")
        {
            if (Convert.ToString(Session["MultipleLocs"]) == "1") // no multiple locs
                GetFiles("/PANELS/" + iPartyID + "/");
            //9/16/16
            //Setting for callos per ticket 2727
            else if (iPartyID == "15576")
                GetFiles("/PANELS/" + iPartyID + "/");
            else
                GetFiles("/PANELS/" + iPartyID + "/" + Session["CustAccountID"]);
        }
        else
        {
            if (iInsPartyID == "5188")
                //GetFiles("/PANELS/" & iInsPartyID & "/" & iPartyID & "/") ' was changed 9/23/14 ticket #612
                GetFiles("/PANELS/" + iPartyID + "/");
            else if (iPartyID == "4097")
                GetFiles("/PANELS/" + iPartyID + "/" + motorists + "/");
            else
                GetFiles("/PANELS/" + iPartyID + "/");
        }

        panelSearchRsltsGrd.DataSource = nwPanelURLs;
        panelSearchRsltsGrd.DataBind();

        if (nwPanelURLs.Rows.Count == 0)
            nwNoResultsFoundTbl.Visible = true;

    }
    public void GetFiles(string path)
    {
        //7/28/16 eb
        //Was doing the find path incorrectly here. Made changes to include the ~/ to get the root then the given file path...
        string myPath = Server.MapPath("~/" + path);
        if (File.Exists(myPath))
            ProcessFile(myPath);
        else if (Directory.Exists(myPath))
            ProcessDirectory(myPath);
    }
    public void ProcessDirectory(string targetDirectory)
    {
        string[] fileEntries = Directory.GetFiles(targetDirectory);
        foreach (string fileName in fileEntries)
            ProcessFile(fileName);

        string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
        foreach (string subdirectory in subdirectoryEntries)
            ProcessDirectory(subdirectory);
    }
    public void ProcessFile(string path)
    {
        FileInfo fi = new FileInfo(path);
        string companyNameStr = nwCompanyNameTxt.Text.Trim();

        if (string.IsNullOrEmpty(nwCompanyNameTxt.Text) == false)
        {
            if (fi.Name.ToLower().Contains(companyNameStr) && fi.FullName.ToLower().Contains(".pdf"))
            {
                DataRow dr = nwPanelURLs.NewRow();
                dr["FileName"] = fi.Name;
                dr["FileDir"] = fi.FullName.Replace("D:\\Hosting\\6546477\\html", "");
                dr["UploadDate"] = fi.LastWriteTime.ToShortDateString();
                nwPanelURLs.Rows.Add(dr);
            }
        }
        else
        {
            if (fi.FullName.ToLower().Contains(".pdf"))
            {
                DataRow dr = nwPanelURLs.NewRow();
                dr["FileName"] = fi.Name;
                dr["FileDir"] = fi.FullName.Replace("D:\\Hosting\\6546477\\html", "");
                dr["UploadDate"] = fi.LastWriteTime.ToShortDateString();
                nwPanelURLs.Rows.Add(dr);
            }
        }
    }
    public void GetDocuments(string relativePath )
    {

        string physicalPath = Server.MapPath(relativePath);
        //Response.Write("physical path " & physicalPath & "<BR>");
        System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(physicalPath);

        if (directory.Exists)
        {
            string searchStr = "";

            if (String.IsNullOrEmpty(nwCompanyNameTxt.Text) == false)
                searchStr = "*" + nwCompanyNameTxt.Text + "*.pdf";
            else
                searchStr = searchStr + "*.pdf";

            foreach (FileInfo myFile in directory.GetFiles(searchStr))
            {
                DataRow dr = nwPanelURLs.NewRow();
                dr["FileName"] = myFile.Name;
                dr["FileDir"] = myFile.FullName.Replace("D:\\Hosting\\6546477\\html", "");
                dr["UploadDate"] = myFile.LastWriteTime.ToShortDateString();
                nwPanelURLs.Rows.Add(dr);
            }
        }
    }
    protected void nwPanelSearchRsltsGrd_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e) 
    {
        GetPanels( sender, e);
        panelSearchRsltsGrd.PageIndex = e.NewPageIndex;
        panelSearchRsltsGrd.DataBind();
    }
    public void NewSearch()
    {
        searchResultsPanel.Visible = false;
        nwPanelSearchPanel.Visible = true;
    }
    public void ClearForm(object sender, EventArgs e)
    {
        nwCompanyNameTxt.Text = "";
        GetPanels(sender, e);
    }
    protected void panelSearchRsltsGrd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GetPanels(sender, e);
        panelSearchRsltsGrd.PageIndex = e.NewPageIndex;
        panelSearchRsltsGrd.DataBind();
    }
    protected void panelSearchRsltsGrd_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            //redid this and removed the startup scripts and made these actual js calls
            string file_ = e.CommandArgument.ToString();
            int iPartyID = Convert.ToInt32(Session["PartyID"]);
            string sCarrierID = Convert.ToString(Session["CarrierID"]);
            string sCustAccountID = Convert.ToString(Session["CustAccountID"]);

            string iInsPartyID = Convert.ToString(Session["InsPartyID"]);
            string motorists = Convert.ToString(Session["MotoristsSub"]);
            if (iInsPartyID == "5674")
            {
                Response.Write("<script>window.open('PANELS/" + iInsPartyID.ToString() + "/" + file_ + "');</script>");
                //10/20/16 this next else if is specifically for the demo site capability upgrade
            }
            else if (iInsPartyID == "45516")
            {
                Response.Write("<script>window.open('PANELS/DemoPanels/" + file_ + "');</script>");
                //9/16/16 eb I had to put in this if statement due to the way motorists is set up and how it was trying to open the party id/party id
                //Now it opens party id/ cust account id / file name.
            }
            else if (iPartyID == 388046) //11/7/16 update for catholic fixing thier workaround they seperated both when it wasn't required
            {
                Response.Write("<script>window.open('PANELS/388046/" + file_ + "');</script>");
            }
            else if (iPartyID == 15900) //11/8/16 update for the fact or statements are a pain.
            {
                Response.Write("<script>window.open('PANELS/388046/" + file_ + "');</script>");
            }
            else if (iInsPartyID == "4097")
            {
                Response.Write("<script>window.open('PANELS/" + iInsPartyID.ToString() + "/" + motorists + "/" + file_ + "');</script>");
            }
            //Below is for callos. Has a different folder structure than normal.
            else if (iPartyID == 15576)
            {
                Response.Write("<script>window.open('PANELS/" + iPartyID.ToString() + "//" + file_ + "');</script>");
            }
            else if (Convert.ToString(Session["InsCarrier"]) == "I")
            {
                Response.Write("<script>window.open('PANELS/" + iPartyID.ToString() + "//" + sCarrierID + "//" + file_ + "');</script>");
            }
            else if (Convert.ToString(Session["InsCarrier"]) == "E")
            {
                if (Convert.ToString(Session["MultipleLocs"]) == "1") // no multiple locs
                {
                    Response.Write("<script>window.open('PANELS/" + iPartyID.ToString() + "/" + file_ + "');</script>");
                }
                else
                {
                    Response.Write("<script>window.open('PANELS/" + iPartyID.ToString() + "/" + sCustAccountID + "/" + file_ + "');</script>");
                }
                //7/19/16 10:22 am changed 1 to Y i don't even think the inscarrier can be 1 for motorists and is set to Y in the log in search.
                //10:31 ok some reason it needs to be 1 for the whole drop down. this is getting beyond confusing.
                //10:43 copied in the code for the filling the table
            }
            else if (Convert.ToString(Session["InsCarrier"]) == "1")
            {
                Response.Write("<script>window.open('PANELS/" + iInsPartyID.ToString() + "/" + Session["MotoristsSub"] + "/" + file_ + "');</script>");

            }
            else
            {
                Response.Write("<script>window.open('PANELS/" + iPartyID.ToString() + "/" + file_ + "');</script>" );
            }
        }

    }
}