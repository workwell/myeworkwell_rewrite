﻿Imports System.Data
Imports System.Data.Odbc
Imports System.Math
Imports System.Drawing


Partial Class ei_SearchProv
    Inherits System.Web.UI.Page

    Private dynamicGrids() As GridView
    Private iCount As Integer = 0



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserType") = 2 Then
            Response.Redirect("iiRoadMap.aspx")
        ElseIf Session("UserType") = 3 Then
            Response.Redirect("ciStatusReport.aspx")
        ElseIf Session("UserType") = 4 Then
            Response.Redirect("MACInsurance.aspx")
        ElseIf Session("UserType") = 5 Then
            Response.Redirect("AholdIT.aspx")
        ElseIf Session("UserType") = 6 Then
            Response.Redirect("Nationwide.aspx")
        ElseIf Session("UserType") = 7 Then
            Response.Redirect("SynergyComp.aspx")
        ElseIf Session("UserType") = -96 Then
            Response.Redirect("dv_ciStatusReport.aspx")
        ElseIf Session("UserType") = -97 Then
            Response.Redirect("dv_eiRoadMap.aspx")
        End If

        eiSearchProvOMTbl.Style("border-collapse") = "collapse"
        eiSearchProvIMTbl.Style("border-collapse") = "collapse"
        eiSearchProvFPTbl.Style("border-collapse") = "collapse"
        eiSearchProvORTbl.Style("border-collapse") = "collapse"
        eiSearchProvGSTbl.Style("border-collapse") = "collapse"
        eiSearchProvNSTbl.Style("border-collapse") = "collapse"
        eiSearchProvOtherTbl.Style("border-collapse") = "collapse"
        eiSearchProvByNameTbl.Style("border-collapse") = "collapse"

        eiSearchByZipTP.HeaderText = "Search By Zip Code"
        eiSearchByNameTP.HeaderText = "Search By Physician Name"

        

        If Request.IsAuthenticated Then
            AuthenticatedMessagePanel.Visible = True
            AnonymousMessagePanel.Visible = False
            If Not IsPostBack Then
                ShowSearchFields()
            End If
        Else
            AuthenticatedMessagePanel.Visible = False
            AnonymousMessagePanel.Visible = True
        End If


    End Sub

    Sub ShowSearchFields()

        'searchProvidersByZipPanel.Visible = True
        eiSearchFiledsPanel.Visible = True

    End Sub

    Sub ShowProvidersByZip()

        Dim objSqlCmd As New OdbcCommand
        Dim objSqlDataRdr As OdbcDataReader = Nothing
        Dim strConnection As String = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;" 'ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()
        Dim objConnection As New OdbcConnection(strConnection)
        Dim decLat As Decimal = 0
        Dim decLng As Decimal = 0
        Dim iRowCount As Integer = 0

        notFoundZipError.Visible = False
        invalidZipError.Visible = False

        If String.IsNullOrEmpty(eiSearchZipCodeTxt.Text) = False Then

            If IsNumeric(eiSearchZipCodeTxt.Text) And eiSearchZipCodeTxt.Text.Length = 5 Then

                objConnection.Open()

                objSqlCmd.Connection = objConnection
                objSqlCmd.CommandText = "SELECT LAT, LON " & _
                                        "FROM MEWW_ZIP_CODE " & _
                                        "WHERE ZIP_CODE = " & eiSearchZipCodeTxt.Text

                objSqlCmd.CommandType = Data.CommandType.Text
                objSqlDataRdr = objSqlCmd.ExecuteReader()

                If objSqlDataRdr.Read Then

                    'hide all panels


                    decLat = objSqlDataRdr.Item(0)
                    decLng = objSqlDataRdr.Item(1)

                    Dim latRng As Decimal = eiSearchDistanceDrp.SelectedValue / 69.172
                    Dim lngRng As Decimal = Abs(eiSearchDistanceDrp.SelectedValue / (Cos(decLat) * 69.172))
                    Dim latMin As Decimal = decLat - latRng
                    Dim latMax As Decimal = decLat + latRng
                    Dim lngMin As Decimal = decLng - lngRng
                    Dim lngMax As Decimal = decLng + lngRng
                    Dim sSpecialtyQry As String = ""

                    Dim objSqlCmd2 As New OdbcCommand
                    Dim objSqlDataRdr2 As OdbcDataReader = Nothing

                    For iWhichTab = 1 To 7


                        'Response.Write("steppint thru " & iWhichTab & "<BR>")
                        Select Case iWhichTab
                            Case 1
                                sSpecialtyQry = " HP.ATTRIBUTE1 = 'OM' "
                                eiSearchProvOMTP.HeaderText = "Occupational Medicine"
                            Case 2
                                sSpecialtyQry = " HP.ATTRIBUTE1 = 'IM' "
                                eiSearchProvIMTP.HeaderText = "Internal Medicine"
                            Case 3
                                sSpecialtyQry = " HP.ATTRIBUTE1 = 'FP' "
                                eiSearchProvFPTP.HeaderText = "Family Practice"

                            Case 4
                                sSpecialtyQry = " HP.ATTRIBUTE1 = 'OR' "
                                eiSearchProvORTP.HeaderText = "Orthopedics"
                            Case 5
                                sSpecialtyQry = " HP.ATTRIBUTE1 = 'GS' "
                                eiSearchProvGSTP.HeaderText = "General Surgery"
                            Case 6
                                sSpecialtyQry = " HP.ATTRIBUTE1 = 'NS' "
                                eiSearchProvNSTP.HeaderText = "Neurosurgery"
                            Case 7
                                sSpecialtyQry = " HP.ATTRIBUTE1 <> 'OM' AND HP.ATTRIBUTE1 <> 'IM' AND HP.ATTRIBUTE1 <> 'FP' AND HP.ATTRIBUTE1 <> 'OR' AND HP.ATTRIBUTE1 <> 'GS' AND  HP.ATTRIBUTE1 <> 'NS' "
                                eiSearchProvOtherTP.HeaderText = "Other Specialties"
                        End Select

                        objSqlCmd2.Connection = objConnection

                        If iWhichTab <> 7 Then
                            '"SELECT CONCAT(HP.PERSON_FIRST_NAME,' ',HP.PERSON_LAST_NAME) 'NAME' , CONCAT(HL.ADDRESS1, ', ', HL.ADDRESS2) 'ADDRESS', HL.CITY 'CITY', HL.STATE 'STATE', HL.POSTAL_CODE 'ZIP', HCA.CUST_ACCOUNT_ID 'ID' " & _
                            ' , (((ACOS(SIN((" & decLat & "*PI()/180)) * SIN((`lat`*PI()/180))+COS((" & decLat & "*PI()/180)) * COS((`lat`*PI()/180)) * COS(((" & decLng & "- `lon`)*PI()/180))))*180/PI())*60*1.1515) 'DIST' " & _
                            objSqlCmd2.CommandText = "SELECT CONCAT(HP.PERSON_FIRST_NAME,' ',HP.PERSON_LAST_NAME) 'NAME' , HL.ADDRESS1 'ADDRESS', HL.CITY 'CITY', HL.STATE 'STATE', HL.POSTAL_CODE 'ZIP', HCA.CUST_ACCOUNT_ID 'ID' " & _
                                                    "FROM HZ_PARTIES HP,  " & _
                                                    "     HZ_CUST_ACCOUNTS HCA, " & _
                                                    "     HZ_CUST_ACCT_SITES_ALL HCASA, " & _
                                                    "     HZ_PARTY_SITES HPS, " & _
                                                    "     HZ_LOCATIONS HL, " & _
                                                    "     MEWW_ZIP_CODE ZIP " & _
                                                    "WHERE HP.PARTY_ID = HCA.PARTY_ID AND  " & _
                                                    "      HCA.STATUS = 'A' AND  " & _
                                                    "      HL.LOCATION_ID = HPS.LOCATION_ID AND " & _
                                                    "      HPS.PARTY_SITE_ID = HCASA.PARTY_SITE_ID AND " & _
                                                    "      HCASA.CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID AND " & _
                                                    "      HL.POSTAL_CODE = ZIP.ZIP_CODE AND  " & _
                                                    "      ZIP.LAT BETWEEN " & latMin & " AND " & latMax & " AND " & _
                                                    "      ZIP.LON BETWEEN " & lngMin & " AND " & lngMax & " AND " & _
                                                    "      HP.PARTY_TYPE = 'PERSON' AND " & _
                                                    "      HP.ATTRIBUTE_CATEGORY = 'PHYSICIAN NETWORK'  AND " & _
                                                    sSpecialtyQry & _
                                                    " ORDER BY HL.POSTAL_CODE, HL.ADDRESS1, HL.ADDRESS2, HP.PERSON_LAST_NAME, HP.PERSON_FIRST_NAME "
                            '" ORDER BY DIST, HP.ADDRESS1, HP.ADDRESS2 "

                            '"HAVING DIST <= " & rng & " " & _

                        Else
                            '"SELECT CONCAT(HP.PERSON_FIRST_NAME,' ',HP.PERSON_LAST_NAME) 'NAME' , HL.ADDRESS1 'ADDRESS', HL.CITY 'CITY', HL.STATE 'STATE', HL.POSTAL_CODE 'ZIP', HCA.CUST_ACCOUNT_ID 'ID', SP.SPECIALTY_NAME 'SPECIALTY' " & _ , HP.ATTRIBUTE1 'SPECIALTY' " & _
                            objSqlCmd2.CommandText = "SELECT CONCAT(HP.PERSON_FIRST_NAME,' ',HP.PERSON_LAST_NAME) 'NAME' , HL.ADDRESS1 'ADDRESS', HL.CITY 'CITY', HL.STATE 'STATE', HL.POSTAL_CODE 'ZIP', HCA.CUST_ACCOUNT_ID 'ID', SP.SPECIALTY_NAME 'SPECIALTY' " & _
                                                    "FROM HZ_PARTIES HP,  " & _
                                                    "     HZ_CUST_ACCOUNTS HCA, " & _
                                                    "     HZ_CUST_ACCT_SITES_ALL HCASA, " & _
                                                    "     HZ_PARTY_SITES HPS, " & _
                                                    "     HZ_LOCATIONS HL, " & _
                                                    "     MEWW_ZIP_CODE ZIP, " & _
                                                    "     MEWW_SPECIALTIES SP " & _
                                                    "WHERE HP.PARTY_ID = HCA.PARTY_ID AND  " & _
                                                    "      HCA.STATUS = 'A' AND  " & _
                                                    "      HL.LOCATION_ID = HPS.LOCATION_ID AND " & _
                                                    "      HPS.PARTY_SITE_ID = HCASA.PARTY_SITE_ID AND " & _
                                                    "      HCASA.CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID AND " & _
                                                    "      HL.POSTAL_CODE = ZIP.ZIP_CODE AND  " & _
                                                    "      ZIP.LAT BETWEEN " & latMin & " AND " & latMax & " AND " & _
                                                    "      ZIP.LON BETWEEN " & lngMin & " AND " & lngMax & " AND " & _
                                                    "      HP.PARTY_TYPE = 'PERSON' AND " & _
                                                    "      HP.ATTRIBUTE1 = SP.SPECIALTY_ABR AND " & _
                                                    "      HP.ATTRIBUTE_CATEGORY = 'PHYSICIAN NETWORK' AND " & _
                                                    sSpecialtyQry & _
                                                    " ORDER BY SP.SPECIALTY_NAME, HL.POSTAL_CODE, HL.ADDRESS1, HL.ADDRESS2, HP.PERSON_LAST_NAME, HP.PERSON_FIRST_NAME "
                            '" ORDER BY HP.ATTRIBUTE1, HP.PERSON_LAST_NAME, HP.PERSON_FIRST_NAME, HP.ADDRESS1, HP.ADDRESS2 "
                        End If
                        '"      HP.ATTRIBUTE1 = SP.SPECIALTY_ABR AND " & _
                        '"     MEWW_SPECIALTIES SP " & _
                        '" ORDER BY SP.SPECIALTY_NAME, HP.PERSON_LAST_NAME, HP.PERSON_FIRST_NAME, HP.ADDRESS1, HP.ADDRESS2 "
                        objSqlCmd2.CommandType = Data.CommandType.Text
                        objSqlDataRdr2 = objSqlCmd2.ExecuteReader()

                        iRowCount = 0

                        While objSqlDataRdr2.Read

                            Dim tblHeaderRow As New TableHeaderRow
                            Dim tblRow As New TableRow
                            Dim cellHdrChkbx As New TableCell
                            Dim cellHdrName As New TableHeaderCell
                            Dim cellHdrAddress As New TableHeaderCell
                            Dim cellHdrCity As New TableHeaderCell
                            Dim cellHdrState As New TableHeaderCell
                            Dim cellHdrZip As New TableHeaderCell
                            Dim lblHdrNameValue As New Label
                            Dim lblHdrAddressValue As New Label
                            Dim lblHdrCityValue As New Label
                            Dim lblHdrStateValue As New Label
                            Dim lblHdrZipValue As New Label
                            Dim chkbxHdrProv As New Label
                            Dim cellChkbx As New TableCell
                            Dim cellName As New TableCell
                            Dim cellAddress As New TableCell
                            Dim cellCity As New TableCell
                            Dim cellState As New TableCell
                            Dim cellZip As New TableCell
                            Dim lblNameValue As New Label
                            Dim lblAddressValue As New Label
                            Dim lblCityValue As New Label
                            Dim lblStateValue As New Label
                            Dim lblZipValue As New Label
                            Dim chkbxProv As New CheckBox

                            If iRowCount = 0 Then

                                lblHdrNameValue.Text = "Physician Name"
                                'lblHdrNameValue.Font.Size = FontUnit.Small
                                'lblHdrNameValue.Font.Bold = True
                                cellHdrName.Width = 100

                                'cellHdrName.HorizontalAlign = HorizontalAlign.Center
                                'cellHdrName.BackColor = Drawing.Color.Black
                                'cellHdrName.ForeColor = Drawing.Color.White
                                cellHdrName.Controls.Add(lblHdrNameValue)

                                lblHdrAddressValue.Text = "Address"
                                'lblHdrAddressValue.Font.Size = FontUnit.Small
                                'lblHdrAddressValue.Font.Bold = True
                                If iWhichTab <> 7 Then
                                    cellHdrAddress.Width = 200
                                Else
                                    cellHdrAddress.Width = 100
                                End If

                                'cellHdrAddress.HorizontalAlign = HorizontalAlign.Center
                                'cellHdrAddress.BackColor = Drawing.Color.Black
                                'cellHdrAddress.ForeColor = Drawing.Color.White
                                cellHdrAddress.Controls.Add(lblHdrAddressValue)

                                lblHdrCityValue.Text = "City"
                                'lblHdrCityValue.Font.Size = FontUnit.Small
                                'lblHdrCityValue.Font.Bold = True
                                'cellHdrCity.Width = 75
                                'cellHdrCity.HorizontalAlign = HorizontalAlign.Center
                                'cellHdrCity.BackColor = Drawing.Color.Black
                                'cellHdrCity.ForeColor = Drawing.Color.White
                                cellHdrCity.Controls.Add(lblHdrCityValue)

                                lblHdrStateValue.Text = "State"
                                'lblHdrStateValue.Font.Size = FontUnit.Small
                                'lblHdrStateValue.Font.Bold = True
                                cellHdrState.Width = 20
                                'cellHdrState.HorizontalAlign = HorizontalAlign.Center
                                'cellHdrState.BackColor = Drawing.Color.Black
                                'cellHdrState.ForeColor = Drawing.Color.White
                                cellHdrState.Controls.Add(lblHdrStateValue)

                                lblHdrZipValue.Text = "Zip"
                                'lblHdrZipValue.Font.Size = FontUnit.Small
                                'lblHdrZipValue.Font.Bold = True
                                cellHdrZip.Width = 30
                                'cellHdrZip.HorizontalAlign = HorizontalAlign.Center
                                'cellHdrZip.BackColor = Drawing.Color.Black
                                'cellHdrZip.ForeColor = Drawing.Color.White
                                '.HorizontalAlign = HorizontalAlign.Center
                                cellHdrZip.Controls.Add(lblHdrZipValue)

                                tblHeaderRow.Cells.Add(cellHdrName)
                                tblHeaderRow.Cells.Add(cellHdrAddress)
                                tblHeaderRow.Cells.Add(cellHdrCity)
                                tblHeaderRow.Cells.Add(cellHdrState)
                                tblHeaderRow.Cells.Add(cellHdrZip)

                                If iWhichTab = 7 Then
                                    Dim cellHdrSpecialty As New TableHeaderCell
                                    Dim lblHdrSpecialtyValue As New Label
                                    lblHdrSpecialtyValue.Text = "Specialty"
                                    'lblHdrSpecialtyValue.Font.Size = FontUnit.Small
                                    'lblHdrSpecialtyValue.Font.Bold = True
                                    cellHdrSpecialty.Width = 200
                                    'cellHdrSpecialty.BackColor = Drawing.Color.Black
                                    'cellHdrSpecialty.ForeColor = Drawing.Color.White
                                    'cellHdrSpecialty.HorizontalAlign = HorizontalAlign.Center
                                    cellHdrSpecialty.Controls.Add(lblHdrSpecialtyValue)
                                    tblHeaderRow.Cells.Add(cellHdrSpecialty)
                                End If

                                Select Case iWhichTab
                                    Case 1
                                        eiSearchProvOMTbl.Rows.Add(tblHeaderRow)
                                    Case 2
                                        eiSearchProvIMTbl.Rows.Add(tblHeaderRow)
                                    Case 3
                                        eiSearchProvFPTbl.Rows.Add(tblHeaderRow)
                                    Case 4
                                        eiSearchProvORTbl.Rows.Add(tblHeaderRow)
                                    Case 5
                                        eiSearchProvGSTbl.Rows.Add(tblHeaderRow)
                                    Case 6
                                        eiSearchProvNSTbl.Rows.Add(tblHeaderRow)
                                    Case 7
                                        eiSearchProvOtherTbl.Rows.Add(tblHeaderRow)
                                End Select

                            End If

                                iRowCount = iRowCount + 1


                                lblNameValue.Text = objSqlDataRdr2.Item(0)
                                cellName.Width = 100
                                cellName.HorizontalAlign = HorizontalAlign.Left
                                cellName.Controls.Add(lblNameValue)

                                'commented this out betcause Darlene doesn't want the address to be listed, just Call 1-800-662-2400
                                'lblAddressValue.Text = objSqlDataRdr.Item(1)
                            lblAddressValue.Text = "Call 1-800-662-2400"
                            cellAddress.Width = 150
                            cellAddress.HorizontalAlign = HorizontalAlign.Center
                            cellAddress.Font.Bold = True
                            cellAddress.Controls.Add(lblAddressValue)

                                lblCityValue.Text = objSqlDataRdr2.Item(2)
                                cellCity.Width = 75
                                cellCity.HorizontalAlign = HorizontalAlign.Left
                                cellCity.Controls.Add(lblCityValue)

                                lblStateValue.Text = objSqlDataRdr2.Item(3)
                                cellState.Width = 20
                                cellState.HorizontalAlign = HorizontalAlign.Left
                                cellState.Controls.Add(lblStateValue)

                                lblZipValue.Text = objSqlDataRdr2.Item(4)
                                cellZip.Width = 30
                                cellZip.HorizontalAlign = HorizontalAlign.Left
                                cellZip.Controls.Add(lblZipValue)

                                tblRow.Cells.Add(cellName)
                                tblRow.Cells.Add(cellAddress)
                                tblRow.Cells.Add(cellCity)
                                tblRow.Cells.Add(cellState)
                                tblRow.Cells.Add(cellZip)


                                Dim tblCell, tblCell2, tblCell3 As New TableCell()
                                If iRowCount Mod 2 = 0 Then
                                    tblRow.BackColor = Color.FromArgb(&H98C6E7)
                                    'Drawing.Color.LightBlue
                                Else
                                    tblRow.BackColor = Drawing.Color.White
                                End If

                                If iWhichTab = 7 Then
                                    Dim cellSpecialty As New TableCell
                                    Dim lblSpecialtyValue As New Label
                                    lblSpecialtyValue.Text = objSqlDataRdr2.Item(6)
                                    cellSpecialty.Width = 20
                                    cellSpecialty.HorizontalAlign = HorizontalAlign.Center
                                    cellSpecialty.Controls.Add(lblSpecialtyValue)
                                    tblRow.Cells.Add(cellSpecialty)
                                End If

                                Select Case iWhichTab
                                    Case 1
                                        eiSearchProvOMTbl.Rows.Add(tblRow)
                                    Case 2
                                        eiSearchProvIMTbl.Rows.Add(tblRow)
                                    Case 3
                                        eiSearchProvFPTbl.Rows.Add(tblRow)
                                    Case 4
                                        eiSearchProvORTbl.Rows.Add(tblRow)
                                    Case 5
                                        eiSearchProvGSTbl.Rows.Add(tblRow)
                                    Case 6
                                        eiSearchProvNSTbl.Rows.Add(tblRow)
                                    Case 7
                                        eiSearchProvOtherTbl.Rows.Add(tblRow)
                                End Select

                        End While

                        If iRowCount = 0 Then

                            Dim tblRow As New TableRow
                            Dim cellNoResults As New TableCell
                            Dim lblNoResults As New Label

                            lblNoResults.Text = "No Providers Found"
                            'cellNoResults.ColumnSpan
                            cellNoResults.HorizontalAlign = HorizontalAlign.Left
                            cellNoResults.BackColor = Drawing.Color.LightYellow
                            cellNoResults.Controls.Add(lblNoResults)
                            tblRow.Controls.Add(cellNoResults)

                            Select Case iWhichTab
                                Case 1
                                    eiSearchProvOMTbl.Rows.Add(tblRow)
                                Case 2
                                    eiSearchProvIMTbl.Rows.Add(tblRow)
                                Case 3
                                    eiSearchProvFPTbl.Rows.Add(tblRow)
                                Case 4
                                    eiSearchProvORTbl.Rows.Add(tblRow)
                                Case 5
                                    eiSearchProvGSTbl.Rows.Add(tblRow)
                                Case 6
                                    eiSearchProvNSTbl.Rows.Add(tblRow)
                                Case 7
                                    eiSearchProvOtherTbl.Rows.Add(tblRow)
                            End Select

                        End If

                        objSqlDataRdr2.Close()
                    Next

                    searchProvidersByZipResultsPanel.Visible = True
                    eiSearchFiledsPanel.Visible = False
                    eiSearchByZipSuccessful.Value = 1
                Else
                    'show zip wan't found
                    notFoundZipError.Visible = True
                End If
            Else
                invalidZipError.Visible = True

            End If
            'If eiSearchByZipSuccessful.Value = 1 Then
            ' eiSearchFiledsPanel.Visible = False
            ' searchProvidersByZipResultsPanel.Visible = True
            'End If
        Else
        invalidZipError.Visible = True
        End If

        ' get insurance carrier for the location chosen

        'Catch ex As Exception
        'Response.Write("Can't load Web page-" & ex.Message)
        'Finally
        ' objSqlDataRdr.Close()
        'objSqlDataRdr.Dispose()
        objConnection.Close()
        objConnection.Dispose()
        'End Try



    End Sub

    'Protected Sub btnClicked(ByVal sender As Object, ByVal e As EventArgs)
    Protected Sub btnClicked(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btn As ImageButton = DirectCast(sender, ImageButton)

        Select Case btn.CommandName
            Case "search"
                If eiSearchByZipSuccessful.Value = 1 Then
                    'searchProvidersByZipPanel.Visible = False
                    'searchProvidersByNamePanel.Visible = False
                    eiSearchFiledsPanel.Visible = False
                    searchProvidersByZipResultsPanel.Visible = True
                End If

                If eiSearchByNameSuccessful.Value = 1 Then
                    'searchProvidersByNamePanel.Visible = False
                    'searchProvidersByZipPanel.Visible = False
                    eiSearchFiledsPanel.Visible = False
                    searchProvidersByNameResultsPanel.Visible = True
                End If

            Case "newsearch"
                'searchProvidersByZipPanel.Visible = True
                'searchProvidersByNamePanel.Visible = True
                eiSearchFiledsPanel.Visible = True
                searchProvidersByZipResultsPanel.Visible = False
                searchProvidersByNameResultsPanel.Visible = False
                eiSearchZipCodeTxt.Text = ""
                eiSearchFNameTxt.Text = ""
                eiSearchLNameTxt.Text = ""
                iCount = 0
                eiSearchByZipSuccessful.Value = 0
                eiSearchByNameSuccessful.Value = 0
                eiSearchProvIMTbl.Rows.Clear()
                eiSearchProvOMTbl.Rows.Clear()
                eiSearchProvFPTbl.Rows.Clear()
                eiSearchProvGSTbl.Rows.Clear()
                eiSearchProvNSTbl.Rows.Clear()
                eiSearchProvORTbl.Rows.Clear()
                eiSearchProvOtherTbl.Rows.Clear()
        End Select


    End Sub




    Sub ShowProvidersByName()

        Dim objSqlCmd As New OdbcCommand
        Dim objSqlDataRdr As OdbcDataReader = Nothing
        Dim strConnection As String = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;" 'ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()
        Dim objConnection As New OdbcConnection(strConnection)
        Dim decLat As Decimal = 0
        Dim decLng As Decimal = 0
        Dim iRowCount As Integer = 0

        noNameEnteredError.Visible = False

        If String.IsNullOrEmpty(eiSearchFNameTxt.Text) = False Or String.IsNullOrEmpty(eiSearchLNameTxt.Text) = False Then

            objConnection.Open()

            objSqlCmd.Connection = objConnection

            objSqlCmd.CommandText = "SELECT CONCAT(HP.PERSON_FIRST_NAME,' ',HP.PERSON_LAST_NAME) 'NAME' , HL.ADDRESS1 'ADDRESS', HL.CITY 'CITY', HL.STATE 'STATE', HL.POSTAL_CODE 'ZIP', HCA.CUST_ACCOUNT_ID 'ID', SP.SPECIALTY_NAME 'SPECIALTY' " & _
                                    "FROM HZ_PARTIES HP,  " & _
                                    "     HZ_CUST_ACCOUNTS HCA, " & _
                                    "     HZ_CUST_ACCT_SITES_ALL HCASA, " & _
                                    "     HZ_PARTY_SITES HPS, " & _
                                    "     HZ_LOCATIONS HL, " & _
                                    "     MEWW_ZIP_CODE ZIP, " & _
                                    "     MEWW_SPECIALTIES SP " & _
                                    "WHERE HP.PARTY_ID = HCA.PARTY_ID AND  " & _
                                    "      HCA.STATUS = 'A' AND  " & _
                                    "      HL.LOCATION_ID = HPS.LOCATION_ID AND " & _
                                    "      HPS.PARTY_SITE_ID = HCASA.PARTY_SITE_ID AND " & _
                                    "      HCASA.CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID AND " & _
                                    "      HL.POSTAL_CODE = ZIP.ZIP_CODE AND  " & _
                                    "      HP.PARTY_TYPE = 'PERSON' AND " & _
                                    "      HP.ATTRIBUTE1 = SP.SPECIALTY_ABR AND " & _
                                    "      HP.ATTRIBUTE_CATEGORY = 'PHYSICIAN NETWORK' AND " & _
                                    "      HP.PERSON_FIRST_NAME LIKE '" & eiSearchFNameTxt.Text.ToUpper & "%' AND " & _
                                    "      HP.PERSON_LAST_NAME LIKE '" & eiSearchLNameTxt.Text.ToUpper & "%'" & _
                                    " ORDER BY SP.SPECIALTY_NAME, HP.PERSON_LAST_NAME, HP.PERSON_FIRST_NAME, HP.ADDRESS1, HP.ADDRESS2 "
            '" ORDER BY HP.ATTRIBUTE1, HP.PERSON_LAST_NAME, HP.PERSON_FIRST_NAME, HP.ADDRESS1, HP.ADDRESS2 "

            '"      HP.ATTRIBUTE1 = SP.SPECIALTY_ABR AND " & _
            '"     MEWW_SPECIALTIES SP " & _
            '" ORDER BY SP.SPECIALTY_NAME, HP.PERSON_LAST_NAME, HP.PERSON_FIRST_NAME, HP.ADDRESS1, HP.ADDRESS2 "
            objSqlCmd.CommandType = Data.CommandType.Text
            objSqlDataRdr = objSqlCmd.ExecuteReader()

            iRowCount = 0

            eiSearchProvidersByNameTP.HeaderText = "Search Results for: " & Trim(eiSearchFNameTxt.Text & " " & eiSearchLNameTxt.Text)

            While objSqlDataRdr.Read

                Dim tblHeaderRow As New TableHeaderRow
                Dim tblRow As New TableRow
                Dim cellHdrChkbx As New TableHeaderCell
                Dim cellHdrName As New TableHeaderCell
                Dim cellHdrAddress As New TableHeaderCell
                Dim cellHdrCity As New TableHeaderCell
                Dim cellHdrState As New TableHeaderCell
                Dim cellHdrZip As New TableHeaderCell
                Dim lblHdrNameValue As New Label
                Dim lblHdrAddressValue As New Label
                Dim lblHdrCityValue As New Label
                Dim lblHdrStateValue As New Label
                Dim lblHdrZipValue As New Label
                Dim chkbxHdrProv As New Label
                Dim cellChkbx As New TableCell
                Dim cellName As New TableCell
                Dim cellAddress As New TableCell
                Dim cellCity As New TableCell
                Dim cellState As New TableCell
                Dim cellZip As New TableCell
                Dim lblNameValue As New Label
                Dim lblAddressValue As New Label
                Dim lblCityValue As New Label
                Dim lblStateValue As New Label
                Dim lblZipValue As New Label
                Dim chkbxProv As New CheckBox

                If iRowCount = 0 Then

                    lblHdrNameValue.Text = "Physician Name"
                    'lblHdrNameValue.Font.Size = FontUnit.Small
                    'lblHdrNameValue.Font.Bold = True
                    cellHdrName.Width = 100
                    'cellHdrName.HorizontalAlign = HorizontalAlign.Center
                    'cellHdrName.BackColor = Drawing.Color.Black
                    'cellHdrName.ForeColor = Drawing.Color.White
                    cellHdrName.Controls.Add(lblHdrNameValue)

                    lblHdrAddressValue.Text = "Address"
                    'lblHdrAddressValue.Font.Size = FontUnit.Small
                    'lblHdrAddressValue.Font.Bold = True
                    cellHdrAddress.Width = 100
                    'cellHdrAddress.HorizontalAlign = HorizontalAlign.Center
                    'cellHdrAddress.BackColor = Drawing.Color.Black
                    'cellHdrAddress.ForeColor = Drawing.Color.White
                    cellHdrAddress.Controls.Add(lblHdrAddressValue)

                    lblHdrCityValue.Text = "City"
                    'lblHdrCityValue.Font.Size = FontUnit.Small
                    'lblHdrCityValue.Font.Bold = True
                    'cellHdrCity.Width = 75
                    'cellHdrCity.HorizontalAlign = HorizontalAlign.Center
                    'cellHdrCity.BackColor = Drawing.Color.Black
                    'cellHdrCity.ForeColor = Drawing.Color.White
                    cellHdrCity.Controls.Add(lblHdrCityValue)

                    lblHdrStateValue.Text = "State"
                    'lblHdrStateValue.Font.Size = FontUnit.Small
                    'lblHdrStateValue.Font.Bold = True
                    cellHdrState.Width = 20
                    'cellHdrState.HorizontalAlign = HorizontalAlign.Center
                    'cellHdrState.BackColor = Drawing.Color.Black
                    'cellHdrState.ForeColor = Drawing.Color.White
                    cellHdrState.Controls.Add(lblHdrStateValue)

                    lblHdrZipValue.Text = "Zip"
                    'lblHdrZipValue.Font.Size = FontUnit.Small
                    'lblHdrZipValue.Font.Bold = True
                    cellHdrZip.Width = 30
                    'cellHdrZip.HorizontalAlign = HorizontalAlign.Center
                    'cellHdrZip.BackColor = Drawing.Color.Black
                    'cellHdrZip.ForeColor = Drawing.Color.White
                    cellHdrZip.Controls.Add(lblHdrZipValue)

                    tblHeaderRow.Cells.Add(cellHdrName)
                    tblHeaderRow.Cells.Add(cellHdrAddress)
                    tblHeaderRow.Cells.Add(cellHdrCity)
                    tblHeaderRow.Cells.Add(cellHdrState)
                    tblHeaderRow.Cells.Add(cellHdrZip)


                    Dim cellHdrSpecialty As New TableHeaderCell
                    Dim lblHdrSpecialtyValue As New Label
                    lblHdrSpecialtyValue.Text = "Specialty"
                    'lblHdrSpecialtyValue.Font.Size = FontUnit.Small
                    'lblHdrSpecialtyValue.Font.Bold = True
                    cellHdrSpecialty.Width = 100
                    'cellHdrSpecialty.BackColor = Drawing.Color.Black
                    'cellHdrSpecialty.ForeColor = Drawing.Color.White
                    'cellHdrSpecialty.HorizontalAlign = HorizontalAlign.Center
                    cellHdrSpecialty.Controls.Add(lblHdrSpecialtyValue)
                    tblHeaderRow.Cells.Add(cellHdrSpecialty)

                    eiSearchProvByNameTbl.Rows.Add(tblHeaderRow)


                End If

                iRowCount = iRowCount + 1


                lblNameValue.Text = objSqlDataRdr.Item(0)
                cellName.Width = 100
                cellName.HorizontalAlign = HorizontalAlign.Left
                cellName.Controls.Add(lblNameValue)

                'commented this out betcause Darlene doesn't want the address to be listed, just Call 1-800-662-2400
                'lblAddressValue.Text = objSqlDataRdr.Item(1)
                lblAddressValue.Text = "Call 1-800-662-2400"
                cellAddress.Width = 150
                cellAddress.HorizontalAlign = HorizontalAlign.Center
                cellAddress.Font.Bold = True
                cellAddress.Controls.Add(lblAddressValue)

                lblCityValue.Text = objSqlDataRdr.Item(2)
                cellCity.Width = 75
                cellCity.HorizontalAlign = HorizontalAlign.Left
                cellCity.Controls.Add(lblCityValue)

                lblStateValue.Text = objSqlDataRdr.Item(3)
                cellState.Width = 20
                cellState.HorizontalAlign = HorizontalAlign.Center
                cellState.Controls.Add(lblStateValue)

                lblZipValue.Text = objSqlDataRdr.Item(4)
                cellZip.Width = 30
                cellZip.HorizontalAlign = HorizontalAlign.Center
                cellZip.Controls.Add(lblZipValue)

                tblRow.Cells.Add(cellName)
                tblRow.Cells.Add(cellAddress)
                tblRow.Cells.Add(cellCity)
                tblRow.Cells.Add(cellState)
                tblRow.Cells.Add(cellZip)


                Dim tblCell, tblCell2, tblCell3 As New TableCell()
                If iRowCount Mod 2 = 0 Then
                    tblRow.BackColor = Color.FromArgb(&H98C6E7)
                Else
                    tblRow.BackColor = Drawing.Color.White
                End If

                Dim cellSpecialty As New TableCell
                Dim lblSpecialtyValue As New Label
                lblSpecialtyValue.Text = objSqlDataRdr.Item(6)
                cellSpecialty.Width = 20
                cellSpecialty.HorizontalAlign = HorizontalAlign.Center
                cellSpecialty.Controls.Add(lblSpecialtyValue)
                tblRow.Cells.Add(cellSpecialty)

                eiSearchProvByNameTbl.Rows.Add(tblRow)

            End While

            If iRowCount = 0 Then

                Dim tblRow As New TableRow
                Dim cellNoResults As New TableCell
                Dim lblNoResults As New Label

                lblNoResults.Text = "No Providers Found"
                'cellNoResults.ColumnSpan
                cellNoResults.HorizontalAlign = HorizontalAlign.Left
                cellNoResults.BackColor = Drawing.Color.LightYellow
                cellNoResults.Controls.Add(lblNoResults)
                tblRow.Controls.Add(cellNoResults)

                eiSearchProvByNameTbl.Rows.Add(tblRow)


            End If

            objSqlDataRdr.Close()

            eiSearchFiledsPanel.Visible = False
            searchProvidersByNameResultsPanel.Visible = True
            eiSearchByNameSuccessful.Value = 1

        Else
            'show error to state that they need to enter a zip
            noNameEnteredError.Visible = True
        End If

        ' get insurance carrier for the location chosen

        'Catch ex As Exception
        'Response.Write("Can't load Web page-" & ex.Message)
        'Finally
        ' objSqlDataRdr.Close()
        'objSqlDataRdr.Dispose()
        objConnection.Close()
        objConnection.Dispose()
        'End Try

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete



    End Sub
End Class
