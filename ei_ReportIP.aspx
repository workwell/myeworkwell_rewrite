﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/meww_no_float.master" CodeFile="ei_ReportIP.aspx.cs" Inherits="ei_ReportIP" EnableEventValidation="false" MaintainScrollPositionOnPostBack="true" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="eiViewPT" ContentPlaceHolderID="contentHldr" Runat="Server">

    <style type="text/css"> 
            html body form .RadInput .riTextBox, 
            .RadPicker .RadInput .riTextBox 
            { 
                background-color: #e4f1f7; 
                font-family:Calibri; 
                font-size:12px;
            } 
            .RadGrid .rgHoveredRow
             {
               background:LightBlue !important;
             }
    </style>
    <script type="text/javascript">
        function scrollTop() {
            window.document.body.scrollTop = 0;
            window.document.documentElement.scrollTop = 0;
        }
    </script>

    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
		<Scripts>
			<%--Needed for JavaScript IntelliSense in VS2010--%>
			<%--For VS2008 replace RadScriptManager with ScriptManager--%>
			<asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
			<asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
			<asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
		</Scripts>
	</telerik:RadScriptManager>

	<%--<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" RestoreOriginalRenderDelegate="false">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="uiCEUsOffered">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="nothing"/>                       
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnSave">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="insurance_info_next_btn" />
                    </UpdatedControls>
                </telerik:AjaxSetting>                
            </AjaxSettings>
	</telerik:RadAjaxManager>--%>
    
    <asp:Panel ID="nothing" runat="server">
    
        <asp:Panel ID="employee_search_pnl" runat="server" Visible="True" Width="860px"> 
           
                <div class="appHeader"> 
                    <!--creates the top edge-->
                    <div class="appHeaderTop"> <img src="images/sectionTopLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionTopRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                    <!--the mid section-->
                    <div class="sectionTitle"> <img src="images/sectionMidLeft.jpg" width="8" height="15" alt="" class="sectionLeftCorner" />
                                <h3>Employee Search</h3>
                            </div>
                    <!--the bottom-->
        
                    <div class="appHeaderBottom"> <img src="images/sectionBottomLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionBottomRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                </div>

                
                 <div class="shortAppForm">   
                    <asp:Table ID="employee_search_tbl" runat="server" Width="860px">
                        <asp:TableHeaderRow><asp:TableCell HorizontalAlign="Left">First Name</asp:TableCell><asp:TableCell HorizontalAlign="Left">Last Name</asp:TableCell><asp:TableCell HorizontalAlign="Left">Social Security #</asp:TableCell></asp:TableHeaderRow>                                              
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="left"><asp:TextBox ID="srch_ip_fname_txt" runat="server" Width="200"  class="txtbox" ></asp:TextBox></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="left"><asp:TextBox ID="srch_ip_lname_txt"  runat="server" Width="200"  class="txtbox" ></asp:TextBox></asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="left"><asp:TextBox  runat="Server" ID="srch_ssn1_txt"  Width="30px" class="txtbox" MaxLength="3"></asp:TextBox>-<asp:TextBox  runat="Server" ID="srch_ssn2_txt"  Width="30px" class="txtbox" MaxLength="2"></asp:TextBox>-<asp:TextBox  runat="Server" ID="srch_ssn3_txt"  Width="40px" class="txtbox"  MaxLength="4"></asp:TextBox></asp:TableCell>
                            <asp:TableCell Wrap="false" HorizontalAlign="left">
                                <asp:ImageButton ID="new_ip_btn" runat="server" ImageUrl="~/Images/meww_new_emp_btn.png" OnClick="add_new_ip" CssClass="smallappBtnGrd"/>
                                <asp:ImageButton ID="clear_ip_srch_btn" ImageUrl="~/Images/meww_clear_btn.png" OnClick="clear_ip_srch_fields" runat="server" CssClass="smallappBtnGrd"/>
                                <asp:ImageButton ID="ip_search_btn" ImageUrl="~/Images/meww_search_btn.png"  OnClick="run_ip_search" runat="server" CssClass="smallappBtnGrd" />
                            </asp:TableCell>                                
                        </asp:TableRow>
                        <asp:TableRow HorizontalAlign="Right">
                            <asp:TableCell Wrap="false">&nbsp;</asp:TableCell>                    
                        </asp:TableRow>                                   
                    </asp:Table>   
                    <!-- No idea how this didn't create on them on the skin="webblue"  -->
                    <telerik:RadGrid ID="ip_name_serch_grd"
                        runat="server"
                        AllowPaging="true"
                        PageSize="20"
                        Width="860px"
                        Font-Names="Calibri"
                        Skin="WebBlue"                        
                        EnableEmbeddedSkins="false"
                        AllowMultiRowSelection="false"
		                AutoGenerateColumns="False"
                        OnItemDataBound="ip_name_serch_grd_ItemDataBound"
                        OnPageIndexChanged="ip_name_serch_grd_PageIndexChanged"
                        OnItemCommand="ip_name_serch_grd_ItemCommand"
                        PagerStyle-Mode="NumericPages"
                        > 
                         
                        <MasterTableView DataKeyNames="PARTY_ID"  
                            EditMode="EditForms"
			                CommandItemDisplay="None"
                            Width="860px">
		                    <Columns>

                    
			                    <telerik:GridBoundColumn DataField="PARTY_NAME" DataType="System.String" 
						                    HeaderText="Employee Name" UniqueName="PARTY_NAME" ItemStyle-Width="150px" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center">
			                    </telerik:GridBoundColumn>	
		
			                    <telerik:GridBoundColumn DataField="ADDRESS1" DataType="System.String" 
						                    HeaderText="Address" UniqueName="ADDRESS1" ItemStyle-Width="275px" HeaderStyle-Width="275px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center">
			                    </telerik:GridBoundColumn>	

                                <telerik:GridBoundColumn DataField="JGZZ_FISCAL_CODE" DataType="System.String" 
						                    HeaderText="SS#" UniqueName="JGZZ_FISCAL_CODE" ItemStyle-Width="60px" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
			                    </telerik:GridBoundColumn>	                    			        
					                            
                                <telerik:GridBoundColumn DataField="DATE_OF_BIRTH" DataType="System.String" 
						                    HeaderText="Date of Birth" UniqueName="DATE_OF_BIRTH" ItemStyle-Width="70px" HeaderStyle-Width="70px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
			                    </telerik:GridBoundColumn>	      
                         
                                <telerik:GridTemplateColumn UniqueName="contact_edit" ItemStyle-Width="40px" HeaderStyle-Width="40px" HeaderText="SELECT" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"> 
                                        <ItemTemplate>
                                            <asp:ImageButton id="edit_contact_btn" ImageUrl="~/Images/select_check_mark.png" CommandArgument='<%# Eval("PARTY_ID") %>' runat="server" CommandName="select_ip"/>
                                        </ItemTemplate>
                                </telerik:GridTemplateColumn>                    

                                <telerik:GridBoundColumn DataField="PARTY_ID" DataType="System.String"  ItemStyle-Width="140px" HeaderStyle-Width="140px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
						                    HeaderText="PARTY_ID" UniqueName="PARTY_ID" Visible="false">						
			                    </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataField="ADDRESS2" DataType="System.String" 
						                    HeaderText="Address2" UniqueName="ADDRESS2" ItemStyle-Width="275px" HeaderStyle-Width="275px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" Visible="false">
			                    </telerik:GridBoundColumn>	

                                <telerik:GridBoundColumn DataField="CITY" DataType="System.String" 
						                    HeaderText="CITY" UniqueName="CITY" ItemStyle-Width="275px" HeaderStyle-Width="275px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" Visible="false">
			                    </telerik:GridBoundColumn>	

                                <telerik:GridBoundColumn DataField="STATE" DataType="System.String" 
						                    HeaderText="STATE" UniqueName="STATE" ItemStyle-Width="275px" HeaderStyle-Width="275px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" Visible="false">
			                    </telerik:GridBoundColumn>	

                                <telerik:GridBoundColumn DataField="POSTAL_CODE" DataType="System.String" 
						                    HeaderText="POSTAL_CODE" UniqueName="POSTAL_CODE" ItemStyle-Width="275px" HeaderStyle-Width="275px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" Visible="false">
			                    </telerik:GridBoundColumn>	
                    
		                    </Columns>		  	
							
	                    </MasterTableView>
                        <ClientSettings EnableRowHoverStyle="true"> 
                        </ClientSettings>
                    </telerik:RadGrid>
            </div>           
        </asp:Panel>

        <asp:Panel ID="injury_report_pnl" runat="server" Visible="false">
            <telerik:RadPanelBar runat="server" ID="report_injury_pnl_bar" ExpandMode="MultipleExpandedItems" Width="100%" >
                    <Items>
                        <telerik:RadPanelItem Enabled="true" Selected="true" Text="Step 1: Patient Information" runat="server" Expanded="True" Value="patient_info_pnl_item_prnt">
                            <Items>
                                <telerik:RadPanelItem Value="patient_info_pnl_item" runat="server">
                                    <ItemTemplate>
                                         <div class="shortAppForm">
                                            <asp:Table ID="patientInfoTbl" runat="server" >
                                                <asp:TableRow>
                                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                                </asp:TableRow> 
                                                <asp:TableRow>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="200px" >First Name:&nbsp;</asp:TableCell>
                                                    <asp:TableCell ColumnSpan="5"><asp:TextBox ID="ip_fname_txt" runat="server" Width="150" class="txtbox" BackColor="#e4f1f7"></asp:TextBox>&nbsp;&nbsp;
                                                        <asp:RequiredFieldValidator runat="server" ID="ip_fname_rf_validator" 
                                                            ControlToValidate="ip_fname_txt" 
                                                            Display="Dynamic" ErrorMessage="* Required!" 
                                                            ValidationGroup="patient_info_validation" CssClass="validator"
                                                            BackColor="LightBlue" Font-Italic="true">
                                                        </asp:RequiredFieldValidator>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right" >Last Name:&nbsp;</asp:TableCell>
                                                    <asp:TableCell ColumnSpan="5"><asp:TextBox ID="ip_lname_txt" runat="server" Width="150" class="txtbox" BackColor="#e4f1f7"></asp:TextBox>&nbsp;&nbsp;
                                                        <asp:RequiredFieldValidator runat="server" ID="ip_lname_rf_validator" 
                                                            ControlToValidate="ip_lname_txt" 
                                                            Display="Dynamic" ErrorMessage="* Required!" 
                                                            ValidationGroup="patient_info_validation" CssClass="validator"
                                                            BackColor="LightBlue" Font-Italic="true">
                                                        </asp:RequiredFieldValidator>&nbsp;&nbsp;Suffix:&nbsp;<asp:TextBox ID="ip_suffix_txt" runat="server" Width="25" class="txtbox"></asp:TextBox>
                                                    </asp:TableCell>    
                                                </asp:TableRow>

                                                <asp:TableRow>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right" >Gender:&nbsp;</asp:TableCell>
                                                    <asp:TableCell ColumnSpan="5">
                                                        <asp:DropDownList ID="gender_txt" runat="server" AutoPostBack="true"> 
                                                            <asp:ListItem Text="---" Value="---" Selected="False"></asp:ListItem>                                                          
                                                            <asp:ListItem Text="Male" Value="M" Selected="False"></asp:ListItem>
                                                            <asp:ListItem Text="Female" Value="F" Selected="False"></asp:ListItem>
                                                        </asp:DropDownList>&nbsp;&nbsp;***NEW FIELD*** 
                                                        <asp:RequiredFieldValidator ID="gender_txt_validator" runat="server"  Display="Dynamic" 
                                                            ControlToValidate="gender_txt" InitialValue="---" ErrorMessage="* Required!" 
                                                            ValidationGroup="patient_info_validation" CssClass="validator">
                                                        </asp:RequiredFieldValidator>                                                       
                                                    </asp:TableCell>
                                                    
                                                </asp:TableRow>

                                                <asp:TableRow>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right" >Occupation:&nbsp;</asp:TableCell>
                                                    <asp:TableCell ColumnSpan="5"><asp:TextBox ID="ip_occupation_txt" runat="server" Width="150" class="txtbox" BackColor="#e4f1f7"></asp:TextBox>&nbsp;&nbsp;
                                                        <asp:RequiredFieldValidator runat="server" ID="ip_occupation_txt_rf_validator" 
                                                            ControlToValidate="ip_occupation_txt" 
                                                            Display="Dynamic" ErrorMessage="* Required!" 
                                                            ValidationGroup="patient_info_validation" CssClass="validator"
                                                            BackColor="LightBlue" Font-Italic="true">
                                                        </asp:RequiredFieldValidator>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Right">Address:&nbsp;</asp:TableCell>
                                                    <asp:TableCell ColumnSpan="5"><asp:TextBox ID="ip_address_txt" runat="server" Width="295"  class="txtbox"></asp:TextBox></asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                                    <asp:TableCell ColumnSpan="5"><asp:TextBox ID="ip_address2_txt" runat="server" Width="295" class="txtbox"></asp:TextBox></asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Right">City:&nbsp;</asp:TableCell>
                                                    <asp:TableCell Width="125"><asp:TextBox ID="ip_city_txt" runat="server" class="txtbox"></asp:TextBox></asp:TableCell>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="55">State:&nbsp;</asp:TableCell>
                                                    <asp:TableCell Width="50"><asp:TextBox ID="ip_state_txt" runat="server" MaxLength="2" Width="20" class="txtbox"></asp:TextBox></asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Right" Width="40">Zip:&nbsp;</asp:TableCell>
                                                    <asp:TableCell><asp:TextBox ID="ip_zip_txt" runat="server" MaxLength="10" Width="70" class="txtbox"></asp:TextBox></asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow >
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">Phone Number:&nbsp;</asp:TableCell>
                                                    <asp:TableCell Wrap="false" ><asp:TextBox ID="ip_area_code_txt" runat="server" Width="30" class="txtbox" MaxLength="3"></asp:TextBox>-<asp:TextBox ID="ip_phone_num_txt" runat="server" Width="70" class="txtbox" MaxLength="8"></asp:TextBox></asp:TableCell>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">ext:&nbsp;</asp:TableCell>
                                                    <asp:TableCell Wrap="false" ><asp:TextBox ID="ip_ext_txt" runat="server" Width="40" class="txtbox"></asp:TextBox></asp:TableCell>
                                                </asp:TableRow>  
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Right">Date Of Birth:&nbsp;</asp:TableCell>
                                                    <asp:TableCell ColumnSpan="2"><asp:TextBox ID="ip_dob_txt" runat="server" Width="60" MaxLength="10" class="txtbox"></asp:TextBox></asp:TableCell>
                                                </asp:TableRow>                           
                                                <asp:TableRow >
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">Social Security Number:&nbsp;</asp:TableCell>
                                                    <asp:TableCell Wrap="False" ColumnSpan="2"><asp:TextBox  runat="Server" ID="ip_ssn1_txt"  Width="30px" class="txtbox"></asp:TextBox>-<asp:TextBox  runat="Server" ID="ip_ssn2_txt"  Width="30px" class="txtbox"></asp:TextBox>-<asp:TextBox  runat="Server" ID="ip_ssn3_txt"  Width="40px" class="txtbox"></asp:TextBox></asp:TableCell>
                            
                                                </asp:TableRow>  
                                                <asp:TableRow>
                                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                                </asp:TableRow>     
                                                <asp:TableRow ID="patient_info_btn_row" runat="server">
                                                   <asp:TableCell Wrap="false" ColumnSpan="6" HorizontalAlign="Right" >
                                                       <asp:ImageButton ID="patient_info_clear_btn" runat="server" ImageUrl="~/images/meww_cancel_btn.png" OnClick="cancel_injury_report" CssClass="smallappBtn"/>
                                                       <asp:ImageButton ID="patient_info_next_btn" runat="server" ImageUrl="~/images/meww_next_btn.png" OnClick="go_to_next_step" CommandName="patient_info" CssClass="smallappBtn" ValidationGroup="patient_info_validation"/>
                                                   </asp:TableCell>
                                                </asp:TableRow> 
                                            </asp:Table>                                            
                                        </div>
                                    </ItemTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelItem>
                        <telerik:RadPanelItem Enabled="False" Text="Step 2: Employer Information" runat="server" Value="employer_info_pnl_item_prnt">
                            <Items>
                                <telerik:RadPanelItem Value="employer_info_pnl_item" runat="server">
                                    <ItemTemplate>      
                                        <div class="shortAppForm">       

                                            <asp:Table ID="loc_address_info_tbl" runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                                </asp:TableRow> 
                                                <asp:TableRow>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right" >Location Name:&nbsp;</asp:TableCell>
                                                    <asp:TableCell ColumnSpan="5" ><asp:DropDownList id="employer_loc_drp" 
                                                            AutoPostBack="True"
                                                            Width="400px"
                                                            BackColor="#e4f1f7"
                                                            OnSelectedIndexChanged="employer_loc_drp_chgd"                                                           
                                                            runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False"></asp:DropDownList>
                                                    </asp:TableCell>
                                                </asp:TableRow> 
                                                <asp:TableRow ID="new_loc_name_row" runat="server" Visible="false">
		                                            <asp:TableCell Width="200px" Wrap="false" HorizontalAlign="Right">New Location Name:</asp:TableCell>
		                                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="5"><asp:TextBox ID="loc_new_loc_name_txt" Width="300px" runat="server" CssClass="txtbox" BackColor="#e4f1f7"></asp:TextBox>&nbsp;&nbsp;
                                                                                                                      <asp:RequiredFieldValidator runat="server" ID="loc_new_loc_name_txt_rf_validator" 
                                                                                                                            ControlToValidate="loc_new_loc_name_txt" 
                                                                                                                            Display="Dynamic" ErrorMessage="* Required!" 
                                                                                                                            ValidationGroup="employer_info_validation" CssClass="validator"
                                                                                                                            BackColor="LightBlue" Font-Italic="true" Enabled="false">
                                                                                                                      </asp:RequiredFieldValidator></asp:TableCell>
	                                            </asp:TableRow>
	                                            <asp:TableRow>
		                                            <asp:TableCell Width="200px" Wrap="false" HorizontalAlign="Right">Address:</asp:TableCell>
		                                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="5"><asp:TextBox ID="loc_address_txt" Width="300px" runat="server" CssClass="txtbox" BackColor="#e4f1f7"></asp:TextBox>&nbsp;&nbsp;
                                                                                                                      <asp:RequiredFieldValidator runat="server" ID="loc_address_rf_validator" 
                                                                                                                            ControlToValidate="loc_address_txt" 
                                                                                                                            Display="Dynamic" ErrorMessage="* Required!" 
                                                                                                                            ValidationGroup="employer_info_validation" CssClass="validator"
                                                                                                                            BackColor="LightBlue" Font-Italic="true">
                                                                                                                      </asp:RequiredFieldValidator></asp:TableCell>
	                                            </asp:TableRow>
	                                            <asp:TableRow>
		                                            <asp:TableCell Width="200px" Wrap="false" HorizontalAlign="Right">&nbsp;</asp:TableCell>
		                                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="5"><asp:TextBox ID="loc_address2_txt" Width="300px" runat="server" CssClass="txtbox"></asp:TextBox></asp:TableCell>
	                                            </asp:TableRow>	                                           
	                                            <asp:TableRow>
		                                            <asp:TableCell Width="200px" Wrap="false" HorizontalAlign="Right">City:</asp:TableCell>
		                                            <asp:TableCell Width="50px" Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="loc_city_txt" Width="150px" runat="server" CssClass="txtbox" BackColor="#e4f1f7"></asp:TextBox>&nbsp;&nbsp;
                                                                                                                      <asp:RequiredFieldValidator runat="server" ID="loc_city_rf_validator" 
                                                                                                                            ControlToValidate="loc_city_txt" 
                                                                                                                            Display="Dynamic" ErrorMessage="* Required!" 
                                                                                                                            ValidationGroup="employer_info_validation" CssClass="validator"
                                                                                                                            BackColor="LightBlue" Font-Italic="true">
                                                                                                                      </asp:RequiredFieldValidator></asp:TableCell>
		                                            <asp:TableCell Width="50px" Wrap="false" HorizontalAlign="Right">State:</asp:TableCell>
		                                            <asp:TableCell Width="50px" Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="loc_state_txt" Width="20px" runat="server" CssClass="txtbox" BackColor="#e4f1f7"></asp:TextBox>&nbsp;&nbsp;
                                                                                                                      <asp:RequiredFieldValidator runat="server" ID="loc_state_rf_validator" 
                                                                                                                            ControlToValidate="loc_state_txt"
                                                                                                                            Display="Dynamic" ErrorMessage="* Required!" 
                                                                                                                            ValidationGroup="employer_info_validation" CssClass="validator"
                                                                                                                            BackColor="LightBlue" Font-Italic="true">
                                                                                                                      </asp:RequiredFieldValidator></asp:TableCell>
		                                            <asp:TableCell Width="50px" Wrap="false" HorizontalAlign="Right">Zip:</asp:TableCell>
		                                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="loc_zip_txt" Width="60px" runat="server" CssClass="txtbox" BackColor="#e4f1f7"></asp:TextBox>&nbsp;&nbsp;
                                                                                                                      <asp:RequiredFieldValidator runat="server" ID="loc_zip_rf_validator" 
                                                                                                                            ControlToValidate="loc_zip_txt" 
                                                                                                                            Display="Dynamic" ErrorMessage="* Required!" 
                                                                                                                            ValidationGroup="employer_info_validation" CssClass="validator"
                                                                                                                            BackColor="LightBlue" Font-Italic="true">
                                                                                                                      </asp:RequiredFieldValidator></asp:TableCell>
	                                            </asp:TableRow>   
                                                <asp:TableRow>
                                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                                </asp:TableRow>                                              
                                                <asp:TableRow ID="employer_info_btn_row" runat="server">
                                                   <asp:TableCell Wrap="false" ColumnSpan="6" HorizontalAlign="Right" >
                                                       <asp:ImageButton ID="employer_info_clear_btn" runat="server" ImageUrl="~/images/meww_cancel_btn.png" OnClick="cancel_injury_report" CssClass="smallappBtn"/>
                                                       <asp:ImageButton ID="employer_info_next_btn" runat="server" ImageUrl="~/images/meww_next_btn.png" OnClick="go_to_next_step" CommandName="patient_info" CssClass="smallappBtn" ValidationGroup="employer_info_validation"/>
                                                   </asp:TableCell>
                                                </asp:TableRow>                                                
                                            </asp:Table>                                            
                                        </div>      
                                    </ItemTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelItem>
                        <telerik:RadPanelItem Enabled="False" Text="Step 3: Insurance Information" runat="server" Value="ins_carrier_pnl_item_prnt">
                            <Items>
                                <telerik:RadPanelItem Value="insurance_pnl_item" runat="server">
                                    <ItemTemplate>
                                            <asp:Table ID="loc_ins_carrier_info_tbl" runat="server"> 
                                                <asp:TableRow>
                                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                                </asp:TableRow>                                                
                                                <asp:TableRow>
                                                    <asp:TableCell Width="200px" Wrap="false" HorizontalAlign="Right"></asp:TableCell>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Left">&nbsp;</asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Width="200px" Wrap="false" HorizontalAlign="Right" ForeColor="Black">Insurance Carrier:</asp:TableCell>
                                                    <asp:TableCell ColumnSpan="5" ><asp:DropDownList id="ins_carrier_loc_drp" 
                                                            AutoPostBack="True"
                                                            Width="450px"
                                                            BackColor="#e4f1f7"
                                                            OnSelectedIndexChanged="ins_carrier_drp_chgd"                                                           
                                                            runat="server" 
                                                            Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" 
                                                            Font-Italic="False" Font-Size="X-Small" 
                                                            Font-Bold="False"
                                                            ></asp:DropDownList>
                                                    </asp:TableCell>
                                                </asp:TableRow>                                                                                                
                                            </asp:Table> 
                                            <asp:Table ID="new_loc_ins_tbl" runat="server" Width="800" Visible="false">
                                                <asp:TableRow >
		                                            <asp:TableCell Width="200px" Wrap="false" HorizontalAlign="Right">New Carrier Name:</asp:TableCell>
		                                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="5"><asp:TextBox ID="new_ins_name_txt" Width="300px" runat="server" CssClass="txtbox" BackColor="#e4f1f7"></asp:TextBox>&nbsp;&nbsp;
                                                                                                                      <asp:RequiredFieldValidator runat="server" ID="new_ins_name_txt_rf_validator" 
                                                                                                                            ControlToValidate="new_ins_name_txt" 
                                                                                                                            Display="Dynamic" ErrorMessage="* Required!" 
                                                                                                                            ValidationGroup="ins_info_validation" CssClass="validator"
                                                                                                                            BackColor="LightBlue" Font-Italic="true" Enabled="false">
                                                                                                                      </asp:RequiredFieldValidator></asp:TableCell>
	                                            </asp:TableRow>
	                                            <asp:TableRow>
		                                            <asp:TableCell Width="200px" Wrap="false" HorizontalAlign="Right">Address:</asp:TableCell>
		                                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="5"><asp:TextBox ID="new_ins_address_txt" Width="300px" runat="server" CssClass="txtbox"></asp:TextBox></asp:TableCell>
	                                            </asp:TableRow>
	                                            <asp:TableRow>
		                                            <asp:TableCell Width="200px" Wrap="false" HorizontalAlign="Right">&nbsp;</asp:TableCell>
		                                            <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="5"><asp:TextBox ID="new_ins_address2_txt" Width="300px" runat="server" CssClass="txtbox"></asp:TextBox></asp:TableCell>
	                                            </asp:TableRow>	                                           
	                                            <asp:TableRow>
		                                            <asp:TableCell Width="200px" Wrap="false" HorizontalAlign="Right">City:</asp:TableCell>
		                                            <asp:TableCell Width="50px" Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="new_ins_city_txt" Width="150px" runat="server" CssClass="txtbox"></asp:TextBox></asp:TableCell>
		                                            <asp:TableCell Width="50px" Wrap="false" HorizontalAlign="Right">State:</asp:TableCell>
		                                            <asp:TableCell Width="50px" Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="new_ins_state_txt" Width="20px" runat="server" CssClass="txtbox"></asp:TextBox></asp:TableCell>
		                                            <asp:TableCell Width="50px" Wrap="false" HorizontalAlign="Right">Zip:</asp:TableCell>
		                                            <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="new_ins_zip_txt" Width="60px" runat="server" CssClass="txtbox"></asp:TextBox></asp:TableCell>
	                                            </asp:TableRow>   
												<asp:TableRow >
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">Phone Number:&nbsp;</asp:TableCell>
                                                    <asp:TableCell Wrap="false" ><asp:TextBox ID="new_ins_area_code_txt" runat="server" Width="30" class="txtbox" MaxLength="3" BackColor="#e4f1f7"></asp:TextBox>-<asp:TextBox ID="new_ins_phone_num_txt" runat="server" Width="70" class="txtbox" MaxLength="8" BackColor="#e4f1f7"></asp:TextBox>&nbsp;&nbsp;
                                                        <asp:RequiredFieldValidator runat="server" ID="new_ins_phone_rf_validator" 
                                                            ControlToValidate="new_ins_phone_num_txt" 
                                                            Display="Dynamic" ErrorMessage="* Required!" 
                                                            ValidationGroup="ins_info_validation" CssClass="validator"
                                                            BackColor="LightBlue" Font-Italic="true" Enabled="false">
												        </asp:RequiredFieldValidator>
                                                    </asp:TableCell>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">ext:&nbsp;</asp:TableCell>
                                                    <asp:TableCell Wrap="false" ><asp:TextBox ID="new_ins_ext_txt" runat="server" Width="40" class="txtbox"></asp:TextBox>&nbsp;&nbsp;
																														
													</asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                            <asp:Table ID="loc_ins_btn_tbl" runat="server" Width="650">
                                                <asp:TableRow>
                                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                                </asp:TableRow> 
                                                <asp:TableRow ID="insurance_info_btn_row" runat="server">
                                                   <asp:TableCell Wrap="false" ColumnSpan="6" HorizontalAlign="Right" ><asp:ImageButton ID="insurance_info_clear_btn" runat="server" ImageUrl="~/images/meww_cancel_btn.png" OnClick="cancel_injury_report" CssClass="smallappBtn"/><asp:ImageButton ID="insurance_info_next_btn" runat="server" ImageUrl="~/images/meww_next_btn.png" OnClick="go_to_next_step" CommandName="patient_info" CssClass="smallappBtn" ValidationGroup="ins_info_validation"/></asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                    </ItemTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelItem>
                        <telerik:RadPanelItem Enabled="False" Text="Step 4: Injury Information" runat="server"  Value="injury_info_pnl_item_prnt">
                            <Items>
                                <telerik:RadPanelItem Value="injury_info_pnl_item" runat="server">
                                    <ItemTemplate>
                                       <asp:Table ID="injury_info_pnl" runat="server">
                                           <asp:TableRow>
                                                <asp:TableCell>&nbsp;</asp:TableCell>
                                            </asp:TableRow>
                                           <asp:TableRow>
                                                <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="200px">Claim Number:&nbsp;</asp:TableCell> 
                                                <asp:TableCell ColumnSpan="2">
                                                    <asp:TextBox ID="claim_number_txt" runat="server" Width="295"  class="txtbox" BackColor="#e4f1f7"></asp:TextBox>&nbsp;&nbsp;***NEW FIELD***
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" 
                                                        ControlToValidate="claim_number_txt" 
                                                        Display="Dynamic" ErrorMessage="* Required!" 
                                                        ValidationGroup="injury_info_validation" CssClass="validator"
                                                        BackColor="LightBlue" Font-Italic="true">
                                                    </asp:RequiredFieldValidator>
                                                </asp:TableCell>                   
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="200px">Injury Date:&nbsp;</asp:TableCell> 
                                                <asp:TableCell ColumnSpan="2"><telerik:RadDatePicker ID="doi_pkr" Width="100" runat="server"></telerik:RadDatePicker>&nbsp;&nbsp;
                                                    <asp:RequiredFieldValidator runat="server" ID="doi_pkr_rf_validator" 
                                                        ControlToValidate="doi_pkr" 
                                                        Display="Dynamic" ErrorMessage="* Required!" 
                                                        ValidationGroup="injury_info_validation" CssClass="validator"
                                                        BackColor="LightBlue" Font-Italic="true">
                                                    </asp:RequiredFieldValidator>
                                                </asp:TableCell>                   
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right">Body Part(s):&nbsp;</asp:TableCell>
                                                <asp:TableCell ColumnSpan="2"><asp:TextBox ID="body_part_txt" runat="server" Width="295"  class="txtbox" BackColor="#e4f1f7"></asp:TextBox>&nbsp;&nbsp;
                                                    <asp:RequiredFieldValidator runat="server" ID="body_part_txt_rf_validator" 
                                                        ControlToValidate="body_part_txt" 
                                                        Display="Dynamic" ErrorMessage="* Required!" 
                                                        ValidationGroup="injury_info_validation" CssClass="validator"
                                                        BackColor="LightBlue" Font-Italic="true">
                                                    </asp:RequiredFieldValidator>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Top">Injury Description:&nbsp;</asp:TableCell>
                                                <asp:TableCell ><asp:TextBox ID="injury_desc_txt" runat="server" class="txtbox" Columns="50" Rows="5" TextMode="MultiLine"  BackColor="#e4f1f7"></asp:TextBox>&nbsp;&nbsp;
                                                    <asp:RequiredFieldValidator runat="server" ID="injury_desc_txt_rf_validator" 
                                                        ControlToValidate="injury_desc_txt" 
                                                        Display="Dynamic" ErrorMessage="* Required!" 
                                                        ValidationGroup="injury_info_validation" CssClass="validator"
                                                        BackColor="LightBlue" Font-Italic="true">
                                                    </asp:RequiredFieldValidator>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell Wrap="false" HorizontalAlign="Right">Has Been Treated Already:&nbsp;</asp:TableCell>
                                                <asp:TableCell ColumnSpan="2"><asp:DropDownList ID="has_been_treated_drp" 
                                                            runat="server"
                                                            AutoPostBack="True"
                                                            OnSelectedIndexChanged="has_been_treated_drp_chgd"
                                                            Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Size="X-Small">
                                                        <asp:ListItem Value="Y">Yes</asp:ListItem>
                                                        <asp:ListItem selected="true" Value="N">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                         
                                        <asp:Panel ID="treated_by_info_tbl" runat="server" Visible="false">
                                            <asp:Table ID="treatedByTbl" runat="server" BorderStyle="None">
                                                <asp:TableRow>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="200px">Patient was treated at:&nbsp;</asp:TableCell>
                                                    <asp:TableCell ColumnSpan="8"><asp:DropDownList ID="was_treated_at_drp" 
                                                                runat="server" 
                                                                RepeatLayout="Flow" 
                                                                RepeatDirection="Horizontal"
                                                                Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Size="X-Small">
                                                            <asp:ListItem Value="ER">ER</asp:ListItem>
                                                            <asp:ListItem Value="CLINIC">Clinic</asp:ListItem>
                                                            <asp:ListItem Value="PHYSICIAN">Physician</asp:ListItem>
                                                            <asp:ListItem Value="OTHER">Other</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">Treating Doctor/Pratice:&nbsp;</asp:TableCell>
                                                    <asp:TableCell ColumnSpan="8">
                                                        <asp:TextBox ID="treating_dr_name_txt" runat="server" Width="300px" class="txtbox" BackColor="#e4f1f7"></asp:TextBox>&nbsp;&nbsp;
                                                            <asp:RequiredFieldValidator runat="server" ID="treating_dr_name_txt_rf_validator" 
                                                                ControlToValidate="treating_dr_name_txt" 
                                                                Display="Dynamic" ErrorMessage="* Required!" 
                                                                ValidationGroup="injury_info_validation" CssClass="validator"
                                                                Enabled="false"
                                                                BackColor="LightBlue" Font-Italic="true">
                                                            </asp:RequiredFieldValidator>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Right" ForeColor="Black" Width="134">Address:&nbsp;</asp:TableCell>
                                                    <asp:TableCell ColumnSpan="8"><asp:TextBox ID="treating_dr_address_txt" CssClass="txtbox" runat="server" Width="250"></asp:TextBox></asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Width="134">&nbsp;</asp:TableCell>
                                                    <asp:TableCell ColumnSpan="8"><asp:TextBox ID="treating_dr_address2_txt" CssClass="txtbox" runat="server" Width="250"></asp:TextBox></asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
						                            <asp:TableCell HorizontalAlign="Right">City:&nbsp;</asp:TableCell>
						                            <asp:TableCell HorizontalAlign="Left" ColumnSpan="3" Width="155"><asp:TextBox ID="treating_dr_city_txt" runat="server" class="txtbox"></asp:TextBox></asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Right" Width="50">State:&nbsp;</asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Left"  Width="35"><asp:TextBox ID="treating_dr_state_txt" runat="server" MaxLength="2" Width="20"  class="txtbox"></asp:TextBox></asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Right" Width="30">Zip:&nbsp;</asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Left"><asp:TextBox ID="treating_dr_zip_txt" runat="server"   MaxLength="10" Width="70" class="txtbox"></asp:TextBox></asp:TableCell>
					                            </asp:TableRow>
                                                <asp:TableRow  HorizontalAlign="Left">
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">Phone Number:&nbsp;</asp:TableCell>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Left" Width="30"><asp:TextBox ID="treating_dr_area_code_txt" runat="server" Width="30" class="txtbox" MaxLength="3"></asp:TextBox></asp:TableCell>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="center" Width="10">-</asp:TableCell>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="treating_dr_phone_txt" runat="server" Width="70" class="txtbox" MaxLength="8"></asp:TextBox></asp:TableCell>                                     
                                                </asp:TableRow>
                                            </asp:Table>
                                            <asp:Table ID="ipOtherTreatmentInfoTbl" runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right" Width="200px">Date of Treatment:&nbsp;</asp:TableCell>
                                                    <asp:TableCell ColumnSpan="2">
                                                        <telerik:RadDatePicker ID="dot_pkr" Width="100" runat="server"></telerik:RadDatePicker>
                                                            <asp:RequiredFieldValidator runat="server" ID="dot_pkr_rf_validator" 
                                                                ControlToValidate="dot_pkr" 
                                                                Display="Dynamic" ErrorMessage="* Required!" 
                                                                ValidationGroup="injury_info_validation" CssClass="validator"
                                                                BackColor="LightBlue" Font-Italic="true" Enabled="false">
                                                            </asp:RequiredFieldValidator>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">Date of Follow-up Appt:&nbsp;</asp:TableCell>
                                                    <asp:TableCell ColumnSpan="2">
                                                        <telerik:RadDatePicker ID="fu_date_pkr" Width="100" runat="server"></telerik:RadDatePicker>
                                                            <asp:RequiredFieldValidator runat="server" ID="fu_date_pkr_rf_validator" 
                                                                ControlToValidate="fu_date_pkr" 
                                                                Display="Dynamic" ErrorMessage="* Required!" 
                                                                ValidationGroup="injury_info_validation" CssClass="validator"
                                                                BackColor="LightBlue" Font-Italic="true" Enabled="false">
                                                            </asp:RequiredFieldValidator>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">Referred to a Specialist:&nbsp;</asp:TableCell>
                                                    <asp:TableCell ColumnSpan="2"> 
                                                        <asp:DropDownList ID="referred_to_specialist_drp" 
                                                                runat="server" 
                                                                Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Size="X-Small">
                                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                                            <asp:ListItem Value="N" selected="true">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">Was PT or DX Scripted:&nbsp;</asp:TableCell>
                                                    <asp:TableCell ColumnSpan="2"><asp:DropDownList ID="pt_dx_scripted_drp" 
                                                                runat="server" 
                                                                Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Size="X-Small">
                                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                                            <asp:ListItem Value="N" selected="true">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                        <asp:Table ID="injury_additional_info_tbl" runat="server" Width="700px">             
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Top"  Width="200px">Additional Information:&nbsp;</asp:TableCell>
                                                <asp:TableCell HorizontalAlign="Left"><asp:TextBox ID="additional_info_txt" runat="server" class="txtbox" Columns="50" Rows="5" TextMode="MultiLine"></asp:TextBox></asp:TableCell>
                                            </asp:TableRow> 
                                            <asp:TableRow>
                                                <asp:TableCell>&nbsp;</asp:TableCell>
                                            </asp:TableRow> 
                                            <asp:TableRow ID="injury_info_btn_row" runat="server">
                                                <asp:TableCell Wrap="false" ColumnSpan="6" HorizontalAlign="Right" >
                                                    <asp:ImageButton ID="injury_info_clear_btn" runat="server" ImageUrl="~/images/meww_cancel_btn.png" OnClick="cancel_injury_report" CssClass="smallappBtn"/>
                                                    <asp:ImageButton ID="injury_info_review_btn" runat="server" ImageUrl="~/images/meww_review_btn.png" OnClick="go_to_next_step" CommandName="patient_info" CssClass="smallappBtn" ValidationGroup="injury_info_validation"/>
                                                    <asp:ImageButton ID="injury_info_submit_btn" runat="server" ImageUrl="~/images/meww_submit_btn.png" OnClick="submit_intake" CssClass="smallappBtn" ValidationGroup="injury_info_validation" Visible="false"/>                                                        
                                                </asp:TableCell>
                                            </asp:TableRow>   
                                            
                                        </asp:Table>                                              
                                    </ItemTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelItem>
                    </Items>
                    <CollapseAnimation Duration="0" Type="None" />
                    <ExpandAnimation Duration="0" Type="None" />
                </telerik:RadPanelBar>
        </asp:Panel>

        <asp:Panel ID="confirmation_notice_pnl" runat="server" Visible="false">
            <h1><asp:Label ID="patient_name_lbl" runat="server"></asp:Label></h1>
             <asp:Table ID="confirmation_btn_tbl" runat="server" Width="860">
                 <asp:TableRow>
                     <asp:TableCell>&nbsp;</asp:TableCell>
                 </asp:TableRow>
                 <asp:TableRow>                
                     <asp:TableCell HorizontalAlign="Right"><asp:ImageButton ID="injury_info_clear_btn" runat="server" ImageUrl="~/images/meww_continue_btn.png" OnClick="cancel_injury_report" CssClass="smallappBtn"/></asp:TableCell>
                 </asp:TableRow>
             </asp:Table>
        </asp:Panel>

        <telerik:RadAjaxPanel ID="select_an_acct_pnl" runat="server" Visible="false">
            <h1>Please select an account before sending a report to WorkWell.</h1>             
        </telerik:RadAjaxPanel>
      </asp:Panel>    

</asp:Content>
