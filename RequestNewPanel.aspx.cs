﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Odbc;
using System.IO;
using System.Net;
using System.Web.Mail;
using System.Net.Mail;

public partial class RequestNewPanel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //noLocationsFoundTbl.Style("border-collapse") = "collapse";
        if (Page.Request.IsAuthenticated)
        {
            if (!Page.IsPostBack)
            {
                SearchLocations(sender, e);
            }
            AuthenticatedMessagePanel.Visible = true;
            AnonymousMessagePanel.Visible = false;
        }
        else
        {
            AuthenticatedMessagePanel.Visible = false;
            AnonymousMessagePanel.Visible = true;
        }
    }
    public void SearchLocations(object sender, EventArgs e)
    {
        int iPartyID = Convert.ToInt32(Session["PartyID"]);
        DataTable dt = new DataTable();
        string sSqlQuery = "SELECT hp.PARTY_ID, hp.ATTRIBUTE_CATEGORY, hp.PARTY_NUMBER, hp.PARTY_NAME, hca.ACCOUNT_NUMBER, " 
        + " hca.ACCOUNT_NAME, hca.STATUS, CONCAT(IFNULL(hl.ADDRESS1,' '),' ',IFNULL(hl.ADDRESS2,' ')) ADDRESS, hl.CITY, hl.STATE, hl.POSTAL_CODE, "
        + " hca.CUST_ACCOUNT_ID " 
        + " FROM  HZ_PARTIES hp, " 
        + " HZ_CUST_ACCOUNTS hca, "
        + "HZ_CUST_ACCT_SITES_ALL hcasa, " 
        + " HZ_PARTY_SITES hps, " 
        + " HZ_LOCATIONS hl " 
        + " WHERE hp.PARTY_ID = hca.PARTY_ID AND hca.CUST_ACCOUNT_ID = hcasa.CUST_ACCOUNT_ID AND " 
        + " hcasa.PARTY_SITE_ID = hps.PARTY_SITE_ID AND hps.LOCATION_ID = hl.LOCATION_ID AND " 
        + " hp.PARTY_ID = " + iPartyID + "  AND hca.STATUS = 'A' AND " 
        + " hca.ATTRIBUTE_CATEGORY = 'EMPLOYER' AND " 
        + " hca.ACCOUNT_NAME LIKE '%" + searchLocationNameTxt.Text.Replace("'", "''") + "%' AND " 
        + " hl.ADDRESS1 LIKE '%" + searchLocationAddressTxt.Text.Replace("'", "''") + "%' AND " 
        + " hl.CITY LIKE '%" + searchLocationCityTxt.Text.Replace("'", "''") + "%' AND " 
        + " hl.STATE LIKE '%" + searchLocationStateTxt.Text.Replace("'", "''") + "%' AND "
        + " hl.POSTAL_CODE LIKE '%" + searchLocationZipTxt.Text.Replace("'", "''") + "%' " 
        + " ORDER BY hl.CITY, hl.POSTAL_CODE, hca.ACCOUNT_NAME ";


        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        OdbcDataAdapter objDataAdapter = new OdbcDataAdapter(sSqlQuery, objConnection);
        try
        {
            objConnection.Open();
            objDataAdapter.Fill(dt);

            viewActiveLocationsGrd.DataSource = dt;
            viewActiveLocationsGrd.PageIndex = 0;
            viewActiveLocationsGrd.DataBind();
            if (dt.Rows.Count > 0)
            {
                viewActiveLocationsGrd.Visible = true;
                noLocationsFoundTbl.Visible = false;
            }
            else
            {
                viewActiveLocationsGrd.Visible = false;
                noLocationsFoundTbl.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Response.Write("Can't load Web page: " + ex.Message);
        }
        finally
        {
            objConnection.Close();
            objDataAdapter.Dispose();
            objConnection.Dispose();
        }
    }
    public void ClearSearchFields(object sender, EventArgs e)
    {
        searchLocationNameTxt.Text = "";
        searchLocationAddressTxt.Text = "";
        searchLocationCityTxt.Text = "";
        searchLocationStateTxt.Text = "";
        searchLocationZipTxt.Text = "";

        SearchLocations(sender, e);

    }
    protected void viewActiveLocationsGrd_PageIndexChanging(Object sender, System.Web.UI.WebControls.GridViewPageEventArgs e) 
    {
        /*
        ''*******************
        '* in this section, I need to test if the session var already exists.  If it does, I need to clear the contents
        '*******************
        */
        noneSelectedErrorTbl.Visible = false;

        int d = viewActiveLocationsGrd.PageCount;
        Boolean[] values = new Boolean[viewActiveLocationsGrd.PageSize - 1];
        CheckBox chb;
        int isChecked = 0;

        for (int i = 0; i <= viewActiveLocationsGrd.Rows.Count - 1; i++)
        {
            chb = (CheckBox)viewActiveLocationsGrd.Rows[i].FindControl("selectionBox");
            if (chb != null)
            {
                values[i] = chb.Checked;
                isChecked = 1;
            }
        }

        if (isChecked == 1)
            Session["page" + viewActiveLocationsGrd.PageIndex] = values;


        SearchLocations(sender, e);

        viewActiveLocationsGrd.PageIndex = e.NewPageIndex;
        viewActiveLocationsGrd.DataBind();

        if (Session["page" + e.NewPageIndex] != null)
        {
            Boolean[] prevValues = (Boolean[])Session["page" + e.NewPageIndex];
            for (int x = 0; x <= viewActiveLocationsGrd.Rows.Count - 1; x++)
            {
                if (prevValues[x])
                    ((CheckBox)viewActiveLocationsGrd.Rows[x].FindControl("selectionBox")).Checked = true;

            }
        }


    }

    public void ShowSelected(object sender, EventArgs e)
    {
        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        OdbcDataReader objSqlDataRdr = null;
        int currentGrdPage = viewActiveLocationsGrd.PageIndex;
        DataTable selectedLocs = new DataTable();
        Boolean[] values = new Boolean[viewActiveLocationsGrd.PageSize - 1];
        CheckBox checkBx;
        int curPageOneChecked = 0;
        int bOneChecked = 0;

        selectedLocs.Columns.Add("ACCOUNT_NAME");
        selectedLocs.Columns.Add("ADDRESS");
        selectedLocs.Columns.Add("CITY");
        selectedLocs.Columns.Add("STATE");
        selectedLocs.Columns.Add("POSTAL_CODE");
        selectedLocs.Columns.Add("CUST_ACCOUNT_ID");

        noneSelectedErrorTbl.Visible = false;

        //--------------------------------------------------------

        //get the selections current page that user is on
        for (int i = 0; i <= viewActiveLocationsGrd.Rows.Count - 1; i++)
        {
            checkBx = (CheckBox)viewActiveLocationsGrd.Rows[i].FindControl("selectionBox");
            if (checkBx != null)
            {
                values[i] = checkBx.Checked;
                if (values[i])
                    curPageOneChecked = 1;
            }
        }

        if (curPageOneChecked == 1) 
            Session["page" + viewActiveLocationsGrd.PageIndex] = values;


        for (int i = 0; i <= viewActiveLocationsGrd.PageCount - 1; i++)
        {
            if (Session["page" + i] != null)
            {
                //set panelSearchRsltsGrd to page that the checked locations were

                SearchLocations(sender, e);
                viewActiveLocationsGrd.PageIndex = i;
                viewActiveLocationsGrd.DataBind();
                Boolean[] prevValues = (Boolean[])Session["page" + i];

                for (int x = 0; x <= viewActiveLocationsGrd.Rows.Count - 1; x++)
                {
                    if (prevValues[x])
                        bOneChecked = 1;

                }
            }
        }

        if (bOneChecked == 1)
        {
            //store the selected locations on current page

            objConnection.Open();
            objSqlCmd.Connection = objConnection;

            //now get all the other pages that user looked and selected

            for (int i = 0; i <= viewActiveLocationsGrd.PageCount - 1; i++)
            {
                if (Session["page" + i] != null)
                {
                    //set viewActiveLocationsGrd to page that the checked locations were

                    SearchLocations(sender, e);
                    viewActiveLocationsGrd.PageIndex = i;
                    viewActiveLocationsGrd.DataBind();


                    Boolean[] prevValues = (Boolean[])Session["page" + i];

                    for (int x = 0; x <= viewActiveLocationsGrd.Rows.Count - 1; x++)
                    {
                        if (prevValues[x])
                        {
                            string iCustAcctID = viewActiveLocationsGrd.DataKeys[x].Value.ToString();

                            try
                            {
                                objSqlCmd.CommandText = "SELECT hca.ACCOUNT_NAME, CONCAT(IFNULL(hl.ADDRESS1,' '),' ',IFNULL(hl.ADDRESS2,' ')) ADDRESS, hl.CITY, hl.STATE, hl.POSTAL_CODE, hca.CUST_ACCOUNT_ID "
                                    + "FROM  HZ_PARTIES hp, " 
                                    + "      HZ_CUST_ACCOUNTS hca, " 
                                    + "      HZ_CUST_ACCT_SITES_ALL hcasa, " 
                                    + "      HZ_PARTY_SITES hps, " 
                                    + "      HZ_LOCATIONS hl " 
                                    + "WHERE hp.PARTY_ID = hca.PARTY_ID AND " 
                                    + "      hca.CUST_ACCOUNT_ID = hcasa.CUST_ACCOUNT_ID AND "
                                    + "      hcasa.PARTY_SITE_ID = hps.PARTY_SITE_ID AND " 
                                    + "      hps.LOCATION_ID = hl.LOCATION_ID AND " 
                                    + "      hca.CUST_ACCOUNT_ID = " + iCustAcctID + "  AND " 
                                    + "      hca.STATUS = 'A' AND " 
                                    + "      hca.ATTRIBUTE_CATEGORY = 'EMPLOYER' ";
                                objSqlCmd.CommandType = System.Data.CommandType.Text;
                                objSqlDataRdr = objSqlCmd.ExecuteReader();

                                if (objSqlDataRdr.Read())
                                {
                                    DataRow dr = selectedLocs.NewRow();
                                    dr["ACCOUNT_NAME"] = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(0));
                                    dr["ADDRESS"] = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(1));
                                    dr["CITY"] = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(2));
                                    dr["STATE"] = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(3));
                                    dr["POSTAL_CODE"] = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(4));
                                    dr["CUST_ACCOUNT_ID"] = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(5));
                                    selectedLocs.Rows.Add(dr);

                                }
                                objSqlDataRdr.Close();
                                objSqlDataRdr.Dispose();
                            }
                            catch(Exception ex)
                            {
                                Response.Write("Can't load Web page " + ex.Message);
                            }
                        }
                    }
                }
            }

            requestCnfrmGrd.DataSource = selectedLocs;
            requestCnfrmGrd.DataBind();

            
            objConnection.Close();
            objConnection.Dispose();

            //now get back to the current page that they were on before submiting the selections
            SearchLocations(sender, e);
            viewActiveLocationsGrd.PageIndex = currentGrdPage;
            viewActiveLocationsGrd.DataBind();

            //keep to current page's selections checked if they click back

            Boolean[] existingValues = (Boolean[])Session["page" + viewActiveLocationsGrd.PageIndex];
            for (int x = 0; x <= viewActiveLocationsGrd.Rows.Count - 1; x++)
            {
                if (existingValues[x])
                    ((CheckBox)viewActiveLocationsGrd.Rows[x].FindControl("selectionBox")).Checked = true;
            }
            searchLocFieldsTbl.Visible = false;
            accountLocationsListingPanel.Visible = false;
            confirmListingPanel.Visible = true;
        }
        else
        {
            //show error
            noneSelectedErrorTbl.Visible = true;
            SearchLocations(sender, e);

            viewActiveLocationsGrd.PageIndex = currentGrdPage;
            viewActiveLocationsGrd.DataBind();

        }

    }

    public void SubmitRequest(object sender, EventArgs e)
    {
        string body = "";
        System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
        string sPanelRequests = "";
        Boolean bOneSelected = false;
        Boolean[] values = new Boolean[viewActiveLocationsGrd.PageSize - 1];


        message.IsBodyHtml = true;
        message.Subject = "Panel Request for Locations - myeWorkWell.com";
        message.From = new MailAddress("info@myeworkwell.com", "myeWorkWell Info Request");
        message.To.Add(new MailAddress("customerservice@eworkwell.com", "Customer Service"));
        message.To.Add(new MailAddress("notifications@eworkwell.com", "IT"));


        for (int i = 0; i <= requestCnfrmGrd.Rows.Count - 1; i++)
        {
            GridViewRow row = requestCnfrmGrd.Rows[i];
            Boolean isChecked = ((CheckBox)row.FindControl("selectionBox")).Checked;
            string sLocationName = "";
            string sAddress = "";

            if (isChecked)
            {
                //e.Row.Cells[columnIndex].Attributes
                sLocationName = UtilityFunctions.myCStr(row.Cells[1].Text);
                sAddress = UtilityFunctions.myCStr(row.Cells[2].Text); // address
                sAddress = sAddress + " " + UtilityFunctions.myCStr(row.Cells[3].Text); // city
                sAddress = sAddress + ", " + UtilityFunctions.myCStr(row.Cells[4].Text); //state
                sAddress = sAddress + " " + UtilityFunctions.myCStr(row.Cells[5].Text); //zip
                bOneSelected = true;
                sPanelRequests = sPanelRequests + "LOCATION NAME: " + sLocationName + "<BR>" + "ADDRESS: " + sAddress + " <BR><BR>";
            }
        }

        body += "The " + Session["UserName"] + " has requested panels for the following location(s)." + "<br/><br/>";
        body += sPanelRequests;
        body += "<hr />";
        body += "<font size='2' color='#cccccc'>";
        body += "<hr />";
        body += "www.myeworkwell.com";
        body += "<br>";
        body += System.DateTime.Now.AddHours(3);
        body += "</font>";

        message.Body = body;

        //relay-hosting.secureserver.net is the valid SMTP server of godaddy.com
        SmtpClient mailClient = new SmtpClient("dedrelay.secureserver.net");
        //Dim mailClient As SmtpClient = New SmtpClient("65.242.19.203")

        if (bOneSelected)
        {
            mailClient.Send(message);

            //delete the session vars holding the checked locations
            for (int i = 0; i <= viewActiveLocationsGrd.PageCount - 1; i++)
            {

                //********
                // this is to clear all selected locations session vars, i can't remove the session vars wirh remove (above), so I'm just seting everything to false for now.
                for (int x = 0; x <= viewActiveLocationsGrd.Rows.Count - 1; x++)
                    values[x] = false;
                Session["page" + i] = values;
            }

            confirmListingPanel.Visible = false;
            eiRqstPnlHdrPanel.Visible = false;
            requestSumbitPanel.Visible = true;
        }
        else
            noneSelectedErrorTbl.Visible = true;
        message.Dispose();
    }
    public void CancelSelection(object sender, EventArgs e)
    {
        Boolean[] values = new Boolean[viewActiveLocationsGrd.PageSize - 1];

        for (int i = 0; i <= viewActiveLocationsGrd.PageCount - 1; i++)
        {
            //********
            // this is to clear all selected locations session vars, i can't remove the session vars wirh remove (above), so I'm just seting everything to false for now.
            for (int x = 0; x <= viewActiveLocationsGrd.Rows.Count - 1; x++)
                values[x] = false;
            Session["page" + i] = values;
        }
        SearchLocations(sender, e);
    }
    public void CancelRequest(object sender, EventArgs e)
    {
        confirmListingPanel.Visible = false;
        accountLocationsListingPanel.Visible = true;
        searchLocFieldsTbl.Visible = true;
    }
}