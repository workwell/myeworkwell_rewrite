﻿Imports System.Data.Odbc

Partial Class ei_EditUI
    Inherits System.Web.UI.Page




    Sub UpdateLocationInfo()

    End Sub

    Sub CancelUpdateLocationInfo()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        eiUserNameInfoTP.HeaderText = "User Name"
        eiPasswordInfoTP.HeaderText = "Password"
        eiPersonalInfoTP.HeaderText = "Personal Information"
        eiContactInfoTP.HeaderText = "Contact Information"

        If Session("UserType") = 2 Then
            Response.Redirect("iiRoadMap.aspx")
        ElseIf Session("UserType") = 3 Then
            Response.Redirect("ciStatusReport.aspx")
        ElseIf Session("UserType") = 4 Then
            Response.Redirect("MACInsurance.aspx")
        ElseIf Session("UserType") = 5 Then
            Response.Redirect("AholdIT.aspx")
        ElseIf Session("UserType") = 6 Then
            Response.Redirect("Nationwide.aspx")
        ElseIf Session("UserType") = 7 Then
            Response.Redirect("SynergyComp.aspx")
        End If

        If Request.IsAuthenticated Then
            ' only want to get data when the page is visited for the 1st time so postbacks aren't over written by GetData()
            If Not IsPostBack Then
                GetPageData()
            End If
            AuthenticatedMessagePanel.Visible = True
            AnonymousMessagePanel.Visible = False
        Else
            AuthenticatedMessagePanel.Visible = False
            AnonymousMessagePanel.Visible = True
        End If
    End Sub

    Sub GetPageData()

        Dim objSqlCmd As New OdbcCommand
        Dim objSqlDataRdr As OdbcDataReader = Nothing
        Dim strConnection As String = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;" 'ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()


        Dim objConnection As New OdbcConnection(strConnection)

        objConnection.Open()

        Dim strSQL As String = "SELECT SUSERNAME, SFIRSTNAME, SLASTNAME, SEMAILADDRESS, SADDRESS, SADDRESS2, SCITY, " & _
                               "SSTATE, SZIP, SPHONEAREACODE, SPHONENUMBER, SEXTENSION, SFAXAREACODE, SFAXNUMBER " & _
                               "FROM USER_INFO " & _
                               "WHERE SUSERNAME = '" & User.Identity.Name.ToUpper & "' AND BACTIVE = 1 "

        objSqlCmd = New OdbcCommand(strSQL, objConnection)
        objSqlDataRdr = objSqlCmd.ExecuteReader

        If objSqlDataRdr.Read() Then
            userNameTxt.Text = myCStr(objSqlDataRdr.Item(0))
            firstNameTxt.Text = myCStr(objSqlDataRdr.Item(1))
            lastNameTxt.Text = myCStr(objSqlDataRdr.Item(2))
            emailTxt.Text = myCStr(objSqlDataRdr.Item(3))
            addressTxt.Text = myCStr(objSqlDataRdr.Item(4))
            address2Txt.Text = myCStr(objSqlDataRdr.Item(5))
            cityText.Text = myCStr(objSqlDataRdr.Item(6))
            stateTxt.Text = myCStr(objSqlDataRdr.Item(7))
            zipTxt.Text = myCStr(objSqlDataRdr.Item(8))
            phoneAreaCodeTxt.Text = myCStr(objSqlDataRdr.Item(9))
            phoneNumberTxt.Text = myCStr(objSqlDataRdr.Item(10))
            extensionTxt.Text = myCStr(objSqlDataRdr(11))
            faxAreaCodeTxt.Text = myCStr(objSqlDataRdr.Item(12))
            faxNumberTxt.Text = myCStr(objSqlDataRdr.Item(13))
        End If

        ClearUpdateMessages()

        objSqlCmd = Nothing
        objSqlDataRdr = Nothing
        objConnection.Close()

    End Sub

    Function myCStr(ByVal test As Object) As String

        Dim sReturnStr As String = ""

        If IsDBNull(test) Then
            sReturnStr = ""
        Else
            sReturnStr = CStr(test)
        End If

        If StrComp(sReturnStr, "#") = 0 Then
            sReturnStr = ""
        End If

        myCStr = sReturnStr

    End Function

    Sub ClearUpdateMessages()

        userNameError.Visible = False
        userNameErrorAlreadyExists.Visible = False
        userNameErrorEmptyName.Visible = False
        passwordError.Visible = False
        passwordErrorCurrentPswd.Visible = False
        passwordErrorNotMatching.Visible = False
        passwordSuccess.Visible = False
        passwordErrorMustEnterPswd.Visible = False
        contactInfoError.Visible = False
        contactInfoSuccess.Visible = False
        personalInfoError.Visible = False
        peronsalInfoSuccess.Visible = False

    End Sub

    Sub UpdateTableData(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim strSQL As String = ""
        Dim objSqlCmd As New OdbcCommand
        Dim objSqlDataRdr As OdbcDataReader = Nothing
        Dim strConnection As String = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;" 'ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()
        Dim objConnection As New OdbcConnection(strConnection)

        Page.Validate()

        If Page.IsValid Then

            Dim btn As ImageButton = DirectCast(sender, ImageButton)
            Select Case btn.CommandName
                Case "userNameSubmit"

                    ClearUpdateMessages()

                    ' see if the user name isn't null; if not null change continue; else throw error that name can't be null
                    If String.IsNullOrEmpty(userNameTxt.Text) <> True Then
                        ' see if the new name is still the same; if not the same update; else do nothing
                        If StrComp(userNameTxt.Text, User.Identity.Name.ToUpper) <> 0 Then
                            'see if name is already taken; if not update; else spit back error

                            objConnection.Open()

                            objSqlCmd = New OdbcCommand(strSQL, objConnection)
                            objSqlCmd.CommandText = "{call sp_updateUserName(?,?)}"
                            objSqlCmd.CommandType = Data.CommandType.StoredProcedure
                            objSqlCmd.Parameters.Add(New OdbcParameter("@userName", OdbcType.VarChar, 255))
                            objSqlCmd.Parameters.Add(New OdbcParameter("@newUserName", OdbcType.VarChar, 255))

                            objSqlCmd.Parameters("@userName").Value = User.Identity.Name.ToUpper
                            objSqlCmd.Parameters("@newUserName").Value = userNameTxt.Text.ToUpper

                            objSqlDataRdr = objSqlCmd.ExecuteReader

                            If objSqlDataRdr.Read() Then
                                If objSqlDataRdr.Item(0) = 1 Then

                                    AuthenticatedMessagePanel.Visible = False
                                    AnonymousMessagePanel.Visible = False
                                    FormsAuthentication.SignOut()
                                    Session.Abandon()

                                    ' clear authentication cookie; won't allow user to navigate site after logout 
                                    Dim authCookie As New HttpCookie(FormsAuthentication.FormsCookieName, "")
                                    authCookie.Expires = DateTime.Now.AddYears(-1)
                                    Response.Cookies.Add(authCookie)

                                    ' clear session cookie; reccomended for further security; won't allow user to nagivate site after log out
                                    Dim sessionCookie As New HttpCookie("ASP.NET_SessionId", "")
                                    sessionCookie.Expires = DateTime.Now.AddYears(-1)
                                    Response.Cookies.Add(sessionCookie)

                                    objConnection.Close()

                                    Response.Redirect("~/login.aspx?iS=0")
                                Else
                                    ClearUpdateMessages()
                                    userNameErrorAlreadyExists.Visible = True
                                    objConnection.Close()
                                End If
                            End If
                        Else
                            userNameError.Visible = False
                            userNameErrorAlreadyExists.Visible = False
                            objConnection.Close()
                        End If
                    Else
                        userNameErrorEmptyName.Visible = True
                    End If

                    Exit Select
                Case "passwordSubmit"
                    ClearUpdateMessages()

                    objConnection.Open()

                    If String.IsNullOrEmpty(oldPasswordTxt.Text) <> True And String.IsNullOrEmpty(newPasswordTxt.Text) <> True And String.IsNullOrEmpty(confirmPasswordTxt.Text) <> True Then
                        If StrComp(newPasswordTxt.Text, confirmPasswordTxt.Text) = 0 Then



                            objSqlCmd = New OdbcCommand(strSQL, objConnection)
                            objSqlCmd.CommandText = "{call sp_updatePassword(?,?,?)}"
                            objSqlCmd.CommandType = Data.CommandType.StoredProcedure
                            objSqlCmd.Parameters.Add(New OdbcParameter("@userName", OdbcType.VarChar, 255))
                            objSqlCmd.Parameters.Add(New OdbcParameter("@oldPassword", OdbcType.VarChar, 255))
                            objSqlCmd.Parameters.Add(New OdbcParameter("@newPassword", OdbcType.VarChar, 255))

                            objSqlCmd.Parameters("@userName").Value = User.Identity.Name.ToUpper
                            objSqlCmd.Parameters("@oldPassword").Value = oldPasswordTxt.Text
                            objSqlCmd.Parameters("@newPassword").Value = newPasswordTxt.Text

                            objSqlDataRdr = objSqlCmd.ExecuteReader()

                            If objSqlDataRdr.Read() Then
                                If objSqlDataRdr.Item(0) <> 0 Then
                                    passwordSuccess.Visible = True
                                Else
                                    passwordErrorCurrentPswd.Visible = True
                                End If
                            End If
                        Else
                            passwordErrorNotMatching.Visible = True
                        End If
                    Else
                        If String.IsNullOrEmpty(oldPasswordTxt.Text) = True And String.IsNullOrEmpty(newPasswordTxt.Text) = True And String.IsNullOrEmpty(confirmPasswordTxt.Text) = True Then
                            ClearUpdateMessages()
                        Else
                            passwordErrorMustEnterPswd.Visible = True
                        End If
                    End If

                    objConnection.Close()

                    Exit Select
                Case "personalInfoSubmit"

                    objConnection.Open()

                    objSqlCmd = New OdbcCommand(strSQL, objConnection)
                    objSqlCmd.CommandText = "{call sp_checkPersonalInfo(?,?,?,?,?,?,?,?)}"
                    objSqlCmd.CommandType = Data.CommandType.StoredProcedure
                    objSqlCmd.Parameters.Add(New OdbcParameter("@firstName", OdbcType.VarChar, 255))
                    objSqlCmd.Parameters.Add(New OdbcParameter("@lastName", OdbcType.VarChar, 255))
                    objSqlCmd.Parameters.Add(New OdbcParameter("@address", OdbcType.VarChar, 255))
                    objSqlCmd.Parameters.Add(New OdbcParameter("@address2", OdbcType.VarChar, 255))
                    objSqlCmd.Parameters.Add(New OdbcParameter("@city", OdbcType.VarChar, 255))
                    objSqlCmd.Parameters.Add(New OdbcParameter("@state", OdbcType.Char, 2))
                    objSqlCmd.Parameters.Add(New OdbcParameter("@zip", OdbcType.VarChar, 10))
                    objSqlCmd.Parameters.Add(New OdbcParameter("@userName", OdbcType.VarChar, 255))

                    objSqlCmd.Parameters("@firstName").Value = myCStr(firstNameTxt.Text.ToUpper)
                    objSqlCmd.Parameters("@lastName").Value = myCStr(lastNameTxt.Text.ToUpper)
                    objSqlCmd.Parameters("@address").Value = myCStr(addressTxt.Text.ToUpper)
                    objSqlCmd.Parameters("@address2").Value = myCStr(address2Txt.Text.ToUpper)
                    objSqlCmd.Parameters("@city").Value = myCStr(cityText.Text.ToUpper)
                    objSqlCmd.Parameters("@state").Value = myCStr(stateTxt.Text.ToUpper)
                    objSqlCmd.Parameters("@zip").Value = myCStr(zipTxt.Text)
                    objSqlCmd.Parameters("@userName").Value = User.Identity.Name.ToUpper

                    objSqlDataRdr = objSqlCmd.ExecuteReader()

                    If objSqlDataRdr.Read() Then
                        If objSqlDataRdr.Item(0) = 1 Then

                            objSqlCmd = New OdbcCommand(strSQL, objConnection)
                            objSqlCmd.CommandText = "{call sp_updatePersonalInfo(?,?,?,?,?,?,?,?)}"
                            objSqlCmd.CommandType = Data.CommandType.StoredProcedure
                            objSqlCmd.Parameters.Add(New OdbcParameter("@firstName", OdbcType.VarChar, 255))
                            objSqlCmd.Parameters.Add(New OdbcParameter("@lastName", OdbcType.VarChar, 255))
                            objSqlCmd.Parameters.Add(New OdbcParameter("@address", OdbcType.VarChar, 255))
                            objSqlCmd.Parameters.Add(New OdbcParameter("@address2", OdbcType.VarChar, 255))
                            objSqlCmd.Parameters.Add(New OdbcParameter("@city", OdbcType.VarChar, 255))
                            objSqlCmd.Parameters.Add(New OdbcParameter("@state", OdbcType.Char, 2))
                            objSqlCmd.Parameters.Add(New OdbcParameter("@zip", OdbcType.VarChar, 10))
                            objSqlCmd.Parameters.Add(New OdbcParameter("@userName", OdbcType.VarChar, 255))

                            objSqlCmd.Parameters("@firstName").Value = myCStr(firstNameTxt.Text.ToUpper)
                            objSqlCmd.Parameters("@lastName").Value = myCStr(lastNameTxt.Text.ToUpper)
                            objSqlCmd.Parameters("@address").Value = myCStr(addressTxt.Text.ToUpper)
                            objSqlCmd.Parameters("@address2").Value = myCStr(address2Txt.Text.ToUpper)
                            objSqlCmd.Parameters("@city").Value = myCStr(cityText.Text.ToUpper)
                            objSqlCmd.Parameters("@state").Value = myCStr(stateTxt.Text.ToUpper)
                            objSqlCmd.Parameters("@zip").Value = myCStr(zipTxt.Text)
                            objSqlCmd.Parameters("@userName").Value = User.Identity.Name.ToUpper
                            objSqlCmd.ExecuteNonQuery()

                            ClearUpdateMessages()
                            peronsalInfoSuccess.Visible = True
                        Else
                            ClearUpdateMessages()
                        End If
                    End If

                    objConnection.Close()

                    Exit Select
                Case "contactInfoSubmit"

                    objConnection.Open()

                    objSqlCmd = New OdbcCommand(strSQL, objConnection)
                    objSqlCmd.CommandText = "{call sp_checkContactInfo(?,?,?,?,?,?,?)}"
                    objSqlCmd.CommandType = Data.CommandType.StoredProcedure
                    objSqlCmd.Parameters.Add(New OdbcParameter("@emailAddress", OdbcType.VarChar, 255))
                    objSqlCmd.Parameters.Add(New OdbcParameter("@phoneAreaCode", OdbcType.VarChar, 3))
                    objSqlCmd.Parameters.Add(New OdbcParameter("@phoneNumber", OdbcType.VarChar, 8))
                    objSqlCmd.Parameters.Add(New OdbcParameter("@extension", OdbcType.VarChar, 10))
                    objSqlCmd.Parameters.Add(New OdbcParameter("@faxAreaCode", OdbcType.VarChar, 3))
                    objSqlCmd.Parameters.Add(New OdbcParameter("@faxNumber", OdbcType.VarChar, 8))
                    objSqlCmd.Parameters.Add(New OdbcParameter("@userName", OdbcType.VarChar, 255))

                    objSqlCmd.Parameters("@emailAddress").Value = myCStr(emailTxt.Text.ToUpper)
                    objSqlCmd.Parameters("@phoneAreaCode").Value = myCStr(phoneAreaCodeTxt.Text)
                    objSqlCmd.Parameters("@phoneNumber").Value = myCStr(phoneNumberTxt.Text)
                    objSqlCmd.Parameters("@extension").Value = myCStr(extensionTxt.Text)
                    objSqlCmd.Parameters("@faxAreaCode").Value = myCStr(faxAreaCodeTxt.Text)
                    objSqlCmd.Parameters("@faxNumber").Value = myCStr(faxNumberTxt.Text)
                    objSqlCmd.Parameters("@userName").Value = User.Identity.Name.ToUpper
                    objSqlDataRdr = objSqlCmd.ExecuteReader()

                    If objSqlDataRdr.Read() Then
                        If objSqlDataRdr.Item(0) = 1 Then
                            objSqlCmd = New OdbcCommand(strSQL, objConnection)
                            objSqlCmd.CommandText = "{call sp_updateContactInfo(?,?,?,?,?,?,?)}"
                            objSqlCmd.CommandType = Data.CommandType.StoredProcedure
                            objSqlCmd.Parameters.Add(New OdbcParameter("@emailAddress", OdbcType.VarChar, 255))
                            objSqlCmd.Parameters.Add(New OdbcParameter("@phoneAreaCode", OdbcType.VarChar, 3))
                            objSqlCmd.Parameters.Add(New OdbcParameter("@phoneNumber", OdbcType.VarChar, 8))
                            objSqlCmd.Parameters.Add(New OdbcParameter("@extension", OdbcType.VarChar, 10))
                            objSqlCmd.Parameters.Add(New OdbcParameter("@faxAreaCode", OdbcType.VarChar, 3))
                            objSqlCmd.Parameters.Add(New OdbcParameter("@faxNumber", OdbcType.VarChar, 8))
                            objSqlCmd.Parameters.Add(New OdbcParameter("@userName", OdbcType.VarChar, 255))

                            objSqlCmd.Parameters("@emailAddress").Value = myCStr(emailTxt.Text.ToUpper)
                            objSqlCmd.Parameters("@phoneAreaCode").Value = myCStr(phoneAreaCodeTxt.Text)
                            objSqlCmd.Parameters("@phoneNumber").Value = myCStr(phoneNumberTxt.Text)
                            objSqlCmd.Parameters("@extension").Value = myCStr(extensionTxt.Text)
                            objSqlCmd.Parameters("@faxAreaCode").Value = myCStr(faxAreaCodeTxt.Text)
                            objSqlCmd.Parameters("@faxNumber").Value = myCStr(faxNumberTxt.Text)
                            objSqlCmd.Parameters("@userName").Value = User.Identity.Name.ToUpper
                            objSqlCmd.ExecuteNonQuery()

                            ClearUpdateMessages()
                            contactInfoSuccess.Visible = True
                        Else
                            ClearUpdateMessages()
                        End If
                    End If

                    objConnection.Close()

                    Exit Select
            End Select

        End If

    End Sub
End Class
