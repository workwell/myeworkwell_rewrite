﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
/// <summary>
/// Basic and general utility functions.
/// </summary>
public class UtilityFunctions
{
    public static string ConvertDate(DateTime dDateOfTreatment)
    {
        if (dDateOfTreatment != null)
            return dDateOfTreatment.ToString("MM/dd/yyyy");
        else
            return "";        
    }
    
    public static string myCStr(object test)
    {
        if (test == DBNull.Value)
        {
            return (String.Empty);
        }
        else
        {
            return Convert.ToString(test);
        }
    }
    public static string ReverseDate(String dDate)
    {
        DateTime dt = Convert.ToDateTime(dDate);
        if (dDate != null)
            return dt.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
        else
            return "";
    }
}