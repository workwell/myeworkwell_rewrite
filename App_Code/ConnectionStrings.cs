﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
/// <summary>
/// Simplet class to use functions to grab the connection strings so I don't need to make a function within ever single class
/// </summary>
public class ConnectionStrings
{
	static public string MySqlConnections()
	{
        string ConnectionString = ConfigurationManager.ConnectionStrings["mysql"].ConnectionString;
        return ConnectionString;
	}
}