﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/myeworkwell_webapp_dashboard.master" CodeFile="UserAccountEditor.aspx.cs" Inherits="UserAccountEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentHldr" Runat="Server">

    <asp:Panel runat="server" ID="AuthenticatedMessagePanel">
        
        <h1>Edit User Profile</h1>
        <div class="divider"></div>
        <div class="appFormLeft"></div> 

        <ajaxToolkit:ToolkitScriptManager runat="Server" EnablePartialRendering="true" ID="eiSearchProvidersSM" /> 

         <ajaxToolkit:TabContainer runat="server" ID="eiViewlocDetailsTC" Height="503px" ActiveTabIndex="0" Width="860px" CssClass="visoft__tab_xpie7"> 
                    
            <ajaxToolkit:TabPanel ID="eiUserNameInfoTP" runat="server" >
                <ContentTemplate>
	                <asp:Panel runat="server" ID="eiUserNameInfoPanel" CssClass="shortAppForm">
	                    <div  style="position:relative;width:850px;height:500px; overflow:auto;">                           
                            <asp:Table ID="userNameTable" runat="server"  Width="230" >  
                                <asp:TableRow>
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                </asp:TableRow> 
                                <asp:TableRow>
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                </asp:TableRow> 
                                <asp:TableRow>
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                </asp:TableRow>                 
                                <asp:TableRow >
                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">User Name:&nbsp;</asp:TableCell>
                                    <asp:TableCell Wrap="false"><asp:TextBox ID="userNameTxt" runat="server" Width="155" CssClass="txtbox"></asp:TextBox></asp:TableCell>                     
                                </asp:TableRow> 
                                <asp:TableRow>
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                </asp:TableRow> 
                                <asp:TableRow>
                                    <asp:TableCell Wrap="false" ColumnSpan="2" HorizontalAlign="Right" >
                                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="images/appBtnCancel.gif" OnClick="GetPageData" CssClass="smallappBtn"/><asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="images/appBtnApply.gif" OnClick="UpdateTableData" CommandName="userNameSubmit" CssClass="smallappBtn"/>
                                    </asp:TableCell>
                                </asp:TableRow>                                                   
                            </asp:Table>
                            <asp:Label ID="userNameError" runat="server" ForeColor="Red" Font-Bold="true" Text="ERROR: Your user name was NOT updated." Visible="False"></asp:Label>
                            <asp:Label ID="userNameErrorAlreadyExists" runat="server" ForeColor="Red" Font-Bold="true" Text="ERROR: That user name is already in use." Visible="False"></asp:Label> 
                            <asp:Label ID="userNameErrorEmptyName" runat="server" ForeColor="Red" Font-Bold="true" Text="ERROR: You must enter a user name." Visible="False"></asp:Label> 
                        </div>        
	                </asp:Panel>
	            </ContentTemplate>
            </ajaxToolkit:TabPanel>


            <ajaxToolkit:TabPanel ID="eiPasswordInfoTP" runat="server" >
                <ContentTemplate>
	                <asp:Panel runat="server" ID="eiPasswordInfoPanel">
	                    <div  style="position:relative;width:850px;height:500px; overflow:auto;">                            
                            <asp:Table ID="passwordTable" runat="server"  Width="270" >
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                                </asp:TableRow> 
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                                </asp:TableRow> 
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                                </asp:TableRow>                 
                                <asp:TableRow >
                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">Old Password:&nbsp;</asp:TableCell>
                                    <asp:TableCell Wrap="false"><asp:TextBox TextMode="Password" ID="oldPasswordTxt" runat="server" Width="155" class="txtbox"></asp:TextBox></asp:TableCell>                     
                                </asp:TableRow>                   
                                <asp:TableRow >
                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">New Password:&nbsp;</asp:TableCell>
                                    <asp:TableCell Wrap="false"><asp:TextBox TextMode="Password" ID="newPasswordTxt" runat="server" Width="155" class="txtbox"></asp:TextBox></asp:TableCell>                     
                                </asp:TableRow> 
                                <asp:TableRow >
                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">Confirm Password:&nbsp;</asp:TableCell>
                                    <asp:TableCell Wrap="false"><asp:TextBox TextMode="Password" ID="confirmPasswordTxt" runat="server" Width="155" class="txtbox"></asp:TextBox></asp:TableCell> 
                                </asp:TableRow> 
                                <asp:TableRow>
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                </asp:TableRow> 
                                <asp:TableRow>
                                    <asp:TableCell Wrap="false" ColumnSpan="2" HorizontalAlign="Right" >
                                        <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="images/appBtnCancel.gif" OnClick="GetPageData" CssClass="smallappBtn"/><asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="images/appBtnApply.gif" OnClick="UpdateTableData" CommandName="passwordSubmit"  CssClass="smallappBtn"/>
                                    </asp:TableCell>
                                </asp:TableRow>                                                    
                            </asp:Table>     
                            <asp:Label ID="passwordSuccess" runat="server" ForeColor="Green" Font-Bold="true" Text="Your password has been updated." Visible="False"></asp:Label> 
                            <asp:Label ID="passwordError" runat="server" ForeColor="Red"  Font-Bold="true" Text="ERROR: Your password has NOT been updated!!!" Visible="False"></asp:Label>
                            <asp:Label ID="passwordErrorNotMatching" runat="server"  Font-Bold="true" ForeColor="Red" Text="ERROR: Your new passwords did not match!!!" Visible="False"></asp:Label>  
                            <asp:Label ID="passwordErrorCurrentPswd" runat="server"  Font-Bold="true" ForeColor="Red" Text="ERROR: Incorrect password!!!" Visible="False"></asp:Label> 
                            <asp:Label ID="passwordErrorMustEnterPswd" runat="server"  Font-Bold="true" ForeColor="Red" Text="ERROR: ALL fields are required!!!" Visible="False"></asp:Label>  
                        </div>        
	                </asp:Panel>
	            </ContentTemplate>
            </ajaxToolkit:TabPanel>

             <ajaxToolkit:TabPanel ID="eiPersonalInfoTP" runat="server" >
                <ContentTemplate>
	                <asp:Panel runat="server" ID="eiPersonalInfoPanel">
	                    <div  style="position:relative;width:850px;height:500px; overflow:auto;">                            
                            <asp:Table ID="userProfileTable" runat="server"  Width="455" >
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="6">&nbsp;</asp:TableCell>
                                </asp:TableRow> 
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="6">&nbsp;</asp:TableCell>
                                </asp:TableRow> 
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="6">&nbsp;</asp:TableCell>
                                </asp:TableRow>                  
                                <asp:TableRow >
                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">First Name:&nbsp;</asp:TableCell>
                                    <asp:TableCell ColumnSpan="5"><asp:TextBox ID="firstNameTxt" runat="server" Width="190" class="txtbox"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow >
                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">Last Name:&nbsp;</asp:TableCell>
                                    <asp:TableCell ColumnSpan="5"><asp:TextBox ID="lastNameTxt" runat="server" Width="190" class="txtbox"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right">Address:&nbsp;</asp:TableCell>
                                    <asp:TableCell ColumnSpan="5"><asp:TextBox ID="addressTxt" runat="server" Width="285"  class="txtbox"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                    <asp:TableCell ColumnSpan="5"><asp:TextBox ID="address2Txt" runat="server" Width="285" class="txtbox"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right">City:&nbsp;</asp:TableCell>
                                    <asp:TableCell  Wrap="false" ><asp:TextBox ID="cityText" runat="server" Width="150" class="txtbox"></asp:TextBox></asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Left">State:</asp:TableCell>
                                    <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="stateTxt" runat="server" MaxLength="2" Width="20" class="txtbox"></asp:TextBox></asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Left">Zip:</asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Left"><asp:TextBox ID="zipTxt" runat="server" MaxLength="10"  Width="70" class="txtbox"></asp:TextBox></asp:TableCell>
                                </asp:TableRow> 
                                <asp:TableRow>
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                </asp:TableRow> 
                                <asp:TableRow>
                                    <asp:TableCell Wrap="false" ColumnSpan="6" HorizontalAlign="Right" >
                                        <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="images/appBtnCancel.gif" OnClick="GetPageData" CssClass="smallappBtn"/><asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="images/appBtnApply.gif" OnClick="UpdateTableData" CommandName="personalInfoSubmit"  CssClass="smallappBtn"/>
                                    </asp:TableCell>
                                </asp:TableRow>                    
                            </asp:Table>  
                            <asp:Label ID="peronsalInfoSuccess" runat="server" Font-Bold="true" ForeColor="Green" Text="Your personal information has been updated." Visible="False"></asp:Label> 
                            <asp:Label ID="personalInfoError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: Your personal information has NOT been updated!!!" Visible="False"></asp:Label>        
                        </div>        
	                </asp:Panel>
	            </ContentTemplate>
            </ajaxToolkit:TabPanel>

            <ajaxToolkit:TabPanel ID="eiContactInfoTP" runat="server" >
                <ContentTemplate>
	                <asp:Panel runat="server" ID="eiContactInfoPanel">
	                    <div  style="position:relative;width:850px;height:500px; overflow:auto;">                            
                            <asp:Table ID="contactTable" runat="server"  Width="150" >
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="6">&nbsp;</asp:TableCell>
                                </asp:TableRow> 
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="6">&nbsp;</asp:TableCell>
                                </asp:TableRow> 
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="6">&nbsp;</asp:TableCell>
                                </asp:TableRow>                 
                                <asp:TableRow >
                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">Email:&nbsp;</asp:TableCell>
                                    <asp:TableCell ColumnSpan="5" HorizontalAlign="Left"><asp:TextBox ID="emailTxt" runat="server" Width="176" class="txtbox"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow >
                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">Phone Number:&nbsp;</asp:TableCell>
                                    <asp:TableCell Wrap="false"><asp:TextBox ID="phoneAreaCodeTxt" runat="server" Width="30" class="txtbox"></asp:TextBox></asp:TableCell>
                                    <asp:TableCell Wrap="false">-</asp:TableCell>
                                    <asp:TableCell Wrap="false"><asp:TextBox ID="phoneNumberTxt" runat="server" Width="70" class="txtbox"></asp:TextBox></asp:TableCell>
                                    <asp:TableCell Wrap="false">ext:</asp:TableCell>
                                    <asp:TableCell Wrap="false"><asp:TextBox ID="extensionTxt" runat="server" Width="40" class="txtbox"></asp:TextBox></asp:TableCell>
                                </asp:TableRow> 
                                <asp:TableRow >
                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">Fax Number:&nbsp;</asp:TableCell>
                                    <asp:TableCell Wrap="false"><asp:TextBox ID="faxAreaCodeTxt" runat="server" Width="30" class="txtbox"></asp:TextBox></asp:TableCell>
                                    <asp:TableCell Wrap="false">-</asp:TableCell>
                                    <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="2"><asp:TextBox ID="faxNumberTxt" runat="server" Width="70" class="txtbox"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Wrap="false" ColumnSpan="6" HorizontalAlign="Right">
                                        <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="images/appBtnCancel.gif" OnClick="GetPageData" CssClass="smallappBtn"/><asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="images/appBtnApply.gif" OnClick="UpdateTableData" CommandName="contactInfoSubmit"  CssClass="smallappBtn"/>
                                    </asp:TableCell>
                                </asp:TableRow>                                
                            </asp:Table>
                            <asp:Label ID="contactInfoSuccess" runat="server"  Font-Bold="true" ForeColor="Green" Text="Your contact information has been updated." Visible="False"></asp:Label> 
                            <asp:Label ID="contactInfoError" runat="server"  Font-Bold="true" ForeColor="Red" Text="ERROR: Your contact information has NOT been updated!!!" Visible="False"></asp:Label>    
                        </div>        
	                </asp:Panel>
	            </ContentTemplate>
            </ajaxToolkit:TabPanel>

            
        </ajaxToolkit:TabContainer>

    </asp:Panel>
    
    <asp:Panel runat="Server" ID="AnonymousMessagePanel">
        <asp:HyperLink runat="server" ID="lnkLogin" Text="Log In**" NavigateUrl="~/Login.aspx"></asp:HyperLink>        
    </asp:Panel>

</asp:Content>
