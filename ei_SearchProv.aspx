﻿<%@ Page Title="" Language="VB" MasterPageFile="~/myeworkwell_webapp_dashboard.master" AutoEventWireup="false" CodeFile="ei_SearchProv.aspx.vb" Inherits="ei_SearchProv" EnableEventValidation="false"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="ei_SearchProv" ContentPlaceHolderID="contentHldr" Runat="Server">

    <asp:Panel runat="server" ID="AuthenticatedMessagePanel">
        
        <asp:HiddenField ID="eiSearchByZipSuccessful" runat="server" value="0"/>
        <asp:HiddenField ID="eiSearchByNameSuccessful" runat="server" value="0"/>

        <h1>Network Physician Search</h1>
        <div class="divider"></div>
        <div class="appFormLeft"></div> 
        <ajaxToolkit:ToolkitScriptManager runat="Server" EnablePartialRendering="true" ID="eiSearchProvidersSM" /> 

        <asp:Panel ID="eiSearchFiledsPanel" runat="server">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <asp:Table runat="server" ID="eiSearchProgressTbl2" Width="860px">
                        <asp:TableRow>
                            <asp:TableCell>&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Width="860px" HorizontalAlign="Center"><asp:Image ID="eiSearchProgressImg" runat="server" ImageUrl= "/images/newloading.gif"/></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Width="860px" HorizontalAlign="Center"><asp:Image ID="Image1" runat="server" ImageUrl= "/images/loading_str.png"/></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </ProgressTemplate> 
            </asp:UpdateProgress>

                            

                    <ajaxToolkit:TabContainer runat="server" ID="eiViewlocDetailsTC" Height="503px" ActiveTabIndex="0" Width="860px" CssClass="visoft__tab_xpie7"> 
                    
                        <ajaxToolkit:TabPanel ID="eiSearchByZipTP" runat="server" >
                            <ContentTemplate> 
                                   <!-- <asp:UpdatePanel ID="eiSearchProvUPanel" runat="server">
                                        <ContentTemplate>         -->       
                                            <asp:Table ID="searchProvidersByZipTbl" runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Width="50">&nbsp;</asp:TableCell>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">Zip Code:&nbsp;</asp:TableCell>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="eiSearchZipCodeTxt" runat="server" Width="90" class="txtbox"></asp:TextBox></asp:TableCell>
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Right">WITHIN:&nbsp;</asp:TableCell>
                                        
                                                    <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:DropDownList id="eiSearchDistanceDrp" 
                                                                    runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Size="X-Small">
                                                                    <asp:ListItem Value="5"> 5 miles</asp:ListItem>
                                                                    <asp:ListItem Value="10">10 miles</asp:ListItem>
                                                                    <asp:ListItem Value="15">15 miles</asp:ListItem>
                                                                    <asp:ListItem Value="20">20 miles</asp:ListItem>
                                                                    <asp:ListItem Value="30">30 miles</asp:ListItem>
                                                                </asp:DropDownList>
                                                    </asp:TableCell>  
                                                </asp:TableRow>   
                                                <asp:TableRow>
                                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                                </asp:TableRow>                       
                                                <asp:TableRow>
                                                    <asp:TableCell Wrap="false" ColumnSpan="5" HorizontalAlign="Right"><asp:ImageButton ID="submitSearchZipCodebtn" runat="server" ImageUrl="images/appBtnSearch.gif" OnClick="ShowProvidersByZip" CssClass="smallappBtn"/></asp:TableCell>                    
                                                </asp:TableRow>
                                            </asp:Table>
                                            <asp:Label ID="invalidZipError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter a valid zip code. *<BR>" Visible="False"></asp:Label>
                                            <asp:Label ID="notFoundZipError" runat="server" Font-Bold="true" ForeColor="Tomato" Text="ALERT: Searched zip code was not found. *<BR>" Visible="False"></asp:Label>                                                        
                                       <!-- </ContentTemplate>
                                </asp:UpdatePanel> -->
	                        </ContentTemplate>

                        </ajaxToolkit:TabPanel>


                        <ajaxToolkit:TabPanel ID="eiSearchByNameTP" runat="server" >
                            <ContentTemplate>
	                            <asp:Table ID="searchProvidersByNameTbl" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Width="50">&nbsp;</asp:TableCell>
                                        <asp:TableCell Wrap="false" HorizontalAlign="Right">First Name:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left"><asp:TextBox ID="eiSearchFNameTxt" runat="server" Width="190" class="txtbox"></asp:TextBox></asp:TableCell>
                                    </asp:TableRow>    
						            <asp:TableRow>
                                        <asp:TableCell Width="50">&nbsp;</asp:TableCell>
                                        <asp:TableCell Wrap="false" HorizontalAlign="Right">Last Name:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left"><asp:TextBox ID="eiSearchLNameTxt" runat="server" Width="190" class="txtbox"></asp:TextBox></asp:TableCell>
                                    </asp:TableRow> 
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                            <asp:TableCell Wrap="false" ColumnSpan="3" HorizontalAlign="Right"><asp:ImageButton ID="submitSearchNamebtn" runat="server" ImageUrl="images/appBtnSearch.gif" OnClick="ShowProvidersByName" CssClass="smallappBtn"/></asp:TableCell>                    
                                        </asp:TableRow>
                                </asp:Table>
                                 <asp:Label ID="noNameEnteredError" runat="server" Font-Bold="true" ForeColor="Tomato" Text="ALERT: You must enter a name, either the first or last name. *<BR>" Visible="False"></asp:Label>                                                        
                                      
	                        </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        
                    </ajaxToolkit:TabContainer> 
        </asp:Panel>

        <asp:Panel ID="searchProvidersByZipResultsPanel" runat="server" Visible="false">                   
            <ajaxToolkit:TabContainer runat="server" ID="eiSearchProvidersTC" Height="403px" ActiveTabIndex="0" Width="850px"> 
                      
                <ajaxToolkit:TabPanel ID="eiSearchProvOMTP" runat="server">
                    <ContentTemplate>
	                    <asp:Panel runat="server" ID="eiSearchProvOMPanel" >
	                                <div style="position:relative;width:840px;height:395px; overflow:auto;">
                                <asp:Table runat="server" ID="eiSearchProvOMTbl" Width="823px" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" Font-Size="11px" Font-Names="Calibri" CssClass="mGrid">
                                        
                                </asp:Table>
                                </div>        
	                    </asp:Panel>
	                </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="eiSearchProvIMTP" runat="server" >
                    <ContentTemplate>
	                    <asp:Panel runat="server" ID="eiSearchProvIMPanel" >
	                                <div  style="position:relative;width:840px;height:395px; overflow:auto;">
                                <asp:Table runat="server" ID="eiSearchProvIMTbl" Width="823px" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" Font-Size="11px" Font-Names="Calibri" CssClass="mGrid">
                                        
                                </asp:Table>
                                </div>        
	                    </asp:Panel>
	                </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="eiSearchProvFPTP" runat="server">
                    <ContentTemplate>
	                    <asp:Panel runat="server" ID="eiSearchProvFPPanel" >
	                                <div style="position:relative;width:840px;height:395px; overflow:auto;">
                                <asp:Table runat="server" ID="eiSearchProvFPTbl" Width="823px" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" Font-Size="11px" Font-Names="Calibri" CssClass="mGrid">
                                        
                                </asp:Table>
                                </div>        
	                    </asp:Panel>
	                </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="eiSearchProvORTP" runat="server">
                    <ContentTemplate>
	                    <asp:Panel runat="server" ID="eiSearchProvORPanel" >
	                                <div style="position:relative;width:840px;height:395px; overflow:auto;">
                                <asp:Table runat="server" ID="eiSearchProvORTbl" Width="823px" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" Font-Size="11px" Font-Names="Calibri" CssClass="mGrid">
                                        
                                </asp:Table>
                                </div>        
	                    </asp:Panel>
	                </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="eiSearchProvGSTP" runat="server">
                    <ContentTemplate>
	                    <asp:Panel runat="server" ID="eiSearchProvGSPanel" >
	                                <div style="position:relative;width:840px;height:395px; overflow:auto;">
                                <asp:Table runat="server" ID="eiSearchProvGSTbl" Width="823px" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" Font-Size="11px" Font-Names="Calibri" CssClass="mGrid">
                                        
                                </asp:Table>
                                </div>        
	                    </asp:Panel>
	                </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="eiSearchProvNSTP" runat="server">
                    <ContentTemplate>
	                    <asp:Panel runat="server" ID="eiSearchProvNSPanel" >
	                                <div style="position:relative;width:840px;height:395px; overflow:auto;">
                                <asp:Table runat="server" ID="eiSearchProvNSTbl" Width="823px" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" Font-Size="11px" Font-Names="Calibri" CssClass="mGrid">
                                        
                                </asp:Table>
                                </div>        
	                    </asp:Panel>
	                </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="eiSearchProvOtherTP" runat="server">
                    <ContentTemplate>
	                    <asp:Panel runat="server" ID="eiSearchProvOtherPanel" >
	                                <div style="position:relative;width:840px;height:395px; overflow:auto;">
                                <asp:Table runat="server" ID="eiSearchProvOtherTbl" Width="823px" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" Font-Size="11px" Font-Names="Calibri" CssClass="mGrid">
                                        
                                </asp:Table>
                                </div>        
	                    </asp:Panel>
	                </ContentTemplate>
                </ajaxToolkit:TabPanel>                      

      
            </ajaxToolkit:TabContainer>
            <asp:Table runat="server" ID="eiSearchProvidersByZipBtnTbl" Width="840px" >
                <asp:TableRow>
                    <asp:TableCell>&nbsp;</asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
		            <asp:TableCell  HorizontalAlign="Right"><asp:ImageButton ID="eiNewSearchBtn" runat="server" ImageUrl="images/appBtnNewSrch.png" OnClick="btnClicked" CommandName="newsearch" CssClass="smallappBtn"/></asp:TableCell>
	            </asp:TableRow>                     
            </asp:Table>
        </asp:Panel>

        <asp:Panel ID="searchProvidersByNameResultsPanel" runat="server" Visible="false">                   
            <ajaxToolkit:TabContainer runat="server" ID="eiSearchProvidersByNameTC" Height="403px" ActiveTabIndex="0" Width="850px"> 
                      
                <ajaxToolkit:TabPanel ID="eiSearchProvidersByNameTP" runat="server">
                    <ContentTemplate>
	                    <asp:Panel runat="server" ID="eiSearchProvidersByNameTabPanel"  >
	                                <div style="position:relative;width:840px;height:395px; overflow:auto;">
                                <asp:Table runat="server" ID="eiSearchProvByNameTbl" Width="823px" BorderStyle="Solid" BorderWidth="1" BorderColor="Black"  Font-Size="11px" Font-Names="Calibri" CssClass="mGrid">
                                        
                                </asp:Table>
                                </div>        
	                    </asp:Panel>
	                </ContentTemplate>
                </ajaxToolkit:TabPanel>                                         

      
            </ajaxToolkit:TabContainer>
            <asp:Table runat="server" ID="eiSearchProvidersByNameBtnTbl" Width="840px" >
                <asp:TableRow>
                    <asp:TableCell>&nbsp;</asp:TableCell>
                </asp:TableRow>
	            <asp:TableRow>
		            <asp:TableCell  HorizontalAlign="Right"><asp:ImageButton ID="eiNewSearch2Btn" runat="server" ImageUrl="images/appBtnNewSrch.png" OnClick="btnClicked" CommandName="newsearch" CssClass="smallappBtn"/></asp:TableCell>
	            </asp:TableRow>                        
            </asp:Table>
        </asp:Panel>

    </asp:Panel>
    
    <asp:Panel runat="Server" ID="AnonymousMessagePanel">
        <asp:HyperLink runat="server" ID="lnkLogin" Text="Log In**" NavigateUrl="~/Login.aspx"></asp:HyperLink>        
    </asp:Panel>

</asp:Content>
