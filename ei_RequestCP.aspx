﻿<%@ Page Title="" Language="VB" MasterPageFile="~/myeworkwell_webapp_dashboard.master" AutoEventWireup="false" CodeFile="ei_RequestCP.aspx.vb" Inherits="ei_RequestCP" %>

<asp:Content ID="ei_RequestCP" ContentPlaceHolderID="contentHldr" Runat="Server">

    <asp:Panel runat="server" ID="AuthenticatedMessagePanel">

        <asp:Panel ID="eiRqstOnlChngHdrPanel" runat="server">
            <h1>Request Changes to Physician Panels</h1>
            <div class="divider"></div>
            <div class="appFormLeft"></div> 
        </asp:Panel>

            <asp:Panel ID="panelSearchPanel" runat="server" DefaultButton="ipSearchBtn">
             
                <asp:Table ID="nPanelSearchTbl" runat="server" Width="860" >                 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Panel Name:&nbsp;</asp:TableCell>
                        <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:TextBox ID="companyNameTxt" runat="server" Width="575"  class="txtbox"></asp:TextBox></asp:TableCell>
                        <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:ImageButton ID="srSearchClearBtn" runat="server" ImageUrl="images/appBtnClear.gif" OnClick="ClearForm" CssClass="smallappBtn"/><asp:ImageButton ID="ipSearchBtn" runat="server" ImageUrl="images/appBtnSearch.gif" OnClick="GetPanels" CssClass="smallappBtn"/></asp:TableCell>
                    </asp:TableRow> 
                    <asp:TableRow>
                        <asp:TableCell>&nbsp;</asp:TableCell>
                    </asp:TableRow>                                                                                                                    
                </asp:Table>
        
            </asp:Panel>

            <asp:Panel ID="searchResultsPanel" runat="server" Visible="false">

                <asp:GridView ID="panelSearchRsltsGrd" Runat="server" 
                    AutoGenerateColumns="False" 
                    AllowPaging="True" 
                    PageSize="35" 
                    AlternatingRowStyle-Wrap="False" 
                    RowStyle-Wrap="False" 
                    GridLines="None" 
                    Width="860" 
                    Font-Name="Calibri" 
                    color="#ffffff"
                    CssClass="mGrid"
                    PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt"
                    RowStyle-HorizontalAlign="Left">  
                    <columns>
                        <asp:TemplateField ControlStyle-Width="10" ItemStyle-Width="16" ItemStyle-HorizontalAlign="Center">                                                                   
                            <ItemTemplate>
                                <asp:CheckBox ID="selectionBox" runat="server"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Panel Name">                                                                  
                            <ItemTemplate>                            
                                <asp:HyperLink runat="server" ID="panelLink" Text='<%# Eval("FileName") %>' NavigateUrl='<%#Eval("FileDir") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:boundfield datafield="UploadDate"  HeaderText="Date Uploaded" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="100" HeaderStyle-HorizontalAlign="Center"/>
                                
                    </columns>
                </asp:GridView>

                <asp:Table runat="server" ID="noResultsFoundTbl" Width="860" Font-Name="Calibri" Visible="false" CssClass="mGrid">
                    <asp:TableFooterRow>
                        <asp:TableHeaderCell Width="400" HorizontalAlign="Center">Panel Name</asp:TableHeaderCell>
                        <asp:TableHeaderCell Width="200" HorizontalAlign="Center">Date Uploaded</asp:TableHeaderCell>
                    </asp:TableFooterRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="6" HorizontalAlign="Left" BackColor="LightYellow"><asp:Label ID="Label1" runat="server" Font-Bold="true" BackColor="LightYellow" ForeColor="Black" Text="No Physcian Panels found."></asp:Label></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>     
                 
                <asp:Table ID="submitSelectedBtnsTbl" runat="server" Width="860">
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="2"><asp:ImageButton ID="cancelSelectedBtn" runat="server" ImageUrl="images/appBtnCancel.gif" OnClick="CancelSelection" CssClass="smallappBtn"/><asp:ImageButton ID="submitSelectedBtn" runat="server" ImageUrl="images/appBtnSubmit.gif" OnClick="GetSelectedPanels" CssClass="smallappBtn"/></asp:TableCell>                    
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
           
            <asp:Panel ID="selectedPanelsPanel" runat="server" Visible="false">
                <asp:GridView ID="selectedPanelsGrd" Runat="server" 
                    AutoGenerateColumns="False" 
                    AllowPaging="True"  
                    PageSize="200" 
                    AlternatingRowStyle-Wrap="True" 
                    RowStyle-Wrap="True" 
                    GridLines="None" 
                    Width="860" 
                    Font-Name="Calibri" 
                    color="#ffffff"
                    CssClass="mGrid"
                    PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt"
                    RowStyle-HorizontalAlign="Left">
                    <columns>
                        <asp:TemplateField HeaderText="Panel Name" ControlStyle-Font-Size="12px" ControlStyle-Font-Names="Calibri" 
                                            HeaderStyle-Font-Size="12px" HeaderStyle-Font-Names="Calibri" ItemStyle-VerticalAlign="Top"
                                            ControlStyle-Width="350"> 
                            <ItemTemplate>
                                <asp:Label ID="panelNameLbl" runat="server" Text='<%# Eval("FileName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Changes Needed" ControlStyle-Font-Size="12px" ControlStyle-Font-Names="Calibri" HeaderStyle-Font-Size="12px" HeaderStyle-Font-Names="Calibri">                                                                  
                            <ItemTemplate>                            
                                <asp:TextBox ID="changesDesc" runat="server" class="txtbox" Width="500" Height="50" TextMode="MultiLine"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>                                
                    </columns>
                </asp:GridView> 
                <asp:Table ID="submitRequestBtnsTbl" runat="server" Width="860">
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Wrap="false" HorizontalAlign="Left" ColumnSpan="2"><asp:ImageButton ID="cancelRequestBtn" runat="server" ImageUrl="images/appBtnCancel.gif" OnClick="CancelRequest" CssClass="smallappBtn"/><asp:ImageButton ID="submitRequestBtn" runat="server" ImageUrl="images/appBtnSubmit.gif" OnClick="SubmitRequest" CssClass="smallappBtn"/></asp:TableCell>                    
                    </asp:TableRow>
                </asp:Table>   

                <asp:Table ID="noChangeDescErrorTbl" runat="server" Visible="false">                
                    <asp:TableRow>
                        <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:Label ID="noChangesDescError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter a description of changes for ALL panels above. *<BR>"></asp:Label></asp:TableCell>                    
                    </asp:TableRow>
                </asp:Table> 
            </asp:Panel>

            <asp:Panel runat="Server" ID="noneSelectedErrorPanel" Visible="false">
                    <asp:Table ID="noneSelectedErrorTbl" runat="server">                
                    <asp:TableRow>
                        <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:Label ID="noneSelectedError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST select at least ONE location above. *<BR>"></asp:Label></asp:TableCell>                    
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>


            <asp:Panel runat="Server" ID="requestSumbitPanel" Visible="false" Width="860">
                <div id="halfPageRight1">
                    <h1>Your Requst has been Sent!</h1>
				    <div class="divider"></div>
			    </div>
			    <div class="clearBoth"></div>
                <label>Click here to go to the </label><asp:HyperLink ID="savedReturnToMainPageLnk" runat="server" Font-Underline="true" ForeColor="LightSkyBlue" NavigateUrl="LandingPage.aspx">main page.</asp:HyperLink>             
            </asp:Panel>
  

        
    </asp:Panel>
    
    <asp:Panel runat="Server" ID="AnonymousMessagePanel">
        <asp:HyperLink runat="server" ID="lnkLogin" Text="Log In**" NavigateUrl="~/Login.aspx"></asp:HyperLink>        
    </asp:Panel>

</asp:Content>

