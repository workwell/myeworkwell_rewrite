﻿Imports System.Data
Imports System.Data.Odbc
Imports System.IO
Imports System.Net
Imports System.Web.Mail
Imports System.Net.Mail

Partial Class ei_RequestCP
    Inherits System.Web.UI.Page




    Dim panelURLs As New DataTable()
    Dim selectedPanels As New DataTable()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserType") = 2 Then
            Response.Redirect("iiRoadMap.aspx")
        ElseIf Session("UserType") = 3 Then
            Response.Redirect("ciStatusReport.aspx")
        ElseIf Session("UserType") = 4 Then
            Response.Redirect("MACInsurance.aspx")
        ElseIf Session("UserType") = 5 Then
            Response.Redirect("AholdIT.aspx")
        ElseIf Session("UserType") = 6 Then
            Response.Redirect("Nationwide.aspx")
        ElseIf Session("UserType") = 7 Then
            Response.Redirect("SynergyComp.aspx")
        End If

        If Request.IsAuthenticated Then
            If Not IsPostBack Then
                GetPanels()
            End If
            AuthenticatedMessagePanel.Visible = True
            AnonymousMessagePanel.Visible = False
        Else
            AuthenticatedMessagePanel.Visible = False
            AnonymousMessagePanel.Visible = True
        End If

    End Sub

    Sub GetPanels()

        panelURLs = New DataTable()
        panelURLs.Columns.Add("FileName")
        panelURLs.Columns.Add("FileDir")
        panelURLs.Columns.Add("UploadDate")

        Dim sFileName As String = ""
        Dim sHeaderName As String = ""
        Dim iPartyID As Integer = Session("PartyID")


        searchResultsPanel.Visible = True
        noResultsFoundTbl.Visible = False

        Dim DocumentVirtualPath As String = "\PANELS\" & iPartyID.ToString & "\"

        GetFiles("\PANELS\" & iPartyID.ToString & "\")

        panelSearchRsltsGrd.DataSource = panelURLs
        panelSearchRsltsGrd.DataBind()
        'End If

        If panelURLs.Rows.Count = 0 Then
            noResultsFoundTbl.Visible = True
        End If

    End Sub

    Public Sub GetFiles(ByVal path As String)

        Dim myPath As String = Server.MapPath(path)

        If File.Exists(myPath) Then
            ' This path is a file 
            ProcessFile(myPath)
        ElseIf Directory.Exists(myPath) Then
            ' This path is a directory 
            ProcessDirectory(myPath)
        End If

    End Sub

    Public Sub ProcessFile(ByVal path As String)

        Dim fi As New FileInfo(path)
        Dim companyNameStr As String = companyNameTxt.Text.Trim

        If String.IsNullOrEmpty(companyNameTxt.Text) = False Then
            If fi.Name.ToLower.Contains(companyNameStr) And fi.FullName.ToLower.Contains(".pdf") Then

                Dim dr As DataRow = panelURLs.NewRow()

                dr("FileName") = fi.Name
                dr("FileDir") = fi.FullName.Replace("D:\Hosting\6546477\html", "")
                dr("UploadDate") = fi.LastWriteTime.ToShortDateString
                panelURLs.Rows.Add(dr)

            End If
        Else
            If fi.FullName.ToLower.Contains(".pdf") Then

                Dim dr As DataRow = panelURLs.NewRow()

                dr("FileName") = fi.Name
                dr("FileDir") = fi.FullName.Replace("D:\Hosting\6546477\html", "")
                dr("UploadDate") = fi.LastWriteTime.ToShortDateString
                panelURLs.Rows.Add(dr)

            End If
        End If

    End Sub

    Public Sub ProcessDirectory(ByVal targetDirectory As String)

        Dim fileEntries As String() = Directory.GetFiles(targetDirectory)
        Dim subdirectoryEntries As String() = Directory.GetDirectories(targetDirectory)

        ' Process the list of files found in the directory. 
        For Each fileName As String In fileEntries
            ProcessFile(fileName)
        Next

        ' Recurse into subdirectories of this directory. 
        For Each subdirectory As String In subdirectoryEntries
            ProcessDirectory(subdirectory)
        Next

    End Sub

    Sub SubmitRequest()

        Dim body As New StringBuilder
        Dim message As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage
        Dim sPanelChanges As String = ""
        Dim iPartyID As Integer = Session("PartyID")
        Dim bChangeDescError As Boolean = False
        Dim values As Boolean() = New Boolean(panelSearchRsltsGrd.PageSize - 1) {}
        Dim from_email_addy As String = ""


        Dim objSqlCmd As New OdbcCommand
        Dim objSqlDataRdr As OdbcDataReader = Nothing
        Dim strConnection As String = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;" 'ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()
        Dim objConnection As New OdbcConnection(strConnection)

        Try

            objConnection.Open()
            objSqlCmd.Connection = objConnection

            objSqlCmd.CommandText = "SELECT SEMAILADDRESS FROM USER_INFO WHERE SUSERNAME = '" & Session("UserName") & "' "

            objSqlCmd.CommandType = Data.CommandType.Text
            objSqlDataRdr = objSqlCmd.ExecuteReader()

            If objSqlDataRdr.Read() Then
                from_email_addy = myCStr(objSqlDataRdr.Item(0))
            End If

            objSqlDataRdr.Close()

        Catch ex As Exception
            Response.Write("Can't load Web page " & ex.Message)
        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try

        message.IsBodyHtml = True
        message.Subject = "Change to Panel Request - myeWorkWell.com"
        message.From = New MailAddress("info@myeworkwell.com", "myeWorkWell Info Request")
        message.To.Add(New MailAddress("customerservice@eworkwell.com", "Customer Service"))
        message.To.Add(New MailAddress("notifications@eworkwell.com", "IT"))

        For i As Integer = 0 To selectedPanelsGrd.Rows.Count - 1

            Dim row As GridViewRow = selectedPanelsGrd.Rows(i)
            Dim sFileName As String = DirectCast(row.FindControl("panelNameLbl"), Label).Text
            Dim sChangesDesc As String = DirectCast(row.FindControl("changesDesc"), TextBox).Text

            If String.IsNullOrEmpty(sChangesDesc.Trim) = True Then
                bChangeDescError = True
                i = selectedPanelsGrd.Rows.Count + 1
            Else
                sPanelChanges = sPanelChanges & "PANEL FILE NAME: " & sFileName & "<BR>" & "REQUESTED CHANGES TO PANEL: " & sChangesDesc & " <BR><BR><BR> PLEASE SEND A COPY OF THE UPDATED PANEL TO: " & from_email_addy & "<BR><BR>"
                'message.Attachments.Add(New Attachment(Server.MapPath("/PANELS/" & iPartyID & "/" & sFileName)))
            End If

        Next

        With body
            .Append("The following panels(s) have been requested to be changed by the employer." & "<br/><br/>")
            .Append(sPanelChanges)
            .Append("<hr />")
            .Append("<font size='2' color='#cccccc'>")
            .Append("<hr />")
            .Append("www.myeworkwell.com")
            .Append("<br>")
            .Append(Date.Now.AddHours(3))
            .Append("</font>")
        End With
        message.Body = body.ToString

        'relay-hosting.secureserver.net is the valid SMTP server of godaddy.com
        Dim mailClient As SmtpClient = New SmtpClient("dedrelay.secureserver.net")

        If bChangeDescError = False Then
            Try
                mailClient.Send(message)
            Catch

            End Try
            message.Dispose()

            For i = 0 To panelSearchRsltsGrd.PageCount - 1

                '********
                ' this is to clear all selected locations session vars, i can't remove the session vars wirh remove (above), so I'm just seting everything to false for now.
                For x As Integer = 0 To panelSearchRsltsGrd.Rows.Count - 1
                    values(x) = False
                Next
                Session("eiRequestCP" & i) = values
            Next

            noChangeDescErrorTbl.Visible = False
            selectedPanelsPanel.Visible = False
            eiRqstOnlChngHdrPanel.Visible = False
            requestSumbitPanel.Visible = True

        Else
            noChangeDescErrorTbl.Visible = True
        End If

    End Sub

    Sub CancelRequest()

        panelSearchPanel.Visible = True
        searchResultsPanel.Visible = True
        selectedPanelsPanel.Visible = False
        noChangeDescErrorTbl.Visible = False

    End Sub

    Sub GetSelectedPanels()

        Dim selectedLocs As New DataTable()
        Dim values As Boolean() = New Boolean(panelSearchRsltsGrd.PageSize - 1) {}
        Dim checkBx As CheckBox
        Dim curPageOneChecked As Integer = 0
        Dim bOneChecked As Integer = 0
        Dim currentGrdPage As Integer = panelSearchRsltsGrd.PageIndex

        noneSelectedErrorPanel.Visible = False

        'get the selections current page that user is on
        For i As Integer = 0 To panelSearchRsltsGrd.Rows.Count - 1
            checkBx = DirectCast(panelSearchRsltsGrd.Rows(i).FindControl("selectionBox"), CheckBox)
            If checkBx IsNot Nothing Then
                values(i) = checkBx.Checked
                If values(i) Then
                    curPageOneChecked = 1
                End If
            End If
        Next

        If curPageOneChecked = 1 Then
            Session("eiRequestCP" & panelSearchRsltsGrd.PageIndex) = values
        End If

        ' loop thru the existing Session("eiRequestCP") and see if any are checked

        For i = 0 To panelSearchRsltsGrd.PageCount - 1

            If Session("eiRequestCP" & i) IsNot Nothing Then

                'set panelSearchRsltsGrd to page that the checked locations were

                GetPanels()
                panelSearchRsltsGrd.PageIndex = i
                panelSearchRsltsGrd.DataBind()

                Dim prevValues As Boolean() = DirectCast(Session("eiRequestCP" & i), Boolean())

                For x = 0 To panelSearchRsltsGrd.Rows.Count - 1
                    If prevValues(x) Then
                        bOneChecked = 1
                    End If
                Next
            End If
        Next

        If bOneChecked = 1 Then

            Dim selectedPanels As New DataTable()
            selectedPanels.Columns.Add("FileName")

            For i = 0 To panelSearchRsltsGrd.PageCount - 1

                If Session("eiRequestCP" & i) IsNot Nothing Then

                    'set viewActiveLocationsGrd to page that the checked locations were

                    GetPanels()
                    panelSearchRsltsGrd.PageIndex = i
                    panelSearchRsltsGrd.DataBind()

                    Dim prevValues As Boolean() = DirectCast(Session("eiRequestCP" & i), Boolean())

                    For x = 0 To panelSearchRsltsGrd.Rows.Count - 1
                        If prevValues(x) Then

                            Dim sFileName As String = DirectCast(panelSearchRsltsGrd.Rows(x).FindControl("panelLink"), HyperLink).Text
                            Dim dr As DataRow = selectedPanels.NewRow()
                            dr("FileName") = sFileName
                            selectedPanels.Rows.Add(dr)

                        End If
                    Next
                End If

                'now get back to the current page that they were on before submiting the selections
                GetPanels()
                panelSearchRsltsGrd.PageIndex = currentGrdPage
                panelSearchRsltsGrd.DataBind()

                'keep to current page's selections checked if they click back

                Dim existingValues As Boolean() = DirectCast(Session("eiRequestCP" & panelSearchRsltsGrd.PageIndex), Boolean())

                For x = 0 To panelSearchRsltsGrd.Rows.Count - 1
                    If existingValues(x) Then
                        DirectCast(panelSearchRsltsGrd.Rows(x).FindControl("selectionBox"), CheckBox).Checked = True
                    End If
                Next

            Next

            selectedPanelsGrd.DataSource = selectedPanels
            selectedPanelsGrd.DataBind()

            selectedPanelsPanel.Visible = True
            searchResultsPanel.Visible = False
            panelSearchPanel.Visible = False

        Else
            'show error
            noneSelectedErrorPanel.Visible = True
            GetPanels()

            panelSearchRsltsGrd.PageIndex = currentGrdPage
            panelSearchRsltsGrd.DataBind()

        End If

    End Sub

    Sub CancelSelection()

        Dim values As Boolean() = New Boolean(panelSearchRsltsGrd.PageSize - 1) {}

        For i = 0 To panelSearchRsltsGrd.PageCount - 1

            ' this is to clear all selected locations session vars, i can't remove the session vars wirh remove (above), so I'm just seting everything to false for now.
            For x As Integer = 0 To panelSearchRsltsGrd.Rows.Count - 1
                values(x) = False
            Next
            Session("eiRequestCP" & i) = values
        Next

        GetPanels()

        panelSearchRsltsGrd.PageIndex = 0
        panelSearchRsltsGrd.DataBind()

    End Sub

    Sub ClearForm()

        Dim values As Boolean() = New Boolean(panelSearchRsltsGrd.PageSize - 1) {}

        companyNameTxt.Text = ""

        For i = 0 To panelSearchRsltsGrd.PageCount - 1

            ' this is to clear all selected locations session vars, i can't remove the session vars wirh remove (above), so I'm just seting everything to false for now.
            For x As Integer = 0 To panelSearchRsltsGrd.Rows.Count - 1
                values(x) = False
            Next
            Session("eiRequestCP" & i) = values

        Next

        GetPanels()

        panelSearchRsltsGrd.PageIndex = 0
        panelSearchRsltsGrd.DataBind()

    End Sub

    Protected Sub panelSearchRsltsGrd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles panelSearchRsltsGrd.PageIndexChanging

        noneSelectedErrorPanel.Visible = False

        Dim d As Integer = panelSearchRsltsGrd.PageCount
        Dim values As Boolean() = New Boolean(panelSearchRsltsGrd.PageSize - 1) {}
        Dim chb As CheckBox
        Dim isChecked As Integer = 0

        Dim count As Integer = 0
        For i As Integer = 0 To panelSearchRsltsGrd.Rows.Count - 1
            chb = DirectCast(panelSearchRsltsGrd.Rows(i).FindControl("selectionBox"), CheckBox)
            If chb IsNot Nothing Then
                values(i) = chb.Checked
                isChecked = 1
            End If
        Next

        If isChecked = 1 Then
            Session("eiRequestCP" & panelSearchRsltsGrd.PageIndex) = values
        End If

        GetPanels()

        panelSearchRsltsGrd.PageIndex = e.NewPageIndex
        panelSearchRsltsGrd.DataBind()

        If Session("eiRequestCP" & e.NewPageIndex) IsNot Nothing Then

            Dim prevValues As Boolean() = DirectCast(Session("eiRequestCP" & e.NewPageIndex), Boolean())

            For x = 0 To panelSearchRsltsGrd.Rows.Count - 1

                If prevValues(x) Then
                    DirectCast(panelSearchRsltsGrd.Rows(x).FindControl("selectionBox"), CheckBox).Checked = True
                End If

            Next
        End If

    End Sub

    Function myCStr(ByVal test As Object) As String

        Dim sReturnStr As String = ""

        If IsDBNull(test) Then
            sReturnStr = ""
        Else
            sReturnStr = CStr(test)
        End If

        If StrComp(sReturnStr, "#") = 0 Then
            sReturnStr = ""
        End If

        myCStr = sReturnStr

    End Function


End Class
