﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Odbc;
using System.Data;
using Telerik.Web.UI;
public partial class LandingPage : System.Web.UI.Page
{
    public void Page_Load(object sender, EventArgs e)
    {
        //this will determine they type of the user and send them where they need to go.
        if (Convert.ToInt32(Session["UserType"]) == 2)
            Response.Redirect("iiRoadMap.aspx");
        else if (Convert.ToInt32(Session["UserType"]) == 3)
            Response.Redirect("ciStatusReport.aspx");
        else if (Convert.ToInt32(Session["UserType"]) == 4)
            Response.Redirect("MACInsurance.aspx");
        else if (Convert.ToInt32(Session["UserType"]) == 5)
            Response.Redirect("AholdIT.aspx");
        else if (Convert.ToInt32(Session["UserType"]) == 6)
            Response.Redirect("Nationwide.aspx");
        else if (Convert.ToInt32(Session["UserType"]) == -96)
            Response.Redirect("dv_ciStatusReport.aspx");
        else if (Convert.ToInt32(Session["UserType"]) == -99)
            Response.Redirect("meww_createClient.aspx");

        if (Page.Request.IsAuthenticated)
        {
            AuthenticatedMessagePanel.Visible = true;
            AnonymousMessagePanel.Visible = false;

            if (Convert.ToString(Session["InsCarrier"]) == "N")
                GetAccountRep();

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["InsCarrier"]) == "N") // Other user type logs in?
                {
                    ShowAccountSelection();
                    acctViewingDrp.SelectedValue = Session["CustAccountID"].ToString();
                }
                else if (Convert.ToString(Session["InsCarrier"]) == "I") //Insurance Logs in
                {
                    emp_acct_combo.Visible = true;
                    ShowInsuranceCoSelection();
                    acctViewingDrp.SelectedValue = Session["CustAccountID"].ToString();
                }
                else if (Convert.ToString(Session["InsCarrier"]) == "E") //Employer logins
                {
                    emp_acct_combo.Visible = true;
                    if (Convert.ToString(Session["MultipleLocs"]) == "1")
                    {
                        if (Convert.ToString(Session["CustAccountID"]) == "")
                        {
                            ShowEmployerCoSelection();
                        }
                        else
                        {
                            ShowEmployerMultiCoSelection();
                        }
                    }
                    else
                    {
                        ShowEmployerCoSelection();
                    }
                }
                else
                {
                    ShowInsuranceRelatedAccts();
                    if (Convert.ToInt32(Session["PartyID"]) != 0)
                    {
                        ins_acct_combo.SelectedValue = Session["PartyID"].ToString();
                    }
                    else
                    {
                        ins_acct_combo.SelectedIndex = 0;
                        Session["PartyID"] = ins_acct_combo.SelectedValue;
                    }
                }
            }
        }
        else
        {
            AuthenticatedMessagePanel.Visible = false;
            AnonymousMessagePanel.Visible = true;            
        }
    }

    public void ins_acct_combo_blur(object sender, EventArgs e)
    {
        emp_acct_combo.Items.Clear();
    }

    public void ins_acct_combo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(Session["InsCarrier"]) == "I")
        {
            Session["CustAccountID"] = ins_acct_combo.SelectedValue;
            Session["CarrierID"] = ins_acct_combo.SelectedValue;

            emp_acct_combo.ClearSelection();
            emp_acct_combo.Text = String.Empty;

            OdbcCommand objSqlCmd = new OdbcCommand();
            OdbcDataReader objSqlDataRdr = null;
            string strConnection = ConnectionStrings.MySqlConnections();
            OdbcConnection objConnection = new OdbcConnection(strConnection);
            objConnection.Open();
            string sql = "SELECT PARTY_ID FROM HZ_CUST_ACCOUNTS WHERE CUST_ACCOUNT_ID = " + Convert.ToString(Session["CustAccountID"]);
            objSqlCmd = new OdbcCommand(sql, objConnection);
            objSqlDataRdr = objSqlCmd.ExecuteReader();

            if (objSqlDataRdr.Read())
            {
                Session["PartyID"] = objSqlDataRdr.GetValue(0);
                GetAccountRep();
            }
            objConnection.Close();
            ShowEmployerRelatedAccts();
        }
        else if (Session["InsCarrier"].ToString() == "E") //Employer lookup
        {
            Session["CustAccountID"] = ins_acct_combo.SelectedValue;
            OdbcCommand objSqlCmd = new OdbcCommand();
            OdbcDataReader objSqlDataRdr = null;
            string strConnection = ConnectionStrings.MySqlConnections();
            OdbcConnection objConnection = new OdbcConnection(strConnection);
            objConnection.Open();
            string sql = "SELECT PARTY_ID FROM HZ_CUST_ACCOUNTS WHERE CUST_ACCOUNT_ID = " + Convert.ToString(Session["CustAccountID"]);
            objSqlCmd = new OdbcCommand(sql, objConnection);
            objSqlDataRdr = objSqlCmd.ExecuteReader();

            if (objSqlDataRdr.Read())
            {
                Session["PartyID"] = objSqlDataRdr.GetValue(0);
            }
            objConnection.Close();
        }
        else if (ins_acct_combo.SelectedItem != null)
        {
            Session["PartyID"] = ins_acct_combo.SelectedValue;   
        }
    }
     public void  ins_employer_combo_SelectedIndexChanged(object sender, EventArgs e)
     {
        if (Session["InsCarrier"].ToString() == "I")
        { 
            Session["CustAccountID"] = emp_acct_combo.SelectedValue;
        }
        else if (ins_acct_combo.SelectedItem != null)
            Session["PartyID"] = ins_acct_combo.SelectedValue;
     }
    //Atm this function is 
    public void changeEmployerInfo()
    {
        Session["CustAccountID"] = acctViewingDrp.SelectedValue;

        OdbcCommand objSqlCmd = new OdbcCommand();
        OdbcDataReader objSqlDataRdr = null;
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
            
        int mult_accts = Convert.ToInt32(Session["mult_accts"]);

        try
        {
            objConnection.Open();

            if (mult_accts >= 1)
            {
                // get the cust_account_ids for the current partyID
                string sql = "SELECT COUNT(DISTINCT(CUST_ACCOUNT_ID)) FROM ACCOUNT_PRIVS WHERE IUSERID = " + Session["UserID"] 
                + " AND CUST_ACCOUNT_ID <> 0 AND PARTY_ID = " + Convert.ToString(Session["PartyID"]) 
                + " AND CUST_ACCOUNT_ID = " + Convert.ToString(Session["CustAccountID"]);
                objSqlCmd = new OdbcCommand(sql, objConnection);
                objSqlDataRdr = objSqlCmd.ExecuteReader();

                if (objSqlDataRdr.Read())
                {
                    Session["mult_locs"] = objSqlDataRdr.GetValue(0);
                }
                objSqlDataRdr.Close();

                sql = "SELECT CUST_ACCOUNT_ID FROM ACCOUNT_PRIVS WHERE IUSERID = " + Session["UserID"]
                + " AND CUST_ACCOUNT_ID <> 0 AND PARTY_ID = " + Convert.ToString(Session["PartyID"]) 
                + " AND CUST_ACCOUNT_ID = " + Convert.ToString(Session["CustAccountID"]);
                objSqlCmd = new OdbcCommand(sql, objConnection);
                objSqlDataRdr = objSqlCmd.ExecuteReader();

                if (Convert.ToInt32(Session["mult_locs"]) == 1)
                {
                     if (objSqlDataRdr.Read())
                {
                        Session["CustAccountID"] = objSqlDataRdr.GetValue(0);
                     }
                }
                else if(Convert.ToInt32(Session["mult_locs"]) > 1)
                {
                    string tmp_cust_acct_ids = "";
                    while (objSqlDataRdr.Read())
                    {
                        tmp_cust_acct_ids += Convert.ToString(objSqlDataRdr.GetValue(0)) + ",";
                    }
                    if (tmp_cust_acct_ids.Length > 1)
                    {
                        Session["CustAccount_IDs"] = tmp_cust_acct_ids.Substring(0, tmp_cust_acct_ids.Length - 1);
                    }
                }
                else
                {
                    Session["CustAccountID"] = null;
                    Session["CustAccount_IDs"] = null;
                }
            }
            objSqlDataRdr.Close();
            objSqlCmd.Dispose();
        }
        catch(Exception ex)
        {
            Response.Write("Can't load Web page: " + ex.Message);
        }
        finally
        {
            objConnection.Close();
            objConnection.Dispose();
        }

    }

    public void changeAcctInfo(object sender, EventArgs e)
    {
        Session["PartyID"] = acctViewingDrp.SelectedValue; //EDH changed to use customerAccountID
        OdbcCommand objSqlCmd = new OdbcCommand();
        OdbcDataReader objSqlDataRdr = null;
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        int mult_accts  = Convert.ToInt32(Session["mult_accts"]);

        try
        {
            objConnection.Open();

            if (mult_accts >= 1)
            {
                string sql = "SELECT COUNT(DISTINCT(CUST_ACCOUNT_ID)) FROM ACCOUNT_PRIVS WHERE IUSERID = " + Session["UserID"]
                    + " AND CUST_ACCOUNT_ID <> 0 AND PARTY_ID = " + Convert.ToString(Session["PartyID"]) ;
                objSqlCmd = new OdbcCommand(sql, objConnection);
                objSqlDataRdr = objSqlCmd.ExecuteReader();

                if (objSqlDataRdr.Read())
                    Session["mult_locs"] = objSqlDataRdr.GetValue(0);

                objSqlDataRdr.Close();

                sql = "SELECT CUST_ACCOUNT_ID FROM ACCOUNT_PRIVS WHERE IUSERID = " + Session["UserID"] 
                    + " AND CUST_ACCOUNT_ID <> 0 AND PARTY_ID = " + Convert.ToString(Session["PartyID"]);

                objSqlCmd = new OdbcCommand(sql, objConnection);
                objSqlDataRdr = objSqlCmd.ExecuteReader();

                if (Convert.ToInt32(Session["mult_locs"]) == 1)
                {
                    if (objSqlDataRdr.Read())
                        Session["CustAccountID"] = objSqlDataRdr.GetValue(0);
                }
                else if (Convert.ToInt32(Session["mult_locs"]) > 1)
                {
                    string tmp_cust_acct_ids = "";

                    while (objSqlDataRdr.Read())
                    {
                        tmp_cust_acct_ids += objSqlDataRdr.GetValue(0).ToString() + ",";
                    }

                    if (tmp_cust_acct_ids.Length > 1)
                        Session["CustAccount_IDs"] = tmp_cust_acct_ids.Substring(0, tmp_cust_acct_ids.Length - 1);
                }
                else
                {
                    Session["CustAccountID"] = null;
                    Session["CustAccount_IDs"] = null;
                }
            }

            objSqlDataRdr.Close();
            objSqlCmd.Dispose();
        }
        catch (Exception ex)
        {
            Response.Write("Can't load Web page: " + ex.Message);
        }
        finally
        {
            objConnection.Close();
            objConnection.Dispose();
        }
    }
    public void ShowInsuranceRelatedAccts()
    {
        string sql = string.Empty;

        if (Convert.ToString(Session["MotoristsSub"]) == "0")
        {
            sql = "SELECT HP.PARTY_ID, HP.PARTY_NAME " 
                + " FROM HZ_CUST_ACCOUNTS HCA_INS INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_INS.CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID " 
                + "	 INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID " 
                + "	 INNER JOIN HZ_PARTIES HP ON HCA_EMP.PARTY_ID = HP.PARTY_ID " 
                + " WHERE HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' " 
                + "	  AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' " 
                + "	  AND HCARA.STATUS = 'A' " 
                + "	  AND HCA_INS.PARTY_ID = (SELECT PARTY_ID  " 
                + "		FROM ACCOUNT_PRIVS " 
                + "	    WHERE IUSERID = " + Convert.ToString(Session["UserID"]) + " )" 
                + "	  AND HCA_EMP.STATUS = 'A' " 
                + " GROUP BY HP.PARTY_ID, HP.PARTY_NAME " 
                + " ORDER BY HP.PARTY_NAME ";
        }
        else
        {
            sql = "SELECT HP.PARTY_ID, HP.PARTY_NAME " 
                + " FROM HZ_CUST_ACCOUNTS HCA_INS INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_INS.CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID " 
                + "	 INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID " 
                + "	 INNER JOIN HZ_PARTIES HP ON HCA_EMP.PARTY_ID = HP.PARTY_ID " 
                + " WHERE HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' " 
                + "	  AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' " 
                + "	  AND HCARA.STATUS = 'A' " 
                + "	  AND HCA_INS.PARTY_ID = (SELECT PARTY_ID  " 
                + "				FROM ACCOUNT_PRIVS " 
                + "				WHERE IUSERID = " + Convert.ToString(Session["UserID"]) + " )" 
                + "	  AND HCA_EMP.STATUS = 'A' " 
                + " AND HCA_INS.CUST_ACCOUNT_ID = " + Convert.ToString(Session["MotoristsSub"]) 
                + " GROUP BY HP.PARTY_ID, HP.PARTY_NAME " 
                + " ORDER BY HP.PARTY_NAME ";
        }

        DataTable dt = new DataTable();
        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        OdbcDataAdapter ObjDataAdapter = new OdbcDataAdapter(sql, objConnection);
        RadComboBoxItem item = new RadComboBoxItem();

        //This si for the 2/7 demo
        if (Session["UserName"].ToString() == "NW2@EWORKWELL.COM")
        {
            ins_acct_combo.Items.Clear();
            ins_acct_combo.ClearSelection();

            item = new RadComboBoxItem();
            item.Text = "SELECT AN ACCOUNT";
            item.Value = Session["InsPartyID"].ToString();

            item.Attributes.Add("ACCT_NAME", "SELECT AN ACCOUNT");
            ins_acct_combo.Items.Add(item);
            item.DataBind();

            item = new RadComboBoxItem();
            item.Text = "ABSOLUTE ABSTRACT AGENCY INC";
            item.Value = "730433";

            string name = "ABSOLUTE ABSTRACT AGENCY INC";
            item.Attributes.Add("ACCT_NAME", name.ToString());

            ins_acct_combo.Items.Add(item);
            item.DataBind();
            multiAcctPanel.Visible = true;
            acctViewingDrp.Visible = false;
            ins_acct_combo.Visible = true;
        }
        else
        {
            try
            {

                string username = Session["UserName"].ToString();
                objConnection.Open();
                ObjDataAdapter.Fill(dt);

                ins_acct_combo.Items.Clear();
                ins_acct_combo.ClearSelection();

                item = new RadComboBoxItem();
                item.Text = "SELECT AN ACCOUNT";
                item.Value = Session["InsPartyID"].ToString();

                item.Attributes.Add("ACCT_NAME", "SELECT AN ACCOUNT");
                ins_acct_combo.Items.Add(item);

                item.DataBind();

                foreach (DataRow dataRow in dt.Rows)
                {
                    item = new RadComboBoxItem();
                    item.Text = dataRow["PARTY_NAME"].ToString();
                    item.Value = dataRow["PARTY_ID"].ToString();

                    string name = dataRow["PARTY_NAME"].ToString();
                    item.Attributes.Add("ACCT_NAME", name.ToString());

                    ins_acct_combo.Items.Add(item);
                    item.DataBind();
                }
                multiAcctPanel.Visible = true;
                acctViewingDrp.Visible = false;
                ins_acct_combo.Visible = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objConnection.Close();
                ObjDataAdapter.Dispose();
                objConnection.Dispose();
            }
        }
    }
    public void ShowInsuranceCoSelection()
    {
        string sql = "SELECT HCA_INS.CUST_ACCOUNT_ID, ACCOUNT_NAME AS PARTY_NAME " 
            + "FROM HZ_CUST_ACCOUNTS HCA_INS " 
            + "WHERE HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' " 
            + "	  AND HCA_INS.STATUS = 'A' " 
            + "	  AND HCA_INS.PARTY_ID = (SELECT PARTY_ID  " 
            + "				FROM ACCOUNT_PRIVS " 
            + "				WHERE IUSERID = " + Convert.ToString(Session["UserID"]) + " )";

        DataTable dt = new DataTable();

        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        OdbcDataAdapter ObjDataAdapter = new OdbcDataAdapter(sql, objConnection);
        RadComboBoxItem item = new RadComboBoxItem();

        try
        {
            objConnection.Open();
            ObjDataAdapter.Fill(dt);

            ins_acct_combo.Items.Clear();
            ins_acct_combo.ClearSelection();
            item = new RadComboBoxItem();
            item.DataBind();

            foreach (DataRow dataRow in dt.Rows)
            {
                item = new RadComboBoxItem();
                item.Text = dataRow["PARTY_NAME"].ToString();
                item.Value = dataRow["CUST_ACCOUNT_ID"].ToString();
                string name = dataRow["PARTY_NAME"].ToString();
                item.Attributes.Add("ACCT_NAME", name.ToString());                
                ins_acct_combo.Items.Add(item);
                item.DataBind();
            }
            multiAcctPanel.Visible = true;
            acctViewingDrp.Visible = false;
            ins_acct_combo.Visible = true;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            objConnection.Close();
            ObjDataAdapter.Dispose();
            objConnection.Dispose();
        }  

    }
    public void ShowEmployerMultiCoSelection123()
    {
        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        string sql = "SELECT PARTY_ID FROM ACCOUNT_PRIVS WHERE IUSERID = " + Convert.ToString(Session["UserID"]) + " AND USER_TYPE=10";
        OdbcDataAdapter ObjDataAdapter = new OdbcDataAdapter(sql, objConnection);
        objConnection.Open();
        DataTable dt = new DataTable();
        ObjDataAdapter.Fill(dt);

        if (dt.Rows.Count > 0)
        {
            foreach ( DataRow dr in dt.Rows)
            {
                sql = "SELECT HCA_INS.CUST_ACCOUNT_ID, HCA_INS.ACCOUNT_NAME FROM HZ_CUST_ACCOUNTS HCA_INS INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA " 
                    + " ON HCA_INS.CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID " 
                    + " INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID " 
                    + " INNER JOIN HZ_PARTIES HP ON HCA_EMP.PARTY_ID = HP.PARTY_ID " 
                    + " WHERE  HCA_INS.ATTRIBUTE_CATEGORY = 'EMPLOYER' AND HCA_EMP.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' AND HCARA.STATUS = 'A' " 
                    + " AND HCA_INS.PARTY_ID = " + dr["PARTY_ID"]
                    + " AND HCA_EMP.STATUS = 'A' ORDER BY HP.PARTY_NAME";

                OdbcDataAdapter ObjDataAdapter1 = new OdbcDataAdapter(sql, objConnection);
                DataTable dt1 = new DataTable();
                ObjDataAdapter1.Fill(dt1);

                RadComboBoxItem item = new RadComboBoxItem();
                item.DataBind();
                foreach (DataRow dr1 in dt1.Rows)
                {
                    item = new RadComboBoxItem();
                    item.Text = dr1["ACCOUNT_NAME"].ToString();
                    item.Value = dr1["CUST_ACCOUNT_ID"].ToString();
                    string name = dr1["ACCOUNT_NAME"].ToString();
                    item.Attributes.Add("ACCT_NAME", name.ToString());
                    ins_acct_combo.Items.Add(item);
                    item.DataBind();
                }
            }
        }
    }

    public void  ShowEmployerMultiCoSelection()
    {
        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        string sql = "SELECT PARTY_ID FROM ACCOUNT_PRIVS WHERE IUSERID = " + Convert.ToString(Session["UserID"]);
        objConnection.Open();
        OdbcDataAdapter ObjDataAdapter = new OdbcDataAdapter(sql, objConnection);
        DataTable dt = new DataTable();
        ObjDataAdapter.Fill(dt);

        if (dt.Rows.Count > 0)
        {
            ShowEmployerMultiCoSelection123();
            multiAcctPanel.Visible = true;
            acctViewingDrp.Visible = false;
            ins_acct_combo.Visible = true;
            emp_acct_combo.Visible = false;
        }
        else
        {
            sql = "SELECT HCA_INS.CUST_ACCOUNT_ID, HCA_INS.ACCOUNT_NAME " 
                + "FROM HZ_CUST_ACCOUNTS HCA_INS INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_INS.CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID  " 
                + "	 INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID " 
                + "	 INNER JOIN HZ_PARTIES HP ON HCA_EMP.PARTY_ID = HP.PARTY_ID " 
                + "   INNER JOIN ACCOUNT_PRIVS AP " 
                + "   ON HCARA.CUST_ACCOUNT_ID = AP.CUST_ACCOUNT_ID " 
                + " INNER JOIN USER_INFO UI " 
                + " ON AP.IUSERID = UI.IUSERID " 
                + " WHERE  HCA_INS.ATTRIBUTE_CATEGORY = 'EMPLOYER'  " 
                + "	AND HCA_EMP.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' " 
                + "  AND UCASE('" + Session["UserName"] + "')" + " = UI.SUSERNAME " 
                + "	AND HCARA.STATUS = 'A' " 
                + "	AND HCA_INS.PARTY_ID = " 
                + " (SELECT PARTY_ID FROM ACCOUNT_PRIVS WHERE IUSERID = " + Convert.ToString(Session["UserID"]) + " LIMIT 1" + " ) " 
                + "	AND HCA_EMP.STATUS = 'A' " 
                + "ORDER BY HP.PARTY_NAME";
            dt = new DataTable();
            ObjDataAdapter = new OdbcDataAdapter(sql, objConnection);
            RadComboBoxItem item = new RadComboBoxItem();

            try
            {
                ObjDataAdapter.Fill(dt);
                ins_acct_combo.Items.Clear();
                ins_acct_combo.ClearSelection();
                item = new RadComboBoxItem();;
                item.DataBind();

                foreach(DataRow dataRow in dt.Rows)
                {
                    item = new RadComboBoxItem();
                    item.Text = dataRow["ACCOUNT_NAME"].ToString();
                    item.Value = dataRow["CUST_ACCOUNT_ID"].ToString();
                    string name = dataRow["ACCOUNT_NAME"].ToString();
                    item.Attributes.Add("ACCT_NAME", name.ToString());
                    ins_acct_combo.Items.Add(item);
                    item.DataBind();
                }
                ins_acct_combo.SelectedIndex = 0;
                Session["CustAccountID"] = ins_acct_combo.SelectedValue;
                objConnection.Open();
                sql = "SELECT PARTY_ID FROM HZ_CUST_ACCOUNTS WHERE CUST_ACCOUNT_ID = " + Convert.ToString(Session["CustAccountID"]);
                objSqlCmd = new OdbcCommand(sql, objConnection);
                OdbcDataReader objSqlDataRdr = null;
                objSqlDataRdr = objSqlCmd.ExecuteReader();

                if (objSqlDataRdr.Read())
                    Session["PartyID"] = objSqlDataRdr.GetValue(0);

                multiAcctPanel.Visible = true;
                acctViewingDrp.Visible = false;
                ins_acct_combo.Visible = true;
                emp_acct_combo.Visible = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objConnection.Close();
                ObjDataAdapter.Dispose();
                objConnection.Dispose();
            }
        }
    }
    public void ShowEmployerCoSelection()
    {
        string sql = " SELECT ap.CUST_ACCOUNT_ID, hca.account_name " 
            + " FROM ACCOUNT_PRIVS ap, hz_cust_accounts hca " 
            + " WHERE ap.IUSERID = " + Convert.ToString(Session["UserID"]) 
            + " and hca.cust_account_id = ap.cust_account_id ";
            
        DataTable dt = new DataTable();

        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        OdbcDataAdapter ObjDataAdapter = new OdbcDataAdapter(sql, objConnection);
        RadComboBoxItem item = new RadComboBoxItem();

        try
        {
            objConnection.Open();
            ObjDataAdapter.Fill(dt);
            ins_acct_combo.Items.Clear();
            ins_acct_combo.ClearSelection();
            item = new RadComboBoxItem();
            item.DataBind();
            foreach (DataRow dataRow in dt.Rows)
            {
                item = new RadComboBoxItem();
                item.Text = dataRow["ACCOUNT_NAME"].ToString();
                item.Value = dataRow["CUST_ACCOUNT_ID"].ToString();
                string name = dataRow["ACCOUNT_NAME"].ToString();
                item.Attributes.Add("ACCT_NAME", name.ToString());
                ins_acct_combo.Items.Add(item);
                item.DataBind();
            }
            multiAcctPanel.Visible = true;
            acctViewingDrp.Visible = false;
            ins_acct_combo.Visible = true;
            emp_acct_combo.Visible = false;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            objConnection.Close();
            ObjDataAdapter.Dispose();
            objConnection.Dispose();
        }
    }
    public void ShowEmployerRelatedAccts()
    {
        string sql  = "SELECT HCA_EMP.CUST_ACCOUNT_ID, HP.PARTY_NAME " 
            + "FROM HZ_CUST_ACCOUNTS HCA_INS INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_INS.CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID " 
            + "	 INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID " 
            + "	 INNER JOIN HZ_PARTIES HP ON HCA_EMP.PARTY_ID = HP.PARTY_ID " 
            + "WHERE HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' " 
            + "	  AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' " 
            + "	  AND HCARA.STATUS = 'A' " 
            + "	  AND HCA_INS.PARTY_ID = " + Convert.ToString(Session["PartyID"]) 
            + "  AND HCA_INS.CUST_ACCOUNT_ID= " + Convert.ToString(Session["CustAccountID"]) 
            + "	  AND HCA_EMP.STATUS = 'A' " 
            + "GROUP BY HP.PARTY_ID, HP.PARTY_NAME " 
            + "ORDER BY HP.PARTY_NAME ";

        DataTable dt = new DataTable();
        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        OdbcDataAdapter ObjDataAdapter = new OdbcDataAdapter(sql, objConnection);
        RadComboBoxItem item = new RadComboBoxItem();

        try
        {
            objConnection.Open();
            ObjDataAdapter.Fill(dt);
            emp_acct_combo.Items.Clear();
            emp_acct_combo.ClearSelection();
            item = new RadComboBoxItem();
            item.Text = "SELECT AN ACCOUNT";
            item.Value = Session["InsPartyID"].ToString();
            item.Attributes.Add("ACCT_NAME", "SELECT AN ACCOUNT");
            emp_acct_combo.Items.Add(item);
            item.DataBind();
            foreach (DataRow dataRow in dt.Rows)
            {
                item = new RadComboBoxItem();
                item.Text = dataRow["PARTY_NAME"].ToString();
                item.Value = dataRow["CUST_ACCOUNT_ID"].ToString();
                string name = dataRow["PARTY_NAME"].ToString();
                item.Attributes.Add("ACCT_NAME", name.ToString());
                emp_acct_combo.Items.Add(item);
                item.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            objConnection.Close();
            ObjDataAdapter.Dispose();
            objConnection.Dispose();
        }
    }
    public void ShowAccountSelection()
    {
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        OdbcCommand objSqlCmd = new OdbcCommand();
        objSqlCmd.Connection = objConnection;
        OdbcCommand objSqlCmd2 = new OdbcCommand();
        objSqlCmd2.Connection = objConnection;
        OdbcDataReader objSqlDataRdr = null;
        OdbcDataReader objSqlDataRdr2 = null;        
        int iPartyID = Convert.ToInt32(Session["PartyID"]); 
        DataTable dt = new DataTable();

        string username = Session["UserName"].ToString();
        try
        {
            objConnection.Open();
            objSqlCmd.CommandText = "{call sp_getNumberOfAccounts(?)}";
            objSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;            
            objSqlCmd.Parameters.Add(new OdbcParameter("@user_name", OdbcType.VarChar, 255)).Value = Session["UserName"];
            objSqlDataRdr = objSqlCmd.ExecuteReader();

            if (objSqlDataRdr.Read())
            {
                if (Convert.ToInt32(objSqlDataRdr.GetValue(0)) > 1)
                {
                    acctViewingDrp.Items.Clear();
                    objSqlCmd2.CommandText = "{call sp_getUserAccounts(?)}";
                    objSqlCmd2.CommandType = System.Data.CommandType.StoredProcedure;

                    objSqlCmd2.Parameters.Add(new OdbcParameter("@user_name", OdbcType.VarChar, 255)).Value = Session["UserName"];
                    objSqlDataRdr2 = objSqlCmd2.ExecuteReader();

                    //   Dim items(objSqlDataRdr.Item(0)) As ListItem //EDH index 0 contains the number of accounts
                    ListItem items = new ListItem(objSqlDataRdr.GetValue(0).ToString());
                    int cnt = 0;
                    int mult_accts = Convert.ToInt32(Session["mult_accts"]);

                    while (objSqlDataRdr2.Read())
                    {
                        acctViewingDrp.Items.Add(new ListItem(objSqlDataRdr2.GetValue(1).ToString(), objSqlDataRdr2.GetValue(0).ToString())); // EDH index 0 was the party_id and index 1 was the company description
                        acctViewingDrp.Font.Size = 10;
                        acctViewingDrp.Font.Name = "calibri";
                        cnt = cnt + 1;
                    }

                    acctViewingDrp.DataBind();
                    Session["PartyID"] = acctViewingDrp.SelectedValue; // edh ticket #1028

                    multiAcctPanel.Visible = true;
                    objSqlDataRdr2.Close();

                    if (mult_accts >= 1)
                    {
                        // get the cust_account_ids for the current partyID
                        //EDH Session("CustAccountID") = Nothing
                        Session["CustAccount_IDs"] = null;

                        string sql = "SELECT COUNT(DISTINCT(CUST_ACCOUNT_ID)) FROM ACCOUNT_PRIVS WHERE IUSERID = " + Session["UserID"] + " AND CUST_ACCOUNT_ID <> 0 AND PARTY_ID = " + Session["PartyID"];
                        objSqlCmd2 = new OdbcCommand(sql, objConnection);
                        objSqlDataRdr2 = objSqlCmd2.ExecuteReader();

                        if (objSqlDataRdr2.Read())
                            Session["mult_locs"] = objSqlDataRdr2.GetValue(0);

                        objSqlDataRdr2.Close();

                        sql = "SELECT CUST_ACCOUNT_ID FROM ACCOUNT_PRIVS WHERE IUSERID = " + Session["UserID"] + " AND CUST_ACCOUNT_ID <> 0 AND PARTY_ID = " + Session["PartyID"];

                        objSqlCmd2 = new OdbcCommand(sql, objConnection);
                        objSqlDataRdr2 = objSqlCmd2.ExecuteReader();

                        if (Convert.ToInt32(Session["mult_locs"]) == 1)
                        {
                            if (objSqlDataRdr2.Read())
                                Session["CustAccountID"] = objSqlDataRdr2.GetValue(0);
                        }
                        else if (Convert.ToInt32(Session["mult_locs"]) > 1)
                        {
                            string tmp_cust_acct_ids = "";

                            while (objSqlDataRdr2.Read())
                            {
                                tmp_cust_acct_ids += Convert.ToString(objSqlDataRdr2.GetValue(0)) + ",";
                            }

                            if (tmp_cust_acct_ids.Length > 1)
                                Session["CustAccount_IDs"] = tmp_cust_acct_ids.Substring(0, tmp_cust_acct_ids.Length - 1);
                        }
                        else
                            Session["CustAccount_IDs"] = null;
                    }

                    objSqlDataRdr2.Close();
                    objSqlCmd2.Dispose();

                    multiAcctPanel.Visible = true;
                    acctViewingDrp.Visible = true;
                    ins_acct_combo.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write("Can't load Web page: " + ex.Message);
        }
        finally
        {
            objConnection.Close();
            objConnection.Dispose();
        }
    }
    public void GetAccountRep()
    {
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        OdbcCommand objSqlCmd = new OdbcCommand();
        objSqlCmd.Connection = objConnection;
        OdbcDataReader objSqlDataRdr = null;
        
        int iPartyID = Convert.ToInt32(Session["PartyID"]);
        DataTable dt = new DataTable();

        try
        {
            objConnection.Open();
            objSqlCmd.CommandText = "{call sp_getAccountRep(?)}";
            objSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
            objSqlCmd.Parameters.Add(new OdbcParameter("@partyID", OdbcType.VarChar, 255)).Value = iPartyID.ToString();

            objSqlDataRdr = objSqlCmd.ExecuteReader();

            if (objSqlDataRdr.Read()) 
            {
                accountWWRepName.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(1)) + " " + UtilityFunctions.myCStr(objSqlDataRdr.GetValue(2));
                accountWWRepPhone.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(3));
                accountWWRepFax.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(4));
                accountWWRepEmail.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(5));
                accountWWRepExt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(7));
            }
        }
        catch (Exception ex)
        {
            Response.Write("Can't load Web page: " + ex.Message);
        }
        finally
        {
            objConnection.Close();
            objConnection.Dispose();
        }
    }
    public void GetNumberOfLocations()
    {
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        OdbcCommand objSqlCmd = new OdbcCommand();
        objSqlCmd.Connection = objConnection;
        OdbcDataReader objSqlDataRdr = null;        
        int iPartyID = Convert.ToInt32(Session["PartyID"]);
        DataTable dt = new DataTable();

        try
        {
            objConnection.Open();

            objSqlCmd.CommandText = "{call sp_getNumberOfLocations(?)}";
            objSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
            objSqlCmd.Parameters.Add(new OdbcParameter("@ipPartyID", OdbcType.VarChar, 15)).Value = iPartyID;
            objSqlDataRdr = objSqlCmd.ExecuteReader();
        }
        catch (Exception ex)
        {
            Response.Write("Can't load Web page: " + ex.Message);
        }
        finally
        {
            objConnection.Close();
            objConnection.Dispose();
        }
    }
     
}