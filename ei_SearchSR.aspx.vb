﻿
Imports System.Data
Imports System.Data.Odbc
Imports System.Net.Mail
Imports System.Net
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO

Partial Class ei_SearchSR
    Inherits System.Web.UI.Page

    Dim filename As String
    Dim _fullfilename As String

    Public Property FullFileName() As String
        Get
            Return _fullfilename
        End Get
        Set(ByVal value As String)
            _fullfilename = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.IsAuthenticated Then
            AuthenticatedMessagePanel.Visible = True
            AnonymousMessagePanel.Visible = False

            If Not IsPostBack Then
                SearchViewStsRpt()
            End If
        Else
            AuthenticatedMessagePanel.Visible = False
            AnonymousMessagePanel.Visible = True
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        If writeItVal.Value = 1 Then
            'write the status report, this is done so I can refresh the submit Status report page
            'writeit()

            writeItVal.Value = 0

        End If

    End Sub


    Function ReverseDate(ByVal dDate As Date) As String

        If String.IsNullOrEmpty(dDate) = False Then
            Return dDate.ToString("yyyy/MM/dd")
        Else
            Return ""
        End If

    End Function

    Function ConvertDate(ByVal dDateOfTreatment As Date) As String

        If String.IsNullOrEmpty(dDateOfTreatment) = False Then
            Return dDateOfTreatment.ToString("MM/dd/yyyy")
        Else
            Return ""
        End If

    End Function

    Function myCStr(ByVal test As Object) As String

        Dim sReturnStr As String = ""

        If IsDBNull(test) Then
            sReturnStr = ""
        Else
            sReturnStr = CStr(test)
        End If

        If StrComp(sReturnStr, "#") = 0 Then
            sReturnStr = ""
        End If

        myCStr = sReturnStr

    End Function

    Sub CheckWorkStatusType()

        If srWorkStatusDrp.SelectedValue = 2 Or srWorkStatusDrp.SelectedValue = 3 Then
            ciUntilDateTxtCell.Visible = True
            ciUntilDateLblCell.Visible = True
            'srWorkStatusUntilBtn.Visible = True
            If srWorkStatusDrp.SelectedValue = 3 Then
                ciModifiedRestrictionsPanel.Visible = True
            Else
                ciModifiedRestrictionsPanel.Visible = False
            End If

            If String.IsNullOrEmpty(srEffectiveDateTxt.Text) Then
                srEffectiveDateTxt.Text = Now.ToShortDateString
            End If
        Else
            ciUntilDateTxtCell.Visible = False
            ciUntilDateLblCell.Visible = False
            'srWorkStatusUntilBtn.Visible = False
            ciModifiedRestrictionsPanel.Visible = False
            If srWorkStatusDrp.SelectedValue = 1 Then
                If String.IsNullOrEmpty(srEffectiveDateTxt.Text) Then
                    srEffectiveDateTxt.Text = Now.ToShortDateString
                End If
            Else
                srUntilDateTxt.Text = ""
                srEffectiveDateTxt.Text = ""
            End If

        End If

        srUntilDateTxt.Text = ""
        srCanLiftDrp.SelectedIndex = -1
        srCanCarryDrp.SelectedIndex = -1
        srSimpleGraspingDrp.SelectedIndex = -1
        srPushingPullingDrp.SelectedIndex = -1
        srFineManipulationDrp.SelectedIndex = -1
        srRepeatRFootWorkDrp.SelectedIndex = -1
        srRepeatLFootWorkDrp.SelectedIndex = -1
        'srRestrictionsDrp.SelectedIndex = -1
        srCanSitPerDayDrp.SelectedIndex = -1
        srCanStandPerDayDrp.SelectedIndex = -1
        srCanDrivePerDayDrp.SelectedIndex = -1
        srCanBendDrp.SelectedIndex = -1
        srCanSquatDrp.SelectedIndex = -1
        srCanClimbStairsDrp.SelectedIndex = -1
        srCanClimbLadderDrp.SelectedIndex = -1
        srCanCrawlDrp.SelectedIndex = -1
        srCanReachDrp.SelectedIndex = -1

    End Sub

    Sub GetCMName()

        Dim objSqlCmd As New OdbcCommand
        Dim objSqlDataRdr As OdbcDataReader = Nothing
        Dim strConnection As String = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;" 'ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()
        Dim objConnection As New OdbcConnection(strConnection)

        Try


            objConnection.Open()
            objSqlCmd.Connection = objConnection

            ' get patient details

            objSqlCmd = New OdbcCommand("", objConnection)

            objSqlCmd.CommandText = "SELECT CM_NAME " & _
                                    "FROM CASE_MANAGERS " & _
                                    "WHERE CM_EMAIL = '" & User.Identity.Name.ToUpper & "@EWORKWELL.COM' "

            objSqlDataRdr = objSqlCmd.ExecuteReader()

            If objSqlDataRdr.Read() Then

                srCMTxt.Text = objSqlDataRdr.Item(0)

            End If

            objSqlDataRdr.Close()
            objSqlDataRdr.Dispose()
        Catch ex As Exception
            Response.Write("Can't load Web page " & ex.Message)
        Finally
            objConnection.Close()
            objConnection.Dispose()
        End Try

        'ipEmployerSearchResultsPanel.Visible = False
        StatusReprtFormPanel.Visible = True

    End Sub

    Sub SearchViewStsRpt()

        Dim iPartyID As Integer = Session("PartyID")
        Dim dDOIStartRangeDate As String = "2000/01/01"
        Dim dDOIEndRangeDate As String = ReverseDate(Now.AddDays(1).ToShortDateString)
        Dim dDOEStartRangeDate As String = "2000/01/01"
        Dim dDOEEndRangeDate As String = ReverseDate(Now.AddDays(1).ToShortDateString)
        Dim sBuiltQuery As String = String.Empty
        Dim sClaimNumberQuery As String = String.Empty
        Dim sSocialSecNum As String = String.Empty
        Dim sSocialSecNumQuery As String = String.Empty
        Dim bSSEntered As Boolean = True
        Dim bDateError As Boolean = False

        Dim ipViewSRDOIStartRangeDate As Boolean = False
        Dim ipViewSRDOIEndRangeDate As Boolean = False
        Dim ipViewSRDOEStartRangeDate As Boolean = False
        Dim ipViewSRDOEEndRangeDate As Boolean = False
        Dim ipViewStartDateAfterEndDate As Boolean = False

        Dim ipDOIStartRangeDate As Boolean = False
        Dim ipDOIEndRangeDate As Boolean = False
        Dim dt As New DataTable()
        Dim sSqlQuery As String = String.Empty

        EditVal.Value = 0

        ipViewFirstNameErrorPointer.Visible = False
        ipViewLastNameErrorPointer.Visible = False
        ipViewstartDOIRangeErrorPointer.Visible = False
        ipViewendDOIRangeErrorPointer.Visible = False
        startViewSRDOIRangeError.Visible = False
        endViewSRDOIRangeError.Visible = False
        startDateAfterEndDateError.Visible = False

        'If String.Compare(Session("UserName").ToString(), "MTFORBES@FEDINS.COM") = 0 Then  ' "MTFORBES@FEDINS.COM"
        '    'ImgAnalyitics.Visible = True
        '    btnAnalytics.Visible = True
        'End If

        If String.Compare(Session("UserName").ToString(), "DEMO") = 0 Then  ' "MTFORBES@FEDINS.COM"
            'ImgAnalyitics.Visible = True
            btnAnalytics.Visible = True
        Else
            btnAnalytics.Visible = False
        End If

        If String.IsNullOrEmpty(ipViewDOIStartRangeTxt.Text) = False And IsDate(ipViewDOIStartRangeTxt.Text) Then
            dDOIStartRangeDate = ReverseDate(ipViewDOIStartRangeTxt.Text)
        ElseIf String.IsNullOrEmpty(ipViewDOIStartRangeTxt.Text) = False And Not IsDate(ipViewDOIStartRangeTxt.Text) Then
            bDateError = True
            ipViewSRDOIStartRangeDate = True
        End If

        If String.IsNullOrEmpty(ipViewDOIEndRangeTxt.Text) = False And IsDate(ipViewDOIEndRangeTxt.Text) Then
            dDOIEndRangeDate = ReverseDate(ipViewDOIEndRangeTxt.Text)
        ElseIf String.IsNullOrEmpty(ipViewDOIEndRangeTxt.Text) = False And Not IsDate(ipViewDOIEndRangeTxt.Text) Then
            bDateError = True
            ipViewSRDOIEndRangeDate = True
        End If

        If IsDate(ipViewDOIEndRangeTxt.Text) And IsDate(ipViewDOIStartRangeTxt.Text) Then
            If dDOIStartRangeDate > dDOIEndRangeDate Then
                bDateError = True
                ipViewStartDateAfterEndDate = True
            End If
        End If


        If bDateError = True Then

            ' show error message; they must enter at least 1 search criteria
            If ipViewSRDOIStartRangeDate = True Then
                startViewSRDOIRangeError.Visible = True
            End If

            If ipViewSRDOIEndRangeDate = True Then
                endViewSRDOIRangeError.Visible = True
            End If

            If ipViewStartDateAfterEndDate = True Then
                startDateAfterEndDateError.Visible = True
            End If

        Else

            ' look up IPs with patient criteria

            ipViewSearchResultsPanel.Visible = True

            If String.IsNullOrEmpty(ipViewSS1Txt.Text) = False Or String.IsNullOrEmpty(ipViewSS2Txt.Text) = False Or String.IsNullOrEmpty(ipViewSS3Txt.Text) = False Then

                If String.IsNullOrEmpty(ipViewSS1Txt.Text) = False Then
                    sSocialSecNum = ipViewSS1Txt.Text.Trim.Replace("'", "''") & "-"
                Else
                    sSocialSecNum = "%-"
                    bSSEntered = False
                End If

                If String.IsNullOrEmpty(ipViewSS2Txt.Text) = False Then
                    sSocialSecNum = sSocialSecNum & ipViewSS2Txt.Text.Trim.Replace("'", "''") & "-"
                Else
                    sSocialSecNum = sSocialSecNum & "%-"
                    bSSEntered = False
                End If

                If String.IsNullOrEmpty(ipViewSS3Txt.Text) = False Then
                    sSocialSecNum = sSocialSecNum & ipViewSS3Txt.Text.Trim.Replace("'", "''")

                Else
                    sSocialSecNum = sSocialSecNum.Replace("'", "''") & "%"
                    bSSEntered = False
                End If

                If bSSEntered Then
                    sBuiltQuery = sBuiltQuery & " AND (SOCIALSECURITYNUMBER = '" & sSocialSecNum.Replace("'", "''") & "') "
                Else
                    sBuiltQuery = sBuiltQuery & " AND (SOCIALSECURITYNUMBER like '" & sSocialSecNum.Replace("'", "''") & "') "
                End If

            End If

            If String.IsNullOrEmpty(ipViewFirstNameTxt.Text.Trim) = False Then
                sBuiltQuery = " AND IPFIRSTNAME LIKE '%" & ipViewFirstNameTxt.Text.ToUpper.Trim.Replace("'", "''") & "%' "
            End If

            If String.IsNullOrEmpty(ipViewLastNameTxt.Text.Trim) = False Then
                sBuiltQuery = sBuiltQuery & " AND IPLASTNAME LIKE '%" & ipViewLastNameTxt.Text.ToUpper.Trim.Replace("'", "''") & "%' "
            End If
            'StrComp(Convert.ToString(Session("InsCarrier")), "N") = 0) Or (StrComp(Convert.ToString(Session("InsCarrier")), "Y") = 0 And
            If (Convert.ToInt32(Session("PartyID")) <> Convert.ToInt32(Session("InsPartyID"))) Then

                ' reset Session("mult_locs")
                Dim objSqlCmd_ As New OdbcCommand
                Dim objSqlDataRdr_ As OdbcDataReader = Nothing
                Dim strConnection1 As String = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;" 'ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()
                Dim objConnection As New OdbcConnection(strConnection1)
                Dim sql As String = "SELECT COUNT(DISTINCT(CUST_ACCOUNT_ID)) FROM ACCOUNT_PRIVS WHERE IUSERID = " & Session("UserID") & " AND CUST_ACCOUNT_ID <> 0 AND PARTY_ID = " + Convert.ToString(Session("PartyID")) '& " AND CUST_ACCOUNT_ID = " & Convert.ToString(Session("CustAccountID"))
                objSqlCmd_ = New OdbcCommand(sql, objConnection)
                objConnection.Open()
                objSqlDataRdr_ = objSqlCmd_.ExecuteReader()
                If objSqlDataRdr_.Read() Then
                    Session("mult_locs") = objSqlDataRdr_.GetValue(0)
                End If
                objSqlDataRdr_.Close()

                Dim mult_locs As Integer = Session("mult_locs")
                Dim cust_account_sql_str As String = "AND ( "

                If mult_locs = 1 And Not String.IsNullOrEmpty(Session("CustAccountID")) Then
                    cust_account_sql_str += " HCA.CUST_ACCOUNT_ID = " & Session("CustAccountID") & " ) "
                Else
                    If StrComp(Convert.ToString(Session("InsCarrier")), "N") <> 0 And StrComp(Convert.ToString(Session("InsCarrier")), "E") <> 0 Then
                        cust_account_sql_str = String.Empty
                    End If
                End If

                If mult_locs > 1 Then
                    Dim str As String = Session("CustAccount_IDs")
                    Dim strSplitArr As String() = str.Split(",")

                    For Each arrStr As String In strSplitArr
                        cust_account_sql_str = cust_account_sql_str & " HCA.CUST_ACCOUNT_ID = " & arrStr & " OR "
                    Next
                    cust_account_sql_str = cust_account_sql_str.Substring(0, cust_account_sql_str.Length - 4) & " ) "
                End If

                If mult_locs = 0 Then
                    cust_account_sql_str = String.Empty
                End If


                If mult_locs = 1 And StrComp(Convert.ToString(Session("InsCarrier")), "E") = 0 Then
                    cust_account_sql_str = String.Empty
                End If

                'If StrComp(Convert.ToString(Session("InsCarrier")), "I") = 0 Then 'EDH
                '    cust_account_sql_str += " HCA.CUST_ACCOUNT_ID = " & Session("CustAccountID") & " ) "
                'End If

                '"SELECT hpip.PARTY_NAME, DATE_FORMAT(hcaip.ATTRIBUTE8,'%m/%d/%Y') ATTRIBUTE8, hcaemp.ACCOUNT_NAME,hcaip.CUST_ACCOUNT_ID, CONCAT(hpip.PERSON_LAST_NAME, ', ', hpip.PERSON_FIRST_NAME, ' ', replace(fn_getMiddleInitial(hpip.PERSON_MIDDLE_NAME),'..','.')) EMP_NAME " & _
                sSqlQuery = "SELECT IPCUSTACCTID, IPFIRSTNAME, IPMIDDLENAME, IPLASTNAME, IPDOI, IPDOE, STATUSREPORTID, IPEMPLOYERNAME, SOCIALSECURITYNUMBER, REPORT_TYPE  " & _
                            "FROM STATUS_REPORTS " & _
                            "WHERE ACTIVE = 1 AND " & _
                            "      EMP_PARTY_ID = " & iPartyID & " AND " & _
                            "      IPDOI BETWEEN '" & ReverseDate(dDOIStartRangeDate) & "' AND '" & ReverseDate(dDOIEndRangeDate) & "' AND " & _
                            "      IPDOE BETWEEN '" & ReverseDate(dDOEStartRangeDate) & "' AND '" & ReverseDate(dDOEEndRangeDate) & "' " & sBuiltQuery & _
                            "      AND IPCUSTACCTID IN (SELECT HCA_SUB.CUST_ACCOUNT_ID " & _
                            "				            FROM HZ_CUST_ACCOUNTS HCA, HZ_PARTIES HP, HZ_CUST_ACCT_SITES_ALL HCASA, HZ_CUST_ACCT_RELATE_ALL HCARA, HZ_CUST_ACCOUNTS HCA_SUB, HZ_PARTIES HP_SUB  " & _
                            "				            WHERE HP.PARTY_ID = " & iPartyID & "  " & _
                            "				                  AND HP.PARTY_ID = HCA.PARTY_ID  " & _
                            "				                  AND HCASA.CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID  " & _
                            "				                  AND HCASA.CUST_ACCOUNT_ID = HCARA.RELATED_CUST_ACCOUNT_ID  " & cust_account_sql_str & _
                            "				                  AND HCARA.CUST_ACCOUNT_ID = HCA_SUB.CUST_ACCOUNT_ID  " & _
                            "				                  AND HCA_SUB.PARTY_ID = HP_SUB.PARTY_ID  " & _
                            "				                  AND HCA_SUB.ATTRIBUTE_CATEGORY = 'PATIENT' " & _
                            "				                  AND HCARA.STATUS = 'A') " & _
                            "ORDER BY IPDOE desc, IPLASTNAME, IPFIRSTNAME, IPMIDDLENAME  "
            Else
                If StrComp(Convert.ToString(Session("InsCarrier")), "I") = 0 Or StrComp(Convert.ToString(Session("InsCarrier")), "E") = 0 Then

                    sSqlQuery = "SELECT IPCUSTACCTID, IPFIRSTNAME, IPMIDDLENAME, IPLASTNAME, IPDOI, IPDOE, STATUSREPORTID, IPEMPLOYERNAME, SOCIALSECURITYNUMBER, REPORT_TYPE " & _
                        "FROM STATUS_REPORTS " & _
                        "WHERE ACTIVE = 1 AND " & _
                        "      IPDOI BETWEEN '" & ReverseDate(dDOIStartRangeDate) & "' AND '" & ReverseDate(dDOIEndRangeDate) & "' AND " & _
                        "      IPDOE BETWEEN '" & ReverseDate(dDOEStartRangeDate) & "' AND '" & ReverseDate(dDOEEndRangeDate) & "' " & sBuiltQuery & _
                        "   AND IPCUSTACCTID IN (SELECT HCA_SUB.CUST_ACCOUNT_ID " & _
                        "   FROM HZ_CUST_ACCOUNTS HCA, HZ_PARTIES HP, HZ_CUST_ACCT_SITES_ALL HCASA, HZ_CUST_ACCT_RELATE_ALL HCARA, HZ_CUST_ACCOUNTS HCA_SUB, HZ_PARTIES HP_SUB  " & _
                        "   WHERE " & _
                        "   HP.PARTY_ID = HCA.PARTY_ID " & _
                        "   AND HCASA.CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID " & _
                        "   AND HCASA.CUST_ACCOUNT_ID = HCARA.RELATED_CUST_ACCOUNT_ID " & _
                        "   AND HCARA.CUST_ACCOUNT_ID = HCA_SUB.CUST_ACCOUNT_ID " & _
                        "   AND HCA_SUB.PARTY_ID = HP_SUB.PARTY_ID  AND HCA_SUB.ATTRIBUTE_CATEGORY = 'PATIENT' " & _
                        "   AND HCARA.STATUS = 'A' " & _
                        "   AND HCA.CUST_ACCOUNT_ID= " & Convert.ToString(Session("CustAccountID")) & _
                        "   )" & _
                        "ORDER BY IPDOE desc, IPLASTNAME, IPFIRSTNAME, IPMIDDLENAME  "

                Else
                    sSqlQuery = "SELECT IPCUSTACCTID, IPFIRSTNAME, IPMIDDLENAME, IPLASTNAME, IPDOI, IPDOE, STATUSREPORTID, IPEMPLOYERNAME, SOCIALSECURITYNUMBER, REPORT_TYPE  " & _
                                "FROM STATUS_REPORTS " & _
                                "WHERE ACTIVE = 1 AND " & _
                                "      IPDOI BETWEEN '" & ReverseDate(dDOIStartRangeDate) & "' AND '" & ReverseDate(dDOIEndRangeDate) & "' AND " & _
                                "      IPDOE BETWEEN '" & ReverseDate(dDOEStartRangeDate) & "' AND '" & ReverseDate(dDOEEndRangeDate) & "' " & sBuiltQuery & _
                                "      AND IPCUSTACCTID IN (SELECT HCA_IP.CUST_ACCOUNT_ID  " & _
                                "				            FROM HZ_CUST_ACCOUNTS HCA_IP " & _
                                "				                 INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_INS ON HCARA_INS.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID " & _
                                "				                 INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA_INS.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID  " & _
                                "				                 INNER JOIN HZ_PARTIES HP_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID " & _
                                "				            WHERE HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT' AND " & _
                                "				                  HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' AND " & _
                                 "	   HCA_INS.PARTY_ID = " & Convert.ToString(Session("InsPartyID")) & " AND " & " HCA_INS.PARTY_ID = " & Convert.ToString(Session("MotoristsSub")) & " AND " & _
                                "				                  HCARA_INS.STATUS = 'A' AND " & _
                                "				                  HCA_IP.ATTRIBUTE9 IS NOT NULL) " & _
                                "ORDER BY IPDOE desc, IPLASTNAME, IPFIRSTNAME, IPMIDDLENAME  "
                End If
            End If

            ' "                             HCA_IP.CUST_ACCOUNT_ID = " & Convert.ToString(Session("CustAccountID")) & " AND " & _

            'Response.Write(sSqlQuery & "<BR>")

            ciStatusRptsViewSrchResultsPanel.Visible = False

            'eiSearchStatusReportsPanel.Visible = False
            ciStatusRptsViewSrchResultsPanel.Visible = True
            ipViewSearchResultsPanel.Visible = True
            srNoViewSearchResultsTbl.Visible = False

            Dim strConnection As String = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;" 'ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()
            Dim objConn As OdbcConnection = New OdbcConnection(strConnection)
            Dim objDataAdapter As OdbcDataAdapter = New OdbcDataAdapter(sSqlQuery, objConn)
            Dim objCmd As OdbcCommand = Nothing

            Try
                objConn.Open()
                objDataAdapter.Fill(dt)

                ciStatusRptViewGrd.DataSource = dt
                ciStatusRptViewGrd.PageIndex = 0
                ciStatusRptViewGrd.DataBind()

                If dt.Rows.Count > 0 Then
                    ciStatusRptViewGrd.Visible = True
                Else
                    ciStatusRptViewGrd.Visible = False
                    srNoViewSearchResultsTbl.Visible = True
                End If
            Catch ex As Exception
                Response.Write("Page Won't Load: " & ex.ToString)
            Finally
                objConn.Close()
                objDataAdapter.Dispose()
                objConn.Dispose()
            End Try

        End If

    End Sub

    Protected Sub ciStatusRptViewGrd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles ciStatusRptViewGrd.PageIndexChanging

        SearchViewStsRpt()

        ciStatusRptViewGrd.PageIndex = e.NewPageIndex
        ciStatusRptViewGrd.DataBind()

    End Sub

    Protected Sub ciStatusRptViewGrd_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles ciStatusRptViewGrd.RowCommand

        statusReportID.Value = e.CommandArgument

        srActiveStatusDrp.SelectedIndex = -1

        If StrComp(e.CommandName, "Page") <> 0 Then

            'this is only for the triage report for chris.

            If StrComp(e.CommandName, "triage") <> 0 Then
                statusReportID.Value = e.CommandArgument

                Dim objSqlCmd As New OdbcCommand
                Dim objSqlDataRdr As OdbcDataReader = Nothing
                Dim strConnection As String = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;" 'ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()
                Dim objConnection As New OdbcConnection(strConnection)

                'ciStatusRprtsSrchPanel.Visible = False
                'Try
                'ipViewSearchResultsPanel.Visible = False

                objConnection.Open()
                objSqlCmd.Connection = objConnection

                ' get patient details

                objSqlCmd = New OdbcCommand("", objConnection)
                objSqlCmd.CommandText = "{call sp_getIPStatusReport(?)}"
                objSqlCmd.CommandType = Data.CommandType.StoredProcedure
                objSqlCmd.Parameters.Add(New OdbcParameter("@statusReportID", OdbcType.VarChar, 255)).Value = statusReportID.Value
                objSqlDataRdr = objSqlCmd.ExecuteReader()

                If objSqlDataRdr.Read() Then

                    ipCustAccountID.Value = objSqlDataRdr.Item(0)

                    reportType.Value = objSqlDataRdr.Item(93)
                    srVisitTypeDrp.SelectedValue = objSqlDataRdr.Item(93)


                    If Not objSqlDataRdr.Item(6) Is DBNull.Value Then
                        srIPDOETxt.Text = ConvertDate(objSqlDataRdr.Item(6).ToString.Replace(" 00:00:00", ""))
                        'Else
                        'srIPDOETxt.Text = ConvertDate(Now.ToShortDateString.Replace(" 00:00:00", ""))
                    End If

                    If Not objSqlDataRdr.Item(7) Is DBNull.Value Then
                        srIPDOITxt.Text = ConvertDate(objSqlDataRdr.Item(7).ToString.Replace(" 00:00:00", ""))
                    End If

                    If Not objSqlDataRdr.Item(8) Is DBNull.Value Then
                        srIPDOBTxt.Text = ConvertDate(objSqlDataRdr.Item(8).ToString.Replace(" 00:00:00", ""))
                    End If

                    srIPFNameTxt.Text = myCStr(objSqlDataRdr.Item(1)).ToUpper
                    srIPMNameTxt.Text = objSqlDataRdr.Item(2) 'GetMiddleInit(objSqlDataRdr.Item(2))
                    srIPLNameTxt.Text = myCStr(objSqlDataRdr.Item(3)).ToUpper
                    srIPEmployerNameTxt.Text = myCStr(objSqlDataRdr.Item(4)).ToUpper
                    srIPSSNumClaimNumTxt.Text = myCStr(objSqlDataRdr.Item(5)).ToUpper

                    srSocialSecurityNumTxt.Text = myCStr(objSqlDataRdr.Item(58)).ToUpper

                    empPartyID.Value = objSqlDataRdr.Item(76)

                    If empPartyID.Value = 222397 Then
                        aghEmpTimeInOutTbl.Visible = True
                        aghEmpDisclaimerTbl.Visible = True
                    Else
                        aghEmpTimeInOutTbl.Visible = False
                        aghEmpDisclaimerTbl.Visible = False
                    End If

                    srTreatingPhysicianTxt.Text = myCStr(objSqlDataRdr.Item(53)).ToUpper
                    srCMTxt.Text = myCStr(objSqlDataRdr.Item(54)).ToUpper
                    srDiagonsisTxt.Text = myCStr(objSqlDataRdr.Item(10)).ToUpper
                    srObjectiveFindingsTxt.Text = myCStr(objSqlDataRdr.Item(11)).ToUpper
                    srMechanismOfInjuryTxt.Text = myCStr(objSqlDataRdr.Item(12)).ToUpper

                    srBodyPartTxt.Text = myCStr(objSqlDataRdr.Item(18)).ToUpper
                    srDiagOtherTxt.Text = myCStr(objSqlDataRdr.Item(17)).ToUpper
                    srReasonTxt.Text = myCStr(objSqlDataRdr.Item(19)).ToUpper

                    srMedicationTxt.Text = myCStr(objSqlDataRdr.Item(26)).ToUpper
                    srInstructionsTxt.Text = myCStr(objSqlDataRdr.Item(27)).ToUpper
                    srOtherTxt.Text = myCStr(objSqlDataRdr.Item(28)).ToUpper

                    srOtherRestrictionsTxt.Text = myCStr(objSqlDataRdr.Item(48)).ToUpper

                    srClincTypeDrp.SelectedIndex = objSqlDataRdr.Item(9)

                    srVistTypeDrp.SelectedValue = objSqlDataRdr.Item(23)


                    srTimesPerWeekDrp.SelectedIndex = objSqlDataRdr.Item(24)

                    srWorkStatusDrp.SelectedIndex = objSqlDataRdr.Item(29)
                    If objSqlDataRdr.Item(29) = 3 Then
                        ciModifiedRestrictionsPanel.Visible = True
                    End If
                    If objSqlDataRdr.Item(29) = 3 Or objSqlDataRdr.Item(29) = 2 Then
                        srUntilDateTxt.Visible = True
                        ciModifiedRestrictionsPanel.Visible = True
                    End If

                    srCanCarryDrp.SelectedIndex = objSqlDataRdr.Item(32)
                    If objSqlDataRdr.Item(32) = 7 Then
                        srCanCarryLblCell.Visible = True
                        srCanCarryTxtCell.Visible = True
                        srCanCarryOtherTxt.Text = objSqlDataRdr.Item(59)
                    Else
                        srCanCarryLblCell.Visible = False
                        srCanCarryTxtCell.Visible = False
                        srCanCarryOtherTxt.Text = ""
                    End If
                    'canliftcarry
                    srCanLiftDrp.SelectedIndex = objSqlDataRdr.Item(32)
                    If objSqlDataRdr.Item(32) = 7 Then
                        srCanLiftLblCell.Visible = True
                        srCanLiftTxtCell.Visible = True
                        srCanLiftOtherTxt.Text = objSqlDataRdr.Item(59)
                    Else
                        srCanLiftLblCell.Visible = False
                        srCanLiftTxtCell.Visible = False
                        srCanLiftOtherTxt.Text = ""
                    End If

                    srSimpleGraspingDrp.SelectedIndex = objSqlDataRdr.Item(33)
                    If objSqlDataRdr.Item(33) = 5 Then
                        srSimpleGraspingLblCell.Visible = True
                        srSimpleGraspingTxtCell.Visible = True
                        srSimpleGraspingOtherTxt.Text = objSqlDataRdr.Item(60)
                    Else
                        srSimpleGraspingLblCell.Visible = False
                        srSimpleGraspingTxtCell.Visible = False
                        srSimpleGraspingOtherTxt.Text = ""
                    End If

                    srPushingPullingDrp.SelectedIndex = objSqlDataRdr.Item(34)
                    If objSqlDataRdr.Item(34) = 5 Then
                        srPushingPullingLblCell.Visible = True
                        srPushingPullingTxtCell.Visible = True
                        srPushingPullingOtherTxt.Text = objSqlDataRdr.Item(61)
                    Else
                        srPushingPullingLblCell.Visible = False
                        srPushingPullingTxtCell.Visible = False
                        srPushingPullingOtherTxt.Text = ""
                    End If

                    srFineManipulationDrp.SelectedIndex = objSqlDataRdr.Item(35)
                    If objSqlDataRdr.Item(35) = 5 Then
                        srFineManipulationLblCell.Visible = True
                        srFineManipulationTxtCell.Visible = True
                        srFineManipulationOtherTxt.Text = objSqlDataRdr.Item(62)
                    Else
                        srFineManipulationLblCell.Visible = False
                        srFineManipulationTxtCell.Visible = False
                        srFineManipulationOtherTxt.Text = ""
                    End If


                    srRepeatRFootWorkDrp.SelectedIndex = objSqlDataRdr.Item(36)
                    If objSqlDataRdr.Item(36) = 3 Then
                        srRepeatRFootWorkLblCell.Visible = True
                        srRepeatRFootWorkTxtCell.Visible = True
                        srRepeatRFootWorkOtherTxt.Text = objSqlDataRdr.Item(63)
                    Else
                        srRepeatRFootWorkLblCell.Visible = False
                        srRepeatRFootWorkTxtCell.Visible = False
                        srRepeatRFootWorkOtherTxt.Text = ""
                    End If

                    srRepeatLFootWorkDrp.SelectedIndex = objSqlDataRdr.Item(37)
                    If objSqlDataRdr.Item(37) = 3 Then
                        srRepeatLFootWorkLblCell.Visible = True
                        srRepeatLFootWorkTxtCell.Visible = True
                        srRepeatLFootWorkOtherTxt.Text = objSqlDataRdr.Item(64)
                    Else
                        srRepeatLFootWorkLblCell.Visible = False
                        srRepeatLFootWorkTxtCell.Visible = False
                        srRepeatLFootWorkOtherTxt.Text = ""
                    End If

                    'srRestrictionsDrp.SelectedIndex = objSqlDataRdr.Item(38)

                    srCanSitPerDayDrp.SelectedValue = objSqlDataRdr.Item(39)
                    If objSqlDataRdr.Item(39) = 13 Then
                        srCanSitPerDayLblCell.Visible = True
                        srCanSitPerDayTxtCell.Visible = True
                        srCanSitPerDayOtherTxt.Text = objSqlDataRdr.Item(65)
                    Else
                        srCanSitPerDayLblCell.Visible = False
                        srCanSitPerDayTxtCell.Visible = False
                        srCanSitPerDayOtherTxt.Text = ""
                    End If

                    srCanStandPerDayDrp.SelectedValue = objSqlDataRdr.Item(40)
                    If objSqlDataRdr.Item(40) = 13 Then
                        srCanStandPerDayLblCell.Visible = True
                        srCanStandPerDayTxtCell.Visible = True
                        srCanStandPerDayOtherTxt.Text = objSqlDataRdr.Item(66)
                    Else
                        srCanStandPerDayLblCell.Visible = False
                        srCanStandPerDayTxtCell.Visible = False
                        srCanStandPerDayOtherTxt.Text = ""
                    End If

                    srCanWalkPerDayDrp.SelectedValue = objSqlDataRdr.Item(85)
                    If objSqlDataRdr.Item(85) = 13 Then
                        srCanWalkPerDayLblCell.Visible = True
                        srCanWalkPerDayTxtCell.Visible = True
                        srCanWalkPerDayOtherTxt.Text = objSqlDataRdr.Item(86)
                    Else
                        srCanWalkPerDayLblCell.Visible = False
                        srCanWalkPerDayTxtCell.Visible = False
                        srCanWalkPerDayOtherTxt.Text = ""
                    End If

                    srCanDrivePerDayDrp.SelectedValue = objSqlDataRdr.Item(41)
                    If objSqlDataRdr.Item(41) = 13 Then
                        srCanDrivePerDayLblCell.Visible = True
                        srCanDrivePerDayTxtCell.Visible = True
                        srCanDrivePerDayOtherTxt.Text = objSqlDataRdr.Item(67)
                    Else
                        srCanDrivePerDayLblCell.Visible = False
                        srCanDrivePerDayTxtCell.Visible = False
                        srCanDrivePerDayOtherTxt.Text = ""
                    End If

                    srCanBendDrp.SelectedIndex = objSqlDataRdr.Item(42)
                    If objSqlDataRdr.Item(42) = 4 Then
                        srCanBendLblCell.Visible = True
                        srCanBendTxtCell.Visible = True
                        srCanBendOtherTxt.Text = objSqlDataRdr.Item(68)
                    Else
                        srCanBendLblCell.Visible = False
                        srCanBendTxtCell.Visible = False
                        srCanBendOtherTxt.Text = ""
                    End If

                    srCanSquatDrp.SelectedIndex = objSqlDataRdr.Item(43)
                    If objSqlDataRdr.Item(44) = 4 Then
                        srCanSquatLblCell.Visible = True
                        srCanSquatTxtCell.Visible = True
                        srCanSquatOtherTxt.Text = objSqlDataRdr.Item(71)
                    Else
                        srCanSquatLblCell.Visible = False
                        srCanSquatTxtCell.Visible = False
                        srCanSquatOtherTxt.Text = ""
                    End If

                    srCanClimbStairsDrp.SelectedIndex = objSqlDataRdr.Item(44)
                    If objSqlDataRdr.Item(44) = 4 Then
                        srCanClimbStairsLblCell.Visible = True
                        srCanClimbStairsTxtCell.Visible = True
                        srCanClimbStairsOtherTxt.Text = objSqlDataRdr.Item(69)
                    Else
                        srCanClimbStairsLblCell.Visible = False
                        srCanClimbStairsTxtCell.Visible = False
                        srCanClimbStairsOtherTxt.Text = ""
                    End If

                    srCanClimbLadderDrp.SelectedIndex = objSqlDataRdr.Item(45)
                    If objSqlDataRdr.Item(45) = 4 Then
                        srCanClimbLadderLblCell.Visible = True
                        srCanClimbLadderTxtCell.Visible = True
                        srCanClimbLadderOtherTxt.Text = objSqlDataRdr.Item(72)
                    Else
                        srCanClimbLadderLblCell.Visible = False
                        srCanClimbLadderTxtCell.Visible = False
                        srCanClimbLadderOtherTxt.Text = ""
                    End If

                    srCanCrawlDrp.SelectedIndex = objSqlDataRdr.Item(46)
                    If objSqlDataRdr.Item(46) = 4 Then
                        srCanCrawlLblCell.Visible = True
                        srCanCrawlTxtCell.Visible = True
                        srCanCrawlOtherTxt.Text = objSqlDataRdr.Item(70)
                    Else
                        srCanCrawlLblCell.Visible = False
                        srCanCrawlTxtCell.Visible = False
                        srCanCrawlOtherTxt.Text = ""
                    End If

                    srCanReachDrp.SelectedIndex = objSqlDataRdr.Item(47)
                    If objSqlDataRdr.Item(47) = 4 Then
                        srCanReachlLblCell.Visible = True
                        srCanReachCell.Visible = True
                        srCanReachOtherTxt.Text = objSqlDataRdr.Item(73)
                    Else
                        srCanReachlLblCell.Visible = False
                        srCanReachCell.Visible = False
                        srCanReachOtherTxt.Text = ""
                    End If
                    srReferredToSpecialistDrp.SelectedIndex = objSqlDataRdr.Item(49)

                    srReferredDrNameTxt.Text = myCStr(objSqlDataRdr.Item(87))
                    srSpecialistApptDateTxt.Text = myCStr(objSqlDataRdr.Item(91))

                    If IsDate(srSpecialistApptDateTxt.Text) Then
                        srSpecialistApptDateTxt.Text = ConvertDate(srSpecialistApptDateTxt.Text)
                    End If

                    If Not objSqlDataRdr.Item(88) Is DBNull.Value Then
                        srSpecialistApptHourDrp.SelectedValue = myCStr(objSqlDataRdr.Item(88))
                        srSpecialistApptMinuteDrp.SelectedValue = myCStr(objSqlDataRdr.Item(89))
                        srSpecialistApptAmPmDrp.SelectedValue = myCStr(objSqlDataRdr.Item(90))
                    End If

                    If objSqlDataRdr.Item(25) > 0 Then
                        srTherapyWeeksTxt.Text = objSqlDataRdr.Item(25)
                    End If


                    srEffectiveDateTxt.Text = myCStr(objSqlDataRdr.Item(30))
                    If IsDate(srEffectiveDateTxt.Text) Then
                        srEffectiveDateTxt.Text = ConvertDate(srEffectiveDateTxt.Text)
                    End If
                    srUntilDateTxt.Text = myCStr(objSqlDataRdr.Item(31)).ToUpper


                    srNextAptDateTxt.Text = myCStr(objSqlDataRdr.Item(51)).ToUpper

                    srTypeOfSpecialistTxt.Text = myCStr(objSqlDataRdr.Item(50)).ToUpper
                    srNextAptHourDrp.SelectedIndex = objSqlDataRdr.Item(55)
                    srNextAptMinuteDrp.SelectedIndex = objSqlDataRdr.Item(56)
                    srNextAptAmPmDrp.SelectedIndex = objSqlDataRdr.Item(57)

                    If objSqlDataRdr.Item(13) = 1 Then
                        srDiagXRayChk.Checked = True
                    End If

                    If objSqlDataRdr.Item(14) = 1 Then
                        srDiagMRIChk.Checked = True
                    End If

                    If objSqlDataRdr.Item(15) = 1 Then
                        srDiagEMGNCVChk.Checked = True
                    End If

                    If objSqlDataRdr.Item(16) = 1 Then
                        srDiagOtherChk.Checked = True
                    End If

                    If objSqlDataRdr.Item(20) = 1 Then
                        srPTChk.Checked = True
                    End If

                    If objSqlDataRdr.Item(21) = 1 Then
                        srOTChk.Checked = True
                    End If

                    If objSqlDataRdr.Item(22) = 1 Then
                        srWHChk.Checked = True
                    End If

                    srTimeInTxt.Text = myCStr(objSqlDataRdr.Item(83))
                    srTimeOutTxt.Text = myCStr(objSqlDataRdr.Item(84))
                    srDiagBodyPartTxt.Text = myCStr(objSqlDataRdr.Item(92))
                    srClinicNameTxt.Text = myCStr(objSqlDataRdr.Item(94))
                    srReasonForReferraltTxt.Text = myCStr(objSqlDataRdr.Item(97))
                End If

                'StatusReprtFormPanel.Visible = True

                objSqlDataRdr.Close()
                objSqlDataRdr.Dispose()

                viewit()

                'Catch ex As Exception
                'Response.Write("Can't load Web page " & ex.Message)
                ' Finally
                objConnection.Close()
                objConnection.Dispose()
                'End Try

            Else
                Dim objSqlCmd As New OdbcCommand
                Dim objSqlDataRdr As OdbcDataReader = Nothing
                Dim strConnection As String = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;" 'ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()
                Dim objConnection As New OdbcConnection(strConnection)

                ipCustAccountID.Value = e.CommandArgument

                objConnection.Open()
                objSqlCmd.Connection = objConnection
                ' get folder
                objSqlCmd = New OdbcCommand("", objConnection)
                objSqlCmd.CommandText = "{call sp_getEmployerPartyIDbyCustomerID(?)}"
                objSqlCmd.CommandType = Data.CommandType.StoredProcedure
                objSqlCmd.Parameters.Add(New OdbcParameter("@IPCUSTACCTID", OdbcType.VarChar, 255)).Value = ipCustAccountID.Value
                objSqlDataRdr = objSqlCmd.ExecuteReader()
                Dim emp_id As Integer

                If objSqlDataRdr.Read() Then
                    emp_id = objSqlDataRdr.Item(0)
                    'Response.Write("The emp_id is: " & emp_id & "The cust acct id is: " & ipCustAccountID.Value & "<BR>")

                    GetTriagePDFs(emp_id.ToString, ipCustAccountID.Value)

                    'Response.Write("The full file name is: " + FullFileName & "<BR>")

                    If FullFileName <> Nothing Then
                        FullFileName = FullFileName.Replace("C:\inetpub\wwwroot\myeworkwell\triage", String.Empty)
                        FullFileName = FullFileName.Replace("\", "/")
                        'Dim popupScript As String = "<script language=javascript> window.open('/triage" & FullFileName & "') </script>"
                        Dim popupScript As String = "<script language=javascript> window.open('" & FullFileName & "') </script>"

                        ClientScript.RegisterStartupScript(Me.[GetType](), "callpopup", popupScript)
                    Else
                        ' no Triage Reports found
                        error_notification.Visible = True
                        error_notification.Text = "No Triage Reports found!"
                        error_notification.Show()
                    End If

                Else
                    ' no Triage Reports found
                    error_notification.Title = "Employer not found!"
                End If

            End If

            'Else
            '    Dim popupScript As String = "<script language=javascript> window.open('/PDFs/JOSEPH_SMITH_TRIAGE_4_18_2012.pdf') </script>"
            '    ClientScript.RegisterStartupScript(Me.[GetType](), "callpopup", popupScript)

        End If

    End Sub

    Sub GetTriagePDFs(emp_id As String, cust_account_id As String)
        Dim DocumentVirtualPath As String = "\triage\" & emp_id & "\" & cust_account_id & "\"

        GetFiles("\triage\" & emp_id.ToString & "\" & cust_account_id.ToString & "\")

    End Sub

    Public Sub GetFiles(ByVal path As String)
        Dim myPath As String = Server.MapPath(path)

        If File.Exists(myPath) Then
            ' This path is a file 
            ProcessFile(myPath)
        ElseIf Directory.Exists(myPath) Then
            ' This path is a directory 
            ProcessDirectory(myPath)
        End If
    End Sub

    Public Sub ProcessFile(ByVal path As String)
        Dim fi As New FileInfo(path)

        If fi.FullName.ToLower.Contains(".pdf") Then
            filename = fi.Name
            'Response.Write("The file name is: " + filename & "<BR>")
            FullFileName = fi.FullName.Replace("D:\Hosting\6546477\html", "")
            'Response.Write("The full file name is: " + FullFileName & "<BR>")
        End If

    End Sub

    Public Sub ProcessDirectory(ByVal targetDirectory As String)
        Dim fileEntries As String() = Directory.GetFiles(targetDirectory, "*.pdf")
        Dim subdirectoryEntries As String() = Directory.GetDirectories(targetDirectory)

        ' Process the list of files found in the directory. 
        For Each fileName As String In fileEntries
            ProcessFile(fileName)
            Exit For ' only get one file if more than one
        Next

        ' Recurse into subdirectories of this directory. 
        For Each subdirectory As String In subdirectoryEntries
            ProcessDirectory(subdirectory)
        Next

    End Sub

    Protected Sub ciStatusRptViewGrd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles ciStatusRptViewGrd.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Try

           
                Dim sSSNumber As String = myCStr(DataBinder.Eval(e.Row.DataItem, "SOCIALSECURITYNUMBER"))
                Dim sFName As String = myCStr(DataBinder.Eval(e.Row.DataItem, "IPFIRSTNAME")).Trim()
                Dim sLName As String = myCStr(DataBinder.Eval(e.Row.DataItem, "IPLASTNAME")).Trim()
                Dim sMName As String = myCStr(DataBinder.Eval(e.Row.DataItem, "IPMIDDLENAME")).Trim()
                Dim sReportType As String = myCStr(DataBinder.Eval(e.Row.DataItem, "REPORT_TYPE")).Trim()

                If StrComp(sReportType, "0") = 0 Then
                    sReportType = "Initial Appt"
                ElseIf StrComp(sReportType, "1") = 0 Then
                    sReportType = "Follow Up Appt"
                    'e.Row.Cells(5).Visible = False
                ElseIf StrComp(sReportType, "2") = 0 Then
                    sReportType = "R.T.W. Appt"
                    'e.Row.Cells(5).Visible = False
                Else
                    sReportType = "UNKNOWN"
                    'e.Row.Cells(5).Visible = False
                End If

                Dim dr As DataRow = DirectCast(e.Row.DataItem, DataRowView).Row

                If String.IsNullOrEmpty(sMName) = False Then
                    If sMName.Length = 1 Then
                        sMName = sMName & "."
                    End If
                End If

                If String.IsNullOrEmpty(sSSNumber.Trim) = False Then
                    If sSSNumber.Length >= 7 Then
                        'Response.Write(sSSNumber.Length)
                        sSSNumber = "XXX-XX-" & sSSNumber.Substring(7)
                    Else
                        If StrComp(sSSNumber.Trim, "--") = 0 Then
                            sSSNumber = "XXX-XX-XXXX"
                        End If
                    End If
                Else
                    sSSNumber = "XXX-XX-XXXX"
                End If



                e.Row.Cells(0).Text = sLName & ", " & sFName & " " & sMName
                e.Row.Cells(4).Text = sSSNumber
                e.Row.Cells(5).Text = sReportType
            Catch ex As Exception
                Response.Write("Error RowDataBound" & ex.ToString & "<BR>")
            End Try

        End If

    End Sub

    Sub viewit()

        'Dim fStatusReport As New Document()
        'Dim sPath As String = Server.MapPath("PDFs")
        'PdfWriter.GetInstance(fStatusReport, New FileStream(sPath & "/StatusReport.pdf", FileMode.Create))
        'Dim c1 As New Chunk("Employee Name: " & ipFirstNameTxt.Text)
        'c1.SetUnderline(0.5F, -1.5F)


        'Dim c1 As New Chunk("Employee Name: ")

        'fStatusReport.Add(c1)

        Dim pdfPath As String = Server.MapPath("PDFs")
        Dim imgPath As String = Server.MapPath("images")
        Dim fontpath As String = Server.MapPath("fonts")


        'Dim r As New Rectangle(400, 200)
        Dim doc As New Document(PageSize.LETTER, 50, 30, 10, 10)
        Dim sFileName As String = "/block2" & Replace(Replace(Now.ToString, "/", "_"), ":", "-") & ".pdf"
        Dim sFileName2 As String = "/block2.pdf"
        'Response.Write(sFileName)

        'Try

        PdfWriter.GetInstance(doc, New FileStream(pdfPath & sFileName, FileMode.Create))

        doc.Open()


        Dim customfont As BaseFont = BaseFont.CreateFont(fontpath & "/calibri.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)

        Dim font As New Font(customfont, 6)

        Dim ipEmpInfoTbl As New PdfPTable(4)
        Dim ipAGHInfoTbl As New PdfPTable(2)
        Dim ipAGHWidths As Single() = New Single() {125.0F, 200.0F}
        ipAGHInfoTbl.TotalWidth = 325.0F
        ipAGHInfoTbl.HorizontalAlignment = Element.ALIGN_LEFT
        ipAGHInfoTbl.SetWidths(ipAGHWidths)
        ipAGHInfoTbl.LockedWidth = True



        'Dim hdrCell As New PdfPCell(New Phrase("WorkWell  Physicians, PC"))
        'Dim empInfoHdrCell As New PdfPCell(Image.GetInstance(imgPath + "/tiny-meww-header-transport-small copy.png"))
        'Dim empInfoHdrCell As New PdfPCell(Image.GetInstance(imgPath + "/workwell_inc.png"))

        If srClincTypeDrp.SelectedValue = 1 Then
            Dim ipCompNameWidths As Single() = New Single() {315.0F, 45.0F}
            Dim ipCompNameHdrTbl As New PdfPTable(2)
            ipCompNameHdrTbl.TotalWidth = 360.0F
            ipCompNameHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipCompNameHdrTbl.AddCell(getCompanyNameCell(0, 0))
            ipCompNameHdrTbl.AddCell(getCompanyNameCell(1, 0))
            ipCompNameHdrTbl.SetWidths(ipCompNameWidths)
            'ipCompNameHdrTbl.AddCell(ipIncLogoCell)
            ipCompNameHdrTbl.LockedWidth = True
            doc.Add(ipCompNameHdrTbl)
        Else
            Dim ipCompNameWidths As Single() = New Single() {245.0F, 450.0F}
            Dim ipCompNameHdrTbl As New PdfPTable(2)
            ipCompNameHdrTbl.TotalWidth = 695.0F
            ipCompNameHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipCompNameHdrTbl.AddCell(getCompanyNameCell(0, 0))
            ipCompNameHdrTbl.AddCell(getCompanyNameCell(2, 0))
            ipCompNameHdrTbl.SetWidths(ipCompNameWidths)
            'ipCompNameHdrTbl.AddCell(ipIncLogoCell)
            ipCompNameHdrTbl.LockedWidth = True
            doc.Add(ipCompNameHdrTbl)
        End If




        'Dim ipEmpWidths As Single() = New Single() {75.0F, 200.0F, 75.0F, 250.0F}


        Dim cell As New PdfPCell(New Phrase("Employee Name:", font))
        Dim empNameHdr As New Phrase("Employer Name:", font)
        Dim ssClaimNumHdr As New Phrase("SS#/Claim#:", font)
        Dim ipDOEHdr As New Phrase("Date of Exam:", font)
        Dim ipDOIHdr As New Phrase("Date of Injury:", font)
        Dim ipDrNameHdr As New Phrase("Physician Name:", font)

        'empInfoHdrCell.Border = 0
        cell.Border = 0
        'Dim cell As New PdfPCell(New Phrase("Employee Name:", font))

        'empInfoHdrCell.Colspan = 4
        'empInfoHdrCell.HorizontalAlignment = 1
        'ipEmpInfoTbl.AddCell(empInfoHdrCell)
        ipEmpInfoTbl.TotalWidth = 500.0F
        'ipEmpInfoTbl.SetWidths(ipEmpWidths)

        ipEmpInfoTbl.LockedWidth = True
        ipEmpInfoTbl.AddCell(getCell(" ", 2, True))
        ipEmpInfoTbl.AddCell(getCell(" ", 0, False))
        ipEmpInfoTbl.AddCell(getCell(" ", 2, True))
        ipEmpInfoTbl.AddCell(getCell(" ", 0, False))
        ipEmpInfoTbl.AddCell(getCell("Employee Name:", 2, True))
        ipEmpInfoTbl.AddCell(getCell(srIPFNameTxt.Text.ToUpper & " " & srIPMNameTxt.Text.ToUpper & " " & srIPLNameTxt.Text.ToUpper, 0, False))
        ipEmpInfoTbl.AddCell(getCell("Date of Exam:", 2, True))
        ipEmpInfoTbl.AddCell(getCell(srIPDOETxt.Text.ToUpper, 0, False))
        ipEmpInfoTbl.AddCell(getCell("Employer Name:", 2, True))
        ipEmpInfoTbl.AddCell(getCell(srIPEmployerNameTxt.Text.ToUpper, 0, False))
        ipEmpInfoTbl.AddCell(getCell("Date of Injury:", 2, True))
        ipEmpInfoTbl.AddCell(getCell(srIPDOITxt.Text.ToUpper, 0, False))
        ipEmpInfoTbl.AddCell(getCell("Social Security #:", 2, True))
        If String.IsNullOrEmpty(srSocialSecurityNumTxt.Text) = False Then
            If srSocialSecurityNumTxt.Text.Replace("-", "").Length >= 4 Then
                ipEmpInfoTbl.AddCell(getCell("XXX-XX-" & srSocialSecurityNumTxt.Text.Replace("-", "").Substring(srSocialSecurityNumTxt.Text.Replace("-", "").Length - 4, 4), 0, False))
            Else
                ipEmpInfoTbl.AddCell(getCell("XXX-XX-XXXX", 0, False))
            End If

        Else
            ipEmpInfoTbl.AddCell(getCell("XXX-XX-XXXX", 0, False))
        End If

        ipEmpInfoTbl.AddCell(getCell("Claim #:", 2, True))

        If StrComp(srSocialSecurityNumTxt.Text.Trim, srIPSSNumClaimNumTxt.Text.Trim) = 0 Then
            ipEmpInfoTbl.AddCell(getCell("N/A", 0, False))
        Else
            ipEmpInfoTbl.AddCell(getCell(srIPSSNumClaimNumTxt.Text.ToUpper, 0, False))
        End If

        ipEmpInfoTbl.AddCell(getCell("Date of Birth:", 2, True))
        ipEmpInfoTbl.AddCell(getCell(srIPDOBTxt.Text.ToUpper, 0, False))
        ipEmpInfoTbl.AddCell(getCell(" ", 2, True))
        ipEmpInfoTbl.AddCell(getCell(" ", 0, False))

        ipEmpInfoTbl.AddCell(getCell("Clinic Type:", 2, True))
        ipEmpInfoTbl.AddCell(getCell(srClincTypeDrp.SelectedItem.Text.ToUpper, 0, False))
        ipEmpInfoTbl.AddCell(getCell(" ", 2, True))
        ipEmpInfoTbl.AddCell(getCell(" ", 0, False))

        ipEmpInfoTbl.AddCell(getCell("Practice Name:", 2, True))
        ipEmpInfoTbl.AddCell(getCell(srClinicNameTxt.Text.ToUpper, 0, False))
        ipEmpInfoTbl.AddCell(getCell(" ", 2, True))
        ipEmpInfoTbl.AddCell(getCell(" ", 0, False))


        doc.Add(ipEmpInfoTbl)
        If empPartyID.Value = 222397 Then
            ipAGHInfoTbl.AddCell(getCell(" ", 2, True))
            ipAGHInfoTbl.AddCell(getCell(" ", 0, False))
            ipAGHInfoTbl.AddCell(getCell("Time In:", 2, True))
            ipAGHInfoTbl.AddCell(getCell(srTimeInTxt.Text, 0, False))
            ipAGHInfoTbl.AddCell(getCell("Time Out:", 2, True))
            ipAGHInfoTbl.AddCell(getCell(srTimeOutTxt.Text, 0, False))

            doc.Add(ipAGHInfoTbl)
        End If


        'Dim ipIncLogoCell As New PdfPCell(Image.GetInstance(imgPath + "/logo_inc.png"))


        Dim ipDiagnosisWidths As Single() = New Single() {105.0F, 350.0F}
        Dim ipDiagHdrTbl As New PdfPTable(1)
        ipDiagHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
        ipDiagHdrTbl.TotalWidth = 600.0F
        ipDiagHdrTbl.AddCell(getHdrCell("Diagnosis", 0))
        doc.Add(ipDiagHdrTbl)

        Dim ipDiagnosisTbl As New PdfPTable(2)

        'Dim diagnosisHdrCell As New PdfPCell(New Phrase("Diagnosis"))
        'diagnosisHdrCell.Border = 0

        'diagnosisHdrCell.Colspan = 2
        'diagnosisHdrCell.HorizontalAlignment = 0
        'ipDiagnosisTbl.AddCell(diagnosisHdrCell)
        ipDiagnosisTbl.TotalWidth = 455.0F
        ipDiagnosisTbl.SetWidths(ipDiagnosisWidths)
        ipDiagnosisTbl.HorizontalAlignment = Element.ALIGN_LEFT
        ipDiagnosisTbl.LockedWidth = True

        ipDiagnosisTbl.LockedWidth = True

        ipDiagnosisTbl.AddCell(getCell(" ", 2, True))
        ipDiagnosisTbl.AddCell(getCell(" ", 0, False))

        ipDiagnosisTbl.AddCell(getCell("Body Part:", 2, True))
        ipDiagnosisTbl.AddCell(getCell(srBodyPartTxt.Text.ToUpper, 0, False))

        If String.IsNullOrEmpty(srDiagonsisTxt.Text.ToUpper) = False Then
            ipDiagnosisTbl.AddCell(getCell("Diagnosis:", 2, True))
            ipDiagnosisTbl.AddCell(getCell(srDiagonsisTxt.Text.ToUpper, 0, False))
        End If

        If String.IsNullOrEmpty(srObjectiveFindingsTxt.Text) = False Then
            ipDiagnosisTbl.AddCell(getCell("Objective Findings:", 2, True))
            ipDiagnosisTbl.AddCell(getCell(srObjectiveFindingsTxt.Text.ToUpper, 0, False))
        End If

        If String.IsNullOrEmpty(srMechanismOfInjuryTxt.Text) = False Then
            ipDiagnosisTbl.AddCell(getCell("Mechanism of Injury:", 2, True))
            ipDiagnosisTbl.AddCell(getCell(srMechanismOfInjuryTxt.Text.ToUpper, 0, False))
        End If

        doc.Add(ipDiagnosisTbl)




        If srDiagEMGNCVChk.Checked = True Or srDiagMRIChk.Checked = True Or srDiagXRayChk.Checked = True Or srDiagOtherChk.Checked = True Then

            Dim ipDXWidths As Single() = New Single() {105.0F, 350.0F}
            Dim ipDXHdrTbl As New PdfPTable(1)
            ipDXHdrTbl.TotalWidth = 455.0F
            ipDXHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipDXHdrTbl.AddCell(getCell(" ", 0, False))
            ipDXHdrTbl.AddCell(getHdrCell("Diagnostics", 0))

            doc.Add(ipDXHdrTbl)

            Dim ipDXTbl As New PdfPTable(2)
            ipDXTbl.TotalWidth = 455.0F
            ipDXTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipDXTbl.SetWidths(ipDXWidths)
            ipDXTbl.LockedWidth = True

            ipDXTbl.AddCell(getCell(" ", 2, True))
            ipDXTbl.AddCell(getCell(" ", 0, False))
            ipDXTbl.AddCell(getCell("Diagnostics Needed:", 2, True))
            ipDXTbl.AddCell(getCell(GetChkBoxNeeded(1), 0, False))

            If String.IsNullOrEmpty(srDiagBodyPartTxt.Text) = False Then
                ipDXTbl.AddCell(getCell("Body Part:", 2, True))
                ipDXTbl.AddCell(getCell(srDiagBodyPartTxt.Text.ToUpper, 0, False))
            End If

            If String.IsNullOrEmpty(srReasonTxt.Text) = False Then
                ipDXTbl.AddCell(getCell("Reason:", 2, True))
                ipDXTbl.AddCell(getCell(srReasonTxt.Text.ToUpper, 0, False))
            End If

            doc.Add(ipDXTbl)

        End If
        'ipDXTbl.AddCell(getCell(srDXTypeDrp.SelectedItem.Text, 0, False))        

        'End If
        If srPTChk.Checked = True Or srOTChk.Checked Or srWHChk.Checked = True Or srVistTypeDrp.SelectedValue <> 0 Or srTimesPerWeekDrp.SelectedValue <> 0 Or String.IsNullOrEmpty(srTherapyWeeksTxt.Text) = False Then

            Dim ipThrpyWidths As Single() = New Single() {105.0F, 400.0F}
            Dim ipThrpyHdrTbl As New PdfPTable(1)
            ipThrpyHdrTbl.TotalWidth = 405.0F
            ipThrpyHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipThrpyHdrTbl.AddCell(getCell(" ", 0, False))
            ipThrpyHdrTbl.AddCell(getHdrCell("Therapy", 0))

            doc.Add(ipThrpyHdrTbl)

            Dim ipTherapyTbl As New PdfPTable(2)
            ipTherapyTbl.TotalWidth = 500.0F
            ipTherapyTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipTherapyTbl.SetWidths(ipThrpyWidths)
            ipTherapyTbl.LockedWidth = True

            ipTherapyTbl.AddCell(getCell(" ", 2, True))
            ipTherapyTbl.AddCell(getCell(" ", 0, False))
            ipTherapyTbl.AddCell(getCell("Therapy Needed:", 2, True))
            ipTherapyTbl.AddCell(getCell(GetChkBoxNeeded(2), 0, False))
            'ipTherapyTbl.AddCell(getCell(srTherapyTypeDrp.SelectedItem.Text, 0, False))
            ipTherapyTbl.AddCell(getCell("Visit Type:", 2, True))
            ipTherapyTbl.AddCell(getCell(srVistTypeDrp.SelectedItem.Text.ToUpper, 0, False))
            ipTherapyTbl.AddCell(getCell("Times Per Week:", 2, True))
            ipTherapyTbl.AddCell(getCell(srTimesPerWeekDrp.SelectedItem.Text.ToUpper, 0, False))
            ipTherapyTbl.AddCell(getCell("Weeks Needed:", 2, True))
            ipTherapyTbl.AddCell(getCell(srTherapyWeeksTxt.Text.ToUpper, 0, False))

            doc.Add(ipTherapyTbl)

        End If

        If String.IsNullOrEmpty(srMedicationTxt.Text) = False Or String.IsNullOrEmpty(srInstructionsTxt.Text) = False Or String.IsNullOrEmpty(srOtherTxt.Text) = False Then

            Dim ipTreatmentWidths As Single() = New Single() {105.0F, 350.0F}
            Dim ipTreatmentHdrTbl As New PdfPTable(1)
            ipTreatmentHdrTbl.TotalWidth = 600.0F
            ipTreatmentHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipTreatmentHdrTbl.AddCell(getCell(" ", 0, False))
            ipTreatmentHdrTbl.AddCell(getHdrCell("Treatment", 0))

            doc.Add(ipTreatmentHdrTbl)


            Dim ipTreatmentTbl As New PdfPTable(2)
            ipTreatmentTbl.TotalWidth = 455.0F
            ipTreatmentTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipTreatmentTbl.SetWidths(ipTreatmentWidths)
            ipTreatmentTbl.LockedWidth = True

            ipTreatmentTbl.AddCell(getCell(" ", 2, True))
            ipTreatmentTbl.AddCell(getCell(" ", 0, False))
            If String.IsNullOrEmpty(srMedicationTxt.Text) = False Then
                ipTreatmentTbl.AddCell(getCell("Medication:", 2, True))
                ipTreatmentTbl.AddCell(getCell(srMedicationTxt.Text.ToUpper, 0, False))
            End If
            If String.IsNullOrEmpty(srInstructionsTxt.Text) = False Then
                ipTreatmentTbl.AddCell(getCell("Instructions:", 2, True))
                ipTreatmentTbl.AddCell(getCell(srInstructionsTxt.Text.ToUpper, 0, False))
            End If
            If String.IsNullOrEmpty(srOtherTxt.Text) = False Then
                ipTreatmentTbl.AddCell(getCell("Other:", 2, True))
                ipTreatmentTbl.AddCell(getCell(srOtherTxt.Text.ToUpper, 0, False))
            End If
            doc.Add(ipTreatmentTbl)
        End If

        Dim ipWorkStatusHdrTbl As New PdfPTable(1)
        ipWorkStatusHdrTbl.TotalWidth = 600.0F
        ipWorkStatusHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
        ipWorkStatusHdrTbl.AddCell(getCell(" ", 0, False))
        ipWorkStatusHdrTbl.AddCell(getHdrCell("Work Status", 0))

        doc.Add(ipWorkStatusHdrTbl)

        Dim ipWorkStatusTbl As New PdfPTable(6)
        'Dim ipWorkStatusWidths As Single() = New Single() {75.0F, 40.0F, 50.0F, 50.0F, 50.0F, 50.0F}
        Dim ipWorkStatusWidths As Single() = New Single() {115.0F, 100.0F, 40.0F, 70.0F, 30.0F, 70.0F}
        ipWorkStatusTbl.TotalWidth = 425.0F
        ipWorkStatusTbl.HorizontalAlignment = Element.ALIGN_LEFT
        ipWorkStatusTbl.SetWidths(ipWorkStatusWidths)
        ipWorkStatusTbl.LockedWidth = True

        ipWorkStatusTbl.AddCell(getCell(" ", 2, True))
        ipWorkStatusTbl.AddCell(getCell(" ", 0, False))
        ipWorkStatusTbl.AddCell(getCell(" ", 2, True))
        ipWorkStatusTbl.AddCell(getCell(" ", 0, False))
        ipWorkStatusTbl.AddCell(getCell(" ", 0, True))
        ipWorkStatusTbl.AddCell(getCell(" ", 0, False))
        ipWorkStatusTbl.AddCell(getCell("Work Status:", 2, True))
        ipWorkStatusTbl.AddCell(getCell(srWorkStatusDrp.SelectedItem.Text.ToUpper, 0, False))
        ipWorkStatusTbl.AddCell(getCell("Effective: ", 2, True))
        If IsDate(srEffectiveDateTxt.Text) = True Then
            ipWorkStatusTbl.AddCell(getCell(ConvertDate(srEffectiveDateTxt.Text.ToUpper), 0, False))
        Else
            ipWorkStatusTbl.AddCell(getCell(srEffectiveDateTxt.Text.ToUpper, 0, False))
        End If

        If String.IsNullOrEmpty(srUntilDateTxt.Text) = False Then
            ipWorkStatusTbl.AddCell(getCell("Until: ", 2, True))
            If IsDate(srUntilDateTxt.Text) = True Then
                ipWorkStatusTbl.AddCell(getCell(ConvertDate(srUntilDateTxt.Text.ToUpper), 0, False))
            Else
                ipWorkStatusTbl.AddCell(getCell(srUntilDateTxt.Text.ToUpper, 0, False))
            End If
        Else
            ipWorkStatusTbl.AddCell(getCell(" ", 2, True))
            ipWorkStatusTbl.AddCell(getCell(" ", 0, False))
        End If

        doc.Add(ipWorkStatusTbl)

        If String.IsNullOrEmpty(srOtherRestrictionsTxt.Text) = False Then
            Dim ipModifiedDutyCommentsTbl As New PdfPTable(2)
            Dim ipModifiedDutyCommentWidths As Single() = New Single() {115.0F, 350.0F}
            'Dim diagnosisHdrCell As New PdfPCell(New Phrase("Diagnosis"))
            'diagnosisHdrCell.Border = 0

            'diagnosisHdrCell.Colspan = 2
            'diagnosisHdrCell.HorizontalAlignment = 0
            'ipDiagnosisTbl.AddCell(diagnosisHdrCell)
            ipModifiedDutyCommentsTbl.TotalWidth = 465.0F
            ipModifiedDutyCommentsTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipModifiedDutyCommentsTbl.SetWidths(ipModifiedDutyCommentWidths)
            ipModifiedDutyCommentsTbl.LockedWidth = True

            'ipModifiedDutyCommentsTbl.LockedWidth = True
            ipModifiedDutyCommentsTbl.AddCell(getCell("Comments/Restrictions:", 2, True))
            ipModifiedDutyCommentsTbl.AddCell(getCell(srOtherRestrictionsTxt.Text.ToUpper, 0, False))
            'Response.Write(srUseRightFootRdo.SelectedValue) srSimpleGraspingDrp

            doc.Add(ipModifiedDutyCommentsTbl)
        End If

        If srWorkStatusDrp.SelectedValue = 3 Then
            Dim ipModifiedDutyWidths As Single() = New Single() {175.0F, 125.0F, 175.0F, 125.0F}
            Dim ipModifiedDutyHdrTbl As New PdfPTable(1)
            ipModifiedDutyHdrTbl.TotalWidth = 600.0F
            ipModifiedDutyHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipModifiedDutyHdrTbl.AddCell(getCell(" ", 0, False))
            ipModifiedDutyHdrTbl.AddCell(getHdrCell("Modified Duty Restrictions", 0))

            doc.Add(ipModifiedDutyHdrTbl)

            Dim ipModifiedDutyUpperWidths As Single() = New Single() {175.0F, 100.0F, 155.0F, 95.0F}
            Dim ipModifiedDutyUpperTbl As New PdfPTable(4)
            ipModifiedDutyUpperTbl.TotalWidth = 488.0F
            ipModifiedDutyUpperTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipModifiedDutyUpperTbl.SetWidths(ipModifiedDutyUpperWidths)

            Dim ipModifiedDutyInnerWidths As Single() = New Single() {175.0F, 100.0F, 155.0F, 95.0F}
            Dim ipModifiedDutyInnerTbl As New PdfPTable(4)
            ipModifiedDutyInnerTbl.TotalWidth = 550.0F
            ipModifiedDutyInnerTbl.SetWidths(ipModifiedDutyInnerWidths)

            Dim ipModifiedDutyOtherInnerWidths As Single() = New Single() {175.0F, 150.0F, 175.0F, 150.0F}
            Dim ipModifiedDutyOtherInnerTbl As New PdfPTable(4)
            ipModifiedDutyOtherInnerTbl.TotalWidth = 650.0F
            ipModifiedDutyOtherInnerTbl.SetWidths(ipModifiedDutyOtherInnerWidths)

            Dim ipModifiedCanLiftOtherWidths As Single() = New Single() {175.0F, 475.0F}
            Dim ipModifiedCanLiftOtherTbl As New PdfPTable(2)
            ipModifiedCanLiftOtherTbl.TotalWidth = 650.0F
            ipModifiedCanLiftOtherTbl.SetWidths(ipModifiedCanLiftOtherWidths)
            'ipModifiedDutyWidths

            ipModifiedDutyUpperTbl.AddCell(getCell(" ", 2, True))
            ipModifiedDutyUpperTbl.AddCell(getCell(" ", 0, False))
            ipModifiedDutyUpperTbl.AddCell(getCell(" ", 2, True))
            ipModifiedDutyUpperTbl.AddCell(getCell(" ", 0, False))

            Dim ipModifiedAGHWidths As Single() = New Single() {500.0F}
            Dim ipModifiedAGHTbl As New PdfPTable(1)
            ipModifiedAGHTbl.TotalWidth = 500.0F
            ipModifiedAGHTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipModifiedAGHTbl.SetWidths(ipModifiedAGHWidths)

            If empPartyID.Value = 222397 Then
                ipModifiedAGHTbl.AddCell(getCell("Please contact Barbara Shealey at 412-359-4522 regarding work restrictions.", 2, True))
                ipModifiedAGHTbl.AddCell(getCell(" ", 2, True))
                doc.Add(ipModifiedAGHTbl)
            End If

            If srCanLiftDrp.SelectedValue <> 0 Then
                If srCanLiftDrp.SelectedValue <> 7 Then
                    ipModifiedCanLiftOtherTbl.AddCell(getCell("Can Lift:", 2, True))
                    ipModifiedCanLiftOtherTbl.AddCell(getCell(srCanLiftDrp.SelectedItem.Text.ToUpper, 0, False))
                Else
                    ipModifiedCanLiftOtherTbl.AddCell(getCell("Can Lift:", 2, True))
                    ipModifiedCanLiftOtherTbl.AddCell(getCell(srCanLiftOtherTxt.Text.ToUpper, 0, False))
                End If
            End If

            'canlifycarry
            If srCanCarryDrp.SelectedValue <> 0 Then
                If srCanCarryDrp.SelectedValue <> 7 Then
                    ipModifiedCanLiftOtherTbl.AddCell(getCell("Can Carry:", 2, True))
                    ipModifiedCanLiftOtherTbl.AddCell(getCell(srCanCarryDrp.SelectedItem.Text.ToUpper, 0, False))
                Else
                    ipModifiedCanLiftOtherTbl.AddCell(getCell("Can Carry:", 2, True))
                    ipModifiedCanLiftOtherTbl.AddCell(getCell(srCanCarryOtherTxt.Text.ToUpper, 0, False))
                End If
            End If

            If srCanLiftDrp.SelectedValue <> 0 Or srCanCarryDrp.SelectedValue <> 0 Then
                doc.Add(ipModifiedCanLiftOtherTbl)
            End If

            If srSimpleGraspingDrp.SelectedValue <> 0 Or srRepeatLFootWorkDrp.SelectedValue <> 0 Or srRepeatRFootWorkDrp.SelectedValue <> 0 Or srPushingPullingDrp.SelectedValue <> 0 Or srFineManipulationDrp.SelectedValue <> 0 Then

                If srSimpleGraspingDrp.SelectedValue = 5 Or srRepeatLFootWorkDrp.SelectedValue = 3 Or srRepeatRFootWorkDrp.SelectedValue = 3 Or srPushingPullingDrp.SelectedValue = 5 Or srFineManipulationDrp.SelectedValue = 5 Then

                    ipModifiedDutyOtherInnerTbl.AddCell(getCell("Patient can use hands repetively:", 2, True))
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell("Patient can use feet repetively:", 2, True))
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 0, False))


                    If srSimpleGraspingDrp.SelectedValue <> 5 Then
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Simple Grasping: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srSimpleGraspingDrp.SelectedItem.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Simple Grasping: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srSimpleGraspingOtherTxt.Text.ToUpper, 0, False))
                    End If

                    If srRepeatLFootWorkDrp.SelectedValue <> 3 Then
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(" Left Foot: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srRepeatLFootWorkDrp.SelectedItem.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(" Left Foot: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srRepeatLFootWorkOtherTxt.Text.ToUpper, 0, False))
                    End If

                    If srPushingPullingDrp.SelectedValue <> 5 Then
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Pushing/Pulling: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srPushingPullingDrp.SelectedItem.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Pushing/Pulling: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srPushingPullingOtherTxt.Text.ToUpper, 0, False))
                    End If

                    If srRepeatRFootWorkDrp.SelectedValue <> 3 Then
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(" Right Foot: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srRepeatRFootWorkDrp.SelectedItem.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(" Right Foot: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srRepeatRFootWorkOtherTxt.Text.ToUpper, 0, False))
                    End If

                    If srFineManipulationDrp.SelectedValue <> 5 Then
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Fine Manipulation: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srFineManipulationDrp.SelectedItem.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Fine Manipulation: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srFineManipulationOtherTxt.Text.ToUpper, 0, False))
                    End If


                    ipModifiedDutyOtherInnerTbl.AddCell(getCell("  ", 2, False))
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 0, False))

                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 0, False))

                    doc.Add(ipModifiedDutyOtherInnerTbl)
                Else
                    ipModifiedDutyInnerTbl.AddCell(getCell("Patient can use hands repetively:", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyInnerTbl.AddCell(getCell("Patient can use feet repetively:", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 0, False))

                    ipModifiedDutyInnerTbl.AddCell(getCell("Simple Grasping: ", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(srSimpleGraspingDrp.SelectedItem.Text.ToUpper, 0, False))
                    ipModifiedDutyInnerTbl.AddCell(getCell(" Left Foot: ", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(srRepeatLFootWorkDrp.SelectedItem.Text.ToUpper, 0, False))
                    ipModifiedDutyInnerTbl.AddCell(getCell("Pushing/Pulling: ", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(srPushingPullingDrp.SelectedItem.Text.ToUpper, 0, False))
                    ipModifiedDutyInnerTbl.AddCell(getCell("Right Foot: ", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(srRepeatRFootWorkDrp.SelectedItem.Text.ToUpper, 0, False))
                    ipModifiedDutyInnerTbl.AddCell(getCell("Fine Manipulation: ", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(srFineManipulationDrp.SelectedItem.Text.ToUpper, 0, False))
                    ipModifiedDutyInnerTbl.AddCell(getCell("  ", 2, False))
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 0, False))

                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 0, False))

                    doc.Add(ipModifiedDutyInnerTbl)
                End If

            End If

            If srCanSitPerDayDrp.SelectedValue <> -1 Or srCanStandPerDayDrp.SelectedValue <> -1 Or srCanDrivePerDayDrp.SelectedValue <> -1 Or srCanWalkPerDayDrp.SelectedValue <> -1 Then
                Dim ipModifiedDutyMiddleTbl As New PdfPTable(4)
                ipModifiedDutyMiddleTbl.TotalWidth = 600.0F
                ipModifiedDutyMiddleTbl.SetWidths(ipModifiedDutyWidths)

                'In a work day patient is able to:
                ipModifiedDutyMiddleTbl.AddCell(getCell("In a work day patient is able to:", 2, True))
                ipModifiedDutyMiddleTbl.AddCell(getCell(" ", 0, False))
                ipModifiedDutyMiddleTbl.AddCell(getCell(" ", 2, True))
                ipModifiedDutyMiddleTbl.AddCell(getCell(" ", 0, False))

                doc.Add(ipModifiedDutyMiddleTbl)

                If srCanSitPerDayDrp.SelectedValue = 13 Or srCanStandPerDayDrp.SelectedValue = 13 Or srCanDrivePerDayDrp.SelectedValue = 13 Or srCanWalkPerDayDrp.SelectedValue = 13 Then
                    Dim ipModifiedDutyInWorkDayOtherWidths As Single() = New Single() {175.0F, 400.0F}
                    Dim ipModifiedDutyInWorkDayOtherTbl As New PdfPTable(2)
                    ipModifiedDutyInWorkDayOtherTbl.TotalWidth = 600.0F
                    ipModifiedDutyInWorkDayOtherTbl.SetWidths(ipModifiedDutyInWorkDayOtherWidths)

                    If srCanSitPerDayDrp.SelectedValue <> -1 And srCanSitPerDayDrp.SelectedValue <> 13 Then
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("      Sit: ", 2, True))
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanSitPerDayDrp.SelectedItem.Text.ToUpper & " hrs/day", 0, False))
                    ElseIf srCanSitPerDayDrp.SelectedValue = 13 Then
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("      Sit: ", 2, True))
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanSitPerDayOtherTxt.Text.ToUpper, 0, False))
                    End If

                    If srCanStandPerDayDrp.SelectedValue <> -1 And srCanStandPerDayDrp.SelectedValue <> 13 Then
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("Stand: ", 2, True))
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanStandPerDayDrp.SelectedItem.Text.ToUpper & " hrs/day", 0, False))
                    ElseIf srCanStandPerDayDrp.SelectedValue = 13 Then
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("Stand: ", 2, True))
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanStandPerDayOtherTxt.Text.ToUpper, 0, False))
                    End If

                    If srCanWalkPerDayDrp.SelectedValue <> -1 And srCanWalkPerDayDrp.SelectedValue <> 13 Then
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("Walk: ", 2, True))
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanWalkPerDayDrp.SelectedItem.Text.ToUpper & " hrs/day", 0, False))
                    ElseIf srCanWalkPerDayDrp.SelectedValue = 13 Then
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("Walk: ", 2, True))
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanWalkPerDayOtherTxt.Text.ToUpper, 0, False))
                    End If

                    If srCanDrivePerDayDrp.SelectedValue <> -1 And srCanDrivePerDayDrp.SelectedValue <> 13 Then
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(" Drive: ", 2, True))
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanDrivePerDayDrp.SelectedItem.Text.ToUpper & " hrs/day", 0, False))
                    ElseIf srCanDrivePerDayDrp.SelectedValue = 13 Then
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(" Drive: ", 2, True))
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanDrivePerDayOtherTxt.Text.ToUpper, 0, False))
                    End If

                    ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(" ", 0, False))

                    doc.Add(ipModifiedDutyInWorkDayOtherTbl)
                Else
                    Dim ipModifiedDutyInWorkDayWidths As Single() = New Single() {175.0F, 175.0F, 225.0F}
                    Dim ipModifiedDutyInWorkDayTbl As New PdfPTable(3)
                    ipModifiedDutyInWorkDayTbl.TotalWidth = 600.0F
                    ipModifiedDutyInWorkDayTbl.SetWidths(ipModifiedDutyInWorkDayWidths)

                    If srCanSitPerDayDrp.SelectedValue <> -1 Then
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell("      Sit: ", 2, True))
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(srCanSitPerDayDrp.SelectedItem.Text.ToUpper & " hrs/day", 0, False))
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                        'ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                    End If

                    If srCanStandPerDayDrp.SelectedValue <> -1 Then
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell("Stand: ", 2, True))
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(srCanStandPerDayDrp.SelectedItem.Text.ToUpper & " hrs/day", 0, False))
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                        'ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                    End If

                    If srCanWalkPerDayDrp.SelectedValue <> -1 Then
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell("Walk: ", 2, True))
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(srCanWalkPerDayDrp.SelectedItem.Text.ToUpper & " hrs/day", 0, False))
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                        'ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                    End If

                    If srCanDrivePerDayDrp.SelectedValue <> -1 Then
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(" Drive: ", 2, True))
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(srCanDrivePerDayDrp.SelectedItem.Text.ToUpper & " hrs/day", 0, False))
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                        'ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                    End If

                    ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 0, False))

                    doc.Add(ipModifiedDutyInWorkDayTbl)
                End If

            End If

            If srCanBendDrp.SelectedValue <> 0 Or srCanSquatDrp.SelectedValue <> 0 Or srCanClimbStairsDrp.SelectedValue <> 0 Or srCanClimbLadderDrp.SelectedValue <> 0 Or srCanCrawlDrp.SelectedValue <> 0 Or srCanReachDrp.SelectedValue <> 0 Then

                If srCanBendDrp.SelectedValue = 4 Or srCanSquatDrp.SelectedValue = 4 Or srCanClimbStairsDrp.SelectedValue = 4 Or srCanClimbLadderDrp.SelectedValue = 4 Or srCanCrawlDrp.SelectedValue = 4 Or srCanReachDrp.SelectedValue = 4 Then
                    Dim ipModifiedDutyIsAbleToOtherWidths As Single() = New Single() {175.0F, 150.0F, 175.0F, 150.0F}
                    Dim ipModifiedDutyIsAbleToOtherTbl As New PdfPTable(4)
                    ipModifiedDutyIsAbleToOtherTbl.TotalWidth = 650.0F
                    ipModifiedDutyIsAbleToOtherTbl.SetWidths(ipModifiedDutyIsAbleToOtherWidths)

                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Patient is able to:", 2, True))
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 0, False))

                    If srCanBendDrp.SelectedValue = 4 Then
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Bend: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanBendOtherTxt.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Bend: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanBendDrp.SelectedItem.Text.ToUpper, 0, False))
                    End If

                    If srCanSquatDrp.SelectedValue = 4 Then
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Squat: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanSquatOtherTxt.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Squat: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanSquatDrp.SelectedItem.Text.ToUpper, 0, False))
                    End If

                    If srCanClimbStairsDrp.SelectedValue = 4 Then
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Climb Stairs: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanClimbStairsOtherTxt.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Climb Stairs: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanClimbStairsDrp.SelectedItem.Text.ToUpper, 0, False))
                    End If

                    If srCanClimbLadderDrp.SelectedValue = 4 Then
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Climb a ladder: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanClimbLadderOtherTxt.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Climb a ladder: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanClimbLadderDrp.SelectedItem.Text.ToUpper, 0, False))
                    End If

                    If srCanCrawlDrp.SelectedValue = 4 Then
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Crawl: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanCrawlOtherTxt.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Crawl: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanCrawlDrp.SelectedItem.Text.ToUpper, 0, False))
                    End If

                    If srCanReachDrp.SelectedValue = 4 Then
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Reach: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanReachOtherTxt.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Reach: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanReachDrp.SelectedItem.Text.ToUpper, 0, False))
                    End If

                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 0, False))

                    doc.Add(ipModifiedDutyIsAbleToOtherTbl)
                Else
                    Dim ipModifiedDutyIsAbleToWidths As Single() = New Single() {110.0F, 50.0F, 50.0F, 50.0F}
                    Dim ipModifiedDutyIsAbleToTbl As New PdfPTable(4)
                    ipModifiedDutyIsAbleToTbl.TotalWidth = 200.0F
                    ipModifiedDutyIsAbleToTbl.HorizontalAlignment = Element.ALIGN_LEFT
                    ipModifiedDutyIsAbleToTbl.SetWidths(ipModifiedDutyIsAbleToWidths)

                    'Patient is able to:
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Patient is able to:", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 0, False))

                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Bend: ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanBendDrp.SelectedItem.Text.ToUpper, 0, False))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Squat: ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanSquatDrp.SelectedItem.Text.ToUpper, 0, False))

                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Climb Stairs: ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanClimbStairsDrp.SelectedItem.Text.ToUpper, 0, False))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Climb a ladder: ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanClimbLadderDrp.SelectedItem.Text.ToUpper, 0, False))

                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Crawl: ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanCrawlDrp.SelectedItem.Text.ToUpper, 0, False))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Reach: ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanReachDrp.SelectedItem.Text.ToUpper, 0, False))

                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 0, False))

                    doc.Add(ipModifiedDutyIsAbleToTbl)
                End If


            End If

        End If

        If srReferredToSpecialistDrp.SelectedValue <> 0 Then
            Dim ipSpecialistHdrTbl As New PdfPTable(1)
            ipSpecialistHdrTbl.TotalWidth = 600.0F
            ipSpecialistHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipSpecialistHdrTbl.AddCell(getCell(" ", 0, False))
            ipSpecialistHdrTbl.AddCell(getHdrCell("Specialist", 0))

            doc.Add(ipSpecialistHdrTbl)

            Dim ipSpecialistWidths As Single() = New Single() {105.0F, 400.0F}
            Dim ipSpecialistTbl As New PdfPTable(2)
            ipSpecialistTbl.TotalWidth = 505.0F
            ipSpecialistTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipSpecialistTbl.SetWidths(ipSpecialistWidths)
            ipSpecialistTbl.LockedWidth = True

            ipSpecialistTbl.AddCell(getCell(" ", 2, True))
            ipSpecialistTbl.AddCell(getCell(" ", 0, False))
            ipSpecialistTbl.AddCell(getCell("Referred to Specialist:", 2, True))
            ipSpecialistTbl.AddCell(getCell(srReferredToSpecialistDrp.SelectedItem.Text.ToUpper, 0, False))
            If String.IsNullOrEmpty(srTypeOfSpecialistTxt.Text.ToUpper) = False Then
                ipSpecialistTbl.AddCell(getCell("Physician/Practice Name:", 2, True))
                ipSpecialistTbl.AddCell(getCell(srReferredDrNameTxt.Text.ToUpper, 0, False))
                ipSpecialistTbl.AddCell(getCell("Type:", 2, True))
                ipSpecialistTbl.AddCell(getCell(srTypeOfSpecialistTxt.Text.ToUpper, 0, False))
            End If

            doc.Add(ipSpecialistTbl)

            Dim ipSpecialistApptWidths As Single() = New Single() {105.0F, 50.0F, 25.0F, 50.0F}
            Dim ipSpecialistApptTbl As New PdfPTable(4)
            ipSpecialistApptTbl.TotalWidth = 230.0F
            ipSpecialistApptTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipSpecialistApptTbl.SetWidths(ipSpecialistApptWidths)
            ipSpecialistApptTbl.LockedWidth = True

            ipSpecialistApptTbl.AddCell(getCell("Date:", 2, True))
            ipSpecialistApptTbl.AddCell(getCell(srSpecialistApptDateTxt.Text.ToUpper, 0, False))

            If srSpecialistApptAmPmDrp.SelectedValue <> 0 And srSpecialistApptHourDrp.SelectedValue <> 0 And srSpecialistApptMinuteDrp.SelectedValue <> 0 Then
                ipSpecialistApptTbl.AddCell(getCell("Time:", 2, True))
                ipSpecialistApptTbl.AddCell(getCell(srSpecialistApptHourDrp.SelectedItem.Text.ToUpper & ":" & srSpecialistApptMinuteDrp.SelectedItem.Text.ToUpper & " " & srSpecialistApptAmPmDrp.SelectedItem.Text.ToUpper, 0, False))
            Else
                ipSpecialistApptTbl.AddCell(getCell(" ", 2, True))
                ipSpecialistApptTbl.AddCell(getCell(" ", 0, False))
            End If
            doc.Add(ipSpecialistApptTbl)

        End If



        If String.IsNullOrEmpty(srNextAptDateTxt.Text) = False Then
            Dim ipFollowupHdrTbl As New PdfPTable(1)
            ipFollowupHdrTbl.TotalWidth = 600.0F
            ipFollowupHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipFollowupHdrTbl.AddCell(getCell(" ", 0, False))
            ipFollowupHdrTbl.AddCell(getHdrCell("Follow-up Appointment", 0))

            doc.Add(ipFollowupHdrTbl)

            Dim ipFollowupWidths As Single() = New Single() {105.0F, 70.0F, 25.0F, 50.0F}
            Dim ipFollowupTbl As New PdfPTable(4)
            ipFollowupTbl.TotalWidth = 225.0F
            ipFollowupTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipFollowupTbl.SetWidths(ipFollowupWidths)
            ipFollowupTbl.LockedWidth = True

            ipFollowupTbl.AddCell(getCell(" ", 2, True))
            ipFollowupTbl.AddCell(getCell(" ", 0, False))
            ipFollowupTbl.AddCell(getCell(" ", 2, True))
            ipFollowupTbl.AddCell(getCell(" ", 0, False))
            ipFollowupTbl.AddCell(getCell("Date:", 2, True))
            ipFollowupTbl.AddCell(getCell(srNextAptDateTxt.Text.ToUpper, 0, False))

            If srNextAptAmPmDrp.SelectedValue <> 0 And srNextAptHourDrp.SelectedValue <> 0 And srNextAptMinuteDrp.SelectedValue <> 0 Then
                ipFollowupTbl.AddCell(getCell("Time:", 2, True))
                ipFollowupTbl.AddCell(getCell(srNextAptHourDrp.SelectedItem.Text.ToUpper & ":" & srNextAptMinuteDrp.SelectedItem.Text.ToUpper & " " & srNextAptAmPmDrp.SelectedItem.Text.ToUpper, 0, False))
            Else
                ipFollowupTbl.AddCell(getCell(" ", 2, True))
                ipFollowupTbl.AddCell(getCell(" ", 0, False))
            End If
            doc.Add(ipFollowupTbl)
        End If

        Dim ipDoctorsCMTWidths As Single() = New Single() {105.0F, 345.0F, 25.0F, 50.0F}
        Dim ipDoctorsCMTbl As New PdfPTable(4)
        ipDoctorsCMTbl.TotalWidth = 525.0F
        ipDoctorsCMTbl.HorizontalAlignment = Element.ALIGN_LEFT
        ipDoctorsCMTbl.SetWidths(ipDoctorsCMTWidths)
        ipDoctorsCMTbl.LockedWidth = True

        ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))

        ipDoctorsCMTbl.AddCell(getCell("Medical Care Coordinator:", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(srCMTxt.Text.ToUpper, 0, False))
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))

        ipDoctorsCMTbl.AddCell(getCell("Physician:", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(srTreatingPhysicianTxt.Text.ToUpper, 0, False))
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))

        If srClincTypeDrp.SelectedValue = 2 Then
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
            ipDoctorsCMTbl.AddCell(getDoctorSignature(srTreatingPhysicianTxt.Text.ToUpper, 0))
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
            ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))
            getSignatureBlock(srTreatingPhysicianTxt.Text.ToUpper, ipDoctorsCMTbl)
        End If


        ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))

        doc.Add(ipDoctorsCMTbl)

        Dim ipAddressTbl As New PdfPTable(1)
        ipAddressTbl.TotalWidth = 4900
        If srClincTypeDrp.SelectedValue = 1 Then
            ipAddressTbl.AddCell(getCell("WorkWell Inc", 1, True, True))
        Else
            ipAddressTbl.AddCell(getCell("WorkWell Physicians PC", 1, True, True))
        End If

        ipAddressTbl.AddCell(getCell("112 Third Ave - Carnegie, PA 15106", 1, True, True))
        ipAddressTbl.AddCell(getCell("Phone: 800.967.5935 Fax: 412-279-5848", 1, True, True))

        doc.Add(ipAddressTbl)

        'Catch dex As DocumentException

        'Throw (dex)

        'Catch ioex As IOException
        'Throw (ioex)
        'Finally
        'statusReportHL.NavigateUrl = "/PDFs" & sFileName

        doc.Close()
        'Response.Redirect("/PDFs" & sFileName)
        'End Try

        Dim popupScript As String = "<script language=javascript> window.open('/PDFs" & sFileName & "') </script>"
        ClientScript.RegisterStartupScript(Me.[GetType](), "callpopup", popupScript)

    End Sub

    Sub viewit2()

        'Dim fStatusReport As New Document()
        'Dim sPath As String = Server.MapPath("PDFs")
        'PdfWriter.GetInstance(fStatusReport, New FileStream(sPath & "/StatusReport.pdf", FileMode.Create))
        'Dim c1 As New Chunk("Employee Name: " & ipFirstNameTxt.Text)
        'c1.SetUnderline(0.5F, -1.5F)


        'Dim c1 As New Chunk("Employee Name: ")

        'fStatusReport.Add(c1)

        Dim pdfPath As String = Server.MapPath("PDFs")
        Dim imgPath As String = Server.MapPath("images")
        Dim fontpath As String = Server.MapPath("fonts")


        'Dim r As New Rectangle(400, 200)
        Dim doc As New Document(PageSize.LETTER, 50, 30, 10, 10)
        Dim sFileName As String = "/block2" & Replace(Replace(Now.ToString, "/", "_"), ":", "-") & ".pdf"
        Dim sFileName2 As String = "/block2.pdf"
        'Response.Write(sFileName)

        'Try

        PdfWriter.GetInstance(doc, New FileStream(pdfPath & sFileName, FileMode.Create))

        doc.Open()


        Dim customfont As BaseFont = BaseFont.CreateFont(fontpath & "/calibri.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)

        Dim font As New Font(customfont, 6)

        Dim ipEmpInfoTbl As New PdfPTable(4)
        Dim ipAGHInfoTbl As New PdfPTable(2)
        Dim ipAGHWidths As Single() = New Single() {125.0F, 200.0F}
        ipAGHInfoTbl.TotalWidth = 325.0F
        ipAGHInfoTbl.HorizontalAlignment = Element.ALIGN_LEFT
        ipAGHInfoTbl.SetWidths(ipAGHWidths)
        ipAGHInfoTbl.LockedWidth = True



        'Dim hdrCell As New PdfPCell(New Phrase("WorkWell  Physicians, PC"))
        'Dim empInfoHdrCell As New PdfPCell(Image.GetInstance(imgPath + "/tiny-meww-header-transport-small copy.png"))
        'Dim empInfoHdrCell As New PdfPCell(Image.GetInstance(imgPath + "/workwell_inc.png"))

        If srClincTypeDrp.SelectedValue = 1 Then
            Dim ipCompNameWidths As Single() = New Single() {315.0F, 45.0F}
            Dim ipCompNameHdrTbl As New PdfPTable(2)
            ipCompNameHdrTbl.TotalWidth = 360.0F
            ipCompNameHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipCompNameHdrTbl.AddCell(getCompanyNameCell(0, 0))
            ipCompNameHdrTbl.AddCell(getCompanyNameCell(1, 0))
            ipCompNameHdrTbl.SetWidths(ipCompNameWidths)
            'ipCompNameHdrTbl.AddCell(ipIncLogoCell)
            ipCompNameHdrTbl.LockedWidth = True
            doc.Add(ipCompNameHdrTbl)
        Else
            Dim ipCompNameWidths As Single() = New Single() {245.0F, 450.0F}
            Dim ipCompNameHdrTbl As New PdfPTable(2)
            ipCompNameHdrTbl.TotalWidth = 695.0F
            ipCompNameHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipCompNameHdrTbl.AddCell(getCompanyNameCell(0, 0))
            ipCompNameHdrTbl.AddCell(getCompanyNameCell(2, 0))
            ipCompNameHdrTbl.SetWidths(ipCompNameWidths)
            'ipCompNameHdrTbl.AddCell(ipIncLogoCell)
            ipCompNameHdrTbl.LockedWidth = True
            doc.Add(ipCompNameHdrTbl)
        End If




        'Dim ipEmpWidths As Single() = New Single() {75.0F, 200.0F, 75.0F, 250.0F}


        Dim cell As New PdfPCell(New Phrase("Employee Name:", font))
        Dim empNameHdr As New Phrase("Employer Name:", font)
        Dim ssClaimNumHdr As New Phrase("SS#/Claim#:", font)
        Dim ipDOEHdr As New Phrase("Date of Exam:", font)
        Dim ipDOIHdr As New Phrase("Date of Injury:", font)
        Dim ipDrNameHdr As New Phrase("Physician Name:", font)

        'empInfoHdrCell.Border = 0
        cell.Border = 0
        'Dim cell As New PdfPCell(New Phrase("Employee Name:", font))

        'empInfoHdrCell.Colspan = 4
        'empInfoHdrCell.HorizontalAlignment = 1
        'ipEmpInfoTbl.AddCell(empInfoHdrCell)
        ipEmpInfoTbl.TotalWidth = 500.0F
        'ipEmpInfoTbl.SetWidths(ipEmpWidths)

        ipEmpInfoTbl.LockedWidth = True
        ipEmpInfoTbl.AddCell(getCell(" ", 2, True))
        ipEmpInfoTbl.AddCell(getCell(" ", 0, False))
        ipEmpInfoTbl.AddCell(getCell(" ", 2, True))
        ipEmpInfoTbl.AddCell(getCell(" ", 0, False))
        ipEmpInfoTbl.AddCell(getCell("Employee Name:", 2, True))
        ipEmpInfoTbl.AddCell(getCell(srIPFNameTxt.Text.ToUpper & " " & srIPMNameTxt.Text.ToUpper & " " & srIPLNameTxt.Text.ToUpper, 0, False))
        ipEmpInfoTbl.AddCell(getCell("Date of Exam:", 2, True))
        ipEmpInfoTbl.AddCell(getCell(srIPDOETxt.Text.ToUpper, 0, False))
        ipEmpInfoTbl.AddCell(getCell("Employer Name:", 2, True))
        ipEmpInfoTbl.AddCell(getCell(srIPEmployerNameTxt.Text.ToUpper, 0, False))
        ipEmpInfoTbl.AddCell(getCell("Date of Injury:", 2, True))
        ipEmpInfoTbl.AddCell(getCell(srIPDOITxt.Text.ToUpper, 0, False))
        ipEmpInfoTbl.AddCell(getCell("Social Security #:", 2, True))
        If String.IsNullOrEmpty(srSocialSecurityNumTxt.Text) = False Then
            If srSocialSecurityNumTxt.Text.Replace("-", "").Length >= 4 Then
                ipEmpInfoTbl.AddCell(getCell("XXX-XX-" & srSocialSecurityNumTxt.Text.Replace("-", "").Substring(srSocialSecurityNumTxt.Text.Replace("-", "").Length - 4, 4), 0, False))
            Else
                ipEmpInfoTbl.AddCell(getCell("XXX-XX-XXXX", 0, False))
            End If

        Else
            ipEmpInfoTbl.AddCell(getCell("XXX-XX-XXXX", 0, False))
        End If

        ipEmpInfoTbl.AddCell(getCell("Claim #:", 2, True))

        If StrComp(srSocialSecurityNumTxt.Text.Trim, srIPSSNumClaimNumTxt.Text.Trim) = 0 Then
            ipEmpInfoTbl.AddCell(getCell("N/A", 0, False))
        Else
            ipEmpInfoTbl.AddCell(getCell(srIPSSNumClaimNumTxt.Text.ToUpper, 0, False))
        End If

        ipEmpInfoTbl.AddCell(getCell("Date of Birth:", 2, True))
        ipEmpInfoTbl.AddCell(getCell(srIPDOBTxt.Text.ToUpper, 0, False))
        ipEmpInfoTbl.AddCell(getCell(" ", 2, True))
        ipEmpInfoTbl.AddCell(getCell(" ", 0, False))

        ipEmpInfoTbl.AddCell(getCell("Clinic Type:", 2, True))
        ipEmpInfoTbl.AddCell(getCell(srClincTypeDrp.SelectedItem.Text.ToUpper, 0, False))
        ipEmpInfoTbl.AddCell(getCell(" ", 2, True))
        ipEmpInfoTbl.AddCell(getCell(" ", 0, False))

        ipEmpInfoTbl.AddCell(getCell("Practice Name:", 2, True))
        ipEmpInfoTbl.AddCell(getCell(srClinicNameTxt.Text.ToUpper, 0, False))
        ipEmpInfoTbl.AddCell(getCell(" ", 2, True))
        ipEmpInfoTbl.AddCell(getCell(" ", 0, False))

        doc.Add(ipEmpInfoTbl)
        If empPartyID.Value = 222397 Then
            ipAGHInfoTbl.AddCell(getCell(" ", 2, True))
            ipAGHInfoTbl.AddCell(getCell(" ", 0, False))
            ipAGHInfoTbl.AddCell(getCell("Time In:", 2, True))
            ipAGHInfoTbl.AddCell(getCell(srTimeInTxt.Text, 0, False))
            ipAGHInfoTbl.AddCell(getCell("Time Out:", 2, True))
            ipAGHInfoTbl.AddCell(getCell(srTimeOutTxt.Text, 0, False))

            doc.Add(ipAGHInfoTbl)
        End If


        'Dim ipIncLogoCell As New PdfPCell(Image.GetInstance(imgPath + "/logo_inc.png"))


        Dim ipDiagnosisWidths As Single() = New Single() {105.0F, 350.0F}
        Dim ipDiagHdrTbl As New PdfPTable(1)
        ipDiagHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
        ipDiagHdrTbl.TotalWidth = 600.0F
        ipDiagHdrTbl.AddCell(getHdrCell("Diagnosis", 0))
        doc.Add(ipDiagHdrTbl)

        Dim ipDiagnosisTbl As New PdfPTable(2)

        'Dim diagnosisHdrCell As New PdfPCell(New Phrase("Diagnosis"))
        'diagnosisHdrCell.Border = 0

        'diagnosisHdrCell.Colspan = 2
        'diagnosisHdrCell.HorizontalAlignment = 0
        'ipDiagnosisTbl.AddCell(diagnosisHdrCell)
        ipDiagnosisTbl.TotalWidth = 455.0F
        ipDiagnosisTbl.SetWidths(ipDiagnosisWidths)
        ipDiagnosisTbl.HorizontalAlignment = Element.ALIGN_LEFT
        ipDiagnosisTbl.LockedWidth = True

        ipDiagnosisTbl.LockedWidth = True

        ipDiagnosisTbl.AddCell(getCell(" ", 2, True))
        ipDiagnosisTbl.AddCell(getCell(" ", 0, False))

        ipDiagnosisTbl.AddCell(getCell("Body Part:", 2, True))
        ipDiagnosisTbl.AddCell(getCell(srBodyPartTxt.Text.ToUpper, 0, False))

        If String.IsNullOrEmpty(srDiagonsisTxt.Text.ToUpper) = False Then
            ipDiagnosisTbl.AddCell(getCell("Diagnosis:", 2, True))
            ipDiagnosisTbl.AddCell(getCell(srDiagonsisTxt.Text.ToUpper, 0, False))
        End If

        If String.IsNullOrEmpty(srObjectiveFindingsTxt.Text) = False Then
            ipDiagnosisTbl.AddCell(getCell("Objective Findings:", 2, True))
            ipDiagnosisTbl.AddCell(getCell(srObjectiveFindingsTxt.Text.ToUpper, 0, False))
        End If

        If String.IsNullOrEmpty(srMechanismOfInjuryTxt.Text) = False Then
            ipDiagnosisTbl.AddCell(getCell("Mechanism of Injury:", 2, True))
            ipDiagnosisTbl.AddCell(getCell(srMechanismOfInjuryTxt.Text.ToUpper, 0, False))
        End If

        doc.Add(ipDiagnosisTbl)




        If srDiagEMGNCVChk.Checked = True Or srDiagMRIChk.Checked = True Or srDiagXRayChk.Checked = True Or srDiagOtherChk.Checked = True Then

            Dim ipDXWidths As Single() = New Single() {105.0F, 350.0F}
            Dim ipDXHdrTbl As New PdfPTable(1)
            ipDXHdrTbl.TotalWidth = 455.0F
            ipDXHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipDXHdrTbl.AddCell(getCell(" ", 0, False))
            ipDXHdrTbl.AddCell(getHdrCell("Diagnostics", 0))

            doc.Add(ipDXHdrTbl)

            Dim ipDXTbl As New PdfPTable(2)
            ipDXTbl.TotalWidth = 455.0F
            ipDXTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipDXTbl.SetWidths(ipDXWidths)
            ipDXTbl.LockedWidth = True

            ipDXTbl.AddCell(getCell(" ", 2, True))
            ipDXTbl.AddCell(getCell(" ", 0, False))
            ipDXTbl.AddCell(getCell("Diagnostics Needed:", 2, True))
            ipDXTbl.AddCell(getCell(GetChkBoxNeeded(1), 0, False))

            If String.IsNullOrEmpty(srDiagBodyPartTxt.Text) = False Then
                ipDXTbl.AddCell(getCell("Body Part:", 2, True))
                ipDXTbl.AddCell(getCell(srDiagBodyPartTxt.Text.ToUpper, 0, False))
            End If

            If String.IsNullOrEmpty(srReasonTxt.Text) = False Then
                ipDXTbl.AddCell(getCell("Reason:", 2, True))
                ipDXTbl.AddCell(getCell(srReasonTxt.Text.ToUpper, 0, False))
            End If

            doc.Add(ipDXTbl)

        End If
        'ipDXTbl.AddCell(getCell(srDXTypeDrp.SelectedItem.Text, 0, False))        

        'End If
        If srPTChk.Checked = True Or srOTChk.Checked Or srWHChk.Checked = True Or srVistTypeDrp.SelectedValue <> 0 Or srTimesPerWeekDrp.SelectedValue <> 0 Or String.IsNullOrEmpty(srTherapyWeeksTxt.Text) = False Then

            Dim ipThrpyWidths As Single() = New Single() {105.0F, 400.0F}
            Dim ipThrpyHdrTbl As New PdfPTable(1)
            ipThrpyHdrTbl.TotalWidth = 405.0F
            ipThrpyHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipThrpyHdrTbl.AddCell(getCell(" ", 0, False))
            ipThrpyHdrTbl.AddCell(getHdrCell("Therapy", 0))

            doc.Add(ipThrpyHdrTbl)

            Dim ipTherapyTbl As New PdfPTable(2)
            ipTherapyTbl.TotalWidth = 500.0F
            ipTherapyTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipTherapyTbl.SetWidths(ipThrpyWidths)
            ipTherapyTbl.LockedWidth = True

            ipTherapyTbl.AddCell(getCell(" ", 2, True))
            ipTherapyTbl.AddCell(getCell(" ", 0, False))
            ipTherapyTbl.AddCell(getCell("Therapy Needed:", 2, True))
            ipTherapyTbl.AddCell(getCell(GetChkBoxNeeded(2), 0, False))
            'ipTherapyTbl.AddCell(getCell(srTherapyTypeDrp.SelectedItem.Text, 0, False))
            ipTherapyTbl.AddCell(getCell("Visit Type:", 2, True))
            ipTherapyTbl.AddCell(getCell(srVistTypeDrp.SelectedItem.Text.ToUpper, 0, False))
            ipTherapyTbl.AddCell(getCell("Times Per Week:", 2, True))
            ipTherapyTbl.AddCell(getCell(srTimesPerWeekDrp.SelectedItem.Text.ToUpper, 0, False))
            ipTherapyTbl.AddCell(getCell("Weeks Needed:", 2, True))
            ipTherapyTbl.AddCell(getCell(srTherapyWeeksTxt.Text.ToUpper, 0, False))

            doc.Add(ipTherapyTbl)

        End If

        If String.IsNullOrEmpty(srMedicationTxt.Text) = False Or String.IsNullOrEmpty(srInstructionsTxt.Text) = False Or String.IsNullOrEmpty(srOtherTxt.Text) = False Then

            Dim ipTreatmentWidths As Single() = New Single() {105.0F, 350.0F}
            Dim ipTreatmentHdrTbl As New PdfPTable(1)
            ipTreatmentHdrTbl.TotalWidth = 600.0F
            ipTreatmentHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipTreatmentHdrTbl.AddCell(getCell(" ", 0, False))
            ipTreatmentHdrTbl.AddCell(getHdrCell("Treatment", 0))

            doc.Add(ipTreatmentHdrTbl)


            Dim ipTreatmentTbl As New PdfPTable(2)
            ipTreatmentTbl.TotalWidth = 455.0F
            ipTreatmentTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipTreatmentTbl.SetWidths(ipTreatmentWidths)
            ipTreatmentTbl.LockedWidth = True

            ipTreatmentTbl.AddCell(getCell(" ", 2, True))
            ipTreatmentTbl.AddCell(getCell(" ", 0, False))
            If String.IsNullOrEmpty(srMedicationTxt.Text) = False Then
                ipTreatmentTbl.AddCell(getCell("Medication:", 2, True))
                ipTreatmentTbl.AddCell(getCell(srMedicationTxt.Text.ToUpper, 0, False))
            End If
            If String.IsNullOrEmpty(srInstructionsTxt.Text) = False Then
                ipTreatmentTbl.AddCell(getCell("Instructions:", 2, True))
                ipTreatmentTbl.AddCell(getCell(srInstructionsTxt.Text.ToUpper, 0, False))
            End If
            If String.IsNullOrEmpty(srOtherTxt.Text) = False Then
                ipTreatmentTbl.AddCell(getCell("Other:", 2, True))
                ipTreatmentTbl.AddCell(getCell(srOtherTxt.Text.ToUpper, 0, False))
            End If
            doc.Add(ipTreatmentTbl)
        End If

        Dim ipWorkStatusHdrTbl As New PdfPTable(1)
        ipWorkStatusHdrTbl.TotalWidth = 600.0F
        ipWorkStatusHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
        ipWorkStatusHdrTbl.AddCell(getCell(" ", 0, False))
        ipWorkStatusHdrTbl.AddCell(getHdrCell("Work Status", 0))

        doc.Add(ipWorkStatusHdrTbl)

        Dim ipWorkStatusTbl As New PdfPTable(6)
        'Dim ipWorkStatusWidths As Single() = New Single() {75.0F, 40.0F, 50.0F, 50.0F, 50.0F, 50.0F}
        Dim ipWorkStatusWidths As Single() = New Single() {115.0F, 100.0F, 40.0F, 70.0F, 30.0F, 70.0F}
        ipWorkStatusTbl.TotalWidth = 425.0F
        ipWorkStatusTbl.HorizontalAlignment = Element.ALIGN_LEFT
        ipWorkStatusTbl.SetWidths(ipWorkStatusWidths)
        ipWorkStatusTbl.LockedWidth = True

        ipWorkStatusTbl.AddCell(getCell(" ", 2, True))
        ipWorkStatusTbl.AddCell(getCell(" ", 0, False))
        ipWorkStatusTbl.AddCell(getCell(" ", 2, True))
        ipWorkStatusTbl.AddCell(getCell(" ", 0, False))
        ipWorkStatusTbl.AddCell(getCell(" ", 0, True))
        ipWorkStatusTbl.AddCell(getCell(" ", 0, False))
        ipWorkStatusTbl.AddCell(getCell("Work Status:", 2, True))
        ipWorkStatusTbl.AddCell(getCell(srWorkStatusDrp.SelectedItem.Text.ToUpper, 0, False))
        ipWorkStatusTbl.AddCell(getCell("Effective: ", 2, True))
        If IsDate(srEffectiveDateTxt.Text) = True Then
            ipWorkStatusTbl.AddCell(getCell(ConvertDate(srEffectiveDateTxt.Text.ToUpper), 0, False))
        Else
            ipWorkStatusTbl.AddCell(getCell(srEffectiveDateTxt.Text.ToUpper, 0, False))
        End If

        If String.IsNullOrEmpty(srUntilDateTxt.Text) = False Then
            ipWorkStatusTbl.AddCell(getCell("Until: ", 2, True))
            If IsDate(srUntilDateTxt.Text) = True Then
                ipWorkStatusTbl.AddCell(getCell(ConvertDate(srUntilDateTxt.Text.ToUpper), 0, False))
            Else
                ipWorkStatusTbl.AddCell(getCell(srUntilDateTxt.Text.ToUpper, 0, False))
            End If
        Else
            ipWorkStatusTbl.AddCell(getCell(" ", 2, True))
            ipWorkStatusTbl.AddCell(getCell(" ", 0, False))
        End If

        doc.Add(ipWorkStatusTbl)

        If String.IsNullOrEmpty(srOtherRestrictionsTxt.Text) = False Then
            Dim ipModifiedDutyCommentsTbl As New PdfPTable(2)
            Dim ipModifiedDutyCommentWidths As Single() = New Single() {115.0F, 350.0F}
            'Dim diagnosisHdrCell As New PdfPCell(New Phrase("Diagnosis"))
            'diagnosisHdrCell.Border = 0

            'diagnosisHdrCell.Colspan = 2
            'diagnosisHdrCell.HorizontalAlignment = 0
            'ipDiagnosisTbl.AddCell(diagnosisHdrCell)
            ipModifiedDutyCommentsTbl.TotalWidth = 465.0F
            ipModifiedDutyCommentsTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipModifiedDutyCommentsTbl.SetWidths(ipModifiedDutyCommentWidths)
            ipModifiedDutyCommentsTbl.LockedWidth = True

            'ipModifiedDutyCommentsTbl.LockedWidth = True
            ipModifiedDutyCommentsTbl.AddCell(getCell("Comments/Restrictions:", 2, True))
            ipModifiedDutyCommentsTbl.AddCell(getCell(srOtherRestrictionsTxt.Text.ToUpper, 0, False))
            'Response.Write(srUseRightFootRdo.SelectedValue) srSimpleGraspingDrp

            doc.Add(ipModifiedDutyCommentsTbl)
        End If

        If srWorkStatusDrp.SelectedValue = 3 Then
            Dim ipModifiedDutyWidths As Single() = New Single() {175.0F, 125.0F, 175.0F, 125.0F}
            Dim ipModifiedDutyHdrTbl As New PdfPTable(1)
            ipModifiedDutyHdrTbl.TotalWidth = 600.0F
            ipModifiedDutyHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipModifiedDutyHdrTbl.AddCell(getCell(" ", 0, False))
            ipModifiedDutyHdrTbl.AddCell(getHdrCell("Modified Duty Restrictions", 0))

            doc.Add(ipModifiedDutyHdrTbl)

            Dim ipModifiedDutyUpperWidths As Single() = New Single() {175.0F, 100.0F, 155.0F, 95.0F}
            Dim ipModifiedDutyUpperTbl As New PdfPTable(4)
            ipModifiedDutyUpperTbl.TotalWidth = 488.0F
            ipModifiedDutyUpperTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipModifiedDutyUpperTbl.SetWidths(ipModifiedDutyUpperWidths)

            Dim ipModifiedDutyInnerWidths As Single() = New Single() {175.0F, 100.0F, 155.0F, 95.0F}
            Dim ipModifiedDutyInnerTbl As New PdfPTable(4)
            ipModifiedDutyInnerTbl.TotalWidth = 550.0F
            ipModifiedDutyInnerTbl.SetWidths(ipModifiedDutyInnerWidths)

            Dim ipModifiedDutyOtherInnerWidths As Single() = New Single() {175.0F, 150.0F, 175.0F, 150.0F}
            Dim ipModifiedDutyOtherInnerTbl As New PdfPTable(4)
            ipModifiedDutyOtherInnerTbl.TotalWidth = 650.0F
            ipModifiedDutyOtherInnerTbl.SetWidths(ipModifiedDutyOtherInnerWidths)

            Dim ipModifiedCanLiftOtherWidths As Single() = New Single() {175.0F, 475.0F}
            Dim ipModifiedCanLiftOtherTbl As New PdfPTable(2)
            ipModifiedCanLiftOtherTbl.TotalWidth = 650.0F
            ipModifiedCanLiftOtherTbl.SetWidths(ipModifiedCanLiftOtherWidths)
            'ipModifiedDutyWidths

            ipModifiedDutyUpperTbl.AddCell(getCell(" ", 2, True))
            ipModifiedDutyUpperTbl.AddCell(getCell(" ", 0, False))
            ipModifiedDutyUpperTbl.AddCell(getCell(" ", 2, True))
            ipModifiedDutyUpperTbl.AddCell(getCell(" ", 0, False))

            Dim ipModifiedAGHWidths As Single() = New Single() {500.0F}
            Dim ipModifiedAGHTbl As New PdfPTable(1)
            ipModifiedAGHTbl.TotalWidth = 500.0F
            ipModifiedAGHTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipModifiedAGHTbl.SetWidths(ipModifiedAGHWidths)

            If empPartyID.Value = 222397 Then
                ipModifiedAGHTbl.AddCell(getCell("Please contact Barbara Shealey at 412-359-4522 regarding work restrictions.", 2, True))
                ipModifiedAGHTbl.AddCell(getCell(" ", 2, True))
                doc.Add(ipModifiedAGHTbl)
            End If

            If srCanLiftDrp.SelectedValue <> 0 Then
                If srCanLiftDrp.SelectedValue <> 7 Then
                    ipModifiedCanLiftOtherTbl.AddCell(getCell("Can Lift:", 2, True))
                    ipModifiedCanLiftOtherTbl.AddCell(getCell(srCanLiftDrp.SelectedItem.Text.ToUpper, 0, False))
                Else
                    ipModifiedCanLiftOtherTbl.AddCell(getCell("Can Lift:", 2, True))
                    ipModifiedCanLiftOtherTbl.AddCell(getCell(srCanLiftOtherTxt.Text.ToUpper, 0, False))
                End If
            End If

            'canlifycarry
            If srCanCarryDrp.SelectedValue <> 0 Then
                If srCanCarryDrp.SelectedValue <> 7 Then
                    ipModifiedCanLiftOtherTbl.AddCell(getCell("Can Carry:", 2, True))
                    ipModifiedCanLiftOtherTbl.AddCell(getCell(srCanCarryDrp.SelectedItem.Text.ToUpper, 0, False))
                Else
                    ipModifiedCanLiftOtherTbl.AddCell(getCell("Can Carry:", 2, True))
                    ipModifiedCanLiftOtherTbl.AddCell(getCell(srCanCarryOtherTxt.Text.ToUpper, 0, False))
                End If
            End If

            If srCanLiftDrp.SelectedValue <> 0 Or srCanCarryDrp.SelectedValue <> 0 Then
                doc.Add(ipModifiedCanLiftOtherTbl)
            End If

            If srSimpleGraspingDrp.SelectedValue <> 0 Or srRepeatLFootWorkDrp.SelectedValue <> 0 Or srRepeatRFootWorkDrp.SelectedValue <> 0 Or srPushingPullingDrp.SelectedValue <> 0 Or srFineManipulationDrp.SelectedValue <> 0 Then

                If srSimpleGraspingDrp.SelectedValue = 5 Or srRepeatLFootWorkDrp.SelectedValue = 3 Or srRepeatRFootWorkDrp.SelectedValue = 3 Or srPushingPullingDrp.SelectedValue = 5 Or srFineManipulationDrp.SelectedValue = 5 Then

                    ipModifiedDutyOtherInnerTbl.AddCell(getCell("Patient can use hands repetively:", 2, True))
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell("Patient can use feet repetively:", 2, True))
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 0, False))


                    If srSimpleGraspingDrp.SelectedValue <> 5 Then
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Simple Grasping: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srSimpleGraspingDrp.SelectedItem.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Simple Grasping: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srSimpleGraspingOtherTxt.Text.ToUpper, 0, False))
                    End If

                    If srRepeatLFootWorkDrp.SelectedValue <> 3 Then
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(" Left Foot: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srRepeatLFootWorkDrp.SelectedItem.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(" Left Foot: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srRepeatLFootWorkOtherTxt.Text.ToUpper, 0, False))
                    End If

                    If srPushingPullingDrp.SelectedValue <> 5 Then
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Pushing/Pulling: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srPushingPullingDrp.SelectedItem.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Pushing/Pulling: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srPushingPullingOtherTxt.Text.ToUpper, 0, False))
                    End If

                    If srRepeatRFootWorkDrp.SelectedValue <> 3 Then
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(" Right Foot: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srRepeatRFootWorkDrp.SelectedItem.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(" Right Foot: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srRepeatRFootWorkOtherTxt.Text.ToUpper, 0, False))
                    End If

                    If srFineManipulationDrp.SelectedValue <> 5 Then
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Fine Manipulation: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srFineManipulationDrp.SelectedItem.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Fine Manipulation: ", 2, True))
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srFineManipulationOtherTxt.Text.ToUpper, 0, False))
                    End If


                    ipModifiedDutyOtherInnerTbl.AddCell(getCell("  ", 2, False))
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 0, False))

                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 0, False))

                    doc.Add(ipModifiedDutyOtherInnerTbl)
                Else
                    ipModifiedDutyInnerTbl.AddCell(getCell("Patient can use hands repetively:", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyInnerTbl.AddCell(getCell("Patient can use feet repetively:", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 0, False))

                    ipModifiedDutyInnerTbl.AddCell(getCell("Simple Grasping: ", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(srSimpleGraspingDrp.SelectedItem.Text.ToUpper, 0, False))
                    ipModifiedDutyInnerTbl.AddCell(getCell(" Left Foot: ", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(srRepeatLFootWorkDrp.SelectedItem.Text.ToUpper, 0, False))
                    ipModifiedDutyInnerTbl.AddCell(getCell("Pushing/Pulling: ", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(srPushingPullingDrp.SelectedItem.Text.ToUpper, 0, False))
                    ipModifiedDutyInnerTbl.AddCell(getCell("Right Foot: ", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(srRepeatRFootWorkDrp.SelectedItem.Text.ToUpper, 0, False))
                    ipModifiedDutyInnerTbl.AddCell(getCell("Fine Manipulation: ", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(srFineManipulationDrp.SelectedItem.Text.ToUpper, 0, False))
                    ipModifiedDutyInnerTbl.AddCell(getCell("  ", 2, False))
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 0, False))

                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 0, False))

                    doc.Add(ipModifiedDutyInnerTbl)
                End If

            End If

            If srCanSitPerDayDrp.SelectedValue <> -1 Or srCanStandPerDayDrp.SelectedValue <> -1 Or srCanDrivePerDayDrp.SelectedValue <> -1 Or srCanWalkPerDayDrp.SelectedValue <> -1 Then
                Dim ipModifiedDutyMiddleTbl As New PdfPTable(4)
                ipModifiedDutyMiddleTbl.TotalWidth = 600.0F
                ipModifiedDutyMiddleTbl.SetWidths(ipModifiedDutyWidths)

                'In a work day patient is able to:
                ipModifiedDutyMiddleTbl.AddCell(getCell("In a work day patient is able to:", 2, True))
                ipModifiedDutyMiddleTbl.AddCell(getCell(" ", 0, False))
                ipModifiedDutyMiddleTbl.AddCell(getCell(" ", 2, True))
                ipModifiedDutyMiddleTbl.AddCell(getCell(" ", 0, False))

                doc.Add(ipModifiedDutyMiddleTbl)

                If srCanSitPerDayDrp.SelectedValue = 13 Or srCanStandPerDayDrp.SelectedValue = 13 Or srCanDrivePerDayDrp.SelectedValue = 13 Or srCanWalkPerDayDrp.SelectedValue = 13 Then
                    Dim ipModifiedDutyInWorkDayOtherWidths As Single() = New Single() {175.0F, 400.0F}
                    Dim ipModifiedDutyInWorkDayOtherTbl As New PdfPTable(2)
                    ipModifiedDutyInWorkDayOtherTbl.TotalWidth = 600.0F
                    ipModifiedDutyInWorkDayOtherTbl.SetWidths(ipModifiedDutyInWorkDayOtherWidths)

                    If srCanSitPerDayDrp.SelectedValue <> -1 And srCanSitPerDayDrp.SelectedValue <> 13 Then
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("      Sit: ", 2, True))
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanSitPerDayDrp.SelectedItem.Text.ToUpper & " hrs/day", 0, False))
                    ElseIf srCanSitPerDayDrp.SelectedValue = 13 Then
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("      Sit: ", 2, True))
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanSitPerDayOtherTxt.Text.ToUpper, 0, False))
                    End If

                    If srCanStandPerDayDrp.SelectedValue <> -1 And srCanStandPerDayDrp.SelectedValue <> 13 Then
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("Stand: ", 2, True))
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanStandPerDayDrp.SelectedItem.Text.ToUpper & " hrs/day", 0, False))
                    ElseIf srCanStandPerDayDrp.SelectedValue = 13 Then
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("Stand: ", 2, True))
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanStandPerDayOtherTxt.Text.ToUpper, 0, False))
                    End If

                    If srCanWalkPerDayDrp.SelectedValue <> -1 And srCanWalkPerDayDrp.SelectedValue <> 13 Then
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("Walk: ", 2, True))
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanWalkPerDayDrp.SelectedItem.Text.ToUpper & " hrs/day", 0, False))
                    ElseIf srCanWalkPerDayDrp.SelectedValue = 13 Then
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("Walk: ", 2, True))
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanWalkPerDayOtherTxt.Text.ToUpper, 0, False))
                    End If

                    If srCanDrivePerDayDrp.SelectedValue <> -1 And srCanDrivePerDayDrp.SelectedValue <> 13 Then
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(" Drive: ", 2, True))
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanDrivePerDayDrp.SelectedItem.Text.ToUpper & " hrs/day", 0, False))
                    ElseIf srCanDrivePerDayDrp.SelectedValue = 13 Then
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(" Drive: ", 2, True))
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanDrivePerDayOtherTxt.Text.ToUpper, 0, False))
                    End If

                    ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(" ", 0, False))

                    doc.Add(ipModifiedDutyInWorkDayOtherTbl)
                Else
                    Dim ipModifiedDutyInWorkDayWidths As Single() = New Single() {175.0F, 175.0F, 225.0F}
                    Dim ipModifiedDutyInWorkDayTbl As New PdfPTable(3)
                    ipModifiedDutyInWorkDayTbl.TotalWidth = 600.0F
                    ipModifiedDutyInWorkDayTbl.SetWidths(ipModifiedDutyInWorkDayWidths)

                    If srCanSitPerDayDrp.SelectedValue <> -1 Then
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell("      Sit: ", 2, True))
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(srCanSitPerDayDrp.SelectedItem.Text.ToUpper & " hrs/day", 0, False))
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                        'ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                    End If

                    If srCanStandPerDayDrp.SelectedValue <> -1 Then
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell("Stand: ", 2, True))
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(srCanStandPerDayDrp.SelectedItem.Text.ToUpper & " hrs/day", 0, False))
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                        'ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                    End If

                    If srCanWalkPerDayDrp.SelectedValue <> -1 Then
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell("Walk: ", 2, True))
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(srCanWalkPerDayDrp.SelectedItem.Text.ToUpper & " hrs/day", 0, False))
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                        'ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                    End If

                    If srCanDrivePerDayDrp.SelectedValue <> -1 Then
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(" Drive: ", 2, True))
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(srCanDrivePerDayDrp.SelectedItem.Text.ToUpper & " hrs/day", 0, False))
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                        'ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                    End If

                    ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 0, False))

                    doc.Add(ipModifiedDutyInWorkDayTbl)
                End If

            End If

            If srCanBendDrp.SelectedValue <> 0 Or srCanSquatDrp.SelectedValue <> 0 Or srCanClimbStairsDrp.SelectedValue <> 0 Or srCanClimbLadderDrp.SelectedValue <> 0 Or srCanCrawlDrp.SelectedValue <> 0 Or srCanReachDrp.SelectedValue <> 0 Then

                If srCanBendDrp.SelectedValue = 4 Or srCanSquatDrp.SelectedValue = 4 Or srCanClimbStairsDrp.SelectedValue = 4 Or srCanClimbLadderDrp.SelectedValue = 4 Or srCanCrawlDrp.SelectedValue = 4 Or srCanReachDrp.SelectedValue = 4 Then
                    Dim ipModifiedDutyIsAbleToOtherWidths As Single() = New Single() {175.0F, 150.0F, 175.0F, 150.0F}
                    Dim ipModifiedDutyIsAbleToOtherTbl As New PdfPTable(4)
                    ipModifiedDutyIsAbleToOtherTbl.TotalWidth = 650.0F
                    ipModifiedDutyIsAbleToOtherTbl.SetWidths(ipModifiedDutyIsAbleToOtherWidths)

                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Patient is able to:", 2, True))
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 0, False))

                    If srCanBendDrp.SelectedValue = 4 Then
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Bend: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanBendOtherTxt.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Bend: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanBendDrp.SelectedItem.Text.ToUpper, 0, False))
                    End If

                    If srCanSquatDrp.SelectedValue = 4 Then
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Squat: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanSquatOtherTxt.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Squat: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanSquatDrp.SelectedItem.Text.ToUpper, 0, False))
                    End If

                    If srCanClimbStairsDrp.SelectedValue = 4 Then
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Climb Stairs: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanClimbStairsOtherTxt.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Climb Stairs: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanClimbStairsDrp.SelectedItem.Text.ToUpper, 0, False))
                    End If

                    If srCanClimbLadderDrp.SelectedValue = 4 Then
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Climb a ladder: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanClimbLadderOtherTxt.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Climb a ladder: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanClimbLadderDrp.SelectedItem.Text.ToUpper, 0, False))
                    End If

                    If srCanCrawlDrp.SelectedValue = 4 Then
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Crawl: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanCrawlOtherTxt.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Crawl: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanCrawlDrp.SelectedItem.Text.ToUpper, 0, False))
                    End If

                    If srCanReachDrp.SelectedValue = 4 Then
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Reach: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanReachOtherTxt.Text.ToUpper, 0, False))
                    Else
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Reach: ", 2, True))
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanReachDrp.SelectedItem.Text.ToUpper, 0, False))
                    End If

                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 0, False))

                    doc.Add(ipModifiedDutyIsAbleToOtherTbl)
                Else
                    Dim ipModifiedDutyIsAbleToWidths As Single() = New Single() {110.0F, 50.0F, 50.0F, 50.0F}
                    Dim ipModifiedDutyIsAbleToTbl As New PdfPTable(4)
                    ipModifiedDutyIsAbleToTbl.TotalWidth = 200.0F
                    ipModifiedDutyIsAbleToTbl.HorizontalAlignment = Element.ALIGN_LEFT
                    ipModifiedDutyIsAbleToTbl.SetWidths(ipModifiedDutyIsAbleToWidths)

                    'Patient is able to:
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Patient is able to:", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 0, False))

                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Bend: ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanBendDrp.SelectedItem.Text.ToUpper, 0, False))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Squat: ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanSquatDrp.SelectedItem.Text.ToUpper, 0, False))

                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Climb Stairs: ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanClimbStairsDrp.SelectedItem.Text.ToUpper, 0, False))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Climb a ladder: ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanClimbLadderDrp.SelectedItem.Text.ToUpper, 0, False))

                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Crawl: ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanCrawlDrp.SelectedItem.Text.ToUpper, 0, False))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Reach: ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanReachDrp.SelectedItem.Text.ToUpper, 0, False))

                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 0, False))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 2, True))
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 0, False))

                    doc.Add(ipModifiedDutyIsAbleToTbl)
                End If


            End If

        End If

        'If srReferredToSpecialistDrp.SelectedValue <> 0 Then
        Dim ipSpecialistHdrTbl As New PdfPTable(1)
        ipSpecialistHdrTbl.TotalWidth = 600.0F
        ipSpecialistHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
        ipSpecialistHdrTbl.AddCell(getCell(" ", 0, False))
        ipSpecialistHdrTbl.AddCell(getHdrCell("Specialist", 0))

        doc.Add(ipSpecialistHdrTbl)

        Dim ipSpecialistWidths As Single() = New Single() {105.0F, 400.0F}
        Dim ipSpecialistTbl As New PdfPTable(2)
        ipSpecialistTbl.TotalWidth = 505.0F
        ipSpecialistTbl.HorizontalAlignment = Element.ALIGN_LEFT
        ipSpecialistTbl.SetWidths(ipSpecialistWidths)
        ipSpecialistTbl.LockedWidth = True

        ipSpecialistTbl.AddCell(getCell(" ", 2, True))
        ipSpecialistTbl.AddCell(getCell(" ", 0, False))
        ipSpecialistTbl.AddCell(getCell("Referred to Specialist:", 2, True))
        ipSpecialistTbl.AddCell(getCell(srReferredToSpecialistDrp.SelectedItem.Text.ToUpper, 0, False))
        If String.IsNullOrEmpty(srTypeOfSpecialistTxt.Text.ToUpper) = False Then
            ipSpecialistTbl.AddCell(getCell("Physician/Practice Name:", 2, True))
            ipSpecialistTbl.AddCell(getCell(srReferredDrNameTxt.Text.ToUpper, 0, False))
            ipSpecialistTbl.AddCell(getCell("Type:", 2, True))
            ipSpecialistTbl.AddCell(getCell(srTypeOfSpecialistTxt.Text.ToUpper, 0, False))
        End If

        doc.Add(ipSpecialistTbl)
        'End If

        If String.IsNullOrEmpty(srNextAptDateTxt.Text) = False Then
            Dim ipFollowupHdrTbl As New PdfPTable(1)
            ipFollowupHdrTbl.TotalWidth = 600.0F
            ipFollowupHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipFollowupHdrTbl.AddCell(getCell(" ", 0, False))
            ipFollowupHdrTbl.AddCell(getHdrCell("Follow-up Appointment", 0))

            doc.Add(ipFollowupHdrTbl)

            Dim ipFollowupWidths As Single() = New Single() {105.0F, 70.0F, 25.0F, 50.0F}
            Dim ipFollowupTbl As New PdfPTable(4)
            ipFollowupTbl.TotalWidth = 225.0F
            ipFollowupTbl.HorizontalAlignment = Element.ALIGN_LEFT
            ipFollowupTbl.SetWidths(ipFollowupWidths)
            ipFollowupTbl.LockedWidth = True

            ipFollowupTbl.AddCell(getCell(" ", 2, True))
            ipFollowupTbl.AddCell(getCell(" ", 0, False))
            ipFollowupTbl.AddCell(getCell(" ", 2, True))
            ipFollowupTbl.AddCell(getCell(" ", 0, False))
            ipFollowupTbl.AddCell(getCell("Date:", 2, True))
            ipFollowupTbl.AddCell(getCell(srNextAptDateTxt.Text.ToUpper, 0, False))

            If srNextAptAmPmDrp.SelectedValue <> 0 And srNextAptHourDrp.SelectedValue <> 0 And srNextAptMinuteDrp.SelectedValue <> 0 Then
                ipFollowupTbl.AddCell(getCell("Time:", 2, True))
                ipFollowupTbl.AddCell(getCell(srNextAptHourDrp.SelectedItem.Text.ToUpper & ":" & srNextAptMinuteDrp.SelectedItem.Text.ToUpper & " " & srNextAptAmPmDrp.SelectedItem.Text.ToUpper, 0, False))
            Else
                ipFollowupTbl.AddCell(getCell(" ", 2, True))
                ipFollowupTbl.AddCell(getCell(" ", 0, False))
            End If
            doc.Add(ipFollowupTbl)
        End If

        Dim ipDoctorsCMTWidths As Single() = New Single() {105.0F, 345.0F, 25.0F, 50.0F}
        Dim ipDoctorsCMTbl As New PdfPTable(4)
        ipDoctorsCMTbl.TotalWidth = 525.0F
        ipDoctorsCMTbl.HorizontalAlignment = Element.ALIGN_LEFT
        ipDoctorsCMTbl.SetWidths(ipDoctorsCMTWidths)
        ipDoctorsCMTbl.LockedWidth = True

        ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))

        ipDoctorsCMTbl.AddCell(getCell("Case Manager:", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(srCMTxt.Text.ToUpper, 0, False))
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))

        ipDoctorsCMTbl.AddCell(getCell("Physician:", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(srTreatingPhysicianTxt.Text.ToUpper, 0, False))
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))

        If srClincTypeDrp.SelectedValue = 2 Then
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
            ipDoctorsCMTbl.AddCell(getDoctorSignature(srTreatingPhysicianTxt.Text.ToUpper, 0))
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
            ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))
            getSignatureBlock(srTreatingPhysicianTxt.Text.ToUpper, ipDoctorsCMTbl)
        End If


        ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))

        doc.Add(ipDoctorsCMTbl)

        Dim ipAddressTbl As New PdfPTable(1)
        ipAddressTbl.TotalWidth = 4900
        If srClincTypeDrp.SelectedValue = 1 Then
            ipAddressTbl.AddCell(getCell("WorkWell Inc", 1, True, True))
        Else
            ipAddressTbl.AddCell(getCell("WorkWell Physicians PC", 1, True, True))
        End If

        ipAddressTbl.AddCell(getCell("112 Third Ave - Carnegie, PA 15106", 1, True, True))
        ipAddressTbl.AddCell(getCell("Phone: 800-967-5935 Fax: 412-279-5848", 1, True, True))

        doc.Add(ipAddressTbl)

        'Catch dex As DocumentException

        'Throw (dex)

        'Catch ioex As IOException
        'Throw (ioex)
        'Finally
        'statusReportHL.NavigateUrl = "/PDFs" & sFileName

        doc.Close()
        'Response.Redirect("/PDFs" & sFileName)

        Dim popupScript As String = "<script language=javascript> window.open('/PDFs" & sFileName & "') </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "callpopup", popupScript)

        'End Try

    End Sub

    Function getCell(ByVal sText As String, ByVal iAlignment As Integer, ByVal bBold As Boolean) As PdfPCell
        '0=Left, 1=Centre, 2=Right


        Dim fontpath As String = Server.MapPath("fonts")
        Dim customfont As BaseFont = BaseFont.CreateFont(fontpath & "/calibri.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)

        Dim font As New Font

        If bBold Then
            font = New Font(customfont, 8, font.BOLD)
        Else
            font = New Font(customfont, 8)
        End If


        Dim cell As New PdfPCell(New Phrase(sText, font))
        cell.Border = 0
        cell.HorizontalAlignment = iAlignment

        Return cell

    End Function

    Sub getSignatureBlock(ByVal srTreatingPhysician As String, ByRef ipDoctorsCMTbl As PdfPTable)

        If srTreatingPhysician.ToUpper.Contains("PRINCE") Then
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
            ipDoctorsCMTbl.AddCell(getCell("Lester O. Prince, M.D.", 0, False))
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
            ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))
        ElseIf srTreatingPhysician.ToUpper.Contains("KATZ") Then
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
            ipDoctorsCMTbl.AddCell(getCell("Richard H. Katz, M.D.", 0, False))
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
            ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))
        ElseIf srTreatingPhysician.ToUpper.Contains("SHEPPARD") Then
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
            ipDoctorsCMTbl.AddCell(getCell("Scott F. Sheppard, M.D.", 0, False))
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
            ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))
        ElseIf srTreatingPhysician.ToUpper.Contains("METCALF") Then
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
            ipDoctorsCMTbl.AddCell(getCell("John W. Metcalf, M.D.", 0, False))
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
            ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))
        End If

    End Sub

    Function getCompanyNameCell(ByVal iWhichCompany As Integer, ByVal iAlignment As Integer) As PdfPCell
        '0=Left, 1=Centre, 2=Right


        Dim fontpath As String = Server.MapPath("fonts")
        Dim customfont As BaseFont = BaseFont.CreateFont(fontpath & "/WarnockPro-Bold.otf", BaseFont.CP1252, BaseFont.EMBEDDED)

        Dim font As New Font
        Dim p As New Phrase()
        If iWhichCompany = 0 Then
            font = New Font(customfont, 24)
            Dim c1 As New Chunk("W", font)
            font = New Font(customfont, 20)
            Dim c2 As New Chunk("ORK", font)
            font = New Font(customfont, 24)
            Dim c3 As New Chunk("W", font)
            font = New Font(customfont, 20)
            Dim c4 As New Chunk("ELL", font)

            p.Add(c1)
            p.Add(c2)
            p.Add(c3)
            p.Add(c4)
        ElseIf iWhichCompany = 1 Then
            font = New Font(customfont, 9)
            Dim c1 As New Chunk("INC", font)

            p.Add(c1)
        Else
            font = New Font(customfont, 24)
            Dim c1 As New Chunk("P", font)
            font = New Font(customfont, 20)
            Dim c2 As New Chunk("HYSICIANS PC", font)

            p.Add(c1)
            p.Add(c2)
        End If

        Dim cell As New PdfPCell(p)
        cell.Border = 0
        If iWhichCompany = 0 Then
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.VerticalAlignment = Element.ALIGN_BASELINE
        ElseIf iWhichCompany = 1 Then
            cell.Rotation = 90
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.VerticalAlignment = Element.ALIGN_BASELINE
        Else
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.VerticalAlignment = Element.ALIGN_BOTTOM
        End If
        'cell.HorizontalAlignment = iAlignment

        Return cell

    End Function

    Function getDoctorSignature(ByVal sDoctor As String, ByVal iAlignment As Integer) As PdfPCell
        '0=Left, 1=Centre, 2=Right

        If sDoctor.ToUpper.Contains("PRINCE") Then
            Dim fontpath As String = Server.MapPath("fonts")
            Dim customfont As BaseFont = BaseFont.CreateFont(fontpath & "/LESTPE__.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim font As New Font(customfont, 24, font.NORMAL)

            Dim sChunk As New Chunk("L P", font)
            'Dim sChunk As New Chunk("_L", font)
            sChunk.SetUnderline(0.5F, -1.5F)

            Dim sPhrase As New Phrase()
            sPhrase.Add(sChunk)


            Dim cell As New PdfPCell(sPhrase)
            cell.Border = 0
            cell.HorizontalAlignment = iAlignment

            Return cell
        ElseIf sDoctor.ToUpper.Contains("METCALF") Then
            Dim fontpath As String = Server.MapPath("fonts")
            Dim customfont As BaseFont = BaseFont.CreateFont(fontpath & "/JOHNWME_.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim font As New Font(customfont, 24, font.NORMAL)

            Dim sChunk As New Chunk("J M", font)
            'Dim sChunk As New Chunk("_J ", font)
            sChunk.SetUnderline(0.5F, -1.5F)

            Dim sPhrase As New Phrase()
            sPhrase.Add(sChunk)


            Dim cell As New PdfPCell(sPhrase)
            cell.Border = 0
            cell.HorizontalAlignment = iAlignment

            Return cell
        ElseIf sDoctor.ToUpper.Contains("SHEPPARD") Then

            Dim fontpath As String = Server.MapPath("fonts")
            Dim customfont As BaseFont = BaseFont.CreateFont(fontpath & "/SCOTSE__.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim font As New Font(customfont, 24, font.NORMAL)

            Dim sChunk As New Chunk("S", font)
            sChunk.SetUnderline(0.5F, -1.5F)

            Dim sPhrase As New Phrase()
            sPhrase.Add(sChunk)


            Dim cell As New PdfPCell(sPhrase)
            cell.Border = 0
            cell.HorizontalAlignment = iAlignment

            Return cell
        ElseIf sDoctor.ToUpper.Contains("KATZ") Then
            Dim fontpath As String = Server.MapPath("fonts")
            Dim customfont As BaseFont = BaseFont.CreateFont(fontpath & "/RICHKE__.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim font As New Font(customfont, 24, font.NORMAL)

            Dim sChunk As New Chunk("R K", font)
            sChunk.SetUnderline(0.5F, -1.5F)

            Dim sPhrase As New Phrase()
            sPhrase.Add(sChunk)


            Dim cell As New PdfPCell(sPhrase)
            cell.Border = 0
            cell.HorizontalAlignment = iAlignment

            Return cell
        ElseIf sDoctor.ToUpper.Contains("WILSON") Then
            Dim fontpath As String = Server.MapPath("fonts")
            Dim customfont As BaseFont = BaseFont.CreateFont(fontpath & "/MWilson2.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim font As New Font(customfont, 24, font.NORMAL)

            Dim sChunk As New Chunk("GW", font)
            'sChunk.SetUnderline(0.5F, -1.5F)

            Dim sPhrase As New Phrase()
            sPhrase.Add(sChunk)


            Dim cell As New PdfPCell(sPhrase)
            cell.Border = 0
            cell.HorizontalAlignment = iAlignment

            Return cell
        ElseIf sDoctor.ToUpper.Contains("BLOOM") Then
            Dim fontpath As String = Server.MapPath("fonts")
            Dim customfont As BaseFont = BaseFont.CreateFont(fontpath & "/ValBloom2.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim font As New Font(customfont, 24, font.NORMAL)

            Dim sChunk As New Chunk("V", font)
            'sChunk.SetUnderline(0.5F, -1.5F)

            Dim sPhrase As New Phrase()
            sPhrase.Add(sChunk)


            Dim cell As New PdfPCell(sPhrase)
            cell.Border = 0
            cell.HorizontalAlignment = iAlignment

            Return cell
        ElseIf sDoctor.ToUpper.Contains("AMANT") Then
            Dim fontpath As String = Server.MapPath("fonts")
            Dim customfont As BaseFont = BaseFont.CreateFont(fontpath & "/tstamant2.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim font As New Font(customfont, 24, font.NORMAL)

            Dim sChunk As New Chunk("T", font)
            'sChunk.SetUnderline(0.5F, -1.5F)

            Dim sPhrase As New Phrase()
            sPhrase.Add(sChunk)


            Dim cell As New PdfPCell(sPhrase)
            cell.Border = 0
            cell.HorizontalAlignment = iAlignment

            Return cell
        Else
            Dim fontpath As String = Server.MapPath("fonts")
            Dim customfont As BaseFont = BaseFont.CreateFont(fontpath & "/calibri.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim font As New Font(customfont, 10, font.BOLD)

            Dim sChunk As New Chunk(" ", font)
            'sChunk.SetUnderline(0.5F, -1.5F)

            Dim sPhrase As New Phrase()
            sPhrase.Add(sChunk)


            Dim cell As New PdfPCell(sPhrase)
            cell.Border = 0
            cell.HorizontalAlignment = iAlignment

            Return cell
        End If

    End Function

    Function getCell(ByVal sText As String, ByVal iAlignment As Integer, ByVal bBold As Boolean, ByVal iCellPadding As Boolean) As PdfPCell
        '0=Left, 1=Centre, 2=Right


        Dim fontpath As String = Server.MapPath("fonts")
        Dim customfont As BaseFont = BaseFont.CreateFont(fontpath & "/calibri.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim font As New Font

        If bBold Then
            font = New Font(customfont, 8, font.BOLD)
        Else
            font = New Font(customfont, 8)
        End If



        Dim cell As New PdfPCell(New Phrase(sText, font))

        cell.Border = 0

        If iCellPadding = True Then
            cell.PaddingBottom = 0.01
            cell.PaddingTop = 0.01
        End If

        cell.HorizontalAlignment = iAlignment

        Return cell

    End Function

    Function getHdrCell(ByVal sText As String, ByVal iAlignment As Integer) As PdfPCell
        '0=Left, 1=Centre, 2=Right


        Dim fontpath As String = Server.MapPath("fonts")
        Dim customfont As BaseFont = BaseFont.CreateFont(fontpath & "/calibri.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim font As New Font(customfont, 10, font.BOLD)

        Dim sChunk As New Chunk(sText, font)
        sChunk.SetUnderline(0.5F, -1.5F)

        Dim sPhrase As New Phrase()
        sPhrase.Add(sChunk)


        Dim cell As New PdfPCell(sPhrase)
        cell.Border = 0
        cell.HorizontalAlignment = iAlignment

        Return cell

    End Function

    Function GetChkBoxNeeded(ByVal iWhich As Integer) As String

        Dim sChkBxNeeded As String = ""

        If iWhich = 1 Then
            If srDiagXRayChk.Checked = True Then
                sChkBxNeeded = sChkBxNeeded & srDiagXRayChk.Text
            End If

            If srDiagMRIChk.Checked = True Then
                If String.IsNullOrEmpty(sChkBxNeeded) = False Then
                    sChkBxNeeded = sChkBxNeeded & ", " & srDiagMRIChk.Text
                Else
                    sChkBxNeeded = srDiagMRIChk.Text
                End If
            End If
            If srDiagEMGNCVChk.Checked = True Then

                If String.IsNullOrEmpty(sChkBxNeeded) = False Then
                    sChkBxNeeded = sChkBxNeeded & ", " & srDiagEMGNCVChk.Text
                Else
                    sChkBxNeeded = srDiagEMGNCVChk.Text
                End If
            End If
            If srDiagOtherChk.Checked = True Then
                If String.IsNullOrEmpty(sChkBxNeeded) = False Then
                    sChkBxNeeded = sChkBxNeeded & ", " & srDiagOtherTxt.Text
                Else
                    sChkBxNeeded = srDiagOtherTxt.Text
                End If
            End If
        Else
            If srPTChk.Checked = True Then
                sChkBxNeeded = sChkBxNeeded & srPTChk.Text
            End If

            If srOTChk.Checked = True Then
                If String.IsNullOrEmpty(sChkBxNeeded) = False Then
                    sChkBxNeeded = sChkBxNeeded & ", " & srOTChk.Text
                Else
                    sChkBxNeeded = srOTChk.Text
                End If
            End If
            If srWHChk.Checked = True Then
                If String.IsNullOrEmpty(sChkBxNeeded) = False Then
                    sChkBxNeeded = sChkBxNeeded & ", " & srWHChk.Text
                Else
                    sChkBxNeeded = srWHChk.Text
                End If
            End If
            If srDiagOtherChk.Checked = True Then
                If String.IsNullOrEmpty(sChkBxNeeded) = False Then
                    sChkBxNeeded = sChkBxNeeded & ", " & srDiagOtherTxt.Text
                Else
                    sChkBxNeeded = srDiagOtherTxt.Text
                End If
            End If
        End If


        Return sChkBxNeeded

    End Function

    Function CheckTime(ByVal sHours As String, ByVal sMins As String, ByVal sAMPM As String) As String

        Dim sTime As String = ""

        If sHours.Contains("-") Or sMins.Contains("-") Or sAMPM.Contains("-") Then
            sTime = "--"
        Else
            sTime = sHours & ":" & sMins & " " & sAMPM
        End If

        Return sTime

    End Function

    Function CheckDate(ByVal sDate As String) As String

        Dim dDate As String

        If IsDate(sDate) Then
            dDate = sDate
        Else
            dDate = "1/1/1900"
        End If

        Return dDate

    End Function

    Function GetCheckedVal(ByVal bChecked As Boolean) As Integer

        Dim iChecked As Integer = 0

        If bChecked Then
            iChecked = 1
        Else
            iChecked = 0
        End If

        Return iChecked

    End Function

    Sub NewViewSearch()

        Response.Redirect("ei_SearchSR.aspx")

    End Sub
End Class
