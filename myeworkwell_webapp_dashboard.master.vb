﻿
Partial Class webapp_myeworkwell_webapp_dashboard
    Inherits System.Web.UI.MasterPage

    'Public ReturnURl As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Try
                GetUrl(myCStr(Request.UrlReferrer.AbsoluteUri.ToString.Trim))
            Catch
                dashBoardLnk.NavigateUrl = "LandingPage.aspx"
                dashBoardLnk.ImageUrl = "images/tabOnDashboard.jpg"
            End Try
        End If

    End Sub

    Sub GetUrl(ByVal returnUrl As String)
        dashBoardLnk.NavigateUrl = "LandingPage.aspx"
        dashBoardLnk.ImageUrl = "images/tabOnDashboard.jpg"
    End Sub

    Function myCStr(ByVal test As Object) As String

        Dim sReturnStr As String = ""

        If IsDBNull(test) Then
            sReturnStr = ""
        Else
            sReturnStr = CStr(test)
        End If

        If StrComp(sReturnStr, "#") = 0 Then
            sReturnStr = ""
        End If

        myCStr = sReturnStr

    End Function

End Class

