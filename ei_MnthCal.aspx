﻿<%@ Page Title="" Language="C#" MasterPageFile="~/myeworkwell_calendar.master" AutoEventWireup="true" CodeFile="ei_MnthCal.aspx.cs" Inherits="ei_MnthCal"  Debug="true"%>

<%@ MasterType virtualpath="~/myeworkwell_calendar.master" %>
<%@ Register Assembly="DayPilot" Namespace="DayPilot.Web.Ui" TagPrefix="DayPilot" %>
<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    function clear() {
        var filterBox = document.getElementById("TextBoxFilter");
        filterBox.value = '';
        filter();
    }
    function filter() {
        var filterBox = document.getElementById("TextBoxFilter");
        var filterText = filterBox.value;
        dpm.clientState = { "filter": filterText };
        dpm.commandCallBack("filter");
    }

    function key(e) {
        var keynum = (window.event) ? event.keyCode : e.keyCode;

        if (keynum === 13) {
            filter();
            return false;
        }

        return true;
    }

    function ask(e) {

        // it's a normal event
        if (!e.recurrent()) {
            edit(e);
            return;
        }

        // it's a recurrent event but it's an exception from the series
        if (e.value() !== null) {
            edit(e);
            return;
        }

        var modal = new DayPilot.Modal();
        modal.top = 150;
        modal.width = 300;
        modal.height = 150
        modal.border = "10px solid #d0d0d0";
        modal.closed = function () {
            if (this.result != "cancel") {
                edit(e, this.result);
            }
        };

        modal.showUrl("RecurrentEditMode.html");
    }

    function edit(e, mode) {

        var url = "ww_ViewCaseHistory.aspx?q=1";

        if (e.recurrentMasterId() !== null) {
            url += "&master=" + e.recurrentMasterId();
        }
        if (e.value() !== null) {
            url += "&id=" + e.value();
        }
        if (mode == "this") {
            url += "&start=" + e.start().toStringSortable();
        }
        url += "&i=" + e.value();
        window.location = url;

    }

    function showAdvSrch() {

        document.getElementById('ctl00_ContentPlaceHolder1_monthlySchedulePanel').style.display = 'none';

    }


    </script>
    <asp:hiddenfield id="ip_PartyID" runat="server" Value="0"/>
    <asp:hiddenfield id="ip_custAcctID" runat="server" Value="0"/>

    <asp:Panel ID="Panel1" runat="server" Visible="false">
        <center>
            <asp:Label runat="server" ID="maintenance_msg" Visible="true" Text="Sorry for the inconvenience, this page is undergoing maintenance at this time.  It will return shortly.<br><br> -WorkWell Staff"></asp:Label>
        </center>
    </asp:Panel>

    <telerik:RadAjaxPanel ID="select_an_acct_pnl" runat="server" Visible="false">
        <h1>Please select an account..</h1>  
        <asp:Table ID="blank_tbl" runat="server">
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
        </asp:Table>           
    </telerik:RadAjaxPanel>

    <asp:Panel ID="monthlySchedulePanel" runat="server" Visible="True">
        <asp:Label ID="notification" runat="server"></asp:Label>
        <asp:Table ID="searchFunctionTbl" runat="server" Width="480">
            <asp:TableRow>
                <asp:TableCell Width="150">&nbsp;</asp:TableCell>
                <asp:TableCell Font-Names="Calibri" Font-Size="Medium" Width="110"><b>Schedule Type:</b></asp:TableCell>
                <asp:TableCell  Width="100"><asp:DropDownList id="schedTypeDrp" AutoPostBack="false"
                                    runat="server" Font-Names="Calibri" Font-Size="12px" OnSelectedIndexChanged="getSchedType" Visible ="true">  
                                    <asp:ListItem Value="0">Injuries</asp:ListItem>                                                   
                                    <asp:ListItem Value="1">Physical Therapy</asp:ListItem>                                       
                                    <asp:ListItem Value="3">Surgeries</asp:ListItem> 
                                    <asp:ListItem Value="4">MRIs</asp:ListItem>             
                                    <asp:ListItem Value="2" Selected="True">All</asp:ListItem>                                       
                                </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell Wrap="false" HorizontalAlign="Left" Visible="true" ID="TableCell3" Width="40"><asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="../Media/layout/btn/appBtnIPSearch.jpg" CssClass="smallappBtnGrd" OnClick="ipSrchSubmitBtn_Click"/></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <table style="width:100%">
            <tr>
                <td valign="top" style="width:150px">
                    <asp:Table ID="legend" runat="server" Width="142" BorderColor="gray" GridLines="Both">
                        <asp:TableHeaderRow>
                            <asp:TableCell CssClass="month_silver_header" HorizontalAlign="Center" Font-Bold="true">Calendar legend</asp:TableCell>
                        </asp:TableHeaderRow>                        
                        <asp:TableRow>                            
                            <asp:TableCell Font-Names="Calibri" Font-Size="Small">Physician Appt</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell BackColor="LightSteelBlue" Font-Names="Calibri" Font-Size="Small">P.T. Appt</asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell BackColor="LightGreen" Font-Names="Calibri" Font-Size="Small">Surgery</asp:TableCell>                            
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell BackColor="LightSalmon" Font-Names="Calibri" Font-Size="Small">MRI Appt</asp:TableCell>                            
                        </asp:TableRow>
                    </asp:Table>
                    <br />
                    <DayPilot:DayPilotNavigator ID="DayPilotNavigator1" runat="server" 
                        BoundDayPilotID="DayPilotMonth1" 
                        SelectMode="Month"
                        ShowMonths="3"
                        SkipMonths="3"
                        CssClassPrefix="navigator_silver_"
                        DataStartField="start"
                        DataEndField="end" 
                        VisibleRangeChangedHandling="PostBack"
                        OnVisibleRangeChanged="DayPilotNavigator1_VisibleRangeChanged"
                         
                        ></DayPilot:DayPilotNavigator>
                    <br />
                    <br />
                    <div style="height:20px"></div>        
                </td>
                <td valign="top"> 
                    <DayPilot:DayPilotMonth 
                       ID="DayPilotMonth1" 
                        runat="server" 
                        DataEndField="end" 
                        DataStartField="start"
                        DataTextField="name" 
                        DataValueField="id" 
                        DataTagFields="name, id" 
                        DataRecurrenceField="recurrence"
                        EventClickHandling="PostBack"
                        EventClickJavaScript="DayPilotMonth1_EventClick" 
                        ContextMenuID="DayPilotMenu1" 
                        OnEventMenuClick="DayPilotCalendar1_EventMenuClick"
                        ClientObjectName="dpm"
                        Width="100%"  
                    
                        OnBeforeEventRender="DayPilotMonth1_BeforeEventRender" 
                        BubbleID="DayPilotBubble1" 
                        ShowToolTip="false" 
                        EventStartTime="false" 
                        EventEndTime="false" 
                        OnCommand="DayPilotMonth1_Command" 
                    
                        OnBeforeCellRender="DayPilotMonth1_BeforeCellRender" 
                        OnEventClick="DayPilotMonth1_EventClick" 
                        HeaderClickHandling="JavaScript"
                        ContextMenuSelectionID="DayPilotMenuSelection"
                                    
                        RecurrentEventImage="~/Media/recur10x9.png"
                        RecurrentEventExceptionImage="~/Media/recurex10x9.png"
                        EventTextAlignment="Center"
                        CssClassPrefix="month_silver_"

                        >
                    </DayPilot:DayPilotMonth>
                </td>
            </tr>
            <tr>
                <td>
                    
                </td>                
            </tr>
        </table>

        <DayPilot:DayPilotMenu ID="DayPilotMenu1" runat="server" CssClassPrefix="menu_">
            <DayPilot:MenuItem Text="Open" Action="JavaScript" JavaScript="alert('Opening event (id ' + e.value() + ')');">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Text="Send" Action="JavaScript" JavaScript="alert('Sending event (id ' + e.value() + ')');">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Text="-" Action="NavigateUrl"></DayPilot:MenuItem>
            <DayPilot:MenuItem Text="Delete (CallBack)" Action="Callback" Command="Delete"></DayPilot:MenuItem>
            <DayPilot:MenuItem Action="PostBack" Command="Delete" Text="Delete (PostBack)" />
            <DayPilot:MenuItem Action="NavigateUrl" NavigateUrl="javascript:alert('Going somewhere else (id {0})');"
                Text="NavigateUrl test" />
        </DayPilot:DayPilotMenu>

        <DayPilot:DayPilotMenu ID="DayPilotMenu2" runat="server" ClientObjectName="menu2"  CssClassPrefix="menu_">
            <DayPilot:MenuItem Text="Open" Action="JavaScript" JavaScript="alert('Opening event (id ' + e.value() + ')');">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Text="Send" Action="JavaScript" JavaScript="alert('Sending event (id ' + e.value() + ')');">
            </DayPilot:MenuItem>
            <DayPilot:MenuItem Text="-" Action="NavigateUrl"></DayPilot:MenuItem>
            <DayPilot:MenuItem Text="Delete (CallBack)" Action="Callback" Command="Delete"></DayPilot:MenuItem>
            <DayPilot:MenuItem Action="PostBack" Command="Delete" Text="Delete (PostBack)" />
            <DayPilot:MenuItem Action="NavigateUrl" NavigateUrl="javascript:alert('Going somewhere else (id {0})');"
                Text="NavigateUrl test" />
        </DayPilot:DayPilotMenu>
    
    
        <DayPilot:DayPilotMenu ID="DayPilotMenuSelection" runat="server" CssClassPrefix="menu_">
            <DayPilot:MenuItem Action="JavaScript" JavaScript="dpm.timeRangeSelectedCallBack(e.start, e.end);"
                Text="Create new event (JavaScript)" />
            <DayPilot:MenuItem Action="PostBack" Command="Insert" Text="Create new event (PostBack)" />
            <DayPilot:MenuItem Action="CallBack" Command="Insert" Text="Create new event (CallBack)" />
            <DayPilot:MenuItem Text="-" ></DayPilot:MenuItem>
            <DayPilot:MenuItem Action="JavaScript" JavaScript="alert('Start: ' + e.start.toString() + '\nEnd: ' + e.end.toString())"
                Text="Show selection details" />
        </DayPilot:DayPilotMenu>
    
        <DayPilot:DayPilotBubble 
            ID="DayPilotBubble1" 
            runat="server" 
            OnRenderContent="DayPilotBubble1_RenderContent" 
            ClientObjectName="bubble"
            UseShadow="True"
            Width="0"
      
            >
        </DayPilot:DayPilotBubble>    
    
        <asp:Label ID="errors" runat="server"></asp:Label>
    </asp:Panel>

    <asp:Panel ID="viewCasePanel" runat="server" Visible="False">

    <center>
        <asp:hiddenfield id="cl" runat="server" Value="0"/>
        <asp:hiddenfield id="st" runat="server" Value="0" />

            <ajaxToolkit:ToolkitScriptManager runat="Server" EnablePartialRendering="true" ID="ww_ClicnicSchedSM"/>
            <br />
            <ajaxToolkit:TabContainer runat="server" ID="injuryDetailsTC" Height="803px" ActiveTabIndex="0" Width="746" CssClass="visoft__tab_xpie7"> 
            
            <ajaxToolkit:TabPanel ID="ipSelectedApptInfoTP" runat="server" >
                <ContentTemplate> 
                <br />                   
                <span style="text-align:left;">
                    <h1>APPOINTMENT DETAILS</h1>
                    <div class="divider"></div>
                    <div class="appFormLeft"></div>
                </span>
                     <asp:Table id="ipSelectedApptTbl" runat="server" Width="746px">                        
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="5">&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">First:</asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left"><asp:TextBox CssClass="txtbox" ID="ipFNameTxt" runat="server" Width="160" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Middle:</asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left"><asp:TextBox CssClass="txtbox" ID="ipMNameTxt" runat="server" Width="90" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left">&nbsp;</asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Last:</asp:TableCell>
                            <asp:TableCell ColumnSpan="4"><asp:TextBox CssClass="txtbox" ID="ipLNameTxt" runat="server" Width="160" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Occupation:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left" ColumnSpan="4"><asp:TextBox CssClass="txtbox" ID="ipOccTxt" runat="server" MaxLength="20" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>							
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Injury Date:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left" ColumnSpan="4"><asp:TextBox CssClass="dpDate" ID="ipDOITxt" runat="server" Width="75"  Font-Size="12px" Font-Names="Calibri" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Call Date:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left" ColumnSpan="4"><asp:TextBox CssClass="dpDate" ID="ipDOCTxt" runat="server" Width="75" Font-Size="12px" Font-Names="Calibri" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Body Part:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left" ColumnSpan="4"><asp:TextBox CssClass="txtbox" ID="ipBodyTxt" runat="server" Width="350" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Employer / Insurance Co:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left" ColumnSpan="4"><asp:TextBox CssClass="txtbox" ID="ipEmpInsTxt" runat="server" Width="550" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Caller / Person Confirming:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left" ColumnSpan="4"><asp:TextBox CssClass="txtbox" ID="ipCalledInByTxt" runat="server" Width="250" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Appointment Date:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left" ColumnSpan="4"><asp:TextBox CssClass="txtbox" ID="ipApptDateTxt" runat="server" Width="75" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Appointment Time:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left" ColumnSpan="4"><asp:TextBox CssClass="txtbox" ID="ipApptTimeTxt" runat="server" Width="75" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow ID="conRow" runat="server">
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Physician Name:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left" ColumnSpan="4"><asp:TextBox CssClass="txtbox" ID="ipDoctorTxt" runat="server" Width="225" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="clinRow" runat="server">
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Physician Name:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left" ColumnSpan="4"><asp:TextBox CssClass="txtbox" ID="ipWWDocNameTxt" runat="server" Width="425" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Practice:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left" ColumnSpan="4"><asp:TextBox CssClass="txtbox" ID="ipPracticeTxt" runat="server" Width="425"  ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Physician Phone / Fax:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left" ColumnSpan="4"><asp:TextBox CssClass="txtbox" ID="ipDocPhoneFaxTxt" runat="server" Width="375"  ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Drug / Alcohol:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left" ColumnSpan="4"><asp:TextBox CssClass="txtbox" ID="ipUDSTxt" runat="server" Width="125" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>                             
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Case Manager:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left" ColumnSpan="4"><asp:TextBox CssClass="txtbox" ID="ipCMNameTxt" runat="server" Width="125" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">ER Name:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left" ColumnSpan="4"><asp:TextBox CssClass="txtbox" ID="ipERTxt" runat="server" Width="425" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Open / Closed:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left" ColumnSpan="4"><asp:TextBox CssClass="txtbox" ID="ipOpenClosedTxt" runat="server" Width="125" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="False" HorizontalAlign="Right">Date Closed:</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="Left" ColumnSpan="4"><asp:TextBox CssClass="txtbox" ID="ipDateClsdTxt" runat="server" Width="75" ReadOnly="true" BackColor="LightGray"></asp:TextBox></asp:TableCell>
                        </asp:TableRow>                        
                    </asp:Table>
                    <div class="divider"></div>
                    <div class="appFormLeft"></div>
                    <asp:Table ID="appDtlsBtnsTbl" runat="server" Width="746">
                        <asp:TableRow>
                            <asp:TableCell Width="700">&nbsp;</asp:TableCell>
                            <asp:TableCell Wrap="False" HorizontalAlign="right"><asp:ImageButton ID="ImageButton2" ImageUrl="../Media/layout/btn/appBtnCancel.gif" CausesValidation="false" runat="server" CssClass="smallappBtnNone" OnClick="returnToCal"/></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
	            </ContentTemplate>
            </ajaxToolkit:TabPanel>
                    
            <ajaxToolkit:TabPanel ID="ipOtherApptsTP" runat="server" >
                <ContentTemplate>
                        <br />                  
                        <span style="text-align:left;">
                            <h1>OTHER APPOINTMENTS FOR THIS INJURY</h1>
                            <div class="divider"></div>
                            <div class="appFormLeft"></div>
                        </span>
                        <br />
                       <asp:GridView ID="relatedApptsGrd" Runat="server" 
                            AutoGenerateColumns="False" 
                            AllowPaging="True" 
                            PageSize="15" 
                            GridLines="None" 
                            Width="746" 
                            Font-Name="Calibri" 
                            CssClass="mGrid_NetSrch"  
                            OnRowCommand="relatedApptsGrd_RowCommand"           
                            OnRowDataBound="relatedApptsGrd_RowDataBound"    
                            PagerStyle-CssClass="pgr"
                            AlternatingRowStyle-CssClass="alt"
                            RowStyle-HorizontalAlign="Left"
                            DataKeyNames="APPT_ID">
                            <columns>                            
                                <asp:boundfield datafield="START_TIME"  HeaderText="Date" HeaderStyle-Font-Size="X-Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="true" ItemStyle-Width="100" HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Size="X-Small"/>
                                <asp:boundfield datafield="START_TIME"  HeaderText="Time" HeaderStyle-Font-Size="X-Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="100" HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Size="X-Small"/>
                                <asp:boundfield datafield="CLINIC"  HeaderText="Location" HeaderStyle-Font-Size="X-Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="500" HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Size="X-Small"/>

                                <asp:TemplateField HeaderStyle-Width="80" ControlStyle-Width="80" ItemStyle-Width="80"  ItemStyle-BackColor="White" ControlStyle-BackColor="White" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="arRemoveLocBtn" runat="server" CommandName="showRelAppt" CommandArgument='<%# Eval("APPT_ID") %>'><asp:Image ID="imgEditImage" ImageUrl="../Media/layout/btn/appBtnSelect.png" ToolTip="" runat="server" BorderStyle="None" CssClass="smallappBtnGrd"/></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </columns>
                        </asp:GridView>

                        <asp:Table runat="server" ID="noOtherApptsTbl" Width="746" Font-Name="Calibri" CssClass="mGrid" Font-Size="Small" Visible="false">
                            <asp:TableFooterRow  ForeColor="White" Font-Bold="True" BackColor="#000000" Font-Size="Small" Font-Names="Calibri">
                                <asp:TableHeaderCell Width="100" HorizontalAlign="Left">Date</asp:TableHeaderCell>
                                    <asp:TableHeaderCell Width="100" HorizontalAlign="Left">Time</asp:TableHeaderCell>
                                    <asp:TableHeaderCell Width="500" HorizontalAlign="Left">Location</asp:TableHeaderCell>
                            </asp:TableFooterRow>
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="6" HorizontalAlign="Left" BackColor="LightYellow"><asp:Label ID="Label1" runat="server" Font-Bold="true" BackColor="LightYellow" ForeColor="Black" Font-Size="Smaller" Text="No Other Appointments Found."></asp:Label></asp:TableCell>
                            </asp:TableRow>                            
                        </asp:Table> 
                        <div class="divider"></div>
                        <div class="appFormLeft"></div>
                        <asp:Table ID="relApptDtlsBtnsTbl" runat="server" Width="746">
                            <asp:TableRow>
                                <asp:TableCell Width="700">&nbsp;</asp:TableCell>
                                <asp:TableCell Wrap="False" HorizontalAlign="right"><asp:ImageButton ID="ImageButton1" ImageUrl="../Media/layout/btn/appBtnCancel.gif" CausesValidation="false" runat="server" CssClass="smallappBtnNone" OnClick="returnToCal"/></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
	            </ContentTemplate>
            </ajaxToolkit:TabPanel>

             <ajaxToolkit:TabPanel ID="ipPTApptsTP" runat="server" >
                <ContentTemplate>
                        <br />                   
                        <span style="text-align:left;">
                            <h1>P.T. APPOINTMENTS FOR THIS INJURY</h1>
                            <div class="divider"></div>
                            <div class="appFormLeft"></div>
                        </span>
                        <br />
                        <asp:GridView ID="ptApptsGrd" Runat="server" 
                            AutoGenerateColumns="False" 
                            AllowPaging="True" 
                            PageSize="25" 
                            GridLines="None" 
                            Width="746" 
                            Font-Name="Calibri" 
                            CssClass="mGrid_NetSrch"
                            PagerStyle-CssClass="pgr"
                            AlternatingRowStyle-CssClass="alt"
                            OnPageIndexChanging="ptApptsGrd_PageIndexChanging"
                            RowStyle-HorizontalAlign="Left">
                            <columns>                            
                                <asp:boundfield datafield="PT_CENTER_NAME"  HeaderText="P.T. Center Name" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="true" ItemStyle-Width="500" HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Size="X-Small"/>
                                <asp:boundfield datafield="SESSION_DATE"  HeaderText="Date" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="100" HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Size="X-Small"/>
                                <asp:boundfield datafield="ATTENDED"  HeaderText="Attended" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="100" HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Size="X-Small"/>                                
                            </columns>
                        </asp:GridView>

                        <asp:Table runat="server" ID="srNoPTApptTbl" Width="746" Font-Name="Calibri" CssClass="mGrid" Font-Size="Small" Visible="False">
                            <asp:TableFooterRow  ForeColor="White" Font-Bold="True" BackColor="#000000" Font-Size="Small" Font-Names="Calibri">
                                <asp:TableHeaderCell Width="100" HorizontalAlign="Center">Date</asp:TableHeaderCell>
                                    <asp:TableHeaderCell Width="100" HorizontalAlign="Center">Time</asp:TableHeaderCell>
                                    <asp:TableHeaderCell Width="500" HorizontalAlign="Center">Location</asp:TableHeaderCell>
                            </asp:TableFooterRow>
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="6" HorizontalAlign="Left" BackColor="LightYellow"><asp:Label ID="Label2" runat="server" Font-Bold="true" BackColor="LightYellow" ForeColor="Black" Font-Size="Smaller" Text="No Physical Therapy Appointments Found."></asp:Label></asp:TableCell>
                            </asp:TableRow>                            
                        </asp:Table> 
                        <div class="divider"></div>
                        <div class="appFormLeft"></div>
                        <asp:Table ID="ptApptDtlsBtnsTbl" runat="server" Width="746">
                            <asp:TableRow>
                                <asp:TableCell Width="700">&nbsp;</asp:TableCell>
                                <asp:TableCell Wrap="False" HorizontalAlign="right"><asp:ImageButton ID="ImageButton6" ImageUrl="../Media/layout/btn/appBtnCancel.gif" CausesValidation="false" runat="server" CssClass="smallappBtnNone" OnClick="returnToCal"/></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
	            </ContentTemplate>
            </ajaxToolkit:TabPanel>

            <ajaxToolkit:TabPanel ID="ipStatusReportsTP" runat="server" >
                <ContentTemplate>                                                
                        <br />                   
                        <span style="text-align:left;">
                            <h1>STATUS REPORTS FOR THIS INJURY</h1>
                            <div class="divider"></div>
                            <div class="appFormLeft"></div>
                        </span>
                        <br />
                        <asp:GridView ID="ciStatusRptViewGrd" Runat="server" 
                            AutoGenerateColumns="False" 
                            AllowPaging="True" 
                            AlternatingRowStyle-BackColor="#C3E4ED" 
                            PageSize="30" 
                            AlternatingRowStyle-Wrap="True" 
                            RowStyle-Wrap="False" 
                            GridLines="None" 
                            Width="500" 
                            Font-Name="Calibri" 
                            color="#ffffff"
                            OnRowCommand="ciStatusRptViewGrd_RowCommand"
                            CssClass="mGrid_NetSrch"                       
                            DataKeyNames="STATUSREPORTID">
                            <PagerStyle ForeColor="Black" HorizontalAlign="Right" BackColor="#C6C3C6" Font-Size = "15px"></PagerStyle>
                            <HeaderStyle ForeColor="White" Font-Bold="True" BackColor="#000000"></HeaderStyle>
                            <columns>                                 
                                <asp:boundfield HeaderText="Name" datafield="IPFIRSTNAME" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250"/>
                                <asp:boundfield Headertext="Date of Exam" datafield="IPDOE" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" ItemStyle-Width="100" DataFormatString="{0:M/d/yyyy}" />
                                <asp:boundfield Headertext="Date of Injury" datafield="IPDOI" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" ItemStyle-Width="100" DataFormatString="{0:M/d/yyyy}" />
                                <asp:TemplateField HeaderStyle-Width="80" ControlStyle-Width="80" ItemStyle-Width="80"  ItemStyle-BackColor="White" ControlStyle-BackColor="White" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="ibtnDelete" runat="server" CommandName="showRelAppt" CommandArgument='<%# Eval("STATUSREPORTID") %>'><asp:Image ID="Image1" ImageUrl="../Media/layout/btn/appBtnView.gif" ToolTip="" runat="server" BorderStyle="None" CssClass="smallappBtnGrd"/></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                    
                            </columns>
                        </asp:GridView>  
                            
                        <asp:Table runat="server" ID="srNoViewSearchResultsTbl" Width="746" Font-Name="Calibri" CssClass="mGrid" Font-Size="Small"  Visible="false" >
                            <asp:TableFooterRow  ForeColor="White" Font-Bold="True" BackColor="#000000" Font-Size = "12px">
                                <asp:TableHeaderCell HorizontalAlign="Center">Name</asp:TableHeaderCell>
                                <asp:TableHeaderCell HorizontalAlign="Center">Date of Exam</asp:TableHeaderCell> 
                                <asp:TableHeaderCell HorizontalAlign="Center">Date of Injury</asp:TableHeaderCell>  
                            </asp:TableFooterRow>
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="3" HorizontalAlign="Left" BackColor="LightYellow"><asp:Label ID="Label3" runat="server" Font-Bold="true" BackColor="LightYellow" ForeColor="Black" Font-Size="Smaller" Text="No Status Reports Found."></asp:Label></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>   

                        <asp:Table ID="ipViewSearchNewSearchTbl" runat="server" width="746" Visible ="false">
                            <asp:TableRow>
                                <asp:TableCell>&nbsp;</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Wrap="false" HorizontalAlign="Right" >
                                    <asp:ImageButton ID="ciNewSearchViewStsRptBtn" runat="server" ImageUrl="../Media/layout/btn/appBtnRequst.png" CssClass="smallappBtn"/>
                                </asp:TableCell>
                            </asp:TableRow>		                
	                    </asp:Table>
                        <div class="divider"></div>
                        <div class="appFormLeft"></div>
                        <asp:Table ID="statusRptsBtnsTbl" runat="server" Width="746">
                            <asp:TableRow>
                                <asp:TableCell Width="700">&nbsp;</asp:TableCell>
                                <asp:TableCell Wrap="False" HorizontalAlign="right"><asp:ImageButton ID="ImageButton7" ImageUrl="../Media/layout/btn/appBtnCancel.gif" CausesValidation="false" runat="server" CssClass="smallappBtnNone" OnClick="returnToCal"/></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
	            </ContentTemplate>
            </ajaxToolkit:TabPanel>

        </ajaxToolkit:TabContainer>

        

      </center>
    </asp:Panel>
    <div class="shortAppForm">
        <asp:Panel ID="patientSrchPanel" runat="server" Visible="false" DefaultButton="ipSrchSubmitBtn">
            <br />
            <asp:Table ID="patientSrchBtnTbl" runat="server">
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="6" HorizontalAlign="Left" style="border-bottom:1px solid #8FABD6;">Patient Appointment Search</asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>&nbsp;</asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">First Name:&nbsp;</asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left"><asp:TextBox CssClass="txtbox"  runat="Server" ID="ipSrchFNameTxt"  Width="125" class="txtbox"></asp:TextBox></asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right">&nbsp;&nbsp;&nbsp;Middle Initial:&nbsp;</asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left"><asp:TextBox CssClass="txtbox"  runat="Server" ID="ipSrchMNameTxt"  Width="30px" class="txtbox" MaxLength="1"></asp:TextBox></asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right">&nbsp;&nbsp;&nbsp;Last Name:&nbsp;</asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left"><asp:TextBox CssClass="txtbox"  runat="Server" ID="ipSrchLNameTxt"  Width="125" class="txtbox"></asp:TextBox></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell> Social Security #:&nbsp;</asp:TableCell>
                    <asp:TableCell Wrap="False" HorizontalAlign="Left"><asp:TextBox CssClass="txtbox"  runat="Server" ID="ipSrchSS1SrchTxt"  Width="30px" class="txtbox" MaxLength="3" ></asp:TextBox><label style="width:10px; text-align:right;">-</label><asp:TextBox CssClass="txtbox"  runat="Server" ID="ipSrchSS2SrchTxt"  Width="30px" class="txtbox" MaxLength="2" ></asp:TextBox><label style="width:10px; text-align:right;">-</label><asp:TextBox CssClass="txtbox"  runat="Server" ID="ipSrchSS3SrchTxt"  Width="40px" class="txtbox" MaxLength="4"></asp:TextBox></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="5">&nbsp;</asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right"><asp:ImageButton ID="ipSrchCancelBtn" ImageUrl="~/Images/appBtnCancel.gif" OnClick="CancelSearchBtn_Click" runat="server" CssClass="smallappBtnGrd"/><asp:ImageButton ID="ipSrchSubmitBtn" ImageUrl="~/Images/appBtnSearch.jpg"  OnClick="SubmitSearchBtn_Click" runat="server" CssClass="smallappBtnGrd" /></asp:TableCell>
                </asp:TableRow>                
            </asp:Table>

        </asp:Panel>

        <asp:Panel ID="patientSrchRsltsPanel" runat="server" Visible="false">
            <br />
            <asp:Table ID="patientSrchRsltsHdrTbl" runat="server" Width="900">
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="4" HorizontalAlign="Left" style="border-bottom:1px solid #8FABD6;">Patient Appointment Search Results</asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>&nbsp;</asp:TableCell>
                </asp:TableRow>
            </asp:Table>
             <asp:GridView ID="patientApptSrchRsltsGrd" Runat="server" 
                AutoGenerateColumns="False" 
                AllowPaging="True" 
                PageSize="25" 
                AlternatingRowStyle-Wrap="True" 
                RowStyle-Wrap="true" 
                GridLines="None" 
                Width="900" 
                Font-Name="Calibri" 
                OnRowCommand="patientApptSrchRsltsGrd_RowCommand"
                OnPageIndexChanging="patientApptSrchRsltsGrd_PageIndexChanging"
                CssClass="mGrid"
                PagerStyle-CssClass="pgr"
                AlternatingRowStyle-CssClass="alt"
                RowStyle-HorizontalAlign="Left"
                DataKeyNames="APPT_ID">
                <columns>                        
                    <asp:boundfield datafield="PERSON_FIRST_NAME"  HeaderText="Name" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="150" HeaderStyle-HorizontalAlign="center" ItemStyle-Font-Size="X-Small"/>
                    <asp:boundfield datafield="JGZZ_FISCAL_CODE"  HeaderText="SS#" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="center" ItemStyle-Wrap="false" HeaderStyle-HorizontalAlign="center" ItemStyle-Font-Size="X-Small"/>
                    <asp:boundfield datafield="APPT_TYPE"  HeaderText="Type" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="true" HeaderStyle-HorizontalAlign="center" ItemStyle-Font-Size="X-Small"/>
                    <asp:boundfield datafield="START_TIME"  HeaderText="Date/Time" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="true" HeaderStyle-HorizontalAlign="center" ItemStyle-Font-Size="X-Small"/>
                    <asp:boundfield datafield="CLINIC"  HeaderText="Clinic" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="true" HeaderStyle-HorizontalAlign="center" ItemStyle-Font-Size="X-Small"/>
                    <asp:boundfield datafield="ACCOUNT_NAME"  HeaderText="Employer" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="true" HeaderStyle-HorizontalAlign="center" ItemStyle-Font-Size="X-Small"/>
                    <asp:TemplateField HeaderStyle-Width="50" ControlStyle-Width="50" ItemStyle-Width="50" ItemStyle-BackColor="White" ControlStyle-BackColor="White">
                        <ItemTemplate>
                            <asp:ImageButton ID="locRestoreBtn" ImageUrl="../Media/layout/btn/appBtnView.gif" CommandArgument='<%# Eval("APPT_ID") %>' CausesValidation="false" CommandName="view" runat="server" CssClass="smallappBtnGrd"/>
                        </ItemTemplate>
                    </asp:TemplateField>     
                </columns>
            </asp:GridView>

            <asp:Table runat="server" ID="noPatientSrchRsltsTbl" Width="400" Font-Name="Calibri" CssClass="mGrid" Font-Size="Small" Visible="False">
                <asp:TableFooterRow  ForeColor="White" Font-Bold="True" BackColor="#000000" Font-Size="Small" Font-Names="Calibri">
                    <asp:TableHeaderCell Width="260" HorizontalAlign="Left">Name</asp:TableHeaderCell>
                        <asp:TableHeaderCell Width="260" HorizontalAlign="Left">SS#</asp:TableHeaderCell>
                        <asp:TableHeaderCell Width="110" HorizontalAlign="Left">Type</asp:TableHeaderCell>
                        <asp:TableHeaderCell Width="260" HorizontalAlign="Left">Date/Time</asp:TableHeaderCell>
                        <asp:TableHeaderCell Width="110" HorizontalAlign="Left">Clinic</asp:TableHeaderCell>
                        <asp:TableHeaderCell Width="260" HorizontalAlign="Left">Employer</asp:TableHeaderCell>
                        <asp:TableHeaderCell ></asp:TableHeaderCell>
                </asp:TableFooterRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="6" HorizontalAlign="Left" BackColor="LightYellow"><asp:Label ID="Label6" runat="server" Font-Bold="true" BackColor="LightYellow" ForeColor="Black" Font-Size="Smaller" Text="No phone numbers found."></asp:Label></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        
            <asp:Table ID="patientRsltsSrchBtnTbl" runat="server" Width="900">
                <asp:TableRow>
                    <asp:TableCell>&nbsp;</asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="700">&nbsp;</asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right"><asp:ImageButton ID="ipSrchRsltsCancelBtn" ImageUrl="~/Images/appBtnCancel.gif" OnClick="CancelSearchRsltsBtn_Click" runat="server" CssClass="smallappBtnGrd"/><asp:ImageButton ID="ipNewSrchSubmitBtn" ImageUrl="~/Images/appBtnNewSrch.png"  OnClick="NewSearchRstlsBtn_Click" runat="server" CssClass="smallappBtnGrd" /></asp:TableCell>
                </asp:TableRow>
            </asp:Table> 
            <asp:Label ID="myerrors" runat="server"></asp:Label>
        </asp:Panel>
    </div>
</asp:Content>





