﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RequestStatusReport.aspx.cs" Inherits="RequestStatusReport" MasterPageFile="~/myeworkwell_webapp_dashboard.master"%>

<asp:Content ID="ei_RequestSP" ContentPlaceHolderID="contentHldr" Runat="Server">

    <asp:Panel runat="server" ID="AuthenticatedMessagePanel">

        <asp:Panel ID="eiRqstSRHdrPanel" runat="server" Visible="true">
            <h1>Request Patient Status Reports</h1>
            <div class="divider"></div>
            <div class="appFormLeft"></div> 
        </asp:Panel>

        <asp:Panel ID="eiConfirmPanel" runat="server" Visible="False">
            <h1>Patient Selection Comfirmation</h1>
            <div class="divider"></div>
            <div class="appFormLeft"></div> 
        </asp:Panel>

            <asp:Panel ID="employeeSearchPanel" runat="server" Visible="True">         
                <asp:Table ID="employeeSearchTbl" runat="server" CssClass="mGrid_drk2" Width="860">  
                
                    <asp:TableHeaderRow><asp:TableCell HorizontalAlign="Left">First Name</asp:TableCell><asp:TableCell HorizontalAlign="Left">Last Name</asp:TableCell><asp:TableCell HorizontalAlign="Left">Date Of Injury</asp:TableCell><asp:TableCell HorizontalAlign="Left" ColumnSpan="5">Social Security Number</asp:TableCell></asp:TableHeaderRow>                                              
                    <asp:TableRow HorizontalAlign="Left">
                        <asp:TableCell Wrap="false" HorizontalAlign="left"><asp:TextBox ID="searchFirstNameTxt" runat="server" Width="200"  class="txtbox" ></asp:TextBox></asp:TableCell>
                        <asp:TableCell Wrap="false" HorizontalAlign="left"><asp:TextBox ID="searchLastNameTxt"  runat="server" Width="200"  class="txtbox" ></asp:TextBox></asp:TableCell>
                        <asp:TableCell Wrap="false" HorizontalAlign="left"><asp:TextBox ID="searchDOITxt"  runat="server" Width="90"  class="txtbox" ></asp:TextBox></asp:TableCell>
                        <asp:TableCell Wrap="false" HorizontalAlign="left"><asp:TextBox ID="searchSS1Txt" runat="server" Width="30" class="txtbox"></asp:TextBox></asp:TableCell>
                        <asp:TableCell Wrap="false">-</asp:TableCell>
                        <asp:TableCell Wrap="false" HorizontalAlign="left"><asp:TextBox ID="searchSS2Txt" runat="server" Width="30" class="txtbox"></asp:TextBox></asp:TableCell>
                        <asp:TableCell Wrap="false">-</asp:TableCell>
                        <asp:TableCell Wrap="false" HorizontalAlign="left"><asp:TextBox ID="searchSS3Txt" runat="server" Width="40" class="txtbox"></asp:TextBox></asp:TableCell>
                        <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:ImageButton ID="clearSearchFieldsBtn" runat="server" ImageUrl="images/appBtnClear.gif" OnClick="CancelRequest" CssClass="smallappBtn"/><asp:ImageButton ID="searchLocationsBtn" runat="server" ImageUrl="images/appBtnSearch.gif" OnClick="SearchEmployees" CssClass="smallappBtn"/></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow HorizontalAlign="Right">
                        <asp:TableCell Wrap="false" ColumnSpan="3" >&nbsp;</asp:TableCell>                    
                    </asp:TableRow>                                   
                </asp:Table>   
                <asp:Label ID="employeeSearchErrorMessage" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST enter at least ONE search criteria!!! *<br/>" Visible="False"></asp:Label> 
            </asp:Panel> 

            <asp:Panel ID="employeeSearchResultsPanel" runat="server" Visible="true">                                                                           
                   
                    <asp:GridView ID="employeeSearchResultsGrd" Runat="server" 
                            AutoGenerateColumns="False" 
                            AllowPaging="true"                             
                            PageSize="25"                            
                            RowStyle-Wrap="False" 
                            GridLines="None"                        
                            Font-Name="Calibri" 
                            Width="860"
                            color="#ffffff"
                            CssClass="mGrid"
                            PagerStyle-CssClass="pgr"
                            AlternatingRowStyle-CssClass="alt"
                            RowStyle-HorizontalAlign="Left"
                            DataKeyNames="CUST_ACCOUNT_ID"
                         OnPageIndexChanging="employeeSearchResultsGrd_PageIndexChanging"> 
                            <columns> 
                                <asp:TemplateField ControlStyle-Width="10" ItemStyle-Width="16" ItemStyle-HorizontalAlign="Center">                                                                   
                                    <ItemTemplate>
                                        <asp:CheckBox ID="selectionBox" runat="server"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                                      
                                <asp:boundfield datafield="PARTY_NAME"  HeaderText="Name" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="250" HeaderStyle-HorizontalAlign="Center"/>
                                <asp:boundfield datafield="ATTRIBUTE8"  HeaderText="Date of Injury" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="125" HeaderStyle-HorizontalAlign="Center"/>
                                <asp:boundfield datafield="ATTRIBUTE3"  HeaderText="Date Reported" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="125" HeaderStyle-HorizontalAlign="Center"/>
                                <asp:boundfield datafield="ATTRIBUTE2"  HeaderText="Body Part" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="250" HeaderStyle-HorizontalAlign="Center"/>
                           </columns>
                    </asp:GridView>
                    
                    <asp:Table runat="server" ID="srNoEditSearchResultsTbl" Width="860" Font-Name="Calibri" Visible="false" CssClass="mGrid_drk" Font-Size="Small">
                        <asp:TableFooterRow  ForeColor="White" Font-Bold="True" BackColor="#000000">
                            <asp:TableHeaderCell HorizontalAlign="Center">Name</asp:TableHeaderCell>
                            <asp:TableHeaderCell HorizontalAlign="Center">Date of Injury</asp:TableHeaderCell>
                            <asp:TableHeaderCell HorizontalAlign="Center">Date Reported</asp:TableHeaderCell>
                            <asp:TableHeaderCell HorizontalAlign="Center">Body Part</asp:TableHeaderCell>
                            <asp:TableHeaderCell HorizontalAlign="Center">&nbsp;</asp:TableHeaderCell>
                        </asp:TableFooterRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="6" HorizontalAlign="Left" BackColor="LightYellow"><asp:Label ID="Label2" runat="server" Font-Bold="true" BackColor="LightYellow" ForeColor="Black" Text="No Employees found."></asp:Label></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    

                    <asp:Table ID="submitSelectionsButtonsTbl" runat="server" Width="860">
                        <asp:TableRow>
                            <asp:TableCell>&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right"><asp:ImageButton ID="eiSelectCancelBtn" runat="server" ImageUrl="images/appBtnCancel.gif" OnClick="CancelRequest" CssClass="smallappBtn"/><asp:ImageButton ID="eiSelectSubmitBtn" runat="server" ImageUrl="images/appBtnSelect.png" OnClick="ShowSelected" CssClass="smallappBtn"/></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table> 
                                        
                </asp:Panel>

            <asp:Panel ID="confirmListingPanel" runat="server" Visible="false">

                    <asp:GridView ID="requestCnfrmGrd" Runat="server" 
                        AutoGenerateColumns="False" 
                        AllowPaging="True" 
                        PageSize="25" 
                        AlternatingRowStyle-Wrap="True" 
                        RowStyle-Wrap="False" 
                        GridLines="None" 
                        Width="860" 
                        Font-Name="Calibri" 
                        CssClass="mGrid"
                        PagerStyle-CssClass="pgr"
                        AlternatingRowStyle-CssClass="alt"
                        RowStyle-HorizontalAlign="Left"
                        DataKeyNames="CUST_ACCOUNT_ID">
                        <columns> 
                            <asp:TemplateField>                                                                   
                                <ItemTemplate>
                                    <asp:CheckBox ID="selectionBox" runat="server" Checked="true"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                                                      
                            <asp:boundfield datafield="PARTY_NAME"  HeaderText="Name" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="250" HeaderStyle-HorizontalAlign="Left"/>
                            <asp:boundfield datafield="ATTRIBUTE8"  HeaderText="Date of Injury" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="125" HeaderStyle-HorizontalAlign="Left"/>
                            <asp:boundfield datafield="ATTRIBUTE3"  HeaderText="Date Reported" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="125" HeaderStyle-HorizontalAlign="Left"/>
                            <asp:boundfield datafield="ATTRIBUTE2"  HeaderText="Body Part" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="250" HeaderStyle-HorizontalAlign="Left"/>
                        </columns>
                    </asp:GridView>

                    <asp:Table ID="confirmationSubmitBtnsTbl" runat="server" Width="860">
                        <asp:TableRow>
                            <asp:TableCell>&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Wrap="false" HorizontalAlign="Right"><asp:ImageButton ID="eiConfirmCancelBtn" runat="server" ImageUrl="images/appBtnCancel.gif" OnClick="CancelRequest" CssClass="smallappBtn"/><asp:ImageButton ID="eiConfirmSubmitBtn" runat="server" ImageUrl="images/appBtnSubmit.gif" OnClick="SubmitRequest" CssClass="smallappBtn"/></asp:TableCell>                                           
                        </asp:TableRow>
                    </asp:Table>              

                </asp:Panel>


            <asp:Panel runat="Server" ID="noneSelectedErrorPanel" Visible="false">
                    <asp:Table ID="noneSelectedErrorTbl" runat="server">                
                    <asp:TableRow>
                        <asp:TableCell Wrap="false" HorizontalAlign="Left"><asp:Label ID="noneSelectedError" runat="server" Font-Bold="true" ForeColor="Red" Text="ERROR: You MUST select at least ONE employee from above. *<BR>"></asp:Label></asp:TableCell>                    
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
 
            <asp:Panel runat="Server" ID="requestSumbitPanel" Visible="false" Width="860">
                <div id="halfPageRight1">
                    <h1>Your Requst has been Sent!</h1>
					<div class="divider"></div>
			    </div>
			    <div class="clearBoth"></div>
                <label>Click here to go to the </label><asp:HyperLink ID="savedReturnToMainPageLnk" runat="server" Font-Underline="true" ForeColor="LightSkyBlue" NavigateUrl="LandingPage.aspx">main page.</asp:HyperLink>             
            </asp:Panel>

    </asp:Panel>
    
    <asp:Panel runat="Server" ID="AnonymousMessagePanel">
        <asp:HyperLink runat="server" ID="lnkLogin" Text="Log In**" NavigateUrl="~/Login.aspx"></asp:HyperLink>        
    </asp:Panel>

</asp:Content>
