﻿<%@ Page Language="C#" MasterPageFile="myeworkwell_webapp.master" AutoEventWireup="true" CodeFile="LandingPage.aspx.cs" Inherits="LandingPage" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%--<%@ Register TagPrefix="Highcharts" Namespace="Highcharts" Assembly="Highcharts" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="contentHldr" Runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
	<Scripts>
		<%--Needed for JavaScript IntelliSense in VS2010--%>
		<%--For VS2008 replace RadScriptManager with ScriptManager--%>
		<asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
		<asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
		<asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
	</Scripts>
    
</telerik:RadScriptManager>

<script type="text/javascript">

    function Confirm() {
        if (document.getElementById('<%=ins_acct_combo.ClientID%>') != null) {
            var index = document.getElementById('<%=ins_acct_combo.ClientID%>').selectedIndex;
            if (document.getElementById('<%=ins_acct_combo.ClientID%>').value == "SELECT AN ACCOUNT") {
                alert("Select An Employer First");
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }

        //alert("value =" + document.getElementById('<%=ins_acct_combo.ClientID%>').value); // show selected value
        //alert("text =" + document.getElementById('<%=ins_acct_combo.ClientID%>').options[index].text); // show selected text 
    }

</script>

    <telerik:RadNotification ID="error_notification" runat="server"  Height="150px" Width="350px" 
             ShowCloseButton="true" Font-Bold="true" Font-Size="XX-Large" Position="Center" Animation="Fade" AutoCloseDelay="1000">
    </telerik:RadNotification>

    <asp:Panel runat="server" ID="AuthenticatedMessagePanel">
            <h1>Welcome Back, <asp:LoginName ID="LoginName1" runat="server" />!</h1>
                      <asp:Panel runat="server" ID="multiAcctPanel" Visible="false">
                            <asp:Table runat="server" ID="multiAcctTbl">
                            <asp:TableRow>
                                <asp:TableCell><h2>You're Currently viewing the account of</h2></asp:TableCell>
                                <asp:TableCell><asp:DropDownList runat="server" ID="acctViewingDrp" OnSelectedIndexChanged="changeAcctInfo" AutoPostBack="true"></asp:DropDownList>
                                               
                                               <telerik:RadComboBox ID="ins_acct_combo" runat="server" Height="300px" Width="450px" 
                                                    DropDownWidth="450px" EmptyMessage="Choose an account to view." HighlightTemplatedItems="true"
                                                    EnableLoadOnDemand="true" Filter="StartsWith" OnSelectedIndexChanged="ins_acct_combo_SelectedIndexChanged" AutoPostBack="true">
                                                    <HeaderTemplate>
                                                        <table style="width: 500px" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td style="width: 500px;">
                                                                    Account Name
                                                                </td>                                                                
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table style="width: 500px" cellspacing="0" cellpadding="0">
                                                            <tr>                                                                                                                                        
                                                                <td style="width: 500px;">
                                                                    <%# DataBinder.Eval(Container, "Attributes['ACCT_NAME']")%>
                                                                </td>                                                                
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </telerik:RadComboBox>

                                </asp:TableCell>                                
                            </asp:TableRow>  
                            
                            <asp:TableRow>        
                                <asp:TableCell>&nbsp;</asp:TableCell>                        
                                <asp:TableCell>
                                <%--<asp:DropDownList runat="server" ID="employerViewingDrp" OnSelectedIndexChanged="changeEmployerInfo" AutoPostBack="true"></asp:DropDownList>--%>
                                               <telerik:RadComboBox Visible="false" ID="emp_acct_combo" runat="server" Height="300px" Width="450px" AllowCustomText="true" 
                                                    DropDownWidth="450px" EmptyMessage="Choose an employer to view." HighlightTemplatedItems="true"
                                                    EnableLoadOnDemand="true" Filter="StartsWith" OnSelectedIndexChanged="ins_employer_combo_SelectedIndexChanged" AutoPostBack="true">
                                                    <HeaderTemplate>
                                                        <table style="width: 500px" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td style="width: 500px;">
                                                                    Account Name
                                                                </td>                                                                
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table style="width: 500px" cellspacing="0" cellpadding="0">
                                                            <tr>                                                                                                                                        
                                                                <td style="width: 500px;">
                                                                    <%# DataBinder.Eval(Container, "Attributes['ACCT_NAME']")%>
                                                                </td>                                                                
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </telerik:RadComboBox>
                                </asp:TableCell>                               
                            </asp:TableRow>                                                                      
                        </asp:Table> 
                       
                       </asp:Panel> 


        

                      <div class="divider"></div>
                      <div class="eWorkWellColumn"> 
                <!--start section header-->
                <div class="sectionTotalBkgd">
                          <div class="sectionTop"></div>
                          <h3 class="sectionHead">User Profile</h3>
                          <div class="sectionBottom"></div>
                        </div>
                <!--end section header-->
                <ul class="sectionContent">
                          <li class="edit"><a href="UserAccountEditor.aspx">Edit User Profile</a></li>
                          <div class="sectionContent"><p>No new announcements.</p></div>
                        </ul>
                <!--start section header-->
                <div class="sectionTotalBkgd">
                          <div class="sectionTop"></div>
                          <h3 class="sectionHead">WorkWell Contact</h3>
                          <div class="sectionBottom"></div>
                        </div>
                <!--end section header-->
                <ul class="sectionContent">
                          <li class="pStyle">Name:&nbsp;<asp:Label ID="accountWWRepName" runat="server" Font-Size="X-Small"></asp:Label><br />
                            Phone:&nbsp;<asp:Label ID="accountWWRepPhone" runat="server" ForeColor="GrayText" Font-Size="X-Small"></asp:Label><br />
                            Ext:&nbsp;<asp:Label ID="accountWWRepExt" runat="server" ForeColor="GrayText" Font-Size="X-Small"></asp:Label><br />
                            Fax:&nbsp;<asp:Label ID="accountWWRepFax" runat="server" ForeColor="GrayText" Font-Size="X-Small"></asp:Label><br />                            
                            Email:&nbsp;<asp:Label ID="accountWWRepEmail" runat="server" ForeColor="GrayText" Font-Size="X-Small"></asp:Label></li>
                          
                        </ul>
                <!--end section--> 
        
           

              </div>
                      <div class="eWorkWellColumn"> 
        

                <!--start section header-->
                <div class="sectionTotalBkgd">
                          <div class="sectionTop"></div>
                          <h3 class="sectionHead">Status Reports</h3>
                          <div class="sectionBottom"></div>
                        </div>
                <!--end section header-->
                <ul class="sectionContent">
                          <li class="search"><a href="ei_SearchSR.aspx">View Status Reports</a></li>
                          <li class="request"><a href="RequestStatusReport.aspx">Request Status Reports</a></li>
                        </ul>
                <!--end section--> 

                <!--start section header-->
                <!--<div class="sectionTotalBkgd">
                          <div class="sectionTop"></div>
                          <h3 class="sectionHead">Injuries</h3>
                          <div class="sectionBottom"></div>
                        </div> -->
                <!--end section header-->
                <!-- <ul class="sectionContent">
                          <li class="report"><a href="ei_ReportIP.aspx">Report an Injury</a></li>
                          <li class="view"><a href="ei_ViewIP.aspx">View/Edit an Injury</a></li> 
                        </ul> -->
                <!--end section--> 
                
              </div>
                      <div class="eWorkWellColumn"> 
        
                    <!--start section header-->
                <div class="sectionTotalBkgd">
                          <div class="sectionTop"></div>
                          <h3 class="sectionHead">Services</h3>
                          <div class="sectionBottom"></div>
                        </div>
                <!--end section header-->
                <ul class="sectionContent">                          
                          <li class="view"><a href="ei_MnthCal.aspx">View Schedules</a></li> 
                          <li class="report"><a href="ei_ReportIP.aspx" onclick="if (Confirm() != true) return false;">Send WW an Injury Report</a></li> 
                          <li >&nbsp;</li> 
                </ul>
                <!--end section--> 

                
                <!--start section header-->
                <!--<div class="sectionTotalBkgd">
                          <div class="sectionTop"></div>
                          <h3 class="sectionHead">Account Info</h3>
                          <div class="sectionBottom"></div>
                        </div> -->
                <!--end section header-->
                <!-- <ul class="sectionContent">
                          <li class="view"><a href="ei_ViewAD.aspx">View/Edit Account Details</a></li>
                          <li class="view"><a href="ei_ViewLD.aspx">View/Edit Location Details</a></li>
                          <li class="edit"><a href="ei_LocationAR.aspx">Add/Remove Location</a></li>
                        </ul> -->
                <!--end section--> 
              </div>
                      <div class="eWorkWellColumn"> 
                <!--start section header-->
                
                <!--end section--> 
        
                <!--start section header-->
                <div class="sectionTotalBkgd">
                          <div class="sectionTop"></div>
                          <h3 class="sectionHead">Physician Panel</h3>
                          <div class="sectionBottom"></div>
                        </div>
                <!--end section header-->
                <ul class="sectionContent">
                          <li class="view"><a href="ViewPanels.aspx">View Panels</a></li>
                          <li class="request"><a href="RequestStatusReportChange.aspx">Request Changes to a Panel</a></li>
                          <li class="request"><a href="RequestNewPanel.aspx">Request Panel for a Location</a></li>
                          <li class="search"><a href="ei_SearchProv.aspx">Provider Search</a></li>
                        </ul>
                <!--end section--> 
            </div>
           </asp:Panel>

            
                <%--<div class="chartcontainerarea">
                    <highcharts:AreaChart ID="hcInjuries" Width="820" Height="420" Visible="true" runat="server" />
                </div>--%>
          

   <%-- <asp:Panel runat="server" ID="Panel2">
                <div class="chartcontainerarea">
                    <highcharts:AreaChart ID="AreaChart1" Width="820" Height="420" Visible="true" runat="server" />
                </div>
           </asp:Panel>--%>

    <asp:Panel runat="Server" ID="AnonymousMessagePanel">
        <asp:HyperLink runat="server" ID="lnkLogin" Text="Log In" NavigateUrl="~/Login.aspx"></asp:HyperLink>        
    </asp:Panel>  

</asp:Content>
