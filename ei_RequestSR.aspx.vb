﻿Imports System.Data
Imports System.Data.Odbc
Imports System.Net
Imports System.IO
Imports System.Web.Mail
Imports System.Net.Mail

Partial Class ei_RequestSR
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserType") = 2 Then
            Response.Redirect("iiRoadMap.aspx")
        ElseIf Session("UserType") = 3 Then
            Response.Redirect("ciStatusReport.aspx")
        ElseIf Session("UserType") = 4 Then
            Response.Redirect("MACInsurance.aspx")
        ElseIf Session("UserType") = 5 Then
            Response.Redirect("AholdIT.aspx")
        ElseIf Session("UserType") = 6 Then
            Response.Redirect("Nationwide.aspx")
        ElseIf Session("UserType") = 7 Then
            Response.Redirect("SynergyComp.aspx")
        ElseIf Session("UserType") = -96 Then
            Response.Redirect("dv_ciStatusReport.aspx")
        ElseIf Session("UserType") = -97 Then
            Response.Redirect("dv_eiRoadMap.aspx")
        End If

        If Request.IsAuthenticated Then
            AuthenticatedMessagePanel.Visible = True
            AnonymousMessagePanel.Visible = False

            'Response.Write("PARTY_ID + " & Convert.ToString(Session("PartyID")))

            If Not IsPostBack Then
                GetData()

                If Session("eiRequestSR0") IsNot Nothing Then
                    Dim prevValues As Boolean() = DirectCast(Session("eiRequestSR0"), Boolean())

                    For x = 0 To employeeSearchResultsGrd.Rows.Count - 1
                        If prevValues(x) Then
                            DirectCast(employeeSearchResultsGrd.Rows(x).FindControl("selectionBox"), CheckBox).Checked = True
                        End If
                    Next
                End If
            End If
        Else
            AuthenticatedMessagePanel.Visible = False
            AnonymousMessagePanel.Visible = True
        End If

    End Sub

    Sub GetData()

        Dim iPartyID As Integer = Session("PartyID")
        Dim dDOIStartRangeDate As String = "2000/01/01"
        Dim dDOIEndRangeDate As String = ReverseDate(Now.AddYears(1).ToShortDateString)
        Dim dDOEStartRangeDate As String = "2000/01/01"
        Dim dDOEEndRangeDate As String = ReverseDate(Now.AddYears(1).ToShortDateString)
        Dim sBuiltQuery As String = ""
        Dim sClaimNumberQuery As String = ""
        Dim sSocialSecNum As String = ""
        Dim sSocialSecNumQuery As String = ""
        Dim bSSEntered As Boolean = True

        srNoEditSearchResultsTbl.Visible = False

        If String.IsNullOrEmpty(searchFirstNameTxt.Text.Trim) = False Then
            sBuiltQuery = " AND HP_IP.PERSON_FIRST_NAME LIKE '%" & searchFirstNameTxt.Text.ToUpper.Trim & "%' "
        End If

        If String.IsNullOrEmpty(searchLastNameTxt.Text.Trim) = False Then
            sBuiltQuery = sBuiltQuery & " AND HP_IP.PERSON_LAST_NAME LIKE '%" & searchLastNameTxt.Text.ToUpper.Trim & "%' "
        End If

        If String.IsNullOrEmpty(searchDOITxt.Text.Trim) = False And IsDate(searchDOITxt.Text) Then
            sBuiltQuery = sBuiltQuery & " AND HCA_IP.ATTRIBUTE8 = '" & ReverseDate(searchDOITxt.Text) & " 00:00:00' "
        End If

        If String.IsNullOrEmpty(searchSS1Txt.Text) = False Or String.IsNullOrEmpty(searchSS2Txt.Text) = False Or String.IsNullOrEmpty(searchSS3Txt.Text) = False Then

            If String.IsNullOrEmpty(searchSS1Txt.Text) = False Then
                sSocialSecNum = searchSS1Txt.Text.Trim & "-"
            Else
                sSocialSecNum = "%-"
                bSSEntered = False
            End If

            If String.IsNullOrEmpty(searchSS2Txt.Text) = False Then
                sSocialSecNum = sSocialSecNum & searchSS2Txt.Text.Trim & "-"
            Else
                sSocialSecNum = sSocialSecNum & "%-"
                bSSEntered = False
            End If

            If String.IsNullOrEmpty(searchSS3Txt.Text) = False Then
                sSocialSecNum = sSocialSecNum & searchSS3Txt.Text.Trim

            Else
                sSocialSecNum = sSocialSecNum & "%"
                bSSEntered = False
            End If

            If bSSEntered Then
                sBuiltQuery = sBuiltQuery & " AND HP_IP.JGZZ_FISCAL_CODE = '" & sSocialSecNum & "' "
            Else
                sBuiltQuery = sBuiltQuery & " AND HP_IP.JGZZ_FISCAL_CODE LIKE '" & sSocialSecNum & "' "
            End If

        End If

        'If String.IsNullOrEmpty(searchClaimTxt.Text.Trim) = False Then
        ' sBuiltQuery = sBuiltQuery & " AND HCA_IP.ACCOUNT_NUMBER LIKE '%" & searchClaimTxt.Text & "%' "
        ' End If

        Dim dt As New DataTable()
        Dim sSqlQuery As String = ""

        If (StrComp(Convert.ToString(Session("InsCarrier")), "N") = 0) Or (StrComp(Convert.ToString(Session("InsCarrier")), "Y") = 0 And Convert.ToInt32(Session("PartyID")) <> Convert.ToInt32(Session("InsPartyID"))) Then

            sSqlQuery = "SELECT HP_IP.PARTY_NAME, REPLACE(HCA_IP.ATTRIBUTE8,' 00:00:00','') ATTRIBUTE8, REPLACE(HCA_IP.ATTRIBUTE3,' 00:00:00','') ATTRIBUTE3, HCA_IP.ATTRIBUTE2, HCA_IP.ATTRIBUTE5, HCA_IP.CUST_ACCOUNT_ID, HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME, HP_IP.PERSON_LAST_NAME " & _
                        "FROM HZ_CUST_ACCOUNTS HCA_IP " & _
                                "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_INS ON HCARA_INS.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID " & _
                                "INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA_INS.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID " & _
                                "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_EMP ON HCARA_EMP.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID " & _
                                "INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCA_EMP.CUST_ACCOUNT_ID = HCARA_EMP.RELATED_CUST_ACCOUNT_ID " & _
                                "INNER JOIN HZ_PARTIES HP_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID " & _
                        "WHERE HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT' AND " & _
                                "HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' AND " & _
                                "HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' AND " & _
                                "HCA_EMP.PARTY_ID = " & iPartyID & " AND " & _
                                "HCARA_INS.STATUS = 'A' AND " & _
                                "HCARA_EMP.STATUS = 'A'  AND " & _
                                "HCA_IP.CUST_ACCOUNT_ID > 0  AND " & _
                                "HCA_IP.ATTRIBUTE9 IS NOT NULL " & sBuiltQuery & _
                        "ORDER BY 2 DESC"

        Else
            sSqlQuery = "SELECT HP_IP.PARTY_NAME, REPLACE(HCA_IP.ATTRIBUTE8,' 00:00:00','') ATTRIBUTE8, REPLACE(HCA_IP.ATTRIBUTE3,' 00:00:00','') ATTRIBUTE3, HCA_IP.ATTRIBUTE2, HCA_IP.ATTRIBUTE5, HCA_IP.CUST_ACCOUNT_ID, HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME, HP_IP.PERSON_LAST_NAME " & _
                        "FROM HZ_CUST_ACCOUNTS HCA_IP  " & _
                        "        INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_INS ON HCARA_INS.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID  " & _
                        "        INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA_INS.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID  " & _
                        "        INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_EMP ON HCARA_EMP.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID  " & _
                        "        INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCA_EMP.CUST_ACCOUNT_ID = HCARA_EMP.RELATED_CUST_ACCOUNT_ID  " & _
                        "        INNER JOIN HZ_PARTIES HP_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID  " & _
                        "        INNER JOIN (SELECT HP.PARTY_ID " & _
                        "			FROM HZ_CUST_ACCOUNTS HCA_INS INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_INS.CUST_ACCOUNT_ID = HCARA.RELATED_CUST_ACCOUNT_ID " & _
                        "				 INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID " & _
                        "				 INNER JOIN HZ_PARTIES HP ON HCA_EMP.PARTY_ID = HP.PARTY_ID " & _
                        "			WHERE HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' " & _
                        "				  AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' " & _
                        "				  AND HCARA.STATUS = 'A' " & _
                        "				  AND HCA_INS.PARTY_ID = " & Convert.ToString(Session("InsPartyID")) & " " & _
                        "				  AND HCA_EMP.STATUS = 'A') REL_ACCTS ON HCA_EMP.PARTY_ID = REL_ACCTS.PARTY_ID " & _
                        "WHERE HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT' AND  " & _
                        "        HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' AND  " & _
                        "        HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' AND " & _
                        "	HCARA_INS.STATUS = 'A' AND  " & _
                        "        HCARA_EMP.STATUS = 'A' AND " & _
                        "         HCA_IP.CUST_ACCOUNT_ID > 0  AND" & _
                        "        HCA_IP.ATTRIBUTE9 IS NOT NULL  " & sBuiltQuery & _
                        "GROUP BY HP_IP.PARTY_NAME, HCA_IP.ATTRIBUTE8, HCA_IP.ATTRIBUTE3, HCA_IP.ATTRIBUTE2, HCA_IP.ATTRIBUTE5, HCA_IP.CUST_ACCOUNT_ID, HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME, HP_IP.PERSON_LAST_NAME  " & _
                        "ORDER BY 2 DESC "
        End If

        'Response.Write(sSqlQuery)

        Dim strConnection As String = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;" 'ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()
        Dim objConn As OdbcConnection = New OdbcConnection(strConnection)
        Dim objDataAdapter As OdbcDataAdapter = New OdbcDataAdapter(sSqlQuery, objConn)
        Dim objCmd As OdbcCommand = Nothing

        Try
            objConn.Open()
            objDataAdapter.Fill(dt)

            employeeSearchResultsGrd.DataSource = dt
            employeeSearchResultsGrd.DataBind()

            If dt.Rows.Count > 0 Then
                employeeSearchResultsGrd.Visible = True
                submitSelectionsButtonsTbl.Visible = True
            Else
                employeeSearchResultsGrd.Visible = False
                submitSelectionsButtonsTbl.Visible = False
                srNoEditSearchResultsTbl.Visible = True
            End If

        Catch ex As Exception
            Throw ex
        Finally
            objConn.Close()
            objDataAdapter.Dispose()
            objConn.Dispose()
        End Try

    End Sub

    Function ReverseDate(ByVal dDate As Date) As String

        If String.IsNullOrEmpty(dDate) = False Then
            Return dDate.ToString("yyyy/MM/dd")
        Else
            Return ""
        End If

    End Function

    Function ConvertDate(ByVal dDateOfTreatment As Date) As String

        If String.IsNullOrEmpty(dDateOfTreatment) = False Then
            Return dDateOfTreatment.ToString("MM/dd/yyyy")
        Else
            Return ""
        End If

    End Function

    Sub SubmitRequest()

        Dim sPatientsInfo As String = ""
        Dim bOneSelected As Boolean = False
        Dim values As Boolean() = New Boolean(requestCnfrmGrd.PageSize - 1) {}

        noneSelectedErrorTbl.Visible = False

        For i As Integer = 0 To requestCnfrmGrd.Rows.Count - 1

            Dim row As GridViewRow = requestCnfrmGrd.Rows(i)
            Dim iCustAcctID As Integer = requestCnfrmGrd.DataKeys(i).Value
            Dim isChecked As Boolean = DirectCast(row.FindControl("selectionBox"), CheckBox).Checked

            If isChecked Then

                bOneSelected = True

                Dim sSqlQuery As String = "SELECT HP_IP.PARTY_NAME, REPLACE(HCA_IP.ATTRIBUTE8,' 00:00:00','') ATTRIBUTE8, REPLACE(HCA_IP.ATTRIBUTE3,' 00:00:00','') ATTRIBUTE3, HCA_IP.ATTRIBUTE2, HCA_IP.ATTRIBUTE5, HCA_IP.CUST_ACCOUNT_ID, HCA_EMP.ACCOUNT_NAME " & _
                                       "FROM HZ_CUST_ACCOUNTS HCA_IP " & _
                                            "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_INS ON HCARA_INS.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID " & _
                                            "INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA_INS.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID " & _
                                            "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_EMP ON HCARA_EMP.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID " & _
                                            "INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCA_EMP.CUST_ACCOUNT_ID = HCARA_EMP.RELATED_CUST_ACCOUNT_ID " & _
                                            "INNER JOIN HZ_PARTIES HP_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID " & _
                                       "WHERE HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT' AND " & _
                                             "HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' AND " & _
                                             "HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' AND " & _
                                             "HCA_IP.CUST_ACCOUNT_ID = " & iCustAcctID & " AND " & _
                                             "HCARA_INS.STATUS = 'A' AND " & _
                                             "HCARA_EMP.STATUS = 'A' AND " & _
                                             "HCA_IP.ATTRIBUTE9 IS NOT NULL " & _
                                       "ORDER BY 2 "

                Dim strConnection As String = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;" 'ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()
                Dim objConn As OdbcConnection = New OdbcConnection(strConnection)
                Dim objSqlCmd As OdbcCommand = Nothing
                Dim objSqlDataRdr As OdbcDataReader = Nothing

                Try
                    objConn.Open()

                    objSqlCmd = New OdbcCommand("", objConn)
                    objSqlCmd.CommandText = sSqlQuery
                    objSqlCmd.CommandType = Data.CommandType.Text
                    objSqlDataRdr = objSqlCmd.ExecuteReader()

                    If objSqlDataRdr.Read() Then
                        sPatientsInfo = sPatientsInfo & "Name: " & objSqlDataRdr.Item(0) & "<BR> Employer: " & objSqlDataRdr.Item(6) & "<BR> Date of Injury: " & ConvertDate(objSqlDataRdr.Item(1)) & "<BR> Date Reprted: " & ConvertDate(objSqlDataRdr.Item(2)) & "<BR><BR>"
                    End If

                Catch ex As Exception
                    Throw ex
                Finally
                    objConn.Close()
                    objSqlCmd.Dispose()
                    objConn.Dispose()
                End Try


            End If
        Next
        Dim req_email As String = ""
        Dim req_person As String = ""
        Dim body As New StringBuilder
        Dim message As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage
        message.IsBodyHtml = True
        message.Subject = "Status Report Request - myeWorkWell.com"
        message.From = New MailAddress("info@myeworkwell.com", "myeWorkwell Info")
        message.To.Add(New MailAddress("consulting@eworkwell.com", "Consulting Case Managers"))
        message.To.Add(New MailAddress("notifications@eworkwell.com", "WorkWell Notifications"))


        Dim sql As String = "SELECT SEMAILADDRESS, SFIRSTNAME, SLASTNAME FROM USER_INFO WHERE SUSERNAME = '" & Convert.ToString(Session("UserName")) & "'"

        Dim connStr As String = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;" 'ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()
        Dim conn As OdbcConnection = New OdbcConnection(connStr)
        Dim cmd As OdbcCommand = Nothing
        Dim rdr As OdbcDataReader = Nothing

        Try
            conn.Open()

            cmd = New OdbcCommand("", conn)
            cmd.CommandText = sql
            cmd.CommandType = Data.CommandType.Text
            rdr = cmd.ExecuteReader()

            If rdr.Read() Then
                req_email = myCStr(rdr.Item(0))
                req_person = myCStr(rdr.Item(1)) & " " & myCStr(rdr.Item(2))
            End If

        Catch ex As Exception
            Throw ex
        Finally
            rdr.Close()
            cmd.Dispose()
            conn.Dispose()
        End Try

        With body
            .Append("Requested by: " & req_person & "<br/>")
            .Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & req_email & "<br/><br/>")
            .Append("Please enter the patient's status report into the myeWorkwell.com database." & "<br/><br/>")
            .Append(sPatientsInfo)
            .Append("<hr />")
            .Append("<font size='2' color='#cccccc'>")
            .Append("<hr />")
            .Append("www.eworkwell.com")
            .Append("<br>")
            .Append(Date.Now.AddHours(3))
            .Append("</font>")
        End With

        If bOneSelected Then
            message.Body = body.ToString
            'relay-hosting.secureserver.net is the valid SMTP server of godaddy.com
            Dim mailClient As SmtpClient = New SmtpClient("dedrelay.secureserver.net")

            mailClient.Send(message)
            message.Dispose()

            For i = 0 To requestCnfrmGrd.PageCount - 1

                '********
                ' this is to clear all selected locations session vars, i can't remove the session vars wirh remove (above), so I'm just seting everything to false for now.
                For x As Integer = 0 To requestCnfrmGrd.Rows.Count - 1
                    values(x) = False
                Next
                Session("eiRequestSR" & i) = values
            Next

            eiConfirmPanel.Visible = False
            eiRqstSRHdrPanel.Visible = False
            requestSumbitPanel.Visible = True
            confirmListingPanel.Visible = False

        Else
            noneSelectedErrorTbl.Visible = True
        End If


    End Sub

    Sub CancelRequest()

        Dim values As Boolean() = New Boolean(employeeSearchResultsGrd.PageSize - 1) {}

        For i = 0 To employeeSearchResultsGrd.PageCount - 1

            '********
            ' this is to clear all selected locations session vars, i can't remove the session vars wirh remove (above), so I'm just seting everything to false for now.
            For x As Integer = 0 To employeeSearchResultsGrd.Rows.Count - 1
                values(x) = False
            Next
            Session("eiRequestSR" & i) = values
        Next

        'GetData()
        ClearSearchFields()

        employeeSearchResultsGrd.PageIndex = 0
        employeeSearchResultsGrd.DataBind()

        employeeSearchPanel.Visible = True
        employeeSearchResultsPanel.Visible = True
        eiConfirmPanel.Visible = False
        eiRqstSRHdrPanel.Visible = True
        confirmListingPanel.Visible = False

    End Sub

    Sub SearchEmployees()

        GetData()

    End Sub

    Sub ClearSearchFields()

        searchFirstNameTxt.Text = ""
        searchLastNameTxt.Text = ""
        searchDOITxt.Text = ""
        searchSS1Txt.Text = ""
        searchSS2Txt.Text = ""
        searchSS3Txt.Text = ""

        GetData()

    End Sub




    Protected Sub employeeSearchResultsGrd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles employeeSearchResultsGrd.PageIndexChanging

        'noneSelectedErrorTbl.Visible = False

        Dim d As Integer = employeeSearchResultsGrd.PageCount
        Dim values As Boolean() = New Boolean(employeeSearchResultsGrd.PageSize - 1) {}
        Dim chb As CheckBox
        Dim isChecked As Integer = 0

        Dim count As Integer = 0
        For i As Integer = 0 To employeeSearchResultsGrd.Rows.Count - 1
            chb = DirectCast(employeeSearchResultsGrd.Rows(i).FindControl("selectionBox"), CheckBox)
            If chb IsNot Nothing Then
                values(i) = chb.Checked
                isChecked = 1
            End If
        Next

        If isChecked = 1 Then
            Session("eiRequestSR" & employeeSearchResultsGrd.PageIndex) = values
        End If

        GetData()

        employeeSearchResultsGrd.PageIndex = e.NewPageIndex
        employeeSearchResultsGrd.DataBind()

        If Session("eiRequestSR" & e.NewPageIndex) IsNot Nothing Then
            Dim prevValues As Boolean() = DirectCast(Session("eiRequestSR" & e.NewPageIndex), Boolean())

            For x = 0 To employeeSearchResultsGrd.Rows.Count - 1
                If prevValues(x) Then
                    DirectCast(employeeSearchResultsGrd.Rows(x).FindControl("selectionBox"), CheckBox).Checked = True
                End If
            Next
        End If

    End Sub

    Sub ShowSelected()

        Dim strConnection As String = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;" 'ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()
        Dim objConnection As New OdbcConnection(strConnection)
        Dim objSqlCmd As New OdbcCommand
        Dim objSqlDataRdr As OdbcDataReader = Nothing
        Dim currentGrdPage As Integer = employeeSearchResultsGrd.PageIndex
        Dim selectedLocs As New DataTable()
        Dim values As Boolean() = New Boolean(employeeSearchResultsGrd.PageSize - 1) {}
        Dim checkBx As CheckBox
        Dim isChecked As Integer = 0
        Dim curPageOneChecked As Integer = 0
        Dim bOneChecked As Integer = 0

        selectedLocs.Columns.Add("CUST_ACCOUNT_ID")
        selectedLocs.Columns.Add("PARTY_NAME")
        selectedLocs.Columns.Add("ATTRIBUTE8")
        selectedLocs.Columns.Add("ATTRIBUTE3")
        selectedLocs.Columns.Add("ATTRIBUTE2")

        noneSelectedErrorPanel.Visible = False

        'get the selections current page that user is on
        For i As Integer = 0 To employeeSearchResultsGrd.Rows.Count - 1
            checkBx = DirectCast(employeeSearchResultsGrd.Rows(i).FindControl("selectionBox"), CheckBox)
            If checkBx IsNot Nothing Then
                values(i) = checkBx.Checked
                If values(i) Then
                    curPageOneChecked = 1
                End If
            End If
        Next

        If curPageOneChecked = 1 Then
            Session("eiRequestSR" & employeeSearchResultsGrd.PageIndex) = values
        End If

        For i = 0 To employeeSearchResultsGrd.PageCount - 1

            If Session("eiRequestSR" & i) IsNot Nothing Then

                'set panelSearchRsltsGrd to page that the checked locations were

                GetData()
                employeeSearchResultsGrd.PageIndex = i
                employeeSearchResultsGrd.DataBind()

                Dim prevValues As Boolean() = DirectCast(Session("eiRequestSR" & i), Boolean())

                For x = 0 To employeeSearchResultsGrd.Rows.Count - 1
                    If prevValues(x) Then
                        bOneChecked = 1
                    End If
                Next
            End If
        Next

        If bOneChecked = 1 Then

            Try

                'store the selected locations on current page
                'Session("page" & viewActiveLocationsGrd.PageIndex) = values

                objConnection.Open()
                objSqlCmd.Connection = objConnection

                'now get all the other pages that user looked and selected

                For i = 0 To employeeSearchResultsGrd.PageCount - 1

                    If Session("eiRequestSR" & i) IsNot Nothing Then

                        'set viewActiveLocationsGrd to page that the checked locations were

                        GetData()
                        employeeSearchResultsGrd.PageIndex = i
                        employeeSearchResultsGrd.DataBind()


                        Dim prevValues As Boolean() = DirectCast(Session("eiRequestSR" & i), Boolean())
                        'Response.Write("page" & viewActiveLocationsGrd.PageIndex & "<BR>")
                        For x = 0 To employeeSearchResultsGrd.Rows.Count - 1
                            'Response.Write("values(" & x & ") = " & values(x))
                            If prevValues(x) Then
                                Dim iCustAcctID As String = employeeSearchResultsGrd.DataKeys(x).Value
                                'Response.Write("Page: " & i & " Row: " & x & " is checked.  The CUST_ACCOUNT_ID = " & iCustAcctID & "<BR>")

                                Try

                                    objSqlCmd.CommandText = "SELECT HP_IP.PARTY_NAME, REPLACE(HCA_IP.ATTRIBUTE8,' 00:00:00','') ATTRIBUTE8, REPLACE(HCA_IP.ATTRIBUTE3,' 00:00:00','') ATTRIBUTE3, HCA_IP.ATTRIBUTE2, HCA_IP.ATTRIBUTE5, HCA_IP.CUST_ACCOUNT_ID " & _
                                                                "FROM HZ_CUST_ACCOUNTS HCA_IP " & _
                                                                     "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_INS ON HCARA_INS.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID " & _
                                                                     "INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA_INS.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID " & _
                                                                     "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_EMP ON HCARA_EMP.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID " & _
                                                                     "INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCA_EMP.CUST_ACCOUNT_ID = HCARA_EMP.RELATED_CUST_ACCOUNT_ID " & _
                                                                     "INNER JOIN HZ_PARTIES HP_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID " & _
                                                                "WHERE HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT' AND " & _
                                                                      "HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' AND " & _
                                                                      "HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' AND " & _
                                                                      "HCA_IP.CUST_ACCOUNT_ID = " & iCustAcctID & " AND " & _
                                                                      "HCARA_INS.STATUS = 'A' AND " & _
                                                                      "HCARA_EMP.STATUS = 'A' AND " & _
                                                                      "HCA_IP.ATTRIBUTE9 IS NOT NULL " & _
                                                                "ORDER BY 2 "
                                    'Response.Write(objSqlCmd.CommandText)
                                    objSqlCmd.CommandType = Data.CommandType.Text
                                    objSqlDataRdr = objSqlCmd.ExecuteReader()

                                    If objSqlDataRdr.Read() Then

                                        Dim dr As DataRow = selectedLocs.NewRow()
                                        dr("PARTY_NAME") = myCStr(objSqlDataRdr.Item(0))
                                        dr("ATTRIBUTE8") = myCStr(objSqlDataRdr.Item(1))
                                        dr("ATTRIBUTE3") = myCStr(objSqlDataRdr.Item(2))
                                        dr("ATTRIBUTE2") = myCStr(objSqlDataRdr.Item(3))
                                        dr("CUST_ACCOUNT_ID") = myCStr(objSqlDataRdr.Item(5))

                                        selectedLocs.Rows.Add(dr)

                                        'Response.Write(myCStr(objSqlDataRdr.Item(0)) & " " & myCStr(objSqlDataRdr.Item(1)) & myCStr(objSqlDataRdr.Item(2)) & " " & myCStr(objSqlDataRdr.Item(3)) & myCStr(objSqlDataRdr.Item(4)) & " " & myCStr(objSqlDataRdr.Item(5)) & "<BR>")
                                    End If
                                    objSqlDataRdr.Close()
                                    objSqlDataRdr.Dispose()
                                Catch ex As Exception
                                    Response.Write("Can't load Web page " & ex.Message)
                                End Try

                            End If

                        Next
                    End If
                Next

                requestCnfrmGrd.DataSource = selectedLocs
                requestCnfrmGrd.DataBind()

            Catch ex As Exception
                Response.Write("Can't load Web page " & ex.Message)
            Finally
                objConnection.Close()
                objConnection.Dispose()
            End Try

            'now get back to the current page that they were on before submiting the selections
            GetData()
            employeeSearchResultsGrd.PageIndex = currentGrdPage
            employeeSearchResultsGrd.DataBind()

            'keep to current page's selections checked if they click back

            Dim existingValues As Boolean() = DirectCast(Session("eiRequestSR" & employeeSearchResultsGrd.PageIndex), Boolean())
            'Response.Write("page" & viewActiveLocationsGrd.PageIndex & "<BR>")
            For x = 0 To employeeSearchResultsGrd.Rows.Count - 1
                'Response.Write("values(" & x & ") = " & values(x))
                If existingValues(x) Then
                    DirectCast(employeeSearchResultsGrd.Rows(x).FindControl("selectionBox"), CheckBox).Checked = True
                End If
            Next

            employeeSearchPanel.Visible = False
            employeeSearchResultsPanel.Visible = False
            eiConfirmPanel.Visible = True
            eiRqstSRHdrPanel.Visible = False
            confirmListingPanel.Visible = True

        Else
            'show error
            noneSelectedErrorPanel.Visible = True
            GetData()

            employeeSearchResultsGrd.PageIndex = currentGrdPage
            employeeSearchResultsGrd.DataBind()

        End If

    End Sub

    Function myCStr(ByVal test As Object) As String

        Dim sReturnStr As String = ""

        If IsDBNull(test) Then
            sReturnStr = ""
        Else
            sReturnStr = CStr(test)
        End If

        If StrComp(sReturnStr, "#") = 0 Then
            sReturnStr = ""
        End If

        myCStr = sReturnStr

    End Function

    Protected Sub employeeSearchResultsGrd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles employeeSearchResultsGrd.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim dDateOfInjury As String = DataBinder.Eval(e.Row.DataItem, "ATTRIBUTE8")
            Dim dDateReported As String = myCStr(DataBinder.Eval(e.Row.DataItem, "ATTRIBUTE3"))
            Dim sFName As String = myCStr(DataBinder.Eval(e.Row.DataItem, "PERSON_FIRST_NAME")).Trim()
            Dim sLName As String = myCStr(DataBinder.Eval(e.Row.DataItem, "PERSON_LAST_NAME")).Trim()
            Dim sMName As String = myCStr(DataBinder.Eval(e.Row.DataItem, "PERSON_MIDDLE_NAME")).Trim()

            Dim dr As DataRow = DirectCast(e.Row.DataItem, DataRowView).Row

            If String.IsNullOrEmpty(sMName) = False Then
                If sMName.Length = 1 Then
                    sMName = sMName & "."
                End If
            End If


            e.Row.Cells(1).Text = sLName & ", " & sFName & " " & sMName
            e.Row.Cells(2).Text = ConvertDate(dDateOfInjury)
            e.Row.Cells(3).Text = ConvertDate(dDateReported)

        End If

    End Sub
End Class
