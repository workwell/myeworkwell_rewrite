﻿Imports System.IO
Imports System.Data

Partial Class ei_ViewPP
    Inherits System.Web.UI.Page

    Dim nwPanelURLs As New DataTable()
    Dim objReader As StreamWriter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.IsAuthenticated Then
            ' this will determine if the user is an employer or insurance carrier/broker
            If Session("UserType") = 2 Then
                Response.Redirect("iiRoadMap.aspx")
            ElseIf Session("UserType") = 3 Then
                Response.Redirect("ciStatusReport.aspx")
            ElseIf Session("UserType") = 4 Then
                Response.Redirect("MACInsurance.aspx")
            ElseIf Session("UserType") = 5 Then
                Response.Redirect("AholdIT.aspx")
            ElseIf Session("UserType") = 6 Then
                Response.Redirect("Nationwide.aspx")
            ElseIf Session("UserType") = 7 Then
                Response.Redirect("SynergyComp.aspx")
            End If

            AuthenticatedMessagePanel.Visible = True
            AnonymousMessagePanel.Visible = False

            GetPanels()

        Else
            AuthenticatedMessagePanel.Visible = False
            AnonymousMessagePanel.Visible = True

        End If

    End Sub



    Sub GetPanels()

        nwPanelURLs = New DataTable()
        nwPanelURLs.Columns.Add("FileName")
        nwPanelURLs.Columns.Add("FileDir")
        nwPanelURLs.Columns.Add("UploadDate")

        Dim sFileName As String = ""
        Dim sHeaderName As String = ""

        Dim iPartyID As String = Convert.ToString(Session("PartyID"))
        Dim iInsPartyID As String = Convert.ToString(Session("InsPartyID"))
        Dim motorists As String = Convert.ToString(Session("MotoristsSub"))
        searchResultsPanel.Visible = True
        nwNoResultsFoundTbl.Visible = False

        If StrComp(Convert.ToString(Session("InsCarrier")), "1") = 0 Then
            If StrComp(iInsPartyID, "5674") = 0 Then
                GetFiles("/PANELS/" & iInsPartyID & "/")
            Else
                GetFiles("/PANELS/" & iInsPartyID & "/" & Session("MotoristsSub"))
            End If
            '10/31/16 eb fixed an issue with strcomp where it wasn't working properly and i'm just going to 
            ' use the simple version instead.
        ElseIf iInsPartyID = "45516" Then
            GetFiles("/PANELS/DemoPanels/")
        ElseIf iPartyID = "388046" Then '11/7/16 update for catholic fixing thier workaround they seperated both when it wasn't required
            GetFiles("/Panels/388046/")
        ElseIf iPartyID = "15900" Then '11/7/16 update for catholic fixing thier workaround they seperated both when it wasn't required
            GetFiles("/Panels/388046/")
        ElseIf StrComp(Convert.ToString(Session("InsCarrier")), "N") = 0 Then
            GetFiles("/PANELS/" & iPartyID & "/")
        ElseIf StrComp(Convert.ToString(Session("InsCarrier")), "I") = 0 Then
            GetFiles("/PANELS/" & iPartyID & "/" & Session("CarrierID"))
        ElseIf StrComp(Convert.ToString(Session("InsCarrier")), "E") = 0 Then

            If StrComp(Convert.ToString(Session("MultipleLocs")), "1") = 0 Then ' no multiple locs
                GetFiles("/PANELS/" & iPartyID & "/")
                '9/16/16
                'Setting for callos per ticket 2727
            ElseIf (StrComp(iPartyID, "15576") = 0) Then
                GetFiles("/PANELS/" & iPartyID & "/")
            Else
                GetFiles("/PANELS/" & iPartyID & "/" & Session("CustAccountID"))
            End If
        Else
            If (StrComp(iInsPartyID, "5188") = 0) Then
                'GetFiles("/PANELS/" & iInsPartyID & "/" & iPartyID & "/") ' was changed 9/23/14 ticket #612
                GetFiles("/PANELS/" & iPartyID & "/")
            ElseIf (StrComp(iPartyID, "4097") = 0) Then
                GetFiles("/PANELS/" & iPartyID & "/" & motorists & "/")
            Else
                GetFiles("/PANELS/" & iPartyID & "/")
                'GetFiles("/PANELS/" & iInsPartyID & "/")
            End If
        End If

        panelSearchRsltsGrd.DataSource = nwPanelURLs
        panelSearchRsltsGrd.DataBind()

        If nwPanelURLs.Rows.Count = 0 Then
            nwNoResultsFoundTbl.Visible = True
        End If

    End Sub


    Sub GetDocuments(ByVal relativePath As String)

        Dim physicalPath As String = Server.MapPath(relativePath)
        Response.Write("physical path " & physicalPath & "<BR>")
        Dim directory As New System.IO.DirectoryInfo(physicalPath)

        If directory.Exists Then
            Dim searchStr As String = ""

            If String.IsNullOrEmpty(nwCompanyNameTxt.Text) = False Then
                searchStr = "*" & nwCompanyNameTxt.Text & "*.pdf"
            Else
                searchStr = searchStr & "*.pdf"
            End If

            For Each myFile In directory.GetFiles(searchStr)
                Dim dr As DataRow = nwPanelURLs.NewRow()
                dr("FileName") = myFile.Name
                dr("FileDir") = myFile.FullName.Replace("D:\Hosting\6546477\html", "")
                dr("UploadDate") = myFile.LastWriteTime.ToShortDateString
                nwPanelURLs.Rows.Add(dr)
            Next

        Else
            Throw New System.IO.DirectoryNotFoundException(physicalPath)
        End If
    End Sub

    Protected Sub nwPanelSearchRsltsGrd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles panelSearchRsltsGrd.PageIndexChanging
        GetPanels()
        panelSearchRsltsGrd.PageIndex = e.NewPageIndex
        panelSearchRsltsGrd.DataBind()

    End Sub

    Sub FindFiles(ByVal Path As String)

        Dim physicalPath As String = Path
        Dim directories As New System.IO.DirectoryInfo(physicalPath)
        Dim myDir As IO.DirectoryInfo

        For Each myDir In directories.GetDirectories
            If myDir.GetDirectories.Length > 0 Then
                FindFiles(myDir.FullName & "\")
            End If
        Next
    End Sub

    Public Sub GetFiles(ByVal path As String)
        '7/28/16 eb
        'Was doing the find path incorrectly here. Made changes to include the ~/ to get the root then the given file path...
        Dim myPath As String = Server.MapPath("~/" + path)
        If File.Exists(myPath) Then
            ProcessFile(myPath)
        ElseIf Directory.Exists(myPath) Then
            ProcessDirectory(myPath)
        End If
    End Sub

    Public Sub ProcessDirectory(ByVal targetDirectory As String)
        Dim fileEntries As String() = Directory.GetFiles(targetDirectory)
        For Each fileName As String In fileEntries
            ProcessFile(fileName)
        Next

        Dim subdirectoryEntries As String() = Directory.GetDirectories(targetDirectory)
        For Each subdirectory As String In subdirectoryEntries
            ProcessDirectory(subdirectory)
        Next
    End Sub


    Public Sub ProcessFile(ByVal path As String)
        Dim fi As New FileInfo(path)
        Dim companyNameStr As String = nwCompanyNameTxt.Text.Trim

        If String.IsNullOrEmpty(nwCompanyNameTxt.Text) = False Then
            If fi.Name.ToLower.Contains(companyNameStr) And fi.FullName.ToLower.Contains(".pdf") Then

                Dim dr As DataRow = nwPanelURLs.NewRow()
                dr("FileName") = fi.Name
                dr("FileDir") = fi.FullName.Replace("D:\Hosting\6546477\html", "")
                dr("UploadDate") = fi.LastWriteTime.ToShortDateString
                nwPanelURLs.Rows.Add(dr)
            End If
        Else
            If fi.FullName.ToLower.Contains(".pdf") Then

                Dim dr As DataRow = nwPanelURLs.NewRow()
                dr("FileName") = fi.Name
                dr("FileDir") = fi.FullName.Replace("D:\Hosting\6546477\html", "")
                dr("UploadDate") = fi.LastWriteTime.ToShortDateString
                nwPanelURLs.Rows.Add(dr)
            End If
        End If
    End Sub

    Sub NewSearch()

        searchResultsPanel.Visible = False
        nwPanelSearchPanel.Visible = True

    End Sub

    Sub ClearForm()

        nwCompanyNameTxt.Text = ""
        GetPanels()

    End Sub

    Protected Sub panelSearchRsltsGrd_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles panelSearchRsltsGrd.RowCommand

        If StrComp(e.CommandName, "Page") <> 0 Then

            Dim file_ As String = e.CommandArgument
            Dim iPartyID As Integer = Session("PartyID")
            Dim sCarrierID As String = Convert.ToString(Session("CarrierID"))
            Dim sCustAccountID As String = Convert.ToString(Session("CustAccountID"))

            Dim iInsPartyID As String = Convert.ToString(Session("InsPartyID"))
            Dim motorists As String = Convert.ToString(Session("MotoristsSub"))
            If iInsPartyID = "5674" Then
                Dim popupScript As String = "<script language=javascript> window.open('" & "/PANELS/" & iInsPartyID.ToString & "/" & file_ & "') </script>"
                ClientScript.RegisterStartupScript(Me.[GetType](), "callpopup", popupScript)
                '10/20/16 this next else if is specifically for the demo site capability upgrade
            ElseIf iInsPartyID = "45516" Then
                Dim popupScript As String = "<script language=javascript> window.open('" & "/PANELS/DemoPanels/" & file_ & "') </script>"
                ClientScript.RegisterStartupScript(Me.[GetType](), "callpopup", popupScript)
                '9/16/16 eb I had to put in this if statement due to the way motorists is set up and how it was trying to open the party id/party id
                'Now it opens party id/ cust account id / file name.
            ElseIf iPartyID = "388046" Then '11/7/16 update for catholic fixing thier workaround they seperated both when it wasn't required
                Dim popupScript As String = "<script language=javascript> window.open('" & "/PANELS/388046/" & file_ & "') </script>"
                ClientScript.RegisterStartupScript(Me.[GetType](), "callpopup", popupScript)
            ElseIf iPartyID = "15900" Then '11/8/16 update for the fact or statements are a pain.
                Dim popupScript As String = "<script language=javascript> window.open('" & "/PANELS/388046/" & file_ & "') </script>"
                ClientScript.RegisterStartupScript(Me.[GetType](), "callpopup", popupScript)
            ElseIf iInsPartyID = "4097" Then
                Dim popupScript As String = "<script language=javascript> window.open('" & "/PANELS/" & iInsPartyID.ToString & "/" & motorists & "/" & file_ & "') </script>"
                ClientScript.RegisterStartupScript(Me.[GetType](), "callpopup", popupScript)
                'Below is for callos. Has a different folder structure than normal.
            ElseIf iPartyID = "15576" Then
                Dim popupScript As String = "<script language=javascript> window.open('" & "/PANELS/" & iPartyID.ToString & "/" & file_ & "') </script>"
                ClientScript.RegisterStartupScript(Me.[GetType](), "callpopup", popupScript)
            ElseIf StrComp(Convert.ToString(Session("InsCarrier")), "I") = 0 Then
                Dim popupScript As String = "<script language=javascript> window.open('" & "/PANELS/" & iPartyID.ToString & "/" & sCarrierID & "/" & file_ & "') </script>"
                ClientScript.RegisterStartupScript(Me.[GetType](), "callpopup", popupScript)

            ElseIf StrComp(Convert.ToString(Session("InsCarrier")), "E") = 0 Then
                If StrComp(Convert.ToString(Session("MultipleLocs")), "1") = 0 Then ' no multiple locs
                    Dim popupScript As String = "<script language=javascript> window.open('" & "/PANELS/" & iPartyID.ToString & "/" & file_ & "') </script>"
                    ClientScript.RegisterStartupScript(Me.[GetType](), "callpopup", popupScript)
                Else
                    Dim popupScript As String = "<script language=javascript> window.open('" & "/PANELS/" & iPartyID.ToString & "/" & sCustAccountID & "/" & file_ & "') </script>"
                    ClientScript.RegisterStartupScript(Me.[GetType](), "callpopup", popupScript)
                End If
                '7/19/16 10:22 am changed 1 to Y i don't even think the inscarrier can be 1 for motorists and is set to Y in the log in search.
                '10:31 ok some reason it needs to be 1 for the whole drop down. this is getting beyond confusing.
                '10:43 copied in the code for the filling the table
            ElseIf StrComp(Convert.ToString(Session("InsCarrier")), "1") = 0 Then


                Dim popupScript As String = "<script language=javascript> window.open('" & "/PANELS/" & iInsPartyID.ToString & "/" & Session("MotoristsSub") & "/" & file_ & "') </script>"
                ClientScript.RegisterStartupScript(Me.[GetType](), "callpopup", popupScript)
            Else
                Dim popupScript As String = "<script language=javascript> window.open('" & "/PANELS/" & iPartyID.ToString & "/" & file_ & "') </script>"
                ClientScript.RegisterStartupScript(Me.[GetType](), "callpopup", popupScript)


            End If




        End If

    End Sub

    Protected Sub panelSearchRsltsGrd_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles panelSearchRsltsGrd.RowDataBound

    End Sub
End Class
