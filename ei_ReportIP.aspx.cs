﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web;
using Telerik.Web.UI;
using System.Net.Mail;

public partial class ei_ReportIP : Telerik.Web.UI.RadAjaxPage
{
    private System.Web.UI.RenderMethod _onRenderDelegate;
    #region IRadAjaxPage Members
    public void AttachOnRender(System.Web.UI.RenderMethod renderMethod)
    {
        _onRenderDelegate = renderMethod;
    }
    #endregion
    protected override void Render(System.Web.UI.HtmlTextWriter writer)
    {
        if (_onRenderDelegate != null)
        {
            _onRenderDelegate(writer, this);
        }
        base.Render(writer);
    }
    //---------------------------------

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            if (Convert.ToString(Session["canceled_intake"]) == "1")
            {
                srch_ip_fname_txt.Text = Convert.ToString(Session["srch_ip_fname_txt"]);
                srch_ip_lname_txt.Text = Convert.ToString(Session["srch_ip_lname_txt"]);
                srch_ssn1_txt.Text = Convert.ToString(Session["srch_ssn1_txt"]);
                srch_ssn2_txt.Text = Convert.ToString(Session["srch_ssn2_txt"]);
                srch_ssn2_txt.Text = Convert.ToString(Session["srch_ssn2_txt"]);

                Session["canceled_intake"] = 0;
            }
           // ip_name_serch_grd.Visible = true; 
            init_ip_search();
        }
    }

    protected void clear_ip_srch_fields(object sender, EventArgs e)
    {
        srch_ip_fname_txt.Text = "";
        srch_ip_lname_txt.Text = "";
        srch_ssn1_txt.Text = "";
        srch_ssn2_txt.Text = "";
        srch_ssn3_txt.Text = "";

        init_ip_search();
    }

    protected void run_ip_search(object sender, EventArgs e)
    {
        init_ip_search();
    }

    protected void init_ip_search()
    {
        string iPartyID = Convert.ToString(Session["PartyID"]);
        string dDOIEndRangeDate = ReverseDate(DateTime.Now.AddYears(1).ToShortDateString());
        string dDOEEndRangeDate = ReverseDate(DateTime.Now.AddYears(1).ToShortDateString());
        string sBuiltQuery = "";
        string sSocialSecNum = "";
        bool bSSEntered = true;


        if (string.IsNullOrEmpty(srch_ip_fname_txt.Text.Trim()) == false)
        {
            sBuiltQuery = " AND HP_IP.PERSON_FIRST_NAME LIKE '%" + srch_ip_fname_txt.Text.ToUpper().Trim() + "%' ";
        }

        if (string.IsNullOrEmpty(srch_ip_lname_txt.Text.Trim()) == false)
        {
            sBuiltQuery = sBuiltQuery + " AND HP_IP.PERSON_LAST_NAME LIKE '%" + srch_ip_lname_txt.Text.ToUpper().Trim() + "%' ";
        }


        if (string.IsNullOrEmpty(srch_ssn1_txt.Text) == false || string.IsNullOrEmpty(srch_ssn2_txt.Text) == false || string.IsNullOrEmpty(srch_ssn3_txt.Text) == false)
        {
            if (srch_ssn1_txt.Text.Trim().Length != 0)
            {
                sSocialSecNum = srch_ssn1_txt.Text.Trim() + "-";
            }
            else
            {
                sSocialSecNum = "%-";
                bSSEntered = false;
            }

            if (srch_ssn2_txt.Text.Trim().Length != 0)
            {
                sSocialSecNum += srch_ssn2_txt.Text.Trim() + "-";
            }
            else
            {
                sSocialSecNum += "%-";
                bSSEntered = false;
            }

            if (srch_ssn3_txt.Text.Trim().Length != 0)
            {
                sSocialSecNum += srch_ssn3_txt.Text.Trim();

            }
            else
            {
                sSocialSecNum = sSocialSecNum + "%";
                bSSEntered = false;
            }

            if (bSSEntered)
            {
                sBuiltQuery += " AND HP_IP.JGZZ_FISCAL_CODE = '" + sSocialSecNum + "' ";
            }
            else
            {
                sBuiltQuery += " AND HP_IP.JGZZ_FISCAL_CODE LIKE '" + sSocialSecNum + "' ";
            }
        }


        DataTable dt = new DataTable();
        string sSqlQuery = "";

        if (Convert.ToInt32(Session["PartyID"]) != Convert.ToInt32(Session["InsPartyID"]))
        {
            sSqlQuery = "SELECT DISTINCT(HP_IP.PARTY_ID), HP_IP.PARTY_NAME, HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME, HP_IP.PERSON_LAST_NAME, HL.ADDRESS1, "
                      + "      HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE, HPP.JGZZ_FISCAL_CODE, HPP.DATE_OF_BIRTH "
                      + "FROM HZ_CUST_ACCOUNTS HCA_IP "
                      + "       INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_EMP ON HCARA_EMP.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID "
                      + "       INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCA_EMP.CUST_ACCOUNT_ID = HCARA_EMP.RELATED_CUST_ACCOUNT_ID "
                      + "       INNER JOIN HZ_PARTIES HP_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID "
                      + "       INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA_IP.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                      + "       INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                      + "       INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                      + "       INNER JOIN HZ_PERSON_PROFILES HPP ON HP_IP.PARTY_ID = HPP.PARTY_ID "
                      + "WHERE HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT' AND "
                      //+ "       HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' AND "
                      + "       HCA_EMP.PARTY_ID = " + iPartyID + " AND "
                      + "       HCARA_EMP.STATUS = 'A' AND "
                      + "       HPP.EFFECTIVE_END_DATE IS NULL "//AND "
                      //+ "       HP_IP.PARTY_ID > 0 "
                      + sBuiltQuery
                      + "GROUP BY HP_IP.PARTY_ID, HP_IP.PARTY_NAME "
                      + "ORDER BY HP_IP.PERSON_LAST_NAME, HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME DESC";

            //string strConnection = "Driver={MySQL ODBC 5.3 Unicode Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
            //OdbcConnection objConn = new OdbcConnection(strConnection);
            OdbcConnection objConn = new OdbcConnection(ConfigurationManager.ConnectionStrings["mysql"].ConnectionString);
            OdbcDataAdapter objDataAdapter = new OdbcDataAdapter(sSqlQuery, objConn);
            
            try
            {
                objConn.Open();
                objDataAdapter.Fill(dt);
                ip_name_serch_grd.DataSource = dt;
                ip_name_serch_grd.DataBind();

                if (dt.Rows.Count > 0)
                {
                    ip_name_serch_grd.CurrentPageIndex = 0;
                    ip_name_serch_grd.DataBind();
                }
                else
                {
                    select_an_acct_pnl.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objConn.Close();
                objDataAdapter.Dispose();
                objConn.Dispose();
            }
        }
        else
        {
            sSqlQuery = "SELECT DISTINCT(HP_IP.PARTY_ID), HP_IP.PARTY_NAME, HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME, HP_IP.PERSON_LAST_NAME, HL.ADDRESS1, "
                        + "      HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE,  HPP.JGZZ_FISCAL_CODE, HPP.DATE_OF_BIRTH "
                        + "FROM HZ_CUST_ACCOUNTS HCA_IP "
                        + "     INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_EMP ON HCARA_EMP.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID "
                        + "     INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCA_EMP.CUST_ACCOUNT_ID = HCARA_EMP.RELATED_CUST_ACCOUNT_ID "
                        + "     INNER JOIN HZ_PARTIES HP_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID "
                        + "     INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA_IP.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                        + "     INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                        + "     INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                        + "     INNER JOIN HZ_PERSON_PROFILES HPP ON HP_IP.PARTY_ID = HPP.PARTY_ID "
                        + "     INNER JOIN (SELECT HP.PARTY_ID "
                        + "                 FROM HZ_CUST_ACCOUNTS HCA_INS INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_INS.CUST_ACCOUNT_ID = HCARA.RELATED_CUST_ACCOUNT_ID "
                        + "                     INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID "
                        + "                     INNER JOIN HZ_PARTIES HP ON HCA_EMP.PARTY_ID = HP.PARTY_ID "
                        + "                 WHERE HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' "
                        + "                         AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                        + "                         AND HCARA.STATUS = 'A' "
                        + "                         AND HCA_INS.PARTY_ID = " + Convert.ToString(Session["InsPartyID"]) + " "
                        + "                         AND HCA_EMP.STATUS = 'A') REL_ACCTS ON HCA_EMP.PARTY_ID = REL_ACCTS.PARTY_ID "
                        + "WHERE HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT' AND "
                        + "      HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' AND "
                        + "      HCARA_EMP.STATUS = 'A' AND "
                        + "      HPP.EFFECTIVE_END_DATE IS NULL"// AND "
                        //+ "      HP_IP.PARTY_ID > 0 "
                        + sBuiltQuery
                        + "GROUP BY HP_IP.PARTY_ID, HP_IP.PARTY_NAME "
                        + "ORDER BY HP_IP.PERSON_LAST_NAME, HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME DESC";

            ////EDH select_an_acct_pnl.Visible = true;
            //////EDH employee_search_pnl.Visible = false;

            //}
            //Response.Write(sSqlQuery);
            //took below out of the query to  pull all emplyees in DB, not just injuries
            //AND " & _
            //"HCA_IP.ATTRIBUTE9 IS NOT NULL 
            //string strConnection = "Driver={MySQL ODBC 5.3 Unicode Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
            //OdbcConnection objConn = new OdbcConnection(strConnection);
            OdbcConnection objConn = new OdbcConnection(ConfigurationManager.ConnectionStrings["mysql"].ConnectionString);
            OdbcDataAdapter objDataAdapter = new OdbcDataAdapter(sSqlQuery, objConn);

            try
            {
                objConn.Open();
                objDataAdapter.Fill(dt);

                ip_name_serch_grd.DataSource = dt;
                ip_name_serch_grd.DataBind();

                if (dt.Rows.Count > 0)
                {
                    ip_name_serch_grd.CurrentPageIndex = 0;
                    ip_name_serch_grd.DataBind();
                }
                else
                {
                    select_an_acct_pnl.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objConn.Close();
                objDataAdapter.Dispose();
                objConn.Dispose();
            }
        }
    }


    protected void ip_name_serch_grd_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem item = (GridDataItem)e.Item;
            string address = myCStr(item["ADDRESS1"].Text).Trim();
            string ssn = myCStr(item["JGZZ_FISCAL_CODE"].Text).Replace("-","").Trim();
            string dob = myCStr(item["DATE_OF_BIRTH"].Text).Replace(" 12:00:00 AM", "").Trim();

            if (myCStr(item["ADDRESS2"].Text).Trim().Length != 0)
            {
                address += ", " + myCStr(item["ADDRESS2"].Text).Trim();
            }

            if (string.IsNullOrEmpty(myCStr(item["CITY"].Text).Trim()) == false)
            {
                address += " " + myCStr(item["CITY"].Text).Trim() + ", ";
            }

            if (string.IsNullOrEmpty(myCStr(item["STATE"].Text).Trim()) == false)
            {
                address += " " + myCStr(item["STATE"].Text).Trim();
            }

            if (string.IsNullOrEmpty(myCStr(item["POSTAL_CODE"].Text).Trim()) == false)
            {
                address += " " + myCStr(item["POSTAL_CODE"].Text).Trim();
            }

            item["ADDRESS1"].Text = address;

            if (ssn.Length > 5)
            {
                ssn = "XXX-XX-" + ssn.Substring(5);
            }
            else
            {
                ssn = "XXX-XX-XXXX";
            }

            item["DATE_OF_BIRTH"].Text = dob;
            item["JGZZ_FISCAL_CODE"].Text = ssn;

        }

        
    }

    protected void ip_name_serch_grd_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        string iPartyID = Convert.ToString(Session["PartyID"]);
        string dDOIEndRangeDate = ReverseDate(DateTime.Now.AddYears(1).ToShortDateString());
        string dDOEEndRangeDate = ReverseDate(DateTime.Now.AddYears(1).ToShortDateString());
        string sBuiltQuery = "";
        string sSocialSecNum = "";
        bool bSSEntered = true;


        if (string.IsNullOrEmpty(srch_ip_fname_txt.Text.Trim()) == false)
        {
            sBuiltQuery = " AND HP_IP.PERSON_FIRST_NAME LIKE '%" + srch_ip_fname_txt.Text.ToUpper().Trim() + "%' ";
        }

        if (string.IsNullOrEmpty(srch_ip_lname_txt.Text.Trim()) == false)
        {
            sBuiltQuery = sBuiltQuery + " AND HP_IP.PERSON_LAST_NAME LIKE '%" + srch_ip_lname_txt.Text.ToUpper().Trim() + "%' ";
        }


        if (string.IsNullOrEmpty(srch_ssn1_txt.Text) == false | string.IsNullOrEmpty(srch_ssn2_txt.Text) == false | string.IsNullOrEmpty(srch_ssn3_txt.Text) == false)
        {
            if (srch_ssn1_txt.Text.Trim().Length != 0)
            {
                sSocialSecNum = srch_ssn1_txt.Text.Trim() + "-";
            }
            else
            {
                sSocialSecNum = "%-";
                bSSEntered = false;
            }

            if (srch_ssn2_txt.Text.Trim().Length != 0)
            {
                sSocialSecNum += srch_ssn2_txt.Text.Trim() + "-";
            }
            else
            {
                sSocialSecNum += "%-";
                bSSEntered = false;
            }

            if (srch_ssn3_txt.Text.Trim().Length != 0)
            {
                sSocialSecNum += srch_ssn3_txt.Text.Trim();

            }
            else
            {
                sSocialSecNum = sSocialSecNum + "%";
                bSSEntered = false;
            }

            if (bSSEntered)
            {
                sBuiltQuery += " AND HP_IP.JGZZ_FISCAL_CODE = '" + sSocialSecNum + "' ";
            }
            else
            {
                sBuiltQuery += " AND HP_IP.JGZZ_FISCAL_CODE LIKE '" + sSocialSecNum + "' ";
            }

        }

        //If String.IsNullOrEmpty(searchClaimTxt.Text.Trim) = False Then
        // sBuiltQuery = sBuiltQuery & " AND HCA_IP.ACCOUNT_NUMBER LIKE '%" & searchClaimTxt.Text & "%' "
        // End If

        DataTable dt = new DataTable();
        string sSqlQuery = "";


        //if (Convert.ToString(Session["InsCarrier"]) == "N" && Convert.ToInt32(Session["PartyID"]) != Convert.ToInt32(Session["InsPartyID"]))
        if ( Convert.ToInt32(Session["PartyID"]) != Convert.ToInt32(Session["InsPartyID"]))
        {
            sSqlQuery = "SELECT DISTINCT(HP_IP.PARTY_ID), HP_IP.PARTY_NAME, HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME, HP_IP.PERSON_LAST_NAME, HL.ADDRESS1, "
                      + "      HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE, HPP.JGZZ_FISCAL_CODE, HPP.DATE_OF_BIRTH "
                      + "FROM HZ_CUST_ACCOUNTS HCA_IP "
                      + "       INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_EMP ON HCARA_EMP.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID "
                      + "       INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCA_EMP.CUST_ACCOUNT_ID = HCARA_EMP.RELATED_CUST_ACCOUNT_ID "
                      + "       INNER JOIN HZ_PARTIES HP_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID "
                      + "       INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA_IP.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                      + "       INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                      + "       INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                      + "       INNER JOIN HZ_PERSON_PROFILES HPP ON HP_IP.PARTY_ID = HPP.PARTY_ID "
                      + "WHERE HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT' AND "
                      + "       HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' AND "
                      + "       HCA_EMP.PARTY_ID = " + iPartyID + " AND "
                      + "       HCARA_EMP.STATUS = 'A' AND "
                      + "       HPP.EFFECTIVE_END_DATE IS NULL AND "
                      + "       HP_IP.PARTY_ID > 0 "
                      + sBuiltQuery
                      + "GROUP BY HP_IP.PARTY_ID, HP_IP.PARTY_NAME "
                      + "ORDER BY HP_IP.PERSON_LAST_NAME, HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME DESC";

            //string strConnection = "Driver={MySQL ODBC 5.3 Unicode Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
            //ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()
            //OdbcConnection objConn = new OdbcConnection(strConnection);
            OdbcConnection objConn = new OdbcConnection(ConfigurationManager.ConnectionStrings["mysql"].ConnectionString);
            OdbcDataAdapter objDataAdapter = new OdbcDataAdapter(sSqlQuery, objConn);

            try
            {
                objConn.Open();
                objDataAdapter.Fill(dt);

                ip_name_serch_grd.DataSource = dt;
                ip_name_serch_grd.CurrentPageIndex = e.NewPageIndex;
                ip_name_serch_grd.DataBind();

                if (dt.Rows.Count > 0)
                {
                    ip_name_serch_grd.CurrentPageIndex = 0;
                    ip_name_serch_grd.DataBind();
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objConn.Close();
                objDataAdapter.Dispose();
                objConn.Dispose();
            }
        }
        /*else
        {
            sSqlQuery = "SELECT DISTINCT(HP_IP.PARTY_ID), HP_IP.PARTY_NAME, HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME, HP_IP.PERSON_LAST_NAME, HL.ADDRESS1, "
                        + "      HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE,  HPP.JGZZ_FISCAL_CODE, HPP.DATE_OF_BIRTH "
                        + "FROM HZ_CUST_ACCOUNTS HCA_IP "
                        + "     INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_EMP ON HCARA_EMP.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID "
                        + "     INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCA_EMP.CUST_ACCOUNT_ID = HCARA_EMP.RELATED_CUST_ACCOUNT_ID "
                        + "     INNER JOIN HZ_PARTIES HP_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID "
                        + "     INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA_IP.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                        + "     INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                        + "     INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                        + "     INNER JOIN HZ_PERSON_PROFILES HPP ON HP_IP.PARTY_ID = HPP.PARTY_ID "
                        + "     INNER JOIN (SELECT HP.PARTY_ID "
                        + "                 FROM HZ_CUST_ACCOUNTS HCA_INS INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_INS.CUST_ACCOUNT_ID = HCARA.RELATED_CUST_ACCOUNT_ID "
                        + "                     INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID "
                        + "                     INNER JOIN HZ_PARTIES HP ON HCA_EMP.PARTY_ID = HP.PARTY_ID "
                        + "                 WHERE HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' "
                        + "                         AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                        + "                         AND HCARA.STATUS = 'A' "
                        + "                         AND HCA_INS.PARTY_ID = " + Convert.ToString(Session["InsPartyID"]) + " "
                        + "                         AND HCA_EMP.STATUS = 'A') REL_ACCTS ON HCA_EMP.PARTY_ID = REL_ACCTS.PARTY_ID "
                        + "WHERE HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT' AND "
                        + "      HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' AND "
                        + "      HCARA_EMP.STATUS = 'A' AND "
                        + "      HPP.EFFECTIVE_END_DATE IS NULL AND "
                        + "      HP_IP.PARTY_ID > 0 "
                        + sBuiltQuery
                        + "GROUP BY HP_IP.PARTY_ID, HP_IP.PARTY_NAME "
                        + "ORDER BY HP_IP.PERSON_LAST_NAME, HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME DESC";
        }
        //Response.Write(sSqlQuery);
        //took below out of the query to  pull all emplyees in DB, not just injuries
        //AND " & _
        //"HCA_IP.ATTRIBUTE9 IS NOT NULL 
        string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        //ConfigurationManager.ConnectionStrings("MySQL_conn").ToString()
        OdbcConnection objConn = new OdbcConnection(strConnection);
        OdbcDataAdapter objDataAdapter = new OdbcDataAdapter(sSqlQuery, objConn);
        OdbcCommand objCmd = null;

        try
        {
            objConn.Open();
            objDataAdapter.Fill(dt);

            ip_name_serch_grd.DataSource = dt;
            ip_name_serch_grd.CurrentPageIndex = e.NewPageIndex;
            ip_name_serch_grd.DataBind();

            if (dt.Rows.Count > 0)
            {
                ip_name_serch_grd.CurrentPageIndex = 0;
                ip_name_serch_grd.DataBind();
            }


        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            objConn.Close();
            objDataAdapter.Dispose();
            objConn.Dispose();
        }*/
    }

    protected void add_new_ip(object sender, EventArgs e)
    {
        employee_search_pnl.Visible = false;
        injury_report_pnl.Visible = true;
    }

    protected void ip_name_serch_grd_ItemCommand(Object sender, GridCommandEventArgs e)
    {
        if (e.Item.ItemType != GridItemType.Pager)
        {
            employee_search_pnl.Visible = false;
            injury_report_pnl.Visible = true;
            
            ip_name_serch_grd.Visible = false;

            OdbcCommand objSqlCmd = new OdbcCommand();
            //string strConnection = "Driver={MySQL ODBC 5.3 Unicode Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
            // OdbcConnection conn = new OdbcConnection(strConnection);
            OdbcConnection conn = new OdbcConnection(ConfigurationManager.ConnectionStrings["mysql"].ConnectionString);

            string party_id = Convert.ToString(e.CommandArgument);
            try
            {

                conn.Open();

                string sql = "SELECT  HP.PERSON_FIRST_NAME, HP.PERSON_LAST_NAME, HP.PERSON_NAME_SUFFIX, HL.ADDRESS1, HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE,  "
                    + "	        PRIMARY_PHONE_AREA_CODE, PRIMARY_PHONE_NUMBER, PRIMARY_PHONE_EXTENSION, HPP.DATE_OF_BIRTH, HPP.JGZZ_FISCAL_CODE "
                    + "		 FROM HZ_PARTIES HP INNER JOIN HZ_CUST_ACCOUNTS HCA ON HP.PARTY_ID = HCA.PARTY_ID  "
                    + "			INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                    + "			INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                    + "			INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                    + "			INNER JOIN HZ_PERSON_PROFILES HPP ON HP.PARTY_ID = HPP.PARTY_ID "
                    + "		 WHERE HCA.CUST_ACCOUNT_ID = ( "
                    + "									 SELECT CUST_ACCOUNT_ID "
                    + "									 FROM ( "
                    + "											 SELECT HCA.CUST_ACCOUNT_ID, HCA.ACCOUNT_NUMBER, HCA.ACCOUNT_NAME "
                    + "											 FROM HZ_PARTIES HP INNER JOIN HZ_CUST_ACCOUNTS HCA ON HP.PARTY_ID = HCA.PARTY_ID "
                    + "											 WHERE HP.PARTY_ID =  " + party_id + " "
                    + "											 ORDER BY HCA.CUST_ACCOUNT_ID DESC "
                    + "									 ) AS T LIMIT 1 "
                    + "									 )  AND "
                    + "		HPP.EFFECTIVE_END_DATE IS NULL LIMIT 1";
                // Response.Write(sql);
                OdbcCommand cmd = new OdbcCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                OdbcDataReader dr3 = cmd.ExecuteReader();                   

                if(dr3.Read()){

                    TextBox ip_fname_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_fname_txt");
                    TextBox ip_lname_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_lname_txt");
                    TextBox ip_suffix_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_suffix_txt");
                    TextBox ip_address_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_address_txt");
                    TextBox ip_address2_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_address2_txt");
                    TextBox ip_city_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_city_txt");
                    TextBox ip_state_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_state_txt");
                    TextBox ip_zip_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_zip_txt");
                    TextBox ip_area_code_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_area_code_txt");
                    TextBox ip_phone_num_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_phone_num_txt");
                    TextBox ip_ext_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_ext_txt");
                    TextBox ip_ssn1_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_ssn1_txt");
                    TextBox ip_ssn2_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_ssn2_txt");
                    TextBox ip_ssn3_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_ssn3_txt");
                    TextBox ip_dob_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_dob_txt");

                    ip_fname_txt.Text = myCStr(dr3.GetValue(0));
                    ip_lname_txt.Text = myCStr(dr3.GetValue(1));
                    ip_suffix_txt.Text = myCStr(dr3.GetValue(2));
                    ip_address_txt.Text = myCStr(dr3.GetValue(3));
                    ip_address2_txt.Text = myCStr(dr3.GetValue(4));
                    ip_city_txt.Text = myCStr(dr3.GetValue(5));
                    ip_state_txt.Text = myCStr(dr3.GetValue(6));
                    ip_zip_txt.Text = myCStr(dr3.GetValue(7));
                    ip_area_code_txt.Text = myCStr(dr3.GetValue(8));
                    ip_phone_num_txt.Text = myCStr(dr3.GetValue(9));
                    ip_ext_txt.Text = myCStr(dr3.GetValue(10));
                    ip_dob_txt.Text = myCStr(dr3.GetValue(11)).Replace(" 12:00:00 AM", "").Trim();

                    /*ip_ssn1_txt.Text = myCStr(dr3.GetValue(0));
                    ip_ssn2_txt.Text = myCStr(dr3.GetValue(0));
                    ip_ssn3_txt.Text = myCStr(dr3.GetValue(0));*/

                    injury_report_pnl.Visible = true;
                    ip_name_serch_grd.Visible = false;
                }

                cmd.Dispose();

            }
            catch (Exception ex)
            {
                Response.Write("Exception: " + Convert.ToString(ex));
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }
    }

    protected void get_employer_locations()
    {
        DropDownList employer_loc_drp = (DropDownList)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("employer_loc_drp");

        OdbcCommand objSqlCmd = new OdbcCommand();
        //string strConnection = "Driver={MySQL ODBC 5.3 Unicode Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        //OdbcConnection conn = new OdbcConnection(strConnection);
        OdbcConnection conn = new OdbcConnection(ConfigurationManager.ConnectionStrings["mysql"].ConnectionString);

        try
        {

            conn.Open();

            string sql = "SELECT CUST_ACCOUNT_ID "
                        + " FROM ACCOUNT_PRIVS "
                        + " WHERE IUSERID = " + Convert.ToString(Session["UserID"])
                        + " ORDER BY CUST_ACCOUNT_ID ";
            //Response.Write(sql);
            OdbcCommand cmd = new OdbcCommand(sql, conn);
            cmd.CommandType = CommandType.Text;
            OdbcDataReader dr3 = cmd.ExecuteReader();

            if (dr3.Read())
            {
                //if (Convert.ToInt32(dr3.GetValue(0)) == 0)
                //{                    
                    ListItem item = new ListItem("", "");
                    DataTable dt = new DataTable();
                    OdbcDataAdapter adapter = new OdbcDataAdapter();
                    adapter.SelectCommand = new OdbcCommand("SELECT CUST_ACCOUNT_ID, ACCOUNT_NAME"
                                                            + " FROM HZ_CUST_ACCOUNTS "
                                                            + " WHERE PARTY_ID = " + Convert.ToString(Session["PartyID"])
                                                            + "       AND STATUS = 'A' "
                                                            + " ORDER BY ACCOUNT_NAME ", conn);
                    
                    adapter.Fill(dt);

                    employer_loc_drp.Items.Clear();
                    employer_loc_drp.DataSource = dt;
                    employer_loc_drp.DataTextField = "ACCOUNT_NAME";
                    employer_loc_drp.DataValueField = "CUST_ACCOUNT_ID";
                    employer_loc_drp.DataBind();

                    employer_loc_drp.Items.Insert(0, item);
                    employer_loc_drp.SelectedIndex = 0;
                    item = new ListItem("OTHER LOCATION", "-99");
                    employer_loc_drp.Items.Insert(employer_loc_drp.Items.Count, item);
                //}
                //else
                //{

                //}
            }
        }
        catch (Exception ex)
        {
            Response.Write("Exception: " + Convert.ToString(ex));
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }
    }

    protected void get_insurance_carrier()
    {
        DropDownList employer_loc_drp = (DropDownList)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("employer_loc_drp");
        DropDownList ins_carrier_loc_drp = (DropDownList)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("ins_carrier_loc_drp");

        OdbcCommand objSqlCmd = new OdbcCommand();
        //string strConnection = "Driver={MySQL ODBC 5.3 Unicode Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        // OdbcConnection conn = new OdbcConnection(strConnection);
        OdbcConnection conn = new OdbcConnection(ConfigurationManager.ConnectionStrings["mysql"].ConnectionString);

        string emp_cust_id = "";
        try
        {

            conn.Open();

            if (employer_loc_drp.SelectedValue == "-99")
                emp_cust_id = employer_loc_drp.Items[1].Value;
            else
                emp_cust_id = employer_loc_drp.SelectedValue;
            
            //if (Convert.ToInt32(Session["PartyID"]) != 0)
            //{
               
                ListItem item = new ListItem("", "");
                DataTable dt = new DataTable();
                OdbcDataAdapter adapter = new OdbcDataAdapter();
                adapter.SelectCommand = new OdbcCommand("SELECT HCA_INS.CUST_ACCOUNT_ID, HCA_INS.ACCOUNT_NAME"
                                                        + " FROM HZ_CUST_ACCOUNTS HCA_EMP " 
                                                        + "      INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_EMP.CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                                                        + "      INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID "
                                                        + " WHERE HCA_EMP.CUST_ACCOUNT_ID = " + emp_cust_id
                                                        + "       AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                                                        + "       AND HCARA.STATUS = 'A' "
                                                        + "       AND HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' "
                                                        + "       AND HCA_INS.STATUS = 'A' "
                                                        + " ORDER BY HCA_INS.ACCOUNT_NAME ", conn);

                adapter.Fill(dt);

                ins_carrier_loc_drp.Items.Clear();
                ins_carrier_loc_drp.DataSource = dt;
                ins_carrier_loc_drp.DataTextField = "ACCOUNT_NAME";
                ins_carrier_loc_drp.DataValueField = "CUST_ACCOUNT_ID";
                ins_carrier_loc_drp.DataBind();

                item = new ListItem("OTHER CARRIER", "-199");
                ins_carrier_loc_drp.Items.Insert(ins_carrier_loc_drp.Items.Count, item);

            /*}
            else
            {
                //get the account info for the carrier that is logged in
            }*/
            
        }
        catch (Exception ex)
        {
            Response.Write("Exception: " + Convert.ToString(ex));
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }
    }

    protected void cancel_injury_report(object sender, EventArgs e)
    {
        Session["canceled_intake"] = 1;
        Session["srch_ip_fname_txt"] = srch_ip_fname_txt.Text;
        Session["srch_ip_lname_txt"] = srch_ip_lname_txt.Text;
        Session["srch_ssn1_txt"] = srch_ssn1_txt.Text;
        Session["srch_ssn2_txt"] = srch_ssn2_txt.Text;
        Session["srch_ssn2_txt"] = srch_ssn2_txt.Text;

        Response.Redirect("ei_ReportIP.aspx");
        
    }

    protected void go_to_next_step(object sender, EventArgs e)
    {
        int selectedIndex = report_injury_pnl_bar.SelectedItem.Index;
        bool conditions_passed = true;

        TableRow patient_info_btn_row = (TableRow)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("patient_info_btn_row");
        TableRow employer_info_btn_row = (TableRow)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("employer_info_btn_row");
        TableRow insurance_info_btn_row = (TableRow)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("insurance_info_btn_row");
        TableRow injury_info_btn_row = (TableRow)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("injury_info_btn_row");
        
        RadPanelItem patient_info_pnl_item_prnt = (RadPanelItem)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item_prnt");
        RadPanelItem employer_info_pnl_item_prnt = (RadPanelItem)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item_prnt");
        RadPanelItem ins_carrier_pnl_item_prnt = (RadPanelItem)report_injury_pnl_bar.FindItemByValue("ins_carrier_pnl_item_prnt");


        ImageButton injury_info_submit_btn = (ImageButton)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("injury_info_submit_btn");
        ImageButton injury_info_review_btn = (ImageButton)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("injury_info_review_btn");

        if (conditions_passed)
        {
            if (selectedIndex < 3)
            {
                report_injury_pnl_bar.Items[selectedIndex + 1].Selected = true;
                report_injury_pnl_bar.Items[selectedIndex + 1].Expanded = true;
                report_injury_pnl_bar.Items[selectedIndex + 1].Enabled = true;
            }

            switch (selectedIndex)
            {
                case 0:

                    patient_info_btn_row.Visible = false;
                    patient_info_pnl_item_prnt.Expanded = false;
                    get_employer_locations();
                    break;
                case 1:
                    employer_info_btn_row.Visible = false;
                    employer_info_pnl_item_prnt.Expanded = false;
                    get_insurance_carrier();
                    break;
                case 2:
                    insurance_info_btn_row.Visible = false;
                    ins_carrier_pnl_item_prnt.Expanded = false;
                    break;
                case 3:
                    injury_info_review_btn.Visible = false;
                    injury_info_submit_btn.Visible = true;
                    //injury_info_btn_row.Visible = false;
                    patient_info_pnl_item_prnt.Expanded = true;
                    employer_info_pnl_item_prnt.Expanded = true;
                    ins_carrier_pnl_item_prnt.Expanded = true;
                    //submit_intake();
                    break;
            }
        }

    }
    
    protected void employer_loc_drp_chgd(object sender, EventArgs e)
    {
        DropDownList employer_loc_drp = (DropDownList)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("employer_loc_drp");

        TextBox loc_address_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_address_txt");
        TextBox loc_address2_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_address2_txt");
        TextBox loc_city_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_city_txt");
        TextBox loc_state_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_state_txt");
        TextBox loc_zip_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_zip_txt");

        TableRow new_loc_name_row = (TableRow)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("new_loc_name_row");
        TextBox loc_new_loc_name_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_new_loc_name_txt");
        RequiredFieldValidator loc_new_loc_name_txt_rf_validator = (RequiredFieldValidator)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_new_loc_name_txt_rf_validator");
        RadPanelItem ins_carrier_pnl_item_prnt = (RadPanelItem)report_injury_pnl_bar.FindItemByValue("ins_carrier_pnl_item_prnt");
        OdbcCommand objSqlCmd = new OdbcCommand();

        //string strConnection = "Driver={MySQL ODBC 5.3 Unicode Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        //OdbcConnection conn = new OdbcConnection(strConnection);
        OdbcConnection conn = new OdbcConnection(ConfigurationManager.ConnectionStrings["mysql"].ConnectionString);

        loc_address_txt.Text = "";
        loc_address2_txt.Text = "";
        loc_city_txt.Text = "";
        loc_state_txt.Text = "";
        loc_zip_txt.Text = "";
        loc_new_loc_name_txt.Text = "";
        new_loc_name_row.Visible = false;
        loc_new_loc_name_txt_rf_validator.Enabled = false;

        try
        {
            string sql = "";

            conn.Open();
            switch (employer_loc_drp.SelectedValue)
            {

                case "":
                    break;
                case "-99":
                    new_loc_name_row.Visible = true;
                    loc_new_loc_name_txt_rf_validator.Enabled = true;
                    break;
                default:
                    sql = "SELECT  HL.ADDRESS1, HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE "
                        + " FROM  HZ_CUST_ACCOUNTS HCA INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                        + "			INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                        + "			INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                        + " WHERE HCA.CUST_ACCOUNT_ID = " + employer_loc_drp.SelectedValue;

                    //Response.Write(sql);
                    OdbcCommand cmd = new OdbcCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    OdbcDataReader dr3 = cmd.ExecuteReader();

                    if (dr3.Read())
                    {
                        loc_address_txt.Text = myCStr(dr3.GetValue(0));
                        loc_address2_txt.Text = myCStr(dr3.GetValue(1));
                        loc_city_txt.Text = myCStr(dr3.GetValue(2));
                        loc_state_txt.Text = myCStr(dr3.GetValue(3));
                        loc_zip_txt.Text = myCStr(dr3.GetValue(4));
                    }

                    if (ins_carrier_pnl_item_prnt.Enabled)
                        get_insurance_carrier();

                    break;
            }

            
        }
        catch (Exception ex)
        {
            Response.Write("Exception: " + Convert.ToString(ex));
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }

    }

    protected void ins_carrier_drp_chgd(object sender, EventArgs e)
    {
        DropDownList ins_carrier_loc_drp = (DropDownList)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("ins_carrier_loc_drp");

        TextBox new_ins_name_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_name_txt");
        TextBox new_ins_address_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_address_txt");
        TextBox new_ins_address2_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_address2_txt");
        TextBox new_ins_city_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_city_txt");
        TextBox new_ins_state_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_state_txt");
        TextBox new_ins_zip_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_zip_txt");

        Table new_loc_ins_tbl = (Table)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_loc_ins_tbl");
        RequiredFieldValidator new_ins_name_txt_rf_validator = (RequiredFieldValidator)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_name_txt_rf_validator");
        RequiredFieldValidator new_ins_phone_rf_validator = (RequiredFieldValidator)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_phone_rf_validator");
        
            
        OdbcCommand objSqlCmd = new OdbcCommand();
        //string strConnection = "Driver={MySQL ODBC 5.3 Unicode Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        //OdbcConnection conn = new OdbcConnection(strConnection);
        OdbcConnection conn = new OdbcConnection(ConfigurationManager.ConnectionStrings["mysql"].ConnectionString);

        new_ins_name_txt.Text = "";
        new_ins_address_txt.Text = "";
        new_ins_address2_txt.Text = "";
        new_ins_city_txt.Text = "";
        new_ins_state_txt.Text = "";
        new_ins_zip_txt.Text = "";

        new_loc_ins_tbl.Visible = false;
        new_ins_name_txt_rf_validator.Enabled = false;
        new_ins_phone_rf_validator.Enabled = false;

        try
        {

            conn.Open();
            switch (ins_carrier_loc_drp.SelectedValue)
            {

                case "":
                    break;
                case "-199":
                    new_loc_ins_tbl.Visible = true;
                    new_ins_name_txt_rf_validator.Enabled = true;
                    new_ins_phone_rf_validator.Enabled = true;
                    break;                
            }

            
        }
        catch (Exception ex)
        {
            Response.Write("Exception: " + Convert.ToString(ex));
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }

    }

    protected void has_been_treated_drp_chgd(object sender, EventArgs e)
    {
        Panel treated_by_info_tbl = (Panel)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("treated_by_info_tbl");
        DropDownList has_been_treated_drp = (DropDownList)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("has_been_treated_drp");
        RequiredFieldValidator dot_pkr_rf_validator = (RequiredFieldValidator)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("dot_pkr_rf_validator");
        RequiredFieldValidator fu_date_pkr_rf_validator = (RequiredFieldValidator)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("fu_date_pkr_rf_validator");
        RequiredFieldValidator treating_dr_name_txt_rf_validator = (RequiredFieldValidator)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("treating_dr_name_txt_rf_validator");

        if (has_been_treated_drp.SelectedValue == "Y")
        {
            treated_by_info_tbl.Visible = true;
            dot_pkr_rf_validator.Enabled = true;
            fu_date_pkr_rf_validator.Enabled = true;
            treating_dr_name_txt_rf_validator.Enabled = true;
        }
        else
        {
            treated_by_info_tbl.Visible = false;
            dot_pkr_rf_validator.Enabled = false;
            fu_date_pkr_rf_validator.Enabled = false;
            treating_dr_name_txt_rf_validator.Enabled = false;
        }
    }

    protected void submit_intake(object sender, EventArgs e)
    {
        OdbcCommand objSqlCmd = new OdbcCommand();
        //string strConnection = "Driver={MySQL ODBC 5.3 Unicode Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        string sql = "";
        string ins_party_id = "";

        //OdbcConnection objConnection = new OdbcConnection(strConnection);
        OdbcConnection objConnection = new OdbcConnection(ConfigurationManager.ConnectionStrings["mysql"].ConnectionString);

        try
        {
            objConnection.Open();;

            TextBox ip_fname_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_fname_txt");
            TextBox ip_lname_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_lname_txt");
            TextBox ip_suffix_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_suffix_txt");
            TextBox ip_occupation_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_occupation_txt");
            TextBox ip_address_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_address_txt");
            TextBox ip_address2_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_address2_txt");
            TextBox ip_city_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_city_txt");
            TextBox ip_state_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_state_txt");
            TextBox ip_zip_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_zip_txt");
            TextBox ip_area_code_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_area_code_txt");
            TextBox ip_phone_num_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_phone_num_txt");
            TextBox ip_ext_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_ext_txt");
            TextBox ip_dob_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_dob_txt");
            TextBox ip_ssn1_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_ssn1_txt");
            TextBox ip_ssn2_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_ssn2_txt");
            TextBox ip_ssn3_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_ssn3_txt");         
            //-------------
            DropDownList employer_loc_drp = (DropDownList)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("employer_loc_drp");
            TextBox loc_address_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_address_txt");
            TextBox loc_address2_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_address2_txt");
            TextBox loc_city_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_city_txt");
            TextBox loc_state_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_state_txt");
            TextBox loc_zip_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_zip_txt");
            TextBox loc_new_loc_name_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_new_loc_name_txt");
            //-------------
            DropDownList ins_carrier_loc_drp = (DropDownList)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("ins_carrier_loc_drp");
            TextBox new_ins_name_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_name_txt");
            TextBox new_ins_address_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_address_txt");
            TextBox new_ins_address2_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_address2_txt");
            TextBox new_ins_city_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_city_txt");
            TextBox new_ins_state_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_state_txt");
            TextBox new_ins_zip_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_zip_txt");
            TextBox new_ins_area_code_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_area_code_txt");
            TextBox new_ins_phone_num_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_area_code_txt");
            TextBox new_ins_ext_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_ext_txt");
            //-------------
            RadDatePicker doi_pkr = (RadDatePicker)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("doi_pkr");
            RadDatePicker dot_pkr = (RadDatePicker)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("dot_pkr");
            RadDatePicker fu_date_pkr = (RadDatePicker)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("fu_date_pkr");
            TextBox body_part_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("body_part_txt");
            TextBox injury_desc_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("injury_desc_txt");
            TextBox treating_dr_name_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("treating_dr_name_txt");
            TextBox treating_dr_address_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("treating_dr_address_txt");
            TextBox treating_dr_address2_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("treating_dr_address2_txt");
            TextBox treating_dr_city_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("treating_dr_city_txt");
            TextBox treating_dr_state_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("treating_dr_state_txt");
            TextBox treating_dr_zip_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("treating_dr_zip_txt");
            TextBox treating_dr_area_code_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("treating_dr_area_code_txt");
            TextBox treating_dr_phone_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("treating_dr_phone_txt");
            TextBox additional_info_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("additional_info_txt");
            DropDownList has_been_treated_drp = (DropDownList)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("has_been_treated_drp");
            DropDownList was_treated_at_drp = (DropDownList)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("was_treated_at_drp");
            DropDownList referred_to_specialist_drp = (DropDownList)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("referred_to_specialist_drp");
            DropDownList pt_dx_scripted_drp = (DropDownList)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("pt_dx_scripted_drp");
            DropDownList gender_dropdown = (DropDownList)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("gender_txt");
            TextBox claim_number = (TextBox)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item").FindControl("claim_number_txt");

            string emp_loc_name = "";

            if(employer_loc_drp.SelectedValue != "-99")
            {

                sql = "SELECT CONCAT_WS(' - ',ACCOUNT_NUMBER,ACCOUNT_NAME) FROM HZ_CUST_ACCOUNTS WHERE CUST_ACCOUNT_ID = " + employer_loc_drp.SelectedValue;
                
                objSqlCmd = new OdbcCommand(sql, objConnection);

                OdbcDataReader dr3 = objSqlCmd.ExecuteReader();

                if (dr3.Read())
                {               
                  emp_loc_name = myCStr(dr3.GetValue(0));
                }

                dr3.Close();
                dr3.Dispose();
                objSqlCmd.Dispose();
            }
            else
            {
                emp_loc_name = "** NEW LOCATION ** - " + loc_new_loc_name_txt.Text.ToUpper();
            }

            string loc_ins_name = "";

            if(ins_carrier_loc_drp.SelectedValue != "-99")
            {

                sql = "SELECT CONCAT_WS(' - ',ACCOUNT_NUMBER,ACCOUNT_NAME), PARTY_ID FROM HZ_CUST_ACCOUNTS WHERE CUST_ACCOUNT_ID = " + ins_carrier_loc_drp.SelectedValue;
                                
                objSqlCmd = new OdbcCommand(sql, objConnection);

                OdbcDataReader dr3_ = objSqlCmd.ExecuteReader();

                if (dr3_.Read())
                {               
                  loc_ins_name = myCStr(dr3_.GetValue(0));
                  ins_party_id = myCStr(dr3_.GetValue(1));
                }

                dr3_.Close();
                dr3_.Dispose();
                objSqlCmd.Dispose();
            }
            else
            {
                loc_ins_name = "** NEW INSURANCE ** - " + new_ins_name_txt.Text.ToUpper();
            }
            
            string user_name = "";

            sql = "SELECT CONCAT_WS(' ', SFIRSTNAME, SLASTNAME) FROM USER_INFO WHERE IUSERID = " + Convert.ToString(Session["UserID"]);
            
            objSqlCmd = new OdbcCommand(sql, objConnection);

            OdbcDataReader dr = objSqlCmd.ExecuteReader();

            if (dr.Read())
            {               
                user_name = myCStr(dr.GetValue(0));
            }

            
            dr.Close();
            dr.Dispose();
            objSqlCmd.Dispose();


            objSqlCmd = new OdbcCommand("", objConnection);

            objSqlCmd.CommandText = "{call sp_insert_new_intake(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
            objSqlCmd.CommandType = CommandType.StoredProcedure;
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 75)).Value = ip_fname_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 75)).Value = ip_lname_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 5)).Value = ip_suffix_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 150)).Value = ip_occupation_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 150)).Value = ip_address_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 150)).Value = ip_address2_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 75)).Value = ip_city_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 2)).Value = ip_state_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 15)).Value = ip_zip_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 3)).Value = ip_area_code_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 8)).Value = ip_phone_num_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 10)).Value = ip_ext_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 10)).Value = ip_dob_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 11)).Value = ip_ssn1_txt.Text.ToUpper() + "-" + ip_ssn2_txt.Text.ToUpper() + "-" + ip_ssn3_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 150)).Value = emp_loc_name; 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 150)).Value = loc_address_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 150)).Value = loc_address2_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 150)).Value = loc_city_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 2)).Value = loc_state_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 15)).Value = loc_zip_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 150)).Value = new_ins_name_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 150)).Value = new_ins_address_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 150)).Value = new_ins_address2_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 150)).Value = new_ins_city_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 2)).Value = new_ins_state_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 15)).Value = new_ins_zip_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 3)).Value = new_ins_area_code_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 8)).Value = new_ins_phone_num_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 10)).Value = new_ins_ext_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 10)).Value = doi_pkr.SelectedDate.ToString().Replace(" 12:00:00 AM", ""); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 200)).Value = body_part_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 1000)).Value = injury_desc_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.Char, 1)).Value = has_been_treated_drp.SelectedValue; 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 9)).Value = was_treated_at_drp.SelectedValue; 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 150)).Value = treating_dr_name_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 150)).Value = treating_dr_address_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 150)).Value = treating_dr_address2_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 150)).Value = treating_dr_city_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 2)).Value = treating_dr_state_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 15)).Value = treating_dr_zip_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 3)).Value = treating_dr_area_code_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 8)).Value = treating_dr_phone_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 10)).Value = dot_pkr.SelectedDate.ToString().Replace(" 12:00:00 AM", ""); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 10)).Value = fu_date_pkr.SelectedDate.ToString().Replace(" 12:00:00 AM", ""); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.Char, 1)).Value = referred_to_specialist_drp.SelectedValue; 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.Char, 1)).Value = pt_dx_scripted_drp.SelectedValue; 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 1000)).Value = additional_info_txt.Text.ToUpper(); 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 200)).Value = user_name; 
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 10)).Value = DateTime.Now.ToShortDateString();
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 1)).Value = gender_dropdown.SelectedValue;
            objSqlCmd.Parameters.Add(new OdbcParameter("@", OdbcType.VarChar, 50)).Value = claim_number.Text;
            objSqlCmd.ExecuteNonQuery();
            

            
            //confirmation_notice_pnl.Visible = true;
            //injury_report_pnl.Visible = false;

            MailMessage mailObj2 = new MailMessage();
            mailObj2.From = new MailAddress("myeworkwell@eworkwell.com", "MYEWORKWELL INTAKE");

            if (ins_party_id == "5188")
            {
                //Made change for ticket 2433
                mailObj2.To.Add("erbayless@eworkwell.com");
                mailObj2.To.Add("mlhostal@eworkwell.com");
                mailObj2.To.Add("intake@eworkwell.com");
                mailObj2.To.Add("notifications@eworkwell.com");
            }
            else
            {
                mailObj2.To.Add("consulting@eworkwell.com");
                mailObj2.To.Add("intake@eworkwell.com");
                mailObj2.To.Add("notifications@eworkwell.com");
            }

            mailObj2.Subject = "NEW INTAKE FOR " + ip_fname_txt.Text.ToUpper() + " " + ip_lname_txt.Text.ToUpper() + " " + ip_suffix_txt.Text.ToUpper();
            mailObj2.Body = "<b>Patient's Name: </b>" + ip_fname_txt.Text.ToUpper() + " " + ip_lname_txt.Text.ToUpper() + " " + ip_suffix_txt.Text.ToUpper() + "<br><br>";
            mailObj2.Body += "<b>Address: </b>" + ip_address_txt.Text.ToUpper() + "<br>";
            if(ip_address2_txt.Text.Trim().Length != 0)
                mailObj2.Body += "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>" + ip_address2_txt.Text.ToUpper() + "<br>";
            mailObj2.Body += "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>" + ip_city_txt.Text.ToUpper() + ", " + ip_state_txt.Text.ToUpper() + " " + ip_zip_txt.Text.ToUpper() + "<br>";
            mailObj2.Body += "<b>Phone: </b>" + ip_area_code_txt.Text.ToUpper() + "-" + ip_phone_num_txt.Text.ToUpper() + " ext: " + ip_ext_txt.Text.ToUpper() + "<br>";
            mailObj2.Body += "<b>SS#: </b>" + ip_ssn1_txt.Text.ToUpper() + "-" + ip_ssn2_txt.Text.ToUpper() + "-" + ip_ssn3_txt.Text.ToUpper() + "<br>";
            mailObj2.Body += "<b>DOB: </b>" + ip_dob_txt.Text.ToUpper() + "<br><br>";
            mailObj2.Body += "<b>Gender: </b>" + gender_dropdown.SelectedValue.ToUpper() + "<br><br>";

            mailObj2.Body += "<b>Called In By: </b>" + user_name + "<br>";
            mailObj2.Body += "<b>Treated At: </b>" + treating_dr_name_txt.Text.ToUpper() + "<br>";
            mailObj2.Body += "<b>Type of Facility: </b>" + was_treated_at_drp.SelectedValue + "<br>";
            mailObj2.Body += "<b>Address: </b>" + treating_dr_address_txt.Text.ToUpper() + "<br>";
            if (treating_dr_address2_txt.Text.Trim().Length != 0)
                mailObj2.Body += "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>" + treating_dr_address2_txt.Text.ToUpper() + "<br>";
            mailObj2.Body += "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>" + treating_dr_city_txt.Text.ToUpper() + ", " + treating_dr_state_txt.Text.ToUpper() + " " + treating_dr_zip_txt.Text.ToUpper() + "<br>";
            mailObj2.Body += "<b>Phone: </b>" + treating_dr_area_code_txt.Text.ToUpper() + "-" + treating_dr_phone_txt.Text.ToUpper() + "<br><br>";

            mailObj2.Body += "<b>Treatment Date: </b>" + dot_pkr.SelectedDate.ToString().Replace(" 12:00:00 AM", "") + "<br>";
            mailObj2.Body += "<b>Follow-up Appt Date: </b>" + fu_date_pkr.SelectedDate.ToString().Replace(" 12:00:00 AM", "") + "<br>";
            mailObj2.Body += "<b>Referred To Specialist: </b>" + referred_to_specialist_drp.SelectedValue + "<br>";
            mailObj2.Body += "<b>PT/Diagnostics: </b>" + pt_dx_scripted_drp.SelectedValue + "<br><br>";

            mailObj2.Body += "<b>Occupation: </b>" + ip_occupation_txt.Text.ToUpper() + "<br>";
            mailObj2.Body += "<b>Claim #: </b>" + claim_number.Text.ToUpper() + "<br>";
            mailObj2.Body += "<b>Date of Injury.: </b>" + doi_pkr.SelectedDate.ToString().Replace(" 12:00:00 AM", "") + "<br>";
            mailObj2.Body += "<b>Date Reported: </b>" + DateTime.Now.ToShortDateString() + "<br>";
            mailObj2.Body += "<b>Body Parts: </b>" + body_part_txt.Text.ToUpper() + "<br>";
            mailObj2.Body += "<b>Injury Description: </b>" + injury_desc_txt.Text.ToUpper() + "<br>";
            mailObj2.Body += "<b>Additional Notes: </b>" + additional_info_txt.Text.ToUpper() + "<br><br>";

            //mailObj2.Body += "<b>Employer Code: <b>" + ip_ + "<br>";


            mailObj2.Body += "<b>Employer: </b>" + emp_loc_name + "<br>";
            mailObj2.Body += "<b>Address: </b>" + loc_address_txt.Text.ToUpper() + "<br>";
            if (loc_address2_txt.Text.Trim().Length != 0)
                mailObj2.Body += "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>" + loc_address2_txt.Text.ToUpper() + "<br>";
            mailObj2.Body += "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>" + loc_city_txt.Text.ToUpper() + ", " + loc_state_txt.Text.ToUpper() + " " + loc_zip_txt.Text.ToUpper() + "<br><br>";
            if (ins_carrier_loc_drp.SelectedValue != "-99")
            {
                mailObj2.Body += "<b>Ins. Carrier: </b>" + loc_ins_name + "<br>";
            }
            else
            {
                mailObj2.Body += "<b>New Insrance Carrier: </b>" + new_ins_name_txt.Text.ToUpper() + "<br>";
                mailObj2.Body += "<b>Address: </b>" + new_ins_address_txt.Text.ToUpper() + "<br>";
                if (new_ins_address2_txt.Text.Trim().Length != 0)
                    mailObj2.Body += "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>" + new_ins_address2_txt.Text.ToUpper() + "<br>";
                mailObj2.Body += "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>" + new_ins_city_txt.Text.ToUpper() + ", " + new_ins_state_txt.Text.ToUpper() + " " + new_ins_zip_txt.Text.ToUpper() + "<br><br>";
                mailObj2.Body += "<b>Phone: </b>" + new_ins_area_code_txt.Text.ToUpper() + "-" + new_ins_phone_num_txt.Text.ToUpper() + " ext: " + new_ins_ext_txt.Text.ToUpper() + "<br><br>";
            }
            //mailObj2.Body += "<b>Ins. Carrier Name: <b>" + ip_ + "<br>";

            mailObj2.IsBodyHtml = true;



            SmtpClient SMTPServer2 = new SmtpClient("dedrelay.secureserver.net");
            //SMTPServer2.Credentials = new System.Net.NetworkCredential("tuser@eworkwell.com", "workWell25");
            try
            {
                SMTPServer2.Send(mailObj2);
            }
            catch (Exception ex)
            {
                //Response.Write(ex);
            }

            
            objSqlCmd.Dispose();
            patient_name_lbl.Text = "the injury report for " + ip_fname_txt.Text + " " + ip_lname_txt.Text + " has been sent to workwell!";
            injury_report_pnl.Visible = false;
            confirmation_notice_pnl.Visible = true;            

            //AuthenticatedMessagePanel.ResponseScripts.Add("scrollTop();");

        }
        catch (Exception ex)
        {
            Response.Write("Can't load Web page: " + ex.Message);
        }
        finally
        {
            objConnection.Close();
            objConnection.Dispose();

            //cancel_injury_report(null,null);
        }
    }

    protected void clear_all_fields()
    {
        TableRow patient_info_btn_row = (TableRow)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("patient_info_btn_row");
        TableRow employer_info_btn_row = (TableRow)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("employer_info_btn_row");
        TableRow insurance_info_btn_row = (TableRow)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("insurance_info_btn_row");
        
        TextBox ip_fname_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_fname_txt");
        TextBox ip_lname_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_lname_txt");
        TextBox ip_suffix_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_suffix_txt");
        TextBox ip_address_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_address_txt");
        TextBox ip_address2_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_address2_txt");
        TextBox ip_city_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_city_txt");
        TextBox ip_state_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_state_txt");
        TextBox ip_zip_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_zip_txt");
        TextBox ip_area_code_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_area_code_txt");
        TextBox ip_phone_num_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_phone_num_txt");
        TextBox ip_ext_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_ext_txt");
        TextBox ip_ssn1_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_ssn1_txt");
        TextBox ip_ssn2_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_ssn2_txt");
        TextBox ip_ssn3_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_ssn3_txt");
        TextBox ip_dob_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("patient_info_pnl_item").FindControl("ip_dob_txt");
        RadPanelItem employer_info_pnl_item_prnt = (RadPanelItem)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item_prnt");
        

        DropDownList employer_loc_drp = (DropDownList)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("employer_loc_drp");
        TextBox loc_address_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_address_txt");
        TextBox loc_address2_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_address2_txt");
        TextBox loc_city_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_city_txt");
        TextBox loc_state_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_state_txt");
        TextBox loc_zip_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_zip_txt");
        TableRow new_loc_name_row = (TableRow)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("new_loc_name_row");
        TextBox loc_new_loc_name_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_new_loc_name_txt");
        RequiredFieldValidator loc_new_loc_name_txt_rf_validator = (RequiredFieldValidator)report_injury_pnl_bar.FindItemByValue("employer_info_pnl_item").FindControl("loc_new_loc_name_txt_rf_validator");
        RadPanelItem ins_carrier_pnl_item_prnt = (RadPanelItem)report_injury_pnl_bar.FindItemByValue("ins_carrier_pnl_item_prnt");

        DropDownList ins_carrier_loc_drp = (DropDownList)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("ins_carrier_loc_drp");
        TextBox new_ins_name_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_name_txt");
        TextBox new_ins_address_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_address_txt");
        TextBox new_ins_address2_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_address2_txt");
        TextBox new_ins_city_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_city_txt");
        TextBox new_ins_state_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_state_txt");
        TextBox new_ins_zip_txt = (TextBox)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_zip_txt");
        Table new_loc_ins_tbl = (Table)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_loc_ins_tbl");
        RequiredFieldValidator new_ins_name_txt_rf_validator = (RequiredFieldValidator)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_name_txt_rf_validator");
        RequiredFieldValidator new_ins_phone_rf_validator = (RequiredFieldValidator)report_injury_pnl_bar.FindItemByValue("insurance_pnl_item").FindControl("new_ins_phone_rf_validator");

        RadPanelItem injury_info_pnl_item_prnt = (RadPanelItem)report_injury_pnl_bar.FindItemByValue("injury_info_pnl_item_prnt");


        patient_info_btn_row.Visible = true;
        employer_info_btn_row.Visible = true;
        insurance_info_btn_row.Visible = true;

        ip_fname_txt.Text = "";
        ip_lname_txt.Text = "";
        ip_suffix_txt.Text = "";
        ip_address_txt.Text = "";
        ip_address2_txt.Text = "";
        ip_city_txt.Text = "";
        ip_state_txt.Text = "";
        ip_zip_txt.Text = "";
        ip_area_code_txt.Text = "";
        ip_phone_num_txt.Text = "";
        ip_ext_txt.Text = "";
        ip_dob_txt.Text = "";
        ip_ssn1_txt.Text = "";
        ip_ssn2_txt.Text = "";
        ip_ssn3_txt.Text = "";

        employer_loc_drp.Items.Clear();
        loc_address_txt.Text = "";
        loc_address2_txt.Text = "";
        loc_city_txt.Text = "";
        loc_state_txt.Text = "";
        loc_zip_txt.Text = "";
        loc_new_loc_name_txt.Text = "";
        new_loc_name_row.Visible = false;        
        loc_new_loc_name_txt_rf_validator.Enabled = false;
        employer_info_pnl_item_prnt.Expanded = false;
        employer_info_pnl_item_prnt.Enabled = false;

        ins_carrier_loc_drp.Items.Clear();
        new_ins_name_txt.Text = "";
        new_ins_address_txt.Text = "";
        new_ins_address2_txt.Text = "";
        new_ins_city_txt.Text = "";
        new_ins_state_txt.Text = "";
        new_ins_zip_txt.Text = "";

        new_loc_ins_tbl.Visible = false;
        new_ins_name_txt_rf_validator.Enabled = false;
        new_ins_phone_rf_validator.Enabled = false;
        ins_carrier_pnl_item_prnt.Expanded = false;
        ins_carrier_pnl_item_prnt.Enabled = false;


        injury_info_pnl_item_prnt.Expanded = false;
        injury_info_pnl_item_prnt.Enabled = false;

    }

    //------------------------------
    // utility functions
    //------------------------------

    protected string myCStr(object test)
    {
        if (test == DBNull.Value)
        {
            return (String.Empty);
        }
        else
        {
            return Convert.ToString(test);
        }
    }

    protected string ReverseDate(string dDate)
    {
        DateTime dDate_ = Convert.ToDateTime(dDate);
        if (dDate.ToString().Length != 0)
        {
            return dDate_.ToString("yyyy/MM/dd");
        }
        else
        {
            return "";
        }

    }

    protected string ConvertDate(System.DateTime dDate)
    {

        if (dDate.ToString().Length != 0)
        {
            return dDate.ToString("MM/dd/yyyy");
        }
        else
        {
            return "";
        }

    }
}