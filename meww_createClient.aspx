﻿<%@ Page Title="" Language="C#" MasterPageFile="~/myeworkwell_webapp.master" AutoEventWireup="true" CodeFile="meww_createClient.aspx.cs" Inherits="meww_createClient" Debug="true"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentHldr" Runat="Server">
    <%--<telerik:RadScriptManager ID="RadScriptManager1" AsyncPostBackTimeout="5000"  runat="server">
        <Scripts>			          
	        <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
			<asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
			<asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
		</Scripts>
	</telerik:RadScriptManager>--%>
    <telerik:RadNotification ID="error_notification" runat="server"  Height="150px" Width="350px" 
             ShowCloseButton="true" Font-Bold="true" Font-Size="XX-Large" Position="Center" Animation="Fade" AutoCloseDelay="1000">
    </telerik:RadNotification>

    <asp:hiddenfield id="empPartyID" runat="server" Value="0"/>
    <asp:hiddenfield id="locPartyID" runat="server" Value="0"/>
    <asp:hiddenfield id="empLocCustAcctID" runat="server" Value="0"/>    
    <asp:hiddenfield id="cliUserID" runat="server" Value="0"/>
    <asp:hiddenfield id="rest_psswrd" runat="server" Value="0"/>
    <asp:hiddenfield id="addAccount" runat="server" Value="0"/>
    <asp:hiddenfield id="acctType" runat="server" Value="0"/>
    <asp:hiddenfield id="update_user" runat="server" Value="0"/>


    <ajaxToolkit:ToolkitScriptManager runat="Server" EnablePartialRendering="true" ID="ciStatusReportsSM" /> 

                <ajaxToolkit:TabContainer runat="server" ID="client_acctTC" Height="703px" ActiveTabIndex="0" Width="860px" CssClass="visoft__tab_xpie7">
                
                    <ajaxToolkit:TabPanel ID="create_client_EntireAcctTP" runat="server" HeaderText="Create User Account For ENTIRE Account(s)" >
                        <ContentTemplate>
                            <br />                           

                            <asp:Panel ID="empSrchPanel" runat="server" Visible="true" DefaultButton="ImageButton7">

                                <div class="appHeader"> 
                                    <!--creates the top edge-->
                                    <div class="appHeaderTop"> <img src="images/sectionTopLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionTopRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                                    <!--the mid section-->
                                    <div class="sectionTitle"> <img src="images/sectionMidLeft.jpg" width="8" height="15" alt="" class="sectionLeftCorner" />
                                                <h3>Employer's Oracle Account Search</h3>
                                            </div>
                                    <!--the bottom-->
        
                                    <div class="appHeaderBottom"> <img src="images/sectionBottomLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionBottomRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                                </div>
                                <asp:Table runat="server" ID="empSrchFieldTbl" Width="640" Font-Name="Calibri" CssClass="mGrid2" Font-Size="Small" Visible="true">          
            
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Width="125" Wrap="false"><asp:DropDownList id="empSrchTYpeDrp" 
                                                runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">  
                                                <asp:ListItem Value="1">BY ACCOUNT NUMBER</asp:ListItem>
                                                <asp:ListItem Value="0">BY ACCOUNT DESCRIPTION</asp:ListItem>                                                   
                                                                                                    
                                            </asp:DropDownList></asp:TableCell>
                                        <asp:TableCell  HorizontalAlign="Left" Wrap="false"><asp:TextBox ID="empSrchFieldTxt" runat="server" Width="300" class="txtbox"></asp:TextBox>&nbsp;<asp:ImageButton ID="ImageButton9" runat="server" ImageUrl="../Media/layout/btn/appBtnCancel.gif" CssClass="smallappBtnGrd" OnClick="cancelEmpSrch" Visible="true"/><asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="../Media/layout/btn/appBtnSearch.jpg" CssClass="smallappBtnGrd" OnClick="searchForEmployer" Visible="true"/></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>

                                <asp:GridView ID="empSrchRstsGrd" Runat="server" 
                                    AutoGenerateColumns="False" 
                                    AllowPaging="True" 
                                    PageSize="15" 
                                    AlternatingRowStyle-Wrap="True" 
                                    RowStyle-Wrap="False" 
                                    GridLines="None" 
                                    Width="860" 
                                    Font-Name="Calibri" 
                                    CssClass="mGrid"
                                    OnRowDataBound="empSrchRstsGrd_RowDataBound"
                                    OnRowCommand="empSrchRstsGrd_RowCommand"
                                    OnPageIndexChanging="empSrchRstsGrd_PageIndexChanging"
                                    PagerStyle-CssClass="pgr"
                                    AlternatingRowStyle-CssClass="alt"
                                    RowStyle-HorizontalAlign="Left"
                                    DataKeyNames="CUST_ACCOUNT_ID">
                                    <columns>                            
                                        <asp:boundfield datafield="ACCOUNT_NUMBER"  HeaderText="Account #" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="75" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small"/>
                                        <asp:boundfield datafield="ACCOUNT_NAME"  HeaderText="Account Description" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="335" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small"/>
                                        <asp:boundfield datafield="ADDRESS1"  HeaderText="Address" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="400" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small"/>
                                        <asp:TemplateField HeaderStyle-Width="50" ControlStyle-Width="50" ItemStyle-Width="50"  ItemStyle-BackColor="White" ControlStyle-BackColor="White">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="empSelectionBtn" ImageUrl="Media/layout/btn/appBtnSelect.png" CommandArgument='<%# Eval("CUST_ACCOUNT_ID") %>' CausesValidation="false" runat="server" CssClass="smallappBtnGrd"/>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </columns>
                                </asp:GridView>

                        
                                <asp:Table runat="server" ID="empSrchNoRsltsTbl" Width="725" Font-Name="Calibri" CssClass="mGrid" Font-Size="Small" Visible="false">
                                    <asp:TableFooterRow  ForeColor="White" Font-Bold="True" BackColor="#000000" Font-Size="Small" Font-Names="Calibri">
                                        <asp:TableHeaderCell Width="75" HorizontalAlign="Left">Account #</asp:TableHeaderCell>
                                            <asp:TableHeaderCell Width="200" HorizontalAlign="Left">Account Name</asp:TableHeaderCell>
                                            <asp:TableHeaderCell Width="450" HorizontalAlign="Left">Address</asp:TableHeaderCell>
                                    </asp:TableFooterRow>
                                    <asp:TableRow>
                                        <asp:TableCell ColumnSpan="6" HorizontalAlign="Left" BackColor="LightYellow"><asp:Label ID="Label5" runat="server" Font-Bold="true" BackColor="LightYellow" ForeColor="Black" Font-Size="Smaller" Text="No Employers found."></asp:Label></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table> 
                        

                            </asp:Panel>                            

                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    
                    <ajaxToolkit:TabPanel ID="create_client_LocationAcctTP" Visible="false" runat="server" HeaderText="Create User Account For Location(s)" >
                        <ContentTemplate>
                            <br />                           
                            <asp:Panel ID="empLocSrchPanel" runat="server" Visible="true" DefaultButton="empLocSrchBtn">

                                <div class="appHeader"> 
                                    <!--creates the top edge-->
                                    <div class="appHeaderTop"> <img src="images/sectionTopLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionTopRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                                    <!--the mid section-->
                                    <div class="sectionTitle"> <img src="images/sectionMidLeft.jpg" width="8" height="15" alt="" class="sectionLeftCorner" />
                                                <h3>Employer's Oracle Location Search</h3>
                                            </div>
                                    <!--the bottom-->
        
                                    <div class="appHeaderBottom"> <img src="images/sectionBottomLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionBottomRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                                </div>
                                <asp:Table runat="server" ID="empLocSrchFieldTbl" Width="640" Font-Name="Calibri" CssClass="mGrid2" Font-Size="Small" Visible="true">          
            
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Width="125" Wrap="false"><asp:DropDownList id="empLocSrchTYpeDrp" 
                                                runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">  
                                                <asp:ListItem Value="1">BY ACCOUNT NUMBER</asp:ListItem>
                                                <asp:ListItem Value="0">BY ACCOUNT DESCRIPTION</asp:ListItem>                                                   
                                                                                                    
                                            </asp:DropDownList></asp:TableCell>
                                        <asp:TableCell  HorizontalAlign="Left" Wrap="false"><asp:TextBox ID="empLocSrchFieldTxt" runat="server" Width="300" class="txtbox"></asp:TextBox>&nbsp;<asp:ImageButton ID="empLocCancelBtn" runat="server" ImageUrl="../Media/layout/btn/appBtnCancel.gif" CssClass="smallappBtnGrd" OnClick="cancelEmpLocSrch" Visible="true"/><asp:ImageButton ID="empLocSrchBtn" runat="server" ImageUrl="../Media/layout/btn/appBtnSearch.jpg" CssClass="smallappBtnGrd" OnClick="searchForEmployerLoc" Visible="true"/></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>

                                <asp:GridView ID="empLocSrchRstsGrd" Runat="server" 
                                    AutoGenerateColumns="False" 
                                    AllowPaging="True" 
                                    PageSize="15" 
                                    AlternatingRowStyle-Wrap="True" 
                                    RowStyle-Wrap="False" 
                                    GridLines="None" 
                                    Width="860" 
                                    Font-Name="Calibri" 
                                    CssClass="mGrid"
                                    OnRowDataBound="empLocSrchRstsGrd_RowDataBound"
                                    OnRowCommand="empLocSrchRstsGrd_RowCommand"
                                    OnPageIndexChanging="empLocSrchRstsGrd_PageIndexChanging"
                                    PagerStyle-CssClass="pgr"
                                    AlternatingRowStyle-CssClass="alt"
                                    RowStyle-HorizontalAlign="Left"
                                    DataKeyNames="CUST_ACCOUNT_ID">
                                    <columns>                            
                                        <asp:boundfield datafield="ACCOUNT_NUMBER"  HeaderText="Account #" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="75" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small"/>
                                        <asp:boundfield datafield="ACCOUNT_NAME"  HeaderText="Account Description" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="335" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small"/>
                                        <asp:boundfield datafield="ADDRESS1"  HeaderText="Address" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="400" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small"/>
                                        <asp:TemplateField HeaderStyle-Width="50" ControlStyle-Width="50" ItemStyle-Width="50"  ItemStyle-BackColor="White" ControlStyle-BackColor="White">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="empLocSelectionBtn" ImageUrl="Media/layout/btn/appBtnSelect.png" CommandArgument='<%# Eval("CUST_ACCOUNT_ID") %>' CausesValidation="false" runat="server" CssClass="smallappBtnGrd"/>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </columns>
                                </asp:GridView>

                        
                                <asp:Table runat="server" ID="empLocSrchNoRsltsTbl" Width="725" Font-Name="Calibri" CssClass="mGrid" Font-Size="Small" Visible="false">
                                    <asp:TableFooterRow  ForeColor="White" Font-Bold="True" BackColor="#000000" Font-Size="Small" Font-Names="Calibri">
                                        <asp:TableHeaderCell Width="75" HorizontalAlign="Left">Account #</asp:TableHeaderCell>
                                            <asp:TableHeaderCell Width="200" HorizontalAlign="Left">Account Name</asp:TableHeaderCell>
                                            <asp:TableHeaderCell Width="450" HorizontalAlign="Left">Address</asp:TableHeaderCell>
                                    </asp:TableFooterRow>
                                    <asp:TableRow>
                                        <asp:TableCell ColumnSpan="6" HorizontalAlign="Left" BackColor="LightYellow"><asp:Label ID="Label2" runat="server" Font-Bold="true" BackColor="LightYellow" ForeColor="Black" Font-Size="Smaller" Text="No Employers found."></asp:Label></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table> 
                        

                            </asp:Panel>


                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="update_user_infoTB" runat="server" HeaderText="Update User Account" >
                        <ContentTemplate>

                            <br />   
                            <div class="appHeader"> 
                                <!--creates the top edge-->
                                <div class="appHeaderTop"> <img src="images/sectionTopLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionTopRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                                <!--the mid section-->
                                <div class="sectionTitle"> <img src="images/sectionMidLeft.jpg" width="8" height="15" alt="" class="sectionLeftCorner" />
                                            <h3>myeWorkWell User Search</h3>
                                        </div>
                                <!--the bottom-->
        
                                <div class="appHeaderBottom"> <img src="images/sectionBottomLeft.jpg" width="8" height="8" alt="" class="sectionLeftCorner" /> <img src="images/sectionBottomRight.jpg" width="8" height="8" alt="" class="sectionRightCorner" /> </div>
                            </div> 
                            <asp:Panel ID="user_search_panel" runat="server" DefaultButton="user_searchSubmithBtn">

                                <asp:Table ID="eiSearchStatusReportsFieldsTbl" runat="server" Width="760">  
                
                                    <asp:TableHeaderRow>
                                        <asp:TableCell HorizontalAlign="Left">First Name</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">Last Name</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">Email Address</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center">&nbsp;</asp:TableCell>
                                    </asp:TableHeaderRow>                                                                  
                    
                                    <asp:TableRow>
                                        <asp:TableCell ><asp:TextBox runat="Server" ID="firstNameTxt" Width="150px" class="txtbox"></asp:TextBox>
                                            <asp:Label  runat="Server" ID="ipViewFirstNameErrorPointer"  Font-Bold="True" ForeColor="Red" Text=" *" Visible="False"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell ><asp:TextBox  runat="Server" ID="lastNameTxt" Width="150px" class="txtbox"></asp:TextBox>
                                            <asp:Label  runat="Server" ID="ipViewLastNameErrorPointer"  Font-Bold="True" ForeColor="Red" Text=" *" Visible="False"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell ><asp:TextBox  runat="Server" ID="emailTxt"  Width="200px" class="txtbox"></asp:TextBox></asp:TableCell>                                    
                                        <asp:TableCell Wrap="false" HorizontalAlign="Right" >
                                            <asp:ImageButton ID="user_searchClearhBtn" runat="server" ImageUrl="images/appBtnClear.gif" CssClass="smallappBtn" OnClick="clear_user_search"/>
                                            <asp:ImageButton ID="user_searchSubmithBtn" runat="server" ImageUrl="images/appBtnSearch.gif"  CssClass="smallappBtn" OnClick="submit_user_search"/>

                                        </asp:TableCell>
                                    </asp:TableRow>                                                               
                                </asp:Table>  
                                                            
                            </asp:Panel>  
                            
                            <asp:Panel ID="user_search_results_panel" runat="server" Visible="false">

                                <asp:GridView ID="user_srchRsltsGrd" Runat="server" HorizontalAlign="Center"
                                        AutoGenerateColumns="False" 
                                        AllowPaging="True" 
                                        PageSize="15" 
                                        AlternatingRowStyle-Wrap="True" 
                                        RowStyle-Wrap="False" 
                                        GridLines="None" 
                                        Width="460" 
                                        Font-Name="Calibri" 
                                        CssClass="mGrid"
                                        OnRowDataBound="user_srchRsltsGrd_RowDataBound"
                                        OnRowCommand="user_srchRsltsGrd_RowCommand"
                                        PagerStyle-CssClass="pgr"
                                        AlternatingRowStyle-CssClass="alt"
                                        RowStyle-HorizontalAlign="Left"
                                        DataKeyNames="IUSERID">
                                        <columns>                            
                                            <asp:boundfield datafield="SFIRSTNAME"  HeaderText="NAME" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="75" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small"/>
                                            <asp:boundfield datafield="SUSERNAME"  HeaderText="USERNAME" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="335" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small"/>
                                            <asp:TemplateField HeaderStyle-Width="50" ControlStyle-Width="50" ItemStyle-Width="50"  ItemStyle-BackColor="White" ControlStyle-BackColor="White">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="empSelectionBtn" ImageUrl="Media/layout/btn/appBtnSelect.png" CommandArgument='<%# Eval("IUSERID") %>' CausesValidation="false" runat="server" CssClass="smallappBtnGrd"/>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </columns>
                                    </asp:GridView>

                        
                                    <asp:Table runat="server" ID="noUserRsltsTbl" Width="460" Font-Name="Calibri" CssClass="mGrid" Font-Size="Small" Visible="false" HorizontalAlign="Center">
                                        <asp:TableFooterRow  ForeColor="White" Font-Bold="True" BackColor="#000000" Font-Size="Small" Font-Names="Calibri">
                                            <asp:TableHeaderCell Width="75" HorizontalAlign="Left">NAME</asp:TableHeaderCell>
                                                <asp:TableHeaderCell Width="385" HorizontalAlign="Left">USERNAME</asp:TableHeaderCell>
                                        </asp:TableFooterRow>
                                        <asp:TableRow>
                                            <asp:TableCell ColumnSpan="6" HorizontalAlign="Left" BackColor="LightYellow"><asp:Label ID="Label1" runat="server" Font-Bold="true" BackColor="LightYellow" ForeColor="Black" Font-Size="Smaller" Text="No users found."></asp:Label></asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table> 

                                    <br />
                                    <asp:Table ID="Table3" runat="server" Width="460" HorizontalAlign="Center">
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right"><asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="../Media/layout/btn/appBtnNewSrch.png" CssClass="smallappBtnGrd" OnClick="new_user_search"/></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>

                            <asp:Panel ID="update_user_info_panel" runat="server" Visible="false">
                            
                                <asp:Table ID="update_contact_ifno_tbl" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Account Status:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false" ColumnSpan="5"><asp:DropDownList id="account_statusDrp" 
                                                runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">  
                                                <asp:ListItem Value="1" Selected="True">ACTIVE</asp:ListItem>
                                                <asp:ListItem Value="0">INACTIVE</asp:ListItem>                                          
                                            </asp:DropDownList></asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">First Name:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false">
                                        <asp:TextBox ID="update_contact_FNameTxt" runat="server" Width="150" class="txtbox" BackColor="LightYellow"></asp:TextBox>
                                        <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ErrorMessage="Required!" ForeColor="Red" Display="Dynamic" 
                                            ControlToValidate="update_contact_FNameTxt">*Required!
                                        </asp:RequiredFieldValidator>

                                        <%--<asp:Label ID="update_fname_Pointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="false"></asp:Label>--%>
                                        </asp:TableCell>

                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Last Name:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="left" Wrap="false" ColumnSpan="3">
                                        <asp:TextBox ID="update_contact_LNameTxt" runat="server" Width="150" class="txtbox" BackColor="LightYellow"></asp:TextBox>
                                        <asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ErrorMessage="Required!" ForeColor="Red" Display="Dynamic" ControlToValidate="update_contact_LNameTxt">*Required!
                                        </asp:RequiredFieldValidator>
                                        <%--<asp:Label ID="update_lname_Pointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="false"></asp:Label>--%>
                                        </asp:TableCell> 

                                    </asp:TableRow>                       
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Account Name:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false" ColumnSpan="5"><asp:TextBox ID="update_contact_AcctNameTxt" runat="server" Width="400" class="txtbox"></asp:TextBox></asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Address:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false" ColumnSpan="5"><asp:TextBox ID="update_contact_Add1Txt" runat="server" Width="375" class="txtbox"></asp:TextBox></asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false" ColumnSpan="5"><asp:TextBox ID="update_contact_Add2Txt" runat="server" Width="375" class="txtbox"></asp:TextBox></asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">City:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false"><asp:TextBox ID="update_contact_CityTxt" runat="server" Width="200" class="txtbox"></asp:TextBox></asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false" Width="25">State:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false"><asp:TextBox ID="update_contact_StateTxt" runat="server" Width="20" class="txtbox"></asp:TextBox></asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false" Width="25">Zip:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false"><asp:TextBox ID="update_contact_ZipTxt" runat="server" Width="80" class="txtbox"></asp:TextBox></asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false">&nbsp;</asp:TableCell> 
                                    </asp:TableRow>
                                </asp:Table>
                                <asp:Table ID="update_contact_phone_ifno_tbl" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false" Width="112">Phone:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false"><asp:TextBox ID="update_contact_PhoneACTxt" runat="server" Width="30" class="txtbox" BackColor="LightYellow"></asp:TextBox>
<%--                                        <asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" ErrorMessage="Required!" ForeColor="Red" Display="Dynamic" ControlToValidate="update_contact_PhoneACTxt">*Required!
                                        </asp:RequiredFieldValidator>--%>
                                        
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" Wrap="false" Width="5">-</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false"><asp:TextBox ID="update_contact_PhoneNumberTxt" runat="server" Width="100" class="txtbox" BackColor="LightYellow"></asp:TextBox>
<%--                                        <asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" ErrorMessage="Required!" ForeColor="Red" Display="Dynamic" ControlToValidate="update_contact_PhoneNumberTxt">*Required!
                                        </asp:RequiredFieldValidator>--%>
                                        
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Ext:</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false"><asp:TextBox ID="update_contact_PhoneExtTxt" runat="server" Width="30" class="txtbox"></asp:TextBox></asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Fax:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false"><asp:TextBox ID="update_contact_FaxACTxt" runat="server" Width="30" class="txtbox"></asp:TextBox></asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" Wrap="false" Width="5">-</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false"><asp:TextBox ID="update_contact_FaxNumberTxt" runat="server" Width="100" class="txtbox"></asp:TextBox></asp:TableCell> 
                                    </asp:TableRow> 
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Email:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false" ColumnSpan="7"><asp:TextBox ID="update_contact_EmailTxt" runat="server" Width="250" class="txtbox" BackColor="LightYellow"></asp:TextBox>
                                        <asp:RequiredFieldValidator id="RequiredFieldValidator5" runat="server" ErrorMessage="Required!" ForeColor="Red" Display="Dynamic" ControlToValidate="update_contact_EmailTxt">*Required!
                                        </asp:RequiredFieldValidator>

                                        <%--<asp:Label ID="update_email_Pointer" runat="server" Font-Bold="true" ForeColor="Red" Text=" *" Visible="False"></asp:Label>--%>
                                        
                                        </asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Receive Emails:&nbsp;</asp:TableCell>
                                         <%--<asp:CheckBox ID="chkReceiveEmails" runat="server" />--%>

                                        <asp:TableCell HorizontalAlign="Left" Wrap="false" ColumnSpan="5">
                                            <asp:DropDownList id="recvieveEmailsDrp" 
                                                runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">  
                                                <asp:ListItem Value="1">YES</asp:ListItem>
                                                <asp:ListItem Value="0" Selected="True">NO</asp:ListItem>                                          
                                            </asp:DropDownList>

                                        </asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">UserName:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false" ColumnSpan="7">
                                            <asp:TextBox ID="update_username" runat="server" Width="250" class="txtbox_mixed" BackColor="Gray" ReadOnly="true"></asp:TextBox>&nbsp;
                                            </asp:TableCell> 
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false">&nbsp;</asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Password:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false" ColumnSpan="7">
                                            <asp:TextBox ID="update_contact_PasswordTxt" runat="server" Width="250" class="txtbox_mixed" BackColor="Gray" ReadOnly="true"></asp:TextBox>&nbsp;
                                            <asp:ImageButton ID="rest_psswrdBtn" runat="server" ImageUrl="../Media/layout/btn/appBtnReset.jpg" CssClass="smallappBtnGrd" Visible="true" OnClick="update_password"/>
                                        </asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>                                        
                                </asp:Table>

                                <asp:Table ID="update_entire_acct_hdr_tbl" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell style="font:15px Calibri, Arial, Helvetica, sans-serif;font-weight:bold;border-bottom:1px solid #467B99;" Width="860">Entire Accounts User Can View</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                                <asp:Table ID="update_entire_acct_tbl" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:GridView ID="update_entire_acct_grd" Runat="server" 
                                                AutoGenerateColumns="False" 
                                                AllowPaging="True" 
                                                PageSize="15" 
                                                AlternatingRowStyle-Wrap="True" 
                                                RowStyle-Wrap="False" 
                                                GridLines="None" 
                                                Width="860" 
                                                OnRowDataBound="update_entire_acct_grd_RowDataBound"
                                                Font-Name="Calibri" 
                                                CssClass="mGrid"
                                                PagerStyle-CssClass="pgr"
                                                AlternatingRowStyle-CssClass="alt"
                                                RowStyle-HorizontalAlign="Left"
                                                DataKeyNames="PARTY_ID">
                                                <columns>                            
                                                    <asp:boundfield datafield="ACCOUNT_NUMBER"  HeaderText="Account #" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="75" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small"/>
                                                    <asp:boundfield datafield="PARTY_NAME"  HeaderText="Account Description" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="335" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small"/>
                                                    <asp:boundfield datafield="ADDRESS1"  HeaderText="Address" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="400" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small"/>
                                                    <asp:TemplateField HeaderStyle-Width="50" ControlStyle-Width="50" ItemStyle-Width="50"  ItemStyle-BackColor="White" ControlStyle-BackColor="White">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="empSelectionBtn" ImageUrl="Media/layout/btn/appBtnRemove.PNG" CommandArgument='<%# Eval("PARTY_ID") %>' CausesValidation="false" runat="server" CssClass="smallappBtnGrd" RowIndex='<%# Container.DisplayIndex %>' OnClick="deleteEmpLocRow"/>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </columns>
                                            </asp:GridView>
                                        </asp:TableCell>
                                    </asp:TableRow>                                                                 
                                </asp:Table>                              

                                <asp:Table runat="server" ID="no_update_entire_acct_tbl" Width="860" Font-Name="Calibri" CssClass="mGrid" Font-Size="Small" Visible="false">
                                    <asp:TableFooterRow  ForeColor="White" Font-Bold="True" BackColor="#000000" Font-Size="Small" Font-Names="Calibri">
                                        <asp:TableHeaderCell Width="75" HorizontalAlign="Left">Account #</asp:TableHeaderCell>
                                            <asp:TableHeaderCell Width="200" HorizontalAlign="Left">Account Description</asp:TableHeaderCell>
                                            <asp:TableHeaderCell Width="450" HorizontalAlign="Left">Address</asp:TableHeaderCell>
                                    </asp:TableFooterRow>
                                    <asp:TableRow>
                                        <asp:TableCell ColumnSpan="6" HorizontalAlign="Left" BackColor="LightYellow"><asp:Label ID="Label4" runat="server" Font-Bold="true" BackColor="LightYellow" ForeColor="Black" Font-Size="Smaller" Text="No Accounts Found."></asp:Label></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                                <asp:Table ID="update_add_account_tbl" runat="server" Width="860">
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>                
                                        <asp:TableCell  HorizontalAlign="right" Wrap="false">
                                            <asp:ImageButton ID="update_add_account_btn" runat="server" ImageUrl="../Media/layout/btn/appBtnAdd.png" CssClass="smallappBtnGrd" Visible="true" OnClick="add_account_to_user" CommandArgument="update_acct"/></asp:TableCell>
                                    </asp:TableRow>      
                                </asp:Table>
                                <br />
                                <asp:Table ID="update_locations_hdr_tbl" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell style="font:15px Calibri, Arial, Helvetica, sans-serif;font-weight:bold;border-bottom:1px solid #467B99;" Width="860">Locations User Can View</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                                <br />
                                <asp:Table ID="update_locations_tbl" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:GridView ID="update_locations_grd" Runat="server" 
                                                AutoGenerateColumns="False" 
                                                AllowPaging="True" 
                                                PageSize="2" 
                                                AlternatingRowStyle-Wrap="True" 
                                                RowStyle-Wrap="False" 
                                                GridLines="None" 
                                                Width="860" 
                                                OnRowDataBound="update_locations_grd_RowDataBound"
                                                OnPageIndexChanging="update_locations_grd_OnPageIndexChanging" 
                                                Font-Name="Calibri" 
                                                CssClass="mGrid"
                                                PagerStyle-CssClass="pgr"
                                                AlternatingRowStyle-CssClass="alt"
                                                RowStyle-HorizontalAlign="Left"
                                                DataKeyNames="CUST_ACCOUNT_ID">
                                                <columns>                            
                                                    <asp:boundfield datafield="ACCOUNT_NUMBER"  HeaderText="Account #" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="75" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small"/>
                                                    <asp:boundfield datafield="ACCOUNT_NAME"  HeaderText="Account Description" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="335" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small"/>
                                                    <asp:boundfield datafield="ADDRESS1"  HeaderText="Address" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="400" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small"/>
                                                    <asp:TemplateField HeaderStyle-Width="50" ControlStyle-Width="50" ItemStyle-Width="50"  ItemStyle-BackColor="White" ControlStyle-BackColor="White">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="empSelectionBtn" ImageUrl="Media/layout/btn/appBtnRemove.PNG" CommandArgument='<%# Eval("CUST_ACCOUNT_ID") %>' CausesValidation="false" runat="server" CssClass="smallappBtnGrd" RowIndex='<%# Container.DisplayIndex %>' OnClick="deleteEmpLocRow"/>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </columns>
                                            </asp:GridView>
                                        </asp:TableCell>
                                    </asp:TableRow>                                                                 
                                </asp:Table>                              

                                <asp:Table runat="server" ID="no_update_locations_tbl" Width="860" Font-Name="Calibri" CssClass="mGrid" Font-Size="Small" Visible="false">
                                    <asp:TableFooterRow  ForeColor="White" Font-Bold="True" BackColor="#000000" Font-Size="Small" Font-Names="Calibri">
                                        <asp:TableHeaderCell Width="75" HorizontalAlign="Left">Account #</asp:TableHeaderCell>
                                            <asp:TableHeaderCell Width="200" HorizontalAlign="Left">Account Description</asp:TableHeaderCell>
                                            <asp:TableHeaderCell Width="450" HorizontalAlign="Left">Address</asp:TableHeaderCell>
                                    </asp:TableFooterRow>
                                    <asp:TableRow>
                                        <asp:TableCell ColumnSpan="6" HorizontalAlign="Left" BackColor="LightYellow"><asp:Label ID="Label6" runat="server" Font-Bold="true" BackColor="LightYellow" ForeColor="Black" Font-Size="Smaller" Text="No Locations Found."></asp:Label></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                               

                                <asp:Table ID="updateuser_info_btnTbl" runat="server" Width="560">
                                    <asp:TableRow>                
                                        <asp:TableCell  HorizontalAlign="right" Wrap="false" ColumnSpan="6"><asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="../Media/layout/btn/appBtnCancel.gif" CssClass="smallappBtnGrd" OnClick="cancel_update_info" Visible="true"/><asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="../Media/layout/btn/appBtnSubmit.gif" CssClass="smallappBtnGrd" OnClick="submit_update_info" Visible="true"/></asp:TableCell>
                                    </asp:TableRow>   
                                </asp:Table>                           
                                
                            </asp:Panel>

                            <asp:Panel ID="account_update_panel" Visible="false" runat="server">
    
                                <asp:Label ID="update_acct_was_created_lbl" runat="server" Font-Names="calibri" Font-Size="small" Font-Bold="true"></asp:Label>
                                <br />
                                <br />
                                <asp:Table ID="Table2" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">User Name:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false">
                                            <asp:TextBox ID="update_user_nametxt" runat="server" Width="200" class="txtbox" ReadOnly="true"></asp:TextBox></asp:TableCell>
                                    </asp:TableRow>                       
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Password:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false" ColumnSpan="5">
                                            <asp:TextBox ID="update_passwordTxt" runat="server" Width="200" class="txtbox_mixed" ReadOnly="true"></asp:TextBox>
                                        </asp:TableCell> 
                                    </asp:TableRow>
                                    
                                    <asp:TableRow>                
                                        <asp:TableCell  HorizontalAlign="right" Wrap="false">
                                            <asp:ImageButton ID="ImageButton10" runat="server" ImageUrl="../Media/layout/btn/appBtnCancel.gif" CssClass="smallappBtnGrd" OnClick="cancel_update" Visible="true"/>
                                        </asp:TableCell>                                                                        
                                    </asp:TableRow>

                                </asp:Table>
                                <br />
                                <asp:Label ID="update_password_changeLbl" runat="server" Text="They will be forced to change their password after the log into myeWorkWell for the first time." Font-Names="calibri" Font-Size="small" Font-Bold="true"></asp:Label>
                                <br />
                                <br />                                
                            </asp:Panel>

                            <asp:Panel ID="pnlUserNotFound" Visible="false" runat="server">
                                <br />
                                <asp:Table ID="tablenotfound" Width="400px" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Left">
                                            <asp:Label ID="Label7" runat="server" Text="User Not Found!" ForeColor="Red" Font-Names="calibri" Font-Size="small" Font-Bold="true"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Right">
                                            <asp:ImageButton ID="ImageButton12" runat="server" ImageUrl="../Media/layout/btn/appBtnNewSrch.png" CssClass="smallappBtnGrd" OnClick="new_user_search"/>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                                
                                <br />                             
                            </asp:Panel>

                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>

                </ajaxToolkit:TabContainer>

                <asp:Panel ID="contact_info_panel" runat="server" Visible="false" DefaultButton="ImageButton2">
                                
                                <asp:Table id="hdrTbl" GridLines="None" runat="server" Width="860">
                                    <asp:TableRow>
                                        <asp:TableCell ColumnSpan="6">&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell style="font:15px Calibri, Arial, Helvetica, sans-serif;font-weight:bold;border-bottom:1px solid #467B99;" ColumnSpan="6">User Contact Information</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell ColumnSpan="6">&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                                <asp:Table ID="contact_ifno_tbl" GridLines="None" runat="server" Width="860">
                                    
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">First Name:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false">
                                            <asp:TextBox ID="contact_FNameTxt" CausesValidation="true" runat="server" Width="150" class="txtbox" BackColor="LightYellow"></asp:TextBox>
                                        <asp:RequiredFieldValidator id="RequiredFieldValidator6" runat="server" ValidationGroup="valSubmitUserContactInfo" ErrorMessage="Required!" ForeColor="Red" Display="Dynamic" ControlToValidate="contact_FNameTxt">
                                        </asp:RequiredFieldValidator>
                                        </asp:TableCell>

                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Last Name:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="left" Wrap="false" ColumnSpan="3">
                                            <asp:TextBox ID="contact_LNameTxt" ValidationGroup="valSubmitUserContactInfo" runat="server" Width="150" class="txtbox" BackColor="LightYellow"></asp:TextBox>
                                        <asp:RequiredFieldValidator id="RequiredFieldValidator7" runat="server" ValidationGroup="valSubmitUserContactInfo" ErrorMessage="Required!" ForeColor="Red" Display="Dynamic" ControlToValidate="contact_LNameTxt">
                                        </asp:RequiredFieldValidator>
                                        </asp:TableCell> 
                                    </asp:TableRow>   
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Insurance Carrier:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false" ColumnSpan="5"><asp:DropDownList id="ins_carrier_drp" Width="150px" 
                                                runat="server" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">  
                                                <asp:ListItem Value="Y">YES</asp:ListItem>
                                                <asp:ListItem Value="N">NO</asp:ListItem>
                                                <asp:ListItem Value="1">MOTORIST GROUP</asp:ListItem> 
                                                <asp:ListItem Value="E">EMPLOYER</asp:ListItem>                                                                                        
                                            </asp:DropDownList></asp:TableCell>
                                            <asp:TableCell>
                                                <asp:CheckBox ID="chkAllAcctAllLoc" runat="server" Text="All Accounts and All Locations, Employers Only" />                                       
                                            
                                        </asp:TableCell> 
                                    </asp:TableRow>                    
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Account Name:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false" ColumnSpan="5">
                                            <asp:TextBox ID="contact_AcctNameTxt" runat="server" Width="400" class="txtbox"></asp:TextBox></asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Address:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false" ColumnSpan="5">
                                            <asp:TextBox ID="contact_Add1Txt" runat="server" Width="375" class="txtbox"></asp:TextBox></asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false" ColumnSpan="5">
                                            <asp:TextBox ID="contact_Add2Txt" runat="server" Width="375" class="txtbox"></asp:TextBox></asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">City:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false"><asp:TextBox ID="contact_CityTxt" runat="server" Width="200" class="txtbox"></asp:TextBox></asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false" Width="25">State:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false"><asp:TextBox ID="contact_StateTxt" runat="server" Width="20" class="txtbox"></asp:TextBox></asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false" Width="25">Zip:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false"><asp:TextBox ID="contact_ZipTxt" runat="server" Width="80" class="txtbox"></asp:TextBox></asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false">&nbsp;</asp:TableCell> 
                                    </asp:TableRow>
                                </asp:Table>

                                <asp:Table ID="contact_phone_ifno_tbl" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false" Width="100">Phone:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false">
                                            <asp:TextBox ID="contact_PhoneACTxt" runat="server" Width="30" class="txtbox"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator id="RequiredFieldValidator8" runat="server" ErrorMessage="Required!" ForeColor="Red" Display="Dynamic" ControlToValidate="contact_PhoneACTxt">*Required!
                                        </asp:RequiredFieldValidator>--%>
                                        
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" Wrap="false" Width="5">-</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false">
                                            <asp:TextBox ID="contact_PhoneNumberTxt" runat="server" Width="100" class="txtbox"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator id="RequiredFieldValidator9" runat="server" ErrorMessage="Required!" ForeColor="Red" Display="Dynamic" ControlToValidate="contact_PhoneNumberTxt">*Required!
                                        </asp:RequiredFieldValidator>--%>
                                        
                                        </asp:TableCell>
                                        
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Ext:</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false"><asp:TextBox ID="contact_PhoneExtTxt" runat="server" Width="30" class="txtbox"></asp:TextBox></asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Fax:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false"><asp:TextBox ID="contact_FaxACTxt" runat="server" Width="30" class="txtbox"></asp:TextBox></asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Center" Wrap="false" Width="5">-</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false"><asp:TextBox ID="contact_FaxNumberTxt" runat="server" Width="100" class="txtbox"></asp:TextBox></asp:TableCell> 
                                    </asp:TableRow> 
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Email:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false" ColumnSpan="5">
                                            <asp:TextBox ID="contact_EmailTxt" ValidationGroup="valSubmitUserContactInfo" runat="server" Width="250" class="txtbox" BackColor="LightYellow"></asp:TextBox>
                                        <asp:RequiredFieldValidator id="RequiredFieldValidator10" runat="server" ValidationGroup="valSubmitUserContactInfo" ErrorMessage="Required!" ForeColor="Red" Display="Dynamic" ControlToValidate="contact_EmailTxt">
                                        </asp:RequiredFieldValidator>
                                        
                                        </asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Receive Emails:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false" ColumnSpan="5">
                                            <asp:CheckBox ID="chkReceiveEmailsupdate" runat="server" />

                                            <%--<asp:DropDownList id="recvieveEmailsDrp" 
                                                runat="server" AutoPostBack="true" Font-Names="Lucida Sans Unicode, Lucida Grande, Sans-Serif" Font-Italic="False" Font-Size="X-Small" Font-Bold="False">  
                                                <asp:ListItem Value="1" Selected="True">YES</asp:ListItem>
                                                <asp:ListItem Value="0">NO</asp:ListItem>                                          
                                            </asp:DropDownList>--%>

                                        </asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false">&nbsp;</asp:TableCell> 
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Password:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false" ColumnSpan="5"><asp:TextBox ID="contact_PasswordTxt" runat="server" Width="250" class="txtbox" BackColor="Gray" ReadOnly="true"></asp:TextBox></asp:TableCell> 
                                    </asp:TableRow>       
                                </asp:Table>  
                                
                                <asp:Table ID="accountsHdrTbl" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell style="font:15px Calibri, Arial, Helvetica, sans-serif;font-weight:bold;border-bottom:1px solid #467B99;" Width="860">User Accounts</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                                <asp:Table ID="accountsTbl" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:GridView ID="acctListingGrid" Runat="server" 
                                                AutoGenerateColumns="False" 
                                                AllowPaging="True" 
                                                PageSize="15" 
                                                AlternatingRowStyle-Wrap="True" 
                                                RowStyle-Wrap="False" 
                                                GridLines="None" 
                                                Width="860" 
                                                Font-Name="Calibri" 
                                                CssClass="mGrid"
                                                PagerStyle-CssClass="pgr"
                                                AlternatingRowStyle-CssClass="alt"
                                                RowStyle-HorizontalAlign="Left"
                                                DataKeyNames="PARTY_ID">
                                                <columns>                            
                                                    <asp:boundfield datafield="ACCOUNT_NUMBER"  HeaderText="Account #" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="75" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small"/>
                                                    <asp:boundfield datafield="ACCOUNT_NAME"  HeaderText="Account Description" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="335" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small"/>
                                                    <asp:boundfield datafield="ADDRESS"  HeaderText="Address" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" ItemStyle-Width="400" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small"/>
                                                    <asp:TemplateField HeaderStyle-Width="50" ControlStyle-Width="50" ItemStyle-Width="50"  ItemStyle-BackColor="White" ControlStyle-BackColor="White">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="empSelectionBtn" ImageUrl="Media/layout/btn/appBtnRemove.PNG" CommandArgument='<%# Eval("PARTY_ID") %>' CausesValidation="false" runat="server" CssClass="smallappBtnGrd" RowIndex='<%# Container.DisplayIndex %>' OnClick="deleteEmpLocRow"/>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </columns>
                                            </asp:GridView>
                                        </asp:TableCell>
                                    </asp:TableRow>                                                                 
                                </asp:Table>                              

                                <asp:Table runat="server" ID="noAcctListingTbl" Width="860" Font-Name="Calibri" CssClass="mGrid" Font-Size="Small" Visible="false">
                                    <asp:TableFooterRow  ForeColor="White" Font-Bold="True" BackColor="#000000" Font-Size="Small" Font-Names="Calibri">
                                        <asp:TableHeaderCell Width="75" HorizontalAlign="Left">Account #</asp:TableHeaderCell>
                                            <asp:TableHeaderCell Width="200" HorizontalAlign="Left">Account Description</asp:TableHeaderCell>
                                            <asp:TableHeaderCell Width="450" HorizontalAlign="Left">Address</asp:TableHeaderCell>
                                    </asp:TableFooterRow>
                                    <asp:TableRow>
                                        <asp:TableCell ColumnSpan="6" HorizontalAlign="Left" BackColor="LightYellow">
                                            <asp:Label ID="Label3" runat="server" Font-Bold="true" BackColor="LightYellow" ForeColor="Black" Font-Size="Smaller" Text="No Employers found."></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table> 

                                <asp:Table ID="addEmpLocBtnTbl" runat="server" Width="860">
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>                
                                        <asp:TableCell  HorizontalAlign="right" Wrap="false">
                                            <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="../Media/layout/btn/appBtnAdd.png" CssClass="smallappBtnGrd" Visible="true" OnClick="add_account_to_user"/></asp:TableCell>
                                    </asp:TableRow>      
                                </asp:Table>

                                <asp:Table runat="server" ID="createUserAcctFooterTbl" Width="860">          
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell style="font:15px Calibri, Arial, Helvetica, sans-serif;font-weight:bold;border-bottom:1px solid #467B99;">&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>                
                                        <asp:TableCell  HorizontalAlign="right" Wrap="false">
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Media/layout/btn/appBtnCancel.gif" CssClass="smallappBtnGrd" OnClick="cancelContactInfo" Visible="true"/>
                                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../Media/layout/btn/appBtnSave.png" CssClass="smallappBtnGrd" CausesValidation="true" ValidationGroup="valSubmitUserContactInfo" OnClick="submitContactInfo" Visible="true"/>
                                            <asp:ImageButton ID="ImageButton11" runat="server" ImageUrl="~/images/appBtnBack.gif" CssClass="smallappBtnGrd" OnClick="BackToAddPage" Visible="true"/></asp:TableCell>
                                        </asp:TableRow>
                                </asp:Table>
    
                                <asp:Label ID="required_field_error" runat="server" ForeColor="red" Font-Bold="true" Text="ERROR: You MUST fill in ALL required fields." Font-Size="X-Small" CssClass="info" Visible="false"></asp:Label>
                            </asp:Panel>

                <asp:Panel ID="account_created_panel" Visible="false" runat="server">
    
                                <asp:Label ID="acct_was_created_lbl" runat="server" Font-Names="calibri" Font-Size="small" Font-Bold="true"></asp:Label>
                                <br />
                                <br />
                                <asp:Table ID="login_infoTbl" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">User Name:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false"><asp:TextBox ID="user_nametxt" runat="server" Width="200" class="txtbox" ReadOnly="true"></asp:TextBox></asp:TableCell>
                                    </asp:TableRow>                       
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Wrap="false">Password:&nbsp;</asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Wrap="false" ColumnSpan="5"><asp:TextBox ID="passwordTxt" runat="server" Width="200" class="txtbox" ReadOnly="true"></asp:TextBox></asp:TableCell> 
                                    </asp:TableRow>
                                </asp:Table>
                                <br />
                                <asp:Label ID="password_changeLbl" runat="server" Text="They will be forced to change their password after the log into myeWorkWell for the first time." Font-Names="calibri" Font-Size="small" Font-Bold="true"></asp:Label>
                                <br />
                                <br />
                                <asp:Label ID="creat_panel_folderLbl" runat="server" Font-Names="calibri" Font-Size="small"></asp:Label>
                                <br />
                                <br />
                                <asp:Label ID="verify_accountLbl" runat="server" Font-Bold="true" ForeColor="Red" Font-Names="calibri" Font-Size="small" Text="** Before sending this information to the user, <u>VERIFY</u> the <u>ENTIRE</u> account is up to date, correct and all the location have been entered... <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Even if the locations haven't any injuries called in yet!"></asp:Label>
                                <asp:Table ID="finishedBtnTbl" runat="server" Width="760">
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right"><asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="../Media/layout/btn/appBtnFinished.png" CssClass="smallappBtnGrd" OnClick="finished_creating_user"/></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>

                <asp:Panel ID="user_exists_already_panel" Visible="false" runat="server">
    
                                <asp:Label ID="existing_user_lbl" runat="server" Font-Names="calibri" Font-Size="small" ForeColor="Red" Text="** This user already has an account.  Please use the Update tab to update this user's information."></asp:Label>
   
                           </asp:Panel>
    
</asp:Content>

