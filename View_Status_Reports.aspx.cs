﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Odbc;
using System.Data;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
//Reminder to self to put the ConvertDate Functions from the old code into the new one I thought it was a literal convert.todateTime
public partial class View_Status_Reports : System.Web.UI.Page
{
    string filename;
    public string _fullfilename
    {
        get
        {
            if (ViewState["_fullfilename"] == null)
                return string.Empty;
            return ViewState["_fullfilename"].ToString();
        }
        set
        {
            ViewState["_fullfilename"] = value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.Request.IsAuthenticated)
        {
            AuthenticatedMessagePanel.Visible = true;
            AnonymousMessagePanel.Visible = false;

            if (!IsPostBack)
                SearchViewStsRpt();
        }
        else
        {
            AuthenticatedMessagePanel.Visible = false;
            AnonymousMessagePanel.Visible = true;
        }
    }
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (writeItVal.Value == "1")
        {
            //write the status report, this is done so I can refresh the submit Status report page
            //'writeit()

            writeItVal.Value = "0";
        }

    }
    public PdfPCell getCell(string sText , int iAlignment , bool bBold) 
    {
        //0=Left, 1=Centre, 2=Right


        string fontpath = Server.MapPath("fonts");
        BaseFont customfont = BaseFont.CreateFont(fontpath + "//calibri.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);

        Font font = new Font();

        if (bBold)
            font = new Font(customfont, 8, Font.BOLD);
        else
            font = new Font(customfont, 8);


        PdfPCell cell = new PdfPCell(new Phrase(sText, font));
        cell.Border = 0;
        cell.HorizontalAlignment = iAlignment;

        return cell;

    }
    public PdfPCell getCell(string sText, int iAlignment, bool bBold, bool iCellPadding)
    {
        //0=Left, 1=Centre, 2=Right
        string fontpath = Server.MapPath("fonts");
        BaseFont customfont = BaseFont.CreateFont(fontpath + "//calibri.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
        Font font = new Font();
        if (bBold)
            font = new Font(customfont, 8, Font.BOLD);
        else
            font = new Font(customfont, 8);
        PdfPCell cell = new PdfPCell(new Phrase(sText, font));
        cell.Border = 0;
        if (iCellPadding)
        {
            cell.PaddingBottom = 0.01f;
            cell.PaddingTop = 0.01f;
        }
        cell.HorizontalAlignment = iAlignment;
        return cell;

    }
    public void CheckWorkStatusType()
    {
        if (srWorkStatusDrp.SelectedValue == "2" || srWorkStatusDrp.SelectedValue == "3")
        {
            ciUntilDateTxtCell.Visible = true;
            ciUntilDateLblCell.Visible = true;
            if (srWorkStatusDrp.SelectedValue == "3")
                ciModifiedRestrictionsPanel.Visible = true;
            else
                ciModifiedRestrictionsPanel.Visible = false;
            if (srEffectiveDateTxt.Text != null)
                srEffectiveDateTxt.Text = DateTime.Now.ToShortDateString();
        }
        else
        {
            ciUntilDateTxtCell.Visible = false;
            ciUntilDateLblCell.Visible = false;
            ciModifiedRestrictionsPanel.Visible = false;
            if (srWorkStatusDrp.SelectedValue == "1")
                if (srEffectiveDateTxt.Text != null) 
                    srEffectiveDateTxt.Text = DateTime.Now.ToShortDateString();
            else
            {
                srUntilDateTxt.Text = "";
                srEffectiveDateTxt.Text = "";
            }

        }

        srUntilDateTxt.Text = "";
        srCanLiftDrp.SelectedIndex = -1;
        srCanCarryDrp.SelectedIndex = -1;
        srSimpleGraspingDrp.SelectedIndex = -1;
        srPushingPullingDrp.SelectedIndex = -1;
        srFineManipulationDrp.SelectedIndex = -1;
        srRepeatRFootWorkDrp.SelectedIndex = -1;
        srRepeatLFootWorkDrp.SelectedIndex = -1;
        srCanSitPerDayDrp.SelectedIndex = -1;
        srCanStandPerDayDrp.SelectedIndex = -1;
        srCanDrivePerDayDrp.SelectedIndex = -1;
        srCanBendDrp.SelectedIndex = -1;
        srCanSquatDrp.SelectedIndex = -1;
        srCanClimbStairsDrp.SelectedIndex = -1;
        srCanClimbLadderDrp.SelectedIndex = -1;
        srCanCrawlDrp.SelectedIndex = -1;
        srCanReachDrp.SelectedIndex = -1;
    }
    public void GetCMName()
    {
        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        OdbcDataReader objSqlDataRdr = null;
        try{
            objConnection.Open();
            objSqlCmd.Connection = objConnection;

            // get patient details

            objSqlCmd = new OdbcCommand("", objConnection);

            objSqlCmd.CommandText = "SELECT CM_NAME " 
                + "FROM CASE_MANAGERS "
                + "WHERE CM_EMAIL = '" + User.Identity.Name.ToUpper() + "@EWORKWELL.COM' ";

            objSqlDataRdr = objSqlCmd.ExecuteReader();

            if (objSqlDataRdr.Read())
                srCMTxt.Text = objSqlDataRdr.GetValue(0).ToString();

            objSqlDataRdr.Close();
            objSqlDataRdr.Dispose();
        }        
        catch (Exception ex)
        {
            Response.Write("Can't load Web page " + ex.Message);
        }
        finally{
            objConnection.Close();
            objConnection.Dispose();
        }
        StatusReprtFormPanel.Visible = true;
    }
    public void SearchViewStsRpt()
    {
        //database variables
        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = ConnectionStrings.MySqlConnections();
        OdbcConnection objConnection = new OdbcConnection(strConnection);
        OdbcDataReader objSqlDataRdr = null;

        int iPartyID = Convert.ToInt32(Session["PartyID"]);
        string dDOIStartRangeDate = "2000/01/01";
        string dDOIEndRangeDate  = UtilityFunctions.ReverseDate(DateTime.Now.AddDays(1).ToShortDateString());
        string dDOEStartRangeDate = "2000/01/01";
        string dDOEEndRangeDate = UtilityFunctions.ReverseDate(DateTime.Now.AddDays(1).ToShortDateString());
        string sBuiltQuery  = String.Empty;
        string sClaimNumberQuery  = String.Empty;
        string sSocialSecNum  = String.Empty;
        string sSocialSecNumQuery  = String.Empty;
        bool bSSEntered = true;
        bool bDateError = false;

        bool ipViewSRDOIStartRangeDate = false;
        bool ipViewSRDOIEndRangeDate = false;
        bool ipViewStartDateAfterEndDate = false;

        DataTable dt = new DataTable();
        string sSqlQuery = String.Empty;

        EditVal.Value = "0";

        ipViewFirstNameErrorPointer.Visible = false;
        ipViewLastNameErrorPointer.Visible = false;
        ipViewstartDOIRangeErrorPointer.Visible = false;
        ipViewendDOIRangeErrorPointer.Visible = false;
        startViewSRDOIRangeError.Visible = false;
        endViewSRDOIRangeError.Visible = false;
        startDateAfterEndDateError.Visible = false;

        if (Session["UserName"].ToString() == "DEMO")
        {
            //ImgAnalyitics.Visible = True
            btnAnalytics.Visible = true;
        }
        else
            btnAnalytics.Visible = false;
        DateTime result;
        if ((ipViewDOIStartRangeTxt.Text != null))
        {
            if (DateTime.TryParse(ipViewDOIStartRangeTxt.Text, out result))
                dDOIStartRangeDate = UtilityFunctions.ReverseDate(ipViewDOIStartRangeTxt.Text);
        }
        else if (ipViewDOIStartRangeTxt.Text != null)
        {
            if (DateTime.TryParse(ipViewDOIStartRangeTxt.Text, out result))
            {
                bDateError = true;
                ipViewSRDOIStartRangeDate = true;
            }
        }

        if (ipViewDOIEndRangeTxt.Text != null)
        {
            if( DateTime.TryParse(ipViewDOIEndRangeTxt.Text, out result))
                dDOIEndRangeDate = UtilityFunctions.ReverseDate(ipViewDOIEndRangeTxt.Text);
        }
        else if (ipViewDOIEndRangeTxt.Text != null)
        {
            if( DateTime.TryParse(ipViewDOIEndRangeTxt.Text,out result))
            {
                bDateError = true;
                ipViewSRDOIEndRangeDate = true;
            }
        }

        if( DateTime.TryParse(ipViewDOIEndRangeTxt.Text, out result))
        {
            if( DateTime.TryParse(ipViewDOIStartRangeTxt.Text, out result))
            {
                if (Convert.ToDateTime(dDOIStartRangeDate) > Convert.ToDateTime(dDOIEndRangeDate))
                {
                    bDateError = true;
                    ipViewStartDateAfterEndDate = true;
                }
            }
        }

        if (bDateError)
        {
            // show error message; they must enter at least 1 search criteria
            if (ipViewSRDOIStartRangeDate)
                startViewSRDOIRangeError.Visible = true;
            if (ipViewSRDOIEndRangeDate)
                endViewSRDOIRangeError.Visible = true;
            if (ipViewStartDateAfterEndDate)
                startDateAfterEndDateError.Visible = true;
        }
        else
        {
            // look up IPs with patient criteria

            ipViewSearchResultsPanel.Visible = true;
            if (ipViewSS1Txt.Text != null || ipViewSS2Txt.Text != null || ipViewSS3Txt.Text != null)
            {
                if (ipViewSS1Txt.Text != null)
                    sSocialSecNum = ipViewSS1Txt.Text.Trim().Replace("'", "''") + "-";
                else
                {
                    sSocialSecNum = "%-";
                    bSSEntered = false;
                }

                if (ipViewSS2Txt.Text != null)
                    sSocialSecNum = sSocialSecNum + ipViewSS2Txt.Text.Trim().Replace("'", "''") + "-";
                else
                {
                    sSocialSecNum = sSocialSecNum + "%-";
                    bSSEntered = false;
                }

                if (String.IsNullOrEmpty(ipViewSS3Txt.Text) == false) 
                    sSocialSecNum = sSocialSecNum + ipViewSS3Txt.Text.Trim().Replace("'", "''");
                else
                {
                    sSocialSecNum = sSocialSecNum.Replace("'", "''") + "%";
                    bSSEntered = false;
                }

                if (bSSEntered)
                    sBuiltQuery = sBuiltQuery + " AND (SOCIALSECURITYNUMBER = '" + sSocialSecNum.Replace("'", "''") + "') ";
                else
                    sBuiltQuery = sBuiltQuery + " AND (SOCIALSECURITYNUMBER like '" + sSocialSecNum.Replace("'", "''") + "') ";
            }
            if (String.IsNullOrEmpty(ipViewFirstNameTxt.Text.Trim()) == false)
                sBuiltQuery = " AND IPFIRSTNAME LIKE '%" + ipViewFirstNameTxt.Text.ToUpper().Trim().Replace("'", "''") + "%' ";
            if (String.IsNullOrEmpty(ipViewLastNameTxt.Text.Trim()) == false)
                sBuiltQuery = sBuiltQuery + " AND IPLASTNAME LIKE '%" + ipViewLastNameTxt.Text.ToUpper().Trim().Replace("'", "''") + "%' ";
            if (Convert.ToInt32(Session["PartyID"]) != Convert.ToInt32(Session["InsPartyID"]))
            {
                // reset Session("mult_locs")
                
                string sql = "SELECT COUNT(DISTINCT(CUST_ACCOUNT_ID)) FROM ACCOUNT_PRIVS WHERE IUSERID = " 
                    + Session["UserID"] + " AND CUST_ACCOUNT_ID <> 0 AND PARTY_ID = " 
                    + Convert.ToString(Session["PartyID"]) +  " AND CUST_ACCOUNT_ID = " + Convert.ToString(Session["CustAccountID"]);
                objSqlCmd = new OdbcCommand(sql, objConnection);
                objConnection.Open();
                objSqlDataRdr = objSqlCmd.ExecuteReader();
                if (objSqlDataRdr.Read())
                    Session["mult_locs"] = objSqlDataRdr.GetValue(0);
                objSqlDataRdr.Close();

                int mult_locs = Convert.ToInt32(Session["mult_locs"]);
                string cust_account_sql_str = "AND ( ";

                if (mult_locs == 1 && String.IsNullOrEmpty(Session["CustAccountID"].ToString()) == false)
                    cust_account_sql_str += " HCA.CUST_ACCOUNT_ID = " + Session["CustAccountID"] + " ) ";
                else
                    if (Convert.ToString(Session["InsCarrier"]) != "N" && Convert.ToString(Session["InsCarrier"]) != "E")
                        cust_account_sql_str = String.Empty;
                if (mult_locs > 1)
                {
                    char[] delimiterChars = { ',' };
                    string str = Session["CustAccount_IDs"].ToString();
                    string[] strSplitArr = str.Split(delimiterChars);

                    foreach ( string arrStr in strSplitArr)
                    {
                        cust_account_sql_str = cust_account_sql_str + " HCA.CUST_ACCOUNT_ID = " + arrStr + " OR ";
                    }
                    cust_account_sql_str = cust_account_sql_str.Substring(0, cust_account_sql_str.Length - 4) + " ) ";
                }
                if (mult_locs == 0)
                    cust_account_sql_str = String.Empty;

                if (mult_locs == 1 && Convert.ToString(Session["InsCarrier"]) == "E")
                    cust_account_sql_str = String.Empty;


                sSqlQuery = "SELECT IPCUSTACCTID, IPFIRSTNAME, IPMIDDLENAME, IPLASTNAME, IPDOI, IPDOE, STATUSREPORTID, IPEMPLOYERNAME, SOCIALSECURITYNUMBER, "
                            + " REPORT_TYPE  " 
                            + "FROM STATUS_REPORTS " 
                            + "WHERE ACTIVE = 1 AND " 
                            + "      EMP_PARTY_ID = " + iPartyID + " AND "
                            + "      IPDOI BETWEEN '" + UtilityFunctions.ReverseDate(dDOIStartRangeDate) 
                            + "' AND '" + UtilityFunctions.ReverseDate(dDOIEndRangeDate) + "' AND " 
                            + "      IPDOE BETWEEN '" + UtilityFunctions.ReverseDate(dDOEStartRangeDate) 
                            + "' AND '" + UtilityFunctions.ReverseDate(dDOEEndRangeDate) + "' " + sBuiltQuery 
                            + "      AND IPCUSTACCTID IN (SELECT HCA_SUB.CUST_ACCOUNT_ID " 
                            + "				            FROM HZ_CUST_ACCOUNTS HCA, HZ_PARTIES HP, HZ_CUST_ACCT_SITES_ALL HCASA, HZ_CUST_ACCT_RELATE_ALL HCARA, "
                            + "HZ_CUST_ACCOUNTS HCA_SUB, HZ_PARTIES HP_SUB  " 
                            + "				            WHERE HP.PARTY_ID = " + iPartyID + "  " 
                            + "				                  AND HP.PARTY_ID = HCA.PARTY_ID  " 
                            + "				                  AND HCASA.CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID  " 
                            + "				                  AND HCASA.CUST_ACCOUNT_ID = HCARA.RELATED_CUST_ACCOUNT_ID  " + cust_account_sql_str 
                            + "				                  AND HCARA.CUST_ACCOUNT_ID = HCA_SUB.CUST_ACCOUNT_ID  " 
                            + "				                  AND HCA_SUB.PARTY_ID = HP_SUB.PARTY_ID  " 
                            + "				                  AND HCA_SUB.ATTRIBUTE_CATEGORY = 'PATIENT' " 
                            + "				                  AND HCARA.STATUS = 'A') " 
                            + "ORDER BY IPDOE desc, IPLASTNAME, IPFIRSTNAME, IPMIDDLENAME  ";
            }
            else
            {
                if (Convert.ToString(Session["InsCarrier"]) == "I" || Convert.ToString(Session["InsCarrier"]) == "E")
                {
                    sSqlQuery = "SELECT IPCUSTACCTID, IPFIRSTNAME, IPMIDDLENAME, IPLASTNAME, IPDOI, IPDOE, STATUSREPORTID, IPEMPLOYERNAME, "
                        + " SOCIALSECURITYNUMBER, REPORT_TYPE " 
                        + "FROM STATUS_REPORTS " 
                        + "WHERE ACTIVE = 1 AND " 
                        + "      IPDOI BETWEEN '" + UtilityFunctions.ReverseDate(dDOIStartRangeDate) + "' AND '" 
                        + UtilityFunctions.ReverseDate(dDOIEndRangeDate) + "' AND " 
                        + "      IPDOE BETWEEN '" + UtilityFunctions.ReverseDate(dDOEStartRangeDate) + "' AND '" 
                        + UtilityFunctions.ReverseDate(dDOEEndRangeDate) + "' " + sBuiltQuery 
                        + "   AND IPCUSTACCTID IN (SELECT HCA_SUB.CUST_ACCOUNT_ID " 
                        + "   FROM HZ_CUST_ACCOUNTS HCA, HZ_PARTIES HP, HZ_CUST_ACCT_SITES_ALL HCASA, HZ_CUST_ACCT_RELATE_ALL HCARA, "
                        + " HZ_CUST_ACCOUNTS HCA_SUB, HZ_PARTIES HP_SUB  " 
                        + "   WHERE " 
                        + "   HP.PARTY_ID = HCA.PARTY_ID " 
                        + "   AND HCASA.CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID " 
                        + "   AND HCASA.CUST_ACCOUNT_ID = HCARA.RELATED_CUST_ACCOUNT_ID " 
                        + "   AND HCARA.CUST_ACCOUNT_ID = HCA_SUB.CUST_ACCOUNT_ID " 
                        + "   AND HCA_SUB.PARTY_ID = HP_SUB.PARTY_ID  AND HCA_SUB.ATTRIBUTE_CATEGORY = 'PATIENT' " 
                        + "   AND HCARA.STATUS = 'A' " 
                        + "   AND HCA.CUST_ACCOUNT_ID= " + Convert.ToString(Session["CustAccountID"]) 
                        + "   )" 
                        + "ORDER BY IPDOE desc, IPLASTNAME, IPFIRSTNAME, IPMIDDLENAME  ";
                }
                else
                {
                    sSqlQuery = "SELECT IPCUSTACCTID, IPFIRSTNAME, IPMIDDLENAME, IPLASTNAME, IPDOI, IPDOE, STATUSREPORTID, IPEMPLOYERNAME, "
                        + " SOCIALSECURITYNUMBER, REPORT_TYPE  " 
                        + "FROM STATUS_REPORTS " 
                        + "WHERE ACTIVE = 1 AND " 
                        + "      IPDOI BETWEEN '" + UtilityFunctions.ReverseDate(dDOIStartRangeDate) + "' AND '"
                        + UtilityFunctions.ReverseDate(dDOIEndRangeDate) + "' AND " 
                        + "      IPDOE BETWEEN '" + UtilityFunctions.ReverseDate(dDOEStartRangeDate) + "' AND '" 
                        + UtilityFunctions.ReverseDate(dDOEEndRangeDate) + "' " + sBuiltQuery 
                        + "      AND IPCUSTACCTID IN (SELECT HCA_IP.CUST_ACCOUNT_ID  " 
                        + "				            FROM HZ_CUST_ACCOUNTS HCA_IP " 
                        + "				                 INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA_INS ON HCARA_INS.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID " 
                        + "				                 INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA_INS.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID  "
                        + "				                 INNER JOIN HZ_PARTIES HP_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID "
                        + "				            WHERE HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT' AND "
                        + "				                  HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' AND "
                        + "	   HCA_INS.PARTY_ID = " + Convert.ToString(Session["InsPartyID"]) + " AND " + " HCA_INS.PARTY_ID = " 
                        + Convert.ToString(Session["MotoristsSub"]) + " AND " 
                        + "				                  HCARA_INS.STATUS = 'A' AND " 
                        + "				                  HCA_IP.ATTRIBUTE9 IS NOT NULL) " 
                        + "ORDER BY IPDOE desc, IPLASTNAME, IPFIRSTNAME, IPMIDDLENAME  ";
                }
            }

            ciStatusRptsViewSrchResultsPanel.Visible = false;
            ciStatusRptsViewSrchResultsPanel.Visible = true;
            ipViewSearchResultsPanel.Visible = true;
            srNoViewSearchResultsTbl.Visible = false;

            
            OdbcDataAdapter objDataAdapter = new OdbcDataAdapter(sSqlQuery, objConnection);


            try
            {
                objConnection.Open();
                objDataAdapter.Fill(dt);

                ciStatusRptViewGrd.DataSource = dt;
                ciStatusRptViewGrd.PageIndex = 0;
                ciStatusRptViewGrd.DataBind();

                if (dt.Rows.Count > 0)
                    ciStatusRptViewGrd.Visible = true;
                else
                {
                    ciStatusRptViewGrd.Visible = false;
                    srNoViewSearchResultsTbl.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Response.Write("Page Won't Load: " + ex.ToString());
            }
            finally
            {
                objConnection.Close();
                objDataAdapter.Dispose();
                objConnection.Dispose();
            }
        }
    }
    protected void ciStatusRptViewGrd_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e )
    {
        SearchViewStsRpt();
        ciStatusRptViewGrd.PageIndex = e.NewPageIndex;
        ciStatusRptViewGrd.DataBind();
    }
    protected void ciStatusRptViewGrd_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e )
    {
        statusReportID.Value = e.CommandArgument.ToString();

        srActiveStatusDrp.SelectedIndex = -1;

        if (e.CommandName == "Page")
        {

            //this is only for the triage report for chris.

            if (e.CommandName == "triage")
            {
                statusReportID.Value = e.CommandArgument.ToString();

                OdbcCommand objSqlCmd = new OdbcCommand();
                OdbcDataReader objSqlDataRdr = null;
                string strConnection = ConnectionStrings.MySqlConnections();
                OdbcConnection objConnection = new OdbcConnection(strConnection);
                
                objConnection.Open();
                objSqlCmd.Connection = objConnection;

                // get patient details

                objSqlCmd = new OdbcCommand("", objConnection);
                objSqlCmd.CommandText = "{call sp_getIPStatusReport(?)}";
                objSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
                objSqlCmd.Parameters.Add(new OdbcParameter("@statusReportID", OdbcType.VarChar, 255)).Value = statusReportID.Value;
                objSqlDataRdr = objSqlCmd.ExecuteReader();

                if (objSqlDataRdr.Read())
                {
                    ipCustAccountID.Value = objSqlDataRdr.GetValue(0).ToString();

                    reportType.Value = objSqlDataRdr.GetValue(93).ToString();
                    srVisitTypeDrp.SelectedValue = objSqlDataRdr.GetValue(93).ToString();


                    if (objSqlDataRdr.GetValue(6).ToString() != null)
                    {
                        srIPDOETxt.Text = objSqlDataRdr.GetValue(6).ToString().Replace(" 00:00:00", "");
                    }

                    if (objSqlDataRdr.GetValue(7).ToString() != null)
                    {
                        srIPDOITxt.Text = objSqlDataRdr.GetValue(7).ToString().Replace(" 00:00:00", "");
                    }

                    if (objSqlDataRdr.GetValue(8) != null )
                    {
                        srIPDOBTxt.Text = objSqlDataRdr.GetValue(8).ToString().Replace(" 00:00:00", "");
                    }

                    srIPFNameTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(1)).ToUpper();
                    srIPMNameTxt.Text = objSqlDataRdr.GetValue(2).ToString();
                    srIPLNameTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(3)).ToUpper();
                    srIPEmployerNameTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(4)).ToUpper();
                    srIPSSNumClaimNumTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(5)).ToUpper();

                    srSocialSecurityNumTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(58)).ToUpper();

                    empPartyID.Value = objSqlDataRdr.GetValue(76).ToString();

                    if (empPartyID.Value == "222397") 
                    {
                        aghEmpTimeInOutTbl.Visible = true;
                        aghEmpDisclaimerTbl.Visible = true;
                    }
                    else
                    {
                        aghEmpTimeInOutTbl.Visible = false;
                        aghEmpDisclaimerTbl.Visible = false;
                    }

                    srTreatingPhysicianTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(53)).ToUpper();
                    srCMTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(54)).ToUpper();
                    srDiagonsisTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(10)).ToUpper();
                    srObjectiveFindingsTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(11)).ToUpper();
                    srMechanismOfInjuryTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(12)).ToUpper();

                    srBodyPartTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(18)).ToUpper();
                    srDiagOtherTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(17)).ToUpper();
                    srReasonTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(19)).ToUpper();

                    srMedicationTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(26)).ToUpper();
                    srInstructionsTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(27)).ToUpper();
                    srOtherTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(28)).ToUpper();

                    srOtherRestrictionsTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(48)).ToUpper();

                    srClincTypeDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(9));

                    srVistTypeDrp.SelectedValue = objSqlDataRdr.GetValue(23).ToString();


                    srTimesPerWeekDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(24));

                    srWorkStatusDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(29));
                    if (objSqlDataRdr.GetValue(29).ToString() == "3")
                        ciModifiedRestrictionsPanel.Visible = true;

                    if (objSqlDataRdr.GetValue(29).ToString() == "3" || objSqlDataRdr.GetValue(29).ToString() == "2")
                    {
                        srUntilDateTxt.Visible = true;
                        ciModifiedRestrictionsPanel.Visible = true;
                    }

                    srCanCarryDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(32));
                    if (objSqlDataRdr.GetValue(32).ToString() == "7")
                    {
                        srCanCarryLblCell.Visible = true;
                        srCanCarryTxtCell.Visible = true;
                        srCanCarryOtherTxt.Text = objSqlDataRdr.GetValue(59).ToString();
                    }
                    else
                    {
                        srCanCarryLblCell.Visible = false;
                        srCanCarryTxtCell.Visible = false;
                        srCanCarryOtherTxt.Text = "";
                    }
                    //canliftcarry
                    srCanLiftDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(32));
                    if (objSqlDataRdr.GetValue(32).ToString() == "7")
                    {
                        srCanLiftLblCell.Visible = true;
                        srCanLiftTxtCell.Visible = true;
                        srCanLiftOtherTxt.Text = objSqlDataRdr.GetValue(59).ToString();
                    }
                    else
                    {
                        srCanLiftLblCell.Visible = false;
                        srCanLiftTxtCell.Visible = false;
                        srCanLiftOtherTxt.Text = "";
                    }

                    srSimpleGraspingDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(33));
                    if (objSqlDataRdr.GetValue(33).ToString() == "5")
                    {
                        srSimpleGraspingLblCell.Visible = true;
                        srSimpleGraspingTxtCell.Visible = true;
                        srSimpleGraspingOtherTxt.Text = objSqlDataRdr.GetValue(60).ToString();
                    }
                    else
                    {
                        srSimpleGraspingLblCell.Visible = false;
                        srSimpleGraspingTxtCell.Visible = false;
                        srSimpleGraspingOtherTxt.Text = "";
                    }

                    srPushingPullingDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(34));
                    if (objSqlDataRdr.GetValue(34).ToString() == "5")
                    {
                        srPushingPullingLblCell.Visible = true;
                        srPushingPullingTxtCell.Visible = true;
                        srPushingPullingOtherTxt.Text = objSqlDataRdr.GetValue(61).ToString();
                    }
                    else
                    {
                        srPushingPullingLblCell.Visible = false;
                        srPushingPullingTxtCell.Visible = false;
                        srPushingPullingOtherTxt.Text = "";
                    }

                    srFineManipulationDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(35));
                    if (objSqlDataRdr.GetValue(35).ToString() == "5")
                    {
                        srFineManipulationLblCell.Visible = true;
                        srFineManipulationTxtCell.Visible = true;
                        srFineManipulationOtherTxt.Text = objSqlDataRdr.GetValue(62).ToString();
                    }
                    else
                    {
                        srFineManipulationLblCell.Visible = false;
                        srFineManipulationTxtCell.Visible = false;
                        srFineManipulationOtherTxt.Text = "";
                    }


                    srRepeatRFootWorkDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(36));
                    if (objSqlDataRdr.GetValue(36).ToString() == "3")
                    {
                        srRepeatRFootWorkLblCell.Visible = true;
                        srRepeatRFootWorkTxtCell.Visible = true;
                        srRepeatRFootWorkOtherTxt.Text = objSqlDataRdr.GetValue(63).ToString();
                    }
                    else
                    {
                        srRepeatRFootWorkLblCell.Visible = false;
                        srRepeatRFootWorkTxtCell.Visible = false;
                        srRepeatRFootWorkOtherTxt.Text = "";
                    }

                    srRepeatLFootWorkDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(37));
                    if (objSqlDataRdr.GetValue(37).ToString() == "3")
                    {
                        srRepeatLFootWorkLblCell.Visible = true;
                        srRepeatLFootWorkTxtCell.Visible = true;
                        srRepeatLFootWorkOtherTxt.Text = objSqlDataRdr.GetValue(64).ToString();
                    }
                    else
                    {
                        srRepeatLFootWorkLblCell.Visible = false;
                        srRepeatLFootWorkTxtCell.Visible = false;
                        srRepeatLFootWorkOtherTxt.Text = "";
                    }

                    srCanSitPerDayDrp.SelectedValue = objSqlDataRdr.GetValue(39).ToString();
                    if (objSqlDataRdr.GetValue(39).ToString() == "13")
                    {
                        srCanSitPerDayLblCell.Visible = true;
                        srCanSitPerDayTxtCell.Visible = true;
                        srCanSitPerDayOtherTxt.Text = objSqlDataRdr.GetValue(65).ToString();
                    }
                    else
                    {
                        srCanSitPerDayLblCell.Visible = false;
                        srCanSitPerDayTxtCell.Visible = false;
                        srCanSitPerDayOtherTxt.Text = "";
                    }

                    srCanStandPerDayDrp.SelectedValue = objSqlDataRdr.GetValue(40).ToString();;
                    if (objSqlDataRdr.GetValue(40).ToString() == "13")
                    {
                        srCanStandPerDayLblCell.Visible = true;
                        srCanStandPerDayTxtCell.Visible = true;
                        srCanStandPerDayOtherTxt.Text = objSqlDataRdr.GetValue(66).ToString();
                    }
                    else
                    {
                        srCanStandPerDayLblCell.Visible = false;
                        srCanStandPerDayTxtCell.Visible = false;
                        srCanStandPerDayOtherTxt.Text = "";
                    }

                    srCanWalkPerDayDrp.SelectedValue = objSqlDataRdr.GetValue(85).ToString();
                    if (objSqlDataRdr.GetValue(85).ToString() == "13")
                    {
                        srCanWalkPerDayLblCell.Visible = true;
                        srCanWalkPerDayTxtCell.Visible = true;
                        srCanWalkPerDayOtherTxt.Text = objSqlDataRdr.GetValue(86).ToString();
                    }
                    else
                    {
                        srCanWalkPerDayLblCell.Visible = false;
                        srCanWalkPerDayTxtCell.Visible = false;
                        srCanWalkPerDayOtherTxt.Text = "";
                    }

                    srCanDrivePerDayDrp.SelectedValue = objSqlDataRdr.GetValue(41).ToString();
                    if (objSqlDataRdr.GetValue(41).ToString() == "13")
                    {
                        srCanDrivePerDayLblCell.Visible = true;
                        srCanDrivePerDayTxtCell.Visible = true;
                        srCanDrivePerDayOtherTxt.Text = objSqlDataRdr.GetValue(67).ToString();
                    }
                    else
                    {
                        srCanDrivePerDayLblCell.Visible = false;
                        srCanDrivePerDayTxtCell.Visible = false;
                        srCanDrivePerDayOtherTxt.Text = "";
                    }

                    srCanBendDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(42));
                    if (objSqlDataRdr.GetValue(42).ToString() == "4")
                    {
                        srCanBendLblCell.Visible = true;
                        srCanBendTxtCell.Visible = true;
                        srCanBendOtherTxt.Text = objSqlDataRdr.GetValue(68).ToString();
                    }
                    else
                    {
                        srCanBendLblCell.Visible = false;
                        srCanBendTxtCell.Visible = false;
                        srCanBendOtherTxt.Text = "";
                    }

                    srCanSquatDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(43));
                    if (objSqlDataRdr.GetValue(44).ToString() == "4")
                    {
                        srCanSquatLblCell.Visible = true;
                        srCanSquatTxtCell.Visible = true;
                        srCanSquatOtherTxt.Text = objSqlDataRdr.GetValue(71).ToString();
                    }
                    else
                    {
                        srCanSquatLblCell.Visible = false;
                        srCanSquatTxtCell.Visible = false;
                        srCanSquatOtherTxt.Text = "";
                    }

                    srCanClimbStairsDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(44));
                    if (objSqlDataRdr.GetValue(44).ToString() == "4")
                    {
                        srCanClimbStairsLblCell.Visible = true;
                        srCanClimbStairsTxtCell.Visible = true;
                        srCanClimbStairsOtherTxt.Text = objSqlDataRdr.GetValue(69).ToString();
                    }
                    else
                    {
                        srCanClimbStairsLblCell.Visible = false;
                        srCanClimbStairsTxtCell.Visible = false;
                        srCanClimbStairsOtherTxt.Text = "";
                    }

                    srCanClimbLadderDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(45));
                    if (objSqlDataRdr.GetValue(45).ToString() == "4")
                    {
                        srCanClimbLadderLblCell.Visible = true;
                        srCanClimbLadderTxtCell.Visible = true;
                        srCanClimbLadderOtherTxt.Text = objSqlDataRdr.GetValue(72).ToString();
                    }
                    else
                    {
                        srCanClimbLadderLblCell.Visible = false;
                        srCanClimbLadderTxtCell.Visible = false;
                        srCanClimbLadderOtherTxt.Text = "";
                    }

                    srCanCrawlDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(46));
                    if (objSqlDataRdr.GetValue(46).ToString() == "4")
                    {
                        srCanCrawlLblCell.Visible = true;
                        srCanCrawlTxtCell.Visible = true;
                        srCanCrawlOtherTxt.Text = objSqlDataRdr.GetValue(70).ToString();
                    }
                    else
                    {
                        srCanCrawlLblCell.Visible = false;
                        srCanCrawlTxtCell.Visible = false;
                        srCanCrawlOtherTxt.Text = "";
                    }

                    srCanReachDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(47));
                    if (objSqlDataRdr.GetValue(47).ToString() == "4")
                    {
                        srCanReachlLblCell.Visible = true;
                        srCanReachCell.Visible = true;
                        srCanReachOtherTxt.Text = objSqlDataRdr.GetValue(73).ToString();
                    }
                    else
                    {
                        srCanReachlLblCell.Visible = false;
                        srCanReachCell.Visible = false;
                        srCanReachOtherTxt.Text = "";
                    }
                    srReferredToSpecialistDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(49));

                    srReferredDrNameTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(87).ToString());
                    srSpecialistApptDateTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(91).ToString());

                    DateTime result;
                    if (DateTime.TryParse(srSpecialistApptDateTxt.Text, out result))
                        srSpecialistApptDateTxt.Text = srSpecialistApptDateTxt.Text;

                    if (objSqlDataRdr.GetValue(88).ToString() != null)
                    {
                        srSpecialistApptHourDrp.SelectedValue = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(88).ToString());
                        srSpecialistApptMinuteDrp.SelectedValue = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(89).ToString());
                        srSpecialistApptAmPmDrp.SelectedValue = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(90).ToString());
                    }

                    if (Convert.ToInt32(objSqlDataRdr.GetValue(25)) > 0)
                        srTherapyWeeksTxt.Text = objSqlDataRdr.GetValue(25).ToString();


                    srEffectiveDateTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(30).ToString());
                    if (DateTime.TryParse(srEffectiveDateTxt.Text, out result))
                        srEffectiveDateTxt.Text = srEffectiveDateTxt.Text;
                    srUntilDateTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(31).ToString()).ToUpper();
                    srNextAptDateTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(51).ToString()).ToUpper();

                    srTypeOfSpecialistTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(50).ToString()).ToUpper();
                    srNextAptHourDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(55));
                    srNextAptMinuteDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(56));
                    srNextAptAmPmDrp.SelectedIndex = Convert.ToInt32(objSqlDataRdr.GetValue(57));

                    if (objSqlDataRdr.GetValue(13).ToString() == "1")
                        srDiagXRayChk.Checked = true;

                    if (objSqlDataRdr.GetValue(14).ToString() == "1")
                        srDiagMRIChk.Checked = true;

                    if (objSqlDataRdr.GetValue(15).ToString() == "1")
                        srDiagEMGNCVChk.Checked = true;

                    if (objSqlDataRdr.GetValue(16).ToString() == "1")
                        srDiagOtherChk.Checked = true;

                    if (objSqlDataRdr.GetValue(20).ToString() == "1")
                        srPTChk.Checked = true;

                    if (objSqlDataRdr.GetValue(21).ToString() == "1")
                        srOTChk.Checked = true;

                    if (objSqlDataRdr.GetValue(22).ToString() == "1")
                        srWHChk.Checked = true;
                    srTimeInTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(83).ToString());
                    srTimeOutTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(84).ToString());
                    srDiagBodyPartTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(92).ToString());
                    srClinicNameTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(94).ToString());
                    srReasonForReferraltTxt.Text = UtilityFunctions.myCStr(objSqlDataRdr.GetValue(97).ToString());

                }

                objSqlDataRdr.Close();
                objSqlDataRdr.Dispose();

                viewit();

                /*'Catch ex As Exception
                'Response.Write("Can't load Web page " & ex.Message)
                ' Finally
                objConnection.Close()
                objConnection.Dispose()
                'End Try*/
            }
            else
            {
                OdbcCommand objSqlCmd = new OdbcCommand();
                OdbcDataReader objSqlDataRdr = null;
                string strConnection = ConnectionStrings.MySqlConnections();
                OdbcConnection objConnection = new OdbcConnection(strConnection);

                ipCustAccountID.Value = e.CommandArgument.ToString();

                objConnection.Open();
                objSqlCmd.Connection = objConnection;
                //' get folder;
                objSqlCmd = new OdbcCommand("", objConnection);
                objSqlCmd.CommandText = "{call sp_getEmployerPartyIDbyCustomerID(?)}";
                objSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
                objSqlCmd.Parameters.Add(new OdbcParameter("@IPCUSTACCTID", OdbcType.VarChar, 255)).Value = ipCustAccountID.Value;
                objSqlDataRdr = objSqlCmd.ExecuteReader();
                int emp_id;

                if (objSqlDataRdr.Read()) 
                {
                    emp_id = Convert.ToInt32(objSqlDataRdr.GetValue(0));
                    //'Response.Write("The emp_id is: " & emp_id & "The cust acct id is: " & ipCustAccountID.Value & "<BR>")

                    GetTriagePDFs(emp_id.ToString(), ipCustAccountID.Value);

                    //Response.Write("The full file name is: " + FullFileName & "<BR>")

                    if (this._fullfilename != null)
                    {
                        this._fullfilename = this._fullfilename.Replace("C:\\inetpub\\wwwroot\\myeworkwell\triage", string.Empty);
                        this._fullfilename = this._fullfilename.Replace("\\", "//");
                    
                        string popupScript = "<script language=javascript> window.open('" + this._fullfilename + "') </script>";

                        ClientScript.RegisterStartupScript(this.GetType(), "callpopup", popupScript);
                    }
                    else
                    {
                        //' no Triage Reports found
                        error_notification.Visible = true;
                        error_notification.Text = "No Triage Reports found!";
                        error_notification.Show();
                    }
                }
                else
                {
                    //' no Triage Reports found
                    error_notification.Title = "Employer not found!";
                }
            }

        }

            //'Else
            //'    Dim popupScript As String = "<script language=javascript> window.open('/PDFs/JOSEPH_SMITH_TRIAGE_4_18_2012.pdf') </script>"
            //'    ClientScript.RegisterStartupScript(Me.[GetType](), "callpopup", popupScript)

    }
    
    public void GetTriagePDFs(string emp_id, string cust_account_id)
    {
        string DocumentVirtualPath = "\\triage\\" + emp_id +  "\\" + cust_account_id + "\\";
        GetFiles("\\triage\\" + emp_id + "\\" + cust_account_id + "\\");
    }
    public void GetFiles(string path)
    {
        string myPath = Server.MapPath(path);

        if (File.Exists(myPath))
        {
            // This path is a file 
            ProcessFile(myPath);
        }
        else if ( Directory.Exists(myPath))
        {
            // This path is a directory 
            ProcessDirectory(myPath);
        }
    }
    public void ProcessFile(string path)
    {
        FileInfo fi = new FileInfo(path);

        if (fi.FullName.ToLower().Contains(".pdf"))
        {
            this.filename = fi.Name;
            //Response.Write("The file name is: " + filename & "<BR>")
            //Might need to look back at this.
            this._fullfilename = fi.FullName.Replace("D:\\Hosting\\6546477\\html", "");
            //Response.Write("The full file name is: " + FullFileName & "<BR>")
        }
    }
    public void ProcessDirectory(string targetDirectory)
    {
        string[] fileEntries = Directory.GetFiles(targetDirectory, "*.pdf");
        string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);

        // Process the list of files found in the directory. 
        foreach (string fileName in fileEntries)
        {
            ProcessFile(fileName);
            //This is a mystery will come back after converting it all.
            break; // only get one file if more than one
        }

        // Recurse into subdirectories of this directory. 
        foreach (string subdirectory in subdirectoryEntries)
        {
            ProcessDirectory(subdirectory);
        }
    }
    protected void ciStatusRptViewGrd_RowDataBound(Object sender , System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {           
                string sSSNumber = UtilityFunctions.myCStr(DataBinder.Eval(e.Row.DataItem, "SOCIALSECURITYNUMBER"));
                string sFName = UtilityFunctions.myCStr(DataBinder.Eval(e.Row.DataItem, "IPFIRSTNAME")).Trim();
                string sLName = UtilityFunctions.myCStr(DataBinder.Eval(e.Row.DataItem, "IPLASTNAME")).Trim();
                string sMName = UtilityFunctions.myCStr(DataBinder.Eval(e.Row.DataItem, "IPMIDDLENAME")).Trim();
                string sReportType = UtilityFunctions.myCStr(DataBinder.Eval(e.Row.DataItem, "REPORT_TYPE")).Trim();

                if (sReportType == "0")
                    sReportType = "Initial Appt";
                else if (sReportType == "1")
                    sReportType = "Follow Up Appt";
                    //'e.Row.Cells(5).Visible = False
                else if (sReportType == "2")
                    sReportType = "R.T.W. Appt";
                    //e.Row.Cells(5).Visible = False
                else
                    sReportType = "UNKNOWN";
                    //e.Row.Cells(5).Visible = False

                DataRow dr = ((DataRowView)(e.Row.DataItem)).Row;

                if (sMName != null)
                    if (sMName.Length == 1)
                        sMName = sMName + ".";

                if (sSSNumber.Trim() != null)
                    if (sSSNumber.Length >= 7)
                        //Response.Write(sSSNumber.Length)
                        sSSNumber = "XXX-XX-" + sSSNumber.Substring(7);
                    else
                        if (sSSNumber.Trim() == "--")
                            sSSNumber = "XXX-XX-XXXX";
                else
                    sSSNumber = "XXX-XX-XXXX";
                e.Row.Cells[0].Text = sLName + ", " + sFName + " " + sMName;
                e.Row.Cells[4].Text = sSSNumber;
                e.Row.Cells[5].Text = sReportType;
            }
            catch (Exception ex)
            {
                Response.Write("Error RowDataBound" + ex.ToString() + "<BR>");
            }
        }
    }
    public void viewit()
    {
        //Dim fStatusReport As New Document()
        //Dim sPath As String = Server.MapPath("PDFs")
        //PdfWriter.GetInstance(fStatusReport, New FileStream(sPath & "/StatusReport.pdf", FileMode.Create))
        //Dim c1 As New Chunk("Employee Name: " & ipFirstNameTxt.Text)
        //c1.SetUnderline(0.5F, -1.5F)


        //Dim c1 As New Chunk("Employee Name: ")

        //fStatusReport.Add(c1)

        string pdfPath = Server.MapPath("PDFs");
        string imgPath = Server.MapPath("images");
        string fontpath = Server.MapPath("fonts");


        //Dim r As New Rectangle(400, 200)
        Document doc = new Document(PageSize.LETTER, 50, 30, 10, 10);
        //Change so that this equals below (//string sFileName = "/block2" + String.Replace(Replace(System.DateTime.Now.ToString(), "/", "_"), ":", "-") + ".pdf";)
        //Has to be more seperated in vb.
        string sFileName = System.DateTime.Now.ToString();
        sFileName = sFileName.Replace("/", "_");
        sFileName = sFileName.Replace(":", "-") + ".pdf";
        
        string sFileName2 = "/block2.pdf";
        //Response.Write(sFileName)

        PdfWriter.GetInstance(doc, new FileStream(pdfPath + sFileName, FileMode.Create));
        doc.Open();

        BaseFont customfont = BaseFont.CreateFont(fontpath + "/calibri.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);

        Font font = new Font(customfont, 6);

        PdfPTable ipEmpInfoTbl = new PdfPTable(4);
        PdfPTable ipAGHInfoTbl = new PdfPTable(2);
        Single[] ipAGHWidths  = new Single[] {125.0F, 200.0F};
        ipAGHInfoTbl.TotalWidth = 325.0F;
        ipAGHInfoTbl.HorizontalAlignment = Element.ALIGN_LEFT;
        ipAGHInfoTbl.SetWidths(ipAGHWidths);
        ipAGHInfoTbl.LockedWidth = true;



        //Dim hdrCell As New PdfPCell(New Phrase("WorkWell  Physicians, PC"))
        //Dim empInfoHdrCell As New PdfPCell(Image.GetInstance(imgPath + "/tiny-meww-header-transport-small copy.png"))
        //Dim empInfoHdrCell As New PdfPCell(Image.GetInstance(imgPath + "/workwell_inc.png"))

        if (srClincTypeDrp.SelectedValue == "1")
        {
            Single[] ipCompNameWidths = new Single[] {315.0F, 45.0F};
            PdfPTable ipCompNameHdrTbl = new PdfPTable(2);
            ipCompNameHdrTbl.TotalWidth = 360.0F;
            ipCompNameHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            ipCompNameHdrTbl.AddCell(getCompanyNameCell(0, 0));
            ipCompNameHdrTbl.AddCell(getCompanyNameCell(1, 0));
            ipCompNameHdrTbl.SetWidths(ipCompNameWidths);
            //ipCompNameHdrTbl.AddCell(ipIncLogoCell);
            ipCompNameHdrTbl.LockedWidth = true;
            doc.Add(ipCompNameHdrTbl);
        }
        else
        {
            Single[] ipCompNameWidths = new Single[] {245.0F, 450.0F};
            PdfPTable ipCompNameHdrTbl = new PdfPTable(2);
            ipCompNameHdrTbl.TotalWidth = 695.0F;
            ipCompNameHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            ipCompNameHdrTbl.AddCell(getCompanyNameCell(0, 0));
            ipCompNameHdrTbl.AddCell(getCompanyNameCell(2, 0));
            ipCompNameHdrTbl.SetWidths(ipCompNameWidths);
            //ipCompNameHdrTbl.AddCell(ipIncLogoCell);
            ipCompNameHdrTbl.LockedWidth = true;
            doc.Add(ipCompNameHdrTbl);
        }




        //Dim ipEmpWidths As Single() = New Single() {75.0F, 200.0F, 75.0F, 250.0F}


        PdfPCell cell = new PdfPCell(new Phrase("Employee Name:", font));
        Phrase empNameHdr = new Phrase("Employer Name:", font);
        Phrase ssClaimNumHdr = new Phrase("SS#/Claim#:", font);
        Phrase ipDOEHdr = new Phrase("Date of Exam:", font);
        Phrase ipDOIHdr = new Phrase("Date of Injury:", font);
        Phrase ipDrNameHdr = new Phrase("Physician Name:", font);

        //empInfoHdrCell.Border = 0
        cell.Border = 0;
        //Dim cell As New PdfPCell(New Phrase("Employee Name:", font))

        //empInfoHdrCell.Colspan = 4
        //empInfoHdrCell.HorizontalAlignment = 1
        //ipEmpInfoTbl.AddCell(empInfoHdrCell)
        ipEmpInfoTbl.TotalWidth = 500.0F;
        //ipEmpInfoTbl.SetWidths(ipEmpWidths)

        ipEmpInfoTbl.LockedWidth = true;
        ipEmpInfoTbl.AddCell(getCell(" ", 2, true));
        ipEmpInfoTbl.AddCell(getCell(" ", 0, false));
        ipEmpInfoTbl.AddCell(getCell(" ", 2, true));
        ipEmpInfoTbl.AddCell(getCell(" ", 0, false));
        ipEmpInfoTbl.AddCell(getCell("Employee Name:", 2, true));
        ipEmpInfoTbl.AddCell(getCell(srIPFNameTxt.Text.ToUpper() + " " + srIPMNameTxt.Text.ToUpper() + " " + srIPLNameTxt.Text.ToUpper(), 0, false));
        ipEmpInfoTbl.AddCell(getCell("Date of Exam:", 2, true));
        ipEmpInfoTbl.AddCell(getCell(srIPDOETxt.Text.ToUpper(), 0, false));
        ipEmpInfoTbl.AddCell(getCell("Employer Name:", 2, true));
        ipEmpInfoTbl.AddCell(getCell(srIPEmployerNameTxt.Text.ToUpper(), 0, false));
        ipEmpInfoTbl.AddCell(getCell("Date of Injury:", 2, true));
        ipEmpInfoTbl.AddCell(getCell(srIPDOITxt.Text.ToUpper(), 0, false));
        ipEmpInfoTbl.AddCell(getCell("Social Security #:", 2, true));

        if (srSocialSecurityNumTxt.Text != null)
        {
            if (srSocialSecurityNumTxt.Text.Replace("-", "").Length >= 4 )
                ipEmpInfoTbl.AddCell(getCell("XXX-XX-" + srSocialSecurityNumTxt.Text.Replace("-", "").Substring(srSocialSecurityNumTxt.Text.Replace("-", "").Length - 4, 4), 0, false));
            else
                ipEmpInfoTbl.AddCell(getCell("XXX-XX-XXXX", 0, false));
        }
        else
            ipEmpInfoTbl.AddCell(getCell("XXX-XX-XXXX", 0, false));

        ipEmpInfoTbl.AddCell(getCell("Claim #:", 2, true));

        if (srSocialSecurityNumTxt.Text.Trim() == srIPSSNumClaimNumTxt.Text.Trim())
            ipEmpInfoTbl.AddCell(getCell("N/A", 0, false));
        else
            ipEmpInfoTbl.AddCell(getCell(srIPSSNumClaimNumTxt.Text.ToUpper(), 0, false));

        ipEmpInfoTbl.AddCell(getCell("Date of Birth:", 2, true));
        ipEmpInfoTbl.AddCell(getCell(srIPDOBTxt.Text.ToUpper(), 0, false));
        ipEmpInfoTbl.AddCell(getCell(" ", 2, true));
        ipEmpInfoTbl.AddCell(getCell(" ", 0, false));

        ipEmpInfoTbl.AddCell(getCell("Clinic Type:", 2, true));
        ipEmpInfoTbl.AddCell(getCell(srClincTypeDrp.SelectedItem.Text.ToUpper(), 0, false));
        ipEmpInfoTbl.AddCell(getCell(" ", 2, true));
        ipEmpInfoTbl.AddCell(getCell(" ", 0, false));

        ipEmpInfoTbl.AddCell(getCell("Practice Name:", 2, true));
        ipEmpInfoTbl.AddCell(getCell(srClinicNameTxt.Text.ToUpper(), 0, false));
        ipEmpInfoTbl.AddCell(getCell(" ", 2, true));
        ipEmpInfoTbl.AddCell(getCell(" ", 0, false));


        doc.Add(ipEmpInfoTbl);
        if (empPartyID.Value == "222397")
        {
            ipAGHInfoTbl.AddCell(getCell(" ", 2, true));
            ipAGHInfoTbl.AddCell(getCell(" ", 0, false));
            ipAGHInfoTbl.AddCell(getCell("Time In:", 2, true));
            ipAGHInfoTbl.AddCell(getCell(srTimeInTxt.Text, 0, false));
            ipAGHInfoTbl.AddCell(getCell("Time Out:", 2, true));
            ipAGHInfoTbl.AddCell(getCell(srTimeOutTxt.Text, 0, false));

            doc.Add(ipAGHInfoTbl);
        }


        //Dim ipIncLogoCell As New PdfPCell(Image.GetInstance(imgPath + "/logo_inc.png"))


        Single[] ipDiagnosisWidths = new Single[] {105.0F, 350.0F};
        PdfPTable ipDiagHdrTbl = new PdfPTable(1);
        ipDiagHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT;
        ipDiagHdrTbl.TotalWidth = 600.0F;
        ipDiagHdrTbl.AddCell(getHdrCell("Diagnosis", 0));
        doc.Add(ipDiagHdrTbl);

        PdfPTable ipDiagnosisTbl = new PdfPTable(2);

        ipDiagnosisTbl.TotalWidth = 455.0F;
        ipDiagnosisTbl.SetWidths(ipDiagnosisWidths);
        ipDiagnosisTbl.HorizontalAlignment = Element.ALIGN_LEFT;
        ipDiagnosisTbl.LockedWidth = true;

        ipDiagnosisTbl.LockedWidth = true;

        ipDiagnosisTbl.AddCell(getCell(" ", 2, true));
        ipDiagnosisTbl.AddCell(getCell(" ", 0, false));

        ipDiagnosisTbl.AddCell(getCell("Body Part:", 2, true));
        ipDiagnosisTbl.AddCell(getCell(srBodyPartTxt.Text.ToUpper(), 0, false));

        if (srDiagonsisTxt.Text.ToUpper() != null)
        {
            ipDiagnosisTbl.AddCell(getCell("Diagnosis:", 2, true));
            ipDiagnosisTbl.AddCell(getCell(srDiagonsisTxt.Text.ToUpper(), 0, false));
        }
        if (srObjectiveFindingsTxt.Text != null)
        {
            ipDiagnosisTbl.AddCell(getCell("Objective Findings:", 2, true));
            ipDiagnosisTbl.AddCell(getCell(srObjectiveFindingsTxt.Text.ToUpper(), 0, false));
        }

        if (srMechanismOfInjuryTxt.Text != null)
        {
            ipDiagnosisTbl.AddCell(getCell("Mechanism of Injury:", 2, true));
            ipDiagnosisTbl.AddCell(getCell(srMechanismOfInjuryTxt.Text.ToUpper(), 0, false));
        }

        doc.Add(ipDiagnosisTbl);


        if (srDiagEMGNCVChk.Checked || srDiagMRIChk.Checked || srDiagXRayChk.Checked || srDiagOtherChk.Checked )
        {

            Single[] ipDXWidths = new Single[] {105.0F, 350.0F};
            PdfPTable ipDXHdrTbl = new PdfPTable(1);
            ipDXHdrTbl.TotalWidth = 455.0F;
            ipDXHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            ipDXHdrTbl.AddCell(getCell(" ", 0, false));
            ipDXHdrTbl.AddCell(getHdrCell("Diagnostics", 0));

            doc.Add(ipDXHdrTbl);

            PdfPTable ipDXTbl = new PdfPTable(2);
            ipDXTbl.TotalWidth = 455.0F;
            ipDXTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            ipDXTbl.SetWidths(ipDXWidths);
            ipDXTbl.LockedWidth = true;

            ipDXTbl.AddCell(getCell(" ", 2, true));
            ipDXTbl.AddCell(getCell(" ", 0, false));
            ipDXTbl.AddCell(getCell("Diagnostics Needed:", 2, true));
            ipDXTbl.AddCell(getCell(GetChkBoxNeeded(1), 0, false));

            if (srDiagBodyPartTxt.Text != null)
            {
                ipDXTbl.AddCell(getCell("Body Part:", 2, true));
                ipDXTbl.AddCell(getCell(srDiagBodyPartTxt.Text.ToUpper(), 0, false));
            }

            if (srReasonTxt.Text != null)
            {
                ipDXTbl.AddCell(getCell("Reason:", 2, true));
                ipDXTbl.AddCell(getCell(srReasonTxt.Text.ToUpper(), 0, false));
            }

            doc.Add(ipDXTbl);
        }
        
        if (srPTChk.Checked || srOTChk.Checked || srWHChk.Checked || srVistTypeDrp.SelectedValue != "0" || srTimesPerWeekDrp.SelectedValue != "0" || srTherapyWeeksTxt.Text != null)
        {
            Single[] ipThrpyWidths = new Single[] {105.0F, 400.0F};
            PdfPTable ipThrpyHdrTbl = new PdfPTable(1);
            ipThrpyHdrTbl.TotalWidth = 405.0F;
            ipThrpyHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            ipThrpyHdrTbl.AddCell(getCell(" ", 0, false));
            ipThrpyHdrTbl.AddCell(getHdrCell("Therapy", 0));

            doc.Add(ipThrpyHdrTbl);

            PdfPTable ipTherapyTbl = new PdfPTable(2);
            ipTherapyTbl.TotalWidth = 500.0F;
            ipTherapyTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            ipTherapyTbl.SetWidths(ipThrpyWidths);
            ipTherapyTbl.LockedWidth = true;

            ipTherapyTbl.AddCell(getCell(" ", 2, true));
            ipTherapyTbl.AddCell(getCell(" ", 0, false));
            ipTherapyTbl.AddCell(getCell("Therapy Needed:", 2, true));
            ipTherapyTbl.AddCell(getCell(GetChkBoxNeeded(2), 0, false));
            ipTherapyTbl.AddCell(getCell("Visit Type:", 2, true));
            ipTherapyTbl.AddCell(getCell(srVistTypeDrp.SelectedItem.Text.ToUpper(), 0, false));
            ipTherapyTbl.AddCell(getCell("Times Per Week:", 2, true));
            ipTherapyTbl.AddCell(getCell(srTimesPerWeekDrp.SelectedItem.Text.ToUpper(), 0, false));
            ipTherapyTbl.AddCell(getCell("Weeks Needed:", 2, true));
            ipTherapyTbl.AddCell(getCell(srTherapyWeeksTxt.Text.ToUpper(), 0, false));

            doc.Add(ipTherapyTbl);
        }   

        if (srMedicationTxt.Text != null || srInstructionsTxt.Text != null || srOtherTxt.Text != null)
        {
            Single[] ipTreatmentWidths = new Single[] {105.0F, 350.0F};
            PdfPTable ipTreatmentHdrTbl = new PdfPTable(1);
            ipTreatmentHdrTbl.TotalWidth = 600.0F;
            ipTreatmentHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            ipTreatmentHdrTbl.AddCell(getCell(" ", 0, false));
            ipTreatmentHdrTbl.AddCell(getHdrCell("Treatment", 0));

            doc.Add(ipTreatmentHdrTbl);


            PdfPTable ipTreatmentTbl = new PdfPTable(2);
            ipTreatmentTbl.TotalWidth = 455.0F;
            ipTreatmentTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            ipTreatmentTbl.SetWidths(ipTreatmentWidths);
            ipTreatmentTbl.LockedWidth = true;

            ipTreatmentTbl.AddCell(getCell(" ", 2, true));
            ipTreatmentTbl.AddCell(getCell(" ", 0, false));
            if (srMedicationTxt.Text != null)
            {
                ipTreatmentTbl.AddCell(getCell("Medication:", 2, true));
                ipTreatmentTbl.AddCell(getCell(srMedicationTxt.Text.ToUpper(), 0, false));
            }
            if (srInstructionsTxt.Text != null)
            {
                ipTreatmentTbl.AddCell(getCell("Instructions:", 2, true));
                ipTreatmentTbl.AddCell(getCell(srInstructionsTxt.Text.ToUpper(), 0, false));
            }
            if (srOtherTxt.Text != null)
            {
                ipTreatmentTbl.AddCell(getCell("Other:", 2, true));
                ipTreatmentTbl.AddCell(getCell(srOtherTxt.Text.ToUpper(), 0, false));
            }
            doc.Add(ipTreatmentTbl);
        }

        PdfPTable ipWorkStatusHdrTbl = new PdfPTable(1);
        ipWorkStatusHdrTbl.TotalWidth = 600.0F;
        ipWorkStatusHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT;
        ipWorkStatusHdrTbl.AddCell(getCell(" ", 0, false));
        ipWorkStatusHdrTbl.AddCell(getHdrCell("Work Status", 0));

        doc.Add(ipWorkStatusHdrTbl);

        PdfPTable ipWorkStatusTbl = new PdfPTable(6);
        Single[] ipWorkStatusWidths  = new Single[] {115.0F, 100.0F, 40.0F, 70.0F, 30.0F, 70.0F};
        ipWorkStatusTbl.TotalWidth = 425.0F;
        ipWorkStatusTbl.HorizontalAlignment = Element.ALIGN_LEFT;
        ipWorkStatusTbl.SetWidths(ipWorkStatusWidths);
        ipWorkStatusTbl.LockedWidth = true;

        ipWorkStatusTbl.AddCell(getCell(" ", 2, true));
        ipWorkStatusTbl.AddCell(getCell(" ", 0, false));
        ipWorkStatusTbl.AddCell(getCell(" ", 2, true));
        ipWorkStatusTbl.AddCell(getCell(" ", 0, false));
        ipWorkStatusTbl.AddCell(getCell(" ", 0, true));
        ipWorkStatusTbl.AddCell(getCell(" ", 0, false));
        ipWorkStatusTbl.AddCell(getCell("Work Status:", 2, true));
        ipWorkStatusTbl.AddCell(getCell(srWorkStatusDrp.SelectedItem.Text.ToUpper(), 0, false));
        ipWorkStatusTbl.AddCell(getCell("Effective: ", 2, true));
        DateTime result;
        if (DateTime.TryParse(srEffectiveDateTxt.Text, out result))
            ipWorkStatusTbl.AddCell(getCell(srEffectiveDateTxt.Text.ToUpper(), 0, false));
        else
            ipWorkStatusTbl.AddCell(getCell(srEffectiveDateTxt.Text.ToUpper(), 0, false));
        
        if (srUntilDateTxt.Text != null) 
        {
            ipWorkStatusTbl.AddCell(getCell("Until: ", 2, true));
            if (DateTime.TryParse(srUntilDateTxt.Text,out result))
                ipWorkStatusTbl.AddCell(getCell(UtilityFunctions.ConvertDate(Convert.ToDateTime(srUntilDateTxt.Text.ToUpper())), 0, false));
            else
                ipWorkStatusTbl.AddCell(getCell(srUntilDateTxt.Text.ToUpper(), 0, false));
        }
        else
        {
            ipWorkStatusTbl.AddCell(getCell(" ", 2, true));
            ipWorkStatusTbl.AddCell(getCell(" ", 0, false));
        }

        doc.Add(ipWorkStatusTbl);

        if (srOtherRestrictionsTxt.Text != null)
        {
            PdfPTable ipModifiedDutyCommentsTbl = new PdfPTable(2);
            Single[] ipModifiedDutyCommentWidths = new Single[] {115.0F, 350.0F};
           
            ipModifiedDutyCommentsTbl.TotalWidth = 465.0F;
            ipModifiedDutyCommentsTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            ipModifiedDutyCommentsTbl.SetWidths(ipModifiedDutyCommentWidths);
            ipModifiedDutyCommentsTbl.LockedWidth = true;
            ipModifiedDutyCommentsTbl.AddCell(getCell("Comments/Restrictions:", 2, true));
            ipModifiedDutyCommentsTbl.AddCell(getCell(srOtherRestrictionsTxt.Text.ToUpper(), 0, false));
            doc.Add(ipModifiedDutyCommentsTbl);
        }

        if (srWorkStatusDrp.SelectedValue == "3")
        {
            Single[] ipModifiedDutyWidths = new Single[] {175.0F, 125.0F, 175.0F, 125.0F};
            PdfPTable ipModifiedDutyHdrTbl = new PdfPTable(1);
            ipModifiedDutyHdrTbl.TotalWidth = 600.0F;
            ipModifiedDutyHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            ipModifiedDutyHdrTbl.AddCell(getCell(" ", 0, false));
            ipModifiedDutyHdrTbl.AddCell(getHdrCell("Modified Duty Restrictions", 0));

            doc.Add(ipModifiedDutyHdrTbl);

            Single[] ipModifiedDutyUpperWidths = new Single[] {175.0F, 100.0F, 155.0F, 95.0F};
            PdfPTable ipModifiedDutyUpperTbl = new PdfPTable(4);
            ipModifiedDutyUpperTbl.TotalWidth = 488.0F;
            ipModifiedDutyUpperTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            ipModifiedDutyUpperTbl.SetWidths(ipModifiedDutyUpperWidths);

            Single[] ipModifiedDutyInnerWidths = new Single[] {175.0F, 100.0F, 155.0F, 95.0F};
            PdfPTable ipModifiedDutyInnerTbl = new PdfPTable(4);
            ipModifiedDutyInnerTbl.TotalWidth = 550.0F;
            ipModifiedDutyInnerTbl.SetWidths(ipModifiedDutyInnerWidths);

            Single[] ipModifiedDutyOtherInnerWidths = new Single[] {175.0F, 150.0F, 175.0F, 150.0F};
            PdfPTable ipModifiedDutyOtherInnerTbl = new PdfPTable(4);
            ipModifiedDutyOtherInnerTbl.TotalWidth = 650.0F;
            ipModifiedDutyOtherInnerTbl.SetWidths(ipModifiedDutyOtherInnerWidths);

            Single[] ipModifiedCanLiftOtherWidths = new Single[] {175.0F, 475.0F};
            PdfPTable ipModifiedCanLiftOtherTbl = new PdfPTable(2);
            ipModifiedCanLiftOtherTbl.TotalWidth = 650.0F;
            ipModifiedCanLiftOtherTbl.SetWidths(ipModifiedCanLiftOtherWidths);

            ipModifiedDutyUpperTbl.AddCell(getCell(" ", 2, true));
            ipModifiedDutyUpperTbl.AddCell(getCell(" ", 0, false));
            ipModifiedDutyUpperTbl.AddCell(getCell(" ", 2, true));
            ipModifiedDutyUpperTbl.AddCell(getCell(" ", 0, false));

            Single[] ipModifiedAGHWidths = new Single[] {500.0F};
            PdfPTable ipModifiedAGHTbl = new PdfPTable(1);
            ipModifiedAGHTbl.TotalWidth = 500.0F;
            ipModifiedAGHTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            ipModifiedAGHTbl.SetWidths(ipModifiedAGHWidths);

            if (empPartyID.Value == "222397")
            {
                ipModifiedAGHTbl.AddCell(getCell("Please contact Barbara Shealey at 412-359-4522 regarding work restrictions.", 2, true));
                ipModifiedAGHTbl.AddCell(getCell(" ", 2, true));
                doc.Add(ipModifiedAGHTbl);
            }

            if (srCanLiftDrp.SelectedValue != "0" )
            {
                if (srCanLiftDrp.SelectedValue != "7")
                {
                    ipModifiedCanLiftOtherTbl.AddCell(getCell("Can Lift:", 2, true));
                    ipModifiedCanLiftOtherTbl.AddCell(getCell(srCanLiftDrp.SelectedItem.Text.ToUpper(), 0, false));
                }
                else
                {
                    ipModifiedCanLiftOtherTbl.AddCell(getCell("Can Lift:", 2, true));
                    ipModifiedCanLiftOtherTbl.AddCell(getCell(srCanLiftOtherTxt.Text.ToUpper(), 0, false));
                }
            }

            //canlifycarry
            if (srCanCarryDrp.SelectedValue != "0")
            {
                if (srCanCarryDrp.SelectedValue != "7")
                {
                    ipModifiedCanLiftOtherTbl.AddCell(getCell("Can Carry:", 2, true));
                    ipModifiedCanLiftOtherTbl.AddCell(getCell(srCanCarryDrp.SelectedItem.Text.ToUpper(), 0, false));
                }
                else
                {
                    ipModifiedCanLiftOtherTbl.AddCell(getCell("Can Carry:", 2, true));
                    ipModifiedCanLiftOtherTbl.AddCell(getCell(srCanCarryOtherTxt.Text.ToUpper(), 0, false));
                }
            }

            if (srCanLiftDrp.SelectedValue != "0" || srCanCarryDrp.SelectedValue != "0" )
                doc.Add(ipModifiedCanLiftOtherTbl);
            if (srSimpleGraspingDrp.SelectedValue != "0" || srRepeatLFootWorkDrp.SelectedValue != "0" || srRepeatRFootWorkDrp.SelectedValue != "0" || srPushingPullingDrp.SelectedValue != "0" || srFineManipulationDrp.SelectedValue != "0")
            {
                if (srSimpleGraspingDrp.SelectedValue == "5" || srRepeatLFootWorkDrp.SelectedValue == "3" || srRepeatRFootWorkDrp.SelectedValue == "3" || srPushingPullingDrp.SelectedValue == "5" || srFineManipulationDrp.SelectedValue == "5")
                {
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell("Patient can use hands repetively:", 2, true));
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 0, false));
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell("Patient can use feet repetively:", 2, true));
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 0, false));

                    if (srSimpleGraspingDrp.SelectedValue != "5")
                    {
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Simple Grasping: ", 2, true));
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srSimpleGraspingDrp.SelectedItem.Text.ToUpper(), 0, false));
                    }
                    else
                    {
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Simple Grasping: ", 2, true));
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srSimpleGraspingOtherTxt.Text.ToUpper(), 0, false));;
                    }

                    if (srRepeatLFootWorkDrp.SelectedValue != "3")
                    {
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(" Left Foot: ", 2, true));
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srRepeatLFootWorkDrp.SelectedItem.Text.ToUpper(), 0, false));
                    }
                    else
                    {
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(" Left Foot: ", 2, true));
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srRepeatLFootWorkOtherTxt.Text.ToUpper(), 0, false));
                    }

                    if (srPushingPullingDrp.SelectedValue != "5")
                    {
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Pushing/Pulling: ", 2, true));
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srPushingPullingDrp.SelectedItem.Text.ToUpper(), 0, false));
                    }
                    else
                    {
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Pushing/Pulling: ", 2, true));
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srPushingPullingOtherTxt.Text.ToUpper(), 0, false));
                    }

                    if (srRepeatRFootWorkDrp.SelectedValue != "3")
                    {
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(" Right Foot: ", 2, true));
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srRepeatRFootWorkDrp.SelectedItem.Text.ToUpper(), 0, false));
                    }
                    else
                    {
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(" Right Foot: ", 2, true));
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srRepeatRFootWorkOtherTxt.Text.ToUpper(), 0, false));
                    }

                    if (srFineManipulationDrp.SelectedValue != "5")
                    {
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Fine Manipulation: ", 2, true));
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srFineManipulationDrp.SelectedItem.Text.ToUpper(), 0, false));
                    }
                    else
                    {
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell("Fine Manipulation: ", 2, true));
                        ipModifiedDutyOtherInnerTbl.AddCell(getCell(srFineManipulationOtherTxt.Text.ToUpper(), 0, false));
                    }


                    ipModifiedDutyOtherInnerTbl.AddCell(getCell("  ", 2, false));
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 0, false));

                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 2, true));
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 0, false));
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 2, true));
                    ipModifiedDutyOtherInnerTbl.AddCell(getCell(" ", 0, false));

                    doc.Add(ipModifiedDutyOtherInnerTbl);
                }
                else
                {
                    ipModifiedDutyInnerTbl.AddCell(getCell("Patient can use hands repetively:", 2, true));
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 0, false));
                    ipModifiedDutyInnerTbl.AddCell(getCell("Patient can use feet repetively:", 2, true));
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 0, false));

                    ipModifiedDutyInnerTbl.AddCell(getCell("Simple Grasping: ", 2, true));
                    ipModifiedDutyInnerTbl.AddCell(getCell(srSimpleGraspingDrp.SelectedItem.Text.ToUpper(), 0, false));
                    ipModifiedDutyInnerTbl.AddCell(getCell(" Left Foot: ", 2, true));
                    ipModifiedDutyInnerTbl.AddCell(getCell(srRepeatLFootWorkDrp.SelectedItem.Text.ToUpper(), 0, false));
                    ipModifiedDutyInnerTbl.AddCell(getCell("Pushing/Pulling: ", 2, true));
                    ipModifiedDutyInnerTbl.AddCell(getCell(srPushingPullingDrp.SelectedItem.Text.ToUpper(), 0, false));
                    ipModifiedDutyInnerTbl.AddCell(getCell("Right Foot: ", 2, true));
                    ipModifiedDutyInnerTbl.AddCell(getCell(srRepeatRFootWorkDrp.SelectedItem.Text.ToUpper(), 0, false));
                    ipModifiedDutyInnerTbl.AddCell(getCell("Fine Manipulation: ", 2, true));
                    ipModifiedDutyInnerTbl.AddCell(getCell(srFineManipulationDrp.SelectedItem.Text.ToUpper(), 0, false));
                    ipModifiedDutyInnerTbl.AddCell(getCell("  ", 2, false));
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 0, false));

                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 2, true));
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 0, false));
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 2, true));
                    ipModifiedDutyInnerTbl.AddCell(getCell(" ", 0, false));

                    doc.Add(ipModifiedDutyInnerTbl);
                }
            }

            if (srCanSitPerDayDrp.SelectedValue != "-1" || srCanStandPerDayDrp.SelectedValue != "-1" || srCanDrivePerDayDrp.SelectedValue != "-1" || srCanWalkPerDayDrp.SelectedValue != "-1")
            {            
                PdfPTable ipModifiedDutyMiddleTbl = new PdfPTable(4);
                ipModifiedDutyMiddleTbl.TotalWidth = 600.0F;
                ipModifiedDutyMiddleTbl.SetWidths(ipModifiedDutyWidths);

                //In a work day patient is able to:
                ipModifiedDutyMiddleTbl.AddCell(getCell("In a work day patient is able to:", 2, true));
                ipModifiedDutyMiddleTbl.AddCell(getCell(" ", 0, false));
                ipModifiedDutyMiddleTbl.AddCell(getCell(" ", 2, true));
                ipModifiedDutyMiddleTbl.AddCell(getCell(" ", 0, false));

                doc.Add(ipModifiedDutyMiddleTbl);

                if (srCanSitPerDayDrp.SelectedValue == "13" || srCanStandPerDayDrp.SelectedValue == "13" || srCanDrivePerDayDrp.SelectedValue == "13" || srCanWalkPerDayDrp.SelectedValue == "13")
                {
                    Single[] ipModifiedDutyInWorkDayOtherWidths = new Single[] {175.0F, 400.0F};
                    PdfPTable ipModifiedDutyInWorkDayOtherTbl = new PdfPTable(2);
                    ipModifiedDutyInWorkDayOtherTbl.TotalWidth = 600.0F;
                    ipModifiedDutyInWorkDayOtherTbl.SetWidths(ipModifiedDutyInWorkDayOtherWidths);

                    if (srCanSitPerDayDrp.SelectedValue != "-1" && srCanSitPerDayDrp.SelectedValue != "13")
                    {
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("      Sit: ", 2, true));
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanSitPerDayDrp.SelectedItem.Text.ToUpper() + " hrs/day", 0, false));
                    }
                    else if (srCanSitPerDayDrp.SelectedValue == "13")
                    {
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("      Sit: ", 2, true));
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanSitPerDayOtherTxt.Text.ToUpper(), 0, false));
                    }

                    if (srCanStandPerDayDrp.SelectedValue != "-1" && srCanStandPerDayDrp.SelectedValue != "13")
                    {
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("Stand: ", 2, true));
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanStandPerDayDrp.SelectedItem.Text.ToUpper() + " hrs/day", 0, false));
                    }
                    else if (srCanStandPerDayDrp.SelectedValue == "13")
                    {
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("Stand: ", 2, true));
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanStandPerDayOtherTxt.Text.ToUpper(), 0, false));
                    }

                    if (srCanWalkPerDayDrp.SelectedValue != "-1" && srCanWalkPerDayDrp.SelectedValue != "13")
                    {
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("Walk: ", 2, true));
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanWalkPerDayDrp.SelectedItem.Text.ToUpper() + " hrs/day", 0, false));
                    }
                    else if (srCanWalkPerDayDrp.SelectedValue == "13")
                    {
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell("Walk: ", 2, true));
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanWalkPerDayOtherTxt.Text.ToUpper(), 0, false));
                    }

                    if (srCanDrivePerDayDrp.SelectedValue != "-1" && srCanDrivePerDayDrp.SelectedValue != "13")
                    {
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(" Drive: ", 2, true));
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanDrivePerDayDrp.SelectedItem.Text.ToUpper() + " hrs/day", 0, false));
                    }
                    else if (srCanDrivePerDayDrp.SelectedValue == "13")
                    {
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(" Drive: ", 2, true));
                        ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(srCanDrivePerDayOtherTxt.Text.ToUpper(), 0, false));
                    }

                    ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(" ", 2, true));
                    ipModifiedDutyInWorkDayOtherTbl.AddCell(getCell(" ", 0, false));

                    doc.Add(ipModifiedDutyInWorkDayOtherTbl);
                }
                else
                {
                    Single[] ipModifiedDutyInWorkDayWidths = new Single[] {175.0F, 175.0F, 225.0F};
                    PdfPTable ipModifiedDutyInWorkDayTbl = new PdfPTable(3);
                    ipModifiedDutyInWorkDayTbl.TotalWidth = 600.0F;
                    ipModifiedDutyInWorkDayTbl.SetWidths(ipModifiedDutyInWorkDayWidths);

                    if (srCanSitPerDayDrp.SelectedValue != "-1")
                    {
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell("      Sit: ", 2, true));
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(srCanSitPerDayDrp.SelectedItem.Text.ToUpper() + " hrs/day", 0, false));
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, true));
                    }
                    
                    if (srCanStandPerDayDrp.SelectedValue != "-1")
                    {
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell("Stand: ", 2, true));
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(srCanStandPerDayDrp.SelectedItem.Text.ToUpper() + " hrs/day", 0, false));
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, true));
                    }

                    if (srCanWalkPerDayDrp.SelectedValue != "-1")
                    {
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell("Walk: ", 2, true));
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(srCanWalkPerDayDrp.SelectedItem.Text.ToUpper() + " hrs/day", 0, false));
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, true));
                    }

                    if (srCanDrivePerDayDrp.SelectedValue != "-1")
                    {
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(" Drive: ", 2, true));
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(srCanDrivePerDayDrp.SelectedItem.Text.ToUpper() + " hrs/day", 0, false));
                        ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, true));
                    }

                    ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, true));
                    ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 0, false));
                    ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 2, true));
                    ipModifiedDutyInWorkDayTbl.AddCell(getCell(" ", 0, false));

                    doc.Add(ipModifiedDutyInWorkDayTbl);
                }
            }

            if (srCanBendDrp.SelectedValue != "0" || srCanSquatDrp.SelectedValue != "0" || srCanClimbStairsDrp.SelectedValue != "0" || srCanClimbLadderDrp.SelectedValue != "0" || srCanCrawlDrp.SelectedValue != "0" || srCanReachDrp.SelectedValue != "0")
            {

                if (srCanBendDrp.SelectedValue == "4" || srCanSquatDrp.SelectedValue == "4" || srCanClimbStairsDrp.SelectedValue == "4" || srCanClimbLadderDrp.SelectedValue == "4" || srCanCrawlDrp.SelectedValue == "4" || srCanReachDrp.SelectedValue == "4")
                {
                    Single[] ipModifiedDutyIsAbleToOtherWidths = new Single[] {175.0F, 150.0F, 175.0F, 150.0F};
                    PdfPTable ipModifiedDutyIsAbleToOtherTbl = new PdfPTable(4);
                    ipModifiedDutyIsAbleToOtherTbl.TotalWidth = 650.0F;
                    ipModifiedDutyIsAbleToOtherTbl.SetWidths(ipModifiedDutyIsAbleToOtherWidths);

                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Patient is able to:", 2, true));
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 0, false));
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 2, true));
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 0, false));

                    if (srCanBendDrp.SelectedValue == "4")
                    {
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Bend: ", 2, true));
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanBendOtherTxt.Text.ToUpper(), 0, false));
                    }
                    else
                    {
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Bend: ", 2, true));
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanBendDrp.SelectedItem.Text.ToUpper(), 0, false));
                    }

                    if (srCanSquatDrp.SelectedValue == "4")
                    {
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Squat: ", 2, true));
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanSquatOtherTxt.Text.ToUpper(), 0, false));
                    }
                    else
                    {
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Squat: ", 2, true));
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanSquatDrp.SelectedItem.Text.ToUpper(), 0, false));
                    }

                    if (srCanClimbStairsDrp.SelectedValue == "4")
                    {
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Climb Stairs: ", 2, true));
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanClimbStairsOtherTxt.Text.ToUpper(), 0, false));
                    }
                    else
                    {
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Climb Stairs: ", 2, true));
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanClimbStairsDrp.SelectedItem.Text.ToUpper(), 0, false));
                    }

                    if (srCanClimbLadderDrp.SelectedValue == "4")
                    {
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Climb a ladder: ", 2, true));
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanClimbLadderOtherTxt.Text.ToUpper(), 0, false));
                    }
                    else
                    {
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Climb a ladder: ", 2, true));
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanClimbLadderDrp.SelectedItem.Text.ToUpper(), 0, false));
                    }

                    if (srCanCrawlDrp.SelectedValue == "4")
                    {
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Crawl: ", 2, true));
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanCrawlOtherTxt.Text.ToUpper(), 0, false));
                    }
                    else
                    {
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Crawl: ", 2, true));
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanCrawlDrp.SelectedItem.Text.ToUpper(), 0, false));
                    }

                    if (srCanReachDrp.SelectedValue == "4")
                    {
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Reach: ", 2, true));
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanReachOtherTxt.Text.ToUpper(), 0, false));
                    }
                    else
                    {
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell("Reach: ", 2, true));
                        ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(srCanReachDrp.SelectedItem.Text.ToUpper(), 0, false));
                    }
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 2, true));
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 0, false));
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 2, true));
                    ipModifiedDutyIsAbleToOtherTbl.AddCell(getCell(" ", 0, false));

                    doc.Add(ipModifiedDutyIsAbleToOtherTbl);
                }
                else
                {
                    Single[] ipModifiedDutyIsAbleToWidths = new Single[] {110.0F, 50.0F, 50.0F, 50.0F};
                    PdfPTable ipModifiedDutyIsAbleToTbl = new PdfPTable(4);
                    ipModifiedDutyIsAbleToTbl.TotalWidth = 200.0F;
                    ipModifiedDutyIsAbleToTbl.HorizontalAlignment = Element.ALIGN_LEFT;
                    ipModifiedDutyIsAbleToTbl.SetWidths(ipModifiedDutyIsAbleToWidths);

                    //Patient is able to:
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Patient is able to:", 2, true));
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 0, false));
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 2, true));
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 0, false));

                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Bend: ", 2, true));
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanBendDrp.SelectedItem.Text.ToUpper(), 0, false));
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Squat: ", 2, true));
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanSquatDrp.SelectedItem.Text.ToUpper(), 0, false));

                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Climb Stairs: ", 2, true));
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanClimbStairsDrp.SelectedItem.Text.ToUpper(), 0, false));
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Climb a ladder: ", 2, true));
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanClimbLadderDrp.SelectedItem.Text.ToUpper(), 0, false));

                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Crawl: ", 2, true));
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanCrawlDrp.SelectedItem.Text.ToUpper(), 0, false));
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell("Reach: ", 2, true));
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(srCanReachDrp.SelectedItem.Text.ToUpper(), 0, false));

                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 2, true));
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 0, false));
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 2, true));
                    ipModifiedDutyIsAbleToTbl.AddCell(getCell(" ", 0, false));

                    doc.Add(ipModifiedDutyIsAbleToTbl);



                }

            }
        }
        if (srReferredToSpecialistDrp.SelectedValue != "0")
        {
            PdfPTable ipSpecialistHdrTbl = new PdfPTable(1);
            ipSpecialistHdrTbl.TotalWidth = 600.0F;
            ipSpecialistHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            ipSpecialistHdrTbl.AddCell(getCell(" ", 0, false));
            ipSpecialistHdrTbl.AddCell(getHdrCell("Specialist", 0));

            doc.Add(ipSpecialistHdrTbl);

            Single[] DipSpecialistWidths = new Single[] {105.0F, 400.0F};
            PdfPTable ipSpecialistTbl = new PdfPTable(2);
            ipSpecialistTbl.TotalWidth = 505.0F;
            ipSpecialistTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            ipSpecialistTbl.SetWidths(DipSpecialistWidths);
            ipSpecialistTbl.LockedWidth = true;

            ipSpecialistTbl.AddCell(getCell(" ", 2, true));
            ipSpecialistTbl.AddCell(getCell(" ", 0, false));
            ipSpecialistTbl.AddCell(getCell("Referred to Specialist:", 2, true));
            ipSpecialistTbl.AddCell(getCell(srReferredToSpecialistDrp.SelectedItem.Text.ToUpper(), 0, false));
            if (srTypeOfSpecialistTxt.Text.ToUpper() != null)
            {
                ipSpecialistTbl.AddCell(getCell("Physician/Practice Name:", 2, true));
                ipSpecialistTbl.AddCell(getCell(srReferredDrNameTxt.Text.ToUpper(), 0, false));
                ipSpecialistTbl.AddCell(getCell("Type:", 2, true));
                ipSpecialistTbl.AddCell(getCell(srTypeOfSpecialistTxt.Text.ToUpper(), 0, false));
            }

            doc.Add(ipSpecialistTbl);

            Single[] ipSpecialistApptWidths = new Single[] {105.0F, 50.0F, 25.0F, 50.0F};
            PdfPTable ipSpecialistApptTbl = new PdfPTable(4);
            ipSpecialistApptTbl.TotalWidth = 230.0F;
            ipSpecialistApptTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            ipSpecialistApptTbl.SetWidths(ipSpecialistApptWidths);
            ipSpecialistApptTbl.LockedWidth = true;

            ipSpecialistApptTbl.AddCell(getCell("Date:", 2, true));
            ipSpecialistApptTbl.AddCell(getCell(srSpecialistApptDateTxt.Text.ToUpper(), 0, false));

            if (srSpecialistApptAmPmDrp.SelectedValue != "0" && srSpecialistApptHourDrp.SelectedValue != "0" && srSpecialistApptMinuteDrp.SelectedValue != "0")
            {
                ipSpecialistApptTbl.AddCell(getCell("Time:", 2, true));
                ipSpecialistApptTbl.AddCell(getCell(srSpecialistApptHourDrp.SelectedItem.Text.ToUpper() + ":" + srSpecialistApptMinuteDrp.SelectedItem.Text.ToUpper() + " " + srSpecialistApptAmPmDrp.SelectedItem.Text.ToUpper(), 0, false));
            }
            else
            {
                ipSpecialistApptTbl.AddCell(getCell(" ", 2, true));
                ipSpecialistApptTbl.AddCell(getCell(" ", 0, false));
            }
            doc.Add(ipSpecialistApptTbl);

        }



        if (srNextAptDateTxt.Text != null)
        {
            PdfPTable ipFollowupHdrTbl = new PdfPTable(1);
            ipFollowupHdrTbl.TotalWidth = 600.0F;
            ipFollowupHdrTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            ipFollowupHdrTbl.AddCell(getCell(" ", 0, false));
            ipFollowupHdrTbl.AddCell(getHdrCell("Follow-up Appointment", 0));

            doc.Add(ipFollowupHdrTbl);

            Single[] ipFollowupWidths = new Single[] {105.0F, 70.0F, 25.0F, 50.0F};
            PdfPTable ipFollowupTbl = new PdfPTable(4);
            ipFollowupTbl.TotalWidth = 225.0F;
            ipFollowupTbl.HorizontalAlignment = Element.ALIGN_LEFT;
            ipFollowupTbl.SetWidths(ipFollowupWidths);
            ipFollowupTbl.LockedWidth = true;

            ipFollowupTbl.AddCell(getCell(" ", 2, true));
            ipFollowupTbl.AddCell(getCell(" ", 0, false));
            ipFollowupTbl.AddCell(getCell(" ", 2, true));
            ipFollowupTbl.AddCell(getCell(" ", 0, false));
            ipFollowupTbl.AddCell(getCell("Date:", 2, true));
            ipFollowupTbl.AddCell(getCell(srNextAptDateTxt.Text.ToUpper(), 0, false));

            if (srNextAptAmPmDrp.SelectedValue != "0" && srNextAptHourDrp.SelectedValue != "0" && srNextAptMinuteDrp.SelectedValue != "0")
            {
                ipFollowupTbl.AddCell(getCell("Time:", 2, true));
                ipFollowupTbl.AddCell(getCell(srNextAptHourDrp.SelectedItem.Text.ToUpper() + ":" + srNextAptMinuteDrp.SelectedItem.Text.ToUpper() + " " + srNextAptAmPmDrp.SelectedItem.Text.ToUpper(), 0, false));
            }
            else
            {
                ipFollowupTbl.AddCell(getCell(" ", 2, true));
                ipFollowupTbl.AddCell(getCell(" ", 0, false));
            }
            doc.Add(ipFollowupTbl);
        }

        Single[] ipDoctorsCMTWidths = new Single[] {105.0F, 345.0F, 25.0F, 50.0F};
        PdfPTable ipDoctorsCMTbl = new PdfPTable(4);
        ipDoctorsCMTbl.TotalWidth = 525.0F;
        ipDoctorsCMTbl.HorizontalAlignment = Element.ALIGN_LEFT;
        ipDoctorsCMTbl.SetWidths(ipDoctorsCMTWidths);
        ipDoctorsCMTbl.LockedWidth = true;

        ipDoctorsCMTbl.AddCell(getCell(" ", 2, true));
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, false));
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, true));
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, false));

        ipDoctorsCMTbl.AddCell(getCell("Medical Care Coordinator:", 2, true));
        ipDoctorsCMTbl.AddCell(getCell(srCMTxt.Text.ToUpper(), 0, false));
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, true));
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, false));

        ipDoctorsCMTbl.AddCell(getCell("Physician:", 2, true));
        ipDoctorsCMTbl.AddCell(getCell(srTreatingPhysicianTxt.Text.ToUpper(), 0, false));
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, true));
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, false));

        if (srClincTypeDrp.SelectedValue == "2")
        {
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, true));
            ipDoctorsCMTbl.AddCell(getDoctorSignature(srTreatingPhysicianTxt.Text.ToUpper(), 0));
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, true));
            ipDoctorsCMTbl.AddCell(getCell(" ", 0, false));
            getSignatureBlock(srTreatingPhysicianTxt.Text.ToUpper(), ipDoctorsCMTbl);
        }


        ipDoctorsCMTbl.AddCell(getCell(" ", 2, true));
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, false));
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, true));
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, false));
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, true));
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, false));
        ipDoctorsCMTbl.AddCell(getCell(" ", 2, true));
        ipDoctorsCMTbl.AddCell(getCell(" ", 0, false));

        doc.Add(ipDoctorsCMTbl);

        PdfPTable ipAddressTbl = new PdfPTable(1);
        ipAddressTbl.TotalWidth = 4900;
        if (srClincTypeDrp.SelectedValue == "1")
            ipAddressTbl.AddCell(getCell("WorkWell Inc", 1, true, true));
        else
            ipAddressTbl.AddCell(getCell("WorkWell Physicians PC", 1, true, true));

        ipAddressTbl.AddCell(getCell("112 Third Ave - Carnegie, PA 15106", 1, true, true));
        ipAddressTbl.AddCell(getCell("Phone: 800.967.5935 Fax: 412-279-5848", 1, true, true));

        doc.Add(ipAddressTbl);

        /*Catch dex As DocumentException

        'Throw (dex)

        'Catch ioex As IOException
        'Throw (ioex)
        'Finally
        'statusReportHL.NavigateUrl = "/PDFs" & sFileName

        doc.Close()
        'Response.Redirect("/PDFs" & sFileName)
        'End Try*/

        String popupScript = "<script language=javascript> window.open('/PDFs" + sFileName + "') </script>";
        ClientScript.RegisterStartupScript(this.GetType(), "callpopup", popupScript);
    }
    public void NewViewSearch(object sender, EventArgs e)
    {
    }
    public void SearchViewStsRpt(object sender, EventArgs e)
    {
    }
    public PdfPCell getCompanyNameCell(int iWhichCompany, int iAlignment )
    {
        //0=Left, 1=Centre, 2=Right


        string fontpath = Server.MapPath("fonts");
        BaseFont customfont = BaseFont.CreateFont(fontpath + "//WarnockPro-Bold.otf", BaseFont.CP1252, BaseFont.EMBEDDED);

        Font font = new Font();
        Phrase p = new Phrase();
        if (iWhichCompany == 0)
        {
            font = new Font(customfont, 24);
            Chunk c1 = new Chunk("W", font);
            font = new Font(customfont, 20);
            Chunk c2 = new Chunk("ORK", font);
            font = new Font(customfont, 24);
            Chunk c3 = new Chunk("W", font);
            font = new Font(customfont, 20);
            Chunk c4 = new Chunk("ELL", font);

            p.Add(c1);
            p.Add(c2);
            p.Add(c3);
            p.Add(c4);
        }
        else if (iWhichCompany == 1) 
        {
            font = new Font(customfont, 9);
            Chunk c1 = new Chunk("INC", font);

            p.Add(c1);
        }
        else
        {
            font = new Font(customfont, 24);
            Chunk c1 = new Chunk("P", font);
            font = new Font(customfont, 20);
            Chunk c2 = new Chunk("HYSICIANS PC", font);

            p.Add(c1);
            p.Add(c2);
        }

        PdfPCell cell = new PdfPCell(p);
        cell.Border = 0;
        if (iWhichCompany == 0)
        {
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_BASELINE;
        }
        else if (iWhichCompany == 1)
        {
            cell.Rotation = 90;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_BASELINE;
        }
        else
        {
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_BOTTOM;
        }

        return cell;
    }
    public string GetChkBoxNeeded(int iWhich)
    {
        string sChkBxNeeded = "";

        if (iWhich == 1) 
        {
            if (srDiagXRayChk.Checked)
                sChkBxNeeded = sChkBxNeeded + srDiagXRayChk.Text;

            if (srDiagMRIChk.Checked)
                if (sChkBxNeeded != null) 
                    sChkBxNeeded = sChkBxNeeded + ", " + srDiagMRIChk.Text;
                else
                    sChkBxNeeded = srDiagMRIChk.Text;
            if (srDiagEMGNCVChk.Checked)
                if (sChkBxNeeded != null)
                    sChkBxNeeded = sChkBxNeeded + ", " + srDiagEMGNCVChk.Text;
                else
                    sChkBxNeeded = srDiagEMGNCVChk.Text;
            if (srDiagOtherChk.Checked)
                if (String.IsNullOrEmpty(sChkBxNeeded) == false)
                    sChkBxNeeded = sChkBxNeeded + ", " + srDiagOtherTxt.Text;
                else
                    sChkBxNeeded = srDiagOtherTxt.Text;
        }
        else
        {
            if (srPTChk.Checked)
                sChkBxNeeded = sChkBxNeeded + srPTChk.Text;

            if (srOTChk.Checked)
                if (String.IsNullOrEmpty(sChkBxNeeded) == false)
                    sChkBxNeeded = sChkBxNeeded + ", " + srOTChk.Text;
                else
                    sChkBxNeeded = srOTChk.Text;
            if (srWHChk.Checked)
                if (String.IsNullOrEmpty(sChkBxNeeded) == false)
                    sChkBxNeeded = sChkBxNeeded + ", " + srWHChk.Text;
                else
                    sChkBxNeeded = srWHChk.Text;
            if (srDiagOtherChk.Checked)
                if (String.IsNullOrEmpty(sChkBxNeeded) == false)
                    sChkBxNeeded = sChkBxNeeded + ", " + srDiagOtherTxt.Text;
                else
                    sChkBxNeeded = srDiagOtherTxt.Text;
        }


        return sChkBxNeeded;

    }
    public string CheckTime(string sHours, string sMins, string sAMPM)
    {
        string sTime  = "";

        if (sHours.Contains("-") || sMins.Contains("-") || sAMPM.Contains("-") )
            sTime = "--";
        else
            sTime = sHours + ":" + sMins + " " + sAMPM;

        return sTime;
    }

    public int GetCheckedVal(bool bChecked)
    {

        int iChecked = 0;

        if (bChecked) 
            iChecked = 1;
        else
            iChecked = 0;

        return iChecked;

    }

    public void NewViewSearch()
    {
        Response.Redirect("View_Status_Reports.aspx");
    }
    public PdfPCell getHdrCell(string sText, int iAlignment)
    {
        //0=Left, 1=Centre, 2=Right
        string fontpath = Server.MapPath("fonts");
        BaseFont customfont = BaseFont.CreateFont(fontpath + "//calibri.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
        Font font = new Font(customfont, 10, Font.BOLD);

        Chunk sChunk = new Chunk(sText, font);
        sChunk.SetUnderline(0.5F, -1.5F);

        Phrase sPhrase = new Phrase();
        sPhrase.Add(sChunk);
            

        PdfPCell cell = new PdfPCell(sPhrase);
        cell.Border = 0;
        cell.HorizontalAlignment = iAlignment;

        return cell;
    }
    //11/14/16 this needs completely rewritten didn't even know this exists.
    public PdfPCell getDoctorSignature(string sDoctor , int iAlignment)
    {
        //0=Left, 1=Centre, 2=Right

        /*if sDoctor.ToUpper.Contains("PRINCE") Then
            Dim fontpath As String = Server.MapPath("fonts")
            Dim customfont As BaseFont = BaseFont.CreateFont(fontpath & "/LESTPE__.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim font As New Font(customfont, 24, font.NORMAL)

            Dim sChunk As New Chunk("L P", font)
            'Dim sChunk As New Chunk("_L", font)
            sChunk.SetUnderline(0.5F, -1.5F)

            Dim sPhrase As New Phrase()
            sPhrase.Add(sChunk)


            Dim cell As New PdfPCell(sPhrase)
            cell.Border = 0
            cell.HorizontalAlignment = iAlignment

            Return cell*/
        if (sDoctor.ToUpper().Contains("METCALF"))
        {
            string fontpath = Server.MapPath("fonts");
            BaseFont customfont = BaseFont.CreateFont(fontpath + "//JOHNWME_.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
            Font font = new Font(customfont, 24, Font.NORMAL);

            Chunk sChunk = new Chunk("J M", font);
            sChunk.SetUnderline(0.5F, -1.5F);

            Phrase sPhrase = new Phrase();
            sPhrase.Add(sChunk);


            PdfPCell cell = new PdfPCell(sPhrase);
            cell.Border = 0;
            cell.HorizontalAlignment = iAlignment;

            return cell;
        }
        else if (sDoctor.ToUpper().Contains("SHEPPARD"))
        {

            string fontpath = Server.MapPath("fonts");
            BaseFont customfont = BaseFont.CreateFont(fontpath + "//SCOTSE__.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
            Font font = new Font(customfont, 24, Font.NORMAL);

            Chunk sChunk = new Chunk("S", font);
            sChunk.SetUnderline(0.5F, -1.5F);

            Phrase sPhrase = new Phrase();
            sPhrase.Add(sChunk);


            PdfPCell cell = new PdfPCell(sPhrase);
            cell.Border = 0;
            cell.HorizontalAlignment = iAlignment;

            return cell;
        }
        else if (sDoctor.ToUpper().Contains("KATZ"))
        {
            string fontpath = Server.MapPath("fonts");
            BaseFont customfont = BaseFont.CreateFont(fontpath + "//RICHKE__.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
            Font font = new Font(customfont, 24, Font.NORMAL);

            Chunk sChunk = new Chunk("R K", font);
            sChunk.SetUnderline(0.5F, -1.5F);

            Phrase sPhrase = new Phrase();
            sPhrase.Add(sChunk);


            PdfPCell cell = new PdfPCell(sPhrase);
            cell.Border = 0;
            cell.HorizontalAlignment = iAlignment;

            return cell;
        }
        else if (sDoctor.ToUpper().Contains("WILSON"))
        {
            string fontpath = Server.MapPath("fonts");
            BaseFont customfont = BaseFont.CreateFont(fontpath + "//MWilson2.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
            Font font = new Font(customfont, 24, Font.NORMAL);

            Chunk sChunk = new Chunk("GW", font);

            Phrase sPhrase = new Phrase();
            sPhrase.Add(sChunk);


            PdfPCell cell = new PdfPCell(sPhrase);
            cell.Border = 0;
            cell.HorizontalAlignment = iAlignment;

            return cell;
        }
        else if (sDoctor.ToUpper().Contains("BLOOM"))
        {
            string fontpath = Server.MapPath("fonts");
            BaseFont customfont = BaseFont.CreateFont(fontpath + "//ValBloom2.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
            Font font = new Font(customfont, 24, Font.NORMAL);

            Chunk sChunk = new Chunk("V", font);

            Phrase sPhrase = new Phrase();
            sPhrase.Add(sChunk);


            PdfPCell cell = new PdfPCell(sPhrase);
            cell.Border = 0;
            cell.HorizontalAlignment = iAlignment;

            return cell;
        }
        /*ElseIf sDoctor.ToUpper.Contains("AMANT") Then
            Dim fontpath As String = Server.MapPath("fonts")
            Dim customfont As BaseFont = BaseFont.CreateFont(fontpath & "/tstamant2.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim font As New Font(customfont, 24, font.NORMAL)

            Dim sChunk As New Chunk("T", font)
            'sChunk.SetUnderline(0.5F, -1.5F)

            Dim sPhrase As New Phrase()
            sPhrase.Add(sChunk)


            Dim cell As New PdfPCell(sPhrase)
            cell.Border = 0
            cell.HorizontalAlignment = iAlignment

            Return cell*/
        else
        {
            string fontpath  = Server.MapPath("fonts");
            BaseFont customfont = BaseFont.CreateFont(fontpath + "//calibri.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
            Font font = new Font(customfont, 10, Font.BOLD);

            Chunk sChunk = new Chunk(" ", font);

            Phrase sPhrase = new Phrase();
            sPhrase.Add(sChunk);


            PdfPCell cell = new PdfPCell(sPhrase);
            cell.Border = 0;
            cell.HorizontalAlignment = iAlignment;

            return cell;
        }
    }
    public void getSignatureBlock(string srTreatingPhysician, PdfPTable ipDoctorsCMTbl)
    {
        /*If srTreatingPhysician.ToUpper.Contains("PRINCE") Then
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
            ipDoctorsCMTbl.AddCell(getCell("Lester O. Prince, M.D.", 0, False))
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, True))
            ipDoctorsCMTbl.AddCell(getCell(" ", 0, False))*/
        if (srTreatingPhysician.ToUpper().Contains("KATZ"))
        {
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, true));
            ipDoctorsCMTbl.AddCell(getCell("Richard H. Katz, M.D.", 0, false));
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, true));
            ipDoctorsCMTbl.AddCell(getCell(" ", 0, false));
        }
        else if (srTreatingPhysician.ToUpper().Contains("SHEPPARD"))
        {
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, true));
            ipDoctorsCMTbl.AddCell(getCell("Scott F. Sheppard, M.D.", 0, false));
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, true));
            ipDoctorsCMTbl.AddCell(getCell(" ", 0, false));
        }
        else if (srTreatingPhysician.ToUpper().Contains("METCALF"))
        {
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, true));
            ipDoctorsCMTbl.AddCell(getCell("John W. Metcalf, M.D.", 0, false));
            ipDoctorsCMTbl.AddCell(getCell(" ", 2, true));
            ipDoctorsCMTbl.AddCell(getCell(" ", 0, false));
        }

    }
}