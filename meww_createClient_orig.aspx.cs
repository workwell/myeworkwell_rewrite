﻿using System;
using System.Data;
using System.Data.Odbc;
using System.Globalization;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DayPilot.Web.Ui.Recurrence;
using Microsoft.VisualBasic;
using System.Net.Mail;

public partial class meww_createClient : System.Web.UI.Page
{
    DataTable account_dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        empSrchRstsGrd.RowDataBound += new GridViewRowEventHandler(empSrchRstsGrd_RowDataBound);
        
        account_dt.Columns.Add("PARTY_ID", typeof(string));
        account_dt.Columns.Add("CUST_ACCOUNT_ID", typeof(string));
        account_dt.Columns.Add("ACCOUNT_NUMBER", typeof(string));
        account_dt.Columns.Add("ACCOUNT_NAME", typeof(string));
        account_dt.Columns.Add("PARTY_NAME", typeof(string));
        account_dt.Columns.Add("ADDRESS", typeof(string));

        if (!Page.IsPostBack)
        {
            account_dt = new DataTable();
            Session["account_listing"] = null;

        }
        else
        {
            if ((DataTable)Session["account_listing"] != null)
            {
                account_dt = (DataTable)Session["account_listing"];
            }
        }
    }

    protected void cancelEmpSrch(object sender, EventArgs e)
    {
        empSrchFieldTxt.Text = "";
        empSrchRstsGrd.Visible = false;
        empSrchNoRsltsTbl.Visible = false;
        Session["account_listing"] = null;

    }

    protected void searchForEmployer(object sender, EventArgs e)
    {
        string sql = "";
        DataTable dt = new DataTable();

        string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        OdbcConnection conn = new OdbcConnection(strConnection);

        try
        {           
            conn.Open();

            if (empSrchTYpeDrp.SelectedValue == "0")
            {
                sql = "SELECT HCA.ACCOUNT_NUMBER,HCA.PARTY_ID, HCA.CUST_ACCOUNT_ID, HCA.ACCOUNT_NAME, HL.ADDRESS1, HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE "
                         + "FROM HZ_CUST_ACCOUNTS HCA INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                         + "     INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                         + "	    INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                         + "WHERE HCA.ACCOUNT_NAME LIKE '%" + empSrchFieldTxt.Text.ToUpper().Replace("'", "''") + "%' AND "
                         + "      HCA.STATUS = 'A' AND "
                         + "      HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                         + "ORDER BY HCA.ACCOUNT_NUMBER, HCA.ACCOUNT_NAME";
            }
            else
            {              
                sql = "SELECT HCA.ACCOUNT_NUMBER,HCA.PARTY_ID, HCA.CUST_ACCOUNT_ID, HCA.ACCOUNT_NAME, HL.ADDRESS1, HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE "
                            + "FROM HZ_CUST_ACCOUNTS HCA INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                            + "     INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                            + "	    INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                            + "WHERE HCA.ACCOUNT_NUMBER LIKE '" + empSrchFieldTxt.Text.ToUpper().Replace("'", "''") + "%' AND "
                            + "      HCA.STATUS = 'A' AND "
                            + "      HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                            + "ORDER BY HCA.ACCOUNT_NUMBER, HCA.ACCOUNT_NAME";
            }
            //Response.Write(sql);

            OdbcCommand cmd = new OdbcCommand(sql, conn);
            cmd.CommandType = CommandType.Text;

            OdbcDataAdapter ora1 = new OdbcDataAdapter();
            ora1.SelectCommand = cmd;
            ora1.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                empSrchRstsGrd.DataSource = dt;
                empSrchRstsGrd.PageIndex = 0;
                empSrchRstsGrd.DataBind();
                empSrchNoRsltsTbl.Visible = false;
                empSrchRstsGrd.Visible = true;
               
            }
            else
            {
                empSrchRstsGrd.Visible = false;
                empSrchNoRsltsTbl.Visible = true;
            }
            cmd.Dispose();
        }

        catch (Exception ex)
        {
            Response.Write("Can't load Web page: " + ex.Message);
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }

    }

    protected void empSrchRstsGrd_RowDataBound(Object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string sAddress = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ADDRESS1"));



            if (!string.IsNullOrEmpty(sAddress))
            {
                sAddress = sAddress + ", ";
            }
            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ADDRESS2"))))
            {
                sAddress += Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ADDRESS2")) + ", ";
            }

            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "CITY"))))
            {
                sAddress += " " + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "CITY")) + ", ";
            }

            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "STATE"))))
            {
                sAddress += " " + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "STATE"));
            }

            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "POSTAL_CODE"))))
            {
                sAddress += " " + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "POSTAL_CODE"));
            }

            e.Row.Cells[2].Text = sAddress;
        }

    }

    protected void empSrchRstsGrd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GetEmpSrchResultsPaging(e.NewPageIndex);
    }

    protected void GetEmpSrchResultsPaging(int pageIndex)
    {

        string sql = "";
        DataTable dt = new DataTable();

        string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        OdbcConnection conn = new OdbcConnection(strConnection);

        try
        {
            conn.Open();

            if (empSrchTYpeDrp.SelectedValue == "0")
            {
                sql = "SELECT HCA.ACCOUNT_NUMBER,HCA.PARTY_ID, HCA.CUST_ACCOUNT_ID, HCA.ACCOUNT_NAME, HL.ADDRESS1, HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE "
                         + "FROM HZ_CUST_ACCOUNTS HCA INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                         + "     INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                         + "	    INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                         + "WHERE HCA.ACCOUNT_NAME LIKE '%" + empSrchFieldTxt.Text.ToUpper().Replace("'", "''") + "%' AND "
                         + "      HCA.STATUS = 'A' AND "
                         + "      HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                         + "ORDER BY HCA.ACCOUNT_NUMBER, HCA.ACCOUNT_NAME";
            }
            else
            {
                sql = "SELECT HCA.ACCOUNT_NUMBER,HCA.PARTY_ID, HCA.CUST_ACCOUNT_ID, HCA.ACCOUNT_NAME, HL.ADDRESS1, HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE "
                            + "FROM HZ_CUST_ACCOUNTS HCA INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                            + "     INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                            + "	    INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                            + "WHERE HCA.ACCOUNT_NUMBER LIKE '" + empSrchFieldTxt.Text.ToUpper().Replace("'", "''") + "%' AND "
                            + "      HCA.STATUS = 'A' AND "
                            + "      HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                            + "ORDER BY HCA.ACCOUNT_NUMBER, HCA.ACCOUNT_NAME";
            }
            //Response.Write(sql);

            OdbcCommand cmd = new OdbcCommand(sql, conn);
            cmd.CommandType = CommandType.Text;

            OdbcDataAdapter ora1 = new OdbcDataAdapter();
            ora1.SelectCommand = cmd;
            ora1.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                empSrchRstsGrd.DataSource = dt;
                empSrchRstsGrd.PageIndex = pageIndex;
                empSrchRstsGrd.DataBind();
                empSrchNoRsltsTbl.Visible = false;
                empSrchRstsGrd.Visible = true;                
            }
            else
            {
                empSrchRstsGrd.Visible = false;
                empSrchNoRsltsTbl.Visible = true;
            }
            cmd.Dispose();
        }

        catch (Exception ex)
        {
            Response.Write("Can't load Web page: " + ex.Message);
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }
    }

    protected void empSrchRstsGrd_RowCommand(Object sender, GridViewCommandEventArgs e)
    {


        if (string.Compare(e.CommandName, "Page") != 0)
        {
            if (addAccount.Value == "0")
            {
                clear_fields();
            }

            int empCustAcctID = Convert.ToInt32(e.CommandArgument);

            //empSrchPanel.Visible = false;

            client_acctTC.Visible = false;

            //ipCasesSrchPanel.Visible = true;
            string sql = "";
            DataTable dt = new DataTable();

            string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
            OdbcConnection conn = new OdbcConnection(strConnection);

            try
            {
                conn.Open();

                sql = "SELECT HCA.PARTY_ID, HL.ADDRESS1, HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE, HP.PARTY_NAME, HCA.ACCOUNT_NUMBER, HCA.ACCOUNT_NAME, HCA.CUST_ACCOUNT_ID "
                         + "FROM HZ_CUST_ACCOUNTS HCA INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                         + "     INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                         + "	 INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                         + "     INNER JOIN HZ_PARTIES HP ON HCA.PARTY_ID = HP.PARTY_ID "
                         + "WHERE HCA.CUST_ACCOUNT_ID = " + empCustAcctID + " AND "
                         + "      HCA.STATUS = 'A' AND "
                         + "      HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                         + "ORDER BY HCA.ACCOUNT_NUMBER, HCA.ACCOUNT_NAME";

                //Response.Write(sql + "<br>");
                OdbcCommand cmd = new OdbcCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                OdbcDataReader dr3 = cmd.ExecuteReader();                

                while (dr3.Read())
                {
                    empPartyID.Value = Convert.ToString(dr3.GetValue(0));
                    contact_Add1Txt.Text = myCStr(dr3.GetValue(1));
                    contact_Add2Txt.Text = myCStr(dr3.GetValue(2));
                    contact_CityTxt.Text = myCStr(dr3.GetValue(3));
                    contact_StateTxt.Text = myCStr(dr3.GetValue(4));
                    contact_ZipTxt.Text = myCStr(dr3.GetValue(5));
                    contact_AcctNameTxt.Text = myCStr(dr3.GetValue(6));
                    contact_PasswordTxt.Text = myCStr(dr3.GetValue(6)).Substring(0, 4).Trim() + myCStr(dr3.GetValue(7)).Substring(0, 4).Trim();

                   if ((DataTable)Session["account_listing"] != null)
                   {
                        account_dt = (DataTable)Session["account_listing"];
                       
                    }

                    DataRow dr;

                    dr = account_dt.NewRow();
                    dr["PARTY_ID"] = myCStr(dr3.GetValue(0));
                    dr["CUST_ACCOUNT_ID"] = myCStr(dr3.GetValue(9));
                    dr["ACCOUNT_NUMBER"] = myCStr(dr3.GetValue(7));
                    dr["PARTY_NAME"] = myCStr(dr3.GetValue(6));
                    dr["ACCOUNT_NAME"] = myCStr(dr3.GetValue(8));

                    if (string.IsNullOrEmpty(myCStr(dr3.GetValue(2))))
                    {
                        dr["ADDRESS"] = myCStr(dr3.GetValue(1)) + " " + myCStr(dr3.GetValue(3)) + ", " + myCStr(dr3.GetValue(4)) + ", " + myCStr(dr3.GetValue(5)) ;
                    }
                    else
                    {
                        dr["ADDRESS"] = myCStr(dr3.GetValue(1)) + " " + myCStr(dr3.GetValue(2)) + ", " + myCStr(dr3.GetValue(3)) + ", " + myCStr(dr3.GetValue(4)) + ", " + myCStr(dr3.GetValue(5));
                    }

                    account_dt.Rows.Add(dr);

                    acctListingGrid.DataSource = account_dt;
                    acctListingGrid.PageIndex = 0;
                    acctListingGrid.DataBind();

                    Session["account_listing"] = account_dt;
                    acctType.Value = "1";

                    if (account_dt.Rows.Count > 0)
                    {
                        acctListingGrid.Visible = true;
                        noAcctListingTbl.Visible = false;
                    }
                    else
                    {
                        acctListingGrid.Visible = false;
                        noAcctListingTbl.Visible = true;
                    }
                    
                }

                dr3.Close();
                dr3.Dispose();
                cmd.Dispose();

                contact_info_panel.Visible = true;
            }

            catch (Exception ex)
            {
                Response.Write("Can't load Web page: " + ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }       

    }

    protected string myCStr(object test)
    {

        if (test == DBNull.Value)
        {
            return (String.Empty);
        }
        else
        {
            if (Convert.ToString(test) == "#")
            {
                return (String.Empty);
            }
            else
            {
                return Convert.ToString(test);
            }
        }
    }

    protected void cancelContactInfo(object sender, EventArgs e)
    {
        clear_fields();
        addAccount.Value = "0";
        contact_info_panel.Visible = false;
        //empSrchPanel.Visible = true;
        client_acctTC.Visible = true;
        Session["account_listing"] = null;
               
    }

    protected void submitContactInfo(object sender, EventArgs e)
    {
        account_created_panel.Visible = false;
        user_exists_already_panel.Visible = false;

        fname_Pointer.Visible = false;
        lname_Pointer.Visible = false;
        phoneAC_Pointer.Visible = false;
        phoneNumer_Pointer.Visible = false;
        email_Pointer.Visible = false;
        required_field_error.Visible = false;

        if (string.IsNullOrEmpty(contact_FNameTxt.Text) || string.IsNullOrEmpty(contact_LNameTxt.Text) || string.IsNullOrEmpty(contact_PhoneACTxt.Text) || string.IsNullOrEmpty(contact_PhoneNumberTxt.Text) || string.IsNullOrEmpty(contact_EmailTxt.Text))
        {
            fname_Pointer.Visible = true;
            lname_Pointer.Visible = true;
            phoneAC_Pointer.Visible = true;
            phoneNumer_Pointer.Visible = true;
            email_Pointer.Visible = true;
            required_field_error.Visible = true;
        }
        else
        {

            string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
            OdbcConnection conn = new OdbcConnection(strConnection);

            try
            {
                conn.Open();

                OdbcCommand cmd = new OdbcCommand("{call sp_createClientAcct(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                
                if(string.IsNullOrEmpty(contact_FNameTxt.Text))
                {
                    cmd.Parameters.Add("FIRST_NAME", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("FIRST_NAME", OdbcType.VarChar, 255).Value = contact_FNameTxt.Text.ToUpper();
                }
                
                
                if(string.IsNullOrEmpty(contact_LNameTxt.Text))
                {
                    cmd.Parameters.Add("LAST_NAME", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("LAST_NAME", OdbcType.VarChar, 255).Value = contact_LNameTxt.Text.ToUpper();
                }

                if (string.IsNullOrEmpty(contact_EmailTxt.Text))
                {
                    cmd.Parameters.Add("EMAIL", OdbcType.VarChar, 255).Value = "#";
                    cmd.Parameters.Add("USERNAME", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("EMAIL", OdbcType.VarChar, 255).Value = contact_EmailTxt.Text.ToUpper();
                    cmd.Parameters.Add("USERNAME", OdbcType.VarChar, 255).Value = contact_EmailTxt.Text.ToUpper();
                }

                cmd.Parameters.Add("PASSWORD", OdbcType.VarChar, 100).Value = contact_PasswordTxt.Text.ToUpper();
                cmd.Parameters.Add("IS_EMPLOYER", OdbcType.TinyInt, 1).Value = 1;
                cmd.Parameters.Add("ACTIVE", OdbcType.TinyInt, 1).Value = 1;
                cmd.Parameters.Add("LAST_LOGIN", OdbcType.Date).Value = DateTime.Now;

                if(string.IsNullOrEmpty(contact_Add1Txt.Text))
                {
                    cmd.Parameters.Add("ADDRESS1", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("ADDRESS1", OdbcType.VarChar, 255).Value = contact_Add1Txt.Text.ToUpper();
                }
                
                if(string.IsNullOrEmpty(contact_Add2Txt.Text))
                {
                    cmd.Parameters.Add("ADDRESS2", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("ADDRESS2", OdbcType.VarChar, 255).Value = contact_Add2Txt.Text.ToUpper();
                }
                
                if(string.IsNullOrEmpty(contact_CityTxt.Text))
                {
                    cmd.Parameters.Add("CITY", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("CITY", OdbcType.VarChar, 255).Value = contact_CityTxt.Text.ToUpper();
                }
                
                if(string.IsNullOrEmpty(contact_StateTxt.Text))
                {
                    cmd.Parameters.Add("STATE", OdbcType.Char, 2).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("STATE", OdbcType.Char, 2).Value = contact_StateTxt.Text.ToUpper();
                }
                
                if(string.IsNullOrEmpty(contact_ZipTxt.Text))
                {
                    cmd.Parameters.Add("ZIP", OdbcType.VarChar, 10).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("ZIP", OdbcType.VarChar, 10).Value = contact_ZipTxt.Text.ToUpper();
                }

                if(string.IsNullOrEmpty(contact_PhoneACTxt.Text))
                {
                    cmd.Parameters.Add("PHONE_AC", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("PHONE_AC", OdbcType.VarChar, 255).Value = contact_PhoneACTxt.Text.ToUpper();
                }
                
                if(string.IsNullOrEmpty(contact_PhoneNumberTxt.Text))
                {
                    cmd.Parameters.Add("PHONEnUMBER", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("PHONEnUMBER", OdbcType.VarChar, 255).Value = contact_PhoneNumberTxt.Text.ToUpper();
                }
                
                
                
                if(string.IsNullOrEmpty(contact_FaxACTxt.Text))
                {
                    cmd.Parameters.Add("FAX_AC", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("FAX_AC", OdbcType.VarChar, 255).Value = contact_FaxACTxt.Text.ToUpper();
                }
                
                if(string.IsNullOrEmpty(contact_FaxNumberTxt.Text))
                {
                    cmd.Parameters.Add("FAX_NUMBER", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("FAX_NUMBER", OdbcType.VarChar, 255).Value = contact_FaxNumberTxt.Text.ToUpper();
                }

                if (string.IsNullOrEmpty(contact_PhoneExtTxt.Text))
                {
                    cmd.Parameters.Add("PHONE_EXT", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("PHONE_EXT", OdbcType.VarChar, 255).Value = contact_PhoneExtTxt.Text.ToUpper();
                }

                cmd.Parameters.Add("RESET_PASSOWRD", OdbcType.TinyInt, 1).Value = 1;
                cmd.Parameters.Add("RECEIVE_EMAILS", OdbcType.TinyInt, 1).Value = Convert.ToInt32(recvieveEmailsDrp.SelectedValue);
                //recvieveEmailsDrp
                OdbcDataReader dr3 = cmd.ExecuteReader();

                if (dr3.Read())
                {

                    //if (myCStr(dr3.GetValue(0)) == "0")                    

                    if (myCStr(dr3.GetValue(0)) != "1")  //meaning it returned a userid back because the user wasn't already created.
                    {
                        string str = "";
                        int i;
                        bool dk_exists = false;

                        int[] party_ids;
                        party_ids = new int[acctListingGrid.Rows.Count]; 

                        user_nametxt.Text = contact_EmailTxt.Text.ToUpper();
                        passwordTxt.Text = contact_PasswordTxt.Text;

                        acct_was_created_lbl.Text = contact_FNameTxt.Text.ToUpper() + " " + contact_LNameTxt.Text.ToUpper() + "'S  ACCOUNT HAS BEEN CREATED!";

                        for (i = 0; i < acctListingGrid.Rows.Count; i++)
                            {
                                party_ids[i] = 0;
                            }

                        foreach (GridViewRow oItem in acctListingGrid.Rows)
                        {

                            // this check so there isn't any dups
                            for (i = 0; i < acctListingGrid.Rows.Count; i++)
                            {
                                if ((Convert.ToInt32(acctListingGrid.DataKeys[oItem.RowIndex].Value) != party_ids[i]))
                                {
                                    if (party_ids[i] == 0)
                                    {
                                        party_ids[i] = Convert.ToInt32(acctListingGrid.DataKeys[oItem.RowIndex].Value);
                                        dk_exists = false; 
                                        i = acctListingGrid.Rows.Count + 1;  // exit the loop because value was enter into array
                                    }
                                }
                                else
                                {
                                    dk_exists = true; 
                                    i = acctListingGrid.Rows.Count + 1;  // exit the loop because value was found
                                }
                            }

                            if (acctType.Value == "1" && dk_exists == false)
                            {

                                OdbcCommand cmd2 = new OdbcCommand("{call sp_createClientPrivs(?,?,?,?)}", conn);
                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd2.Parameters.Add("USER_ID", OdbcType.Int, 11).Value = Convert.ToInt32(myCStr(dr3.GetValue(0)));
                                cmd2.Parameters.Add("PARTY_ID", OdbcType.Int, 11).Value = Convert.ToInt32(acctListingGrid.DataKeys[oItem.RowIndex].Value);
                                cmd2.Parameters.Add("CUST_ACCOUNT_ID", OdbcType.Int, 11).Value = 0;
                                cmd2.Parameters.Add("TYPE", OdbcType.Int, 11).Value = 1;

                                cmd2.ExecuteNonQuery();
                                cmd2.Dispose();
                            }
                            else if (acctType.Value == "2")
                            {

                                string sql = "SELECT CUST_ACCOUNT_ID FROM HZ_CUST_ACCOUNTS WHERE ACCOUNT_NUMBER = '" + acctListingGrid.Rows[oItem.RowIndex].Cells[0].Text + "'";

                                OdbcCommand cmd_custacct = new OdbcCommand(sql, conn);
                                cmd_custacct.CommandType = CommandType.Text;

                                OdbcDataReader dr3_ = cmd_custacct.ExecuteReader();

                                if (dr3_.Read())
                                {
                                    
                                    OdbcCommand cmd2 = new OdbcCommand("{call sp_createClientPrivs(?,?,?,?)}", conn);
                                    cmd2.CommandType = CommandType.StoredProcedure;

                                    cmd2.Parameters.Add("USER_ID", OdbcType.Int, 11).Value = Convert.ToInt32(myCStr(dr3.GetValue(0)));
                                    cmd2.Parameters.Add("PARTY_ID", OdbcType.Int, 11).Value = Convert.ToInt32(acctListingGrid.DataKeys[oItem.RowIndex].Value);
                                    cmd2.Parameters.Add("CUST_ACCOUNT_ID", OdbcType.Int, 11).Value = Convert.ToInt32(myCStr(dr3_.GetValue(0)));
                                    cmd2.Parameters.Add("TYPE", OdbcType.Int, 11).Value = 2;

                                    cmd2.ExecuteNonQuery();
                                    cmd2.Dispose();
                                }

                                dr3_.Close();
                                dr3_.Dispose();                               

                            }
                            
                        }

                        for (i = 0; i < acctListingGrid.Rows.Count; i++){

                            if (party_ids[i] != 0)
                            {
                                str += "Use Filezilla to create a folder named: <B>" + party_ids[i] + "</B><BR>";
                            }
                            else
                            {
                                i = acctListingGrid.Rows.Count + 1;
                            }
                        }

                        creat_panel_folderLbl.Text = str + "<BR>Then update myeworkwell <B><U>panel client list.xlsx</b></u> with the Account and Folder name located in the wwpanel folder on the shares drive.";

                        contact_info_panel.Visible = false;
                        account_created_panel.Visible = true;
                    }
                    else
                    {
                        user_exists_already_panel.Visible = true;
                        contact_info_panel.Visible = false;
                    }
                }

                addAccount.Value = "0";
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Response.Write("Can't load Web page: " + ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
                

        }



    }

    protected void clear_fields()
    {
        contact_FNameTxt.Text = "";
        contact_LNameTxt.Text = "";
        contact_AcctNameTxt.Text = "";
        contact_Add1Txt.Text = "";
        contact_Add2Txt.Text = "";
        contact_CityTxt.Text = "";
        contact_StateTxt.Text = "";
        contact_ZipTxt.Text = "";
        contact_PhoneACTxt.Text = "";
        contact_PhoneNumberTxt.Text = "";
        contact_PhoneExtTxt.Text = "";
        contact_FaxACTxt.Text = "";
        contact_FaxNumberTxt.Text = "";
        contact_EmailTxt.Text = "";
        contact_PasswordTxt.Text = "";
        recvieveEmailsDrp.SelectedIndex = 0;
    }

    protected void clear_user_search(object sender, EventArgs e)
    {
        firstNameTxt.Text = "";
        lastNameTxt.Text = "";
        emailTxt.Text = "";

        user_search_results_panel.Visible = false;
    }

    protected void submit_user_search(object sender, EventArgs e)
    {

        string sql = "";
        DataTable dt = new DataTable();

        string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        OdbcConnection conn = new OdbcConnection(strConnection);

        rest_psswrd.Value = "0";

        try
        {
            conn.Open();

           
            sql = "SELECT UI.SFIRSTNAME, UI.SLASTNAME, UI.SUSERNAME, HP.PARTY_NAME, UI.IUSERID, HP.PARTY_ID "
                  + " FROM USER_INFO UI INNER JOIN ACCOUNT_PRIVS AP ON UI.IUSERID = AP.IUSERID "
                  + "      INNER JOIN HZ_PARTIES HP ON AP.PARTY_ID = HP.PARTY_ID "
                  +" WHERE UI.SFIRSTNAME LIKE '%" + firstNameTxt.Text.ToUpper() + "%' AND "
                  + "      UI.SLASTNAME LIKE '%" + lastNameTxt.Text.ToUpper() + "%' AND "
                  + "      UI.SEMAILADDRESS LIKE '%" + emailTxt.Text.ToUpper() + "%' AND "
                  + "      AP.USER_TYPE = 1 "
                  + " GROUP BY UI.SFIRSTNAME, UI.SLASTNAME, UI.SUSERNAME, HP.PARTY_NAME, UI.IUSERID, HP.PARTY_ID " 
                  + " ORDER BY UI.SLASTNAME, UI.SFIRSTNAME ";
           
            //Response.Write(sql);

            OdbcCommand cmd = new OdbcCommand(sql, conn);
            cmd.CommandType = CommandType.Text;

            OdbcDataAdapter ora1 = new OdbcDataAdapter();
            ora1.SelectCommand = cmd;
            ora1.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                user_srchRsltsGrd.DataSource = dt;
                user_srchRsltsGrd.PageIndex = 0;
                user_srchRsltsGrd.DataBind();
                noUserRsltsTbl.Visible = false;
                user_srchRsltsGrd.Visible = true;

            }
            else
            {
                user_srchRsltsGrd.Visible = false;
                noUserRsltsTbl.Visible = true;
            }
            cmd.Dispose();

            user_search_results_panel.Visible = true;
            user_search_panel.Visible = false;

        }

        catch (Exception ex)
        {
            Response.Write("Can't load Web page: " + ex.Message);
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }

    }

    protected void user_srchRsltsGrd_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            
            string sFName = myCStr(DataBinder.Eval(e.Row.DataItem, "SFIRSTNAME")).Trim();
            string sLName = myCStr(DataBinder.Eval(e.Row.DataItem, "SLASTNAME")).Trim();

            e.Row.Cells[0].Text = sLName + ", " + sFName;

        }
    }

    protected void user_srchRsltsGrd_RowCommand(Object sender, GridViewCommandEventArgs e)
    {

        if (string.Compare(e.CommandName, "Page") != 0)
        {


            int user_id = Convert.ToInt32(e.CommandArgument);
            cliUserID.Value = Convert.ToString(user_id);

            OdbcCommand objSqlCmd = new OdbcCommand();
            string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
            OdbcConnection conn = new OdbcConnection(strConnection);


            try
            {
                conn.Open();

                string sql = "SELECT IUSERID , SFIRSTNAME , SLASTNAME , SEMAILADDRESS , SUSERNAME , SPASSWORD , BISEMPLOYER  , BACTIVE , SADDRESS , SADDRESS2 , SCITY , SSTATE ,  SZIP , SPHONEAREACODE , SPHONENUMBER , SFAXAREACODE , SFAXNUMBER , SEXTENSION , BRESETPASSWORD "
                             + " FROM USER_INFO "
                             + " WHERE IUSERID = " + user_id;
 

                //Response.Write(sql);

                OdbcCommand cmd = new OdbcCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                OdbcDataReader dr3 = cmd.ExecuteReader();

                while (dr3.Read())
                {
                    account_statusDrp.SelectedValue = myCStr(dr3.GetValue(7));

                    update_contact_FNameTxt.Text = myCStr(dr3.GetValue(1));
                    update_contact_LNameTxt.Text = myCStr(dr3.GetValue(2));

                    update_contact_Add1Txt.Text = myCStr(dr3.GetValue(8));
                    update_contact_Add2Txt.Text = myCStr(dr3.GetValue(9));
                    update_contact_CityTxt.Text = myCStr(dr3.GetValue(10));
                    update_contact_StateTxt.Text = myCStr(dr3.GetValue(11));
                    update_contact_ZipTxt.Text = myCStr(dr3.GetValue(12));

                    update_contact_PhoneACTxt.Text = myCStr(dr3.GetValue(13));
                    update_contact_PhoneNumberTxt.Text = myCStr(dr3.GetValue(14));
                    update_contact_PhoneExtTxt.Text = myCStr(dr3.GetValue(17));
                    update_contact_FaxACTxt.Text = myCStr(dr3.GetValue(15));
                    update_contact_FaxNumberTxt.Text = myCStr(dr3.GetValue(16));
                    update_contact_EmailTxt.Text = myCStr(dr3.GetValue(3));

                    update_contact_PasswordTxt.Text = myCStr(dr3.GetValue(5));
                }

                dr3.Close();
                cmd.Dispose();

                sql = "SELECT DISTINCT(PARTY_ID) "
                    + " FROM ACCOUNT_PRIVS"
                    + " WHERE IUSERID = " + user_id
                    + "       AND CUST_ACCOUNT_ID = 0 ";


                //Response.Write(sql);

                cmd = new OdbcCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                dr3 = cmd.ExecuteReader();

                DataTable dt = new DataTable();

                dt.Columns.Add("PARTY_ID", typeof(string));
                dt.Columns.Add("ADDRESS1", typeof(string));
                dt.Columns.Add("ADDRESS2", typeof(string));
                dt.Columns.Add("CITY", typeof(string));
                dt.Columns.Add("STATE", typeof(string));
                dt.Columns.Add("POSTAL_CODE", typeof(string));
                dt.Columns.Add("PARTY_NAME", typeof(string));
                dt.Columns.Add("ACCOUNT_NUMBER", typeof(string));
                dt.Columns.Add("CUST_ACCOUNT_ID", typeof(string));

                DataRow dr;

                while (dr3.Read())
                {
                    dr = dt.NewRow();

                    sql = "SELECT MIN(ACCOUNT_NUMBER) "
                        + " FROM HZ_CUST_ACCOUNTS "
                        + " WHERE PARTY_ID = " + myCStr(dr3.GetValue(0));

                    OdbcCommand cmd_ = new OdbcCommand(sql, conn);
                    cmd_.CommandType = CommandType.Text;
                    OdbcDataReader dr4 = cmd_.ExecuteReader();

                    if (dr4.Read())
                    {

                        sql = "SELECT HP.PARTY_ID, HL.ADDRESS1, HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE, HP.PARTY_NAME, HCA.ACCOUNT_NUMBER, HCA.CUST_ACCOUNT_ID "
                                + " FROM HZ_CUST_ACCOUNTS HCA INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                                + "      INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                                + "      INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                                + "      INNER JOIN HZ_PARTIES HP ON HCA.PARTY_ID = HP.PARTY_ID "
                                + " WHERE HCA.ACCOUNT_NUMBER = (SELECT MIN(ACCOUNT_NUMBER) "
                                + "                               FROM HZ_CUST_ACCOUNTS "
                                + "                               WHERE ACCOUNT_NUMBER = '" + myCStr(dr4.GetValue(0)) + "' "
                                + "                                     AND STATUS = 'A') "
                                + " 	  AND HCA.STATUS = 'A'  "
                                + " 	  AND HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                                + " ORDER BY HCA.ACCOUNT_NUMBER, HCA.ACCOUNT_NAME ";

                        //Response.Write(sql);

                        OdbcCommand cmd_inner = new OdbcCommand(sql, conn);
                        cmd_inner.CommandType = CommandType.Text;
                        OdbcDataReader dr5 = cmd_inner.ExecuteReader();


                        if (dr5.Read())
                        {
                            dr["PARTY_ID"] = myCStr(dr5.GetValue(0));
                            dr["ADDRESS1"] = myCStr(dr5.GetValue(1));
                            dr["ADDRESS2"] = myCStr(dr5.GetValue(2));
                            dr["CITY"] = myCStr(dr5.GetValue(3));
                            dr["STATE"] = myCStr(dr5.GetValue(4));
                            dr["POSTAL_CODE"] = myCStr(dr5.GetValue(5));
                            dr["PARTY_NAME"] = myCStr(dr5.GetValue(6));
                            dr["ACCOUNT_NUMBER"] = myCStr(dr5.GetValue(7));
                            dr["CUST_ACCOUNT_ID"] = myCStr(dr5.GetValue(8));

                            dt.Rows.Add(dr);
                        }

                        dr5.Close();
                        dr5.Dispose();
                        cmd_.Dispose();

                    }

                    dr4.Close();
                    dr4.Dispose();
                    cmd_.Dispose();
                    
                }

                dr3.Close();
                cmd.Dispose();

                if (dt.Rows.Count > 0)
                {
                    update_entire_acct_grd.DataSource = dt;
                    update_entire_acct_grd.PageIndex = 0;
                    update_entire_acct_grd.DataBind();
                    no_update_entire_acct_tbl.Visible = false;
                    update_entire_acct_grd.Visible = true;

                }
                else
                {
                    update_entire_acct_grd.Visible = false;
                    no_update_entire_acct_tbl.Visible = true;
                }

               
                sql = "SELECT HP.PARTY_ID, HL.ADDRESS1, HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE, HP.PARTY_NAME, HCA.ACCOUNT_NUMBER, HCA.ACCOUNT_NAME, HCA.CUST_ACCOUNT_ID "
                        + " FROM HZ_CUST_ACCOUNTS HCA INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                        + "      INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                        + "      INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                        + "      INNER JOIN HZ_PARTIES HP ON HCA.PARTY_ID = HP.PARTY_ID "
                        + " WHERE HCA.CUST_ACCOUNT_ID IN (SELECT CUST_ACCOUNT_ID "
                        + "                               FROM ACCOUNT_PRIVS "
                        + "                               WHERE IUSERID = " + user_id
                        + "                               AND PARTY_ID <> 0) "
                        + " 	  AND HCA.STATUS = 'A'  "
                        + " 	  AND HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                        + " ORDER BY HCA.ACCOUNT_NUMBER, HCA.ACCOUNT_NAME ";               

                dt = new DataTable();
                cmd = new OdbcCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                OdbcDataAdapter ora1 = new OdbcDataAdapter();
                ora1.SelectCommand = cmd;
                ora1.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    update_locations_grd.DataSource = dt;
                    update_locations_grd.PageIndex = 0;
                    update_locations_grd.DataBind();
                    no_update_locations_tbl.Visible = false;
                    update_locations_grd.Visible = true;

                }
                else
                {
                    update_locations_grd.Visible = false;
                    no_update_locations_tbl.Visible = true;
                }
                cmd.Dispose();

            } 

            catch (Exception ex)
            {
                Response.Write("Can't load Web page***: " + ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            update_user_info_panel.Visible = true;
            user_search_results_panel.Visible = false;
        }


    }

    protected void update_password(object sender, EventArgs e)
    {

        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        OdbcConnection conn = new OdbcConnection(strConnection);


        try
        {
            conn.Open();

            string sql = "SELECT HP.PARTY_NAME, HCA.ACCOUNT_NUMBER "
                        + "FROM HZ_PARTIES HP INNER JOIN HZ_CUST_ACCOUNTS HCA ON HP.PARTY_ID = HCA.PARTY_ID "
                        + "WHERE HCA.PARTY_ID = (SELECT PARTY_ID "
                        + "              	     FROM ACCOUNT_PRIVS "
                        + "		                 WHERE IUSERID = " + cliUserID.Value +") "
                        + "ORDER BY HCA.ACCOUNT_NUMBER";


            //Response.Write(sql);

            OdbcCommand cmd = new OdbcCommand(sql, conn);
            cmd.CommandType = CommandType.Text;
            OdbcDataReader dr3 = cmd.ExecuteReader();

            if (dr3.Read())
            { 
                update_contact_PasswordTxt.Text = myCStr(dr3.GetValue(0)).Substring(0, 4).Trim() + myCStr(dr3.GetValue(1)).Substring(0, 4).Trim();
            }

            dr3.Close();
            dr3.Dispose();

            rest_psswrd.Value = "1";

            cmd.Dispose();
        }

        catch (Exception ex)
        {
            Response.Write("Can't load Web page: " + ex.Message);
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }
        
       
    }

    protected void cancel_update_info(object sender, EventArgs e)
    {
        Session["account_listing"] = null;
        Response.Redirect("meww_createClient.aspx");
    }

    protected void submit_update_info(object sender, EventArgs e)
    {

        update_fname_Pointer.Visible = false;
        update_lname_Pointer.Visible = false;
        update_phoneAC_Pointer.Visible = false;
        update_phoneNumer_Pointer.Visible = false;
        update_email_Pointer.Visible = false;
        required_field_error.Visible = false;

        if (string.IsNullOrEmpty(update_contact_FNameTxt.Text) || string.IsNullOrEmpty(update_contact_LNameTxt.Text) || string.IsNullOrEmpty(update_contact_PhoneACTxt.Text) || string.IsNullOrEmpty(update_contact_PhoneNumberTxt.Text) || string.IsNullOrEmpty(update_contact_EmailTxt.Text))
        {
            update_fname_Pointer.Visible = true;
            update_lname_Pointer.Visible = true;
            update_phoneAC_Pointer.Visible = true;
            update_phoneNumer_Pointer.Visible = true;
            update_email_Pointer.Visible = true;
            required_field_error.Visible = true;
        }
        else
        {

            string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
            OdbcConnection conn = new OdbcConnection(strConnection);
            
            try
            {
                conn.Open();

                OdbcCommand cmd = new OdbcCommand("{call sp_updateClientAcct(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}", conn);
                cmd.CommandType = CommandType.StoredProcedure;


                if (string.IsNullOrEmpty(update_contact_FNameTxt.Text))
                {
                    cmd.Parameters.Add("FIRST_NAME", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("FIRST_NAME", OdbcType.VarChar, 255).Value = update_contact_FNameTxt.Text.ToUpper();
                }


                if (string.IsNullOrEmpty(update_contact_LNameTxt.Text))
                {
                    cmd.Parameters.Add("LAST_NAME", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("LAST_NAME", OdbcType.VarChar, 255).Value = update_contact_LNameTxt.Text.ToUpper();
                }

                if (string.IsNullOrEmpty(update_contact_EmailTxt.Text))
                {
                    cmd.Parameters.Add("EMAIL", OdbcType.VarChar, 255).Value = "#";
                    cmd.Parameters.Add("USERNAME", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("EMAIL", OdbcType.VarChar, 255).Value = update_contact_EmailTxt.Text.ToUpper();
                    cmd.Parameters.Add("USERNAME", OdbcType.VarChar, 255).Value = update_contact_EmailTxt.Text.ToUpper();
                }

                cmd.Parameters.Add("PASSWORD", OdbcType.VarChar, 100).Value = update_contact_PasswordTxt.Text;
                cmd.Parameters.Add("IS_EMPLOYER", OdbcType.TinyInt, 1).Value = 1;
                cmd.Parameters.Add("ACTIVE", OdbcType.TinyInt, 1).Value = 1;
                cmd.Parameters.Add("LAST_LOGIN", OdbcType.Date).Value = DateTime.Now;

                if (string.IsNullOrEmpty(update_contact_Add1Txt.Text))
                {
                    cmd.Parameters.Add("ADDRESS1", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("ADDRESS1", OdbcType.VarChar, 255).Value = update_contact_Add1Txt.Text.ToUpper();
                }

                if (string.IsNullOrEmpty(update_contact_Add2Txt.Text))
                {
                    cmd.Parameters.Add("ADDRESS2", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("ADDRESS2", OdbcType.VarChar, 255).Value = update_contact_Add2Txt.Text.ToUpper();
                }

                if (string.IsNullOrEmpty(update_contact_CityTxt.Text))
                {
                    cmd.Parameters.Add("CITY", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("CITY", OdbcType.VarChar, 255).Value = update_contact_CityTxt.Text.ToUpper();
                }

                if (string.IsNullOrEmpty(update_contact_StateTxt.Text))
                {
                    cmd.Parameters.Add("STATE", OdbcType.Char, 2).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("STATE", OdbcType.Char, 2).Value = update_contact_StateTxt.Text.ToUpper();
                }

                if (string.IsNullOrEmpty(update_contact_ZipTxt.Text))
                {
                    cmd.Parameters.Add("ZIP", OdbcType.VarChar, 10).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("ZIP", OdbcType.VarChar, 10).Value = update_contact_ZipTxt.Text.ToUpper();
                }

                if (string.IsNullOrEmpty(update_contact_PhoneACTxt.Text))
                {
                    cmd.Parameters.Add("PHONE_AC", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("PHONE_AC", OdbcType.VarChar, 255).Value = update_contact_PhoneACTxt.Text.ToUpper();
                }

                if (string.IsNullOrEmpty(update_contact_PhoneNumberTxt.Text))
                {
                    cmd.Parameters.Add("PHONEnUMBER", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("PHONEnUMBER", OdbcType.VarChar, 255).Value = update_contact_PhoneNumberTxt.Text.ToUpper();
                }



                if (string.IsNullOrEmpty(update_contact_FaxACTxt.Text))
                {
                    cmd.Parameters.Add("FAX_AC", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("FAX_AC", OdbcType.VarChar, 255).Value = update_contact_FaxACTxt.Text.ToUpper();
                }

                if (string.IsNullOrEmpty(update_contact_FaxNumberTxt.Text))
                {
                    cmd.Parameters.Add("FAX_NUMBER", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("FAX_NUMBER", OdbcType.VarChar, 255).Value = update_contact_FaxNumberTxt.Text.ToUpper();
                }

                if (string.IsNullOrEmpty(update_contact_PhoneExtTxt.Text))
                {
                    cmd.Parameters.Add("PHONE_EXT", OdbcType.VarChar, 255).Value = "#";
                }
                else
                {
                    cmd.Parameters.Add("PHONE_EXT", OdbcType.VarChar, 255).Value = update_contact_PhoneExtTxt.Text.ToUpper();
                }

                if (rest_psswrd.Value == "1")
                {
                    cmd.Parameters.Add("RESET_PASSOWRD", OdbcType.TinyInt, 1).Value = 1;
                }
                else
                {
                    cmd.Parameters.Add("RESET_PASSOWRD", OdbcType.TinyInt, 1).Value = 0;
                }

                cmd.Parameters.Add("RECEIVE_EMAILS", OdbcType.TinyInt, 1).Value = Convert.ToInt32(recvieveEmailsDrp.SelectedValue);
                cmd.Parameters.Add("USER_ID", OdbcType.Int, 11).Value = Convert.ToInt32(cliUserID.Value);

                cmd.ExecuteNonQuery();

                update_user_nametxt.Text = update_contact_EmailTxt.Text.ToUpper();
                update_passwordTxt.Text = update_contact_PasswordTxt.Text;

                update_acct_was_created_lbl.Text = update_contact_FNameTxt.Text.ToUpper() + " " + update_contact_LNameTxt.Text.ToUpper() + "'S  ACCOUNT HAS BEEN UPDATED!";

                if (rest_psswrd.Value == "1")
                {
                    update_password_changeLbl.Visible = true;

                    string sql = "UPDATE USER_INFO "
                                + "SET SPASSWORD = '" + update_contact_PasswordTxt.Text + "' "
                                + "WHERE IUSERID = " + cliUserID.Value;

                    cmd = new OdbcCommand(sql, conn);

                    cmd.ExecuteNonQuery();
                }
                else
                {
                    update_password_changeLbl.Visible = false;
                }

                update_user_info_panel.Visible = false;
                account_update_panel.Visible = true;
                
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Response.Write("Can't load Web page: " + ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

        }

     }
    
    protected void add_account_to_user(object sender, EventArgs e)
    {
        ImageButton btn = (ImageButton)sender;
        string comm_arg = btn.CommandArgument; 


        addAccount.Value = "1";

        if (comm_arg == "update_acct" || comm_arg == "acct")
        {
            client_acctTC.ActiveTabIndex = 0;
        }
        else
        {
            client_acctTC.ActiveTabIndex = 1;
        }
        contact_info_panel.Visible = false;
        //empSrchPanel.Visible = true;
        client_acctTC.Visible = true;
        empSrchRstsGrd.Visible = false;
        empSrchFieldTxt.Text = "";
    }

    protected void cancelEmpLocSrch(object sender, EventArgs e)
    {
        empLocSrchFieldTxt.Text = "";
        empLocSrchRstsGrd.Visible = false;
        empLocSrchNoRsltsTbl.Visible = false;
        Session["account_listing"] = null;
    }

    protected void searchForEmployerLoc(object sender, EventArgs e)
    {
        string sql = "";
        DataTable dt = new DataTable();

        string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        OdbcConnection conn = new OdbcConnection(strConnection);

        try
        {
            conn.Open();

            if (empLocSrchTYpeDrp.SelectedValue == "0")
            {
                sql = "SELECT HCA.ACCOUNT_NUMBER,HCA.PARTY_ID, HCA.CUST_ACCOUNT_ID, HCA.ACCOUNT_NAME, HL.ADDRESS1, HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE "
                         + "FROM HZ_CUST_ACCOUNTS HCA INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                         + "     INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                         + "	    INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                         + "WHERE HCA.ACCOUNT_NAME LIKE '%" + empLocSrchFieldTxt.Text.ToUpper().Replace("'", "''") + "%' AND "
                         + "      HCA.STATUS = 'A' AND "
                         + "      HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                         + "ORDER BY HCA.ACCOUNT_NUMBER, HCA.ACCOUNT_NAME";
            }
            else
            {
                sql = "SELECT HCA.ACCOUNT_NUMBER,HCA.PARTY_ID, HCA.CUST_ACCOUNT_ID, HCA.ACCOUNT_NAME, HL.ADDRESS1, HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE "
                            + "FROM HZ_CUST_ACCOUNTS HCA INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                            + "     INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                            + "	    INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                            + "WHERE HCA.ACCOUNT_NUMBER LIKE '" + empLocSrchFieldTxt.Text.ToUpper().Replace("'", "''") + "%' AND "
                            + "      HCA.STATUS = 'A' AND "
                            + "      HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                            + "ORDER BY HCA.ACCOUNT_NUMBER, HCA.ACCOUNT_NAME";
            }
            //Response.Write(sql);

            OdbcCommand cmd = new OdbcCommand(sql, conn);
            cmd.CommandType = CommandType.Text;

            OdbcDataAdapter ora1 = new OdbcDataAdapter();
            ora1.SelectCommand = cmd;
            ora1.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                empLocSrchRstsGrd.DataSource = dt;
                empLocSrchRstsGrd.PageIndex = 0;
                empLocSrchRstsGrd.DataBind();
                empLocSrchNoRsltsTbl.Visible = false;
                empLocSrchRstsGrd.Visible = true;

            }
            else
            {
                empLocSrchRstsGrd.Visible = false;
                empLocSrchNoRsltsTbl.Visible = true;
            }
            cmd.Dispose();
        }

        catch (Exception ex)
        {
            Response.Write("Can't load Web page: " + ex.Message);
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }

    }

    protected void empLocSrchRstsGrd_RowDataBound(Object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string sAddress = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ADDRESS1"));



            if (!string.IsNullOrEmpty(sAddress))
            {
                sAddress = sAddress + ", ";
            }
            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ADDRESS2"))))
            {
                sAddress += Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ADDRESS2")) + ", ";
            }

            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "CITY"))))
            {
                sAddress += " " + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "CITY")) + ", ";
            }

            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "STATE"))))
            {
                sAddress += " " + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "STATE"));
            }

            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "POSTAL_CODE"))))
            {
                sAddress += " " + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "POSTAL_CODE"));
            }

            e.Row.Cells[2].Text = sAddress;
        }

    }

    protected void update_locations_grd_RowDataBound(Object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string sAddress = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ADDRESS1"));



            if (!string.IsNullOrEmpty(sAddress))
            {
                sAddress = sAddress + ", ";
            }
            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ADDRESS2"))))
            {
                sAddress += Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ADDRESS2")) + ", ";
            }

            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "CITY"))))
            {
                sAddress += " " + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "CITY")) + ", ";
            }

            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "STATE"))))
            {
                sAddress += " " + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "STATE"));
            }

            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "POSTAL_CODE"))))
            {
                sAddress += " " + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "POSTAL_CODE"));
            }

            e.Row.Cells[2].Text = sAddress;
        }

    }

    protected void update_entire_acct_grd_RowDataBound(Object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string sAddress = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ADDRESS1"));



            if (!string.IsNullOrEmpty(sAddress))
            {
                sAddress = sAddress + ", ";
            }
            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ADDRESS2"))))
            {
                sAddress += Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ADDRESS2")) + ", ";
            }

            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "CITY"))))
            {
                sAddress += " " + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "CITY")) + ", ";
            }

            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "STATE"))))
            {
                sAddress += " " + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "STATE"));
            }

            if (!string.IsNullOrEmpty(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "POSTAL_CODE"))))
            {
                sAddress += " " + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "POSTAL_CODE"));
            }

            e.Row.Cells[2].Text = sAddress;
        }

    }

    protected void empLocSrchRstsGrd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GetEmpLocSrchResultsPaging(e.NewPageIndex);
    }

    protected void GetEmpLocSrchResultsPaging(int pageIndex)
    {

        string sql = "";
        DataTable dt = new DataTable();

        string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        OdbcConnection conn = new OdbcConnection(strConnection);

        try
        {
            conn.Open();

            if (empLocSrchTYpeDrp.SelectedValue == "0")
            {
                sql = "SELECT HCA.ACCOUNT_NUMBER,HCA.PARTY_ID, HCA.CUST_ACCOUNT_ID, HCA.ACCOUNT_NAME, HL.ADDRESS1, HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE "
                         + "FROM HZ_CUST_ACCOUNTS HCA INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                         + "     INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                         + "	    INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                         + "WHERE HCA.ACCOUNT_NAME LIKE '%" + empLocSrchFieldTxt.Text.ToUpper().Replace("'", "''") + "%' AND "
                         + "      HCA.STATUS = 'A' AND "
                         + "      HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                         + "ORDER BY HCA.ACCOUNT_NUMBER, HCA.ACCOUNT_NAME";
            }
            else
            {
                sql = "SELECT HCA.ACCOUNT_NUMBER,HCA.PARTY_ID, HCA.CUST_ACCOUNT_ID, HCA.ACCOUNT_NAME, HL.ADDRESS1, HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE "
                            + "FROM HZ_CUST_ACCOUNTS HCA INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                            + "     INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                            + "	    INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                            + "WHERE HCA.ACCOUNT_NUMBER LIKE '" + empLocSrchFieldTxt.Text.ToUpper().Replace("'", "''") + "%' AND "
                            + "      HCA.STATUS = 'A' AND "
                            + "      HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                            + "ORDER BY HCA.ACCOUNT_NUMBER, HCA.ACCOUNT_NAME";
            }
            //Response.Write(sql);

            OdbcCommand cmd = new OdbcCommand(sql, conn);
            cmd.CommandType = CommandType.Text;

            OdbcDataAdapter ora1 = new OdbcDataAdapter();
            ora1.SelectCommand = cmd;
            ora1.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                empLocSrchRstsGrd.DataSource = dt;
                empLocSrchRstsGrd.PageIndex = pageIndex;
                empLocSrchRstsGrd.DataBind();
                empLocSrchNoRsltsTbl.Visible = false;
                empLocSrchRstsGrd.Visible = true;
            }
            else
            {
                empLocSrchRstsGrd.Visible = false;
                empLocSrchNoRsltsTbl.Visible = true;
            }
            cmd.Dispose();
        }

        catch (Exception ex)
        {
            Response.Write("Can't load Web page: " + ex.Message);
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }
    }

    protected void empLocSrchRstsGrd_RowCommand(Object sender, GridViewCommandEventArgs e)
    {


        if (string.Compare(e.CommandName, "Page") != 0)
        {
            if (addAccount.Value == "0")
            {
                clear_fields();
            }

            int empCustAcctID = Convert.ToInt32(e.CommandArgument);

            //empSrchPanel.Visible = false;

            client_acctTC.Visible = false;

            //ipCasesSrchPanel.Visible = true;
            string sql = "";
            DataTable dt = new DataTable();

            string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
            OdbcConnection conn = new OdbcConnection(strConnection);

            try
            {
                conn.Open();

                sql = "SELECT HCA.PARTY_ID, HL.ADDRESS1, HL.ADDRESS2, HL.CITY, HL.STATE, HL.POSTAL_CODE, HP.PARTY_NAME, HCA.ACCOUNT_NUMBER, HCA.ACCOUNT_NAME, HCA.CUST_ACCOUNT_ID "
                         + "FROM HZ_CUST_ACCOUNTS HCA INNER JOIN HZ_CUST_ACCT_SITES_ALL HCASA ON HCA.CUST_ACCOUNT_ID = HCASA.CUST_ACCOUNT_ID "
                         + "     INNER JOIN HZ_PARTY_SITES HPS ON HCASA.PARTY_SITE_ID = HPS.PARTY_SITE_ID "
                         + "	 INNER JOIN HZ_LOCATIONS HL ON HPS.LOCATION_ID = HL.LOCATION_ID "
                         + "     INNER JOIN HZ_PARTIES HP ON HCA.PARTY_ID = HP.PARTY_ID "
                         + "WHERE HCA.CUST_ACCOUNT_ID = " + empCustAcctID + " AND "
                         + "      HCA.STATUS = 'A' AND "
                         + "      HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                         + "ORDER BY HCA.ACCOUNT_NUMBER, HCA.ACCOUNT_NAME";

                OdbcCommand cmd = new OdbcCommand(sql, conn);
                cmd.CommandType = CommandType.Text;

                OdbcDataReader dr3 = cmd.ExecuteReader();                

                while (dr3.Read())
                {
                    empLocCustAcctID.Value = Convert.ToString(dr3.GetValue(0));
                    contact_Add1Txt.Text = myCStr(dr3.GetValue(1));
                    contact_Add2Txt.Text = myCStr(dr3.GetValue(2));
                    contact_CityTxt.Text = myCStr(dr3.GetValue(3));
                    contact_StateTxt.Text = myCStr(dr3.GetValue(4));
                    contact_ZipTxt.Text = myCStr(dr3.GetValue(5));
                    contact_AcctNameTxt.Text = myCStr(dr3.GetValue(8));
                    contact_PasswordTxt.Text = myCStr(dr3.GetValue(6)).Substring(0, 4).Trim() + myCStr(dr3.GetValue(7)).Substring(0, 4).Trim();

                   if ((DataTable)Session["account_listing"] != null)
                   {
                        account_dt = (DataTable)Session["account_listing"];
                       
                    }

                    DataRow dr;

                    dr = account_dt.NewRow();
                    dr["PARTY_ID"] = myCStr(dr3.GetValue(0));
                    dr["CUST_ACCOUNT_ID"] = myCStr(dr3.GetValue(9));
                    dr["ACCOUNT_NUMBER"] = myCStr(dr3.GetValue(7));
                    dr["PARTY_NAME"] = myCStr(dr3.GetValue(6));
                    dr["ACCOUNT_NAME"] = myCStr(dr3.GetValue(8));
                    if (string.IsNullOrEmpty(myCStr(dr3.GetValue(2))))
                    {
                        dr["ADDRESS"] = myCStr(dr3.GetValue(1)) + " " + myCStr(dr3.GetValue(3)) + ", " + myCStr(dr3.GetValue(4)) + ", " + myCStr(dr3.GetValue(5)) ;
                    }
                    else
                    {
                        dr["ADDRESS"] = myCStr(dr3.GetValue(1)) + " " + myCStr(dr3.GetValue(2)) + ", " + myCStr(dr3.GetValue(3)) + ", " + myCStr(dr3.GetValue(4)) + ", " + myCStr(dr3.GetValue(5));
                    }

                    account_dt.Rows.Add(dr);

                    acctListingGrid.DataSource = account_dt;
                    acctListingGrid.PageIndex = 0;
                    acctListingGrid.DataBind();

                    Session["account_listing"] = account_dt;
                    acctType.Value = "2";

                    if (account_dt.Rows.Count > 0)
                    {
                        acctListingGrid.Visible = true;
                        noAcctListingTbl.Visible = false;
                    }
                    else
                    {
                        acctListingGrid.Visible = false;
                        noAcctListingTbl.Visible = true;
                    }
                    
                }

                dr3.Close();
                dr3.Dispose();
                cmd.Dispose();

                contact_info_panel.Visible = true;
            }

            catch (Exception ex)
            {
                Response.Write("Can't load Web page: " + ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }

    }

    protected void finished_creating_user(object sender, EventArgs e)
    {
        Session["account_listing"] = null;
        Response.Redirect("meww_createClient.aspx");
    }

    protected void new_user_search(object sender, EventArgs e)
    {
        user_search_results_panel.Visible = false;
        user_search_panel.Visible = true;
    }

    protected void deleteEmpLocRow(object sender, EventArgs e)
    {

        GridViewRow gvRow = (GridViewRow)(sender as Control).Parent.Parent;
        int index = gvRow.RowIndex;


        DataTable table = (DataTable)Session["account_listing"];

        table.Rows[index].Delete();

        Session["account_listing"] = table;
        acctListingGrid.DataSource = table;
        acctListingGrid.DataBind();


        if (table.Rows.Count > 0)
        {
            acctListingGrid.Visible = true;
            noAcctListingTbl.Visible = false;
        }
        else
        {
            acctListingGrid.Visible = false;
            noAcctListingTbl.Visible = true;
        }

    }
}