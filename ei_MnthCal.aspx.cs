﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Globalization;
using DayPilot.Utils;
using DayPilot.Web.Ui.Enums;
using DayPilot.Web.Ui.Events;
using DayPilot.Web.Ui.Events.Bubble;
using DayPilot.Web.Ui.Recurrence;
using System.Data.Odbc;

public partial class ei_MnthCal : System.Web.UI.Page
{
    private DataTable table;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ciStatusRptViewGrd.RowDataBound += new GridViewRowEventHandler(ciStatusRptViewGrd_RowDataBound);
            patientApptSrchRsltsGrd.RowDataBound += new GridViewRowEventHandler(patientApptSrchRsltsGrd_RowDataBound);

            ptApptsGrd.RowDataBound += new GridViewRowEventHandler(ptApptsGrd_RowDataBound);

            DateTime start = DateTime.Now.AddDays(-365);
            DateTime end = DateTime.Now.AddDays(150);

            initData();


            //DayPilotMonth1.DataSource = getData(DayPilotMonth1.VisibleStart, DayPilotMonth1.VisibleEnd, (string)DayPilotMonth1.ClientState["filter"]);
            //DayPilotNavigator1.DataSource = getData(DayPilotNavigator1.VisibleStart, DayPilotNavigator1.VisibleEnd, null);
            DayPilotMonth1.DataSource = getData(start, end, (string)DayPilotMonth1.ClientState["filter"]);
            DayPilotNavigator1.DataSource = getData(start, end, null);
            DayPilotMonth1.DataBind();
            DayPilotNavigator1.DataBind();
        }

    }

    protected void DayPilotCalendar1_EventMenuClick(object sender, EventMenuClickEventArgs e)
    {

        if (e.Command == "Delete")
        {
            #region Simulation of database update
            DataRow dr = table.Rows.Find(e.Value);

            if (dr != null)
            {
                table.Rows.Remove(dr);
                table.AcceptChanges();
            }
            #endregion

            
        }
        DateTime start = DateTime.Now.AddDays(-365);
        DateTime end = DateTime.Now.AddDays(150);


        //DayPilotMonth1.DataSource = getData(DayPilotMonth1.VisibleStart, DayPilotMonth1.VisibleEnd, (string)DayPilotMonth1.ClientState["filter"]);
        //DayPilotNavigator1.DataSource = getData(DayPilotNavigator1.VisibleStart, DayPilotNavigator1.VisibleEnd, null);
        DayPilotMonth1.DataSource = getData(start, end, (string)DayPilotMonth1.ClientState["filter"]);
        DayPilotNavigator1.DataSource = getData(start, end, null);
        DayPilotMonth1.DataBind();
        DayPilotNavigator1.DataBind();
    }

    protected void DayPilotMonth1_EventMove(object sender, EventMoveEventArgs e)
    {
        #region Simulation of database update

        DataRow dr = table.Rows.Find(e.Value);

        // recurrent event but no exception
        if (e.Recurrent && !e.RecurrentException)
        {
            DataRow master = table.Rows.Find(e.RecurrentMasterId);

            dr = table.NewRow();
            dr["id"] = Guid.NewGuid().ToString();
            dr["name"] = master["name"];
            dr["start"] = e.NewStart;
            dr["end"] = e.NewEnd;
            dr["recurrence"] = RecurrenceRule.EncodeExceptionModified(e.RecurrentMasterId, e.OldStart);
            table.Rows.Add(dr);
            table.AcceptChanges();

            //DayPilotMonth1.UpdateWithMessage("Recurrent event exception added.");
        }
        // recurrent exception or regular event
        else if (dr != null)
        {
            dr["start"] = e.NewStart;
            dr["end"] = e.NewEnd;
            table.AcceptChanges();

            // DayPilotMonth1.UpdateWithMessage("Event moved.");
        }
        else
        {
            //DayPilotMonth1.UpdateWithMessage("No update.");
        }

        #endregion
        DayPilotMonth1.DataSource = getData(DayPilotMonth1.VisibleStart, DayPilotMonth1.VisibleEnd, (string)DayPilotMonth1.ClientState["filter"]);
        DayPilotMonth1.DataBind();


    }

    protected void DayPilotMonth1_EventResize(object sender, EventResizeEventArgs e)
    {
        #region Simulation of database update

        DataRow dr = table.Rows.Find(e.Value);
        /*
        if (dr != null)
        {
            dr["start"] = e.NewStart;
            dr["end"] = e.NewEnd;
            table.AcceptChanges();
        }
        */

        if (e.Recurrent && !e.RecurrentException)
        {
            DataRow master = table.Rows.Find(e.RecurrentMasterId);

            dr = table.NewRow();
            dr["id"] = Guid.NewGuid().ToString();
            dr["recurrence"] = RecurrenceRule.EncodeExceptionModified(e.RecurrentMasterId, e.OldStart);
            dr["start"] = e.NewStart;
            dr["end"] = e.NewEnd;
            dr["name"] = master["name"];
            table.Rows.Add(dr);
            table.AcceptChanges();
        }
        else if (dr != null)
        {
            dr["start"] = e.NewStart;
            dr["end"] = e.NewEnd;
            table.AcceptChanges();
        }
        #endregion

        DayPilotMonth1.DataSource = getData(DayPilotMonth1.VisibleStart, DayPilotMonth1.VisibleEnd, (string)DayPilotMonth1.ClientState["filter"]);
        DayPilotMonth1.DataBind();
    }

    protected void DayPilotMonth1_TimeRangeSelected(object sender, TimeRangeSelectedEventArgs e)
    {
        /*#region Simulation of database update

        DataRow dr = table.NewRow();
        dr["start"] = e.Start;
        dr["end"] = e.End;
        dr["id"] = Guid.NewGuid().ToString();
        dr["name"] = "New event";

        table.Rows.Add(dr);
        table.AcceptChanges();
        #endregion*/
        initData();
        DayPilotMonth1.DataSource = getData(DayPilotMonth1.VisibleStart, DayPilotMonth1.VisibleEnd, (string)DayPilotMonth1.ClientState["filter"]);
        DayPilotMonth1.DataBind();
       // DayPilotMonth1.Update();
    }

    protected void DayPilotMonth1_BeforeEventRender(object sender, DayPilot.Web.Ui.Events.Month.BeforeEventRenderEventArgs e)
    {
    //this gets triggered for every appointment on the calandar
        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        OdbcConnection conn = new OdbcConnection(strConnection);



        try
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();

                string appt_type = "";

                string sql = "SELECT WS.APPT_TYPE "
                           + "FROM WW_SCHEDULE WS "
                           + "WHERE WS.APPT_ID = " + e.Value;

                OdbcCommand cmd = new OdbcCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                OdbcDataReader dr3 = cmd.ExecuteReader();
                if (dr3.Read())
                {
                    appt_type = myCStr(dr3.GetValue(0));
                }
                //This is wierd
                cmd.Dispose();
                dr3.Close();
                dr3.Dispose();
                if (appt_type != "")
                {

                    if (appt_type == "IP" || appt_type == "RE-CH")
                    {
                        sql = "SELECT SR.STATUSREPORTID "
                               + "FROM WW_SCHEDULE WS INNER JOIN STATUS_REPORTS SR ON WS.APPT_DATE = SR.IPDOE "
                               + "WHERE WS.IP_CUST_ACCOUNT_ID = SR.IPCUSTACCTID AND "
                               + "WS.APPT_ID = " + e.Value;

                        cmd = new OdbcCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        dr3 = cmd.ExecuteReader();

                        if (dr3.Read())
                        {
                            if (e.InnerHTML.Length < 17)
                            {
                                e.InnerHTML = "<span style=\"font-size: 7pt;\">" + e.InnerHTML + "</span><span style=\"font-size: 7pt;\"> |</span><A HREF='ei_ViewSR.aspx?sr_=" + myCStr(dr3.GetValue(0)) + "' style=\"color: #1155a3;text-decoration: underline;font-size: 8px\" onmouseover=\"this.style.color= 'red'\" onmouseout=\"this.style.color='#1155a3'\" target=\"_blank\">Sts Rprt</A>";
                            }
                            else
                            {
                                e.InnerHTML = "<span style=\"font-size: 7pt;\">" + e.InnerHTML + "</span><span style=\"font-size: 7pt;\"> |</span><A HREF='ei_ViewSR.aspx?sr_=" + myCStr(dr3.GetValue(0)) + "' style=\"color: #1155a3;text-decoration: underline;font-size: 8px\" onmouseover=\"this.style.color= 'red'\" onmouseout=\"this.style.color='#1155a3'\" target=\"_blank\">Rprt</A>";
                            }


                        }
                        else
                        {
                            if (e.InnerHTML.Length < 17)
                            {
                                e.InnerHTML = "<span style=\"font-size: 7pt;\">" + e.InnerHTML + "</span><span style=\"font-size: 7pt;\"> |</span><A HREF='ei_RqstSR.aspx?sr_=" + e.Value + "' style=\"color: #1155a3;text-decoration: underline;font-size: 8px\" onmouseover=\"this.style.color= 'red'\" onmouseout=\"this.style.color='#1155a3'\" target=\"_blank\">Rqst Rprt</A>";
                            }
                            else
                            {
                                e.InnerHTML = "<span style=\"font-size: 7pt;\">" + e.InnerHTML + "</span><span style=\"font-size: 7pt;\"> |</span><A HREF='ei_RqstSR.aspx?sr_=" + e.Value + "' style=\"color: #1155a3;text-decoration: underline;font-size: 8px\" onmouseover=\"this.style.color= 'red'\" onmouseout=\"this.style.color='#1155a3'\" target=\"_blank\">Rprt</A>";
                            }
                        }

                        dr3.Close();
                        cmd.Dispose();

                    }
                    else
                    {
                        //This can never be hit in here it needs to be moved out and changed.
                        if (e.InnerHTML.Substring(0, 3) == "PT*")
                        {
                            e.BackgroundColor = "lightsteelblue";
                        }
                        else
                        {
                            switch (appt_type)
                            {
                                case "MRI":
                                    e.BackgroundColor = "lightsalmon";
                                    break;
                                case "SUR":
                                    e.BackgroundColor = "lightgreen";
                                    break;
                            }
                        }


                        e.InnerHTML = "<span style=\"font-size: 7pt;\">" + e.InnerHTML.Substring(3) + "</span>";

                    }

                }
            }
        }
        catch (Exception ex)
        {
            Response.Write("Exception: " + Convert.ToString(ex));
        }
        finally
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
                conn.Dispose();
            }
        }
    }

    protected void DayPilotBubble1_RenderContent(object sender, RenderEventArgs e)
    {

        if (e is RenderEventBubbleEventArgs)
        {
            RenderEventBubbleEventArgs re = e as RenderEventBubbleEventArgs;
            if (!re.Text.Contains("New Appt") && !re.Text.Contains("PT*"))
            {

                OdbcCommand objSqlCmd = new OdbcCommand();
                string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
                OdbcConnection conn = new OdbcConnection(strConnection);

                try
                {
                    conn.Open();

                    string sql = "SELECT SUBMITTED FROM WW_SCHEDULE WHERE APPT_ID = " + re.Value;

                    OdbcCommand cmd = new OdbcCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    OdbcDataReader dr3 = cmd.ExecuteReader();

                    if (dr3.Read())
                    {
                    }

                    dr3.Close();
                    dr3.Dispose();
                    cmd.Dispose();

                    sql = "SELECT WS.APPT_ID, WS.START_TIME, WS.APPT_TYPE, WAPD.PERSON_FIRST_NAME, WAPD.PERSON_MIDDLE_NAME, "
                                + "        WAPD.PERSON_LAST_NAME, WAPD.JGZZ_FISCAL_CODE, WAD.ATTRIBUTE4, WAD.ATTRIBUTE5 , WAD.ATTRIBUTE6, "
                                + "        WAD.ATTRIBUTE7, WAD.ATTRIBUTE8 , WAD.ATTRIBUTE9, WAD.ATTRIBUTE10, WAD.ATTRIBUTE11 ,  "
                                + "        WAD.ATTRIBUTE12 , WAD.ATTRIBUTE13 , WAD.ATTRIBUTE14 , WAD.ATTRIBUTE15 , WS.PRIME_PHONE, "
                                + "        WS.ATTRIBUTE_CATEGORY,  WS.ADDITIONAL_NOTES, WS.SUBMITTED  "
                                + "FROM WW_SCHEDULE WS INNER JOIN HZ_PARTIES WAPD ON WS.IP_PARTY_ID = WAPD.PARTY_ID "
                                + "        INNER JOIN WW_APPOINTMENT_DETAILS WAD ON WS.APPT_ID = WAD.APPT_ID "
                                + " WHERE WAD.APPT_ID = " + re.Value;


                    cmd = new OdbcCommand(sql, conn);
                    cmd.CommandType = CommandType.Text;
                    dr3 = cmd.ExecuteReader();

                    if (dr3.Read())
                    {
                        string name = myCStr(dr3.GetValue(3));
                        string doctor = "UNKNOWN";
                        string type = "UNKNOWN";
                        string employer = "UKNOWN";
                        string notes = "NONE";
                        string location = "UNKNOWN";
                        string ancils = "";
                        string cm = "UNKNOWN";
                        string phone = "";
                        string ds = "NONE";

                        int submitted = Convert.ToInt16(myCStr(dr3.GetValue(22)));


                        if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(4))))
                        {
                            name += "  " + myCStr(dr3.GetValue(4));
                        }

                        name += "  " + myCStr(dr3.GetValue(5));

                        switch (myCStr(dr3.GetValue(2)))
                        {
                            case "A":
                                type = "ANCILLARY";
                                break;
                            case "CON":
                                type = "CONSULTATION";
                                break;
                            case "DEP":
                                type = "DEPOSITION";
                                break;
                            case "IP":
                                type = "INITIAL PANEL";
                                break;
                            case "RTW":
                                type = "RETURN TO WORK";
                                break;
                            case "RE-CH":
                                type = "RE-CHECK";
                                break;
                            case "RE-OP":
                                type = "RE-OPEN";
                                break;
                            case "SUR":
                                type = "SURGERY";
                                break;
                            case "EIO":
                                type = "ENTER IN ONLY";
                                break;
                            case "PT0":
                                type = "PT ONLY";
                                break;
                            case "BLOCK":
                                type = "BLOCKED APPOINTMENT";
                                break;
                            case "EMG":
                                type = "EMG";
                                break;
                            case "MRI":
                                type = "MRI";
                                break;

                        }


                        if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(21))))
                        {
                            notes = myCStr(dr3.GetValue(21));
                        }

                        if (submitted == 1)
                        {
                            sql = "SELECT HCP.PHONE_AREA_CODE, HCP.PHONE_NUMBER, HCP.PHONE_EXTENSION, HCP.PHONE_LINE_TYPE , HCP.PRIMARY_FLAG "
                                    + "FROM WW_SCHEDULE WS INNER JOIN HZ_CONTACT_POINTS HCP ON IP_PARTY_ID = OWNER_TABLE_ID "
                                    + "WHERE WS.APPT_ID = " + re.Value + " AND "
                                    + "      HCP.STATUS = 'A' AND "
                                    + "      HCP.PRIMARY_FLAG = 'Y' "
                                    + "ORDER BY CONTACT_POINT_ID ";

                            OdbcCommand cmd_ = new OdbcCommand(sql, conn);
                            cmd_.CommandType = CommandType.Text;
                            OdbcDataReader dr3_ = cmd_.ExecuteReader();

                            if (dr3_.Read())
                            {
                                phone = "(" + myCStr(dr3_.GetValue(0)) + ") " + myCStr(dr3_.GetValue(1));

                                if (!string.IsNullOrEmpty(myCStr(dr3_.GetValue(2))))
                                {
                                    phone += " x" + myCStr(dr3_.GetValue(2));
                                }

                                switch (myCStr(dr3_.GetValue(3)))
                                {
                                    case "GEN":
                                        phone = "(h) " + phone;
                                        break;
                                    case "WK":
                                        phone = "(w) " + phone;
                                        break;
                                    case "MOBILE":
                                        phone = "(c) " + phone;
                                        break;

                                }
                            }
                            else
                            {
                                dr3_.Close();
                                dr3_.Dispose();
                                cmd_.Dispose();

                                sql = "SELECT HCP.PHONE_AREA_CODE, HCP.PHONE_NUMBER, HCP.PHONE_EXTENSION, HCP.PHONE_LINE_TYPE , HCP.PRIMARY_FLAG "
                                    + "FROM WW_SCHEDULE WS INNER JOIN HZ_CONTACT_POINTS HCP ON IP_PARTY_ID = OWNER_TABLE_ID "
                                    + "WHERE WS.APPT_ID = " + re.Value + " AND "
                                    + "      HCP.STATUS = 'A' AND "
                                    + "      ROWNUM = 1 "
                                    + "ORDER BY CONTACT_POINT_ID ";

                                cmd_ = new OdbcCommand(sql, conn);
                                cmd_.CommandType = CommandType.Text;
                                dr3_ = cmd_.ExecuteReader();

                                if (dr3_.Read())
                                {
                                    phone = "(" + myCStr(dr3_.GetValue(0)) + ") " + myCStr(dr3_.GetValue(1));

                                    if (!string.IsNullOrEmpty(myCStr(dr3_.GetValue(2))))
                                    {
                                        phone += " x" + myCStr(dr3_.GetValue(2));
                                    }

                                    switch (myCStr(dr3_.GetValue(3)))
                                    {
                                        case "GEN":
                                            phone = "(h) " + phone;
                                            break;
                                        case "WK":
                                            phone = "(w) " + phone;
                                            break;
                                        case "MOBILE":
                                            phone = "(c) " + phone;
                                            break;

                                    }
                                }
                            }

                            dr3_.Close();
                            dr3_.Dispose();
                            cmd_.Dispose();

                        }
                        else
                        {
                            sql = "SELECT AREA_CODE, PHONE_NUMBER, EXTENSION, LINE_TYPE FROM WW_APPT_PERSON_CONTACTS WHERE APPT_ID = " + re.Value + " AND STATUS = 'A' AND PRIMARY_FLAG = 'Y' ORDER BY CONTACT_POINT_ID ";

                            OdbcCommand cmd_ = new OdbcCommand(sql, conn);
                            cmd_.CommandType = CommandType.Text;
                            OdbcDataReader dr3_ = cmd_.ExecuteReader();

                            if (dr3_.Read())
                            {
                                phone = "(" + myCStr(dr3_.GetValue(0)) + ") " + myCStr(dr3_.GetValue(1));

                                if (!string.IsNullOrEmpty(myCStr(dr3_.GetValue(2))))
                                {
                                    phone += " x" + myCStr(dr3_.GetValue(2));
                                }

                                switch (myCStr(dr3_.GetValue(3)))
                                {
                                    case "GEN":
                                        phone = "(h) " + phone;
                                        break;
                                    case "WK":
                                        phone = "(w) " + phone;
                                        break;
                                    case "MOBILE":
                                        phone = "(c) " + phone;
                                        break;

                                }
                            }
                            else
                            {
                                dr3_.Close();
                                dr3_.Dispose();
                                cmd_.Dispose();

                                sql = "SELECT AREA_CODE, PHONE_NUMBER, EXTENSION, LINE_TYPE FROM WW_APPT_PERSON_CONTACTS WHERE APPT_ID = " + re.Value + " AND STATUS = 'A' AND ROWNUM = 1 ORDER BY CONTACT_POINT_ID ";

                                cmd_ = new OdbcCommand(sql, conn);
                                cmd_.CommandType = CommandType.Text;
                                dr3_ = cmd_.ExecuteReader();

                                if (dr3_.Read())
                                {
                                    phone = "(" + myCStr(dr3_.GetValue(0)) + ") " + myCStr(dr3_.GetValue(1));

                                    if (!string.IsNullOrEmpty(myCStr(dr3_.GetValue(2))))
                                    {
                                        phone += " x" + myCStr(dr3_.GetValue(2));
                                    }

                                    switch (myCStr(dr3_.GetValue(3)))
                                    {
                                        case "GEN":
                                            phone = "(h) " + phone;
                                            break;
                                        case "WK":
                                            phone = "(w) " + phone;
                                            break;
                                        case "MOBILE":
                                            phone = "(c) " + phone;
                                            break;

                                    }
                                }
                            }

                            dr3_.Close();
                            dr3_.Dispose();
                            cmd_.Dispose();
                        }

                        if (myCStr(dr3.GetValue(20)) == "PATIENT")
                        {

                            switch (myCStr(dr3.GetValue(12)))
                            {
                                case "DKA":
                                    doctor = "RICHARD KATZ, MD";
                                    break;
                                case "DRF":
                                    doctor = "JOHN METCALF, MD";
                                    break;
                                case "DRQ":
                                    doctor = "LESTER PRINCE, MD";
                                    break;
                                case "DSS":
                                    doctor = "SCOTT SHEPPARD, MD";
                                    break;
                                case "DMW":
                                    doctor = "MARK WILSON, MD";
                                    break;
                                case "DCC":
                                    doctor = "CORINNE CONTE, MD";
                                    break;
                                case "DSG":
                                    doctor = "STEPHEN GERWEL, MD";
                                    break;
                                case "DHW":
                                    doctor = "WALTER HOOVER, MD";
                                    break;
                                case "DSJ":
                                    doctor = "SHADRACH JONES, MD";
                                    break;
                                case "DRS":
                                    doctor = "ROY STAHLMAN, MD";
                                    break;
                                case "DRT":
                                    doctor = "BLAIR TRUXAL, MD";
                                    break;
                                case "PAZ":
                                    doctor = "ROBERT ZIRKLE, PA-C";
                                    break;
                                case "CON":
                                    if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(9))))
                                    {
                                        doctor = myCStr(dr3.GetValue(9));
                                    }
                                    else
                                    {
                                        doctor = "UNKNOWN";
                                    }

                                    break;

                            }

                            if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(11))))
                            {
                                switch (myCStr(dr3.GetValue(11)))
                                {
                                    case "BA":
                                        ds = "BREATH ALCOHOL";
                                        break;
                                    case "D":
                                        ds = "DRUG SCREEN";
                                        break;
                                    case "D & BA":
                                        ds = "DRUG SCREEN AND BREATH ALCOHOL";
                                        break;
                                    case "D & SA":
                                        ds = "DRUG SCREEN AND SALIVA ALCOHOL";
                                        break;
                                    case "HT":
                                        ds = "HAIR TEST";
                                        break;
                                    case "SA":
                                        ds = "SALIVA ALCOHOL";
                                        break;
                                }

                            }

                            if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(13))))
                            {
                                switch (myCStr(dr3.GetValue(13)))
                                {
                                    case "SB":
                                        cm = "SHEREEA BOWSWELL";
                                        break;
                                    case "RB":
                                        cm = "REBECCA BURGET";
                                        break;
                                    case "CB":
                                        cm = "CHRISTIE BRAKE";
                                        break;
                                    case "DC":
                                        cm = "DEBBIE CRINGLE";
                                        break;
                                    case "AC":
                                        cm = "ALYSIA CUNNINGHAM";
                                        break;
                                    case "RD":
                                        cm = "RACHEL DENNIS";
                                        break;
                                    case "AD":
                                        cm = "APRIL DONOFRIO";
                                        break;
                                    case "BF":
                                        cm = "BRANDINE FEHIR";
                                        break;
                                    case "AF":
                                        cm = "ALYSSA FETTINGER";
                                        break;
                                    case "JG":
                                        cm = "JOY GORHAM";
                                        break;
                                    case "MH":
                                        cm = "MICHELLE HOSTAL";
                                        break;
                                    case "TM":
                                        cm = "TOM MCCOY";
                                        break;
                                    case "BM":
                                        cm = "BRANDIE MOYER";
                                        break;
                                    case "ER":
                                        cm = "ERICA REED";
                                        break;
                                    case "MR":
                                        cm = "MICHELLE RODGERS";
                                        break;
                                    case "DS":
                                        cm = "DAVID STREICH";
                                        break;
                                    case "JT":
                                        cm = "JULIE TYO";
                                        break;
                                    case "MW":
                                        cm = "MARIA WILSON";
                                        break;
                                    case "TW":
                                        cm = "TONI WOODRING";
                                        break;
                                    case "SA":
                                        cm = "STACEY ASTFALK";
                                        break;
                                    case "EB":
                                        cm = "ERIN BAKER";
                                        break;
                                    case "AB":
                                        cm = "AMY BRADFORD";
                                        break;
                                    case "PC":
                                        cm = "PATIENT CARE";
                                        break;
                                    case "CC":
                                        cm = "CHRISTIE CORNELIUS";
                                        break;
                                    case "NF":
                                        cm = "NICOLE FALCONER";
                                        break;
                                    case "TG":
                                        cm = "TRACEY FERRY";
                                        break;
                                    case "TF":
                                        cm = "TRACY FERRY";
                                        break;
                                    case "AJ":
                                        cm = "AMBER JANECZKO";
                                        break;
                                    case "DJ":
                                        cm = "DARIN JANECZKO";
                                        break;
                                    case "MK":
                                        cm = "MALLORY KAROM";
                                        break;
                                    case "MA":
                                        cm = "MICHAEL KELLY";
                                        break;
                                    case "AL":
                                        cm = "ANDREA LEONARD";
                                        break;
                                    case "KL":
                                        cm = "KAITLIN LUCAS";
                                        break;
                                    case "MM":
                                        cm = "MELISSA MAKOFSKE";
                                        break;
                                    case "KM":
                                        cm = "KAREN MARKITELL";
                                        break;
                                    case "HM":
                                        cm = "HEATHER MARSHALL";
                                        break;
                                    case "JP":
                                        cm = "JENNIFER PUSKAR";
                                        break;
                                    case "ES":
                                        cm = "EMILY SMITH";
                                        break;
                                    case "SS":
                                        cm = "STACEY SUPER";
                                        break;
                                    case "BT":
                                        cm = "BRANDINE TURNER";
                                        break;

                                }

                            }

                            if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(10))))
                            {
                                location = myCStr(dr3.GetValue(10));

                            }

                            if (submitted == 1)
                            {
                                sql = "SELECT HCA_EMP.ACCOUNT_NAME "
                                + "  FROM HZ_CUST_ACCOUNTS HCA_EMP, "
                                + "        HZ_CUST_ACCOUNTS HCA_IP, "
                                + "        HZ_CUST_ACCT_RELATE_ALL HCARA, "
                                + "        HZ_CUST_ACCT_SITES_ALL hcasa, "
                                + "        WW_SCHEDULE WS  "
                                + "   WHERE HCA_EMP.CUST_ACCOUNT_ID = HCARA.RELATED_CUST_ACCOUNT_ID AND "
                                + "         HCARA.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID AND  "
                                + "         HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT' AND "
                                + "         HCARA.STATUS = 'A' AND  "
                                + "         HCA_EMP.CUST_ACCOUNT_ID = hcasa.CUST_ACCOUNT_ID AND  "
                                + "         HCA_EMP.STATUS = 'A' AND "
                                + "         HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' AND "
                                + "         WS.IP_CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID AND "
                                + "         WS.APPT_ID =" + re.Value;
                            }
                            else
                            {
                                sql = "SELECT HCA.ACCOUNT_NAME, WS.APPT_ID "
                                    + " FROM WW_SCHEDULE WS INNER JOIN WW_APPT_RELATIONSHIPS WAR ON WS.APPT_ID = WAR.APPT_ID  "
                                    + "      INNER JOIN HZ_CUST_ACCOUNTS HCA ON WAR.REL_CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID "
                                    + " WHERE HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                                    + "       AND WAR.STATUS = 'A' "
                                    + "       AND WS.APPT_ID = " + re.Value;
                            }

                            OdbcCommand cmd_ = new OdbcCommand(sql, conn);
                            cmd_.CommandType = CommandType.Text;
                            OdbcDataReader dr3_ = cmd_.ExecuteReader();

                            if (dr3_.Read())
                            {
                                if (!string.IsNullOrEmpty(myCStr(dr3_.GetValue(0))))
                                {
                                    employer = myCStr(dr3_.GetValue(0));
                                }
                            }


                            if (type == "INITIAL PANEL")
                            {
                                re.InnerHTML = "<div style='text-align:left;'><b><u>Appointment Details</u></b><br /><br /><b>Name:</b> " + name + "<br /><b>Employer:</b> " + employer + "<BR/><b>SS#:</b> " + myCStr(dr3.GetValue(6)) + "<br/><b>Phone:</b> " + phone + "<br/><b>Date:</b> " + myCStr(dr3.GetValue(1)).Replace(":00 ", " ") + "<br /><b>Location:</b> " + location + "<br /><b>Doctor:</b> " + doctor + "<br /><b>Case Manager:</b>  " + cm + "<br /><b>Type:</b> " + type + "<br /><b>Drug Screen:</b>  " + ds + "<br /><b>Notes:</b> " + notes + "</div>";
                            }
                            else
                            {
                                re.InnerHTML = "<div style='text-align:left;'><b><u>Appointment Details</u></b><br /><br /><b>Name:</b> " + name + "<br /><b>Employer:</b> " + employer + "<BR/><b>SS#:</b> " + myCStr(dr3.GetValue(6)) + "<br/><b>Phone:</b> " + phone + "<br/><b>Date:</b> " + myCStr(dr3.GetValue(1)).Replace(":00 ", " ") + "<br /><b>Location:</b> " + location + "<br /><b>Doctor:</b> " + doctor + "<br /><b>Case Manager:</b>  " + cm + "<br /><b>Type:</b> " + type + "<br /><b>Notes:</b> " + notes + "</div>";
                            }
                        }
                        else if (myCStr(dr3.GetValue(20)) == "ANCILLARY SERVICE")
                        {
                            for (int i = 10; i <= 18; i++)
                            {
                                if (myCStr(dr3.GetValue(i)) != "0")
                                {
                                    ancils += ", " + myCStr(dr3.GetValue(i));
                                }

                            }

                            if (!string.IsNullOrEmpty(ancils))
                            {
                                // REMOVE THE BEGINGING ', '
                                ancils = ancils.Substring(2);
                            }

                            if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(9))))
                            {
                                switch (myCStr(dr3.GetValue(9)))
                                {
                                    case "SB":
                                        cm = "SHEREEA BOWSWELL";
                                        break;
                                    case "RB":
                                        cm = "REBECCA BURGET";
                                        break;
                                    case "CB":
                                        cm = "CHRISTIE BRAKE";
                                        break;
                                    case "DC":
                                        cm = "DEBBIE CRINGLE";
                                        break;
                                    case "AC":
                                        cm = "ALYSIA CUNNINGHAM";
                                        break;
                                    case "RD":
                                        cm = "RACHEL DENNIS";
                                        break;
                                    case "AD":
                                        cm = "APRIL DONOFRIO";
                                        break;
                                    case "BF":
                                        cm = "BRANDINE FEHIR";
                                        break;
                                    case "AF":
                                        cm = "ALYSSA FETTINGER";
                                        break;
                                    case "JG":
                                        cm = "JOY GORHAM";
                                        break;
                                    case "MH":
                                        cm = "MICHELLE HOSTAL";
                                        break;
                                    case "TM":
                                        cm = "TOM MCCOY";
                                        break;
                                    case "BM":
                                        cm = "BRANDIE MOYER";
                                        break;
                                    case "ER":
                                        cm = "ERICA REED";
                                        break;
                                    case "MR":
                                        cm = "MICHELLE RODGERS";
                                        break;
                                    case "DS":
                                        cm = "DAVID STREICH";
                                        break;
                                    case "JT":
                                        cm = "JULIE TYO";
                                        break;
                                    case "MW":
                                        cm = "MARIA WILSON";
                                        break;
                                    case "TW":
                                        cm = "TONI WOODRING";
                                        break;
                                    case "SA":
                                        cm = "STACEY ASTFALK";
                                        break;
                                    case "EB":
                                        cm = "ERIN BAKER";
                                        break;
                                    case "AB":
                                        cm = "AMY BRADFORD";
                                        break;
                                    case "PC":
                                        cm = "PATIENT CARE";
                                        break;
                                    case "CC":
                                        cm = "CHRISTIE CORNELIUS";
                                        break;
                                    case "NF":
                                        cm = "NICOLE FALCONER";
                                        break;
                                    case "TG":
                                        cm = "TRACEY FERRY";
                                        break;
                                    case "TF":
                                        cm = "TRACY FERRY";
                                        break;
                                    case "AJ":
                                        cm = "AMBER JANECZKO";
                                        break;
                                    case "DJ":
                                        cm = "DARIN JANECZKO";
                                        break;
                                    case "MK":
                                        cm = "MALLORY KAROM";
                                        break;
                                    case "MA":
                                        cm = "MICHAEL KELLY";
                                        break;
                                    case "AL":
                                        cm = "ANDREA LEONARD";
                                        break;
                                    case "KL":
                                        cm = "KAITLIN LUCAS";
                                        break;
                                    case "MM":
                                        cm = "MELISSA MAKOFSKE";
                                        break;
                                    case "KM":
                                        cm = "KAREN MARKITELL";
                                        break;
                                    case "HM":
                                        cm = "HEATHER MARSHALL";
                                        break;
                                    case "JP":
                                        cm = "JENNIFER PUSKAR";
                                        break;
                                    case "ES":
                                        cm = "EMILY SMITH";
                                        break;
                                    case "SS":
                                        cm = "STACEY SUPER";
                                        break;
                                    case "BT":
                                        cm = "BRANDINE TURNER";
                                        break;

                                }

                            }

                            if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(7))))
                            {
                                //cm = myCStr(dr3.GetValue(13));
                                switch (myCStr(dr3.GetValue(7)))
                                {
                                    case "DKA":
                                        doctor = "RICHARD KATZ, MD";
                                        break;
                                    case "DRF":
                                        doctor = "JOHN METCALF, MD";
                                        break;
                                    case "DRQ":
                                        doctor = "LESTER PRINCE, MD";
                                        break;
                                    case "DSS":
                                        doctor = "SCOTT SHEPPARD, MD";
                                        break;
                                    case "DMW":
                                        doctor = "MARK WILSON, MD";
                                        break;
                                    case "DCC":
                                        doctor = "CORINNE CONTE, MD";
                                        break;
                                    case "DSG":
                                        doctor = "STEPHEN GERWEL, MD";
                                        break;
                                    case "DHW":
                                        doctor = "WALTER HOOVER, MD";
                                        break;
                                    case "DSJ":
                                        doctor = "SHADRACH JONES, MD";
                                        break;
                                    case "DRS":
                                        doctor = "ROY STAHLMAN, MD";
                                        break;
                                    case "DRT":
                                        doctor = "BLAIR TRUXAL, MD";
                                        break;
                                    case "PAZ":
                                        doctor = "ROBERT ZIRKLE, PA-C";
                                        break;
                                    case "CON":
                                        if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(3))))
                                        {
                                            doctor = myCStr(dr3.GetValue(13));
                                        }
                                        else
                                        {
                                            doctor = "UNKNOWN";
                                        }

                                        break;
                                }
                            }

                            if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(8))))
                            {
                                switch (myCStr(dr3.GetValue(8)))
                                {
                                    case "DT":
                                        location = "AGH";
                                        break;
                                    case "AP":
                                        location = "AIRPORT";
                                        break;
                                    case "CB":
                                        location = "CRANBERRY";
                                        break;
                                    case "MV":
                                        location = "MONROEVILLE";
                                        break;
                                    case "SH":
                                        location = "SOUTH HILLS";
                                        break;
                                    case "CON":
                                        location = "CONSULTING";
                                        break;

                                }
                            }


                            if (submitted == 1)
                            {
                                sql = "SELECT  HCA.ATTRIBUTE3 "
                                      + " FROM HZ_CUST_ACCOUNTS HCA, "
                                      + "      WW_SCHEDULE WS "
                                      + " WHERE  WS.IP_CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID "
                                      + "        AND WS.APPT_ID = " + re.Value;
                            }
                            else
                            {
                                sql = "SELECT ATTRIBUTE3 "
                                + "    FROM WW_APPOINTMENT_DETAILS "
                                + "    WHERE APPT_ID =" + re.Value;
                            }

                            OdbcCommand cmd_ = new OdbcCommand(sql, conn);
                            cmd_.CommandType = CommandType.Text;
                            OdbcDataReader dr3_ = cmd_.ExecuteReader();

                            if (dr3_.Read())
                            {
                                if (!string.IsNullOrEmpty(myCStr(dr3_.GetValue(0))))
                                {
                                    employer = myCStr(dr3_.GetValue(0));
                                }
                            }

                            re.InnerHTML = "<div style='text-align:left;'><b><u>Appointment Details</u></b><br /><br /><b>Name:</b> " + name + "<br /><b>Employer:</b> " + employer + "<BR/><b>SS#:</b> " + myCStr(dr3.GetValue(6)) + "<br/><b>Phone:</b> " + phone + "<br/><b>Date:</b> " + myCStr(dr3.GetValue(1)).Replace(":00 ", " ") + "<br /><b>Location:</b> " + location + "<br /><b>Doctor:</b> " + doctor + "<br /><b>Case Manager:</b>  " + cm + "<br /><b>Type:</b> " + type + "<br /><b>Ancillaries:</b> " + ancils + "<br /><b>Notes:</b> " + notes + "</div>";
                        }
                        else
                        {
                            re.InnerHTML = "<div style='text-align:left;'><b><u>Appointment Details</u></b><br /><br /><b>Name:</b> " + name + "<br /><b>Employer:</b> " + employer + "<BR/><b>SS#:</b> " + myCStr(dr3.GetValue(6)) + "<br/><b>Phone:</b> " + phone + "<br/><b>Date:</b> " + myCStr(dr3.GetValue(1)).Replace(":00 ", " ") + "<br /><b>Location:</b> " + location + "<br /><b>Doctor:</b> " + doctor + "<br /><b>Case Manager:</b>  " + cm + "<br /><b>Type:</b> " + type + "<br /><b>Notes:</b> " + notes + "</div>";
                        }


                    }

                    cmd.Dispose();
                    dr3.Close();
                    dr3.Dispose();

                }
                catch (Exception ex)
                {
                    Response.Write("Exception: " + Convert.ToString(ex));
                }
                finally
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            else
            {
                if (re.Text.Contains("New Appt"))
                {
                    re.InnerHTML = "NEW APPOINTMENT";
                }
                else
                {
                    OdbcCommand objSqlCmd = new OdbcCommand();
                    string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
                    OdbcConnection conn = new OdbcConnection(strConnection);
                    string sql = "";

                    try
                    {
                        conn.Open();

                        string name = "";
                        string facility = "UNKNOWN";
                        string attended = "UNKNOWN";
                        string employer = "UKNOWN";
                        string verified = "UNKNOWN";
                        string phone = "";
                        string ssn = "UNKNOWN";
                        string session_date = "UNKNOWN";
                        string treatment_type = "UNKNOWN";

                        sql = "SELECT HP.PERSON_FIRST_NAME, HP.PERSON_MIDDLE_NAME, HP.PERSON_LAST_NAME, HPP.JGZZ_FISCAL_CODE, HCP.PHONE_LINE_TYPE, "
                                + " CONCAT('(', HCP.PHONE_AREA_CODE, ') ', HCP.PHONE_NUMBER),  PPC.PT_CENTER_NAME, PPS.SESSION_DATE, PPS.ATTENDED, PPS.VERIFIED, "
                                + "  HCA_EMP.ACCOUNT_NAME, HCP.PHONE_EXTENSION, PCH.TREATMENT_TYPE "
                                + "FROM HZ_PARTIES HP INNER JOIN HZ_CUST_ACCOUNTS HCA ON HP.PARTY_ID = HCA.PARTY_ID "
                                + "	 INNER JOIN WW_PC_PT_SESSIONS PPS ON HCA.CUST_ACCOUNT_ID = PPS.IP_CUST_ACCOUNT_ID "
                                + "	 INNER JOIN WW_PC_CLAIM_PT_DETAIL PCPD ON PPS.CLAIM_ID = PCPD.CLAIM_ID "
                                + "	 INNER JOIN WW_PC_PT_CENTERS PPC ON PCPD.PT_CENTER_ID = PPC.PT_CENTER_ID "
                                + "  INNER JOIN WW_PC_CLAIM_HEADER PCH ON PCPD.CLAIM_ID = PCH.CLAIM_ID "
                                + "	 INNER JOIN HZ_PERSON_PROFILES HPP ON HP.PARTY_ID = HPP.PARTY_ID "
                                + "	 INNER JOIN HZ_CONTACT_POINTS HCP ON HP.PARTY_ID = HCP.OWNER_TABLE_ID "
                                + "	 INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA.CUST_ACCOUNT_ID  = HCARA.CUST_ACCOUNT_ID "
                                + "	 INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID "
                                + "WHERE HCP.OWNER_TABLE_NAME = 'HZ_PARTIES'  "
                                + "	  AND HP.PARTY_TYPE = 'PERSON' "
                                + "	  AND PPS.PT_SESSION_ID = " + re.Value + " "
                                + "	  AND HCP.PRIMARY_FLAG = 'Y' "
                                + "	  AND HPP.EFFECTIVE_END_DATE IS NULL "
                                + "	  AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                                + "	  AND HCARA.STATUS = 'A' "
                                + "	ORDER BY PPS.SESSION_DATE ";

                        OdbcCommand cmd = new OdbcCommand(sql, conn);
                        cmd.CommandType = CommandType.Text;
                        OdbcDataReader dr3 = cmd.ExecuteReader();

                        if (dr3.Read())
                        {
                            name = myCStr(dr3.GetValue(0));

                            if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(1))))
                            {
                                name += "  " + myCStr(dr3.GetValue(1));
                            }

                            name += "  " + myCStr(dr3.GetValue(2));

                            phone = myCStr(dr3.GetValue(5));

                            if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(11))))
                            {
                                phone += " x" + myCStr(dr3.GetValue(11));
                            }

                            switch (myCStr(dr3.GetValue(12)))
                            {
                                case "OT":
                                    treatment_type = "OCCUPATIONAL THERAPY";
                                    break;
                                case "PT":
                                    treatment_type = "PHYSICAL THERAPY";
                                    break;
                                default:
                                    treatment_type = "NONE SPECIFIED";
                                    break;

                            }

                            switch (myCStr(dr3.GetValue(4)))
                            {
                                case "GEN":
                                    phone = "(h) " + phone;
                                    break;
                                case "WK":
                                    phone = "(w) " + phone;
                                    break;
                                case "MOBILE":
                                    phone = "(c) " + phone;
                                    break;

                            }

                            if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(10))))
                            {
                                employer = myCStr(dr3.GetValue(10));
                            }

                            if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(6))))
                            {
                                facility = myCStr(dr3.GetValue(6)).Replace("-AND-", "&");
                            }

                            if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(3))))
                            {
                                ssn = "XXX-XX-" + myCStr(dr3.GetValue(3)).Substring(7);
                            }

                            if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(8))))
                            {
                                switch (myCStr(dr3.GetValue(8)))
                                {
                                    case "A":
                                        attended = "ATTENDED";
                                        break;
                                    case "C":
                                        attended = "CANCELED";
                                        break;
                                    case "X":
                                        attended = "NOT SCHEDULED";
                                        break;

                                }
                            }

                            if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(9))))
                            {
                                switch (myCStr(dr3.GetValue(9)))
                                {
                                    case "Y":
                                        verified = "YES";
                                        break;
                                    case "N":
                                        verified = "NO";
                                        break;
                                    case "NA":
                                        verified = "N/A";
                                        break;

                                }
                            }

                            if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(7))))
                            {
                                session_date = myCStr(dr3.GetValue(7)).Replace("12:00:00 AM", "");
                            }


                            dr3.Close();
                            dr3.Dispose();
                            cmd.Dispose();

                            re.InnerHTML = "<div style='text-align:left;'><b><u>P.T. Appointment Details</u></b><br /><br /><b>Name: </b>" + name + "<br /><b>Employer:</b> " + employer + "<BR/><b>SS#:</b> " + ssn + "<br/><b>Phone:</b> " + phone + "<br/><b>Date:</b> " + session_date + "<br /><b>Type:</b>  " + treatment_type + "<br /><b>Facility:</b> " + facility + "<br /><b>Attended:</b> " + attended + "<br /><b>Verified:</b>  " + verified + "<br /></div>";

                        }
                    }
                    catch (Exception ex)
                    {
                        Response.Write("Exception: " + Convert.ToString(ex));
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }


            }

        }
    }

    protected void DayPilotNavigator1_VisibleRangeChanged(object sender, EventArgs e)
    {
        initData();
        DateTime start = DateTime.Now.AddDays(-365);
        DateTime end = DateTime.Now.AddDays(150);

       // initData();

        DayPilotMonth1.DataSource = getData(start, end, (string)DayPilotMonth1.ClientState["filter"]);
        DayPilotNavigator1.DataSource = getData(start, end, null);
        DayPilotMonth1.DataBind();
        DayPilotNavigator1.DataBind();
       

    }

    protected void DayPilotMonth1_Command(object sender, DayPilot.Web.Ui.Events.CommandEventArgs e)
    {

        //To make sure table isn't null
        initData();

        switch (e.Command)
        {
            case "navigate":
                DayPilotMonth1.StartDate = (DateTime)e.Data["start"];
                DayPilotMonth1.DataSource = getData(DayPilotMonth1.VisibleStart, DayPilotMonth1.VisibleEnd, (string)DayPilotMonth1.ClientState["filter"]);
                DayPilotMonth1.DataBind();
                DayPilotMonth1.Update();
                break;
            case "filter":
                DayPilotMonth1.DataSource = getData(DayPilotMonth1.VisibleStart, DayPilotMonth1.VisibleEnd, (string)DayPilotMonth1.ClientState["filter"]);
                DayPilotMonth1.DataBind();
                DayPilotMonth1.Update();
                break;
            case "refresh":
                DayPilotMonth1.DataSource = getData(DayPilotMonth1.VisibleStart, DayPilotMonth1.VisibleEnd, (string)DayPilotMonth1.ClientState["filter"]);
                DayPilotMonth1.DataBind();
                DayPilotMonth1.Update();
                break;

        }


    }

    /// <summary>
    /// This method should normally load the data from the database.
    /// We will load our copy from a Session, just simulating a database.
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="filter"></param>
    /// <returns></returns>
    private DataTable getData(DateTime start, DateTime end, string filter)
    {
        String select;
        if (String.IsNullOrEmpty(filter))
        {
            select = String.Format(" (([end] <= '{1:s}') AND ([start] >= '{0:s}'))", start, end, filter);
        }
        else
        {
            select = String.Format(" (([end] <= '{1:s}') AND ([start] >= '{0:s}')) and [name] like '%{2}%'", start, end, filter);
            //            throw new Exception(select);
        }


        DataRow[] rows = table.Select(select);

        DataTable filtered = table.Clone();

        foreach (DataRow r in rows)
        {
            filtered.ImportRow(r);
        }

        return filtered;
    }

    /// <summary>
    /// Make sure a copy of the data is in the Session so users can try changes on their own copy.
    /// </summary>
    private void initData()
    {

        Session["MonthRecurrence"] = null;
        Session["MonthRecurrence"] = getData();
        table = (DataTable)Session["MonthRecurrence"];
    }

    protected void DayPilotMonth1_BeforeCellRender(object sender, DayPilot.Web.Ui.Events.Month.BeforeCellRenderEventArgs e)
    {
        // e.
        e.BackgroundColor = "white";
        e.CssClass = "image";

    }

    protected void DayPilotMonth1_EventClick(object sender, EventClickEventArgs e)
    {
        monthlySchedulePanel.Visible = false;
        viewCasePanel.Visible = true;

        getApptDetails(Convert.ToInt32(e.Value));

    }

    protected string myCStr(object test)
    {
        if (test == DBNull.Value)
        {
            return (String.Empty);
        }
        else
        {
            return Convert.ToString(test);
        }

    }

    private DataTable getData()
    {
        Int32 iPartyID = Convert.ToInt32(Session["PartyID"]);

        DataTable dt;
        dt = new DataTable();
        dt.Columns.Add("start", typeof(DateTime));
        dt.Columns.Add("end", typeof(DateTime));
        dt.Columns.Add("name", typeof(string));
        dt.Columns.Add("id", typeof(string));
        dt.Columns.Add("allday", typeof(bool));
        dt.Columns.Add("recurrence", typeof(string));

        //02/03/17 fixing the issue with the drop downs and start and end dates.
        DateTime start = DateTime.Now.AddDays(-365);
        DateTime end = DateTime.Now.AddDays(150);


        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        OdbcConnection conn = new OdbcConnection(strConnection);

        try
        {
            conn.Open();

            string sql = "";
            
            OdbcCommand cmd = new OdbcCommand(sql, conn);
            cmd.CommandType = CommandType.Text;
            OdbcDataReader dr3;

            int mult_locs = Convert.ToInt32(Session["mult_locs"]);
            string cust_account_sql_str = "AND ( ";

            if ((mult_locs == 1) && (!string.IsNullOrEmpty(Convert.ToString(Session["CustAccountID"]))))
            {
                cust_account_sql_str += " HCARA.RELATED_CUST_ACCOUNT_ID = " + Session["CustAccountID"] + " ) ";
            }
                else
            {
                cust_account_sql_str = string.Empty;
            }

            if ((mult_locs > 1)  && (!string.IsNullOrEmpty(Convert.ToString(Session["CustAccountID"]))))
            {
                string str = Convert.ToString(Session["CustAccount_IDs"]);
                char[] separator = new char[] { ',' };
                string[] strSplitArr = str.Split(separator);

                foreach (string arrStr in strSplitArr)
                {
                    cust_account_sql_str = cust_account_sql_str + " HCARA.RELATED_CUST_ACCOUNT_ID = " + arrStr + " OR ";
                }
                cust_account_sql_str = " AND ( " + cust_account_sql_str.Substring(0, cust_account_sql_str.Length - 4) + " ) ";
            }

            if (mult_locs == 0)
            {
                cust_account_sql_str = string.Empty;
            }
            if(iPartyID == 15576)
                cust_account_sql_str = string.Empty;

            if (schedTypeDrp.SelectedValue == "2")
            {

                if (Convert.ToString(Session["InsCarrier"]) == "I" && check_ins_partner(iPartyID))
                {
                    sql = "SELECT "
                       + "	WS.APPT_ID, "
                       + "	WS.START_TIME, "
                       + "	WS.END_TIME, "
                       + "	PRIME_PHONE, "
                       + "	WAD.ATTRIBUTE11, "
                       + "	WAD.ATTRIBUTE7, "
                       + "	WAPD.PERSON_FIRST_NAME, "
                       + "	WAPD.PERSON_MIDDLE_NAME, "
                       + "	WAPD.PERSON_LAST_NAME, "
                       + "	WAD.ATTRIBUTE_CATEGORY, "
                       + "	WS.APPT_TYPE, "
                       + "	WS.SUBMITTED, "
                       + "	WS.ADDITIONAL_NOTES "
                       + "FROM "
                       + "	WW_SCHEDULE WS "
                       + "INNER JOIN WW_APPOINTMENT_DETAILS WAD ON WS.APPT_ID = WAD.APPT_ID "
                       + "INNER JOIN HZ_PARTIES WAPD ON WS.IP_PARTY_ID = WAPD.PARTY_ID "
                       + "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON WS.IP_CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                       + "INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID "
                       + "WHERE "
                       + "	WS. STATUS = 'A' "
                       + "AND HCARA. STATUS = 'A' "
                       + "AND HCARA.RELATED_CUST_ACCOUNT_ID = " + Session["CustAccountID"].ToString() + " "
                       + "AND WS.IP_CUST_ACCOUNT_ID IN (SELECT HCA_IP.CUST_ACCOUNT_ID "
                       + "								 FROM HZ_PARTIES HP_IP INNER JOIN HZ_CUST_ACCOUNTS HCA_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID "
                       + "									  INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_IP.CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                       + "									  INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID "
                       + "								 WHERE HCA_INS.PARTY_ID = " + iPartyID
                       + "									   AND HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' "
                       + "									   AND HCARA.STATUS = 'A' "
                       + "									   AND HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT')				 "
                       + "AND WS.IP_PARTY_ID <> 0 "
                       + "AND WS.ATTRIBUTE_CATEGORY = 'PATIENT' "
                       + "AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                       //+ "AND( "
                       //+ "	WS.APPT_TYPE = 'IP' "
                       //+ "	OR WS.APPT_TYPE = 'RE-CH') "
                       + "AND WS.START_TIME BETWEEN '" + start.ToString("yyyy-MM-dd") + "' AND '" + end.ToString("yyyy-MM-dd") + "' "
                       + "GROUP BY "
                       + "	WS.APPT_ID, "
                       + "	WS.START_TIME, "
                       + "	WS.END_TIME, "
                       + "	PRIME_PHONE, "
                       + "	WAD.ATTRIBUTE11, "
                       + "	WAD.ATTRIBUTE7, "
                       + "	WAPD.PERSON_FIRST_NAME, "
                       + "	WAPD.PERSON_MIDDLE_NAME, "
                       + "	WAPD.PERSON_LAST_NAME, "
                       + "	WAD.ATTRIBUTE_CATEGORY, "
                       + "	WS.APPT_TYPE, "
                       + "	WS.SUBMITTED, "
                       + "	WS.ADDITIONAL_NOTES "
                       + "ORDER BY "
                       + "	WS.APPT_ID DESC ";
                }
                else if (Convert.ToString(Session["InsCarrier"]) == "Y" && check_ins_partner(iPartyID))
                {
                    sql = "SELECT "
                        + "	WS.APPT_ID, "
                        + "	WS.START_TIME, "
                        + "	WS.END_TIME, "
                        + "	PRIME_PHONE, "
                        + "	WAD.ATTRIBUTE11, "
                        + "	WAD.ATTRIBUTE7, "
                        + "	WAPD.PERSON_FIRST_NAME, "
                        + "	WAPD.PERSON_MIDDLE_NAME, "
                        + "	WAPD.PERSON_LAST_NAME, "
                        + "	WAD.ATTRIBUTE_CATEGORY, "
                        + "	WS.APPT_TYPE, "
                        + "	WS.SUBMITTED, "
                        + "	WS.ADDITIONAL_NOTES "
                        + "FROM "
                        + "	WW_SCHEDULE WS "
                        + "INNER JOIN WW_APPOINTMENT_DETAILS WAD ON WS.APPT_ID = WAD.APPT_ID "
                        + "INNER JOIN HZ_PARTIES WAPD ON WS.IP_PARTY_ID = WAPD.PARTY_ID "
                        + "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON WS.IP_CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                        + "INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID "
                        + "WHERE "
                        + "	WS. STATUS = 'A' "
                        + "AND HCARA. STATUS = 'A' "
                        + "AND WS.IP_CUST_ACCOUNT_ID IN (SELECT HCA_IP.CUST_ACCOUNT_ID "
                        + "								 FROM HZ_PARTIES HP_IP INNER JOIN HZ_CUST_ACCOUNTS HCA_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID "
                        + "									  INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_IP.CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                        + "									  INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID "
                        + "								 WHERE HCA_INS.PARTY_ID = " + iPartyID
                        + "									   AND HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' "
                        + "									   AND HCARA.STATUS = 'A' "
                        + "									   AND HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT')				 "
                        + "AND WS.IP_PARTY_ID <> 0 "
                        + "AND WS.ATTRIBUTE_CATEGORY = 'PATIENT' "
                        + "AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                        //+ "AND( "
                        //+ "	WS.APPT_TYPE = 'IP' "
                        //+ "	OR WS.APPT_TYPE = 'RE-CH') "
                        + "AND WS.START_TIME BETWEEN '" + start.ToString("yyyy-MM-dd") + "' AND '" + end.ToString("yyyy-MM-dd") + "' "
                        + "GROUP BY "
                        + "	WS.APPT_ID, "
                        + "	WS.START_TIME, "
                        + "	WS.END_TIME, "
                        + "	PRIME_PHONE, "
                        + "	WAD.ATTRIBUTE11, "
                        + "	WAD.ATTRIBUTE7, "
                        + "	WAPD.PERSON_FIRST_NAME, "
                        + "	WAPD.PERSON_MIDDLE_NAME, "
                        + "	WAPD.PERSON_LAST_NAME, "
                        + "	WAD.ATTRIBUTE_CATEGORY, "
                        + "	WS.APPT_TYPE, "
                        + "	WS.SUBMITTED, "
                        + "	WS.ADDITIONAL_NOTES "
                        + "ORDER BY "
                        + "	WS.APPT_ID DESC ";
                }
                else
                {
                    sql = "SELECT WS.APPT_ID, WS.START_TIME, WS.END_TIME,PRIME_PHONE, WAD.ATTRIBUTE11,WAD.ATTRIBUTE7, WAPD.PERSON_FIRST_NAME,  "
                    + "		WAPD.PERSON_MIDDLE_NAME, WAPD.PERSON_LAST_NAME, WAD.ATTRIBUTE_CATEGORY, WS.APPT_TYPE, WS.SUBMITTED, WS.ADDITIONAL_NOTES   "
                    + "FROM WW_SCHEDULE WS INNER JOIN WW_APPOINTMENT_DETAILS WAD ON WS.APPT_ID = WAD.APPT_ID   "
                    + "	 INNER JOIN HZ_PARTIES WAPD ON WS.IP_PARTY_ID = WAPD.PARTY_ID   "
                    + "     INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON WS.IP_CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID  "
                    + "	 INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID  "
                    + "WHERE WS.STATUS = 'A'   "
                    + "		AND HCARA.STATUS = 'A'   "
                    + "		AND HCA_EMP.PARTY_ID = " + iPartyID
                    + "		AND WS.IP_PARTY_ID <> 0   "
                    + cust_account_sql_str + " "
                    + "		AND WS.ATTRIBUTE_CATEGORY = 'PATIENT'   "
                    + "		AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER'  "
                   // + "		AND (WS.APPT_TYPE =  'IP' OR WS.APPT_TYPE =  'RE-CH' OR WS.APPT_TYPE =  'CON')     "
                    + "		AND WS.START_TIME BETWEEN '" + start.ToString("yyyy-MM-dd") + "' AND '" + end.ToString("yyyy-MM-dd") + "' "
                    + "GROUP BY WS.APPT_ID, WS.START_TIME, WS.END_TIME,PRIME_PHONE, WAD.ATTRIBUTE11,WAD.ATTRIBUTE7, WAPD.PERSON_FIRST_NAME, WAPD.PERSON_MIDDLE_NAME, WAPD.PERSON_LAST_NAME, WAD.ATTRIBUTE_CATEGORY, WS.APPT_TYPE, WS.SUBMITTED, WS.ADDITIONAL_NOTES ORDER BY WS.APPT_ID DESC  ";


                }

                //Response.Write(sql + "<BR>");

                cmd = new OdbcCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                dr3 = cmd.ExecuteReader();

                while (dr3.Read())
                {
                    string ipName = "";
                    string apptType = "";

                    DataRow dr;

                    dr = dt.NewRow();
                    dr["id"] = Convert.ToString(myCStr(dr3.GetValue(0)));
                    dr["start"] = Convert.ToDateTime(myCStr(dr3.GetValue(1)).Replace(".", ""));
                    dr["end"] = Convert.ToDateTime(myCStr(dr3.GetValue(2)).Replace(".", ""));
                    dr["allday"] = false;


                    ipName = myCStr(dr3.GetValue(6)).Substring(0, 1) + ". " + myCStr(dr3.GetValue(8));

                    if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(5))))
                    {
                        if (myCStr(dr3.GetValue(5)) != "0")
                        {
                            apptType = myCStr(dr3.GetValue(10)) + "/" + myCStr(dr3.GetValue(5));
                        }
                        else
                        {
                            apptType = myCStr(dr3.GetValue(10));
                        }
                    }
                    else
                    {
                        apptType = myCStr(dr3.GetValue(4));
                    }

                    if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(9))))
                    {
                        // this will be for the WorkWell scheduler, so the can see all of the info about the appointment
                        //dr["name"] = Convert.ToString(ipName + " | " + apptType);                   

                        // this will be for the clients to view the patient's name and the hyperlink to the status report.
                        dr["name"] = Convert.ToString(ipName);

                    }
                    else
                    {
                        dr["name"] = "New Appt";
                    }
                    dt.Rows.Add(dr);
                }
                cmd.Dispose();
                dr3.Close();
                cmd.Dispose();
            }







            if (schedTypeDrp.SelectedValue == "0" )
            {

                if (Convert.ToString(Session["InsCarrier"]) == "I" && check_ins_partner(iPartyID))
                {
                     sql = "SELECT "
                        + "	WS.APPT_ID, "
                        + "	WS.START_TIME, "
                        + "	WS.END_TIME, "
                        + "	PRIME_PHONE, "
                        + "	WAD.ATTRIBUTE11, "
                        + "	WAD.ATTRIBUTE7, "
                        + "	WAPD.PERSON_FIRST_NAME, "
                        + "	WAPD.PERSON_MIDDLE_NAME, "
                        + "	WAPD.PERSON_LAST_NAME, "
                        + "	WAD.ATTRIBUTE_CATEGORY, "
                        + "	WS.APPT_TYPE, "
                        + "	WS.SUBMITTED, "
                        + "	WS.ADDITIONAL_NOTES "
                        + "FROM "
                        + "	WW_SCHEDULE WS "
                        + "INNER JOIN WW_APPOINTMENT_DETAILS WAD ON WS.APPT_ID = WAD.APPT_ID "
                        + "INNER JOIN HZ_PARTIES WAPD ON WS.IP_PARTY_ID = WAPD.PARTY_ID "
                        + "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON WS.IP_CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                        + "INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID "
                        + "WHERE "
                        + "	WS. STATUS = 'A' "
                        + "AND HCARA. STATUS = 'A' "
                        + "AND HCARA.RELATED_CUST_ACCOUNT_ID = " + Session["CustAccountID"].ToString() + " "
                        + "AND WS.IP_CUST_ACCOUNT_ID IN (SELECT HCA_IP.CUST_ACCOUNT_ID "
                        + "								 FROM HZ_PARTIES HP_IP INNER JOIN HZ_CUST_ACCOUNTS HCA_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID "
                        + "									  INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_IP.CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                        + "									  INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID "
                        + "								 WHERE HCA_INS.PARTY_ID = " + iPartyID
                        + "									   AND HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' "
                        + "									   AND HCARA.STATUS = 'A' "
                        + "									   AND HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT')				 "
                        + "AND WS.IP_PARTY_ID <> 0 "
                        + "AND WS.ATTRIBUTE_CATEGORY = 'PATIENT' "
                        + "AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                        + "AND( "
                        + "	WS.APPT_TYPE = 'IP' "
                        + "	OR WS.APPT_TYPE = 'RE-CH') "
                        + "AND WS.START_TIME BETWEEN '" + start.ToString("yyyy-MM-dd") + "' AND '" + end.ToString("yyyy-MM-dd") + "' "
                        + "GROUP BY "
                        + "	WS.APPT_ID, "
                        + "	WS.START_TIME, "
                        + "	WS.END_TIME, "
                        + "	PRIME_PHONE, "
                        + "	WAD.ATTRIBUTE11, "
                        + "	WAD.ATTRIBUTE7, "
                        + "	WAPD.PERSON_FIRST_NAME, "
                        + "	WAPD.PERSON_MIDDLE_NAME, "
                        + "	WAPD.PERSON_LAST_NAME, "
                        + "	WAD.ATTRIBUTE_CATEGORY, "
                        + "	WS.APPT_TYPE, "
                        + "	WS.SUBMITTED, "
                        + "	WS.ADDITIONAL_NOTES "
                        + "ORDER BY "
                        + "	WS.APPT_ID DESC ";
                }
                else if (Convert.ToString(Session["InsCarrier"]) == "Y" && check_ins_partner(iPartyID))
                {
                    sql = "SELECT "
                        + "	WS.APPT_ID, "
                        + "	WS.START_TIME, "
                        + "	WS.END_TIME, "
                        + "	PRIME_PHONE, "
                        + "	WAD.ATTRIBUTE11, "
                        + "	WAD.ATTRIBUTE7, "
                        + "	WAPD.PERSON_FIRST_NAME, "
                        + "	WAPD.PERSON_MIDDLE_NAME, "
                        + "	WAPD.PERSON_LAST_NAME, "
                        + "	WAD.ATTRIBUTE_CATEGORY, "
                        + "	WS.APPT_TYPE, "
                        + "	WS.SUBMITTED, "
                        + "	WS.ADDITIONAL_NOTES "
                        + "FROM "
                        + "	WW_SCHEDULE WS "
                        + "INNER JOIN WW_APPOINTMENT_DETAILS WAD ON WS.APPT_ID = WAD.APPT_ID "
                        + "INNER JOIN HZ_PARTIES WAPD ON WS.IP_PARTY_ID = WAPD.PARTY_ID "
                        + "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON WS.IP_CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                        + "INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID "
                        + "WHERE "
                        + "	WS. STATUS = 'A' "
                        + "AND HCARA. STATUS = 'A' "
                        + "AND WS.IP_CUST_ACCOUNT_ID IN (SELECT HCA_IP.CUST_ACCOUNT_ID "
                        + "								 FROM HZ_PARTIES HP_IP INNER JOIN HZ_CUST_ACCOUNTS HCA_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID "
                        + "									  INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_IP.CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                        + "									  INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID "
                        + "								 WHERE HCA_INS.PARTY_ID = " + iPartyID
                        + "									   AND HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' "
                        + "									   AND HCARA.STATUS = 'A' "
                        + "									   AND HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT')				 "
                        + "AND WS.IP_PARTY_ID <> 0 "
                        + "AND WS.ATTRIBUTE_CATEGORY = 'PATIENT' "
                        + "AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                        + "AND( "
                        + "	WS.APPT_TYPE = 'IP' "
                        + "	OR WS.APPT_TYPE = 'RE-CH') "
                        + "AND WS.START_TIME BETWEEN '" + start.ToString("yyyy-MM-dd") + "' AND '" + end.ToString("yyyy-MM-dd") + "' "
                        + "GROUP BY "
                        + "	WS.APPT_ID, "
                        + "	WS.START_TIME, "
                        + "	WS.END_TIME, "
                        + "	PRIME_PHONE, "
                        + "	WAD.ATTRIBUTE11, "
                        + "	WAD.ATTRIBUTE7, "
                        + "	WAPD.PERSON_FIRST_NAME, "
                        + "	WAPD.PERSON_MIDDLE_NAME, "
                        + "	WAPD.PERSON_LAST_NAME, "
                        + "	WAD.ATTRIBUTE_CATEGORY, "
                        + "	WS.APPT_TYPE, "
                        + "	WS.SUBMITTED, "
                        + "	WS.ADDITIONAL_NOTES "
                        + "ORDER BY "
                        + "	WS.APPT_ID DESC ";
                }
                else
                {
                    sql = "SELECT WS.APPT_ID, WS.START_TIME, WS.END_TIME,PRIME_PHONE, WAD.ATTRIBUTE11,WAD.ATTRIBUTE7, WAPD.PERSON_FIRST_NAME,  "
                    + "		WAPD.PERSON_MIDDLE_NAME, WAPD.PERSON_LAST_NAME, WAD.ATTRIBUTE_CATEGORY, WS.APPT_TYPE, WS.SUBMITTED, WS.ADDITIONAL_NOTES   "
                    + "FROM WW_SCHEDULE WS INNER JOIN WW_APPOINTMENT_DETAILS WAD ON WS.APPT_ID = WAD.APPT_ID   "
                    + "	 INNER JOIN HZ_PARTIES WAPD ON WS.IP_PARTY_ID = WAPD.PARTY_ID   "
                    + "     INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON WS.IP_CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID  "
                    + "	 INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID  "
                    + "WHERE WS.STATUS = 'A'   "
                    + "		AND HCARA.STATUS = 'A'   "
                    + "		AND HCA_EMP.PARTY_ID = " + iPartyID
                    + "		AND WS.IP_PARTY_ID <> 0   "
                    + cust_account_sql_str + " "
                    + "		AND WS.ATTRIBUTE_CATEGORY = 'PATIENT'   "
                    + "		AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER'  "
                    + "		AND (WS.APPT_TYPE =  'IP' OR WS.APPT_TYPE =  'RE-CH' OR WS.APPT_TYPE =  'CON')     "
                    + "		AND WS.START_TIME BETWEEN '" + start.ToString("yyyy-MM-dd") + "' AND '" + end.ToString("yyyy-MM-dd") + "' "
                    + "GROUP BY WS.APPT_ID, WS.START_TIME, WS.END_TIME,PRIME_PHONE, WAD.ATTRIBUTE11,WAD.ATTRIBUTE7, WAPD.PERSON_FIRST_NAME, WAPD.PERSON_MIDDLE_NAME, WAPD.PERSON_LAST_NAME, WAD.ATTRIBUTE_CATEGORY, WS.APPT_TYPE, WS.SUBMITTED, WS.ADDITIONAL_NOTES ORDER BY WS.APPT_ID DESC  ";


                }
                
                //Response.Write(sql + "<BR>");

                cmd = new OdbcCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                dr3 = cmd.ExecuteReader();

                while (dr3.Read())
                {
                    string ipName = "";
                    string apptType = "";

                    DataRow dr;

                    dr = dt.NewRow();
                    dr["id"] = Convert.ToString(myCStr(dr3.GetValue(0)));
                    dr["start"] = Convert.ToDateTime(myCStr(dr3.GetValue(1)).Replace(".", ""));
                    dr["end"] = Convert.ToDateTime(myCStr(dr3.GetValue(2)).Replace(".", ""));
                    dr["allday"] = false;


                    ipName = myCStr(dr3.GetValue(6)).Substring(0, 1) + ". " + myCStr(dr3.GetValue(8));

                    if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(5))))
                    {
                        if (myCStr(dr3.GetValue(5)) != "0")
                        {
                            apptType = myCStr(dr3.GetValue(10)) + "/" + myCStr(dr3.GetValue(5));
                        }
                        else
                        {
                            apptType = myCStr(dr3.GetValue(10));
                        }
                    }
                    else
                    {
                        apptType = myCStr(dr3.GetValue(4));
                    }

                    if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(9))))
                    {
                        // this will be for the WorkWell scheduler, so the can see all of the info about the appointment
                        //dr["name"] = Convert.ToString(ipName + " | " + apptType);                   

                        // this will be for the clients to view the patient's name and the hyperlink to the status report.
                        dr["name"] = Convert.ToString(ipName);

                    }
                    else
                    {
                        dr["name"] = "New Appt";
                    }
                    dt.Rows.Add(dr);
                }
                cmd.Dispose();
                dr3.Close();
                cmd.Dispose();
            }
            //This is patient care we don't need this for now its not even on godaddy
            /*
            if (schedTypeDrp.SelectedValue == "1" || schedTypeDrp.SelectedValue == "2")
            {
                if (Convert.ToInt16(Session["UserID"]) != 28)
                {

    
                    if (Convert.ToString(Session["InsCarrier"]) == "Y"  && check_ins_partner(iPartyID))
                    {
                        sql = "SELECT IP.PERSON_FIRST_NAME, IP.PERSON_MIDDLE_NAME, IP.PERSON_LAST_NAME, PT.SESSION_DATE, PT.PT_SESSION_ID "
                            + "FROM (SELECT HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME, "
                            + "		     HP_IP.PERSON_LAST_NAME, HCA_IP.CUST_ACCOUNT_ID "
                            + "		FROM HZ_PARTIES HP_IP INNER JOIN HZ_CUST_ACCOUNTS HCA_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID "
                            + "			 INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_IP.CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                            + "			 INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID "
                            + "		WHERE HCA_INS.PARTY_ID = " + iPartyID
                            + "			  AND HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' "
                            + "			  AND HCARA.STATUS = 'A' "
                            + "			  AND HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT') IP  "
                            + "		INNER JOIN (SELECT PPS.SESSION_DATE, PPS.PT_SESSION_ID, PPS.IP_CUST_ACCOUNT_ID  "
                            + "							  FROM WW_PC_PT_SESSIONS PPS  "
                            + "												INNER JOIN WW_PC_CLAIM_PT_DETAIL PCPD ON PPS.CLAIM_ID = PCPD.CLAIM_ID  "
                            + "												INNER JOIN WW_PC_PT_CENTERS PPC ON PCPD.PT_CENTER_ID = PPC.PT_CENTER_ID  "
                            + "												INNER JOIN WW_PC_CLAIM_HEADER PCH ON PPS.CLAIM_ID = PCH.CLAIM_ID  "
                            + "												INNER JOIN WW_PC_PATIENTS PP ON PCH.PATIENT_ID = PP.PATIENT_ID  "
                            + "								WHERE PPS.SESSION_DATE BETWEEN '" + Convert.ToDateTime(DayPilotNavigator1.VisibleStart).ToString("yyyy-MM-dd") + "' AND '" + Convert.ToDateTime(DayPilotNavigator1.VisibleEnd).ToString("yyyy-MM-dd") + "'  ) PT ON IP.CUST_ACCOUNT_ID = PT.IP_CUST_ACCOUNT_ID  "
                            + "GROUP BY PT_SESSION_ID ,PERSON_FIRST_NAME, PERSON_MIDDLE_NAME, PERSON_LAST_NAME, SESSION_DATE ";
                    }
                    else
                    {
                        sql = "SELECT IP.PERSON_FIRST_NAME, IP.PERSON_MIDDLE_NAME, IP.PERSON_LAST_NAME, PT.SESSION_DATE, PT.PT_SESSION_ID "
                                + " FROM  "
                                + " (SELECT HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME, HP_IP.PERSON_LAST_NAME, HCA_IP.CUST_ACCOUNT_ID "
                                + "  FROM WW_APPT_RELATIONSHIPS WAR INNER JOIN HZ_CUST_ACCOUNTS HCA ON WAR.REL_CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID  "
                                + "       INNER JOIN HZ_CUST_ACCOUNTS HCA_IP ON WAR.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID "
                                + "       INNER JOIN HZ_PARTIES HP_IP ON HCA_IP.PARTY_ID = HP_IP.PARTY_ID "
                                + "  WHERE WAR.STATUS = 'A'  "
                                + "        AND HCA.PARTY_ID = " + iPartyID
                                + "        AND HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER') IP INNER JOIN       "
                                + " (SELECT PPS.SESSION_DATE, PPS.PT_SESSION_ID, PPS.IP_CUST_ACCOUNT_ID  "
                                + "  FROM WW_PC_PT_SESSIONS PPS INNER JOIN WW_PC_CLAIM_PT_DETAIL PCPD ON PPS.CLAIM_ID = PCPD.CLAIM_ID  "
                                + "       INNER JOIN WW_PC_PT_CENTERS PPC ON PCPD.PT_CENTER_ID = PPC.PT_CENTER_ID  "
                                + "       INNER JOIN WW_PC_CLAIM_HEADER PCH ON PPS.CLAIM_ID = PCH.CLAIM_ID  "
                                + "       INNER JOIN WW_PC_PATIENTS PP ON PCH.PATIENT_ID = PP.PATIENT_ID  "
                                + "   WHERE PPS.SESSION_DATE BETWEEN '" + Convert.ToDateTime(DayPilotNavigator1.VisibleStart).ToString("yyyy-MM-dd") + "' AND '" + Convert.ToDateTime(DayPilotNavigator1.VisibleEnd).ToString("yyyy-MM-dd") + "' "
                                + " ) PT ON IP.CUST_ACCOUNT_ID = PT.IP_CUST_ACCOUNT_ID "
                                + " GROUP BY PT_SESSION_ID ,PERSON_FIRST_NAME, PERSON_MIDDLE_NAME, PERSON_LAST_NAME, SESSION_DATE ";
                    }
                }
                else
                {
                    sql = "SELECT IP.PERSON_FIRST_NAME, IP.PERSON_MIDDLE_NAME, IP.PERSON_LAST_NAME, PT.SESSION_DATE, PT.PT_SESSION_ID "
                        + "FROM ( "
                        + "		SELECT HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME, HP_IP.PERSON_LAST_NAME, HCA_IP.CUST_ACCOUNT_ID  "
                        + "		FROM WW_APPT_RELATIONSHIPS WAR INNER JOIN HZ_CUST_ACCOUNTS HCA ON WAR.REL_CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID  "
                        + "			INNER JOIN HZ_CUST_ACCOUNTS HCA_IP ON WAR.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID  "
                        + "			INNER JOIN HZ_PARTIES HP_IP ON HCA_IP.PARTY_ID = HP_IP.PARTY_ID  "
                        + "		WHERE WAR.STATUS = 'A'  "
                        + "				AND HCA.PARTY_ID = 380786  "
                        + "				AND HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER') IP 	 "
                        + "	INNER JOIN (  "
                        + "		SELECT WPP.FIRST_NAME, WPP.LAST_NAME, WPPS.SESSION_DATE, WPPS.PT_SESSION_ID, WS.IP_CUST_ACCOUNT_ID "
                        + "FROM WW_SCHEDULE WS INNER JOIN WW_PC_PATIENTS WPP ON WS.IP_PARTY_ID = WPP.PARTY_ID "
                        + "    INNER JOIN WW_PC_CLAIM_HEADER WPCH ON WPP.PATIENT_ID = WPCH.PATIENT_ID "
                        + "     INNER JOIN WW_PC_CLAIM_PT_DETAIL WPCPD ON WPCH.CLAIM_ID =  WPCPD.CLAIM_ID "
                         + "    INNER JOIN WW_PC_PT_SESSIONS WPPS ON WPCPD.CLAIM_ID = WPPS.CLAIM_ID "
                        + "WHERE (WPCH.EMPLOYER_ID = 23403)   "
                        + "GROUP BY WPP.FIRST_NAME, WPP.LAST_NAME, WPPS.SESSION_DATE, WPPS.PT_SESSION_ID "
                        + "ORDER BY WPP.PATIENT_ID, WPCH.CLAIM_ID "
                        + "		 ) PT ON IP.CUST_ACCOUNT_ID = PT.IP_CUST_ACCOUNT_ID  "
                        + "GROUP BY PT_SESSION_ID ,PERSON_FIRST_NAME, PERSON_MIDDLE_NAME, PERSON_LAST_NAME, SESSION_DATE  ";
                }
                cmd = new OdbcCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                dr3 = cmd.ExecuteReader();

                while (dr3.Read())
                {
                    string ipName = "";

                    DataRow dr;

                    dr = dt.NewRow();
                    dr["id"] = Convert.ToString(myCStr(dr3.GetValue(4)));
                    dr["start"] = Convert.ToDateTime(myCStr(dr3.GetValue(3)).Replace(".", ""));
                    dr["end"] = Convert.ToDateTime(myCStr(dr3.GetValue(3)).Replace(".", "")).AddHours(1);
                    dr["allday"] = false;

                    ipName = "PT*" + myCStr(dr3.GetValue(0)).Substring(0, 1) + ". " + myCStr(dr3.GetValue(2));

                    dr["name"] = Convert.ToString(ipName);

                    dt.Rows.Add(dr);
                }
                cmd.Dispose();
                dr3.Close();
                cmd.Dispose();

            }*/

            if (schedTypeDrp.SelectedValue == "3" )
            {
                if (Convert.ToString(Session["InsCarrier"]) == "I" && check_ins_partner(iPartyID))
                {
                    sql = "SELECT "
                        + "	WS.APPT_ID, "
                        + "	WS.START_TIME, "
                        + "	WS.END_TIME, "
                        + "	PRIME_PHONE, "
                        + "	WAD.ATTRIBUTE11, "
                        + "	WAD.ATTRIBUTE7, "
                        + "	WAPD.PERSON_FIRST_NAME, "
                        + "	WAPD.PERSON_MIDDLE_NAME, "
                        + "	WAPD.PERSON_LAST_NAME, "
                        + "	WAD.ATTRIBUTE_CATEGORY, "
                        + "	WS.APPT_TYPE, "
                        + "	WS.SUBMITTED, "
                        + "	WS.ADDITIONAL_NOTES "
                        + "FROM "
                        + "	WW_SCHEDULE WS "
                        + "INNER JOIN WW_APPOINTMENT_DETAILS WAD ON WS.APPT_ID = WAD.APPT_ID "
                        + "INNER JOIN HZ_PARTIES WAPD ON WS.IP_PARTY_ID = WAPD.PARTY_ID "
                        + "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON WS.IP_CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                        + "INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID "
                        + "WHERE "
                        + "	WS. STATUS = 'A' "
                        + "AND HCARA. STATUS = 'A' "
                        + "AND HCARA.RELATED_CUST_ACCOUNT_ID = " + Session["CustAccountID"].ToString() + " "
                        + "AND WS.IP_CUST_ACCOUNT_ID IN (SELECT HCA_IP.CUST_ACCOUNT_ID "
                        + "								 FROM HZ_PARTIES HP_IP INNER JOIN HZ_CUST_ACCOUNTS HCA_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID "
                        + "									  INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_IP.CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                        + "									  INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID "
                        + "								 WHERE HCA_INS.PARTY_ID = " + iPartyID
                        + "									   AND HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' "
                        + "									   AND HCARA.STATUS = 'A' "
                        + "									   AND HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT')				 "
                        + "AND WS.IP_PARTY_ID <> 0 "
                        + "AND WS.ATTRIBUTE_CATEGORY = 'PATIENT' "
                        + "AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                        + "AND WS.APPT_TYPE = 'SUR' "
                        + "AND WS.START_TIME BETWEEN '" + start.ToString("yyyy-MM-dd") + "' AND '" + end.ToString("yyyy-MM-dd") + "' "
                        + "GROUP BY "
                        + "	WS.APPT_ID, "
                        + "	WS.START_TIME, "
                        + "	WS.END_TIME, "
                        + "	PRIME_PHONE, "
                        + "	WAD.ATTRIBUTE11, "
                        + "	WAD.ATTRIBUTE7, "
                        + "	WAPD.PERSON_FIRST_NAME, "
                        + "	WAPD.PERSON_MIDDLE_NAME, "
                        + "	WAPD.PERSON_LAST_NAME, "
                        + "	WAD.ATTRIBUTE_CATEGORY, "
                        + "	WS.APPT_TYPE, "
                        + "	WS.SUBMITTED, "
                        + "	WS.ADDITIONAL_NOTES "
                        + "ORDER BY "
                        + "	WS.APPT_ID DESC ";

                }
                else if (Convert.ToString(Session["InsCarrier"]) == "Y" && check_ins_partner(iPartyID))
                {
                    sql = "SELECT "
                        + "	WS.APPT_ID, "
                        + "	WS.START_TIME, "
                        + "	WS.END_TIME, "
                        + "	PRIME_PHONE, "
                        + "	WAD.ATTRIBUTE11, "
                        + "	WAD.ATTRIBUTE7, "
                        + "	WAPD.PERSON_FIRST_NAME, "
                        + "	WAPD.PERSON_MIDDLE_NAME, "
                        + "	WAPD.PERSON_LAST_NAME, "
                        + "	WAD.ATTRIBUTE_CATEGORY, "
                        + "	WS.APPT_TYPE, "
                        + "	WS.SUBMITTED, "
                        + "	WS.ADDITIONAL_NOTES "
                        + "FROM "
                        + "	WW_SCHEDULE WS "
                        + "INNER JOIN WW_APPOINTMENT_DETAILS WAD ON WS.APPT_ID = WAD.APPT_ID "
                        + "INNER JOIN HZ_PARTIES WAPD ON WS.IP_PARTY_ID = WAPD.PARTY_ID "
                        + "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON WS.IP_CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                        + "INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID "
                        + "WHERE "
                        + "	WS. STATUS = 'A' "
                        + "AND HCARA. STATUS = 'A' "
                        + "AND WS.IP_CUST_ACCOUNT_ID IN (SELECT HCA_IP.CUST_ACCOUNT_ID "
                        + "								 FROM HZ_PARTIES HP_IP INNER JOIN HZ_CUST_ACCOUNTS HCA_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID "
                        + "									  INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_IP.CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                        + "									  INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID "
                        + "								 WHERE HCA_INS.PARTY_ID = " + iPartyID
                        + "									   AND HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' "
                        + "									   AND HCARA.STATUS = 'A' "
                        + "									   AND HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT')				 "
                        + "AND WS.IP_PARTY_ID <> 0 "
                        + "AND WS.ATTRIBUTE_CATEGORY = 'PATIENT' "
                        + "AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                        + "AND WS.APPT_TYPE = 'SUR' "
                        + "AND WS.START_TIME BETWEEN '" + start.ToString("yyyy-MM-dd") + "' AND '" + end.ToString("yyyy-MM-dd") + "' "
                        + "GROUP BY "
                        + "	WS.APPT_ID, "
                        + "	WS.START_TIME, "
                        + "	WS.END_TIME, "
                        + "	PRIME_PHONE, "
                        + "	WAD.ATTRIBUTE11, "
                        + "	WAD.ATTRIBUTE7, "
                        + "	WAPD.PERSON_FIRST_NAME, "
                        + "	WAPD.PERSON_MIDDLE_NAME, "
                        + "	WAPD.PERSON_LAST_NAME, "
                        + "	WAD.ATTRIBUTE_CATEGORY, "
                        + "	WS.APPT_TYPE, "
                        + "	WS.SUBMITTED, "
                        + "	WS.ADDITIONAL_NOTES "
                        + "ORDER BY "
                        + "	WS.APPT_ID DESC ";
                }
                else
                {
                    sql = "SELECT WS.APPT_ID, WS.START_TIME, WS.END_TIME,PRIME_PHONE, WAD.ATTRIBUTE11,WAD.ATTRIBUTE7, WAPD.PERSON_FIRST_NAME,  "
                        + "		WAPD.PERSON_MIDDLE_NAME, WAPD.PERSON_LAST_NAME, WAD.ATTRIBUTE_CATEGORY, WS.APPT_TYPE, WS.SUBMITTED, WS.ADDITIONAL_NOTES   "
                        + "FROM WW_SCHEDULE WS INNER JOIN WW_APPOINTMENT_DETAILS WAD ON WS.APPT_ID = WAD.APPT_ID   "
                        + "	 INNER JOIN HZ_PARTIES WAPD ON WS.IP_PARTY_ID = WAPD.PARTY_ID   "
                        + "     INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON WS.IP_CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID  "
                        + "	 INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID  "
                        + "WHERE WS.STATUS = 'A'   "
                        + "		AND HCARA.STATUS = 'A'   "
                        + "		AND HCA_EMP.PARTY_ID = " + iPartyID
                        + "		AND WS.IP_PARTY_ID <> 0   "
                        + cust_account_sql_str + " "
                        + "		AND WS.ATTRIBUTE_CATEGORY = 'PATIENT'   "
                        + "		AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER'  "
                        + "		AND WS.APPT_TYPE =  'SUR' "
                        + "		AND WS.START_TIME BETWEEN '" + start.ToString("yyyy-MM-dd") + "' AND '" + end.ToString("yyyy-MM-dd") + "' "
                        + "GROUP BY WS.APPT_ID, WS.START_TIME, WS.END_TIME,PRIME_PHONE, WAD.ATTRIBUTE11,WAD.ATTRIBUTE7, WAPD.PERSON_FIRST_NAME, WAPD.PERSON_MIDDLE_NAME, WAPD.PERSON_LAST_NAME, WAD.ATTRIBUTE_CATEGORY, WS.APPT_TYPE, WS.SUBMITTED, WS.ADDITIONAL_NOTES ORDER BY WS.APPT_ID DESC  ";
                }


                cmd = new OdbcCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                dr3 = cmd.ExecuteReader();

                while (dr3.Read())
                {
                    string ipName = "";
                    string apptType = "";

                    DataRow dr;

                    dr = dt.NewRow();
                    dr["id"] = Convert.ToString(myCStr(dr3.GetValue(0)));
                    dr["start"] = Convert.ToDateTime(myCStr(dr3.GetValue(1)).Replace(".", ""));
                    dr["end"] = Convert.ToDateTime(myCStr(dr3.GetValue(2)).Replace(".", ""));
                    dr["allday"] = false;


                    ipName = myCStr(dr3.GetValue(6)).Substring(0, 1) + ". " + myCStr(dr3.GetValue(8));

                    if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(5))))
                    {
                        if (myCStr(dr3.GetValue(5)) != "0")
                        {
                            apptType = myCStr(dr3.GetValue(10)) + "/" + myCStr(dr3.GetValue(5));
                        }
                        else
                        {
                            apptType = myCStr(dr3.GetValue(10));
                        }
                    }
                    else
                    {
                        apptType = myCStr(dr3.GetValue(4));
                    }

                    
                    if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(9))))
                    {
                        dr["name"] = Convert.ToString(ipName);
                    }
                    else
                    {
                        dr["name"] = "New Appt";
                    }
                    dt.Rows.Add(dr);
                }
                cmd.Dispose();
                dr3.Close();
                cmd.Dispose();
            }

            if (schedTypeDrp.SelectedValue == "4" )
            {
                if (Convert.ToString(Session["InsCarrier"]) == "I" && check_ins_partner(iPartyID))
                {
                    sql = "SELECT "
                        + "	WS.APPT_ID, "
                        + "	WS.START_TIME, "
                        + "	WS.END_TIME, "
                        + "	PRIME_PHONE, "
                        + "	WAD.ATTRIBUTE11, "
                        + "	WAD.ATTRIBUTE7, "
                        + "	WAPD.PERSON_FIRST_NAME, "
                        + "	WAPD.PERSON_MIDDLE_NAME, "
                        + "	WAPD.PERSON_LAST_NAME, "
                        + "	WAD.ATTRIBUTE_CATEGORY, "
                        + "	WS.APPT_TYPE, "
                        + "	WS.SUBMITTED, "
                        + "	WS.ADDITIONAL_NOTES "
                        + "FROM "
                        + "	WW_SCHEDULE WS "
                        + "INNER JOIN WW_APPOINTMENT_DETAILS WAD ON WS.APPT_ID = WAD.APPT_ID "
                        + "INNER JOIN HZ_PARTIES WAPD ON WS.IP_PARTY_ID = WAPD.PARTY_ID "
                        + "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON WS.IP_CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                        + "INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID "
                        + "WHERE "
                        + "	WS. STATUS = 'A' "
                        + "AND HCARA. STATUS = 'A' "
                        + "AND HCARA.RELATED_CUST_ACCOUNT_ID = " + Session["CustAccountID"].ToString() + " "
                        + "AND WS.IP_CUST_ACCOUNT_ID IN (SELECT HCA_IP.CUST_ACCOUNT_ID "
                        + "								 FROM HZ_PARTIES HP_IP INNER JOIN HZ_CUST_ACCOUNTS HCA_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID "
                        + "									  INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_IP.CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                        + "									  INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID "
                        + "								 WHERE HCA_INS.PARTY_ID = " + iPartyID
                        + "									   AND HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' "
                        + "									   AND HCARA.STATUS = 'A' "
                        + "									   AND HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT')				 "
                        + "AND WS.IP_PARTY_ID <> 0 "
                        + "AND WS.ATTRIBUTE_CATEGORY = 'PATIENT' "
                        + "AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                        + "AND WS.APPT_TYPE = 'MRI' "
                        + "AND WS.START_TIME BETWEEN '" + start.ToString("yyyy-MM-dd") + "' AND '" + end.ToString("yyyy-MM-dd") + "' "
                        + "GROUP BY "
                        + "	WS.APPT_ID, "
                        + "	WS.START_TIME, "
                        + "	WS.END_TIME, "
                        + "	PRIME_PHONE, "
                        + "	WAD.ATTRIBUTE11, "
                        + "	WAD.ATTRIBUTE7, "
                        + "	WAPD.PERSON_FIRST_NAME, "
                        + "	WAPD.PERSON_MIDDLE_NAME, "
                        + "	WAPD.PERSON_LAST_NAME, "
                        + "	WAD.ATTRIBUTE_CATEGORY, "
                        + "	WS.APPT_TYPE, "
                        + "	WS.SUBMITTED, "
                        + "	WS.ADDITIONAL_NOTES "
                        + "ORDER BY "
                        + "	WS.APPT_ID DESC ";
                }
                else if (Convert.ToString(Session["InsCarrier"]) == "Y" && check_ins_partner(iPartyID))
                {
                    sql = "SELECT "
                        + "	WS.APPT_ID, "
                        + "	WS.START_TIME, "
                        + "	WS.END_TIME, "
                        + "	PRIME_PHONE, "
                        + "	WAD.ATTRIBUTE11, "
                        + "	WAD.ATTRIBUTE7, "
                        + "	WAPD.PERSON_FIRST_NAME, "
                        + "	WAPD.PERSON_MIDDLE_NAME, "
                        + "	WAPD.PERSON_LAST_NAME, "
                        + "	WAD.ATTRIBUTE_CATEGORY, "
                        + "	WS.APPT_TYPE, "
                        + "	WS.SUBMITTED, "
                        + "	WS.ADDITIONAL_NOTES "
                        + "FROM "
                        + "	WW_SCHEDULE WS "
                        + "INNER JOIN WW_APPOINTMENT_DETAILS WAD ON WS.APPT_ID = WAD.APPT_ID "
                        + "INNER JOIN HZ_PARTIES WAPD ON WS.IP_PARTY_ID = WAPD.PARTY_ID "
                        + "INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON WS.IP_CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                        + "INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID "
                        + "WHERE "
                        + "	WS. STATUS = 'A' "
                        + "AND HCARA. STATUS = 'A' "
                        + "AND WS.IP_CUST_ACCOUNT_ID IN (SELECT HCA_IP.CUST_ACCOUNT_ID "
                        + "								 FROM HZ_PARTIES HP_IP INNER JOIN HZ_CUST_ACCOUNTS HCA_IP ON HP_IP.PARTY_ID = HCA_IP.PARTY_ID "
                        + "									  INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON HCA_IP.CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID "
                        + "									  INNER JOIN HZ_CUST_ACCOUNTS HCA_INS ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_INS.CUST_ACCOUNT_ID "
                        + "								 WHERE HCA_INS.PARTY_ID = " + iPartyID
                        + "									   AND HCA_INS.ATTRIBUTE_CATEGORY = 'INSURANCE CARRIER' "
                        + "									   AND HCARA.STATUS = 'A' "
                        + "									   AND HCA_IP.ATTRIBUTE_CATEGORY = 'PATIENT')				 "
                        + "AND WS.IP_PARTY_ID <> 0 "
                        + "AND WS.ATTRIBUTE_CATEGORY = 'PATIENT' "
                        + "AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                        + "AND WS.APPT_TYPE = 'MRI' "
                        + "AND WS.START_TIME BETWEEN '" + start.ToString("yyyy-MM-dd") + "' AND '" + end.ToString("yyyy-MM-dd") + "' "
                        + "GROUP BY "
                        + "	WS.APPT_ID, "
                        + "	WS.START_TIME, "
                        + "	WS.END_TIME, "
                        + "	PRIME_PHONE, "
                        + "	WAD.ATTRIBUTE11, "
                        + "	WAD.ATTRIBUTE7, "
                        + "	WAPD.PERSON_FIRST_NAME, "
                        + "	WAPD.PERSON_MIDDLE_NAME, "
                        + "	WAPD.PERSON_LAST_NAME, "
                        + "	WAD.ATTRIBUTE_CATEGORY, "
                        + "	WS.APPT_TYPE, "
                        + "	WS.SUBMITTED, "
                        + "	WS.ADDITIONAL_NOTES "
                        + "ORDER BY "
                        + "	WS.APPT_ID DESC ";
                }
                else
                {
                    sql = "SELECT WS.APPT_ID, WS.START_TIME, WS.END_TIME,PRIME_PHONE, WAD.ATTRIBUTE11,WAD.ATTRIBUTE7, WAPD.PERSON_FIRST_NAME,  "
                        + "		WAPD.PERSON_MIDDLE_NAME, WAPD.PERSON_LAST_NAME, WAD.ATTRIBUTE_CATEGORY, WS.APPT_TYPE, WS.SUBMITTED, WS.ADDITIONAL_NOTES   "
                        + "FROM WW_SCHEDULE WS INNER JOIN WW_APPOINTMENT_DETAILS WAD ON WS.APPT_ID = WAD.APPT_ID   "
                        + "	 INNER JOIN HZ_PARTIES WAPD ON WS.IP_PARTY_ID = WAPD.PARTY_ID   "
                        + "     INNER JOIN HZ_CUST_ACCT_RELATE_ALL HCARA ON WS.IP_CUST_ACCOUNT_ID = HCARA.CUST_ACCOUNT_ID  "
                        + "	 INNER JOIN HZ_CUST_ACCOUNTS HCA_EMP ON HCARA.RELATED_CUST_ACCOUNT_ID = HCA_EMP.CUST_ACCOUNT_ID  "
                        + "WHERE WS.STATUS = 'A'   "
                        + "		AND HCARA.STATUS = 'A'   "
                        + "		AND HCA_EMP.PARTY_ID = " + iPartyID
                        + "		AND WS.IP_PARTY_ID <> 0   "
                        + cust_account_sql_str + " "
                        + "		AND WS.ATTRIBUTE_CATEGORY = 'PATIENT'   "
                        + "		AND HCA_EMP.ATTRIBUTE_CATEGORY = 'EMPLOYER'  "
                        + "		AND WS.APPT_TYPE =  'MRI' "
                        + "		AND WS.START_TIME BETWEEN '" + start.ToString("yyyy-MM-dd") + "' AND '" + end.ToString("yyyy-MM-dd") + "' "
                        + "GROUP BY WS.APPT_ID, WS.START_TIME, WS.END_TIME,PRIME_PHONE, WAD.ATTRIBUTE11,WAD.ATTRIBUTE7, WAPD.PERSON_FIRST_NAME, WAPD.PERSON_MIDDLE_NAME, WAPD.PERSON_LAST_NAME, WAD.ATTRIBUTE_CATEGORY, WS.APPT_TYPE, WS.SUBMITTED, WS.ADDITIONAL_NOTES ORDER BY WS.APPT_ID DESC  ";
                }

                cmd = new OdbcCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                dr3 = cmd.ExecuteReader();

                while (dr3.Read())
                {
                    string ipName = "";
                    string apptType = "";

                    DataRow dr;

                    dr = dt.NewRow();
                    dr["id"] = Convert.ToString(myCStr(dr3.GetValue(0)));
                    dr["start"] = Convert.ToDateTime(myCStr(dr3.GetValue(1)).Replace(".", ""));
                    dr["end"] = Convert.ToDateTime(myCStr(dr3.GetValue(2)).Replace(".", ""));
                    dr["allday"] = false;


                    ipName = myCStr(dr3.GetValue(6)).Substring(0, 1) + ". " + myCStr(dr3.GetValue(8));

                    if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(5))))
                    {
                        if (myCStr(dr3.GetValue(5)) != "0")
                        {
                            apptType = myCStr(dr3.GetValue(10)) + "/" + myCStr(dr3.GetValue(5));
                        }
                        else
                        {
                            apptType = myCStr(dr3.GetValue(10));
                        }
                    }
                    else
                    {
                        apptType = myCStr(dr3.GetValue(4));
                    }

                    if (!string.IsNullOrEmpty(myCStr(dr3.GetValue(9))))
                    {
                        dr["name"] = Convert.ToString(ipName);

                    }
                    else
                    {
                        dr["name"] = "New Appt";
                    }
                    dt.Rows.Add(dr);
                }
                cmd.Dispose();
                dr3.Close();
                cmd.Dispose();
            }

            dt.PrimaryKey = new DataColumn[] { dt.Columns["id"] };
        }
        catch (Exception ex)
        {
            Response.Write("Exception: " + Convert.ToString(ex));
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }



        return dt;

    }

    protected void getSchedType(object sender, EventArgs e)
    {
        switch (Convert.ToInt16(schedTypeDrp.SelectedValue))
        {

            case 0:
                initData();
                DayPilotMonth1.DataSource = getData(DayPilotMonth1.VisibleStart, DayPilotMonth1.VisibleEnd, (string)DayPilotMonth1.ClientState["filter"]);
                DayPilotNavigator1.DataSource = getData(DayPilotNavigator1.VisibleStart, DayPilotNavigator1.VisibleEnd, null);
                DataBind();
                break;

            case 1:
                initData();
                DayPilotMonth1.DataSource = getData(DayPilotMonth1.VisibleStart, DayPilotMonth1.VisibleEnd, (string)DayPilotMonth1.ClientState["filter"]);
                DayPilotNavigator1.DataSource = getData(DayPilotNavigator1.VisibleStart, DayPilotNavigator1.VisibleEnd, null);
                DataBind();
                break;

            case 2:
                initData();
                DayPilotMonth1.DataSource = getData(DayPilotMonth1.VisibleStart, DayPilotMonth1.VisibleEnd, (string)DayPilotMonth1.ClientState["filter"]);
                DayPilotNavigator1.DataSource = getData(DayPilotNavigator1.VisibleStart, DayPilotNavigator1.VisibleEnd, null);
                DataBind();
                break;


        }
    }

    protected void getApptDetails(Int32 ipApptID)
    {
        Int32 iPartyID = Convert.ToInt32(Session["PartyID"]);
        ip_PartyID.Value = Convert.ToString(iPartyID);
        ipSelectedApptInfoTP.HeaderText = "Selected Appointment";
        ipOtherApptsTP.HeaderText = "Related Appointments";
        ipPTApptsTP.HeaderText = "Physical Therapy Appointments";
        ipStatusReportsTP.HeaderText = "Status Reports";
        injuryDetailsTC.ActiveTabIndex = 0;
        Int32 custAcctID = 0;
        string sql = "";
        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        OdbcConnection conn = new OdbcConnection(strConnection);

        try
        {

            conn.Open();

            OdbcCommand cmd = new OdbcCommand(sql, conn);
            cmd.CommandType = CommandType.Text;
            OdbcDataReader dr3 = null;

            if (ipApptID < 0)
            {
                sql = "SELECT MIN(APPT_ID) "
                    + "FROM WW_SCHEDULE "
                    + "WHERE IP_CUST_ACCOUNT_ID = ( "
                    + " SELECT "
                    + " 		WS.IP_CUST_ACCOUNT_ID "
                    + " 	FROM "
                    + " 		WW_SCHEDULE WS "
                    + " 	INNER JOIN WW_PC_PATIENTS WPP ON WS.IP_PARTY_ID = WPP.PARTY_ID "
                    + " 	INNER JOIN WW_PC_CLAIM_HEADER WPCH ON WPP.PATIENT_ID = WPCH.PATIENT_ID "
                    + " 	INNER JOIN WW_PC_CLAIM_PT_DETAIL WPCPD ON WPCH.CLAIM_ID = WPCPD.CLAIM_ID "
                    + " 	INNER JOIN WW_PC_PT_SESSIONS WPPS ON WPCPD.CLAIM_ID = WPPS.CLAIM_ID "
                    + " 	WHERE "
                    + " 		WPPS.PT_SESSION_ID = " + ipApptID
                    + " 	GROUP BY "
                    + " 		WPP.FIRST_NAME, "
                    + " 		WPP.LAST_NAME, "
                    + " 		WPPS.SESSION_DATE, "
                    + " 		WPPS.PT_SESSION_ID )"
                    + "AND STATUS = 'A' ";

                cmd = new OdbcCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                dr3 = cmd.ExecuteReader();

                if (dr3.Read())
                {
                    ipApptID = Convert.ToInt32(dr3.GetValue(0));
                }

            }
            
            sql = "SELECT WAD.APPT_ID, WAD.ATTRIBUTE1, WAD.ATTRIBUTE2, WAD.ATTRIBUTE3, WAD.ATTRIBUTE4, WAD.ATTRIBUTE5, WAD.ATTRIBUTE6, WAD.ATTRIBUTE7, WAD.ATTRIBUTE8, WAD.ATTRIBUTE9, WAD.ATTRIBUTE10, "
                                + "    WAD.ATTRIBUTE11, WAD.ATTRIBUTE12, WAD.ATTRIBUTE13, WAD.ATTRIBUTE14, WAD.DOI, WAD.DOC, WAD.DATE_CLOSED, WAD.ATTRIBUTE_CATEGORY, WAD.ATTRIBUTE15, WAD.ATTRIBUTE16, WAD.DOA, "
                                + "    WAPD.PERSON_FIRST_NAME, WAPD.PERSON_MIDDLE_NAME, WAPD.PERSON_LAST_NAME, HPP.GENDER, WAPD.JGZZ_FISCAL_CODE, HPP.DATE_OF_BIRTH, WAPD.ADDRESS1, WAPD.ADDRESS2, "
                                + "    WAPD.CITY, WAPD.STATE, WAPD.POSTAL_CODE, WS.APPT_TYPE, WS.START_TIME, WS.IP_CUST_ACCOUNT_ID, WS.IP_PARTY_ID, WS.SCHEDULED, WAD.ATTRIBUTE17, WS.APPT_DATE, WCM.CM_NAME "
                                + "FROM WW_APPOINTMENT_DETAILS WAD  "
                                + "     INNER JOIN WW_SCHEDULE WS ON WAD.APPT_ID = WS.APPT_ID "
                                + " INNER JOIN HZ_PARTIES WAPD ON WS.IP_PARTY_ID = WAPD.PARTY_ID "
                                + " INNER JOIN HZ_PERSON_PROFILES HPP ON WAPD.PARTY_ID = HPP.PARTY_ID "
                                + "     LEFT JOIN CASE_MANAGERS WCM ON WAD.ATTRIBUTE10 = WCM.CM_INITIALS "
                                + "WHERE WS.APPT_ID = " + ipApptID;
            cmd = new OdbcCommand(sql, conn);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sql;

            dr3 = cmd.ExecuteReader();

            if (dr3.Read())
            {
                custAcctID = Convert.ToInt32(myCStr(dr3.GetValue(35)));
                ip_custAcctID.Value = Convert.ToString(custAcctID);

                ipFNameTxt.Text = myCStr(dr3.GetValue(22)); 
                ipMNameTxt.Text = myCStr(dr3.GetValue(23));
                ipLNameTxt.Text = myCStr(dr3.GetValue(24));
                if (myCStr(dr3.GetValue(18)) == "PATIENT")
                {

                    ipOccTxt.Text = myCStr(dr3.GetValue(3)).Replace(" 12:00:00 AM", "");
                    ipDOITxt.Text = myCStr(dr3.GetValue(15)).Replace(" 12:00:00 AM", "");
                    ipBodyTxt.Text = myCStr(dr3.GetValue(19));
                    ipDOCTxt.Text = myCStr(dr3.GetValue(16)).Replace(" 12:00:00 AM", "");
                    ipEmpInsTxt.Text = myCStr(dr3.GetValue(4));
                    ipCalledInByTxt.Text = myCStr(dr3.GetValue(5));
                    ipApptDateTxt.Text = myCStr(dr3.GetValue(39)).Replace(" 12:00:00 AM", "");
                    ipApptTimeTxt.Text = Convert.ToDateTime(myCStr(dr3.GetValue(34))).ToString("h:mm tt");

                    switch (myCStr(dr3.GetValue(9)))
                    {
                        case "CON":
                            conRow.Visible = true;
                            clinRow.Visible = false;
                            ipDoctorTxt.Text = myCStr(dr3.GetValue(6));
                            break;
                        default:
                            conRow.Visible = false;
                            clinRow.Visible = true;
                            ipWWDocNameTxt.Text = myCStr(dr3.GetValue(6));
                            break;
                    }


                    switch (myCStr(dr3.GetValue(38)))
                    {
                        case "DT":
                            ipPracticeTxt.Text = "WORKWELL PHYSICIANS PC - AGH CLINIC";
                            break;
                        case "AP":
                            ipPracticeTxt.Text = "WORKWELL PHYSICIANS PC - AIRPORT CLINIC";
                            break;
                        case "CB":
                            ipPracticeTxt.Text = "WORKWELL PHYSICIANS PC - CRANBERRY CLINIC";
                            break;
                        case "MV":
                            ipPracticeTxt.Text = "WORKWELL PHYSICIANS PC - MONROEVILLE CLINIC";
                            break;
                        case "SH":
                            ipPracticeTxt.Text = "WORKWELL PHYSICIANS PC - SOUTH HILLS CLINIC";
                            break;
                        case "CON":
                            ipPracticeTxt.Text = myCStr(dr3.GetValue(7));
                            break;

                    }


                    ipDocPhoneFaxTxt.Text = myCStr(dr3.GetValue(20));
                    ipUDSTxt.Text = myCStr(dr3.GetValue(8));

                    ipCMNameTxt.Text = myCStr(dr3.GetValue(40));
                    ipERTxt.Text = myCStr(dr3.GetValue(12));

                    if (myCStr(dr3.GetValue(13)) == "O")
                    {
                        ipOpenClosedTxt.Text = "OPEN";
                    }
                    else
                    {
                        ipOpenClosedTxt.Text = "CLOSED";
                    }

                    ipDateClsdTxt.Text = myCStr(dr3.GetValue(17)).Replace(" 12:00:00 AM", "");//ConvertDate(myCStr(dr3.GetValue(30)).Replace(" 00:00:00", ""));

                }

                string employerName = "";
                string insuranceName = "";


                // check ww_appt_relationships

                sql = "SELECT WAR.REL_CUST_ACCOUNT_ID, HP.PARTY_NAME, HP.PARTY_ID, HCA.ACCOUNT_NAME, HCA.ATTRIBUTE_CATEGORY, HCA.ACCOUNT_NUMBER "
                        + "FROM WW_APPT_RELATIONSHIPS WAR INNER JOIN HZ_CUST_ACCOUNTS HCA ON WAR.REL_CUST_ACCOUNT_ID =  HCA.CUST_ACCOUNT_ID "
                        + "     INNER JOIN HZ_PARTIES HP ON HCA.PARTY_ID = HP.PARTY_ID "
                        + "WHERE WAR.APPT_ID = " + ipApptID + " AND "
                        + "      WAR.STATUS = 'A' ";

                cmd = new OdbcCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sql;

                dr3 = cmd.ExecuteReader();

                while (dr3.Read())
                {

                    if (myCStr(dr3.GetValue(4)) == "INSURANCE CARRIER")
                    {
                        insuranceName = myCStr(dr3.GetValue(3));
                    }
                    else
                    {
                        employerName = myCStr(dr3.GetValue(3));
                    }
                }

                cmd.Dispose();
                dr3.Close();


                if (!string.IsNullOrEmpty(employerName))
                {
                    ipEmpInsTxt.Text = employerName;
                }

                if (!string.IsNullOrEmpty(employerName) && !string.IsNullOrEmpty(insuranceName))
                {
                    ipEmpInsTxt.Text += " / " + insuranceName;
                }
                else if (!string.IsNullOrEmpty(insuranceName))
                {
                    ipEmpInsTxt.Text = insuranceName;
                }

            }

            sql = "SELECT WS_REL.APPT_ID, WS_REL.START_TIME, WS_REL.APPT_TYPE, WS_REL.CLINIC "
                        + "FROM WW_SCHEDULE WS INNER JOIN WW_SCHEDULE WS_REL ON WS.IP_CUST_ACCOUNT_ID = WS_REL.IP_CUST_ACCOUNT_ID "
                        + "WHERE WS.APPT_ID = " + ipApptID + " AND "
                        + "      WS_REL.APPT_ID <> " + ipApptID + " AND "
                        + "      WS_REL.STATUS = 'A' AND "
                        + "      WS_REL.IP_CUST_ACCOUNT_ID <> 0 AND "
                        + "      WS.IP_CUST_ACCOUNT_ID <> 0 "
                        + "ORDER BY WS_REL.START_TIME DESC";

            // THE REASON FOR <> 0 IS BECAUSE 0 MEANS THAT AN INJURY HASN'T BEEN CREATED/ATTACHED TO THE APPOINTMENT YET
            //  IF CHRIS EVENTUALLY WANTS TO SHOW ALL INJURIES FOR THE PATIENT TO THE EMPLOYER, I CAN USE THE PATIENT'S PARTY_ID TO LINK THEM AND THEN BREAK THEM DOWN BY THE CUST_ACCOUNT_IDs


            DataTable dt = new DataTable();

            cmd = new OdbcCommand(sql, conn);
            cmd.CommandType = CommandType.Text;

            OdbcDataAdapter ora1 = new OdbcDataAdapter();
            ora1.SelectCommand = cmd;
            ora1.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                relatedApptsGrd.DataSource = dt;
                relatedApptsGrd.DataBind();
                relatedApptsGrd.Visible = true;
                noOtherApptsTbl.Visible = false;
            }
            else
            {
                relatedApptsGrd.Visible = false;
                noOtherApptsTbl.Visible = true;
            }

            cmd.Dispose();
            ora1.Dispose();

            dt = new DataTable();

            sql = "SELECT STATUSREPORTID, IPFIRSTNAME, IPMIDDLENAME, IPLASTNAME, IPDOE, IPDOI FROM STATUS_REPORTS WHERE IPCUSTACCTID = " + custAcctID + " ORDER BY IPDOE DESC";

            OdbcDataAdapter objDataAdapter = new OdbcDataAdapter(sql, conn);
            try
            {
                objDataAdapter.Fill(dt);

                ciStatusRptViewGrd.DataSource = dt;
                ciStatusRptViewGrd.PageIndex = 0;
                ciStatusRptViewGrd.DataBind();

                if (dt.Rows.Count > 0)
                {
                    ciStatusRptViewGrd.Visible = true;
                    srNoViewSearchResultsTbl.Visible = false;
                }
                else
                {
                    ciStatusRptViewGrd.Visible = false;
                    srNoViewSearchResultsTbl.Visible = true;
                }

            }
            catch (Exception ex)
            {
                Response.Write("Exception: " + Convert.ToString(ex));
            }
            finally
            {
                objDataAdapter.Dispose();
            }

            dt = new DataTable();

            if (Convert.ToInt16(Session["UserID"]) != 28)
            {
                sql = "SELECT IP.PERSON_FIRST_NAME, IP.PERSON_MIDDLE_NAME, IP.PERSON_LAST_NAME, PT.SESSION_DATE, PT.PT_SESSION_ID , PT.PT_CENTER_NAME , PT.ATTENDED  "
                        + "FROM  "
                        + "(SELECT HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME, HP_IP.PERSON_LAST_NAME, HCA_IP.CUST_ACCOUNT_ID  "
                        + " FROM WW_APPT_RELATIONSHIPS WAR INNER JOIN HZ_CUST_ACCOUNTS HCA ON WAR.REL_CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID  "
                        + "      INNER JOIN HZ_CUST_ACCOUNTS HCA_IP ON WAR.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID  "
                        + "      INNER JOIN HZ_PARTIES HP_IP ON HCA_IP.PARTY_ID = HP_IP.PARTY_ID  "
                        + " WHERE WAR.STATUS = 'A'  "
                        + "	AND HCA.PARTY_ID = " + iPartyID
                        + "	AND HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                        + "	AND HCA_IP.CUST_ACCOUNT_ID = " + custAcctID
                        + ") IP INNER JOIN  "
                        + "(SELECT PPS.SESSION_DATE, PPS.PT_SESSION_ID, PPS.IP_CUST_ACCOUNT_ID, PPC.PT_CENTER_NAME, PPS.ATTENDED  "
                        + " FROM WW_PC_PT_SESSIONS PPS INNER JOIN WW_PC_CLAIM_PT_DETAIL PCPD ON PPS.CLAIM_ID = PCPD.CLAIM_ID  "
                        + "       INNER JOIN WW_PC_PT_CENTERS PPC ON PCPD.PT_CENTER_ID = PPC.PT_CENTER_ID  "
                        + "       INNER JOIN WW_PC_CLAIM_HEADER PCH ON PPS.CLAIM_ID = PCH.CLAIM_ID  "
                        + "       INNER JOIN WW_PC_PATIENTS PP ON PCH.PATIENT_ID = PP.PATIENT_ID  "
                        + "WHERE PPS.IP_CUST_ACCOUNT_ID =  " + custAcctID
                        + " ) PT ON IP.CUST_ACCOUNT_ID = PT.IP_CUST_ACCOUNT_ID  "
                        + "GROUP BY PT_SESSION_ID ,PERSON_FIRST_NAME, PERSON_MIDDLE_NAME, PERSON_LAST_NAME, SESSION_DATE  "
                        + "ORDER BY SESSION_DATE DESC";
            }
            else
            {
                sql = "SELECT "
                    + "	IP.PERSON_FIRST_NAME, "
                    + "	IP.PERSON_MIDDLE_NAME, "
                    + "	IP.PERSON_LAST_NAME, "
                    + "	PT.SESSION_DATE, "
                    + "	PT.PT_SESSION_ID, "
                    + "	PT.PT_CENTER_NAME, "
                    + "	PT.ATTENDED "
                    + "FROM "
                    + "	( "
                    + "		SELECT "
                    + "			HP_IP.PERSON_FIRST_NAME, "
                    + "			HP_IP.PERSON_MIDDLE_NAME, "
                    + "			HP_IP.PERSON_LAST_NAME, "
                    + "			HCA_IP.CUST_ACCOUNT_ID "
                    + "		FROM "
                    + "			WW_APPT_RELATIONSHIPS WAR "
                    + "		INNER JOIN HZ_CUST_ACCOUNTS HCA ON WAR.REL_CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID "
                    + "		INNER JOIN HZ_CUST_ACCOUNTS HCA_IP ON WAR.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID "
                    + "		INNER JOIN HZ_PARTIES HP_IP ON HCA_IP.PARTY_ID = HP_IP.PARTY_ID "
                    + "		WHERE "
                    + "			WAR. STATUS = 'A' "
                    + "		AND HCA.PARTY_ID = 380786 "
                    + "		AND HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                    + "		AND HCA_IP.CUST_ACCOUNT_ID = " + custAcctID
                    + "	)IP "
                    + "INNER JOIN( "
                    + "	SELECT "
                    + "		WPP.FIRST_NAME, "
                    + "		WPP.LAST_NAME, "
                    + "		WPPS.SESSION_DATE, "
                    + "		WPPS.PT_SESSION_ID, "
                    + "		WS.IP_CUST_ACCOUNT_ID, "
                    + "		WPPC.PT_CENTER_NAME, "
                    + "		WPPS.ATTENDED "
                    + "	FROM "
                    + "		WW_SCHEDULE WS "
                    + "	INNER JOIN WW_PC_PATIENTS WPP ON WS.IP_PARTY_ID = WPP.PARTY_ID "
                    + "	INNER JOIN WW_PC_CLAIM_HEADER WPCH ON WPP.PATIENT_ID = WPCH.PATIENT_ID "
                    + "	INNER JOIN WW_PC_CLAIM_PT_DETAIL WPCPD ON WPCH.CLAIM_ID = WPCPD.CLAIM_ID "
                    + "	INNER JOIN WW_PC_PT_SESSIONS WPPS ON WPCPD.CLAIM_ID = WPPS.CLAIM_ID "
                    + "	INNER JOIN WW_PC_PT_CENTERS WPPC ON WPCPD.PT_CENTER_ID = WPPC.PT_CENTER_ID "
                    + "	WHERE "
                    + "		WPCH.EMPLOYER_ID = 23403 "
                    + "		AND WS.IP_CUST_ACCOUNT_ID = " + custAcctID
                    + "	GROUP BY "
                    + "		WPP.FIRST_NAME, "
                    + "		WPP.LAST_NAME, "
                    + "		WPPS.SESSION_DATE, "
                    + "		WPPS.PT_SESSION_ID "
                    + "	ORDER BY "
                    + "		WPP.PATIENT_ID, "
                    + "		WPCH.CLAIM_ID "
                    + ")PT ON IP.CUST_ACCOUNT_ID = PT.IP_CUST_ACCOUNT_ID "
                    + "GROUP BY "
                    + "	PT_SESSION_ID, "
                    + "	PERSON_FIRST_NAME, "
                    + "	PERSON_MIDDLE_NAME, "
                    + "	PERSON_LAST_NAME, "
                    + "	SESSION_DATE "
                    + "ORDER BY SESSION_DATE, PT_CENTER_NAME ";
            }

            objDataAdapter = new OdbcDataAdapter(sql, conn);

            try
            {
                objDataAdapter.Fill(dt);

                ptApptsGrd.DataSource = dt;
                ptApptsGrd.PageIndex = 0;
                ptApptsGrd.DataBind();

                if (dt.Rows.Count > 0)
                {
                    ptApptsGrd.Visible = true;
                    srNoPTApptTbl.Visible = false;
                }
                else
                {
                    ptApptsGrd.Visible = false;
                    srNoPTApptTbl.Visible = true;
                }

            }
            catch (Exception ex)
            {
                Response.Write("Exception: " + Convert.ToString(ex));
            }
            finally
            {
                objDataAdapter.Dispose();
            }

        }
        catch (Exception ex)
        {
            Response.Write("Exception: " + Convert.ToString(ex));
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }
    }

    protected void returnToCal(object sender, EventArgs e)
    {
        monthlySchedulePanel.Visible = true;
        viewCasePanel.Visible = false;
    }

    protected void relatedApptsGrd_RowDataBound(Object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string startTime = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "START_TIME"));
            int loc = Convert.ToInt16(DataBinder.Eval(e.Row.DataItem, "CLINIC"));
            string location = "";

            DateTime mysTime = Convert.ToDateTime(startTime.Replace("T", " "));
            DateTime myeTime = Convert.ToDateTime(startTime.Replace("T", " "));

            switch (loc)
            {
                case 1:
                    location = "WORKWELL PHYSICIANS, PC - ALLEGHENY GENERAL HOSPITAL";
                    //ipDocPhoneFaxTxt.Text = "(412)321-3051 / (412)321-2084";
                    break;
                case 2:
                    location = "WORKWELL PHYSICIANS, PC - AIRPORT";
                    //ipDocPhoneFaxTxt.Text = "(412)262-1610 / (412)262-8883";
                    break;
                case 3:
                    location = "WORKWELL PHYSICIANS, PC - CRANBERRY";
                    //ipDocPhoneFaxTxt.Text = "(724)779-3970 / (724)779-3988";
                    break;
                case 4:
                    location = "WORKWELL PHYSICIANS, PC - MONROEVILLE";
                    //ipDocPhoneFaxTxt.Text = "(412)373-8430 / (412)373-8445";
                    break;
                case 5:
                    location = "WORKWELL PHYSICIANS, PC - SOUTH HILLS";
                    //ipDocPhoneFaxTxt.Text = "(412)833-2403 / (412)835-4365";
                    break;
                default:
                    location = "CONSULTING PHYSICIAN'S OFFICE";
                    //ipDocPhoneFaxTxt.Text = "(412)833-2403 / (412)835-4365";
                    break;
            }

            e.Row.Cells[0].Text = mysTime.ToString("MM/dd/yyyy");
            e.Row.Cells[1].Text = mysTime.ToString("h:mm tt");
            e.Row.Cells[2].Text = location;


        }
    }

    protected void relatedApptsGrd_RowCommand(Object sender, GridViewCommandEventArgs e)
    {

        if (string.Compare(e.CommandName, "Page") != 0)
        {


            int ipApptID = Convert.ToInt32(e.CommandArgument);


            OdbcCommand objSqlCmd = new OdbcCommand();
            string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
            OdbcConnection conn = new OdbcConnection(strConnection);


            try
            {
                conn.Open();

                string sql = "SELECT APPT_ID, START_TIME, END_TIME FROM WW_SCHEDULE WHERE APPT_ID = " + ipApptID;



                OdbcCommand cmd = new OdbcCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                OdbcDataReader dr3 = cmd.ExecuteReader();

                if (dr3.Read())
                {
                    getApptDetails(Convert.ToInt32(myCStr(dr3.GetValue(0))));
                }
                else
                {
                    Dispose();
                }
            }

            catch (Exception ex)
            {
                Response.Write("Can't load Web page: " + ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }

    }

    void ciStatusRptViewGrd_RowDataBound(Object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string sFName = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IPFIRSTNAME"));
            string sMName = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IPMIDDLENAME"));
            string sLName = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IPLASTNAME"));
            string name = "";

            if (string.IsNullOrEmpty(sMName) == false)
            {
                sMName = sMName.Substring(0, 1) + ".";
                name = sFName + " " + sMName + " " + sLName;
            }
            else
            {
                name = sFName + " " + sLName;
            }

            e.Row.Cells[0].Text = name;

        }

    }

    protected void ciStatusRptViewGrd_RowCommand(Object sender, GridViewCommandEventArgs e)
    {


        Int32 status_rpt_id = Convert.ToInt32(e.CommandArgument);

        if (string.Compare(e.CommandName, "Page") != 0)
        {
            string popupScript = "<script language=javascript> window.open('ei_ViewSR.aspx?sr_=" + status_rpt_id + "') </script>";
            ClientScript.RegisterStartupScript(this.GetType(), "callpopup", popupScript);
        }
    }

    protected void SubmitSearchBtn_Click(Object sender, EventArgs e)
    {

        patientSrchPanel.Visible = false;
        Int32 iPartyID = Convert.ToInt32(Session["PartyID"]);

        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        OdbcConnection conn = new OdbcConnection(strConnection);

        string socialSecNum = "";
        string ssnQuery = "";
        bool bSSEntered = false;

        try
        {

            conn.Open();

            DataSet ds = null;

            OdbcDataAdapter dsCmd = default(OdbcDataAdapter);

            if (!string.IsNullOrEmpty(ipSrchSS1SrchTxt.Text))
            {
                socialSecNum = ipSrchSS1SrchTxt.Text.Trim();
            }
            else
            {
                socialSecNum = "%";
                bSSEntered = false;
            }

            if (!string.IsNullOrEmpty(ipSrchSS2SrchTxt.Text))
            {
                socialSecNum = socialSecNum + ipSrchSS2SrchTxt.Text.Trim();
            }
            else
            {
                socialSecNum = socialSecNum + "%";
                bSSEntered = false;
            }

            if (!string.IsNullOrEmpty(ipSrchSS3SrchTxt.Text))
            {
                socialSecNum = socialSecNum + ipSrchSS3SrchTxt.Text.Trim();

            }
            else
            {
                socialSecNum = socialSecNum + "%";
                bSSEntered = false;
            }

            if (bSSEntered)
            {
                ssnQuery = " AND REPLACE(WAPD.JGZZ_FISCAL_CODE,'-','') = '" + socialSecNum + "' ";
            }
            else
            {
                ssnQuery = " AND REPLACE(WAPD.JGZZ_FISCAL_CODE,'-','') LIKE '" + socialSecNum + "' ";
            }

            string sql = "SELECT WS.APPT_ID, WAPD.PERSON_FIRST_NAME, WAPD.PERSON_MIDDLE_NAME, WAPD.PERSON_LAST_NAME, WAPD.JGZZ_FISCAL_CODE, WS.APPT_TYPE, WS.START_TIME, WS.END_TIME, WS.CLINIC, HCA.ACCOUNT_NAME "
                       + "FROM WW_SCHEDULE WS INNER JOIN HZ_PARTIES WAPD ON WS.IP_PARTY_ID = WAPD.PARTY_ID "
                       + "        INNER JOIN WW_APPT_RELATIONSHIPS WAR ON WS.APPT_ID = WAR.APPT_ID "
                       + "        INNER JOIN HZ_CUST_ACCOUNTS HCA ON WAR.REL_CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID "
                       + "WHERE WS.STATUS = 'A' "
                       + "          AND HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                       + "          AND HCA.PARTY_ID = " + iPartyID
                       + "          AND WAPD.PERSON_FIRST_NAME LIKE '%" + ipSrchFNameTxt.Text.ToUpper().Trim() + "%' "
                       + "          AND WAPD.PERSON_MIDDLE_NAME LIKE '%" + ipSrchMNameTxt.Text.ToUpper().Trim() + "%' "
                       + "          AND WAPD.PERSON_LAST_NAME LIKE '%" + ipSrchLNameTxt.Text.ToUpper().Trim() + "%' " + ssnQuery + " "
                       + "          AND WS.APPT_TYPE <> 'A' "
                       + "ORDER BY WS.START_TIME DESC ";

            dsCmd = dsCmd = new OdbcDataAdapter(sql, conn);
            ds = new DataSet();
            dsCmd.Fill(ds, "myQuery");

            if (ds.Tables[0].Rows.Count > 0)
            {
                patientApptSrchRsltsGrd.DataSource = ds;
                patientApptSrchRsltsGrd.PageIndex = 0;
                patientApptSrchRsltsGrd.DataBind();
                patientApptSrchRsltsGrd.Visible = true;
                noPatientSrchRsltsTbl.Visible = false;

            }
            else
            {
                noPatientSrchRsltsTbl.Visible = true;
                patientApptSrchRsltsGrd.Visible = false;
            }

            dsCmd.Dispose();
            ds.Dispose();
        }
        catch (Exception ex)
        {
            Response.Write("Exception: " + Convert.ToString(ex));
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }

        patientSrchRsltsPanel.Visible = true;

    }

    protected void CancelSearchBtn_Click(Object sender, EventArgs e)
    {
        monthlySchedulePanel.Visible = true;
        patientSrchPanel.Visible = false;
    }

    protected void NewSearchRstlsBtn_Click(Object sender, EventArgs e)
    {
        ipSrchFNameTxt.Text = "";
        ipSrchMNameTxt.Text = "";
        ipSrchLNameTxt.Text = "";
        ipSrchSS1SrchTxt.Text = "";
        ipSrchSS2SrchTxt.Text = "";
        ipSrchSS3SrchTxt.Text = "";

        monthlySchedulePanel.Visible = false;
        patientSrchRsltsPanel.Visible = false;
        patientSrchPanel.Visible = true;
    }

    protected void CancelSearchRsltsBtn_Click(Object sender, EventArgs e)
    {
        monthlySchedulePanel.Visible = true;
        patientSrchPanel.Visible = false;
        patientSrchRsltsPanel.Visible = false;
    }

    void patientApptSrchRsltsGrd_RowDataBound(Object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string appt_id = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "APPT_ID"));
            string sSSNumber = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "JGZZ_FISCAL_CODE"));
            string sFName = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "PERSON_FIRST_NAME"));
            string sLName = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "PERSON_LAST_NAME"));
            string sMName = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "PERSON_MIDDLE_NAME"));
            string sClinic = getClinic(Convert.ToInt16(DataBinder.Eval(e.Row.DataItem, "CLINIC")));
            string sAppt_Time = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "START_TIME")).Replace(":00 ", " ");

            string start_time = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "START_TIME")).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
            string end_time = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "END_TIME")).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");

            if (!string.IsNullOrEmpty(sMName.Trim()))
            {
                if (sMName.Trim().Length == 1)
                {
                    sMName = sMName + ".";
                }
            }

            if (string.IsNullOrEmpty(sSSNumber))
            {
                sSSNumber = "NOT GIVEN";
            }
            else
            {
                sSSNumber = "XXX-XX-9821";  //**************************************  need to change this so it really works  **************************************
            }

            e.Row.Cells[0].Text = sLName + ", " + sFName + " " + sMName;
            e.Row.Cells[1].Text = sSSNumber;
            e.Row.Cells[3].Text = sAppt_Time;
            e.Row.Cells[4].Text = sClinic;

        }

    }

    protected string getClinic(int clinic)
    {
        string rtnVal = "";

        switch (clinic)
        {
            case 1:
                rtnVal = "DT";
                break;
            case 2:
                rtnVal = "AP";
                break;
            case 3:
                rtnVal = "CB";
                break;
            case 4:
                rtnVal = "MV";
                break;
            case 5:
                rtnVal = "SH";
                break;
            case 0:
                rtnVal = "CON";
                break;

        }

        return rtnVal;
    }

    protected void ipSrchSubmitBtn_Click(Object sender, EventArgs e)
    {

        monthlySchedulePanel.Visible = false;
        patientSrchPanel.Visible = true;

        ipSrchFNameTxt.Text = "";
        ipSrchMNameTxt.Text = "";
        ipSrchLNameTxt.Text = "";
        ipSrchSS1SrchTxt.Text = "";
        ipSrchSS2SrchTxt.Text = "";
        ipSrchSS3SrchTxt.Text = "";


    }

    protected void patientApptSrchRsltsGrd_RowCommand(Object sender, GridViewCommandEventArgs e)
    {


        Int32 appt_id = Convert.ToInt32(e.CommandArgument);


        if (string.Compare(e.CommandName, "Page") != 0)
        {
            monthlySchedulePanel.Visible = false;
            viewCasePanel.Visible = true;
            patientSrchRsltsPanel.Visible = false;

            getApptDetails(appt_id);

        }

    }

    protected void ptApptsGrd_RowDataBound(Object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string sessionDate = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "SESSION_DATE")).Replace("12:00:00 AM", "");
            string attended = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ATTENDED"));

            switch (attended)
            {
                case "A":
                    attended = "ATTENDED";
                    break;
                case "N":
                    attended = "NOT SCHEDULED";
                    break;
                case "C":
                    attended = "CANCELED";
                    break;
                case "X":
                    attended = "NO SHOW";
                    break;
            }

            e.Row.Cells[1].Text = sessionDate;
            e.Row.Cells[2].Text = attended;


        }
    }

    protected void ptApptsGrd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        OdbcConnection conn = new OdbcConnection(strConnection);

        try
        {

            conn.Open();

            DataTable dt = new DataTable();

            string sql = "SELECT IP.PERSON_FIRST_NAME, IP.PERSON_MIDDLE_NAME, IP.PERSON_LAST_NAME, PT.SESSION_DATE, PT.PT_SESSION_ID , PT.PT_CENTER_NAME , PT.ATTENDED  "
                        + "FROM  "
                        + "(SELECT HP_IP.PERSON_FIRST_NAME, HP_IP.PERSON_MIDDLE_NAME, HP_IP.PERSON_LAST_NAME, HCA_IP.CUST_ACCOUNT_ID  "
                        + " FROM WW_APPT_RELATIONSHIPS WAR INNER JOIN HZ_CUST_ACCOUNTS HCA ON WAR.REL_CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID  "
                        + "      INNER JOIN HZ_CUST_ACCOUNTS HCA_IP ON WAR.CUST_ACCOUNT_ID = HCA_IP.CUST_ACCOUNT_ID  "
                        + "      INNER JOIN HZ_PARTIES HP_IP ON HCA_IP.PARTY_ID = HP_IP.PARTY_ID  "
                        + " WHERE WAR.STATUS = 'A'  "
                        + "	AND HCA.PARTY_ID = " + ip_PartyID.Value
                        + "	AND HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                        + "	AND HCA_IP.CUST_ACCOUNT_ID = " + ip_custAcctID.Value
                        + ") IP INNER JOIN  "
                        + "(SELECT PPS.SESSION_DATE, PPS.PT_SESSION_ID, PPS.IP_CUST_ACCOUNT_ID, PPC.PT_CENTER_NAME, PPS.ATTENDED  "
                        + " FROM WW_PC_PT_SESSIONS PPS INNER JOIN WW_PC_CLAIM_PT_DETAIL PCPD ON PPS.CLAIM_ID = PCPD.CLAIM_ID  "
                        + "       INNER JOIN WW_PC_PT_CENTERS PPC ON PCPD.PT_CENTER_ID = PPC.PT_CENTER_ID  "
                        + "       INNER JOIN WW_PC_CLAIM_HEADER PCH ON PPS.CLAIM_ID = PCH.CLAIM_ID  "
                        + "       INNER JOIN WW_PC_PATIENTS PP ON PCH.PATIENT_ID = PP.PATIENT_ID  "
                        + "WHERE PPS.IP_CUST_ACCOUNT_ID =  " + ip_custAcctID.Value
                        + " ) PT ON IP.CUST_ACCOUNT_ID = PT.IP_CUST_ACCOUNT_ID  "
                        + "GROUP BY PT_SESSION_ID ,PERSON_FIRST_NAME, PERSON_MIDDLE_NAME, PERSON_LAST_NAME, SESSION_DATE  "
                        + "ORDER BY SESSION_DATE DESC";

            OdbcDataAdapter objDataAdapter = new OdbcDataAdapter(sql, conn);

            try
            {
                objDataAdapter.Fill(dt);

                ptApptsGrd.DataSource = dt;
                ptApptsGrd.PageIndex = e.NewPageIndex;
                ptApptsGrd.DataBind();

                if (dt.Rows.Count > 0)
                {
                    ptApptsGrd.Visible = true;
                    srNoPTApptTbl.Visible = false;
                }
                else
                {
                    ptApptsGrd.Visible = false;
                    srNoPTApptTbl.Visible = true;
                }

            }
            catch (Exception ex)
            {
                Response.Write("Exception: " + Convert.ToString(ex));
            }
            finally
            {
                objDataAdapter.Dispose();
            }
        }
        catch (Exception ex)
        {
            Response.Write("Exception: " + Convert.ToString(ex));
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }
    }

    protected void patientApptSrchRsltsGrd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Int32 iPartyID = Convert.ToInt32(Session["PartyID"]);
        
        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        OdbcConnection conn = new OdbcConnection(strConnection);

        string socialSecNum = "";
        string ssnQuery = "";
        bool bSSEntered = false;

        try
        {

            conn.Open();

            DataSet ds = null;

            OdbcDataAdapter dsCmd = default(OdbcDataAdapter);

            if (!string.IsNullOrEmpty(ipSrchSS1SrchTxt.Text))
            {
                socialSecNum = ipSrchSS1SrchTxt.Text.Trim();
            }
            else
            {
                socialSecNum = "%";
                bSSEntered = false;
            }

            if (!string.IsNullOrEmpty(ipSrchSS2SrchTxt.Text))
            {
                socialSecNum = socialSecNum + ipSrchSS2SrchTxt.Text.Trim();
            }
            else
            {
                socialSecNum = socialSecNum + "%";
                bSSEntered = false;
            }

            if (!string.IsNullOrEmpty(ipSrchSS3SrchTxt.Text))
            {
                socialSecNum = socialSecNum + ipSrchSS3SrchTxt.Text.Trim();

            }
            else
            {
                socialSecNum = socialSecNum + "%";
                bSSEntered = false;
            }

            if (bSSEntered)
            {
                ssnQuery = " AND REPLACE(WAPD.JGZZ_FISCAL_CODE,'-','') = '" + socialSecNum + "' ";
            }
            else
            {
                ssnQuery = " AND REPLACE(WAPD.JGZZ_FISCAL_CODE,'-','') LIKE '" + socialSecNum + "' ";
            }

            string sql = "SELECT WS.APPT_ID, WAPD.PERSON_FIRST_NAME, WAPD.PERSON_MIDDLE_NAME, WAPD.PERSON_LAST_NAME, WAPD.JGZZ_FISCAL_CODE, WS.APPT_TYPE, WS.START_TIME, WS.END_TIME, WS.CLINIC, HCA.ACCOUNT_NAME "
                       + "FROM WW_SCHEDULE WS INNER JOIN HZ_PARTIES WAPD ON WS.IP_PARTY_ID = WAPD.PARTY_ID "
                       + "        INNER JOIN WW_APPT_RELATIONSHIPS WAR ON WS.APPT_ID = WAR.APPT_ID "
                       + "        INNER JOIN HZ_CUST_ACCOUNTS HCA ON WAR.REL_CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID "
                       + "WHERE WS.STATUS = 'A' "
                       + "          AND HCA.ATTRIBUTE_CATEGORY = 'EMPLOYER' "
                       + "          AND HCA.PARTY_ID = " + iPartyID
                       + "          AND WAPD.PERSON_FIRST_NAME LIKE '%" + ipSrchFNameTxt.Text.ToUpper().Trim() + "%' "
                       + "          AND WAPD.PERSON_MIDDLE_NAME LIKE '%" + ipSrchMNameTxt.Text.ToUpper().Trim() + "%' "
                       + "          AND WAPD.PERSON_LAST_NAME LIKE '%" + ipSrchLNameTxt.Text.ToUpper().Trim() + "%' " + ssnQuery + " "
                       + "          AND WS.APPT_TYPE <> 'A' "
                       + "ORDER BY WS.START_TIME DESC ";

            dsCmd = dsCmd = new OdbcDataAdapter(sql, conn);
            ds = new DataSet();
            dsCmd.Fill(ds, "myQuery");

            if (ds.Tables[0].Rows.Count > 0)
            {
                patientApptSrchRsltsGrd.DataSource = ds;
                patientApptSrchRsltsGrd.PageIndex = e.NewPageIndex;
                patientApptSrchRsltsGrd.DataBind();
                patientApptSrchRsltsGrd.Visible = true;
                noPatientSrchRsltsTbl.Visible = false;

            }
            else
            {
                noPatientSrchRsltsTbl.Visible = true;
                patientApptSrchRsltsGrd.Visible = false;
            }

            dsCmd.Dispose();
            ds.Dispose();
        }
        catch (Exception ex)
        {
            Response.Write("Exception: " + Convert.ToString(ex));
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }

        patientSrchRsltsPanel.Visible = true;

    }

    protected bool check_ins_partner(Int32 iPartyID)
    {
        OdbcCommand objSqlCmd = new OdbcCommand();
        string strConnection = "Driver={MySQL ODBC 3.51 Driver};SERVER=166.62.90.46;DATABASE=mewwoh;UID=mewwoh;PWD=RptsPass1;";
        OdbcConnection conn = new OdbcConnection(strConnection);
        bool return_val = false;

        try
        {

            conn.Open();

            string sql = "SELECT 1 FROM WW_INS_PARTNERS WHERE PARTY_ID = " + iPartyID;

            OdbcCommand cmd = new OdbcCommand(sql, conn);
            cmd.CommandType = CommandType.Text;
            OdbcDataReader dr3 = cmd.ExecuteReader();

            if (dr3.Read())
                return_val = true;
            else
                return_val = false;

            dr3.Close();
           
        }
        catch (Exception ex)
        {
            Response.Write("Exception: " + Convert.ToString(ex));
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }

        return return_val;
    }
}